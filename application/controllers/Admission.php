<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admission extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->model('admission_model');
		$this->load->model('academic_model');
		$this->load->model('student_model');
		$this->load->helper('download');
	
	}
	
	public function download($file=null){
        if(!empty($file)){
                  $file1 = 'uploads/certificate/university/'.$file;
           
            force_download($file1, NULL);
        }
    }
    public function download_ten($file=null){
        if(!empty($file)){
                  $file1 = 'uploads/certificate/10th/'.$file;
           
            force_download($file1, NULL);
        }
    }
     public function download_twel($file=null){
        if(!empty($file)){
                  $file1 = 'uploads/certificate/12th/'.$file;
           
            force_download($file1, NULL);
        }
    }
       public function download_dip($file=null){
        if(!empty($file)){
                  $file1 = 'uploads/certificate/diploma/'.$file;
           
            force_download($file1, NULL);
        }
    }
	public function tracker()
	{
		$course_name =str_replace('%20',' ',$this->uri->segment(3));
		//echo $course_name;

		$userdata = $this->session->userdata('logged_in');
		$id=$userdata['id'];
		$comParentData = $this->academic_model->getCourseByComName($course_name);
		
		$comData = $this->academic_model->getCourseByComParent($comParentData->id);

		$ucomData = $this->admission_model->getUComByParent($comParentData->id,$id);
		
		

		
		// print_r(count($ucomData1));
		// print_r(count($ucomData2));
		
		$all_status=array(
				array(
				'id'=>1,
				'staus'=>'New Application'
				),array(
				'id'=>3,
				'staus'=>'Under Review'
				),array(
				'id'=>4,
				'staus'=>'In Process'
				),array(
				'id'=>6,
				'staus'=>'Fees Paid'
				),array(
				'id'=>7,
				'staus'=>'Admission Done'
				),array(
				'id'=>8,
				'staus'=>'Rejected'
				)
		
		);
		//echo '<pre>',print_r($all_status);
		$data['courses'] = $comData;
		$data['all_status'] = $all_status;
		
		$data['dafault_app'] = $ucomData;
		//echo '<pre>',print_r($data['dafault_app']);
		$this->session->set_userdata('count',count($data['dafault_app']));
		
		$data['subview'] = 'admission/tracker';
		$this->load->view('layout_au_main',$data);
	}
	
	// public function student_details()
	// {
	// 	$sid = $this->uri->segment(3);

	//  $as_id = $this->uri->segment(4);
		
	// 	$data['compparent'] = $this->uri->segment(4);
		
	// 	$appStatus = $this->admission_model->getAppUpdatedStatus($as_id);
		
	// 	$data['appStat'] = $appStatus;
	
	// 	$smdata = $this->student_model->getStudentMasterById($sid);
	// 	//print_r($smdata->stm_id);
	// 	$data['personalData'] = $this->student_model->getPersonalDetails($smdata->stm_id);
	// 	if(!empty($data['personalData'])){
	// 			$data['statedata'] = $this->student_model->getStateByCountry($data['personalData']->spd_cm_id);
	// 			$data['citydata'] = $this->student_model->getCityByState($data['personalData']->spd_sm_id);
	// 		}
	
	// 	$data['eduData'] = $this->student_model->getEducationalDetails($smdata->stm_id);
	// 	$data['workData'] = $this->student_model->getWorkDetails($smdata->stm_id);
	// 	// echo '<pre>',print_r($data['eduData']);
	// 	$data['subview'] = 'admission/application_status';
	// 	$this->load->view('layout_au_main',$data);
	// }
public function student_details()
	{
		// $sid = $this->uri->segment(3);

	 // $as_id = $this->uri->segment(4);
		// //echo $as = $this->uri->segment(5);
		// $data['compparent'] = $this->uri->segment(5);
		
		//  $url = str_replace('%20',' ',$this->uri->segment(6));
		// $appStatus = $this->admission_model->getAppUpdatedStatus($as_id);
		
		// $data['appStat'] = $appStatus;
		// $data['spe'] = $url;
	$sid = $this->uri->segment(4);

		  $as_id = $this->uri->segment(5,0);
		//echo $as = $this->uri->segment(3,0);
		$data['compparent'] = $this->uri->segment(3,0);
		$url1= str_replace('%20',' ',$this->uri->segment(6));
		   $url = str_replace('%20',' ',$this->uri->segment(7));
		  // echo str_replace('&',' ',$url);
		// echo $url =urldecode($this->uri->segment(7));
		$appStatus = $this->admission_model->getAppUpdatedStatus($as_id);
		
		$data['appStat'] = $appStatus;
		$data['spe'] = $url;
		$data['spe_cource'] = $url1;
		$smdata = $this->student_model->getStudentMasterById($sid);
		//print_r($smdata->stm_id);
		$data['personalData'] = $this->student_model->getPersonalDetails($smdata->stm_id);
		if(!empty($data['personalData'])){
				$data['statedata'] = $this->student_model->getStateByCountry($data['personalData']->spd_cm_id);
				$data['citydata'] = $this->student_model->getCityByState($data['personalData']->spd_sm_id);
			}
	
		$data['eduData'] = $this->student_model->getEducationalDetails($smdata->stm_id);
		$data['workData'] = $this->student_model->getWorkDetails($smdata->stm_id);
		// echo '<pre>',print_r($data['eduData']);
		$data['subview'] = 'admission/application_status';
		$this->load->view('layout_au_main',$data);
	}
	
	public function admission_form()
	{
		$userdata = $this->session->userdata('logged_in');
		$universedata = $this->academic_model->getUniversityByUser($userdata['id']);
		
		if($this->input->post()){
			
			$progm = explode('-',$this->input->post('prog_name'));
			
			$prgmData = $this->admission_model->getCourseByName(trim($progm[1]));
			
			$prgmMasterData = $this->admission_model->getProgramMasterData($prgmData->com_id);
			
			$fmData['ufm_upm_id'] = $prgmMasterData->upm_id;
			$fmData['ufm_name'] = $this->input->post('prog_name');
			$fmData['ufm_link'] = $this->input->post('form_link');
			$fmData['ufm_admin_approve'] = 'NOTAPPROVED';
			$fmData['ufm_status'] = 'DEACTIVE';
			$fmData['created_at'] = date('Y-m-d h:i:s');
			$fmData['updated_at'] = date('Y-m-d h:i:s');
					
			$ufm_id = $this->admission_model->insertFormMaster($fmData);
			
			if($ufm_id){
				
				$form_fields = $this->input->post('form-fields');
				
				foreach($form_fields as $f_field){
					
					$ffData['uff_ufm_id'] = $ufm_id;
					$ffData['uff_fm_id'] = $f_field;
					$ffData['uff_status'] = '';
					$ffData['uff_sort'] = '';
					$ffData['created_at'] = date('Y-m-d h:i:s');
					$ffData['updated_at'] = date('Y-m-d h:i:s');
					
					$this->admission_model->insertFormFields($ffData);
				}
				
				$this->session->set_flashdata('success','Admission Form Added Successfully');
			    redirect(base_url('admission/admission_form'));
			}
		
		}
		$data['categories'] = $this->admission_model->getCategories();
		$data['catfields'] = $this->admission_model->getCatFeilds();
		$data['subview'] = 'admission/admission_form';
		$this->load->view('layout_au_main',$data);
	}
	
	public function addFormField(){
		
		$fieldId = $this->input->post('formfId');
		$formFieldData = $this->admission_model->getCatFeilds($fieldId);
		
		$onmsover = "this.src='". base_url('assets/img/cross-icon-2.png')."'";
		$onmsout = "this.src='". base_url('assets/img/cross-icon-1.png')."'";
		
	if($formFieldData->fm_type == 'TEXTFIELD' || $formFieldData->fm_type =='TEXTAREA'){
			
			$htmlData = '<div class="col s12 m4 l4" id="frmf-'.$formFieldData->fm_id.'">
							<input type="hidden" name="form-fields[]" value="'.$formFieldData->fm_id.'"/>
							<div class="row">
								<div class="col s12 m4 l4">
									<label class="'.$formFieldData->fm_label .'">'.$formFieldData->fm_label .'</label>
								</div>
								<div class="col s10 m6 l6 no-padding-origin">
									<input type="text" id="'.$formFieldData->fm_form_name .'">
								</div>
								<div class="col s2 m2 l2">
								<span class="admission-form-remove-fields" id="rembutton-'.$formFieldData->fm_id.'"><img 
									src="'.base_url('assets/img/cross-icon-1.png').'" onmouseover="'.$onmsover.'" onmouseout="'.$onmsout.'"
									/></span>
								</div>
							</div>
						</div>';
			
		}else if($formFieldData->fm_type == 'DROPDOWN'){
			
			$htmlData ='<div class="col s12 m4 l4" id="frmf-'.$formFieldData->fm_id.'">
							<input type="hidden" name="form-fields[]" value="'.$formFieldData->fm_id.'"/>
							<div class="row">
								<div class="col s12 m4 l4">
									<label class="'.$formFieldData->fm_label .'">'.$formFieldData->fm_label .'</label>
								</div>
								<div class="col s10 m6 l6 no-padding-origin">
									<select class="browser-default Ustory">
										<option value="" disabled selected>'.$formFieldData->fm_label .'</option>
									</select>
								</div>
								<div class="col s2 m2 l2">
								<span class="admission-form-remove-fields" id="rembutton-'.$formFieldData->fm_id.'"><img
										src="'.base_url('assets/img/cross-icon-1.png').'" onmouseover="'.$onmsover.'" onmouseout="'.$onmsout.'"/></span>
								</div>
							</div>
						</div>';
		}else if($formFieldData->fm_type == 'CHECKBOX'){
			
			$htmlData ='<div class="col s12 m4 l4" id="frmf-'.$formFieldData->fm_id.'">
							<input type="hidden" name="form-fields[]" value="'.$formFieldData->fm_id.'"/>
							<div class="row">
								<div class="col s12 m4 l4">
									<label class="'.$formFieldData->fm_label .'">'.$formFieldData->fm_label .'</label>
								</div>
								<div class="col s10 m6 l6 no-padding-origin">
									 <input type="checkbox" id="test5" />
									  <label for="test5">Option 1</label>
									   <input type="checkbox" id="test6" />
									  <label for="test6">Option 2</label>
								</div>
								<div class="col s2 m2 l2">
								<span class="admission-form-remove-fields" id="rembutton-'.$formFieldData->fm_id.'"><img
										src="'.base_url('assets/img/cross-icon-1.png').'" onmouseover="'.$onmsover.'" onmouseout="'.$onmsout.'"/></span>
								</div>
							</div>
						</div>';
		}else if($formFieldData->fm_type == 'RADIO'){
			
			$formRadioOption = $this->admission_model->getFormRadioOption($formFieldData->fm_id);
			
			$htmlData ='<div class="col s12 m4 l4" id="frmf-'.$formFieldData->fm_id.'">
							<input type="hidden" name="form-fields[]" value="'.$formFieldData->fm_id.'"/>
							<div class="row">
								<div class="col s12 m4 l4">
									<label class="'.$formFieldData->fm_label .'">'.$formFieldData->fm_label .'</label>
								</div>
								<div class="col s10 m6 l6 no-padding-origin">';
				foreach($formRadioOption as $radioOption){					
				$htmlData .= '<input type="radio" id="'.$formFieldData->fm_form_name .'" name="'.$formFieldData->fm_form_name .'"/><label for="'.$radioOption->fr_value .'">'.$radioOption->fr_value .'</label>';
				}					 
			$htmlData .='</div>
								<div class="col s2 m2 l2">
								<span class="admission-form-remove-fields" id="rembutton-'.$formFieldData->fm_id.'"><img
										src="'.base_url('assets/img/cross-icon-1.png').'" onmouseover="'.$onmsover.'" onmouseout="'.$onmsout.'"/></span>
								</div>
							</div>
						</div>';
		}else if($formFieldData->fm_type == 'FILE'){
			
			$htmlData = '<div class="col s12 m4 l4 " id="frmf-'.$formFieldData->fm_id.'">
							<input type="hidden" name="form-fields[]" value="'.$formFieldData->fm_id.'"/>
							<div class="row file-field input-field">
								<div class="col s12 m4 l4">
								<div class="btn upload-btn">
									<span>File</span>
									<input type="file">
								</div>
								</div>
								<div class="col s10 m5 l5 no-padding-origin">	
								<input class="file-path validate" type="text"/>
								</div>
								<div class="col s2 m2 l2">
									<span class="admission-form-remove-fields" id="rembutton-'.$formFieldData->fm_id.'"><img
										src="'.base_url('assets/img/cross-icon-1.png').'" onmouseover="'.$onmsover.'" onmouseout="'.$onmsout.'"/></span>
								</div>
							</div>
						</div>';
		}else if($formFieldData->fm_type == 'DATEPICKER'){
			
			$htmlData = '<div class="col s12 m4 l4" id="frmf-'.$formFieldData->fm_id.'">
							<input type="hidden" name="form-fields[]" value="'.$formFieldData->fm_id.'"/>
							<div class="row">
								<div class="col s12 m4 l4">
									<label class="'.$formFieldData->fm_label .'">'.$formFieldData->fm_label .'</label>
								</div>
								<div class="col s10 m6 l6 no-padding-origin">
									<input type="text" id="'.$formFieldData->fm_form_name .'" placeholder="DD/MM/YYYY">
								</div>
								<div class="col s2 m2 l2">
								<span class="admission-form-remove-fields" id="rembutton-'.$formFieldData->fm_id.'"><img 
									src="'.base_url('assets/img/cross-icon-1.png').'" onmouseover="'.$onmsover.'" onmouseout="'.$onmsout.'"
									/></span>
								</div>
							</div>
						</div>';
			
		}
		echo $htmlData;
	}
	
	public function getAppTrackData(){
		
		$userdata = $this->session->userdata('logged_in');
		
		$universedata = $this->academic_model->getUniversityByUser($userdata['id']);
			
		$specVal = $this->input->post('specVal');
		$umId = $universedata->um_id;
		$unvCsData = $this->academic_model->getUnvCrsByComUmid($specVal,$umId);
		echo json_encode($unvCsData);
	}
	
	public function getAppTrackDataStatus(){
		
		$userdata = $this->session->userdata('logged_in');
		
		$universedata = $this->academic_model->getUniversityByUser($userdata['id']);
			
		$specVal = $this->input->post('specVal');
		 $course_name = str_replace('%20',' ', $this->input->post('cname'));

		$umId = $universedata->um_id;
		
		$comParentData = $this->academic_model->getCourseByComName($course_name);
	
		$comid=$comParentData->id;
		$unvCsData = $this->academic_model->getUnvCrsByComUmid_status($comid,$umId,$specVal);
		echo json_encode($unvCsData);
	}
	
	// public function updateAppStatus(){
			
	// 	$appstatus = $this->input->post('appstatus');
	// 	$as_id = $this->input->post('as_id');
		
	// 	$apps['as_status'] = $appstatus;
		
	// 	if($this->admission_model->updateApplicationStatus($as_id,$apps)){
			
	// 		//$this->session->set_flashdata('success','Status updated successfully');
	// 		echo 'success';
			
	// 	}else{
			
	// 		//$this->session->set_flashdata('error','Status not updated');
	// 		echo 'fail';
	// 	}
		
	// }
	public function updateAppStatus(){
			
		$appstatus = $this->input->post('appstatus');
		$as_id = $this->input->post('as_id');
		//echo $appstatus;die;
		$apps['as_status'] = $appstatus;
		
		if($this->admission_model->updateApplicationStatus($as_id,$apps)){
			
			$this->session->set_flashdata('success','Status updated successfully');
			echo 'success';
			
		}else{
			
			$this->session->set_flashdata('error','Status not updated');
			echo 'fail';
		}
		
	}
	
}
