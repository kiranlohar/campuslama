<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->model('home_model');
		$this->load->model('explore_model');
		$this->load->model('academic_model');
		$this->load->model('login_model');
		$this->load->model('student_model');
		
	}
	public function index()
	{
	
		$data['subview'] = 'home/index';
		
		$this->load->view('layout_main',$data);
		
	}

		public function explore()
	{
		
		if($this->input->post()){
			
		$usrname = $this->input->post('1');
			$usremail = $this->input->post('4');
			$usrcontct = $this->input->post('8');
			$cource_search = $this->input->post('48');
			//print_r($cource_search);die;
			$usrpassword = $this->input->post('password');
			$rep_password = $this->input->post('rep_password');
			
			$postData = $this->input->post();
			$cate_id = $this->input->post('50');
			$type_id = $this->input->post('49');
			//cate_id
			//print_r($cource_search);die;
			$arr_v=array('cate_id'=>$cate_id,'cid'=>$cource_search,'type'=>$type_id);
			$this->session->set_userdata('visitor',$arr_v);
			$udata['name'] = $usrname;
			$udata['email'] = $usremail;
			
			$password = hash('sha256', $rep_password);
			
			$udata['password'] = $password;
			$udata['role'] = 2;
			$udata['randomkey'] = $this->generateRandomString(100);
			$udata['created_at'] = date('Y-m-d h:i:s');
			$udata['updated_at'] = date('Y-m-d h:i:s');
			$userPresentData = $this->login_model->userPresent($usremail);
			if($this->session->userdata('logged_in')==''){
			$loginUserPresent = $this->login_model->LoginPresent($usremail,$password);
		}else{
			$loginUserPresent='';
		}
//print_r($loginUserPresent);
// if($userPresentData==false && $loginUserPresent){
// 	if($this->session->userdata('logged_in')==''){
// 		$sess_array = array(
// 				 'id' => $loginUserPresent->id,
// 				 'name' => $loginUserPresent->name,
// 				 'email' => $loginUserPresent->email,
// 				 'role' => $loginUserPresent->role,
// 				 'status' => $loginUserPresent->status,
// 			   );
// 			   //print_r($sess_array);
// 				$this->session->set_userdata('logged_in', $sess_array);
				
				
// 				$this->session->set_flashdata('success','Welcome '.$sess_array['name'].'..!');
// 				}
				
// 			$userId = $this->login_model->insertUser($udata);
			
// 			if($userId){
				
// 				foreach($postData as $kfid=>$frmval){
					
// 					if(is_numeric($kfid)){
						
// 						$stformdata['ufd_stm_id'] = $userId;
// 						$stformdata['ufd_fm_id'] = $kfid;
// 						$stformdata['ufd_value'] = $frmval;
// 						$stformdata['created_at'] = date('Y-m-d h:i:s');
// 						$stformdata['updated_at'] = date('Y-m-d h:i:s');
						
// 						$this->explore_model->insertStudentField($stformdata);
							
// 					}
// 				}
// 				$smtdata['stm_user_id'] = $userId;
// 					$smtdata['created_at'] = date('Y-m-d h:i:s');
// 					$smtdata['updated_at'] = date('Y-m-d h:i:s');
					
// 					$sm_id = $this->login_model->insertStudentMstr($smtdata);
// 				$this->session->set_flashdata('success', 'You are successfully registered');
// 				if($this->session->userdata('visitor')!=''){
// 							redirect(base_url('search'));
// 						}else{
// 							redirect(base_url('search'));
// 						}	
// 			}
// 		}
// 		else
// 		{
// 			if($this->session->userdata('logged_in')==''){
// 				$sess_array = array(
// 				 'id' => $loginUserPresent->id,
// 				 'name' => $loginUserPresent->name,
// 				 'email' => $loginUserPresent->email,
// 				 'role' => $loginUserPresent->role,
// 				 'status' => $loginUserPresent->status,
// 			   );
// 				$this->session->set_userdata('logged_in', $sess_array);
	
// 				$this->session->set_flashdata('success','Welcome '.$sess_array['name'].'..!');
// 				}
			
// 			foreach($this->input->post() as $k=>$val){
// 				//print_r($val);
// 				$id=$userPresentData->id;
// 						$stformdata['ufd_fm_id'] = $k;
// 						$stformdata['ufd_value'] = $val;
// 						$stformdata['created_at'] = date('Y-m-d h:i:s');
// 						$stformdata['updated_at'] = date('Y-m-d h:i:s');

// 						$this->explore_model->insertStudentField_update($id,$k,$stformdata);

// 			}

// 						$this->session->set_flashdata('success', 'You are successfully updated');
// 				if($this->session->userdata('visitor')!=''){
// 							redirect(base_url('search'));
// 						}else{
// 							redirect(base_url('search'));
// 						}	
// 		}
		//}	
		if($userPresentData==NULL){
			$userId = $this->login_model->insertUser($udata);
			
			if($userId){
		
				$loginUserPresent = $this->login_model->LoginPresent($usremail,$password);
				if($loginUserPresent){
					$sess_array = array(
				 'id' => $loginUserPresent->id,
				 'name' => $loginUserPresent->name,
				 'email' => $loginUserPresent->email,
				 'role' => $loginUserPresent->role,
				 'status' => $loginUserPresent->status,
			   );

				$this->session->set_userdata('logged_in', $sess_array);
				
				
				$this->session->set_flashdata('success','Welcome '.$sess_array['name'].'..!');
				foreach($postData as $kfid=>$frmval){
					
					if(is_numeric($kfid)){
						
						$stformdata['ufd_stm_id'] = $userId;
						$stformdata['ufd_fm_id'] = $kfid;
						$stformdata['ufd_value'] = $frmval;
						$stformdata['created_at'] = date('Y-m-d h:i:s');
						$stformdata['updated_at'] = date('Y-m-d h:i:s');
						
						$this->explore_model->insertStudentField($stformdata);
							
					}
				}
				$smtdata['stm_user_id'] = $userId;
					$smtdata['created_at'] = date('Y-m-d h:i:s');
					$smtdata['updated_at'] = date('Y-m-d h:i:s');
					
					$sm_id = $this->login_model->insertStudentMstr($smtdata);
					$gender='';
			if($this->input->post('10')=='BOY'){
				$gender='Male';
			}else{
				$gender='Female';
			}
			$spdpdata['spd_stm_id']=$sm_id;
			$spdpdata['spd_firstname']=$this->input->post('1');
			$spdpdata['spd_email']=$this->input->post('4');
			$spdpdata['spd_mobile']=$this->input->post('8');
			$spdpdata['spd_dob']=$this->input->post('7').'-'.$this->input->post('6').'-'.$this->input->post('5');
			$spdpdata['spd_gender']=$gender;
			$spdpdata['eligi_cate_id']=$cate_id;
			$this->student_model->insertPersonalData($spdpdata);
				$this->session->set_flashdata('success', 'You are successfully registered');
				if($this->session->userdata('visitor')!='' && $cource_search!=''){
							redirect(base_url('listing/index/'.$cource_search));
						}else{
							redirect(base_url('search'));
						}
					//redirect(base_url('search'));	
					}
			}
		}
		else
		{


			if($this->session->userdata('logged_in')==''){
				$sess_array = array(
				 'id' => $loginUserPresent->id,
				 'name' => $loginUserPresent->name,
				 'email' => $loginUserPresent->email,
				 'role' => $loginUserPresent->role,
				 'status' => $loginUserPresent->status,
			   );
				$this->session->set_userdata('logged_in', $sess_array);
	
				$this->session->set_flashdata('success','Welcome '.$sess_array['name'].'..!');
				}else{


			
			foreach($this->input->post() as $k=>$val){
				//print_r($val);
				$id=$userPresentData->id;
						$stformdata['ufd_fm_id'] = $k;
						$stformdata['ufd_value'] = $val;
						$stformdata['created_at'] = date('Y-m-d h:i:s');
						$stformdata['updated_at'] = date('Y-m-d h:i:s');

						$this->explore_model->insertStudentField_update($id,$k,$stformdata);

			}
			$query=$this->db->where('stm_user_id',$userPresentData->id)->get('student_mstr');
			$data=$query->result();
					$gender='';
			if($this->input->post('10')=='BOY'){
				$gender='Male';
			}else{
				$gender='Female';
			}
		//	$spdpdata['spd_stm_id']=$sm_id;
			$spdpdata['spd_firstname']=$this->input->post('1');
			$spdpdata['spd_email']=$this->input->post('4');
			$spdpdata['spd_mobile']=$this->input->post('8');
			$spdpdata['spd_dob']=$this->input->post('7').'-'.$this->input->post('6').'-'.$this->input->post('5');
			$spdpdata['spd_gender']=$gender;
			$spdpdata['eligi_cate_id']=$cate_id;
			$spd_id=$data[0]->stm_id;
			$this->student_model->updatePersonalData_exp($spdpdata,$spd_id);
}
						$this->session->set_flashdata('success', 'You are successfully updated');
				if($this->session->userdata('visitor')!='' && $cource_search!=''){
							redirect(base_url('listing/index/'.$cource_search));
						}else{
							redirect(base_url('search'));
						}
		}
		}else{


			$this->load->view('components/front_header_1');
			
			$data['course_master'] = $this->explore_model->getCourseMaster();
			$data['mst_type']=$this->academic_model->getTypeProgram();
			$data['eligibility']=$this->academic_model->getEligiblity();
			$data['top_uni'] = $this->explore_model->get_top_university();
			$data['testimonial'] = $this->explore_model->get_testimonial();
			$data['top_cities']=$this->explore_model->get_top_cities();
			$data['top_cities_5']=$this->explore_model->getUniversity_city_top_5();
			$this->load->view('explore/index',$data);
			//$this->load->view('components/front_footer_1');
		}
	}
	
	function generateRandomString($length = 10) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}
}
