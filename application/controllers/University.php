<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class University extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->model('university_model');
		$this->load->model('academic_model');
	}
}
