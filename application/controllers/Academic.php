<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Academic extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->model('academic_model');
		$this->load->model('login_model');
		$this->load->model('student_model');
		
	}
	
	public function academic_profile()
	{
		$userdata = $this->session->userdata('logged_in');
		$universedata = $this->academic_model->getUniversityByUser($userdata['id']);
		$data['academicPhoto'] = '';
		$data['academicVideo'] = '';
		$data['acfacility'] = array();
		$data['universityData'] = $universedata;
		if(!empty($universedata)){
		$data['academicPhoto'] = $this->academic_model->getAcademicPhoto($universedata->um_id);
		$data['academicVideo'] = $this->academic_model->getAcademicVideo($universedata->um_id);
		$allfacility = $this->academic_model->getAcademicFacility($universedata->um_id);
		
		$newFac = array();
		
		foreach($allfacility as $afacil){
			array_push($newFac,$afacil->ufc_name);
		}
		
		$data['acfacility'] = $newFac;
		
		}
		//print_r($data['universityData']->state_id);
		if (!empty($universedata)) {
		 $data['statedata'] = $this->student_model->getStateByCountry($data['universityData']->country_id);
		 $data['citydata'] = $this->student_model->getCityByState($data['universityData']->state_id);
		}
		 //print_r($data['citydata']);
		$data['subview'] = 'academic/academic_profile';
		$this->load->view('layout_au_main',$data);
	}
	
	public function program_manager()
	{
		
		$userdata = $this->session->userdata('logged_in');
		$universedata = $this->academic_model->getUniversityByUser($userdata['id']);
		//print_r($this->input->post());die;
		if($this->input->post()){
			
				$udata['name'] = $this->input->post('admin_name');
				$udata['email'] = $this->input->post('admin_email');
				
				$password = '';
				
				$udata['password'] = $password;
				$udata['role'] = 6;
				$udata['randomkey'] = $this->generateRandomString(100);
				$udata['created_at'] = date('Y-m-d h:i:s');
				$udata['updated_at'] = date('Y-m-d h:i:s');
				
				$userId = $this->login_model->insertUser($udata);
				
				if($userId){
					
					$upmdata['upm_um_id'] = $universedata->um_id;
					$upmdata['upm_type'] = $this->input->post('program_type');
					$upmdata['upm_com_id_stream'] = $this->input->post('stream');
					$upmdata['upm_com_id_program'] = $this->input->post('program');
					$upmdata['created_at'] = date('Y-m-d h:i:s');
					$upmdata['updated_at'] = date('Y-m-d h:i:s');
					
					$upmid = $this->academic_model->insertUPM($upmdata);
					
					if($upmid){
						
						$upmadmindata['upa_upm_id'] = $upmid;
						$upmadmindata['upa_user_id'] = $userId;
						$upmadmindata['upm_admin_name'] = $this->input->post('admin_name');
						$upmadmindata['upm_admin_email'] = $this->input->post('admin_email');
						$upmadmindata['upm_admin_mobile'] = $this->input->post('admin_mobile');
						$upmadmindata['created_at'] = date('Y-m-d h:i:s');
						$upmadmindata['updated_at'] = date('Y-m-d h:i:s');
						
						$upmadminid = $this->academic_model->insertUPMAdmin($upmadmindata);
						
						if($upmadminid){
							
							$upshowdata['uc_um_id'] = $universedata->um_id;
							$upshowdata['uc_com_id'] = $this->input->post('program');
							$upshowdata['uc_upa_id'] = $upmadminid;
							//$upshowdata['uc_fees'] = implode(',',$this->input->post('fees'));
							$upshowdata['uc_course_length'] = $this->input->post('duration').' '.$this->input->post('dur-unit');
							$upshowdata['uc_course_type'] = $this->input->post('program_type');
							$upshowdata['uc_parent'] = $this->input->post('stream');
							$upshowdata['created_at'] = date('Y-m-d h:i:s');
							$upshowdata['updated_at'] = date('Y-m-d h:i:s');
							
							$uc_id = $this->academic_model->insertUCourse($upshowdata);
							
							if($uc_id){
								$this->session->set_flashdata('success','Program Manager Added Successfully');
								redirect(base_url('academic/program_manager'));
								//echo json_encode('Program Manager Added Successfully');
							}
						}
					}
				}
		}
		 $data['streams'] = $this->academic_model->getCourseMainMaster();
		$data['fees'] = $this->academic_model->getFeesMaster();
		$data['university'] = $universedata;
		
		if(!empty($universedata)){
			$data['programManager'] = $this->academic_model->getProjectManagerByUmId($universedata->um_id);
		}
		//echo json_encode($data['programManager']);
		$data['type']=$this->academic_model->getTypeProgram();
		$data['spe']=$this->academic_model->getSpe();
		$data['eligiblity']=$this->academic_model->getEligiblity();
		$data['course_spe']=$this->academic_model->getCourseSpe_course();
		$data['subview'] = 'academic/program_manager';
		$this->load->view('layout_au_main',$data); 
	}
	
	public function add_pro()
	{
		$userdata = $this->session->userdata('logged_in');
		$universedata = $this->academic_model->getUniversityByUser($userdata['id']);
		//print_r($this->input->post());die;
		if($this->input->post()){
			
				$udata['name'] = $this->input->post('admin_name');
				$udata['email'] = $this->input->post('admin_email');
				
				$password = '';
				
				$udata['password'] = $password;
				$udata['role'] = 6;
				$udata['randomkey'] = $this->generateRandomString(100);
				$udata['created_at'] = date('Y-m-d h:i:s');
				$udata['updated_at'] = date('Y-m-d h:i:s');
				
				$userId = $this->login_model->insertUser($udata);
				
				if($userId){
					
					$upmdata['upm_um_id'] = $universedata->um_id;
					$upmdata['upm_type'] = $this->input->post('program_type');
					$upmdata['upm_com_id_stream'] = $this->input->post('stream');
					$upmdata['upm_com_id_program'] = $this->input->post('program');
					$upmdata['created_at'] = date('Y-m-d h:i:s');
					$upmdata['updated_at'] = date('Y-m-d h:i:s');
					
					$upmid = $this->academic_model->insertUPM($upmdata);
					
					if($upmid){
						
						$upmadmindata['upa_upm_id'] = $upmid;
						$upmadmindata['upa_user_id'] = $userId;
						$upmadmindata['upm_admin_name'] = $this->input->post('admin_name');
						$upmadmindata['upm_admin_email'] = $this->input->post('admin_email');
						$upmadmindata['upm_admin_mobile'] = $this->input->post('admin_mobile');
						$upmadmindata['created_at'] = date('Y-m-d h:i:s');
						$upmadmindata['updated_at'] = date('Y-m-d h:i:s');
						
						$upmadminid = $this->academic_model->insertUPMAdmin($upmadmindata);
						
						if($upmadminid){
							
							$upshowdata['uc_um_id'] = $universedata->um_id;
							$upshowdata['uc_com_id'] = $this->input->post('program');
							$upshowdata['uc_upa_id'] = $upmadminid;
							$upshowdata['uc_fees'] = $this->input->post('fees').' '.$this->input->post('dur-unit-1');
							$upshowdata['uc_course_length'] = $this->input->post('duration').' '.$this->input->post('dur-unit');
							$upshowdata['uc_course_type'] = $this->input->post('program_type');
							$upshowdata['uc_parent'] = $this->input->post('stream');
							$upshowdata['uc_eligibility'] =  implode(',',$this->input->post('eligibility'));
							$upshowdata['uc_info'] = $this->input->post('about');
							$upshowdata['uc_objective'] = $this->input->post('objective');
							$upshowdata['uc_spe_id'] = $this->input->post('course_spe');
							
							$upshowdata['created_at'] = date('Y-m-d h:i:s');
							$upshowdata['updated_at'] = date('Y-m-d h:i:s');
							
							$uc_id = $this->academic_model->insertUCourse($upshowdata);
							
							if($uc_id){
								//$this->session->set_flashdata('success','Program Manager Added Successfully');
								//redirect(base_url('academic/program_manager'));
								echo json_encode('Program Manager Added Successfully');
							}
						}
					}
				}
		}
	}
	
	public function get_ajax()
	{
		
		$userdata = $this->session->userdata('logged_in');
		$universedata = $this->academic_model->getUniversityByUser($userdata['id']);
		$data['university'] = $universedata;
		//print_r($universedata->um_id);
		if(!empty($universedata)){
			$data['programManager'] = $this->academic_model->getProjectManagerByUmId($universedata->um_id);
		}
		echo json_encode($data['programManager']);
	}
	public function edit_pro()
	{
		//echo $uc_id = $this->uri->segment(3);
		$uc_id=$this->input->post('id');
		//$data['type']=$this->academic_model->getTypeProgram();
		//$data['streams'] = $this->academic_model->getCourseMainMaster();
		$data['programManager'] = $this->academic_model->getProgManager($uc_id);
		echo json_encode($data['programManager']);
		//echo '<pre>'; print_r($data['programManager']);die;
		//$data['programs'] = $this->academic_model->getCourseMainMaster($data['programManager']->com_parent);
		//$data['fees'] = $this->academic_model->getFeesMaster();
	}
	public function edit_program_manager()
	{
		
		$userdata = $this->session->userdata('logged_in');
		$universedata = $this->academic_model->getUniversityByUser($userdata['id']);
		// print_r($this->input->post());
		// exit;
		if($this->input->post()){
			
				$udata['name'] = $this->input->post('admin_name');
				$udata['email'] = $this->input->post('admin_email');
				$userId = $this->input->post('userId');
				$upmid = $this->input->post('upmid');
				$upmadminid = $this->input->post('upmadminid');
				$uc_id = $this->input->post('uc_id');
				
				if($this->login_model->updateUser($udata,$userId)){
					
					$upmdata['upm_um_id'] = $universedata->um_id;
					$upmdata['upm_type'] = $this->input->post('program_type');
					$upmdata['upm_com_id_stream'] = $this->input->post('stream');
					$upmdata['upm_com_id_program'] = $this->input->post('program');
					$upmdata['updated_at'] = date('Y-m-d h:i:s');
					
					if($this->academic_model->updateUPM($upmdata,$upmid)){
						
						$upmadmindata['upm_admin_name'] = $this->input->post('admin_name');
						$upmadmindata['upm_admin_email'] = $this->input->post('admin_email');
						$upmadmindata['upm_admin_mobile'] = $this->input->post('admin_mobile');
						$upmdata['updated_at'] = date('Y-m-d h:i:s');
						
						if($this->academic_model->updateUPMAdmin($upmadmindata,$upmadminid)){
							
							$upshowdata['uc_um_id'] = $universedata->um_id;
							$upshowdata['uc_com_id'] = $this->input->post('program');
							$upshowdata['uc_upa_id'] = $upmadminid;
							$upshowdata['uc_fees'] = $this->input->post('fees').' '.$this->input->post('dur-unit-1');
							$upshowdata['uc_course_length'] = $this->input->post('duration').' '.$this->input->post('dur-unit');
							$upshowdata['uc_course_type'] = $this->input->post('program_type');
							$upshowdata['uc_eligibility'] =  implode(',',$this->input->post('eligibility'));
							$upshowdata['uc_info'] = $this->input->post('about');
							$upshowdata['uc_objective'] = $this->input->post('objective');
							$upshowdata['uc_spe_id'] = $this->input->post('course_spe');
							$upshowdata['uc_parent'] = $this->input->post('stream');
							$upshowdata['updated_at'] = date('Y-m-d h:i:s');
							
							if($this->academic_model->updateUCourse($upshowdata,$uc_id)){
								//$this->session->set_flashdata('success','Program Manager Updated Successfully');
								echo json_encode('Program Manager Updated Successfully');	
							}
						}
					}
				}
		}
		
	/* 	echo $uc_id = $this->uri->segment(3);
		$data['type']=$this->academic_model->getTypeProgram();
		$data['streams'] = $this->academic_model->getCourseMainMaster();
		$data['programManager'] = $this->academic_model->getProgManager($uc_id);
		//echo '<pre>'; print_r($data['programManager']);die;
		$data['programs'] = $this->academic_model->getCourseMainMaster($data['programManager']->com_parent);
		$data['fees'] = $this->academic_model->getFeesMaster();
		$data['subview'] = 'academic/edit_program_manager';
		$this->load->view('layout_au_main',$data); */
	}
	
	
	public function user_setting()
	{
		$data['subview'] = 'academic/user_setting';
		$this->load->view('layout_au_main',$data);
	}
	public function update_user_setting()
	{
		$username=$this->input->post('username');
		$useremail=$this->input->post('useremail');
		$userid=$this->input->post('userid');
		$data=array(
		
			'name'=>$username,
			'email'=>$useremail
		);
		$re=$this->academic_model->update_user_profile($userid,$data);
		if($re)
		{
			echo json_encode(array('msg'=>'success'));
		}
	}
	
	public function get_user_details()
	{
		
		$userid=$this->input->post('userid');
		$roleid=$this->input->post('roleid');
		
		$re=$this->academic_model->get_user_profile($userid,$roleid);
		
			echo json_encode($re);
		
	}
	public function change_pass()
	{
		
		$oldpass=$this->input->post('oldpass');
		$newpass=$this->input->post('newpass');
		$userid=$this->input->post('userid');
		$roleid=$this->input->post('roleid');
		
		$re=$this->academic_model->check_old_pass($userid,$roleid,$oldpass);
		if($re)
		{
			
			$npass=array('password'=>hash('sha256',$newpass));
			$result=$this->academic_model->update_pass($userid,$roleid,$npass);
			if($result){
				echo json_encode(array('msg'=>'Password Changed Successfully'));
			}
		}
		else{
			echo json_encode(array('msg'=>'Invalid Old Password'));
		}
			
		
	}
	public function fees_plan()
	{
		$userdata = $this->session->userdata('logged_in');
		$universedata = $this->academic_model->getUniversityByUser($userdata['id']);
		
		if($this->input->post()){
			
			$plan_name = $this->input->post('plan_name');
			$plan_type = $this->input->post('plan_type');
			
			if($plan_type == 'Installment'){
				$amount = implode(',',$this->input->post('install_amount'));
				$num_install = $this->input->post('num_install');
			}else{
				$amount = $this->input->post('lumpsum_amount');
				$num_install =1;
			}
			
			$fpdata['ufeem_um_id'] = $universedata->um_id;
			$fpdata['ufeem_plan_name'] = $plan_name;
			$fpdata['ufeem_plan_type'] = $plan_type;
			$fpdata['ufeem_num_install'] = $num_install;
			$fpdata['ufeem_amount'] = $amount;
			$fpdata['created_at'] = date('Y-m-d h:i:s');
			$fpdata['updated_at'] = date('Y-m-d h:i:s');
			
			$fpId = $this->academic_model->insertFeesPlan($fpdata);
			
			if($fpId){
				
				$this->session->set_flashdata('success','Fees Plan Added Successfully');
				redirect(base_url('academic/fees_plan'));
				
			}
		}
		
		$data['feesdata'] = $this->academic_model->getFeesMaster();
		
		$data['subview'] = 'academic/fees_plan';
		$this->load->view('layout_au_main',$data);
	}
	
	public function edit_fees_plan()
	{
		$userdata = $this->session->userdata('logged_in');
		$universedata = $this->academic_model->getUniversityByUser($userdata['id']);
		
		if($this->input->post()){
			
			$ufee_id = $this->input->post('ufee_id');
			$plan_name = $this->input->post('plan_name');
			$plan_type = $this->input->post('plan_type');
			
			if($plan_type == 'Installment'){
				$amount = implode(',',$this->input->post('install_amount'));
				$num_install = $this->input->post('num_install');
			}else{
				$amount = $this->input->post('lumpsum_amount');
				$num_install =1;
			}
			
			$fpdata['ufeem_um_id'] = $universedata->um_id;
			$fpdata['ufeem_plan_name'] = $plan_name;
			$fpdata['ufeem_plan_type'] = $plan_type;
			$fpdata['ufeem_num_install'] = $num_install;
			$fpdata['ufeem_amount'] = $amount;
			$fpdata['created_at'] = date('Y-m-d h:i:s');
			$fpdata['updated_at'] = date('Y-m-d h:i:s');
			
			if($this->academic_model->updateFeesPlan($fpdata,$ufee_id)){
				
				$this->session->set_flashdata('success','Fees Plan Updated Successfully');
				redirect(base_url('academic/fees_plan'));
				
			}
		}
		
		$ufee_id = $this->uri->segment(3);
		$data['feesdata'] = $this->academic_model->getFeesMaster($ufee_id);
		$data['type']=$this->academic_model->getTypeProgram();
		$data['subview'] = 'academic/edit_fees_plan';
		$this->load->view('layout_au_main',$data);
	}
	
	public function stream_program()
	{
		$streamid = $this->input->post('streamid');
		$programdata = $this->academic_model->getCourseMainMaster($streamid);
		echo json_encode($programdata);
	}

	public function stream_program_delete()
	{
		$streamid = $this->input->post('id');
		$arr=array('uc_is_approved'=>2);
		$programdata = $this->academic_model->getCourseMainMaster_del($streamid,$arr);
		echo json_encode(array('msg'=>'delete'));
	}
	public function stream_program_active()
	{
		$streamid = $this->input->post('id');
		$arr=array('uc_is_approved'=>1);
		$programdata = $this->academic_model->getCourseMainMaster_del($streamid,$arr);
		echo json_encode(array('msg'=>'active'));
	}
	
	public function stream_program_spe()
	{
		$streamid = $this->input->post('streamid');
		$programdata = $this->academic_model->getCourse_spe($streamid);
		echo json_encode($programdata);
	}
	
	
	
	function generateRandomString($length = 10) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}
	
	function photoUpload(){
		
		$userdata = $this->session->userdata('logged_in');
		$universedata = $this->academic_model->getUniversityByUser($userdata['id']);
		
	    if(is_array($_FILES)) {
			
			/*********************** Start Photos **********************************/
			
			if(!empty($_FILES['Infra-file']['name'])){
				
				if(is_uploaded_file($_FILES['Infra-file']['tmp_name'])) {
					
					$sourcePath = $_FILES['Infra-file']['tmp_name'];
					$temp = explode(".", $_FILES["Infra-file"]["name"]);
					$newfilename = round(microtime(true)) . '.' . end($temp);
					$targetPath = "uploads/".$newfilename;
					$outimg = '';
					move_uploaded_file($sourcePath,$targetPath);
					
					$category = 'INFRASTRUCTURE';
					$media_type = 'PHOTO';
					if($this->input->post('Infra-caption')){
						$caption = $this->input->post('Infra-caption');
					}else{
						$caption ='';
					}
				}
				
			}else if(!empty($_FILES['sp-file']['name'])){
				
				if(is_uploaded_file($_FILES['sp-file']['tmp_name'])) {
				
					$sourcePath = $_FILES['sp-file']['tmp_name'];
					$temp = explode(".", $_FILES["sp-file"]["name"]);
					$newfilename = round(microtime(true)) . '.' . end($temp);
					$targetPath = "uploads/".$newfilename;
					$outimg = '';
					move_uploaded_file($sourcePath,$targetPath);
					
					$category = 'STUDENT PLAY';
					$media_type = 'PHOTO';
					if($this->input->post('sp-caption')){
						$caption = $this->input->post('sp-caption');
					}else{
						$caption ='';
					}
				}
			}else if(!empty($_FILES['scl-file']['name'])){
				
				if(is_uploaded_file($_FILES['scl-file']['tmp_name'])) {
				
					$sourcePath = $_FILES['scl-file']['tmp_name'];
					$temp = explode(".", $_FILES["scl-file"]["name"]);
					$newfilename = round(microtime(true)) . '.' . end($temp);
					$targetPath = "uploads/".$newfilename;
					$outimg = '';
					move_uploaded_file($sourcePath,$targetPath);
					
					$category = 'STUDENT IN CLASSROOM AND LAB';
					$media_type = 'PHOTO';
					if($this->input->post('scl-caption')){
						$caption = $this->input->post('scl-caption');
					}else{
						$caption ='';
					}
				}
			}else if(!empty($_FILES['saa-file']['name'])){
				
				if(is_uploaded_file($_FILES['saa-file']['tmp_name'])) {
				
					$sourcePath = $_FILES['saa-file']['tmp_name'];
					$temp = explode(".", $_FILES["saa-file"]["name"]);
					$newfilename = round(microtime(true)) . '.' . end($temp);
					$targetPath = "uploads/".$newfilename;
					$outimg = '';
					move_uploaded_file($sourcePath,$targetPath);
					
					$category = 'STUDENT AT ACTION';
					$media_type = 'PHOTO';
					if($this->input->post('saa-caption')){
						$caption = $this->input->post('saa-caption');
					}else{
						$caption ='';
					}
				}
			}else if(!empty($_FILES['sn-file']['name'])){
				
				if(is_uploaded_file($_FILES['sn-file']['tmp_name'])) {
				
					$sourcePath = $_FILES['sn-file']['tmp_name'];
					$temp = explode(".", $_FILES["sn-file"]["name"]);
					$newfilename = round(microtime(true)) . '.' . end($temp);
					$targetPath = "uploads/".$newfilename;
					$outimg = '';
					move_uploaded_file($sourcePath,$targetPath);
					
					$category = 'STUDENT NEWS';
					$media_type = 'PHOTO';
					if($this->input->post('sn-caption')){
						$caption = $this->input->post('sn-caption');
					}else{
						$caption ='';
					}
				}
			}else if(!empty($_FILES['spro-file']['name'])){
				
				if(is_uploaded_file($_FILES['spro-file']['tmp_name'])) {
				
					$sourcePath = $_FILES['spro-file']['tmp_name'];
					$temp = explode(".", $_FILES["spro-file"]["name"]);
					$newfilename = round(microtime(true)) . '.' . end($temp);
					$targetPath = "uploads/".$newfilename;
					$outimg = '';
					move_uploaded_file($sourcePath,$targetPath);
					
					$category = 'STUDENT PROJECT';
					$media_type = 'PHOTO';
					if($this->input->post('spro-caption')){
						$caption = $this->input->post('spro-caption');
					}else{
						$caption ='';
					}
				}
			}
			/*********************** End Photos **********************************/
			
			/*********************** Start Videos **********************************/
			
			if(!empty($_FILES['Infra-vid-file']['name'])){
				
				if(is_uploaded_file($_FILES['Infra-vid-file']['tmp_name'])) {
					
					$sourcePath = $_FILES['Infra-vid-file']['tmp_name'];
					$temp = explode(".", $_FILES["Infra-vid-file"]["name"]);
					$newfilename = round(microtime(true)) . '.' . end($temp);
					$targetPath = "uploads/".$newfilename;
					$outimg = '';
					move_uploaded_file($sourcePath,$targetPath);
					
					$category = 'INFRASTRUCTURE VIDEO';
					$media_type = 'VIDEO';
					if($this->input->post('Infra-vid-caption')){
						$caption = $this->input->post('Infra-vid-caption');
					}else{
						$caption ='';
					}
				}
				
			}else if(!empty($_FILES['sp-vid-file']['name'])){
				
				if(is_uploaded_file($_FILES['sp-vid-file']['tmp_name'])) {
				
					$sourcePath = $_FILES['sp-vid-file']['tmp_name'];
					$temp = explode(".", $_FILES["sp-vid-file"]["name"]);
					$newfilename = round(microtime(true)) . '.' . end($temp);
					$targetPath = "uploads/".$newfilename;
					$outimg = '';
					move_uploaded_file($sourcePath,$targetPath);
					
					$category = 'STUDENT PLAY VIDEO';
					$media_type = 'VIDEO';
					if($this->input->post('sp-vid-caption')){
						$caption = $this->input->post('sp-vid-caption');
					}else{
						$caption ='';
					}
				}
			}else if(!empty($_FILES['scl-vid-file']['name'])){
				
				if(is_uploaded_file($_FILES['scl-vid-file']['tmp_name'])) {
				
					$sourcePath = $_FILES['scl-vid-file']['tmp_name'];
					$temp = explode(".", $_FILES["scl-vid-file"]["name"]);
					$newfilename = round(microtime(true)) . '.' . end($temp);
					$targetPath = "uploads/".$newfilename;
					$outimg = '';
					move_uploaded_file($sourcePath,$targetPath);
					
					$category = 'STUDENT IN CLASSROOM AND LAB VIDEO';
					$media_type = 'VIDEO';
					if($this->input->post('scl-vid-caption')){
						$caption = $this->input->post('scl-vid-caption');
					}else{
						$caption ='';
					}
				}
			}else if(!empty($_FILES['saa-vid-file']['name'])){
				
				if(is_uploaded_file($_FILES['saa-vid-file']['tmp_name'])) {
				
					$sourcePath = $_FILES['saa-vid-file']['tmp_name'];
					$temp = explode(".", $_FILES["saa-vid-file"]["name"]);
					$newfilename = round(microtime(true)) . '.' . end($temp);
					$targetPath = "uploads/".$newfilename;
					$outimg = '';
					move_uploaded_file($sourcePath,$targetPath);
					
					$category = 'STUDENT AT ACTION VIDEO';
					$media_type = 'VIDEO';
					if($this->input->post('saa-vid-caption')){
						$caption = $this->input->post('saa-vid-caption');
					}else{
						$caption ='';
					}
				}
			}else if(!empty($_FILES['sn-vid-file']['name'])){
				
				if(is_uploaded_file($_FILES['sn-vid-file']['tmp_name'])) {
				
					$sourcePath = $_FILES['sn-vid-file']['tmp_name'];
					$temp = explode(".", $_FILES["sn-vid-file"]["name"]);
					$newfilename = round(microtime(true)) . '.' . end($temp);
					$targetPath = "uploads/".$newfilename;
					$outimg = '';
					move_uploaded_file($sourcePath,$targetPath);
					
					$category = 'STUDENT NEWS VIDEO';
					$media_type = 'VIDEO';
					if($this->input->post('sn-vid-caption')){
						$caption = $this->input->post('sn-vid-caption');
					}else{
						$caption ='';
					}
				}
			}else if(!empty($_FILES['spro-vid-file']['name'])){
				
				if(is_uploaded_file($_FILES['spro-vid-file']['tmp_name'])) {
				
					$sourcePath = $_FILES['spro-vid-file']['tmp_name'];
					$temp = explode(".", $_FILES["spro-vid-file"]["name"]);
					$newfilename = round(microtime(true)) . '.' . end($temp);
					$targetPath = "uploads/".$newfilename;
					$outimg = '';
					move_uploaded_file($sourcePath,$targetPath);
					
					$category = 'STUDENT PROJECT VIDEO';
					$media_type = 'VIDEO';
					if($this->input->post('spro-vid-caption')){
						$caption = $this->input->post('spro-vid-caption');
					}else{
						$caption ='';
					}
				}
			}
			
			/*********************** End Videos **********************************/
			
			
			$fudata['ugm_um_id'] = $universedata->um_id;
			$fudata['ugm_link'] = $newfilename;
			$fudata['ugm_info'] = $caption;
			$fudata['ugm_category'] = $category;
			$fudata['ugm_media_type'] = $media_type;
			$fudata['created_at'] = date('Y-m-d h:i:s');
			$fudata['updated_at'] = date('Y-m-d h:i:s');
					
			$umphotoid = $this->academic_model->insertUniversityPhoto($fudata);
			
			if($umphotoid){
				
				if($media_type == 'PHOTO'){
					
					$outimg .= '<div class="col s12 m6 l3 center-align  image_div">
								<a class="ap-image-remove media_remove" id="photo_'.$umphotoid.'"><img style="margin-left: 132px;cursor:pointer" src="'. base_url("assets/images/cross-icon-2.png").'"></a>
								<img src="'.base_url($targetPath).'"  class="responsive-img media_media"><p class="ap-image-name">'.$caption.'</p>';
								
					$outimg .= '</div>';
					
				}else{
					
					$outimg .= '<div class="col s12 m5 l5 center-align video_div"><a class="ap-image-remove media_remove" id="video_'.$umphotoid.'" style="float: right;"><img style="margin-left: 132px;cursor:pointer" src="'. base_url("assets/images/cross-icon-2.png").'"></a><div class="video-container"><video width="200" height="120" controls=""><source src="'.base_url($targetPath).'" type="video/ogg">Your browser does not support the video element.</video></div<p class="ap-image-name vheading">'.$caption.'</p></div>';
					
				}
			}
			
			echo $outimg;
		}
		
	}
	
	function removePhotoUploaded(){
		if($this->input->post()){
			$rmphotoId = $this->input->post('rmphotoId');
			if($this->academic_model->deleteUniversityPhoto($rmphotoId)){
				echo 'success';
			}else{
				echo 'fail';
			}
		}
	}
	
	function addPersonalInfo(){
		
		if($this->input->post('subtype') == 'add'){
			
			$userdata = $this->session->userdata('logged_in');
			$userId = $userdata['id'];
			$fullname = $this->input->post('fullname');
			$affiliation = $this->input->post('affiliation');
			$email = $this->input->post('email');
			$address = $this->input->post('address');
			$country = $this->input->post('country');
			$state = $this->input->post('state');
			$city = $this->input->post('city');
			$contact_number = $this->input->post('contact_number');
			$micro_url = $this->input->post('micro_url');
			$about_university = $this->input->post('about_university');
			
			if(!empty($_FILES['logo']['name'])){
					
				if(is_uploaded_file($_FILES['logo']['tmp_name'])) {
					
					$sourcePath = $_FILES['logo']['tmp_name'];
					$temp = explode(".", $_FILES["logo"]["name"]);
					$newfilename = round(microtime(true)) . '.' . end($temp);
					$targetPath = "uploads/logos/".$newfilename;
					$outimg = '';
					move_uploaded_file($sourcePath,$targetPath);
					$limage = $newfilename;
				}
					
			}else{
				$limage = '';
			}
			
			if(!empty($_FILES['cover_image']['name'])){
					
				if(is_uploaded_file($_FILES['cover_image']['tmp_name'])) {
					
					$sourcePath = $_FILES['cover_image']['tmp_name'];
					$temp = explode(".", $_FILES["cover_image"]["name"]);
					$newfilename = round(microtime(true)) . '.' . end($temp);
					$targetPath = "uploads/coverimages/".$newfilename;
					$outimg = '';
					move_uploaded_file($sourcePath,$targetPath);
					$cimage = $newfilename;
				}
				
			}else{
				$cimage ='';
			}
			
			$udata['um_name'] = $fullname;
			$udata['um_user_id'] = $userId;
			$udata['um_link'] = $micro_url;
			$udata['um_mobile'] = $contact_number;
			$udata['um_status'] = 'ACCEPTED';
			$udata['created_at'] = date('Y-m-d h:i:s');
			$udata['updated_at'] = date('Y-m-d h:i:s');
			
			$um_id = $this->academic_model->addUniversity($udata);
			
			if($um_id){
				
				$uinfodata['ui_um_id'] = $um_id;
				$uinfodata['ui_aboutus'] = $about_university;
				$uinfodata['ui_recognition'] = $affiliation;
				$uinfodata['ui_coverimage'] = $cimage;
				$uinfodata['ui_logoimage'] = $limage;
				$uinfodata['ui_address'] = $address;
				$uinfodata['country_id'] = $country;
				$uinfodata['state_id'] = $state;
				$uinfodata['city_id'] = $city;
				
				$uinfodata['ui_contact'] = $contact_number;
				$uinfodata['created_at'] = date('Y-m-d h:i:s');
				$uinfodata['updated_at'] = date('Y-m-d h:i:s');
				
				$ui_id = $this->academic_model->addUniversityInfo($uinfodata);
				
				if($ui_id){
					echo 'University added successfully';
				}
			}
			
		}else{
			
			$userdata = $this->session->userdata('logged_in');
			$userId = $userdata['id'];
			$fullname = $this->input->post('fullname');
			$affiliation = $this->input->post('affiliation');
			$email = $this->input->post('email');
			$address = $this->input->post('address');
			$country = $this->input->post('country');
			$state = $this->input->post('state');
			$city = $this->input->post('city');
			$contact_number = $this->input->post('contact_number');
			$micro_url = $this->input->post('micro_url');
			$about_university = $this->input->post('about_university');
			
			if(!empty($_FILES['logo']['name'])){
					
				if(is_uploaded_file($_FILES['logo']['tmp_name'])) {
					
					$sourcePath = $_FILES['logo']['tmp_name'];
					$temp = explode(".", $_FILES["logo"]["name"]);
					$newfilename = round(microtime(true)) . '.' . end($temp);
					$targetPath = "uploads/logos/".$newfilename;
					$outimg = '';
					move_uploaded_file($sourcePath,$targetPath);
					$limage = $newfilename;
				}
				
				$uinfodata['ui_logoimage'] = $limage;	
			}
			
			if(!empty($_FILES['cover_image']['name'])){
					
				if(is_uploaded_file($_FILES['cover_image']['tmp_name'])) {
					
					$sourcePath = $_FILES['cover_image']['tmp_name'];
					$temp = explode(".", $_FILES["cover_image"]["name"]);
					$newfilename = round(microtime(true)) . '.' . end($temp);
					$targetPath = "uploads/coverimages/".$newfilename;
					$outimg = '';
					move_uploaded_file($sourcePath,$targetPath);
					$cimage = $newfilename;
				}
				
				$uinfodata['ui_coverimage'] = $cimage;
			}
			
			$udata['um_name'] = $fullname;
			$udata['um_user_id'] = $userId;
			$udata['um_link'] = $micro_url;
			$udata['um_mobile'] = $contact_number;
			$udata['um_status'] = 'ACCEPTED';
			$udata['updated_at'] = date('Y-m-d h:i:s');
			
			$um_id = $this->input->post('um_id');
			
			if($this->academic_model->updateUniversity($udata,$um_id)){
				
				$uinfodata['ui_um_id'] = $um_id;
				$uinfodata['ui_aboutus'] = $about_university;
				$uinfodata['ui_recognition'] = $affiliation;
				$uinfodata['ui_address'] = $address;
				$uinfodata['country_id'] = $country;
				$uinfodata['state_id'] = $state;
				$uinfodata['city_id'] = $city;
				$uinfodata['ui_contact'] = $contact_number;
				$uinfodata['updated_at'] = date('Y-m-d h:i:s');
				
				$ui_id = $this->input->post('ui_id');
				
				if($this->academic_model->updateUniversityInfo($uinfodata,$ui_id)){
					echo 'University Update successfully';
				}
			}
		}
		
	}
	
	// function addFacilityInfo(){
		
	// 	$userdata = $this->session->userdata('logged_in');
	// 	$universedata = $this->academic_model->getUniversityByUser($userdata['id']);
		
	// 	$facilities = explode(',',$this->input->post('facilities'));
		
	// 	$facilities = array_unique($facilities);
		
	// 	foreach($facilities as $facility){
			
	// 	 $facData['ufc_um_id'] = $universedata->um_id;
	// 	 $facData['ufc_name'] = $facility;
	// 	 $facData['created_at'] = date('Y-m-d h:i:s');
	// 	 $facData['updated_at'] = date('Y-m-d h:i:s');
		 
	// 	 $this->academic_model->addFacility($facData);
		 
	// 	}
		
	// 	echo 'Facilities added successfully';
				
	// }

	function addFacilityInfo(){
		
		$userdata = $this->session->userdata('logged_in');
		$universedata = $this->academic_model->getUniversityByUser($userdata['id']);
		

		$facilities = explode(',',$this->input->post('facilities'));
		//print_r($facilities);
		$facilities = array_unique($facilities);
		
		foreach($facilities as $facility){
		$check=$this->academic_model->checkFacility($facility,$universedata->um_id);
		if($check){
			//echo 'tes';
			$id= $universedata->um_id;
			 $facData['ufc_name'] = $facility;
			 $facData['created_at'] = date('Y-m-d h:i:s');
			 $facData['updated_at'] = date('Y-m-d h:i:s');
			 
			 $this->academic_model->updateFacility($id,$facData);
			}else{
			$facData['ufc_um_id'] = $universedata->um_id;
			 $facData['ufc_name'] = $facility;
			 $facData['created_at'] = date('Y-m-d h:i:s');
			 $facData['updated_at'] = date('Y-m-d h:i:s');
			 
			 $this->academic_model->addFacility($facData);
			}
			 
		 
		}
		
		echo 'Facilities added successfully';
				
	}
}
