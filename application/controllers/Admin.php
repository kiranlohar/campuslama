<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->model('admin_model');
		$this->load->model('academic_model');
		$this->load->model('login_model');
		$this->load->dbforge();
	}
	
	public function dashboard(){
		
		$userdata = $this->session->userdata('logged_in');
		$data['front'] = $this->admin_model->front_menu();
		$data['subview'] = 'admin/dashboard';
		$this->load->view('layout_admin_main',$data);
	}
	public function user_setting()
	{
		$userdata = $this->session->userdata('logged_in');
		$data['front'] = $this->admin_model->front_menu();
		$data['subview'] = 'academic/user_setting';
		$this->load->view('layout_admin_main',$data);
	}
	public function course_list(){
		
		$userdata = $this->session->userdata('logged_in');
		$data['progrmManager'] = $this->admin_model->getProgramsList();
		$data['university'] = $this->admin_model->get_university();
		$data['front'] = $this->admin_model->front_menu();
		$data['subview'] = 'admin/course_list';
		$this->load->view('layout_admin_main',$data);
	}
	
	public function program_list(){
		
		$userdata = $this->session->userdata('logged_in');
		$data['progrmManager'] = $this->admin_model->getProgramsList();
		echo json_encode($data['progrmManager']);
		
	}
	public function get_program_list(){
		
		$userdata = $this->session->userdata('logged_in');
		$id=$this->input->post('uid');
		$data['progrmManager'] = $this->admin_model->getProgramsList_by_university($id);
		echo json_encode($data['progrmManager']);
		
	}
	

	public function university(){
		
		$userdata = $this->session->userdata('logged_in');
		$data['academic'] = $this->admin_model->getAcademicUser();
		$data['front'] = $this->admin_model->front_menu();
		$data['subview'] = 'admin/manage_university';
		$this->load->view('layout_admin_main',$data);
	}
	
	public function academin_user_list(){
		
		$userdata = $this->session->userdata('logged_in');
		$data['academic'] = $this->admin_model->getAcademicUser();
		echo json_encode($data['academic']);
		
	}
	public function packages(){
		
		$userdata = $this->session->userdata('logged_in');
		
		if($this->input->post()){
			
			$packdata['pack_um_id'] = $this->input->post('university');;
			$packdata['pack_name'] = $this->input->post('package_name');
			$packdata['pack_amount'] = $this->input->post('package_amount');
			$packdata['pack_valid'] = $this->input->post('valid').' '.$this->input->post('valid-unit');
			$packdata['num_list'] = $this->input->post('num_listout');
			$packdata['created_at'] = date('Y-m-d h:i:s');
			$packdata['updated_at'] = date('Y-m-d h:i:s');
			
			$pack_id = $this->admin_model->insertPackage($packdata);
			
			if($pack_id){
				
				$this->session->set_flashdata('success','Package Added Successfully');
				redirect(base_url('admin/packages'));
			}
		}
		$data['packageData'] = $this->admin_model->getPackageList();
		$data['uniData'] = $this->admin_model->getUniversities();
		$data['subview'] = 'admin/packages';
		$this->load->view('layout_admin_main',$data);
	}
	 
	public function menu()
	{
		$data['mData'] = $this->admin_model->getMasterMenu();
		$data['front'] = $this->admin_model->front_menu();
		
		$data['subview'] = 'admin/menu';
		$this->load->view('layout_admin_main',$data);
	}
	
	
	## Manage master page
	public function master_program($id=null)
	{
		
		//echo $id;
		$data['mData'] = $this->admin_model->getMasterMenu();
		$data['front'] = $this->admin_model->front_menu();
		$data['menu_data'] = $this->admin_model->get_child_menu_detail($id);
		//$data['mp_data'] = $this->admin_model->get_pro($id);
		$ta=$this->admin_model->get_table_child($id);
		$table=$ta[0]->table_name;
		$data['mp_data'] = $this->admin_model->get_page_detail($table);
		$data['program'] = $this->admin_model->get_program();
		$data['program'] = $this->admin_model->get_program();
		$data['eligibility']=$this->academic_model->getEligiblity();
		$data['course_spe']=$this->academic_model->getCourseSpe();
		//$d = $this->admin_model->get_page_detail($table);
		//echo json_encode($d);
		//echo '<pre>',print_r($data['mp_data']);
		$data['subview'] = 'admin/master_page';
		$this->load->view('layout_admin_main',$data);
	}

	public function master_program_test($id=null)
	{
		
		//echo $id;
		//$data['mData'] = $this->admin_model->getMasterMenu();
		//$data['front'] = $this->admin_model->front_menu();
		//$data['menu_data'] = $this->admin_model->get_child_menu_detail($id);
		//$data['mp_data'] = $this->admin_model->get_pro($id);
		$arr='';
		$ta=$this->admin_model->get_table_child($id);
		$table=$ta[0]->table_name;
		$data['mp_data'] = $this->admin_model->get_page_detail($table);
		$d = $this->admin_model->get_page_detail($table);
		// //array_push($arr, $d);
		// foreach ($d as $stream) {
		// 	//$sn= $this->admin_model->get_program_stream_name($stream->program_id);
			
		// 	$sn= $this->admin_model->get_program_stream_name($stream->program_id);
		// 	//print_r($sn[0]->name);
		// 		//array_push($arr, $sn[0]->name);
		// //	print_r($stream);
		// 		$arr=$sn[0]->st;
				
		// }

	echo json_encode($d);
		

	}
	
	public function submenu_1()
	{
		//$data['subview'] = 'admin/master_page';
		$data['front'] = $this->admin_model->front_menu();
		$this->load->view('components/menu_sub',$data);
	}
	 public function add_program($id=null,$method=null)
	 {
				$result='';
		   $data=$this->input->post('name'); 
		   $childid=$this->input->post('childid'); 
		  // eligibi
		   $eligibi=$this->input->post('eligibi'); 
		    $course_id=$this->input->post('course_id'); 
		    //print_r($course_id);die();
		   if($eligibi!=''){
		   	for($i=0;$i<count($data);$i++)
			{
			  $mpdata=array(
				'name'=>$this->input->post('name')[$i],
				'eligi_cate_id'=>$this->input->post('eligibi'),
				'updated_at	'=>date('Y-m-d h:i:s')
		  );
			
		 $ta=$this->admin_model->get_table_child($childid);
		 $table=$ta[0]->table_name;
	     /* if($table)
		 { */
	 $check=$this->admin_model->user_exists_master($this->input->post('name')[$i],$table);
			if($check==false){
			//array_push($msg,$this->input->post('name')[$i],'name already exist');
			echo json_encode('Name Already exist');
			//die;
			//$msg= 'Name Already exist';
			}
			else{
				//echo json_encode($msg);
				$result.=$this->db->insert($table,$mpdata);
			}
			  
			  }
		   }else if($course_id!=''){
		   		for($i=0;$i<count($data);$i++)
			{
			  $mpdata=array(
				'name'=>$this->input->post('name')[$i],
				'course_id'=>$this->input->post('course_id'),
				'updated_at	'=>date('Y-m-d h:i:s')
		  );
			
		 $ta=$this->admin_model->get_table_child($childid);
		 $table=$ta[0]->table_name;
	     /* if($table)
		 { */
	 $check=$this->admin_model->user_exists_master($this->input->post('name')[$i],$table);
			if($check==false){
			//array_push($msg,$this->input->post('name')[$i],'name already exist');
			echo json_encode('Name Already exist');
			//die;
			//$msg= 'Name Already exist';
			}
			else{
				//echo json_encode($msg);
				$result.=$this->db->insert($table,$mpdata);
			}
			  
			  }
		   }else{
		   	for($i=0;$i<count($data);$i++)
			{
			  $mpdata=array(
				'name'=>$this->input->post('name')[$i],
				'updated_at	'=>date('Y-m-d h:i:s')
		  );
			
		 $ta=$this->admin_model->get_table_child($childid);
		 $table=$ta[0]->table_name;
	     /* if($table)
		 { */
	 		$check=$this->admin_model->user_exists_master($this->input->post('name')[$i],$table);
			if($check==false){
			//array_push($msg,$this->input->post('name')[$i],'name already exist');
			echo json_encode('Name Already exist');
			//die;
			//$msg= 'Name Already exist';
			}
			else{
				//echo json_encode($msg);
				$result.=$this->db->insert($table,$mpdata);
			}
			  
			  }
		   }
		  

			  if($result)
			  {
				  	echo json_encode('Added Successfully');
				 //return true;
				 //  redirect('admin/master_program/'. $childid);
			  }
		// }
	 
	 
	 ## old
		 
	 }
	 
	 public function add_program_spe($id=null,$method=null)
	 {
		//print_r($this->input->post('program'));die;
				$result='';
		   //$data=$this->input->post('name'); 
		   $data=$this->input->post('name'); 
		   $childid=$this->input->post('childid'); 
		  for($i=0;$i<count($data);$i++)
			{
			  $mpdata=array(
				'name'=>$this->input->post('name')[$i],
				'program_id'=>$this->input->post('program'),
				'updated_at	'=>date('Y-m-d h:i:s')
		  );
			// print_r( $mpdata);die;
		 $ta=$this->admin_model->get_table_child($childid);
		 $table=$ta[0]->table_name;
	     /* if($table)
		 { */
	 $check=$this->admin_model->user_exists_master($this->input->post('name')[$i],$table);
			if($check==false){
			//array_push($msg,$this->input->post('name')[$i],'name already exist');
			echo json_encode('Name Already exist');
			//die;
			//$msg= 'Name Already exist';
			}
			else{
				//echo json_encode($msg);
				$result.=$this->db->insert($table,$mpdata);
			}
			  
			  }
			  if($result)
			  {
				  	echo json_encode('Added Successfully');
				 //return true;
				 //  redirect('admin/master_program/'. $childid);
			  }
		// }
	 
	 
	 ## old
		 
	 }
	 
	 public function edit_program($method=null,$id=null)
	 {
		
		  if($_SERVER['REQUEST_METHOD'] == 'POST' && $method=='edit_master_page') 
				{
					$idm= $this->input->post('id');
					$child= $this->input->post('child_id');
					$ta=$this->admin_model->get_table_child($child);
					$table=$ta[0]->table_name;
					
					$m_id = $this->admin_model->edit_master($idm,$table);
					
					echo json_encode($m_id);
					
				}
				if($_SERVER['REQUEST_METHOD'] == 'POST' && $method=='update_master_page') 
				{
					$id= $this->input->post('id');
					$name=$this->input->post('name'); 
				    $eligibi=$this->input->post('eligibi'); 
				    $eligibi_sp=$this->input->post('eligibi_sp'); 
				    $childid=$this->input->post('childid'); 
				    $course_id=$this->input->post('course_id'); 
				    if($eligibi!=''){
				    	 $mpdata=array(
						'name'=>$name,
						'eligi_cate_id'=>$eligibi,
						'updated_at	'=>date('Y-m-d h:i:s')
				  );
				    }else if($eligibi_sp!=''){
				    	$mpdata=array(
						'name'=>$name,
						'program_id'=>$eligibi_sp,
						'updated_at	'=>date('Y-m-d h:i:s')
				  );
				    }else if($course_id!=''){
				    	$mpdata=array(
						'name'=>$name,
						'course_id'=>$course_id,
						'updated_at	'=>date('Y-m-d h:i:s')
				  );
				    }
				    else{
				    	 $mpdata=array(
						'name'=>$name,
						'updated_at	'=>date('Y-m-d h:i:s')
				  );
				    }

				    	
				 
				 $ta=$this->admin_model->get_table_child($childid);
				 $table=$ta[0]->table_name;
				 if($table)
				 {
				// $check=$this->admin_model->user_exists_master($name,$table);
				//if($check==false){
					//array_push($msg,$this->input->post('name')[$i],'name already exist');
				//	echo '200';
					//die;
					//$msg= 'Name Already exist';
					//}
					////else
					//{
						$result=$this->admin_model->update_master_p($id,$mpdata,$table);
					//}
					  
					  if($result)
					  {
						  echo '400';
						  // redirect('admin/master_program/'. $childid);
					  }
				// }
				 }
					
				}
			if($_SERVER['REQUEST_METHOD'] == 'POST' && $method=='del_master_page') 
				{
					$id= $this->input->post('id');
					
				    $childid=$this->input->post('table'); 
				  $mpdata=array(
						'status'=>0,
						'updated_at	'=>date('Y-m-d h:i:s')
				  );
				 $ta=$this->admin_model->get_table_child($childid);
				 $table=$ta[0]->table_name;
				 if($table)
				 {
					  $result=$this->admin_model->update_master_d($id,$mpdata,$table);
					  if($result)
					  {
						  echo 'ok';
						  // redirect('admin/master_program/'. $childid);
					  }
				 }
					
				}
				if($_SERVER['REQUEST_METHOD'] == 'POST' && $method=='active_master_page') 
				{
					$id= $this->input->post('id');
					
				    $childid=$this->input->post('table'); 
				  $mpdata=array(
						'status'=>1,
						'updated_at	'=>date('Y-m-d h:i:s')
				  );
				 $ta=$this->admin_model->get_table_child($childid);
				 $table=$ta[0]->table_name;
				 if($table)
				 {
					  $result=$this->admin_model->update_master_d($id,$mpdata,$table);
					  if($result)
					  {
						  $this->session->set_flashdata('success','Approved');
						  //echo 'ok';
						  // redirect('admin/master_program/'. $childid);
					  }
				 }
					
				}
	 }
	## end Master 
	public function add_menu($method=null)
	{
		if($_SERVER['REQUEST_METHOD'] == 'POST' && $method=='add_master') 
		{
			
			$mdata['menu_name'] = $this->input->post('name');
			$mdata['updated_date'] = date('Y-m-d h:i:s');
			
			$m_id = $this->admin_model->insertMenu($mdata);
			if($m_id)
			{
				echo 'Menu added successfully';
			}
			else
			{
				echo 'error';
			}
			
		}
		if($_SERVER['REQUEST_METHOD'] == 'POST' && $method=='del_master') 
		{
			$id= $this->input->post('id');
			$mdata['status'] = 0;
			$mdata['updated_date'] = date('Y-m-d h:i:s');
			
			$m_id = $this->admin_model->updateMenu($id,$mdata);
			if($m_id)
			{
				echo 'Menu Updated successfully';
			}
			else
			{
				echo 'error';
			}
			
		}
		if($_SERVER['REQUEST_METHOD'] == 'POST' && $method=='edit_master') 
		{
			$id= $this->input->post('id');
			
			
			$m_id = $this->admin_model->editMenu($id);
			
			echo json_encode($m_id);
			
		}
		if($_SERVER['REQUEST_METHOD'] == 'POST' && $method=='update_master') 
		{
			$id= $this->input->post('id');
			$mdata['menu_name'] = $this->input->post('name');
			$mdata['updated_date'] = date('Y-m-d h:i:s');
			
			$m_id = $this->admin_model->updateMenu($id,$mdata);
			if($m_id)
			{
				echo 'Menu Updated successfully';
			}
			else
			{
				echo 'error';
			}
			
		}
		
	## sub menu
		if($_SERVER['REQUEST_METHOD'] == 'POST' && $method=='add_sub_master') 
		{
			$sm_id=null;
	
			//$msg=array();
			$data=$this->input->post('name');
		
			for($i=0;$i<count($data);$i++)
			{	
			
			$mdata['menu_id'] = $this->input->post('validunit');
			$mdata['name'] = $this->input->post('name')[$i];
			$mdata['updated_at'] = date('Y-m-d h:i:s');
			$check=$this->admin_model->user_exists( $this->input->post('name')[$i]);
			if($check==false){
			//array_push($msg,$this->input->post('name')[$i],'name already exist');
			echo json_encode('Name Already exist');
			//die;
			//$msg= 'Name Already exist';
			}
			else{
				//echo json_encode($msg);
				$sm_id .= $this->admin_model->insertMenu_sub($mdata);
			}
			
			}
			if($sm_id)
			{
				$val='Added Successfully';
				echo json_encode($val);
			
			}
			 else
			{
				//$msg1=implode(',',$msg);
				//$msg1='Error';
				//echo json_encode($msg1);
				
			} 
			
		}
		
		if($_SERVER['REQUEST_METHOD'] == 'POST' && $method=='del_sub_master') 
		{
			$id= $this->input->post('id');
			$mdata['status'] = 0;
			$mdata['updated_at'] = date('Y-m-d h:i:s');
			
			$m_id = $this->admin_model->update_sub_Menu($id,$mdata);
			if($m_id)
			{
				echo 'Menu Updated successfully';
			}
			else
			{
				echo 'error';
			}
			
		}
		
		if($_SERVER['REQUEST_METHOD'] == 'POST' && $method=='active_sub_master') 
		{
			$id= $this->input->post('id');
			$mdata['status'] = 1;
			$mdata['updated_at'] = date('Y-m-d h:i:s');
			
			$m_id = $this->admin_model->update_sub_Menu($id,$mdata);
			if($m_id)
			{
				echo 'Activ Updated successfully';
			}
			else
			{
				echo 'error';
			}
			
		}
		
		if($_SERVER['REQUEST_METHOD'] == 'POST' && $method=='edit_sub_master') 
		{
			$id= $this->input->post('id');
			
			
			$m_id = $this->admin_model->edit_Sub_Menu($id);
			
			echo json_encode($m_id);
			
		}
		
		if($_SERVER['REQUEST_METHOD'] == 'POST' && $method=='update_sub_master') 
		{
			$id= $this->input->post('id');
			$mdata['name'] = $this->input->post('name');
			$mdata['updated_at'] = date('Y-m-d h:i:s');
			//prin
			$check=$this->admin_model->user_exists($this->input->post('name'));
			if($check==false){
				echo 'Already exist';
			}
			else
			{
			$m_id = $this->admin_model->update_Sub_Menu($id,$mdata);
			if($m_id)
			{
				echo 'Menu Updated successfully';
			}
			else
			{
				echo 'error';
			}
			}
		}
		
		## child sub
		
		if($_SERVER['REQUEST_METHOD'] == 'POST' && $method=='add_child_sub_master') 
		{
			
			$sm_id=null;
	
			//$msg_ch=array();
			$data=$this->input->post('name');
			//print_r($data);die;
			for($i=0;$i<count($data);$i++)
			{
			$mdata['menu_id'] = $this->input->post('validunit');
			$mdata['sub_menu_id'] = $this->input->post('submenuid');
			$mdata['child_sub_menu_name'] = $this->input->post('name')[$i];
			$mdata['updated_at'] = date('Y-m-d h:i:s');
			
			$table='master_'.$this->input->post('name')[$i];
			//if ($this->dbforge->create_database('hallo_addmission')) {
     $fields = array(
         'id' => array(
              'type' => 'INT',
              'constraint' => 5,
              'unsigned' => TRUE,
              'auto_increment' => TRUE
          ),
          'name'=> array(
              'type' => 'VARCHAR',
              'constraint' => '100',
          ),
          'status' => array(
              'type' => 'INT',
              'constraint' => '11',
              'default' => 1,
          ),
          'updated_at' => array(
              'type' => 'VARCHAR',
              'constraint' => '100'
          ),
     );
     $this->dbforge->add_field($fields);
     $this->dbforge->add_key('id', TRUE);
     $this->dbforge->create_table($table, TRUE);
	 
	 $table1='master_'.$this->input->post('name')[$i];

			
			
			$check=$this->admin_model->user_exists_ch( $this->input->post('name')[$i]);
			if($check==false){
			//array_push($msg_ch,$this->input->post('name')[$i],'name already exist');
			//$msg= 'Name Already exist';
			echo json_encode('Name Already exist');
			}
			else{
				
				//$sm_id .= $this->admin_model->insertMenu_sub($mdata);
			$sm_id .= $this->admin_model->insertMenu_child_sub($mdata);
			$tdata=array('child_id'=>$sm_id,'table_name'=>$table1);
			$this->db->insert('master_child_table',$tdata);
			}
			}
			
			if($sm_id)
			{
				$val='Added Successfully';
				echo json_encode($val);
			}
			else
			{
				/* $msg1=implode(',',$msg_ch);
				  $this->session->set_flashdata('error',$msg1);
				  redirect('admin/child_subMenu/'.$this->input->post('valid-unit')); */
			}
			
		}
		
		if($_SERVER['REQUEST_METHOD'] == 'POST' && $method=='del_child_sub_master') 
		{
			echo $id= $this->input->post('id');
			$mdata['status'] = 0;
			$mdata['updated_at'] = date('Y-m-d h:i:s');
			
			$m_id = $this->admin_model->update_child_sub_Menu_del($id,$mdata);
			if($m_id)
			{
				echo 'Menu Updated successfully';
			}
			else
			{
				echo 'error';
			}
			
		}
		
		if($_SERVER['REQUEST_METHOD'] == 'POST' && $method=='activ_child_sub_master') 
		{
			$id= $this->input->post('id');
			$mdata['status'] = 1;
			$mdata['updated_at'] = date('Y-m-d h:i:s');
			
			$m_id = $this->admin_model->update_child_sub_Menu($id,$mdata);
			if($m_id)
			{
				echo 'Menu Updated successfully';
			}
			else
			{
				echo 'error';
			}
			
		}
		
		
		if($_SERVER['REQUEST_METHOD'] == 'POST' && $method=='edit_child_sub_master') 
		{
			$id= $this->input->post('id');
			
			
			$m_id = $this->admin_model->edit_child_Sub_Menu($id);
			
			echo json_encode($m_id);
			
		}
		
		if($_SERVER['REQUEST_METHOD'] == 'POST' && $method=='update_child_sub_master') 
		{
			 $id= $this->input->post('id');
			$mdata['child_sub_menu_name'] = $this->input->post('name');
			$mdata['updated_at'] = date('Y-m-d h:i:s');
			
			
			$check=$this->admin_model->user_exists_ch($this->input->post('name'));
			if($check==false){
				echo 'Already exist';
			}
			else
			{
			//$q=$this->admin_model->get_table_child($id);
			//$table_old=$q[0]->table_name;
			//die;
			//$table_nw='master_'.$this->input->post('name');
			
			//$this->dbforge->rename_table($table_old, $table_nw);
			
			//$this->admin_model->update_child_master($id,$table_nw);
			
			$m_id = $this->admin_model->update_child_sub_Menu($id,$mdata);
			if($m_id)
			{
				echo 'Menu Updated successfully';
			}
			else
			{
				echo 'error';
			}
			}
		}
		
	}
	
	public function subMenu($id=null)
	{
		//echo $id;
		$this->session->set_userdata('menu_id',$id);
		$data['mData_drop'] = $this->admin_model->getMasterMenu_drop($id);
	    $data['mData'] = $this->admin_model->getMasterSub_Menu($id);
	    $data['front'] = $this->admin_model->front_menu();
		//print_r($data['mData']);die;
		$data['subview'] = 'admin/sub_menu';
		$this->load->view('layout_admin_main',$data);
	}
	
	public function sub_data($id=null)
	{
		 $data= $this->admin_model->getMasterSub_Menu(1);
		 echo json_encode($data);
	}
	
	## End 
	## Start child sub menu
	public function child_subMenu($id=null)
	{
		$mid=$this->session->userdata('menu_id');
		$data['mData_drop'] = $this->admin_model->getMasterMenu_drop($mid);
		$data['mData_sub_drop'] = $this->admin_model->getMaster_sub_Menu_drop($id);
	    $data['mData'] = $this->admin_model->getMaster_child_Sub_Menu($id);
	 
	    $data['front'] = $this->admin_model->front_menu();
		//print_r($data['mData']);die;
		$data['subview'] = 'admin/child_sub_menu';
		$this->load->view('layout_admin_main',$data);
		   
		
	}
	
	public function get_child_data($id=null)
	{
		$d = $this->admin_model->getMaster_child_Sub_Menu($id);
		echo json_encode($d);
	}
	
	## empty
	public function mark_approval(){
		
		$uc_id = $this->input->post('id');
		
		$ucdata['uc_is_approved'] = 1;
		
		if($this->admin_model->updateCourseList($ucdata,$uc_id)){
			$this->session->set_flashdata('success','Course Approved for Listing');
			echo json_encode('Course Approved for Listing');
			//redirect(base_url('admin/course_list'));
		}
		
	}
	
	public function mark_reject(){
		
		$uc_id =  $this->input->post('id');
		
		$ucdata['uc_is_approved'] = 2;
		
		if($this->admin_model->updateCourseList($ucdata,$uc_id)){
			$this->session->set_flashdata('error','Course Rejected for Listing');
			echo json_encode('Course Rejected for Listing');
			//redirect(base_url('admin/course_list'));
		}
		
	}
public function academic_active(){
		
		$id =  $this->input->post('id');
		
		$ucdata['IsActive'] = 1;
		
		if($this->db->where('id',$id)->update('users',$ucdata)){
			$this->session->set_flashdata('success','Activeted.....!');
			echo json_encode('Activeted.....!');
			//redirect(base_url('admin/course_list'));
		}
		
	}

public function academic_inactive(){
		
		$id =  $this->input->post('id');
		$status =  $this->input->post('status');
		$ucdata['IsActive'] = $status;
		
		if($this->db->where('id',$id)->update('users',$ucdata)){
			$this->session->set_flashdata('info','Activeted.....!');
			echo json_encode('Activeted.....!');
			//redirect(base_url('admin/course_list'));
		}
		
	}
	public function academic_user_reg(){
		
	
				$data['name'] = $this->input->post('name');
				$data['email'] = $this->input->post('email');
				
				$password = hash('sha256', $this->input->post('password'));
				
				$data['password'] = $password;
				$data['role'] = 1;
				$data['randomkey'] = $this->generateRandomString(100);
				$data['email_confirmed']='YES';
				$data['status']=1;
				$data['created_at'] = date('Y-m-d h:i:s');
				$data['updated_at'] = date('Y-m-d h:i:s');
				
					

				// print_r($data);
				 $userId = $this->login_model->insertUser($data);
				//$userId='tes';
				if($userId){
						echo json_encode( array('msg' =>'Successfull'));
					}else
					{
						echo json_encode( array('msg' =>'No'));
					}
}

public function academic_user_edit(){
	$id=$this->input->post('id');
	$data=$this->admin_model->getAcademicUser_edit($id);
	echo json_encode($data);
}
public function academic_user_update(){
		
	$id=$this->input->post('id');
				$data['name'] = $this->input->post('name');
				
				$data['created_at'] = date('Y-m-d h:i:s');
				$data['updated_at'] = date('Y-m-d h:i:s');
				
					

				// print_r($data);
				 $userId = $this->admin_model->getAcademicUser_update($id,$data);
				//$userId='tes';
				if($userId){
						echo json_encode( array('msg' =>'Successfull'));
					}else
					{
						echo json_encode( array('msg' =>'No'));
					}
}
public function check_email(){
	$email=$this->input->post('email');
	if($this->login_model->userPresent($email))
	{
		echo json_encode( array('msg' =>'Exitst'));
	}else
	{
		echo json_encode( array('msg' =>'No'));
	}
}
	function generateRandomString($length = 10) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}

	public function switch_to_academic()
	{
		$this->session->unset_userdata('logged_in');

		 $id=$this->uri->segment(3);
		$this->session->set_userdata('admin_switch',$this->uri->segment(4));
		
		$query=$this->db->where('id',$id)->get('users');
		$data=$query->result();
		//print_r($data[0]->email);die();
		$email=$data[0]->email;
		$password=$data[0]->password;
		$loginUserPresent = $this->login_model->LoginPresent_admin($email,$password);
			
			if($loginUserPresent){
				
				$sess_array = array(
				 'id' => $loginUserPresent->id,
				 'name' => $loginUserPresent->name,
				 'email' => $loginUserPresent->email,
				 'role' => $loginUserPresent->role,
				 'status' => $loginUserPresent->status,
			   );
			   //print_r($sess_array);
				$this->session->set_userdata('logged_in', $sess_array);
				
				
				$this->session->set_flashdata('success','Welcome '.$sess_array['name'].'..!');
				
				if($sess_array['role'] == 1){
					
					redirect(base_url('dashboard/au'));
					
				}else if($sess_array['role'] == 2){
				
					redirect(base_url('dashboard/student'));
					
				}else if($sess_array['role'] == 4){
				
					redirect(base_url('admin/dashboard'));
				}
				
				
			}
	}
	public function switch_to_admin()
	{
		$this->session->unset_userdata('admin_switch');
		$id=$this->uri->segment(3);
		
		$query=$this->db->where('id',$id)->get('users');
		$data=$query->result();
		//print_r($data[0]->email);
		$email=$data[0]->email;
		$password=$data[0]->password;
		$loginUserPresent = $this->login_model->LoginPresent_admin($email,$password);
			
			if($loginUserPresent){
				
				$sess_array = array(
				 'id' => $loginUserPresent->id,
				 'name' => $loginUserPresent->name,
				 'email' => $loginUserPresent->email,
				 'role' => $loginUserPresent->role,
				 'status' => $loginUserPresent->status,
			   );
			   //print_r($sess_array);
				$this->session->set_userdata('logged_in', $sess_array);
				
				
				$this->session->set_flashdata('success','Welcome '.$sess_array['name'].'..!');
				
				if($sess_array['role'] == 1){
					
					redirect(base_url('dashboard/au'));
					
				}else if($sess_array['role'] == 2){
				
					redirect(base_url('dashboard/student'));
					
				}else if($sess_array['role'] == 4){
				
					redirect(base_url('admin/university'));
				}
				
				
			}
	}
}
