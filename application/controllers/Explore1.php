<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Explore extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->model('explore_model');
		$this->load->model('login_model');
	}
	public function index()
	{
		//print_r($this->input->post());
		if($this->input->post()){
			
			$usrname = $this->input->post('1');
			$usremail = $this->input->post('4');
			$usrcontct = $this->input->post('8');
			$usrpassword = $this->input->post('password');
			$rep_password = $this->input->post('rep_password');
			$postData = $this->input->post();
			$udata['name'] = $usrname;
			$udata['email'] = $usremail;
			
			$password = hash('sha256', $rep_password);
			
			$udata['password'] = $password;
			$udata['role'] = 2;
			$udata['randomkey'] = $this->generateRandomString(100);
			$udata['created_at'] = date('Y-m-d h:i:s');
			$udata['updated_at'] = date('Y-m-d h:i:s');
			
			$userId = $this->login_model->insertUser($udata);
			
			if($userId){
				
				foreach($postData as $kfid=>$frmval){
					
					if(is_numeric($kfid)){
						
						$stformdata['ufd_stm_id'] = $userId;
						$stformdata['ufd_fm_id'] = $kfid;
						$stformdata['ufd_value'] = $frmval;
						$stformdata['created_at'] = date('Y-m-d h:i:s');
						$stformdata['updated_at'] = date('Y-m-d h:i:s');
						
						$this->explore_model->insertStudentField($stformdata);
							
					}
				}
				$smtdata['stm_user_id'] = $userId;
					$smtdata['created_at'] = date('Y-m-d h:i:s');
					$smtdata['updated_at'] = date('Y-m-d h:i:s');
					
					$sm_id = $this->login_model->insertStudentMstr($smtdata);
				$this->session->set_flashdata('success', 'You are successfully registered');
				redirect(base_url('login/index'));
			}
			
		}else{
			$this->load->view('components/front_header_1');
			
			$data['course_master'] = $this->explore_model->getCourseMaster();
			$this->load->view('explore/index',$data);
			//$this->load->view('components/front_footer_1');
		}
	}
	
	public function explore()
	{
		//print_r($this->input->post());
		if($this->input->post()){
			
			$usrname = $this->input->post('1');
			$usremail = $this->input->post('4');
			$usrcontct = $this->input->post('8');
			$usrpassword = $this->input->post('password');
			$rep_password = $this->input->post('rep_password');
			$postData = $this->input->post();
			$udata['name'] = $usrname;
			$udata['email'] = $usremail;
			
			$password = hash('sha256', $rep_password);
			
			$udata['password'] = $password;
			$udata['role'] = 2;
			$udata['randomkey'] = $this->generateRandomString(100);
			$udata['created_at'] = date('Y-m-d h:i:s');
			$udata['updated_at'] = date('Y-m-d h:i:s');
			
			$userId = $this->login_model->insertUser($udata);
			
			if($userId){
				
				foreach($postData as $kfid=>$frmval){
					
					if(is_numeric($kfid)){
						
						$stformdata['ufd_stm_id'] = $userId;
						$stformdata['ufd_fm_id'] = $kfid;
						$stformdata['ufd_value'] = $frmval;
						$stformdata['created_at'] = date('Y-m-d h:i:s');
						$stformdata['updated_at'] = date('Y-m-d h:i:s');
						
						$this->explore_model->insertStudentField($stformdata);
							
					}
				}
				$smtdata['stm_user_id'] = $userId;
					$smtdata['created_at'] = date('Y-m-d h:i:s');
					$smtdata['updated_at'] = date('Y-m-d h:i:s');
					
					$sm_id = $this->login_model->insertStudentMstr($smtdata);
				$this->session->set_flashdata('success', 'You are successfully registered');
				redirect(base_url('login/index'));
			}
			
		}else{
			$this->load->view('components/front_header_1');
			
			$data['course_master'] = $this->explore_model->getCourseMaster();
			$this->load->view('explore/home',$data);
			//$this->load->view('components/front_footer_1');
		}
	}
	
	function generateRandomString($length = 10) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}
}
