<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		//$this->load->library('session');
		$this->load->model('login_model');
		$this->load->model('student_model');
		$this->load->library('form_validation');	
		$this->load->library('email');	
		$this->load->library('facebook');
		$this->load->library('google');
			
	}

	

	public function index()
	{
		$userData = array();
		
		// Check if user is logged in
		if($this->facebook->is_authenticated()){
			// Get user facebook profile details
			$userProfile = $this->facebook->request('post', '/me?fields=id,first_name,last_name,email,gender,locale,picture');
            $pass='123456';
            $userData['oauth_provider'] = 'facebook';
            $userData['oauth_uid'] = $userProfile['id'];
            $userData['name'] = $userProfile['first_name'];
            $userData['password'] = hash('sha256', $pass);
            $userData['email'] = $userProfile['email'];
           	$userData['role'] = 2;
            $userID = $this->login_model->checkUser($userData);
            if(!empty($userID)){
            $get_userdata=$this->login_model->get_fb_user($userID);
               $smtdata['stm_user_id'] = $userID;
					$smtdata['created_at'] = date('Y-m-d h:i:s');
					$smtdata['updated_at'] = date('Y-m-d h:i:s');
					
					$sm_id = $this->login_model->insertStudentMstr($smtdata);
                $loginUserPresent = $this->login_model->Login_fb($get_userdata[0]->email,$get_userdata[0]->oauth_uid);
			
			if($loginUserPresent){
				
				$sess_array = array(
				 'id' => $loginUserPresent->id,
				 'name' => $loginUserPresent->name,
				 'email' => $loginUserPresent->email,
				 'role' => $loginUserPresent->role,
				 'status' => $loginUserPresent->status,
			   );
			   //print_r($sess_array);
				$this->session->set_userdata('logged_in', $sess_array);
				$this->session->set_flashdata('success','Welcome '.$sess_array['name'].'..!');
				if($sess_array['role'] == 1){
					redirect(base_url('dashboard/au'));
					
				}else if($sess_array['role'] == 2){
				
					redirect(base_url('home'));
					
				}else if($sess_array['role'] == 4){
				
					redirect(base_url('admin/dashboard'));
				}

			}else{
				$this->session->set_flashdata('error', 'Invalid Username and Password');
			}
            } 
			// Get logout URL
			$data['logoutUrl'] = $this->facebook->logout_url();
		}else{
            $fbuser = '';
            $data['authUrl'] =  $this->facebook->login_url();
        }
		$data['loginURL'] = $this->google->loginURL();
		$data['subview'] = 'login/index';
		$this->load->view('layout_login',$data);

		// $userData = array();
		
		// // Check if user is logged in
		// if($this->facebook->is_authenticated()){
		// 	// Get user facebook profile details
		// 	$userProfile = $this->facebook->request('get', '/me?fields=id,first_name,last_name,email,gender,locale,picture');
		
  //           $pass='123456';
  //           $userData['oauth_provider'] = 'facebook';
  //           $userData['oauth_uid'] = $userProfile['id'];
  //           $userData['name'] = $userProfile['first_name'];
  //           $userData['password'] = hash('sha256', $pass);
  //           $userData['email'] = $userProfile['email'];
  //          	$userData['role'] = 2;
  //            $userID = $this->login_model->checkUser($userData);

  //           if(!empty($userID)){
  //           $get_userdata=$this->login_model->get_fb_user($userID);
  //              $smtdata['stm_user_id'] = $userID;
		// 			$smtdata['created_at'] = date('Y-m-d h:i:s');
		// 			$smtdata['updated_at'] = date('Y-m-d h:i:s');
					
		// 			$sm_id = $this->login_model->insertStudentMstr($smtdata);
  //               $loginUserPresent = $this->login_model->Login_fb($get_userdata[0]->email,$get_userdata[0]->oauth_uid);
			
		// 	if($loginUserPresent){
				
		// 		$sess_array = array(
		// 		 'id' => $loginUserPresent->id,
		// 		 'name' => $loginUserPresent->name,
		// 		 'email' => $loginUserPresent->email,
		// 		 'role' => $loginUserPresent->role,
		// 		 'status' => $loginUserPresent->status,
		// 	   );
		// 	   print_r($sess_array);
		// 		$this->session->set_userdata('logged_in', $sess_array);
		// 		$this->session->set_flashdata('success','Welcome '.$sess_array['name'].'..!');
		// 		if($sess_array['role'] == 1){
		// 			redirect(base_url('dashboard/au'));
					
		// 		}else if($sess_array['role'] == 2){
				
		// 			redirect(base_url('home'));
					
		// 		}else if($sess_array['role'] == 4){
				
		// 			redirect(base_url('admin/dashboard'));
		// 		}

		// 	}else{
		// 		$this->session->set_flashdata('error', 'Invalid Username and Password');
		// 	}
  //           } 
		// 	// Get logout URL
		// 	$data['logoutUrl'] = $this->facebook->logout_url();
		// }else{
  //           $fbuser = '';
  //           $data['authUrl'] =  $this->facebook->login_url();
            
  //       }
		// $data['loginURL'] = $this->google->loginURL();
		// $data['subview'] = 'login/index';
		// $this->load->view('layout_login',$data);
	}
	public function singup()
	{
		
		$data['subview'] = 'login/signup';
		$this->load->view('layout_login',$data);
	}
	public function login()
	{
		$this->form_validation->set_rules('email', 'email', 'trim|required|xss_clean');
$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');

if ($this->form_validation->run() == FALSE) {
		//if($this->input->post()){
			
			$email = $this->input->post('email');
			
			$password = hash('sha256', $this->input->post('password'));
			
			$loginUserPresent = $this->login_model->LoginPresent($email,$password);
			
			if($loginUserPresent){
				
				$sess_array = array(
				 'id' => $loginUserPresent->id,
				 'name' => $loginUserPresent->name,
				 'email' => $loginUserPresent->email,
				 'role' => $loginUserPresent->role,
				 'status' => $loginUserPresent->status,
			   );
			   //print_r($sess_array);
				$this->session->set_userdata('logged_in', $sess_array);
				
				
				$this->session->set_flashdata('success','Welcome '.$sess_array['name'].'..!');
				
				if($sess_array['role'] == 1){
					
					redirect(base_url('dashboard/au'));
					
				}else if($sess_array['role'] == 2){
				
					redirect(base_url('dashboard/student'));
					
				}else if($sess_array['role'] == 4){
				
					redirect(base_url('admin/dashboard'));
				}
				
				
			}else{
				
				$this->session->set_flashdata('error', 'Invalid Username and Password');
				redirect(base_url('login/index'));
			}
		}
	}
	public function student_register()
	{
		if($this->input->post()){
			
			$email = $this->input->post('cemail');
			
			$userPresentData = $this->login_model->userPresent($email);
			
			if(empty($userPresentData)){
				
				$data['name'] = $this->input->post('uname');
				$data['email'] = $email;
				
				$password = hash('sha256', $this->input->post('confirm_password'));
				
				$data['password'] = $password;
				$data['role'] = 2;
				$data['IsActive'] = 1;
				$data['randomkey'] = $this->generateRandomString(100);
				$data['created_at'] = date('Y-m-d h:i:s');
				$data['updated_at'] = date('Y-m-d h:i:s');
				
				$userId = $this->login_model->insertUser($data);
				
				if($userId){
					
					$smtdata['stm_user_id'] = $userId;
					$smtdata['created_at'] = date('Y-m-d h:i:s');
					$smtdata['updated_at'] = date('Y-m-d h:i:s');
					
					$sm_id = $this->login_model->insertStudentMstr($smtdata);
					
					if($sm_id){
						
						$username = $this->input->post('uname');
						
						$subject = 'Verify Email';
						
				        $message = 'Dear '.$username.',<br/><br/> Please click on the below activation link to verify your email address.<br/><br/> <a href="'.base_url('login/email_verify/'.md5($email).'/' . base64_encode($sm_id)).'">Click Here</a> <br/><br/><br/> Thanks <br/> Hello Admission Team';
						
						$config['protocol']    = 'smtp';
						$config['smtp_host']    = 'ssl://smtp.gmail.com';
						$config['smtp_port']    = '465';
						$config['smtp_timeout'] = '7';
						$config['smtp_user']    = 'noreply@talentedgenext.com';
						$config['smtp_pass']    = 'Mohammed@1292';
						$config['charset']    = 'utf-8';
						$config['newline']    = "\r\n";
						$config['mailtype'] = 'html';

						$this->email->initialize($config);
						
				        $this->email->from('noreply@talentedgenext.com', 'HELLO ADMISSION');
				        $this->email->to($email);
				        $this->email->subject($subject);
				        $this->email->message($message);
				        $this->email->send();
						
						$this->session->set_flashdata('success', 'You are successfully registered..!');
						//$this->session->set_flashdata('info', 'Please check your email to verify !');
						redirect(base_url('login/index'));
					}
					
				}
			}else{
				$this->session->set_flashdata('error', 'User already present');
				redirect(base_url('login'));
			}
		}
		$data['subview'] = 'login/student_register';
		$this->load->view('layout_login',$data);
	}
	
	public function academic_register()
	{
		if($this->input->post()){
			
			$email = $this->input->post('cemail');
			
			$userPresentData = $this->login_model->userPresent($email);
			
			if(empty($userPresentData)){
				
				$data['name'] = $this->input->post('uname');
				$data['email'] = $email;
				
				$password = hash('sha256', $this->input->post('confirm_password'));
				
				$data['password'] = $password;
				$data['role'] = 1;
				$data['randomkey'] = $this->generateRandomString(100);
				$data['created_at'] = date('Y-m-d h:i:s');
				$data['updated_at'] = date('Y-m-d h:i:s');
				
				$userId = $this->login_model->insertUser($data);
				
				if($userId){
					$this->session->set_flashdata('success', 'You are successfully registered');
					redirect(base_url('login/index'));
				}
			}else{
				$this->session->set_flashdata('error', 'User already present');
				redirect(base_url('login/academic_register'));
			}
		}
		$data['subview'] = 'login/academic_register';
		$this->load->view('layout_login',$data);
	}
	
	public function after_apply()
	{
		if($this->input->post()){
			
			$email = $this->input->post('cemail');
			
			$userPresentData = $this->login_model->userPresent($email);
				
				if(!empty($userPresentData)){
					
					$course_id = $this->input->post('course_id');
					$applytype = $this->input->post('applytype');
					
					$smdata = $this->student_model->getStudentMasterById($userPresentData->id);
					
					if($applytype == 'shortlist'){
						
						$app_data['sc_stm_id'] = $smdata->stm_id;
						$app_data['sc_uc_id'] = $course_id;
						$app_data['created_at'] = date('Y-m-d h:i:s');
						$app_data['updated_at'] = date('Y-m-d h:i:s');
						
						$sc_id = $this->student_model->insertShortlistCourse($app_data);
						
						$app_statusdata['as_sa_id'] = $sc_id;
						$app_statusdata['as_stm_id'] = $smdata->stm_id;
						$app_statusdata['as_app_type'] = 'Shortlist';
						$app_statusdata['as_status'] = 1;
						$app_statusdata['created_at'] = date('Y-m-d h:i:s');
						$app_statusdata['updated_at'] = date('Y-m-d h:i:s');
						
						$this->student_model->insertShortlistStatus($app_statusdata);
						
					}else{
						
						$app_data['sa_stm_id'] = $smdata->stm_id;
						$app_data['sa_uc_id'] = $course_id;
						$app_data['created_at'] = date('Y-m-d h:i:s');
						$app_data['updated_at'] = date('Y-m-d h:i:s');
						
						$sa_id = $this->student_model->insertAppliedCourse($app_data);
						
						$app_statusdata['as_sa_id'] = $sa_id;
						$app_statusdata['as_stm_id'] = $smdata->stm_id;
						$app_statusdata['as_app_type'] = 'Apply';
						$app_statusdata['as_status'] = 1;
						$app_statusdata['created_at'] = date('Y-m-d h:i:s');
						$app_statusdata['updated_at'] = date('Y-m-d h:i:s');
						
						$this->student_model->insertAppliedStatus($app_statusdata);
					}
					$sess_array = array(
						 'id' => $userPresentData->id,
						 'name' => $userPresentData->name,
						 'email' => $userPresentData->email,
						 'role' => $userPresentData->role,
						 'status' => $userPresentData->status,
					);
					
					$this->session->set_userdata('logged_in', $sess_array);
					
					$this->session->set_flashdata('success','Welcome '.$sess_array['name'].'..! Please complete your profile first and select package plan');
					
					redirect(base_url('student/course_packages'));
					
				}else{
					
					$data['name'] = $this->input->post('uname');
					$data['email'] = $this->input->post('cemail');
					
					$password = hash('sha256', $this->input->post('confirm_password'));
					
					$data['password'] = $password;
					$data['role'] = 2;
					$data['randomkey'] = $this->generateRandomString(100);
					$data['created_at'] = date('Y-m-d h:i:s');
					$data['updated_at'] = date('Y-m-d h:i:s');
					
					$userId = $this->login_model->insertUser($data);
					
					if($userId){
					
						$smtdata['stm_user_id'] = $userId;
						$smtdata['created_at'] = date('Y-m-d h:i:s');
						$smtdata['updated_at'] = date('Y-m-d h:i:s');
						
						$sm_id = $this->login_model->insertStudentMstr($smtdata);
						
						if($sm_id){
							
							$course_id = $this->input->post('course_id');
							$applytype = $this->input->post('applytype');
							
							if($applytype == 'shortlist'){
						
								$app_data['sc_stm_id'] = $sm_id;
								$app_data['sc_uc_id'] = $course_id;
								$app_data['created_at'] = date('Y-m-d h:i:s');
								$app_data['updated_at'] = date('Y-m-d h:i:s');
								
								$sc_id = $this->student_model->insertShortlistCourse($app_data);
								
								$app_statusdata['as_sa_id'] = $sc_id;
								$app_statusdata['as_stm_id'] = $sm_id;
								$app_statusdata['as_app_type'] = 'Shortlist';
								$app_statusdata['as_status'] = 1;
								$app_statusdata['created_at'] = date('Y-m-d h:i:s');
								$app_statusdata['updated_at'] = date('Y-m-d h:i:s');
								
								$this->student_model->insertShortlistStatus($app_statusdata);
						
							}else{
								
								$app_data['sa_stm_id'] = $sm_id;
								$app_data['sa_uc_id'] = $course_id;
								$app_data['created_at'] = date('Y-m-d h:i:s');
								$app_data['updated_at'] = date('Y-m-d h:i:s');
								
								$sa_id = $this->student_model->insertAppliedCourse($app_data);
								
								$app_statusdata['as_sa_id'] = $sa_id;
								$app_statusdata['as_stm_id'] = $sm_id;
								$app_statusdata['as_app_type'] = 'Apply';
								$app_statusdata['as_status'] = 1;
								$app_statusdata['created_at'] = date('Y-m-d h:i:s');
								$app_statusdata['updated_at'] = date('Y-m-d h:i:s');
								
								$this->student_model->insertAppliedStatus($app_statusdata);
							}
							
							$sess_array = array(
							 'id' => $userId,
							 'name' => $this->input->post('uname'),
							 'email' => $this->input->post('cemail'),
							 'role' => 2,
							 'status' =>0,
							);
							
							$this->session->set_userdata('logged_in', $sess_array);
							
							$this->session->set_flashdata('success','Welcome '.$sess_array['name'].'..! Please complete your profile first and select package plan');
							
							redirect(base_url('student/course_packages'));
						}
					}
				}
			
		}
	}
	
	// public function after_apply_login()
	// {
	// 	if($this->input->post()){
			
	// 		$email = $this->input->post('email');
			
	// 		$password = hash('sha256', $this->input->post('password'));
			
	// 		$loginUserPresent = $this->login_model->LoginPresent($email,$password);
			
	// 		if($loginUserPresent){
				
	// 				$course_id = $this->input->post('course_id');
	// 				$applytype = $this->input->post('applytype');
					
	// 				$smdata = $this->student_model->getStudentMasterById($loginUserPresent->id);
					
	// 				if($applytype == 'shortlist'){
						
	// 					$app_data['sc_stm_id'] = $smdata->stm_id;
	// 					$app_data['sc_uc_id'] = $course_id;
	// 					$app_data['created_at'] = date('Y-m-d h:i:s');
	// 					$app_data['updated_at'] = date('Y-m-d h:i:s');
						
	// 					$sc_id = $this->student_model->insertShortlistCourse($app_data);
						
	// 					$app_statusdata['as_sa_id'] = $sc_id;
	// 					$app_statusdata['as_stm_id'] = $smdata->stm_id;
	// 					$app_statusdata['as_app_type'] = 'Shortlist';
	// 					$app_statusdata['as_status'] = 1;
	// 					$app_statusdata['created_at'] = date('Y-m-d h:i:s');
	// 					$app_statusdata['updated_at'] = date('Y-m-d h:i:s');
						
	// 					$this->student_model->insertShortlistStatus($app_statusdata);
						
	// 				}else{
						
	// 					$app_data['sa_stm_id'] = $smdata->stm_id;
	// 					$app_data['sa_uc_id'] = $course_id;
	// 					$app_data['created_at'] = date('Y-m-d h:i:s');
	// 					$app_data['updated_at'] = date('Y-m-d h:i:s');
						
	// 					$sa_id = $this->student_model->insertAppliedCourse($app_data);
						
	// 					$app_statusdata['as_sa_id'] = $sa_id;
	// 					$app_statusdata['as_stm_id'] = $smdata->stm_id;
	// 					$app_statusdata['as_app_type'] = 'Apply';
	// 					$app_statusdata['as_status'] = 1;
	// 					$app_statusdata['created_at'] = date('Y-m-d h:i:s');
	// 					$app_statusdata['updated_at'] = date('Y-m-d h:i:s');
						
	// 					$this->student_model->insertAppliedStatus($app_statusdata);
	// 				}
				
	// 			$sess_array = array(
	// 			 'id' => $loginUserPresent->id,
	// 			 'name' => $loginUserPresent->name,
	// 			 'email' => $loginUserPresent->email,
	// 			 'role' => $loginUserPresent->role,
	// 			 'status' => $loginUserPresent->status,
	// 		   );
			   
	// 			$this->session->set_userdata('logged_in', $sess_array);
				
	// 			$this->session->set_flashdata('success','Welcome '.$sess_array['name'].'..! Please complete your profile first and select package plan');
				
	// 			redirect(base_url('student/course_packages'));	
				
	// 		}else{
				
	// 			$this->session->set_flashdata('error', 'Invalid Username and Password');
	// 			redirect(base_url('home/index'));
	// 		}
	// 	}
	// }
public function register(){
		//print_r($this->input->post());die;
		if($this->input->post()){
			
			$email = $this->input->post('email');
			
			$userPresentData = $this->login_model->userPresent($email);
			
			if(empty($userPresentData)){
				
				$data['name'] = $this->input->post('uname');
				$data['email'] = $email;
				
				$password = hash('sha256', $this->input->post('password'));
				
				$data['password'] = $password;
				$data['role'] = 2;
				$data['IsActive'] = 1;
				$data['randomkey'] = $this->generateRandomString(100);
				$data['created_at'] = date('Y-m-d h:i:s');
				$data['updated_at'] = date('Y-m-d h:i:s');
				
				$userId = $this->login_model->insertUser($data);
				
				if($userId){
					
					$smtdata['stm_user_id'] = $userId;
					$smtdata['created_at'] = date('Y-m-d h:i:s');
					$smtdata['updated_at'] = date('Y-m-d h:i:s');
					
					$sm_id = $this->login_model->insertStudentMstr($smtdata);
					
					if($sm_id){
						
						$username = $this->input->post('uname');
						
						$subject = 'Verify Email';
						
				        $message = 'Dear '.$username.',<br/><br/> Please click on the below activation link to verify your email address.<br/><br/> <a href="'.base_url('login/email_verify/'.md5($email).'/' . base64_encode($sm_id)).'">Click Here</a> <br/><br/><br/> Thanks <br/> Hello Admission Team';
						
						$config['protocol']    = 'smtp';
						$config['smtp_host']    = 'ssl://smtp.gmail.com';
						$config['smtp_port']    = '465';
						$config['smtp_timeout'] = '7';
						$config['smtp_user']    = 'noreply@talentedgenext.com';
						$config['smtp_pass']    = 'Mohammed@1292';
						$config['charset']    = 'utf-8';
						$config['newline']    = "\r\n";
						$config['mailtype'] = 'html';

						$this->email->initialize($config);
						
				        $this->email->from('noreply@talentedgenext.com', 'HELLO ADMISSION');
				        $this->email->to($email);
				        $this->email->subject($subject);
				        $this->email->message($message);
				        $this->email->send();
						
						//$this->session->set_flashdata('success', 'You are successfully registered..!  Please check your email to verify !');
						echo json_encode(array('msg' =>'You are successfully registered..!  Please check your email to verify !'));
						//$this->session->set_flashdata('info', 'Please check your email to verify !');
						//redirect(base_url('login/index'));
					}
					
				}
			}else{
				echo json_encode(array('msg' =>'User already present'));
				//$this->session->set_flashdata('error', 'User already present');

				//redirect(base_url('login'));
			}
		}
	}
	public function after_apply_login()
	{

		if($this->input->post()){
			
			$email = $this->input->post('email');
			
			$password = hash('sha256', $this->input->post('password'));
			
			$loginUserPresent = $this->login_model->LoginPresent_student($email,$password);
			
			if($loginUserPresent){
				
					$course_id = $this->input->post('course_id');
					$applytype = $this->input->post('applytype');
					
					$smdata = $this->student_model->getStudentMasterById($loginUserPresent->id);
				//	print_r($smdata);
			$verify = $this->db->get_where('users',array('id'=>$smdata->stm_user_id));
			$emailverify=$verify->result();
			//echo $emailverify[0]->email_confirmed;die;

			 if($emailverify[0]->email_confirmed!='NO'){
					if($applytype == 'shortlist'){
						$query = $this->db->get_where('shortlisted_courses',array('sc_stm_id'=>$smdata->stm_id,'sc_uc_id'=>$course_id));
						if($query->num_rows() > 0)
								{ 
									echo json_encode(array('msg' =>'Already Shortlisted'));
								$sess_array = array(
								 'id' => $loginUserPresent->id,
								 'name' => $loginUserPresent->name,
								 'email' => $loginUserPresent->email,
								 'role' => $loginUserPresent->role,
								 'status' => $loginUserPresent->status,
							   );
								$this->session->set_userdata('logged_in', $sess_array);
			   
							  }else{
							  		$app_data['sc_stm_id'] = $smdata->stm_id;
									$app_data['sc_uc_id'] = $course_id;
									$app_data['created_at'] = date('Y-m-d h:i:s');
									$app_data['updated_at'] = date('Y-m-d h:i:s');
									
									$sc_id = $this->student_model->insertShortlistCourse($app_data);
									
									$app_statusdata['as_sa_id'] = $sc_id;
									$app_statusdata['as_stm_id'] = $smdata->stm_id;
									$app_statusdata['as_app_type'] = 'Shortlist';
									$app_statusdata['as_status'] = 1;
									$app_statusdata['created_at'] = date('Y-m-d h:i:s');
									$app_statusdata['updated_at'] = date('Y-m-d h:i:s');
									
									$this->student_model->insertShortlistStatus($app_statusdata);

							   	$sess_array = array(
								 'id' => $loginUserPresent->id,
								 'name' => $loginUserPresent->name,
								 'email' => $loginUserPresent->email,
								 'role' => $loginUserPresent->role,
								 'status' => $loginUserPresent->status,
							   );
							  	$this->session->set_userdata('logged_in', $sess_array);
							  	echo json_encode(array('msg' =>'Successfully'));
							  }
						
					}else{
						$query_app = $this->db->get_where('student_applications',array('sa_stm_id'=>$smdata->stm_id,'sa_uc_id'=>$course_id));
						if($query_app->num_rows() > 0)
								{ 
									echo json_encode(array('msg' =>'Already Applied'));
								$sess_array = array(
								 'id' => $loginUserPresent->id,
								 'name' => $loginUserPresent->name,
								 'email' => $loginUserPresent->email,
								 'role' => $loginUserPresent->role,
								 'status' => $loginUserPresent->status,
							   );
								$this->session->set_userdata('logged_in', $sess_array);
			   
							  }else{
									$app_data['sa_stm_id'] = $smdata->stm_id;
									$app_data['sa_uc_id'] = $course_id;
									$app_data['created_at'] = date('Y-m-d h:i:s');
									$app_data['updated_at'] = date('Y-m-d h:i:s');
									
									$sa_id = $this->student_model->insertAppliedCourse($app_data);
									
									$app_statusdata['as_sa_id'] = $sa_id;
									$app_statusdata['as_stm_id'] = $smdata->stm_id;
									$app_statusdata['as_app_type'] = 'Apply';
									$app_statusdata['as_status'] = 1;
									$app_statusdata['created_at'] = date('Y-m-d h:i:s');
									$app_statusdata['updated_at'] = date('Y-m-d h:i:s');
									
									$this->student_model->insertAppliedStatus($app_statusdata);
									 	$sess_array = array(
								 'id' => $loginUserPresent->id,
								 'name' => $loginUserPresent->name,
								 'email' => $loginUserPresent->email,
								 'role' => $loginUserPresent->role,
								 'status' => $loginUserPresent->status,
							   );
							  	$this->session->set_userdata('logged_in', $sess_array);
							  	echo json_encode(array('msg' =>'Successfully'));
							  }
						
					}

				}else{
					$sess_array = array(
								 'id' => $loginUserPresent->id,
								 'name' => $loginUserPresent->name,
								 'email' => $loginUserPresent->email,
								 'role' => $loginUserPresent->role,
								 'status' => $loginUserPresent->status,
							   );
							  	$this->session->set_userdata('logged_in', $sess_array);
					echo json_encode(array('msg' =>'Please verify your email address'));
				}

		
				
			}else{
				
				//$this->session->set_flashdata('error', 'Invalid Username and Password');
			//	redirect(base_url('home/index'));
				echo json_encode(array('msg' =>'Invalid Username and Password'));
			}
		}
	}
	
	public function forgot_password()
	{
		$data['subview'] = 'login/forgot_password';
		$this->load->view('layout_login',$data);
	}
	
	public function change_password($id=null)
	{
		$data['user_id']=base64_decode($this->uri->segment(4));
		$data['subview'] = 'login/change_password';
		$this->load->view('layout_login',$data);
	}

	public function email_verify($email_hash=null)
	{
		$email_hash=$this->uri->segment(3);
		$url=$this->uri->segment(5);
		//print_r(base64_decode($url));die;
		$data=array('status' =>1 ,'email_confirmed'=>'YES' );
		$query=$this->db->where('md5(email)',$email_hash)->update('users',$data);

		if($query){
            $this->session->set_flashdata('info','Your Email is successfully verified!');
            redirect(base64_decode($url));
        }
        else
        {
            $this->session->set_flashdata('error','Sorry! There is error verifying your Email Address!');
            redirect(base_url('login'));
        }
		//$data['subview'] = 'login/change_password';
		//$this->load->view('layout_login',$data);
	}
	
	public function update_password()
	{
		$userid=$this->input->post('userid');
		$newpass=$this->input->post('npassword');
		$result=$this->db->where('id',$userid)->update('users',array('password'=>hash('sha256',$newpass)));
		if($result){
			echo json_encode(array('msg'=>'Password change Successfully'));
		}
	}
	
	public function forgot()
	{
		$email=$this->input->post('email');
		$check=$this->login_model->check_email($email);
		if($check)
		{
			// $sess_array = array(
				 // 'id' => $check[0]->id,
				 // 'name' => $check[0]->name,
				 // 'role' => $check[0]->role,

			   // );
			   $subject = 'Reset Password';
		
        $message = '<b>Dear ,<b>'.$check[0]->name.',<br/><br/> Please click on the below activation link to reset your password.<br/><br/> <a href='.base_url('login/change_password/'.md5($check[0]->name).'/' . base64_encode($check[0]->id)).'>Click Here</a> <br/><br/><br/> Thanks <br/> Hello Admission Team';
		
		$config['protocol']    = 'smtp';
		$config['smtp_host']    = 'ssl://smtp.gmail.com';
		$config['smtp_port']    = '465';
		$config['smtp_timeout'] = '7';
		$config['smtp_user']    = 'noreply@talentedgenext.com';
		$config['smtp_pass']    = 'Mohammed@1292';
		$config['charset']    = 'utf-8';
		$config['newline']    = "\r\n";
		$config['mailtype'] = 'html';

		$this->email->initialize($config);
		
        $this->email->from('noreply@talentedgenext.com', 'Hello Admission');
        $this->email->to($email);
        $this->email->subject($subject);
        $this->email->message($message);
        $this->email->send();
			  
			
			echo json_encode(array('msg'=>'Mail send on your register email Please check'));
			
		}
		else{
			echo json_encode(array('msg'=>'Enter email does not exits'));
		}
	}
	
	function generateRandomString($length = 10) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}
	
	function logout()
	{
		// Remove local Facebook session
		$this->facebook->destroy_session();
		// Remove user data from session
		$this->session->unset_userdata('userData');
		// Redirect to login page
	   $this->session->unset_userdata('logged_in');
	   $this->session->sess_destroy();
	   $this->session->set_flashdata('success','You are Successfully Logout..!');
	   redirect(base_url('home'));
	}
	
	function verify($hash=NULL)
    {
        if ($this->login_model->verifyEmailID($hash))
        {
            $this->session->set_flashdata('success','Your Email Address is successfully verified! Please login to access your account!');
            redirect(base_url('login'));
        }
        else
        {
            $this->session->set_flashdata('error','Sorry! There is error verifying your Email Address!');
            redirect(base_url('login/register'));
        }
    }
	 public function google_outh()
	{
		$userData=array();
		if(isset($_GET['code'])){
			//authenticate user
			$this->google->getAuthenticate();
			
			//get user info from google
			$gpInfo = $this->google->getUserInfo();
			$pass='123456';
            //preparing data for database insertion
			$userData['oauth_provider'] = 'google';
			$userData['oauth_uid'] 		= $gpInfo['id'];
            $userData['name'] 	= $gpInfo['given_name'];
            $userData['password'] = hash('sha256', $pass);
            $userData['email'] 			= $gpInfo['email'];
            $userData['password'] = hash('sha256', $pass);
            $userData['role'] = 2;
			$userID = $this->login_model->checkUser($userData);
		
            if(!empty($userID)){
            $get_userdata=$this->login_model->get_fb_user($userID);
       
            $smtdata['stm_user_id'] = $userID;
					$smtdata['created_at'] = date('Y-m-d h:i:s');
					$smtdata['updated_at'] = date('Y-m-d h:i:s');
					$sm_id = $this->login_model->insertStudentMstr($smtdata);
                $loginUserPresent = $this->login_model->Login_fb($get_userdata[0]->email,$get_userdata[0]->oauth_uid);
			if($loginUserPresent){
				$sess_array = array(
				 'id' => $loginUserPresent->id,
				 'name' => $loginUserPresent->name,
				 'email' => $loginUserPresent->email,
				 'role' => $loginUserPresent->role,
				 'status' => $loginUserPresent->status,
			   );
			   //print_r($sess_array);
				$this->session->set_userdata('logged_in', $sess_array);
				$this->session->set_flashdata('success','Welcome '.$sess_array['name'].'..!');
				if($sess_array['role'] == 1){
					redirect(base_url('dashboard/au'));
				}else if($sess_array['role'] == 2){
					redirect(base_url('home'));
				}else if($sess_array['role'] == 4){
					redirect(base_url('admin/dashboard'));
				}
			}else{
				$this->session->set_flashdata('error', 'Invalid Username and Password');
			}
            } 
			
		} 
	}
	function sendVerificationMail($user_name,$to_email)
    {
        $subject = 'Email Verification';
		
        $message = 'Dear '.$user_name.',<br/><br/> Please click on the below activation link to verify your email address.<br/><br/> '.base_url('login/verify/' . md5($to_email)).' <br/><br/><br/> Thanks <br/> Talentedge Team';
		
		$config['protocol']    = 'smtp';
		$config['smtp_host']    = 'ssl://smtp.gmail.com';
		$config['smtp_port']    = '465';
		$config['smtp_timeout'] = '7';
		$config['smtp_user']    = 'noreply@talentedgenext.com';
		$config['smtp_pass']    = 'Mohammed@1292';
		$config['charset']    = 'utf-8';
		$config['newline']    = "\r\n";
		$config['mailtype'] = 'html';

		$this->email->initialize($config);
		
        $this->email->from('noreply@talentedgenext.com', 'TALENTEDGENEXT');
        $this->email->to($to_email);
        $this->email->subject($subject);
        $this->email->message($message);
        $this->email->send();
		return true;
    }
}
