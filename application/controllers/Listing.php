<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Listing extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		//$this->load->library('session');
		$this->load->model('listing_model');
		$this->load->model('academic_model');
		$this->load->model('university_model');
		$this->load->model('student_model',true);
	}
	public function index()
	{
	$course = $this->uri->segment(3);
	//echo $course;
		$encCourse = urldecode($course);
		$courseMaster = $this->listing_model->getMasterCourse(trim($encCourse));
		$courseMaster_spe = $this->listing_model->getMasterCourse_spe(trim($encCourse));
		
		$visitor=$this->session->userdata('visitor');
	
		$course_filter = $this->listing_model->getMasterCourse(trim($visitor['cid']));
		$coursesData = array();
		
		if(!empty($course_filter)){	
			//echo 'test';
			$coursesData =  $this->listing_model->getUniversityCourses_visitory($courseMaster->id,$visitor);
			$data['uniCourses'] = $coursesData;
	//echo '<pre>',print_r($data['uniCourses']);die;
			$data['subview'] = 'listing/list';
	
			$data['spe']=$this->academic_model->getSpe();
			$data['streams'] = $this->academic_model->getCourseMainMaster();
			$this->load->view('layout_main',$data);	
				 $this->session->unset_userdata('visitor');
		}else if(!empty($courseMaster)){	
//echo 'test1';
			$coursesData =  $this->listing_model->getUniversityCourses($courseMaster->id);
			$data['uniCourses'] = $coursesData;

			$data['subview'] = 'listing/list';
	
			$data['spe']=$this->academic_model->getSpe();
			$data['streams'] = $this->academic_model->getCourseMainMaster();
			$this->load->view('layout_main',$data);	
				
		}else if(!empty($courseMaster_spe)){	

			$coursesData =  $this->listing_model->getUniversityCourses($courseMaster_spe->id);
			$data['uniCourses'] = $coursesData;
 //echo '<pre>',print_r($data['uniCourses']);die;
			$data['subview'] = 'listing/list';
	
			$data['spe']=$this->academic_model->getSpe();
			$data['streams'] = $this->academic_model->getCourseMainMaster();
			$this->load->view('layout_main',$data);	
				
		}
	else{
			
			$university = $this->university_model->getUniversityByName(trim($encCourse));
			$data['universData'] = $university;
			$data['uniMedia'] = $this->university_model->getUniversityMedia($university->um_id);
			$Facility = $this->university_model->getUniversityFacility($university->um_id);
			foreach($Facility as $Facil){
				$newFac[] = $Facil->ufc_name; 
			}
			if(!empty($newFac)){
				$data['uniFacility'] =$newFac;
			}
			
			$data['uniCourse'] = $this->university_model->getUniversityCourse($university->um_id);
			$data['semilar_college']=$this->university_model->getsemilar_college();
			//echo '<pre>',print_r($data['semilar_college']);
			$data['subview'] = 'university/index';
			$this->load->view('layout_main',$data);
		}
		
		if(!empty($courseMaster) && !empty($university)){
			$data['uniCourses'] = $coursesData;
			$data['spe']=$this->academic_model->getSpe();
			$data['streams'] = $this->academic_model->getCourseMainMaster();
			$data['subview'] = 'listing/list';
			$this->load->view('layout_main',$data);	
		}
		
	}
	
	public function stream_program()
	{
		$streamid = $this->input->post('streamid');
		$sid = $this->input->post('spid');
		//print_r($streamid);
		//print_r($sid);
		if($streamid!='')
		{
		$cource=array();
		$coursesData =  $this->listing_model->getUniversityCourses_filter($streamid,$sid);
		$data['uniCourses'] = $coursesData;
		array_push($cource,$coursesData);
		foreach($data['uniCourses'] as $uniCrs){
			//echo $uniCrs->uc_is_approved ;
				if($uniCrs->uc_is_approved ==1){
				$crsStream = $this->academic_model->getCourseById($uniCrs->id);
				array_push($cource,$crsStream);
				$uc_fees = explode(',',$uniCrs->uc_fees);
				
				$umId = $uniCrs->uc_um_id;
				
				for($i=0;$i<count($uc_fees);$i++)
				{	
					$LumpsumFeesData = $this->academic_model->getLumsumpFees($uc_fees[$i],$umId,'Lumpsum');
					$InstallmentFeesData = $this->academic_model->getInstallmentFees($uc_fees[$i],$umId,'Installment');	
					//array_push($cource,$LumpsumFeesData);
					//array_push($cource,$InstallmentFeesData);
					
				}
				}
		}
		}
		//print_r($cource);
		// $re='';
		// if(!empty($cource))
		// {
			// $re=$cource;
		// }
		// else{
			// $re='No Record found';
		// }
		
		$programdata = $this->listing_model->getCourseMainMaster($streamid);
		//if(!empty($cource) ){
		$arr=array('spelist'=>$programdata,'cource'=>$cource);
		//}else{
		//	$arr=array('spelist'=>$programdata,'cource'=>'No record');
		
		//}
		echo json_encode($arr);
	}

public function stream_program_new()
	{
		$streamid = $this->input->post('streamid');
		$sid = $this->input->post('spid');
		//print_r($streamid);
		//print_r($sid);
		if($streamid!='')
		{
		$cource=array();
		$coursesData =  $this->listing_model->getUniversityCourses_filter($streamid,$sid);
		$data['uniCourses'] = $coursesData;
		array_push($cource,$coursesData);
		foreach($data['uniCourses'] as $uniCrs){
			//echo $uniCrs->uc_is_approved ;
				if($uniCrs->uc_is_approved ==1){
				$crsStream = $this->academic_model->getCourseById($uniCrs->id);
				array_push($cource,$crsStream);
				$uc_fees = explode(',',$uniCrs->uc_fees);
				
				$umId = $uniCrs->uc_um_id;
				
				for($i=0;$i<count($uc_fees);$i++)
				{	
					$LumpsumFeesData = $this->academic_model->getLumsumpFees($uc_fees[$i],$umId,'Lumpsum');
					$InstallmentFeesData = $this->academic_model->getInstallmentFees($uc_fees[$i],$umId,'Installment');	
					//array_push($cource,$LumpsumFeesData);
					//array_push($cource,$InstallmentFeesData);
					
				}
				}
		}
		}
		//print_r($cource);
		// $re='';
		// if(!empty($cource))
		// {
			// $re=$cource;
		// }
		// else{
			// $re='No Record found';
		// }
		
		$programdata = $this->listing_model->getCourseMainMaster($streamid);
		//if(!empty($cource) ){
		$arr=array('spelist'=>$programdata,'cource'=>$cource);
		//}else{
		//	$arr=array('spelist'=>$programdata,'cource'=>'No record');
		
		//}
		echo json_encode($arr);
	}

	public function viewcource(){
			$streamid=$this->uri->segment(3);
			$ucmid=$this->uri->segment(4);
			
			//$data['similar_course'] = $this->listing_model->get_similar_coruse($ucmid);
			$encCourse = urldecode($ucmid);
			$courseMaster = $this->listing_model->getMasterCourse_spe(trim($encCourse));
			//print_r($courseMaster);
			
			//echo $courseMaster->id;die;
			if($courseMaster){
				$coursesData =  $this->listing_model->getUniversityCourses($courseMaster->id);
			}else{
				
			}
			if(empty($coursesData)){
				$data['similar_course'] = '';
			}else{
				$data['similar_course'] = $coursesData;
			}
			
			//echo '<pre>',print_r($data);
			$data['details'] = $this->listing_model->get_cource_details($streamid);
			$data['subview'] = 'listing/view_details';
			$this->load->view('layout_main',$data);	

	}
	public function setCompareSession()
	{
		$course_comp_array = array(
		 'courseIds' => $this->input->post('courseIds'),
		 'uIds' => $this->input->post('uId')
		);
		   
		 
		$this->session->set_userdata('course_compare', $course_comp_array);
		
		$session=$this->session->userdata('course_compare');

			redirect(base_url('listing/compareCourse'));

		
	}
	public function compareCourse()
	{
		$sesCourseArray = $this->session->userdata('course_compare');
		$courseIds = $sesCourseArray['courseIds'];
		$uIds = $sesCourseArray['uIds'];
		$newCourseArray = array();
		for ($i=0; $i <count($courseIds) ; $i++) { 
			$newCourseArray[] = $this->listing_model->get_cource_details($courseIds[$i]);
		//	echo ;
		}
		//echo '<pre>',print_r($newCourseArray);die();
			$data['comp']=$newCourseArray;
			$data['subview'] = 'listing/compare';
			$this->load->view('layout_main',$data);	
		
	}
	public function resetCompare($couse=null)
	{
		$this->session->unset_userdata('course_compare');
		//$page_url=current_url();die();
		redirect('listing/index/'.$couse);
	}
}
