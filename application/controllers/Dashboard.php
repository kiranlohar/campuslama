<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->model('dashboard_model');
		$this->load->model('student_model');
	}
	
	public function au()
	{
		$data['subview'] = 'dashboard/au';
		$this->load->view('layout_au_main',$data);
	}
	public function user_setting()
	{
		$data['subview'] = 'academic/user_setting';
		$this->load->view('layout_stud_main',$data);
	}
	
	public function student(){
		$userdata = $this->session->userdata('logged_in');
		$smdata = $this->student_model->getStudentMasterById($userdata['id']);
	
		if(!empty($smdata)){
			$appData = $this->dashboard_model->getUserApplications($smdata->stm_id);
				//echo '<pre>',print_r($appData);
			$shortData = $this->dashboard_model->getUserShortlist($smdata->stm_id);
			$data['courseApp'] = $appData;
			$data['courseShort'] = $shortData;
		}else{
			$data['courseApp'] = '';
			$data['courseShort'] ='';
		}
		
		$data['subview'] = 'dashboard/student';
		$this->load->view('layout_stud_main',$data);
	}
	
	public function course_apply(){
		
		if($this->input->post()){
			
			$user_id = $this->input->post('userid');
			$course_id = $this->input->post('courceid');
			
			$smdata = $this->student_model->getStudentMasterById($user_id);
			$userid=$smdata->stm_id;
			$chk = $this->student_model->check_applied($userid,$course_id);
			if($chk==false){
			$app_data['sa_stm_id'] = $smdata->stm_id;
			$app_data['sa_uc_id'] = $course_id;
			$app_data['created_at'] = date('Y-m-d h:i:s');
			$app_data['updated_at'] = date('Y-m-d h:i:s');
			
			
			$sa_id = $this->student_model->insertAppliedCourse($app_data);
			
			if($sa_id){
				
				$app_statusdata['as_sa_id'] = $sa_id;
				$app_statusdata['as_stm_id'] = $smdata->stm_id;
				$app_statusdata['as_app_type'] = 'Apply';
				$app_statusdata['as_status'] = 1;
				$app_statusdata['created_at'] = date('Y-m-d h:i:s');
				$app_statusdata['updated_at'] = date('Y-m-d h:i:s');
				
				$this->student_model->insertAppliedStatus($app_statusdata);
				echo 'Course Applied Successfully';	
				//$this->session->set_flashdata('success','Course Applied Successfully');
				//redirect(base_url('dashboard/student'));
			}
			}
		else{
			echo 'Already Applied';
		}
		}
	}
}
