<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Student extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->library('email');
		$this->load->model('student_model');
	}
	
	public function personal_details(){
		
		$userdata = $this->session->userdata('logged_in');
		$smdata = $this->student_model->getStudentMasterById($userdata['id']);
		
		$stmId = '';
		$data['personalData'] ='';
		
		if(!empty($smdata)){
			
			if($this->input->post()){
				
				if(!empty($_FILES['profile_pic']['name'])){
					if(is_uploaded_file($_FILES['profile_pic']['tmp_name'])){
						$sourcePath = $_FILES['profile_pic']['tmp_name'];
						$temp = explode(".", $_FILES["profile_pic"]["name"]);
						$newfilename = round(microtime(true)) . '.' . end($temp);
						$targetPath = "uploads/users/".$newfilename;
						move_uploaded_file($sourcePath,$targetPath);
					}else{
						$newfilename = '';
					}
				}
				
				$firstname = $this->input->post('firstname');
				$middlename = $this->input->post('middlename');
				$lastname = $this->input->post('lastname');
				$email = $this->input->post('email');
				$mobile = $this->input->post('mobile');
				$birthdate = date('Y-m-d',strtotime($this->input->post('birthdate')));
				$gender = $this->input->post('gender');
				$country = $this->input->post('country');
				$state = $this->input->post('state');
				$city = $this->input->post('city');
				$pincode = $this->input->post('pincode');
				$address = $this->input->post('address');
				$about_you = $this->input->post('about_you');
				$profile_pic = $newfilename;
				$stmId = $smdata->stm_id;
				
				$studdata['spd_stm_id'] = $stmId;
				$studdata['spd_firstname'] = $firstname;
				$studdata['spd_middlename'] = $middlename;
				$studdata['spd_lastname'] = $lastname;
				$studdata['spd_email'] = $email;
				$studdata['spd_mobile'] = $mobile; 
				$studdata['spd_dob'] = $birthdate;
				$studdata['spd_gender'] = $gender;
				$studdata['spd_profile_pic'] = $profile_pic;
				$studdata['spd_cm_id'] = $country;
				$studdata['spd_sm_id'] = $state;
				$studdata['spd_cim_id'] = $city;
				$studdata['spd_pincode'] = $pincode;
				$studdata['spd_address'] = $address;
				$studdata['spd_about_you'] = $about_you;
				$studdata['created_at'] = date('Y-m-d h:i:s');
				$studdata['updated_at'] = date('Y-m-d h:i:s');
				
				$spd_id = $this->student_model->insertPersonalData($studdata);
				
				if($spd_id){
					$this->session->set_flashdata('success','Personal Details added Successfully');
					redirect(base_url('student/personal_details'));
				}
			}
			
			$data['personalData'] = $this->student_model->getPersonalDetails($smdata->stm_id);
			
			if(!empty($data['personalData'])){
				$data['statedata'] = $this->student_model->getStateByCountry($data['personalData']->spd_cm_id);
				$data['citydata'] = $this->student_model->getCityByState($data['personalData']->spd_sm_id);
			}
		}
		$data['userData'] = $userdata;
		$data['subview'] = 'student/personal_details';
		$this->load->view('layout_stud_main',$data);
	}
	
	public function edit_personal_details(){
		
		$userdata = $this->session->userdata('logged_in');
		$smdata = $this->student_model->getStudentMasterById($userdata['id']);
		
		if($this->input->post()){
			
			if(!empty($_FILES['profile_pic']['name'])){
				if(is_uploaded_file($_FILES['profile_pic']['tmp_name'])){
					$sourcePath = $_FILES['profile_pic']['tmp_name'];
					$temp = explode(".", $_FILES["profile_pic"]["name"]);
					$newfilename = round(microtime(true)) . '.' . end($temp);
					$targetPath = "uploads/users/".$newfilename;
					move_uploaded_file($sourcePath,$targetPath);
				}else{
					$newfilename = '';
				}
			}else{
				
				$newfilename = $this->input->post('file_selected');
			}
			
			$spd_id = $this->input->post('spd_id');
			$firstname = $this->input->post('firstname');
			$middlename = $this->input->post('middlename');
			$lastname = $this->input->post('lastname');
			$email = $this->input->post('email');
			$mobile = $this->input->post('mobile');
			$birthdate = date('Y-m-d',strtotime($this->input->post('birthdate')));
			$gender = $this->input->post('gender');
			$country = $this->input->post('country');
			$state = $this->input->post('state');
			$city = $this->input->post('city');
			$pincode = $this->input->post('pincode');
			$address = $this->input->post('address');
			$about_you = $this->input->post('about_you');
			$profile_pic = $newfilename;
			
			$studdata['spd_stm_id'] = $smdata->stm_id;
			$studdata['spd_firstname'] = $firstname;
			$studdata['spd_middlename'] = $middlename;
			$studdata['spd_lastname'] = $lastname;
			$studdata['spd_email'] = $email;
			$studdata['spd_mobile'] = $mobile; 
			$studdata['spd_dob'] = $birthdate;
			$studdata['spd_gender'] = $gender;
			$studdata['spd_profile_pic'] = $profile_pic;
			$studdata['spd_cm_id'] = $country;
			$studdata['spd_sm_id'] = $state;
			$studdata['spd_cim_id'] = $city;
			$studdata['spd_pincode'] = $pincode;
			$studdata['spd_address'] = $address;
			$studdata['spd_about_you'] = $about_you;
			$studdata['updated_at'] = date('Y-m-d h:i:s');
			
			if($this->student_model->updatePersonalData($studdata,$spd_id)){
				$this->session->set_flashdata('success','Personal Details updated Successfully');
				redirect(base_url('student/personal_details'));
			}
		}
	}
	
	
	// public function educational_details(){
		
	// 	$userdata = $this->session->userdata('logged_in');
	// 	$smdata = $this->student_model->getStudentMasterById($userdata['id']);
	// 	if(!empty($smdata)){
	// 	if($this->input->post()){
			
	// 		$ssc_pass_year = $this->input->post('ssc_pass_year');
	// 		$ssc_percent = $this->input->post('ssc_percent');
	// 		$ssc_school_name = $this->input->post('ssc_school_name');
	// 		$hsc_pass_year = $this->input->post('hsc_pass_year');
	// 		$hsc_percent = $this->input->post('hsc_percent');
	// 		$hsc_college_name = $this->input->post('hsc_college_name');
	// 		$hsc_university_name = $this->input->post('hsc_university_name');
	// 		$hsc_specialisation = $this->input->post('hsc_specialisation');
	// 		$diploma_pass_year = $this->input->post('diploma_pass_year');
	// 		$diploma_percent = $this->input->post('diploma_percent');
	// 		$diploma_college_name = $this->input->post('diploma_college_name');
	// 		$diploma_university_name = $this->input->post('diploma_university_name');
	// 		$diploma_specialisation = $this->input->post('diploma_specialisation');
	// 		$grad_pass_year = $this->input->post('grad_pass_year');
	// 		$graduate_percent = $this->input->post('graduate_percent');
	// 		$grad_college_name = $this->input->post('grad_college_name');
	// 		$grad_university_name = $this->input->post('grad_university_name');
	// 		$grad_specialisation = $this->input->post('grad_specialisation');
	// 		$edu_achieve = $this->input->post('edu_achieve');
			
	// 		$edudata['sed_stm_id'] = $smdata->stm_id;
	// 		$edudata['sed_10th_passout_year'] = $ssc_pass_year;
	// 		$edudata['sed_10th_percent'] = $ssc_percent;
	// 		$edudata['sed_10th_school_name'] = $ssc_school_name;
	// 		$edudata['sed_12th_passout_year'] = $hsc_pass_year;
	// 		$edudata['sed_12th_percent'] = $hsc_percent;
	// 		$edudata['sed_12th_college_name'] = $hsc_college_name; 
	// 		$edudata['sed_12th_university_name'] = $hsc_university_name; 
	// 		$edudata['sed_12th_specialisation'] = $hsc_specialisation;
	// 		$edudata['sed_diploma_passout_year'] = $diploma_pass_year;
	// 		$edudata['sed_diploma_percent'] = $diploma_percent;
	// 		$edudata['sed_diploma_college_name'] = $diploma_college_name;
	// 		$edudata['sed_diploma_specialisation'] = $diploma_specialisation;
	// 		$edudata['sed_diploma_university'] = $diploma_university_name;
	// 		$edudata['sed_graduate_passout_year'] = $grad_pass_year;
	// 		$edudata['sed_graduate_percent'] = $grad_percent;
	// 		$edudata['sed_graduate_college_name'] = $grad_college_name;
	// 		$edudata['sed_graduate_specialisation'] = $grad_specialisation;
	// 		$edudata['sed_graduate_university'] = $grad_university_name;
	// 		$edudata['sed_achievement'] = $edu_achieve;
	// 		$edudata['created_at'] = date('Y-m-d h:i:s');
	// 		$edudata['updated_at'] = date('Y-m-d h:i:s');
			
	// 		$sed_id = $this->student_model->insertEducationalData($edudata);
			
	// 		if($sed_id){
	// 			$this->session->set_flashdata('success','Educational Details added Successfully');
	// 			redirect(base_url('student/educational_details'));
	// 		}
	// 	}
	// 	$data['eduData'] = $this->student_model->getEducationalDetails($smdata->stm_id);
	// 	}
		
	// 	$data['courses'] = $this->student_model->getCourseName();
	// 	$data['subview'] = 'student/educational_details';
	// 	$this->load->view('layout_stud_main',$data);
	// }
	
	// public function edit_educational_details(){
		
	// 	$userdata = $this->session->userdata('logged_in');
	// 	$smdata = $this->student_model->getStudentMasterById($userdata['id']);
		
	// 	if($this->input->post()){
			
	// 		$sed_id = $this->input->post('sed_id');
	// 		$ssc_pass_year = $this->input->post('ssc_pass_year');
	// 		$ssc_percent = $this->input->post('ssc_percent');
	// 		$ssc_school_name = $this->input->post('ssc_school_name');
	// 		$hsc_pass_year = $this->input->post('hsc_pass_year');
	// 		$hsc_percent = $this->input->post('hsc_percent');
	// 		$hsc_college_name = $this->input->post('hsc_college_name');
	// 		$hsc_university_name = $this->input->post('hsc_university_name');
	// 		$hsc_specialisation = $this->input->post('hsc_specialisation');
	// 		$diploma_pass_year = $this->input->post('diploma_pass_year');
	// 		$diploma_percent = $this->input->post('diploma_percent');
	// 		$diploma_college_name = $this->input->post('diploma_college_name');
	// 		$diploma_university_name = $this->input->post('diploma_university_name');
	// 		$diploma_specialisation = $this->input->post('diploma_specialisation');
	// 		$grad_pass_year = $this->input->post('grad_pass_year');
	// 		$graduate_percent = $this->input->post('graduate_percent');
	// 		$grad_college_name = $this->input->post('grad_college_name');
	// 		$grad_university_name = $this->input->post('grad_university_name');
	// 		$grad_specialisation = $this->input->post('grad_specialisation');
	// 		$edu_achieve = $this->input->post('edu_achieve');
			
	// 		$edudata['sed_10th_passout_year'] = $ssc_pass_year;
	// 		$edudata['sed_10th_percent'] = $ssc_percent;
	// 		$edudata['sed_10th_school_name'] = $ssc_school_name;
	// 		$edudata['sed_12th_passout_year'] = $hsc_pass_year;
	// 		$edudata['sed_12th_percent'] = $hsc_percent;
	// 		$edudata['sed_12th_college_name'] = $hsc_college_name; 
	// 		$edudata['sed_12th_university_name'] = $hsc_university_name; 
	// 		$edudata['sed_12th_specialisation'] = $hsc_specialisation;
	// 		$edudata['sed_diploma_passout_year'] = $diploma_pass_year;
	// 		$edudata['sed_diploma_percent'] = $diploma_percent;
	// 		$edudata['sed_diploma_college_name'] = $diploma_college_name;
	// 		$edudata['sed_diploma_specialisation'] = $diploma_specialisation;
	// 		$edudata['sed_diploma_university'] = $diploma_university_name;
	// 		$edudata['sed_graduate_passout_year'] = $grad_pass_year;
	// 		$edudata['sed_graduate_percent'] = $grad_percent;
	// 		$edudata['sed_graduate_college_name'] = $grad_college_name;
	// 		$edudata['sed_graduate_specialisation'] = $grad_specialisation;
	// 		$edudata['sed_graduate_university'] = $grad_university_name;
	// 		$edudata['sed_achievement'] = $edu_achieve;
	// 		$edudata['updated_at'] = date('Y-m-d h:i:s');
			
	// 		if($this->student_model->updateEducationalData($edudata,$sed_id)){
	// 			$this->session->set_flashdata('success','Educational Details updated Successfully');
	// 			redirect(base_url('student/educational_details'));
	// 		}
	// 	}
	// }
	
		public function educational_details(){
		
		$userdata = $this->session->userdata('logged_in');
		$smdata = $this->student_model->getStudentMasterById($userdata['id']);
		if(!empty($smdata)){
		if($this->input->post()){
			if(!empty($_FILES['10th_certi']['name'])){
				if(is_uploaded_file($_FILES['10th_certi']['tmp_name'])){
					$sourcePath = $_FILES['10th_certi']['tmp_name'];
					$temp = explode(".", $_FILES["10th_certi"]["name"]);
					$newfilename_ten = round(microtime(true)) . '.' . end($temp);
					$targetPath = "uploads/certificate/10th/".$newfilename_ten;
					move_uploaded_file($sourcePath,$targetPath);
				}else{
					$newfilename_ten = '';
				}
			}
			
				if(!empty($_FILES['12th_certi']['name'])){
				if(is_uploaded_file($_FILES['12th_certi']['tmp_name'])){
					$sourcePath = $_FILES['12th_certi']['tmp_name'];
					$temp = explode(".", $_FILES["12th_certi"]["name"]);
					$newfilename_twel = round(microtime(true)) . '.' . end($temp);
					$targetPath = "uploads/certificate/12th/".$newfilename_twel;
					move_uploaded_file($sourcePath,$targetPath);
				}else{
					$newfilename_twel = '';
				}
			}

				if(!empty($_FILES['diploma_cer']['name'])){
				if(is_uploaded_file($_FILES['diploma_cer']['tmp_name'])){
					$sourcePath = $_FILES['diploma_cer']['tmp_name'];
					$temp = explode(".", $_FILES["diploma_cer"]["name"]);
					$newfilename_d = round(microtime(true)) . '.' . end($temp);
					$targetPath = "uploads/certificate/diploma/".$newfilename_d;
					move_uploaded_file($sourcePath,$targetPath);
				}else{
					$newfilename_d= '';
				}
			}

				if(!empty($_FILES['univ_cer']['name'])){
				if(is_uploaded_file($_FILES['univ_cer']['tmp_name'])){
					$sourcePath = $_FILES['univ_cer']['tmp_name'];
					$temp = explode(".", $_FILES["univ_cer"]["name"]);
					$newfilename_u = round(microtime(true)) . '.' . end($temp);
					$targetPath = "uploads/certificate/university/".$newfilename_u;
					move_uploaded_file($sourcePath,$targetPath);
				}else{
					$newfilename_u = '';
				}
			}
			$ssc_pass_year = $this->input->post('ssc_pass_year');
			$ssc_percent = $this->input->post('ssc_percent');
			$ssc_school_name = $this->input->post('ssc_school_name');
			$hsc_pass_year = $this->input->post('hsc_pass_year');
			$hsc_percent = $this->input->post('hsc_percent');
			$hsc_college_name = $this->input->post('hsc_college_name');
			$hsc_university_name = $this->input->post('hsc_university_name');
			$hsc_specialisation = $this->input->post('hsc_specialisation');
			$diploma_pass_year = $this->input->post('diploma_pass_year');
			$diploma_percent = $this->input->post('diploma_percent');
			$diploma_college_name = $this->input->post('diploma_college_name');
			$diploma_university_name = $this->input->post('diploma_university_name');
			$diploma_specialisation = $this->input->post('diploma_specialisation');
			$grad_pass_year = $this->input->post('grad_pass_year');
			$graduate_percent = $this->input->post('grad_percent');
			$grad_college_name = $this->input->post('grad_college_name');
			$grad_university_name = $this->input->post('grad_university_name');
			$grad_specialisation = $this->input->post('grad_specialisation');
			$edu_achieve = $this->input->post('edu_achieve');
			
			$edudata['sed_stm_id'] = $smdata->stm_id;
			$edudata['sed_10th_passout_year'] = $ssc_pass_year;
			$edudata['sed_10th_percent'] = $ssc_percent;
			$edudata['sed_10th_school_name'] = $ssc_school_name;
			$edudata['sed_12th_passout_year'] = $hsc_pass_year;
			$edudata['sed_12th_percent'] = $hsc_percent;
			$edudata['sed_12th_college_name'] = $hsc_college_name; 
			$edudata['sed_12th_university_name'] = $hsc_university_name; 
			$edudata['sed_12th_specialisation'] = $hsc_specialisation;
			$edudata['sed_diploma_passout_year'] = $diploma_pass_year;
			$edudata['sed_diploma_percent'] = $diploma_percent;
			$edudata['sed_diploma_college_name'] = $diploma_college_name;
			$edudata['sed_diploma_specialisation'] = $diploma_specialisation;
			$edudata['sed_diploma_university'] = $diploma_university_name;
			$edudata['sed_graduate_passout_year'] = $grad_pass_year;
			$edudata['sed_graduate_percent'] = $graduate_percent;
			$edudata['sed_graduate_college_name'] = $grad_college_name;
			$edudata['sed_graduate_specialisation'] = $grad_specialisation;
			$edudata['sed_graduate_university'] = $grad_university_name;
			$edudata['sed_achievement'] = $edu_achieve;
			$edudata['tenth_certi'] = $newfilename_ten;
			$edudata['twelth_certi'] = $newfilename_twel;
			$edudata['diploma_certi'] = $newfilename_d;
			$edudata['univer_certi'] = $newfilename_u;
			$edudata['created_at'] = date('Y-m-d h:i:s');
			$edudata['updated_at'] = date('Y-m-d h:i:s');
			
			$sed_id = $this->student_model->insertEducationalData($edudata);
			
			if($sed_id){
				$this->session->set_flashdata('success','Educational Details added Successfully');
				redirect(base_url('student/educational_details'));
			}
		}
		$data['eduData'] = $this->student_model->getEducationalDetails($smdata->stm_id);
		}
		
		$data['courses'] = $this->student_model->getCourseName();
		$data['subview'] = 'student/educational_details';
		$this->load->view('layout_stud_main',$data);
	}
	
	public function edit_educational_details(){
		
		$userdata = $this->session->userdata('logged_in');
		$smdata = $this->student_model->getStudentMasterById($userdata['id']);
	//	print_r($this->input->post());die();
		if($this->input->post()){

			if(!empty($_FILES['10th_certi']['name'])){
				if(is_uploaded_file($_FILES['10th_certi']['tmp_name'])){
					$sourcePath = $_FILES['10th_certi']['tmp_name'];
					$temp = explode(".", $_FILES["10th_certi"]["name"]);
					$newfilename_ten = round(microtime(true)) . '.' . end($temp);
					$targetPath = "uploads/certificate/10th/".$newfilename_ten;
					move_uploaded_file($sourcePath,$targetPath);
				}else{
					$newfilename_ten = '';
				}
			}else{
				
				$newfilename_ten = $this->input->post('10th_certi');
			}
			
				if(!empty($_FILES['12th_certi']['name'])){
				if(is_uploaded_file($_FILES['12th_certi']['tmp_name'])){
					$sourcePath = $_FILES['12th_certi']['tmp_name'];
					$temp = explode(".", $_FILES["12th_certi"]["name"]);
					$newfilename_twel = round(microtime(true)) . '.' . end($temp);
					$targetPath = "uploads/certificate/12th/".$newfilename_twel;
					move_uploaded_file($sourcePath,$targetPath);
				}else{
					$newfilename_twel = '';
				}
			}else{
				
				$newfilename_twel = $this->input->post('12th_certi');
			}

				if(!empty($_FILES['diploma_cer']['name'])){
				if(is_uploaded_file($_FILES['diploma_cer']['tmp_name'])){
					$sourcePath = $_FILES['diploma_cer']['tmp_name'];
					$temp = explode(".", $_FILES["diploma_cer"]["name"]);
					$newfilename_d = round(microtime(true)) . '.' . end($temp);
					$targetPath = "uploads/certificate/diploma/".$newfilename_d;
					move_uploaded_file($sourcePath,$targetPath);
				}else{
					$newfilename_d= '';
				}
			}else{
				
				$newfilename_d = $this->input->post('diploma_cer');
			}

				if(!empty($_FILES['univ_cer']['name'])){
				if(is_uploaded_file($_FILES['univ_cer']['tmp_name'])){
					$sourcePath = $_FILES['univ_cer']['tmp_name'];
					$temp = explode(".", $_FILES["univ_cer"]["name"]);
					$newfilename_u = round(microtime(true)) . '.' . end($temp);
					$targetPath = "uploads/certificate/university/".$newfilename_u;
					move_uploaded_file($sourcePath,$targetPath);
				}else{
					$newfilename_u = '';
				}
			}else{
				
				$newfilename_u = $this->input->post('univ_cer');
			}


			$sed_id = $this->input->post('sed_id');
			$ssc_pass_year = $this->input->post('ssc_pass_year');
			$ssc_percent = $this->input->post('ssc_percent');
			$ssc_school_name = $this->input->post('ssc_school_name');
			$hsc_pass_year = $this->input->post('hsc_pass_year');
			$hsc_percent = $this->input->post('hsc_percent');
			$hsc_college_name = $this->input->post('hsc_college_name');
			$hsc_university_name = $this->input->post('hsc_university_name');
			$hsc_specialisation = $this->input->post('hsc_specialisation');
			$diploma_pass_year = $this->input->post('diploma_pass_year');
			$diploma_percent = $this->input->post('diploma_percent');
			$diploma_college_name = $this->input->post('diploma_college_name');
			$diploma_university_name = $this->input->post('diploma_university_name');
			$diploma_specialisation = $this->input->post('diploma_specialisation');
			$grad_pass_year = $this->input->post('grad_pass_year');
			$graduate_percent = $this->input->post('grad_percent');
			$grad_college_name = $this->input->post('grad_college_name');
			$grad_university_name = $this->input->post('grad_university_name');
			$grad_specialisation = $this->input->post('grad_specialisation');
			$edu_achieve = $this->input->post('edu_achieve');
			
			$edudata['sed_10th_passout_year'] = $ssc_pass_year;
			$edudata['sed_10th_percent'] = $ssc_percent;
			$edudata['sed_10th_school_name'] = $ssc_school_name;
			$edudata['sed_12th_passout_year'] = $hsc_pass_year;
			$edudata['sed_12th_percent'] = $hsc_percent;
			$edudata['sed_12th_college_name'] = $hsc_college_name; 
			$edudata['sed_12th_university_name'] = $hsc_university_name; 
			$edudata['sed_12th_specialisation'] = $hsc_specialisation;
			$edudata['sed_diploma_passout_year'] = $diploma_pass_year;
			$edudata['sed_diploma_percent'] = $diploma_percent;
			$edudata['sed_diploma_college_name'] = $diploma_college_name;
			$edudata['sed_diploma_specialisation'] = $diploma_specialisation;
			$edudata['sed_diploma_university'] = $diploma_university_name;
			$edudata['sed_graduate_passout_year'] = $grad_pass_year;
			$edudata['sed_graduate_percent'] =$graduate_percent;
			$edudata['sed_graduate_college_name'] = $grad_college_name;
			$edudata['sed_graduate_specialisation'] = $grad_specialisation;
			$edudata['sed_graduate_university'] = $grad_university_name;
			$edudata['tenth_certi'] = $newfilename_ten;
			$edudata['twelth_certi'] = $newfilename_twel;
			$edudata['diploma_certi'] = $newfilename_d;
			$edudata['univer_certi'] = $newfilename_u;
						
			$edudata['sed_achievement'] = $edu_achieve;
			$edudata['updated_at'] = date('Y-m-d h:i:s');
			
			if($this->student_model->updateEducationalData($edudata,$sed_id)){
				$this->session->set_flashdata('success','Educational Details updated Successfully');
				redirect(base_url('student/educational_details'));
			}
		}
	}
	
	public function work_details(){
		
		$userdata = $this->session->userdata('logged_in');
		$smdata = $this->student_model->getStudentMasterById($userdata['id']);
		
		if(!empty($smdata)){
			
			if($this->input->post()){
				
				$is_work_experience = $this->input->post('is_work_experience');
				$work_experience = $this->input->post('work_experience');
				$prev_comp_name = $this->input->post('prev_comp_name');
				$prev_comp_desi = $this->input->post('prev_comp_desi');
				$curr_comp_name = $this->input->post('curr_comp_name');
				$curr_comp_desi = $this->input->post('curr_comp_desi');
				
				
				$workdata['swd_stm_id'] = $smdata->stm_id;
				$workdata['swd_is_work_experience'] = $is_work_experience;
				$workdata['swd_work_experience'] = $work_experience.' Months';
				$workdata['swd_prev_comp_name'] = $prev_comp_name;
				$workdata['swd_prev_comp_desi'] = $prev_comp_desi;
				$workdata['swd_curr_comp_name'] = $curr_comp_name;
				$workdata['swd_curr_comp_desi'] = $curr_comp_desi;
				$workdata['created_at'] = date('Y-m-d h:i:s');
				$workdata['updated_at'] = date('Y-m-d h:i:s');
				
				$swd_id = $this->student_model->insertWorkData($workdata);
				
				if($swd_id){
					$this->session->set_flashdata('success','Work Details added Successfully');
					redirect(base_url('student/work_details'));
				}
			}
			$data['workData'] = $this->student_model->getWorkDetails($smdata->stm_id);
		}
		$data['subview'] = 'student/work_details';
		$this->load->view('layout_stud_main',$data);
	}
	
	public function edit_work_details(){
		
		$userdata = $this->session->userdata('logged_in');
		$smdata = $this->student_model->getStudentMasterById($userdata['id']);
		
		if($this->input->post()){
			
			$swd_id = $this->input->post('swd_id');
			$is_work_experience = $this->input->post('is_work_experience');
			$work_experience = $this->input->post('work_experience');
			$prev_comp_name = $this->input->post('prev_comp_name');
			$prev_comp_desi = $this->input->post('prev_comp_desi');
			$curr_comp_name = $this->input->post('curr_comp_name');
			$curr_comp_desi = $this->input->post('curr_comp_desi');
			
			
			$workdata['swd_stm_id'] = $smdata->stm_id;
			$workdata['swd_is_work_experience'] = $is_work_experience;
			$workdata['swd_work_experience'] = $work_experience;
			$workdata['swd_prev_comp_name'] = $prev_comp_name;
			$workdata['swd_prev_comp_desi'] = $prev_comp_desi;
			$workdata['swd_curr_comp_name'] = $curr_comp_name;
			$workdata['swd_curr_comp_desi'] = $curr_comp_desi;
			$workdata['updated_at'] = date('Y-m-d h:i:s');
			
			if($this->student_model->updateWorkData($workdata,$swd_id)){
				$this->session->set_flashdata('success','Work Details updated Successfully');
				redirect(base_url('student/work_details'));
			}
		}
	}
	
	public function course_packages(){
		
		$userdata = $this->session->userdata('logged_in');
		$smdata = $this->student_model->getStudentMasterById($userdata['id']);
		if(!empty($smdata)){
		$data['personalData'] = $this->student_model->getPersonalDetails($smdata->stm_id);
		}
		$data['packagedata'] = $this->student_model->getPackages();
		$data['subview'] = 'student/course_packages';
		$this->load->view('layout_stud_main',$data);
	}
	
	public function course_apply(){
		
		if($this->input->post()){
			
			$user_id = $this->input->post('user_id');
			$course_id = $this->input->post('course_id');
				$userdata = $this->session->userdata('logged_in');
		$smdata = $this->student_model->getStudentMasterById($userdata['id']);
		//print_r($smdata);
		if(!empty($smdata)){
		$personalData= $this->student_model->getPersonalDetails($smdata->stm_id);
		$eduData= $this->student_model->getEducationalDetails($smdata->stm_id);
		}
	 //print_r($personalData);
		if(!empty($personalData)){
			$verify = $this->db->get_where('users',array('id'=>$user_id));
			$emailverify=$verify->result();
			//echo $emailverify[0]->email_confirmed;die;
					if($emailverify[0]->email_confirmed!='NO'){
						//echo 'ok';
						//echo json_encode(array('msg' => 'ok'));
					$smdata = $this->student_model->getStudentMasterById($user_id);
					$userid=$smdata->stm_id;
					$chk = $this->student_model->check_applied($userid,$course_id);
					if($chk==false){
					$app_data['sa_stm_id'] = $smdata->stm_id;
					$app_data['sa_uc_id'] = $course_id;
					$app_data['created_at'] = date('Y-m-d h:i:s');
					$app_data['updated_at'] = date('Y-m-d h:i:s');
					
					
					$sa_id = $this->student_model->insertAppliedCourse($app_data);
					
					if($sa_id){
						
						$app_statusdata['as_sa_id'] = $sa_id;
						$app_statusdata['as_stm_id'] = $smdata->stm_id;
						$app_statusdata['as_app_type'] = 'Apply';
						$app_statusdata['as_status'] = 1;
						$app_statusdata['created_at'] = date('Y-m-d h:i:s');
						$app_statusdata['updated_at'] = date('Y-m-d h:i:s');
						
						$this->student_model->insertAppliedStatus($app_statusdata);
						//echo 'Course Applied Successfully';	
						echo json_encode(array('msg' => 'Course Applied Successfully'));
						//$this->session->set_flashdata('success','Course Applied Successfully');
						//redirect(base_url('dashboard/student'));
					}
					}
						else{
							//echo 'Already Applied';
							echo json_encode(array('msg' => 'Already Applied'));
						}
					}else{
						//echo 'Please verify your email address';
						echo json_encode(array('msg' => 'Please verify your email address'));
					}
			}
			else{
				echo json_encode(array('msg' => 'Please Update your profile'));
					//$this->session->set_flashdata('Please Update your profile first');
			}	
				
				
		}
	}
	
	public function course_shortlist(){
		
		  
		if($this->input->post()){
			
			 $user_id = $this->input->post('user_id');
			$course_id = $this->input->post('course_id');
			$verify = $this->db->get_where('users',array('id'=>$user_id));
			$emailverify=$verify->result();
			//echo $emailverify[0]->email_confirmed;
			if($emailverify[0]->email_confirmed!='NO'){
			$smdata = $this->student_model->getStudentMasterById($user_id);
			$userid=$smdata->stm_id;
			$chk = $this->student_model->check_shorlist($userid,$course_id);
			if($chk==false){
				//echo 'tes';
			$app_data['sc_stm_id'] = $smdata->stm_id;
			$app_data['sc_uc_id'] = $course_id;
			$app_data['created_at'] = date('Y-m-d h:i:s');
			$app_data['updated_at'] = date('Y-m-d h:i:s');
			
				
			$sc_id = $this->student_model->insertShortlistCourse($app_data);
			
			if($sc_id){
				
				$app_statusdata['as_sa_id'] = $sc_id;
				$app_statusdata['as_stm_id'] = $smdata->stm_id;
				$app_statusdata['as_app_type'] = 'Shortlist';
				$app_statusdata['as_status'] = 1;
				$app_statusdata['created_at'] = date('Y-m-d h:i:s');
				$app_statusdata['updated_at'] = date('Y-m-d h:i:s');
				
				$this->student_model->insertShortlistStatus($app_statusdata);
				
			//	echo 'Shotrlisted successfully';
				// /$this->session->set_flashdata('success','Course Shortlisted Successfully');
				echo json_encode(array('msg' => 'Course Shortlisted Successfully'));
			}
		
		}
		else{
			echo json_encode(array('msg' => 'Already Shortlisted'));
		}
		}else{
				//echo 'Please verify your email address';
				echo json_encode(array('msg' => 'Please verify your email address'));
			}
	}
	}
	
	
	public function getAjaxState(){
		$cntId = $this->input->post('country');
		$statedata = $this->student_model->getStateByCountry($cntId);
		echo json_encode($statedata);
	}
	public function getAjaxCity(){
		$stsId = $this->input->post('state');
		$citydata = $this->student_model->getCityByState($stsId);
		echo json_encode($citydata);
	}
	
	public function email_verify(){
		$id = $this->input->post('id');
		$email = $this->input->post('email');
		$name = $this->input->post('name');
		$url = $this->input->post('url');
		//print_r($email);
		$subject = 'Verify Email';
		
        $message = 'Dear '.$name.',<br/><br/> Please click on the below activation link to verify your email address.<br/><br/> <a href="'.base_url('login/email_verify/'.md5($email).'/' . base64_encode($id)).'/'.base64_encode($url).')">Click Here</a> <br/><br/><br/> Thanks <br/> Hello Admission Team';
		
		$config['protocol']    = 'smtp';
		$config['smtp_host']    = 'ssl://smtp.gmail.com';
		$config['smtp_port']    = '465';
		$config['smtp_timeout'] = '7';
		$config['smtp_user']    = 'noreply@talentedgenext.com';
		$config['smtp_pass']    = 'Mohammed@1292';
		$config['charset']    = 'utf-8';
		$config['newline']    = "\r\n";
		$config['mailtype'] = 'html';

		$this->email->initialize($config);
		
        $this->email->from('noreply@talentedgenext.com', 'HELLO ADMISSION');
        $this->email->to($email);
        $this->email->subject($subject);
        $this->email->message($message);
        if($this->email->send()){
        	echo json_encode(array('msg'=>'Please check your email at to verify the email address'));
        }
			   //print_r($sess_array);
			//$this->session->set_userdata('chpass', $sess_array);
			
			
	}

	//Testimonial

	public function testimonial()
	{
		$data['subview'] = 'student/testimonial';
		$this->load->view('layout_stud_main',$data);
	}
	public function testimonial_list()
	{
		$id=$this->input->post('id');
		$data=$this->student_model->get_testimonial_list($id);
		echo json_encode($data);
	}

	public function add_testimonial()
	{
		
		$title=$this->input->post('title');
		$message=$this->input->post('message');
		$stuid=$this->input->post('stuid');
		$testi=array(
			'title'=>$title,
			'testimonial'=>$message,
			'student_id'=>$stuid
			);
		$result=$this->student_model->insert_testimonial($testi);
		if($result){
			echo json_encode(array('msg'=>'successfully'));
		}
	}

	public function update_testimonial()
	{
		$id=$this->input->post('id');
		$title=$this->input->post('title');
		$message=$this->input->post('message');
		//$stuid=$this->input->post('stuid');
		$testi=array(
			'title'=>$title,
			'testimonial'=>$message
			
			);
		$result=$this->student_model->update_testimonials($id,$testi);
		if($result){
			echo json_encode(array('msg'=>'successfully'));
		}
	}
	public function testimonial_edit(){

	$id=$this->input->post('id');
	$data=$this->student_model->get_t_data($id);
	echo json_encode($data);
	}
	public function testimonial_active(){

	$id=$this->input->post('id');
	$state=array('status'=>'0');
	$data=$this->student_model->active_testimonials($id,$state);
	echo json_encode($data);
	}
}
