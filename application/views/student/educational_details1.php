<style>
a.active {
    background: #fff;
}
.tab-icon{
	font-size: 28px;
    vertical-align: middle;
}
.admission-form-remove-fields{
	padding-top: 10px;
}
.file-field input[type=file] {
    width: 77%;
}
label {
    color: #000000;
}
</style>
           
<section id="content"> 
	<div class="row"  id="side-menu-three-div">
		<div class="col s12 m8 l8 offset-l3 offset-m4">
			<div class="row row-margin-bottom">
				<div class="Space30 col l12 m12 s12"></div>
				<div class="col s12 m12 l12">
				<h4 class="admission-form-heading-margin">Educational Details</h4>
				</div>
				<div class="col s12 m3 l3"></div>	
			</div>	
		</div>

		<div class="col s12 m12 l12" style="margin-top: -6.8px;">	
		 <div class="card" style="height:600px">
			<div class="col s12 l12 m12 Space40"></div>
				<div class="col s12 m12 l12">
					<?php if(!isset($eduData)){ ?>
				<form action="<?php echo base_url('student/educational_details'); ?>" method="post" id="saveForm">
				<?php }else{ ?>
				<form action="<?php echo base_url('student/edit_educational_details'); ?>" method="post" id="editForm">
				<?php } ?>
			
					<div class="row">
					    <div class="input-field col s3">
					     
					      <?php $yearArray = range(2000, 2050); ?>
										<select class="browser-default" name="ssc_pass_year" id="ssc_pass_year" style="margin-top: 17px;">
											<option value="">Select Year</option>
											<?php
											foreach ($yearArray as $year) {
												if($eduData->sed_10th_passout_year == $year){
													$selected = 'selected';	
												}else{
													$selected = '';
												}
												echo '<option value="'.$year.'" '.$selected.'>'.$year.'</option>';
											}
											?>
										</select>
					      	<label class="10th Passing Year" style="margin-top: -16px;">10th Passing Year</label>
					    </div>
					    <div class="input-field col s3">
					          	<?php $perArray = range(35, 100); ?>
										<select class="browser-default" name="ssc_percent" id="ssc_percent" style="margin-top: 17px;">
											<option value="">Select Percentage</option>
											<?php
											foreach ($perArray as $percentage) {
												
												if($eduData->sed_10th_percent == $percentage){
													$selected = 'selected';	
												}else{
													$selected = '';
												}
												echo '<option value="'.$percentage.'%" '.$selected.'>'.$percentage.'%</option>';
											}
											?>
										</select>
					      	<label class="10th Passing Year" style="margin-top: -16px;">10th Percentage (%)</label>
					    </div>
					    <div class="col s3">
					    	<label class="10th Passing Year" style="margin-top: -32px;">10th School Name</label>
					    	<input type="text" name="ssc_school_name" id="ssc_school_name" value="<?php echo isset($eduData->sed_10th_school_name) ? $eduData->sed_10th_school_name : '';?>" style="margin-top: 14px;">
					     
					    </div>
					     <div class="input-field col s3">
					     
					    </div>
					  </div>
					  <br>
					  <div class="rows">
					  	 <div class="input-field col s3">
					     <?php $yearArray = range(2000, 2050); ?>
										<select class="browser-default" name="hsc_pass_year" id="hsc_pass_year" style="margin-top:16px">
											<option value="">Select Year</option>
											<?php
											foreach ($yearArray as $year) {
												
												if($eduData->sed_12th_passout_year == $year){
													$selected = 'selected';	
												}else{
													$selected = '';
												}
												
												echo '<option '.$selected.' value="'.$year.'">'.$year.'</option>';
											}
											?>
										</select>
						<label class="10th Passing Year" style="margin-top: -16px;">12th Passing Year</label>
					    </div>
					     <div class="input-field col s3">
					     <?php $perArray = range(35, 100); ?>
										<select class="browser-default" name="hsc_percent" id="hsc_percent" style="margin-top:16px">
											<option value="">Select Percentage</option>
											<?php
											foreach ($perArray as $percentage) {
												
												if($eduData->sed_12th_percent == $percentage){
													$selected = 'selected';	
												}else{
													$selected = '';
												}
												
												echo '<option '.$selected.' value="'.$percentage.'%">'.$percentage.'%</option>';
											}
											?>
										</select>
						<label class="10th Passing Year" style="margin-top: -16px;">12th Percentage (%)</label>
					    </div>
					    <div class="col s2">
					    	<label class="10th Passing Year" style="margin-top: -32px;">12th College Name</label>
					     	<input type="text" name="hsc_college_name" id="hsc_college_name" value="<?php echo isset($eduData->sed_12th_college_name) ? $eduData->sed_12th_college_name : '';?>" style="margin-top: 14px;">
					    </div>
					    <div class="col s2">
					    	<label class="10th Passing Year" style="margin-top: -32px;">12th University Name</label>
					    	<input type="text" name="hsc_university_name" id="hsc_university_name" value="<?php echo isset($eduData->sed_12th_university_name) ? $eduData->sed_12th_university_name : '';?>" style="margin-top: 14px;">
					     	
					    </div>
					  </div>
					  <br>
					  <br>
					    <div class="rows">
					  	 <div class="input-field col s3">
					    <?php $yearArray = range(2000, 2050); ?>
										<select class="browser-default" name="diploma_pass_year" id="diploma_pass_year" style="margin-top:38px">
											<option value="">Select Year</option>
											<?php
											foreach ($yearArray as $year) {
												
												if($eduData->sed_diploma_passout_year == $year){
													$selected = 'selected';	
												}else{
													$selected = '';
												}
												
												echo '<option '.$selected.' value="'.$year.'">'.$year.'</option>';
											}
											?>
										</select>
						<label class="10th Passing Year" style="margin-top: 0px;">Diploma Passing Year</label>
					    </div>
					     <div class="input-field col s3">
					    <?php $perArray = range(35, 100); ?>
										<select class="browser-default" name="diploma_percent" id="diploma_percent" style="margin-top:38px">
											<option value="">Select Percentage</option>
											<?php
											foreach ($perArray as $percentage) {
												
												if($eduData->sed_diploma_percent == $percentage){
													$selected = 'selected';	
												}else{
													$selected = '';
												}
												echo '<option '.$selected.' value="'.$percentage.'%">'.$percentage.'%</option>';
											}
											?>
										</select>
						<label class="10th Passing Year" style="margin-top: 5px;">Diploma Percentage (%)</label>
					    </div>
					    <div class="col s2" style="margin-top: 21px;">
					    	<label class="10th Passing Year" style="margin-top: -32px;">Diploma College Name</label>
					     	
					     	<input type="text" name="diploma_college_name" id="diploma_college_name" value="<?php echo isset($eduData->sed_diploma_college_name) ? $eduData->sed_diploma_college_name : '';?>" style="margin-top: 14px;" >
					    </div>
					    <div class="col s2" style="margin-top: 21px;">
					    	<label class="10th Passing Year" style="margin-top: -32px;">Diploma University Name</label>
					    
					     	<input type="text" name="diploma_university_name" id="diploma_university_name"  value="<?php echo isset($eduData->sed_diploma_university) ? $eduData->sed_diploma_university : '';?>" style="margin-top: 14px;">
					    </div>
					    <div class="col s2" style="margin-top: 21px;">
					    	<label class="10th Passing Year" style="margin-top: -32px;">Diploma Specialization </label>
					    
					     	
					     	<input type="text" name="diploma_specialisation" id="diploma_specialisation" value="<?php echo isset($eduData->sed_diploma_specialisation) ? $eduData->sed_diploma_specialisation : '';?>" style="margin-top: 14px;">
					    </div>
					  </div>

					 <br>
<!--Graduation-->
 <div class="rows">
					  	 <div class="input-field col s3">
					    <?php $yearArray = range(2000, 2050); ?>
											<?php $yearArray = range(2000, 2050); ?>
										<select class="browser-default" name="grad_pass_year" id="grad_pass_year" style="margin-top:37px">
											<option value="">Select Year</option>
											<?php
											foreach ($yearArray as $year) {
												if($eduData->sed_graduate_passout_year == $year){
													$selected = 'selected';	
												}else{
													$selected = '';
												}
												echo '<option '.$selected.' value="'.$year.'">'.$year.'</option>';
											}
											?>
										</select>
						<label class="10th Passing Year" style="margin-top: -0px;">Graduation Passing Year</label>
					    </div>
					     <div class="input-field col s3">
					    <?php $perArray = range(35, 100); ?>
										<select class="browser-default" name="grad_percent" id="grad_percent" style="margin-top:37px">
											<option value="">Select Percentage</option>
											<?php
											foreach ($perArray as $percentage) {
												
												if($eduData->sed_graduate_percent == $percentage){
													$selected = 'selected';	
												}else{
													$selected = '';
												}
												echo '<option '.$selected.' value="'.$percentage.'%">'.$percentage.'%</option>';
											}
											?>
										</select>
						<label class="10th Passing Year" style="margin-top: 8px;">Graduation Percentage (%)</label>
					    </div>
					    <div class="col s2" style="margin-top:19px">
					    	<label class="10th Passing Year" style="margin-top: -32px;">Graduation College Name</label>
					     	<input type="text" name="grad_college_name" id="grad_college_name" value="<?php echo isset($eduData->sed_graduate_college_name) ? $eduData->sed_graduate_college_name : '';?>" style="margin-top: 14px;">
					     	
					    </div>
					    <div class="col s2" style="margin-top:19px">
					    	<label class="10th Passing Year" style="margin-top: -32px;">Graduation University Name</label>
					    <input type="text" name="grad_university_name" id="grad_university_name" value="<?php echo isset($eduData->sed_graduate_university) ? $eduData->sed_graduate_university : '';?>" style="margin-top: 14px;">
					     
					    </div>
					    <div class="col s2" style="margin-top:19px">
					    	<label class="10th Passing Year" style="margin-top: -32px;">Graduation Specialization</label>
					    <input type="text" name="grad_specialisation" id="grad_specialisation" value="<?php echo isset($eduData->sed_graduate_specialisation) ? $eduData->sed_graduate_specialisation : '';?>" style="margin-top: 14px;">
					     
					    </div>
					  </div>

					  <div class="rows">
					  	 <div class="input-field col s3">
					    
					    </div>
					     <div class="input-field col s3">
					    
					    </div>
					    <div class="col s2">
					    	<label class="10th Passing Year" style="margin-top: -32px;">Educational Achievements</label>
					    	<input type="text" name="edu_achieve" id="edu_achieve" value="<?php echo isset($eduData->sed_achievement) ? $eduData->sed_achievement : '';?>" style="margin-top: 14px;">
					    
					     	
					    </div>
					    
					  </div>
					  <div class="rows">
					  	 <div class="input-field col s6">
					   <?php if(!isset($eduData)){ ?>
								<button type="button" class="waves-effect waves-light btn btn-form-submit Searchbtn center-align save" style="margin-top: 100px; align: center; margin-left: 407px;">Add</button>

								<div class="input-field col s6"></div>
								<?php }else{ ?>
								<input type="hidden" name="sed_id" value="<?php echo $eduData->sed_id; ?>">
								<button type="button" class="waves-effect waves-light btn btn-form-submit Searchbtn center-align update" style="margin-top: 100px; align: center; margin-left: 407px;">Update</button>
								<div class="input-field col s6"></div>
								<?php } ?>
					    </div>
					    <a type="button" href="<?php echo base_url('dashboard/student')?>" class="waves-effect waves-light btn btn-form-submit Searchbtn center-align " style="margin-top: 100px; align: center; margin-left: -113px;">Cancel</a>
					  </div>
</form>
					
				</div>
			</div>
			<!--div class="card" >
				<div class="col s12 l12 m12 Space40"></div>
				<!--div class="col s12 m12 l12">
				
					<div class="row">
						<div class="row" id="formfielddiv">
							<div class="col s12 m4 l4">

								<div class="row">
									<div class="col s12 m4 l4">
										<label class="10th Passing Year">10th Passing Year</label>
									</div>
									<div class="col s10 m6 l6 no-padding-origin">
										<?php $yearArray = range(2000, 2050); ?>
										<select class="browser-default" name="ssc_pass_year" id="ssc_pass_year">
											<option value="">Select Year</option>
											<?php
											foreach ($yearArray as $year) {
												if($eduData->sed_10th_passout_year == $year){
													$selected = 'selected';	
												}else{
													$selected = '';
												}
												echo '<option value="'.$year.'" '.$selected.'>'.$year.'</option>';
											}
											?>
										</select>
									</div>
								</div>
							</div>

							<div class="col s12 m4 l4">
								<div class="row">
									<div class="col s12 m4 l4">
										<label class="10th Percentage (%)">10th Percentage (%)</label>
									</div>
									<div class="col s10 m6 l4 no-padding-origin">
										<?php $perArray = range(35, 100); ?>
										<select class="browser-default" name="ssc_percent" id="ssc_percent">
											<option value="">Select Year</option>
											<?php
											foreach ($perArray as $percentage) {
												
												if($eduData->sed_10th_percent == $percentage){
													$selected = 'selected';	
												}else{
													$selected = '';
												}
												echo '<option value="'.$percentage.'%" '.$selected.'>'.$percentage.'%</option>';
											}
											?>
										</select>
									</div>
								</div>
							</div>
							<div class="col s12 m4 l4">
								<div class="row">
									<div class="col s12 m4 l4">
										<label class="10th School Name">10th School Name</label>
									</div>
									<div class="col s10 m6 l6 no-padding-origin">
										<input type="text" name="ssc_school_name" id="ssc_school_name" value="<?php echo isset($eduData->sed_10th_school_name) ? $eduData->sed_10th_school_name : '';?>">
									</div>
								</div>
							</div>

							<div class="col s12 m4 l4">
								<div class="row">
									<div class="col s12 m4 l4">
										<label class="12th Passing Year">12th Passing Year</label>
									</div>
									<div class="col s10 m6 l6 no-padding-origin">
										<?php $yearArray = range(2000, 2050); ?>
										<select class="browser-default" name="hsc_pass_year" id="hsc_pass_year">
											<option value="">Select Year</option>
											<?php
											foreach ($yearArray as $year) {
												
												if($eduData->sed_12th_passout_year == $year){
													$selected = 'selected';	
												}else{
													$selected = '';
												}
												
												echo '<option '.$selected.' value="'.$year.'">'.$year.'</option>';
											}
											?>
										</select>
									</div>
								</div>

							</div>

							<div class="col s12 m4 l4">
								<div class="row">
									<div class="col s12 m4 l4">
										<label class="12th Percentage (%)">12th Percentage (%)</label>
									</div>
									<div class="col s10 m6 l6 no-padding-origin">
										<?php $perArray = range(35, 100); ?>
										<select class="browser-default" name="hsc_percent" id="hsc_percent">
											<option value="">Select Year</option>
											<?php
											foreach ($perArray as $percentage) {
												
												if($eduData->sed_12th_percent == $percentage){
													$selected = 'selected';	
												}else{
													$selected = '';
												}
												
												echo '<option '.$selected.' value="'.$percentage.'%">'.$percentage.'%</option>';
											}
											?>
										</select>
									</div>
								</div>
							</div>

							<div class="col s12 m4 l4">
								<div class="row">
									<div class="col s12 m4 l4">
										<label class="12th College Name">12th College Name</label>
									</div>
									<div class="col s10 m6 l6 no-padding-origin">
										<input type="text" name="hsc_college_name" id="hsc_college_name" value="<?php echo isset($eduData->sed_12th_college_name) ? $eduData->sed_12th_college_name : '';?>" >
									</div>
								</div>
							</div>

							<div class="col s12 m4 l4">
								<div class="row">
									<div class="col s12 m4 l4">
										<label class="12th University Name">12th University Name</label>
									</div>
									<div class="col s10 m6 l6 no-padding-origin">
										<input type="text" name="hsc_university_name" id="hsc_university_name" value="<?php echo isset($eduData->sed_12th_university_name) ? $eduData->sed_12th_university_name : '';?>" >
									</div>
								</div>
							</div>
							
							<div class="col s12 m4 l4">
								<div class="row">
									<div class="col s12 m4 l4">
										<label class="Diploma Passing Year">Diploma Passing Year</label>
									</div>
									<div class="col s10 m6 l6 no-padding-origin">
										<?php $yearArray = range(2000, 2050); ?>
										<select class="browser-default" name="diploma_pass_year" id="diploma_pass_year">
											<option value="">Select Year</option>
											<?php
											foreach ($yearArray as $year) {
												
												if($eduData->sed_diploma_passout_year == $year){
													$selected = 'selected';	
												}else{
													$selected = '';
												}
												
												echo '<option '.$selected.' value="'.$year.'">'.$year.'</option>';
											}
											?>
										</select>
									</div>
								</div>
							</div>
							<div class="col s12 m4 l4">
								<div class="row">
									<div class="col s12 m4 l4">
										<label class="Diploma Percentage (%)">Diploma Percentage (%)</label>
									</div>
									<div class="col s10 m6 l6 no-padding-origin">
										<?php $perArray = range(35, 100); ?>
										<select class="browser-default" name="diploma_percent" id="diploma_percent">
											<option value="">Select Year</option>
											<?php
											foreach ($perArray as $percentage) {
												
												if($eduData->sed_diploma_percent == $percentage){
													$selected = 'selected';	
												}else{
													$selected = '';
												}
												echo '<option '.$selected.' value="'.$percentage.'%">'.$percentage.'%</option>';
											}
											?>
										</select>
									</div>
								</div>
							</div>

							<div class="col s12 m4 l4">
								<div class="row">
									<div class="col s12 m4 l4">
										<label class="Diploma College Name">Diploma College Name</label>
									</div>
									<div class="col s10 m6 l6 no-padding-origin">
										<input type="text" name="diploma_college_name" id="diploma_college_name" value="<?php echo isset($eduData->sed_diploma_college_name) ? $eduData->sed_diploma_college_name : '';?>" >
									</div>
								</div>
							</div>
							<div class="col s12 m4 l4">
								<div class="row">
									<div class="col s12 m4 l4">
										<label class="Diploma University Name">Diploma University Name</label>
									</div>
									<div class="col s10 m6 l6 no-padding-origin">
										<input type="text" name="diploma_university_name" id="diploma_university_name"  value="<?php echo isset($eduData->sed_diploma_university) ? $eduData->sed_diploma_university : '';?>">
									</div>
								</div>
							</div>
							<div class="col s12 m4 l4">
								<div class="row">
									<div class="col s12 m4 l4">
										<label class="Diploma Specialization">Diploma Specialization</label>
									</div>
									<div class="col s10 m6 l6 no-padding-origin">
										<input type="text" name="diploma_specialisation" id="diploma_specialisation" value="<?php echo isset($eduData->sed_diploma_specialisation) ? $eduData->sed_diploma_specialisation : '';?>">
									</div>
									
								</div>
							</div>
						</div>
							<hr>
							<div class="col s12 m4 l4" id="frmf-29">
								<div class="row">
									<div class="col s12 m4 l4">
										<label class="Graduation Passing Year">Graduation Passing Year</label>
									</div>
									<div class="col s10 m6 l6 no-padding-origin">
										<?php $yearArray = range(2000, 2050); ?>
										<select class="browser-default" name="grad_pass_year" id="grad_pass_year">
											<option value="">Select Year</option>
											<?php
											foreach ($yearArray as $year) {
												if($eduData->sed_graduate_passout_year == $year){
													$selected = 'selected';	
												}else{
													$selected = '';
												}
												echo '<option '.$selected.' value="'.$year.'">'.$year.'</option>';
											}
											?>
										</select>
									</div>
									
								</div>
							</div>
							<div class="col s12 m4 l4">
								<div class="row">
									<div class="col s12 m4 l4">
										<label class="Graduation Percentage (%)">Graduation Percentage (%)</label>
									</div>
									<div class="col s10 m6 l6 no-padding-origin">
										<?php $perArray = range(35, 100); ?>
										<select class="browser-default" name="grad_percent" id="grad_percent">
											<option value="">Select Year</option>
											<?php
											foreach ($perArray as $percentage) {
												
												if($eduData->sed_graduate_percent == $percentage){
													$selected = 'selected';	
												}else{
													$selected = '';
												}
												echo '<option '.$selected.' value="'.$percentage.'%">'.$percentage.'%</option>';
											}
											?>
										</select>
									</div>
								</div>
							</div>
							<div class="col s12 m4 l4">
								<div class="row">
									<div class="col s12 m4 l4">
										<label class="Graduation College Name">Graduation College Name</label>
									</div>
									<div class="col s10 m6 l6 no-padding-origin">
										<input type="text" name="grad_college_name" id="grad_college_name" value="<?php echo isset($eduData->sed_graduate_college_name) ? $eduData->sed_graduate_college_name : '';?>" >
									</div>
								</div>
							</div>
							<div class="col s12 m4 l4">
								<div class="row">
									<div class="col s12 m4 l4">
										<label class="Graduation University Name">Graduation University Name</label>
									</div>
									<div class="col s10 m6 l6 no-padding-origin">
										<input type="text" name="grad_university_name" id="grad_university_name" value="<?php echo isset($eduData->sed_graduate_university) ? $eduData->sed_graduate_university : '';?>">
									</div>
								</div>
							</div>
							<div class="col s12 m4 l4">
								<div class="row">
									<div class="col s12 m4 l4">
										<label class="Graduation Specialization">Graduation Specialization</label>
									</div>
									<div class="col s10 m6 l6 no-padding-origin">
										<input type="text" name="grad_specialisation" id="grad_specialisation" value="<?php echo isset($eduData->sed_graduate_specialisation) ? $eduData->sed_graduate_specialisation : '';?>">
									</div>
								</div>
							</div>
							<div class="col s12 m4 l4" id="frmf-34">
								<div class="row">
									<div class="col s12 m4 l4">
										<label class="Educational Achievements">Educational Achievements</label>
									</div>
									<div class="col s10 m6 l6 no-padding-origin">
										<input type="text" name="edu_achieve" id="edu_achieve" value="<?php echo isset($eduData->sed_achievement) ? $eduData->sed_achievement : '';?>" >
									</div>
								</div>
							</div>
							<!--<div class="col s12 m4 l4">
								<div class="row">
									<div class="col s12 m4 l4">
										<label class="Course (Looking for)">Course (Looking for)</label>
									</div>
									<div class="col s10 m6 l6 no-padding-origin">
										<select class="browser-default Ustory" name="course">
											<option value="" disabled="" selected="">Course (Looking for)</option>
											<?php foreach($courses as $course){ ?>
											<option value="<?php echo $course->com_name; ?>"><?php echo $course->com_name; ?></option>
											<?php } ?>
										</select>
									</div>
								</div>
							</div>
							<div class="col s12 m4 l4">
								<div class="row">
									<div class="col s12 m4 l4">
										<label class="Highest Qualification">Highest Qualification</label>
									</div>
									<div class="col s10 m6 l6 no-padding-origin">
										<select class="browser-default Ustory" name="high_quali">
											<option value="" disabled="" selected="">Highest Qualification</option>
										</select>
									</div>
								</div>
							</div>-->
							<!--div class="row">
								<div class="col s12 m12 l12 center-align">
									<?php if(!isset($eduData)){ ?>
								<button type="button" class="waves-effect waves-light btn btn-form-submit Searchbtn center-align save">Add</button>
								<div class="col s12 m12 l12 Space10"></div>
								<?php }else{ ?>
								<input type="hidden" name="sed_id" value="<?php echo $eduData->sed_id; ?>">
								<button type="button" class="waves-effect waves-light btn btn-form-submit Searchbtn center-align update">Update</button>
								<div class="col s12 m12 l12 Space10"></div>
								<?php } ?>
								</div>
							</div>
						</div-->
					</div>
				</form>
				</div>
			</div>					
		</div>
	</div>
</section>
<script type="text/javascript" src="<?php echo base_url('assets/js/plugins/jquery-1.11.2.min.js');?>"></script>
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/1.3.1/js/toastr.js"></script>
<script>


$('.update').click(function(){
	 var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	 var phoneno = /^(([1-9]*)|(([1-9]*).([0-9]*)))$/;  
	 var s=$('#mobile').val();
	 console.log(s);

if($('#ssc_pass_year').val()==""){
	$('#ssc_pass_year').focus();
	toastr.error('Select SSC Passing year');
	return false;
}else if($('#ssc_percent').val()==""){
	$('#ssc_percent').focus();
	toastr.error('Select SSC Percentage ');
	return false;
}else if($('#ssc_school_name').val()==""){
	$('#ssc_school_name').focus();
	toastr.error('Enter Ssc school name');
	return false;
}else if($('#hsc_pass_year').val()==""){
	$('#hsc_pass_year').focus();
	toastr.error('Select hsc Passing year');
	return false;
}else if($('#hsc_percent').val()==""){
	$('#hsc_percent').focus();
	toastr.error('Select hsc Percentage');
	return false;
}else if($('#hsc_college_name').val()==""){
	$('#hsc_college_name').focus();
	toastr.error('Enter hsc college name');
	return false;
}else if($('#hsc_university_name').val()==""){
	$('#hsc_university_name').focus();
	toastr.error('Enter hsc university name');
	return false;
}else if($('#diploma_pass_year').val()==""){
	$('#diploma_pass_year').focus();
	toastr.error('Select diploma passing year');
	return false;
}else if($('#diploma_percent').val()==""){
	$('#diploma_percent').focus();
	toastr.error('Select diploma percent');
	return false;
}else if($('#diploma_college_name').val()==""){
	$('#diploma_college_name').focus();
	toastr.error('Enter diploma college name');
	return false;
}else if($('#diploma_university_name').val()==""){
	$('#diploma_university_name').focus();
	toastr.error('Enter diploma university name');
	return false;
}else if($('#diploma_specialisation').val()==""){
	$('#diploma_specialisation').focus();
	toastr.error('Enter diploma diploma_specialisation');
	return false;
}else if($('#grad_pass_year').val()==""){
	$('#grad_pass_year').focus();
	toastr.error('Select grad pass year');
	return false;
}else if($('#grad_percent').val()==""){
	$('#grad_percent').focus();
	toastr.error('Select grad percent');
	return false;
}else if($('#grad_college_name').val()==""){
	$('#grad_college_name').focus();
	toastr.error('Enter grad college name');
	return false;
}else if($('#grad_university_name').val()==""){
	$('#grad_university_name').focus();
	toastr.error('Enter grad university name');
	return false;
}else if($('#grad_specialisation').val()==""){
	$('#grad_specialisation').focus();
	toastr.error('Enter diploma grad specialisation');
	return false;
}else if($('#edu_achieve').val()==""){
	$('#edu_achieve').focus();
	toastr.error('Enter education achievement');
	return false;
}else{
	toastr.success('Update Successfully');

	$('#editForm').submit();
}

	//alert('ih');






	
});

$('.save').click(function(){
if($('#ssc_pass_year').val()==""){
	$('#ssc_pass_year').focus();
	toastr.error('Select SSC Passing year');
	return false;
}else if($('#ssc_percent').val()==""){
	$('#ssc_percent').focus();
	toastr.error('Select SSC Percentage ');
	return false;
}else if($('#ssc_school_name').val()==""){
	$('#ssc_school_name').focus();
	toastr.error('Enter Ssc school name');
	return false;
}else if($('#hsc_pass_year').val()==""){
	$('#hsc_pass_year').focus();
	toastr.error('Select hsc Passing year');
	return false;
}else if($('#hsc_percent').val()==""){
	$('#hsc_percent').focus();
	toastr.error('Select hsc Percentage');
	return false;
}else if($('#hsc_college_name').val()==""){
	$('#hsc_college_name').focus();
	toastr.error('Enter hsc college name');
	return false;
}else if($('#hsc_university_name').val()==""){
	$('#hsc_university_name').focus();
	toastr.error('Enter hsc university name');
	return false;
}else if($('#diploma_pass_year').val()==""){
	$('#diploma_pass_year').focus();
	toastr.error('Select diploma passing year');
	return false;
}else if($('#diploma_percent').val()==""){
	$('#diploma_percent').focus();
	toastr.error('Select diploma percent');
	return false;
}else if($('#diploma_college_name').val()==""){
	$('#diploma_college_name').focus();
	toastr.error('Enter diploma college name');
	return false;
}else if($('#diploma_university_name').val()==""){
	$('#diploma_university_name').focus();
	toastr.error('Enter diploma university name');
	return false;
}else if($('#diploma_specialisation').val()==""){
	$('#diploma_specialisation').focus();
	toastr.error('Enter diploma diploma_specialisation');
	return false;
}else if($('#grad_pass_year').val()==""){
	$('#grad_pass_year').focus();
	toastr.error('Select grad pass year');
	return false;
}else if($('#grad_percent').val()==""){
	$('#grad_percent').focus();
	toastr.error('Select grad percent');
	return false;
}else if($('#grad_college_name').val()==""){
	$('#grad_college_name').focus();
	toastr.error('Enter grad college name');
	return false;
}else if($('#grad_university_name').val()==""){
	$('#grad_university_name').focus();
	toastr.error('Enter grad university name');
	return false;
}else if($('#grad_specialisation').val()==""){
	$('#grad_specialisation').focus();
	toastr.error('Enter diploma grad specialisation');
	return false;
}else if($('#edu_achieve').val()==""){
	$('#edu_achieve').focus();
	toastr.error('Enter education achievement');
	return false;
}else{
	toastr.success('Update Successfully');

	$('#saveForm').submit();
}

	//alert('ih');
	
});

$('#country').change(function(){
	var country = $(this).val();
	$('#state').empty();
	$.ajax({
		type: "POST",
		url: "<?php echo base_url('student/getAjaxState'); ?>",
		data:{country:country},
		success: function(response){
			var res = JSON.parse(response);
			$('#state').append('<option value="" disabled selected>Select State</option>');
			$.each(res, function(key, value) {   
				 $('#state')
					 .append($("<option></option>")
								.attr("value",value.sm_id)
								.text(value.sm_name)); 
			});
		}
	});
});

$('#state').change(function(){
	var state = $(this).val();
	$('#city').empty();
	$.ajax({
		type: "POST",
		url: "<?php echo base_url('student/getAjaxCity'); ?>",
		data:{state:state},
		success: function(response){
			var res = JSON.parse(response);
			$('#city').append('<option value="" disabled selected>Select City</option>');
			$.each(res, function(key, value) {   
				 $('#city')
					 .append($("<option></option>")
								.attr("value",value.cim_id)
								.text(value.cim_name)); 
			});
		}
	});
});
</script>