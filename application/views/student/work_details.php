<style>
a.active {
    background: #fff;
}
.tab-icon{
	font-size: 28px;
    vertical-align: middle;
}
.admission-form-remove-fields{
	padding-top: 10px;
}
.file-field input[type=file] {
    width: 77%;
}
</style>
           
            <section id="content"> 
			
				
	
	<div class="row"  id="side-menu-three-div">
		<div class="col s12 m8 l8 offset-l3 offset-m4">
			<div class="row row-margin-bottom">
				<div class="Space30 col l12 m12 s12"></div>
				<div class="col s12 m12 l12">
				<h4 class="admission-form-heading-margin">Work Details</h4>
				</div>
				<div class="col s12 m3 l3"></div>	
			</div>	
		</div>
		<div class="col s12 m12 l12" style="margin-top: -6.8px;">
			<div class="card" >
				<div class="col s12 l12 m12 Space40"></div>
				<div class="col s12 m12 l12">
				<?php if(!isset($workData)){ ?>
				<form method="post" action="<?php echo base_url('student/work_details'); ?>" id="saveForm">
				<?php }else{ ?>
				<form method="post" action="<?php echo base_url('student/edit_work_details'); ?>" id="editForm">
				<?php } ?>
					<div class="row">
						<div class="col s12 m12 l12">
							<div class="row">
								<div class="col s12 m4 l4">
									<label class="NameLable">Do you have working experience ?</label>
								</div>
								<div class="col s10 m6 l6 no-padding-origin">
								<?php if(isset($workData->swd_is_work_experience)){ ?>
									  <input name="is_work_experience" type="radio" id="test1"  value="1" <?php echo ($workData->swd_is_work_experience == '1') ? 'checked="checked"' : ''; ?> />
									  <label for="test1">Yes</label>
									  <input name="is_work_experience" type="radio" id="test2"  value="0" <?php echo ($workData->swd_is_work_experience == '0') ? 'checked="checked"' : ''; ?>/>
									  <label for="test2">No</label>
								<?php }else{ ?>
									  <input name="is_work_experience" type="radio" id="test1"  value="1" />
									  <label for="test1">Yes</label>
									  <input name="is_work_experience" type="radio" id="test2"  value="0" />
									  <label for="test2">No</label>
								<?php } ?>
								</div>
							</div>
						</div>
						<?php if(!empty($workData->swd_work_experience)){
							  $workexperiece = array(); 
							  $workexperiece = explode(' ', $workData->swd_work_experience); 
						} ?>
						<div class="col s12 m12 l12">
							<div class="row">
								<div class="col s12 m4 l4">
									<label class="NameLable">Work Experience</label>
								</div>
								<div class="col s10 m3 l3 no-padding-origin">
									<input type="text" name="work_experience" id="work_experience" value="<?php echo isset($workexperiece[0]) ? $workexperiece[0] : '';?>">
								</div>
								<div class="col s10 m3 l3 no-padding-origin">
									&nbsp;&nbsp;<span>Months</span>
								</div>
							</div>
						</div>
						<div class="col s12 m12 l12">
							<div class="row">
								<div class="col s12 m4 l4">
									<label class="NameLable">Previous Company</label>
								</div>
								<div class="col s10 m6 l6 no-padding-origin">
									<input type="text" name="prev_comp_name" id="prev_comp_name" value="<?php echo isset($workData->swd_prev_comp_name) ? $workData->swd_prev_comp_name : '';?>">
								</div>
							</div>
						</div>
						<div class="col s12 m12 l12">
							<div class="row">
								<div class="col s12 m4 l4">
									<label class="NameLable">Previous Company Designation</label>
								</div>
								<div class="col s10 m6 l6 no-padding-origin">
									<input type="text" name="prev_comp_desi" id="prev_comp_desi" value="<?php echo isset($workData->swd_prev_comp_desi) ? $workData->swd_prev_comp_desi : '';?>">
								</div>
							</div>
						</div>
						<div class="col s12 m12 l12">
							<div class="row">
								<div class="col s12 m4 l4">
									<label class="NameLable">Current Company</label>
								</div>
								<div class="col s10 m6 l6 no-padding-origin">
									<input type="text" name="curr_comp_name" id="curr_comp_name" value="<?php echo isset($workData->swd_curr_comp_name) ? $workData->swd_curr_comp_name : '';?>">
								</div>
							</div>
						</div>
						<div class="col s12 m12 l12">
							<div class="row">
								<div class="col s12 m4 l4">
									<label class="NameLable">Current Company Designation</label>
								</div>
								<div class="col s10 m6 l6 no-padding-origin">
									<input type="text" name="curr_comp_desi" id="curr_comp_desi" value="<?php echo isset($workData->swd_curr_comp_desi) ? $workData->swd_curr_comp_desi : '';?>">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col s12 m12 l12 center-align">
								<?php if(!isset($workData)){ ?>
								<button type="button" class="waves-effect waves-light btn btn-form-submit Searchbtn center-align save">Add</button>
								<a type="button" href="<?php echo base_url('dashboard/student');?>"class="waves-effect waves-light btn btn-form-submit Searchbtn center-align">Cancel</a>
								<div class="col s12 m12 l12 Space10"></div>
								<?php }else{ ?>
								<input type="hidden" name="swd_id" value="<?php echo $workData->swd_id; ?>">
								<button type="button" class="waves-effect waves-light btn btn-form-submit Searchbtn center-align update">Update</button>
								<a type="button" href="<?php echo base_url('dashboard/student');?>"class="waves-effect waves-light btn btn-form-submit Searchbtn center-align">Cancel</a>
								<div class="col s12 m12 l12 Space10"></div>
								<?php } ?>
							</div>
						</div>
					</div>
				</form>
				</div>
			</div>					
		</div>
	</div>
</section>
<script type="text/javascript" src="<?php echo base_url('assets/js/plugins/jquery-1.11.2.min.js');?>"></script>
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/1.3.1/js/toastr.js"></script>
<script>





var addcount = 0;
$('.update').click(function(){
if($('#work_experience').val()==""){
	$('#work_experience').focus();
	toastr.error('Enter work experience');
	return false;
}else if($('#prev_comp_name').val()==""){
	$('#prev_comp_name').focus();
	toastr.error('Enter prev company name');
	return false;
}else if($('#prev_comp_desi').val()==""){
	$('#prev_comp_desi').focus();
	toastr.error('Enter prev comp desi');
	return false;
}else if($('#curr_comp_name').val()==""){
	$('#curr_comp_name').focus();
	toastr.error('Enter curr comp name');
	return false;
}else if($('#curr_comp_desi').val()==""){
	$('#curr_comp_desi').focus();
	toastr.error('Enter current company designation');
	return false;
}else{
	toastr.success('Update Successfully');

	$('#editForm').submit();
}

	//alert('ih');
	
});

$('.save').click(function(e){
	if($('#work_experience').val()==""){
	$('#work_experience').focus();
	toastr.error('Enter work experience');
	return false;
}else if($('#prev_comp_name').val()==""){
	$('#prev_comp_name').focus();
	toastr.error('Enter prev company name');
	return false;
}else if($('#prev_comp_desi').val()==""){
	$('#prev_comp_desi').focus();
	toastr.error('Enter prev comp desi');
	return false;
}else if($('#curr_comp_name').val()==""){
	$('#curr_comp_name').focus();
	toastr.error('Enter curr comp name');
	return false;
}else if($('#curr_comp_desi').val()==""){
	$('#curr_comp_desi').focus();
	toastr.error('Enter current company designation');
	return false;
}else{
	toastr.success('Update Successfully');

	$('#saveForm').submit();
}

	//alert('ih');
	
});

$('.admission-form-add-fields').click(function(){
	
	var bttnId = $(this).attr('id');
	
	if ($('.row.extrarow').is(':empty')) { 
		$('.row.extrarow').remove();
	}
	
	bttnId = bttnId.split('-');
	formfId = bttnId[1];
	
	var formHtml = '';
	
	if (addcount % 3 == 2){
	formHtml += '</div><div class="row extrarow">';
	}
	$.ajax({
		type: "POST",
		url: "<?php echo base_url('admission/addFormField'); ?>",
		data:{formfId:formfId},
		success: function(response){
			formHtml += response;
			$('#formfielddiv').append(formHtml);
		}
	});
	addcount++;
	$('#field-'+bttnId[1]).hide();
});

$("#formfielddiv").on('click','.admission-form-remove-fields',function(){
		var rmbttnId = $(this).attr('id');
		if ($('.row.extrarow').is(':empty')) {
			$('.row.extrarow').remove();
		}
		rmbttnId = rmbttnId.split('-');
		$(this).parent().parent().parent().remove();
		$('#field-'+rmbttnId[1]).show();
});
</script>