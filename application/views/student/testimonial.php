<style>
#add_program_form{
	display:none;
}
.select-dropdown {
     height: auto !important; 
}
.select-wrapper input.select-dropdown{
	line-height: 2rem;
}
.publish-approve{
	background-color: #38E98F !important;
    color: #ffffff !important;
    padding: 5px;
}
.publish-pending{
	background-color: #e4b331 !important;
    color: #ffffff !important;
    padding: 5px;
}
.publish-reject{
	background-color: #e93853 !important;
    color: #ffffff !important;
    padding: 5px;
}

</style>
<section id="content">
<div class="row"  id="side-menu-five-div">
	<div class="col s12 m12 l12" style="margin-top: -6.8px;">
		<div class="card">		
			<div class="">			
							  <!-- card Section Start-->
				<div id="card-stats" class="seaction">
					<div class="row">
						<div class="col s12 m8 l8 offset-l3 offset-m4">
							<div class="row row-margin-bottom">
								<div class="Space30 col l12 m12 s12"></div>
								<div class="col s12 m12 l12">
								<h5 class="admission-form-heading-margin">Manage Testmonial</h5>
								</div>
								<div class="col s12 m3 l3"></div>	
							</div>	
						</div>
						<div class="Space30 col l12 m12 s12"></div>
						<div class="col l12 m12 s12">
							
							<a id="add_program" style="margin-left:10px" class="waves-effect waves-light btn btn-form-submit Searchbtn center-align">Add Testimonial</a>
						
							</div>
							<div class="col s12 m12 l12 Space10"></div>
							<div class="row col l12 m12 s12" id="add_program_form">
								<form action="#" method="post" id="academic_form">
								<div class="card col l5 m5 s12" style="margin-left:12px">
									<div class="col s12 m12 l12 Space10"></div>
									<div class="col s12 m3 l3">
										<label class="NameLable">Title</label>
									</div>
									<div class="col s12 m8 l8 no-padding-origin">
										<input type="text" name="name" id="name" Placeholder="Enter Title">
									
									</div>
									<div class="col s12 m3 l3">
										<label class="NameLable">Message</label>
									</div>
									<div class="col s12 m8 l8 no-padding-origin">
										<textarea type="text" name="laname" id="lname" Placeholder="Enter Enter Message"></textarea>
									
									</div>
									
									<div class="col s12 m12 l12 Space10"></div>
								
									
									<div class="col s12 m12 l12 Space10"></div>
									<div class="col s12 m8 l8 offset-l3 offset-m3 no-padding-origin">
										<input type="button" value="Save" class="waves-effect waves-light btn btn-form-submit Searchbtn center-align up-save-btn" id="save">
												<input type="button" value="cancel" class="waves-effect waves-light btn btn-form-submit Searchbtn center-align up-save-btn" id="cancel_save">

									</div>
									<div class="col s12 m12 l12 Space10"></div>
								</div>
								</form>
								<div class="col l7 m7 s12"></div>
							</div>

							<div class="row col l12 m12 s12" id="edit_program_form">
								<form action="#" method="post" id="academic_form">
								<div class="card col l5 m5 s12" style="margin-left:12px">
									<div class="col s12 m12 l12 Space10"></div>
									<div class="col s12 m3 l3">
										<label class="NameLable">Title</label>
									</div>
									<div class="col s12 m8 l8 no-padding-origin">
										<input type="text" name="name" id="name_edit" Placeholder="Enter Title">
									
									</div>
									<div class="col s12 m3 l3">
										<label class="NameLable">Message</label>
									</div>
									<div class="col s12 m8 l8 no-padding-origin">
										<textarea type="text" name="laname" id="lname_edit" Placeholder="Enter Enter Message"></textarea>
									<input type="hidden" id="tid" name="tid" value="">
									</div>
									
									<div class="col s12 m12 l12 Space10"></div>
								
									
									<div class="col s12 m12 l12 Space10"></div>
									<div class="col s12 m8 l8 offset-l3 offset-m3 no-padding-origin">
										<input type="button" value="Update" class="waves-effect waves-light btn btn-form-submit Searchbtn center-align up-save-btn" id="update">
										<input type="button" value="cancel" class="waves-effect waves-light btn btn-form-submit Searchbtn center-align up-save-btn" id="cancel_update">

									</div>
									<div class="col s12 m12 l12 Space10"></div>
								</div>
								</form>
								<div class="col l7 m7 s12"></div>
							</div>
						
						<div class="col l12 m12 s12">
							<div id="responsive-table">
								<div class="row">
									<div class="col s12 l12 m12">
										<table class="responsive-table">
											<thead>
												<tr>
													<th class="upm-table-head-bg center-align">No.</th>
													<th class="upm-table-head-bg center-align">Title</th>
													<th class="upm-table-head-bg center-align">Message</th>
													<!-- <th class="upm-table-head-bg center-align">Role</th> -->
													<th class="upm-table-head-bg center-align">Status</th>
													<th class="upm-table-head-bg center-align" colspan='0'>Action</th>
												</tr>
											</thead>
											<tbody id="program_list">
											
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>	
</div>
<?php 
$userdata = $this->session->userdata('logged_in');
?>
</section>
<script type="text/javascript" src="<?php echo base_url('assets/js/plugins/jquery-1.11.2.min.js');?>"></script>
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/1.3.1/js/toastr.js"></script>

<script>
$('#edit_program_form').hide( "slow" );
get_program()
function get_program()
{
$.ajax({
		type: "POST",
		url: "<?php echo base_url('student/testimonial_list'); ?>",
		data:{'id':<?php echo $userdata['id'];?>},
		//dataType: "json",
		success: function(response){
			var obj = JSON.parse(response);
			console.log(obj);
			 var html='';
			 var count=1;
			 if(obj.length > 0){
				$.each(obj, function(ind,val){
					
						var msg='';
						var msg1='';
						var action='<a href="#" onclick="edit('+val.id+')"><img class="sideIcon cust-cursor" src="<?php echo base_url('assets/img/edit-icon-01.png'); ?>"/></a>';
						var action_del='<a href="#" onclick="del_test('+val.id+')"><img class="sideIcon cust-cursor" src="<?php echo base_url();?>assets/img/delete-icon-02.png"></a>';
						var action1='<a href="#"></a>';
						
					if(val.status==1){
							msg+='<div class="switch section" id="input-switches" ><label><input type="checkbox" class="status" checked disabled><span class="lever"></span> </label></div>';
						}else if(val.status==0){
							msg+='<div class="switch section" id="input-switches" ><label><input type="checkbox" class="status" disabled><span class="lever"></span> </label></div>';
						}

						
						
						html+='<tr ><td class="upm-table-content-bg center-align">'+ count++ +'</td>'+
						'<td class="upm-table-content-bg center-align">'+val.title+'</td>'+
						'<td class="upm-table-content-bg center-align">'+val.testimonial+'</td>'+
					//	'<td class="upm-table-content-bg center-align">'+val.role+'</td>'+
						'<td class="upm-table-content-bg center-align">'+msg+'</td>'+
						'<td class="upm-table-content-bg center-align">'+action+' '+action_del+' &nbsp;&nbsp;&nbsp;&nbsp; '+ action1 +'</td>'+
						//'<td class="upm-table-content-bg center-align">'+msg1+'</td>'+ 
					    
						'</tr>';
						
						
				});
}else
{
	html='<td colspan="6" class="upm-table-content-bg center-align">No Result Found</td>';
}
	
				$('#program_list').html(html);
			
			
		}
	});
	
}
$('#save').click(function(){
var name=$('#name').val();
var lname=$('#lname').val();
var stuid=<?php echo $userdata['id'];?>;

 var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(name==''){
    	toastr.error("Enter name");
    	$('#name').focus();
		return false;
    }else if(lname==''){
    	toastr.error("Enter last name");
    	$('#lname').focus();
		return false;
	
	}else{
		//toastr.success("success");
var dataS={
	'title':name,
	'message':lname,
	'stuid':stuid

}
// console.log(dataS);
$.ajax({
			type:'POST',
			url:'<?php echo base_url()?>student/add_testimonial',
			data:dataS,
			success:function(res){
				//console.log(res);
				var obj=JSON.parse(res);
				//console.log(obj);
				if(obj.msg=='successfully'){
				
					toastr.success('Successfully Added..!');
					setTimeout(function(){
					get_program();
					 $('#academic_form').trigger("reset");;
					}, 100);
				}
	
		
			}
	})
}

});

$('#update').click(function(){
var title=$('#name_edit').val();
var message=$('#lname_edit').val();
var acid=$('#tid').val();
  if(title==''){
    	toastr.error("Enter Title");
    	$('#name_edit').focus();
		return false;
    }else if(message==''){
    	toastr.error("Enter Message");
    	$('#lname_edit').focus();
		return false;
	
	}else{
$.ajax({
			type:'POST',
			url:'<?php echo base_url()?>student/update_testimonial',
			data:{'title':title,'id':acid,'message':message},
			success:function(res){
				//console.log(res);
				var obj=JSON.parse(res);
				console.log(obj);
				if(obj.msg=='successfully'){
				
					toastr.success('Successfully Updated..!');
					setTimeout(function(){
					 get_program();
					 $('#edit_program_form').hide();
					 $('#edit_academic_form').trigger("reset");;
					}, 100);
				}
	
		
			}
	});
}
});


function edit(id){
	$('#add_program_form').hide();
	$('#edit_program_form').show( "slow" );
$.ajax({
			type:'POST',
			url:'<?php echo base_url()?>student/testimonial_edit',
			data:{'id':id},
			success:function(res){
				var obj=JSON.parse(res);
				console.log(obj);
				$('#name_edit').val(obj[0].title);
				$('#lname_edit').val(obj[0].testimonial);
				$('#tid').val(obj[0].id);
				//$('#email_edit').val(obj[0].email);
		
			}
	})
}
function del_test(id)
{
	 // alert(id);
	 // return false;
	$.ajax({
			type:'POST',
			url:'<?php echo base_url()?>student/testimonial_active',
			data:{'id':id},
			success:function(res){
			if(res)
			{
			toastr.success("Delete Successfully");
			
						setTimeout(function(){
					 get_program();
		}, 100);
			}
		
			}
	})
}

function reject(id)
{
	$.ajax({
			type:'POST',
			url:'<?php echo base_url()?>admin/academic_inactive',
			data:{'id':id},
			success:function(res){
				//alert(res);
			if(res)
			{
				
			toastr.error("Rejected......!");
			
						setTimeout(function(){
					 get_program();
		}, 100);
			}
		
			}
	})
}


$('#add_program').click(function(){
	$('#add_program_form').slideToggle( "slow" );
	$('#edit_program_form').hide();
});
$('#cancel_save').click(function(){
	//$('#add_program_form').slideToggle( "slow" );
	$('#add_program_form').hide('slow');
});
$('#cancel_update').click(function(){
	//$('#add_program_form').slideToggle( "slow" );
	
	$('#edit_program_form').hide('slow');
});
function changeStatus(id,status){
 	
 	$.ajax({
			type:'POST',
			url:'<?php echo base_url()?>admin/academic_inactive',
			data:{'id':id,'status':status},
			success:function(res){
			if(res)
			{
				
			toastr.success("Status Has been changed......!");
			
						setTimeout(function(){
					 get_program();
		}, 100);
			}
		
			}
	})
	
}
</script>