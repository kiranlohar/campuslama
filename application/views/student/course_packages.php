<style>
a.active {
    background: #fff;
}
.tab-icon{
	font-size: 28px;
    vertical-align: middle;
}
.admission-form-remove-fields{
	padding-top: 10px;
}
.file-field input[type=file] {
    width: 77%;
}

</style>
           
            <section id="content"> 
			
			<?php if(empty($personalData)){ ?>	
	<div id="msg">
		
	</div>
	<?php } ?>
	<div class="row"  id="side-menu-three-div">
		<div class="col s12 m8 l8 offset-l3 offset-m4">
			<div class="row row-margin-bottom">
				<div class="Space30 col l12 m12 s12"></div>
				<div class="col s12 m12 l12">
				<h4 class="admission-form-heading-margin">Application Packages</h4>
				</div>
				<div class="col s12 m3 l3"></div>	
			</div>	
		</div>
		<div class="col s12 m12 l12" style="margin-top: -6.8px;">
			<div class="card" >
				<div class="col s12 l12 m12 Space40"></div>
				<div class="col s12 m12 l12">
					<section class="plans-container" id="plans">
						<?php $color =''; $i=0; foreach($packagedata as $pckdata){ 
						if ($i==0){
							$color = 'purple';
						}else if (($i%1) == 0 && $i==1){
							$color = 'cyan';
						}else if (($i%2) == 0){
							$color = 'green';
						}
						$i++;
						?>
						<article class="col s12 m6 l4">
						  <div class="card hoverable">
							<div class="card-image <?php echo $color; ?> waves-effect">
							  <div class="card-title"><?php echo $pckdata->pack_name; ?></div>
							  <div class="price"><sup>Rs.</sup><?php echo $pckdata->pack_amount; ?></div>
							  <div class="price-desc"><?php echo $pckdata->pack_valid; ?></div>
							</div>
							<div class="card-content">
							  <ul class="collection">
								<li class="collection-item"><?php echo $pckdata->num_list; ?> Applications</li>
							  </ul>
							</div>
							<div class="card-action center-align">                      
							  <button class="waves-effect waves-light btn plan_select">Select Plan</button>
							</div>
						  </div>
						</article>
						<?php } ?>
					</section>
				</div>
			</div>					
		</div>
	</div>
</section>
<script type="text/javascript" src="<?php echo base_url('assets/js/plugins/jquery-1.11.2.min.js');?>"></script>
<script>
var addcount = 0;
$('.admission-form-add-fields').click(function(){
	
	var bttnId = $(this).attr('id');
	
	if ($('.row.extrarow').is(':empty')) { 
		$('.row.extrarow').remove();
	}
	
	bttnId = bttnId.split('-');
	formfId = bttnId[1];
	
	var formHtml = '';
	
	if (addcount % 3 == 2){
	formHtml += '</div><div class="row extrarow">';
	}
	$.ajax({
		type: "POST",
		url: "<?php echo base_url('admission/addFormField'); ?>",
		data:{formfId:formfId},
		success: function(response){
			formHtml += response;
			$('#formfielddiv').append(formHtml);
		}
	});
	addcount++;
	$('#field-'+bttnId[1]).hide();
});

$("#formfielddiv").on('click','.admission-form-remove-fields',function(){
		var rmbttnId = $(this).attr('id');
		if ($('.row.extrarow').is(':empty')) {
			$('.row.extrarow').remove();
		}
		rmbttnId = rmbttnId.split('-');
		$(this).parent().parent().parent().remove();
		$('#field-'+rmbttnId[1]).show();
});

$(".plan_select").click(function(){
	$("#msg").empty();
	var htmlmsg = '<div id="card-alert" class="card-alert-facility red" ><div class="card-content white-text"> <p>Sorry..! Please Update Your Profile First</p></div><button type="button" class="close white-text" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button></div>';
	
	$("#msg").append(htmlmsg);
});


</script>