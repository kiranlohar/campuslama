<style>
a.active {
    background: #fff;
}
.tab-icon{
	font-size: 28px;
    vertical-align: middle;
}
.admission-form-remove-fields{
	padding-top: 10px;
}
.file-field input[type=file] {
    width: 77%;
}


</style>
           
            <section id="content"> 
			
			<?php //print_r($userData); ?>	
	
	<div class="row"  id="side-menu-three-div">
		<div class="col s12 m8 l8 offset-l3 offset-m4">
			<div class="row row-margin-bottom">
				<div class="Space30 col l12 m12 s12"></div>
				<div class="col s12 m12 l12">
				<h4 class="admission-form-heading-margin">Personal Details</h4>
				</div>
				<div class="col s12 m3 l3"></div>	
			</div>	
		</div>
		<div class="col s12 m12 l12" style="margin-top: -6.8px;">		
			<div class="card" >
				<div class="col s12 l12 m12 Space40"></div>
				<div class="col s12 m12 l12">
				<?php if(!isset($personalData)){ ?>
				<form method="post" action="<?php echo base_url('student/personal_details'); ?>" enctype="multipart/form-data" id="saveForm">
				<?php }else{ ?>
				<form method="post" action="<?php echo base_url('student/edit_personal_details'); ?>" enctype="multipart/form-data" id="editForm">
				<?php } ?>
					<div class="row">
						<?php 
						$uname = explode(' ',$userData['name']); 
						$firstname = '';
						$middlename = '';
						$lastname = '';
						$email = '';
						if($personalData){
							
							$firstname = $personalData->spd_firstname;
							$middlename = $personalData->spd_middlename;
							$lastname = $personalData->spd_lastname;
							$email = $personalData->spd_email;
						}else{
							if(count($uname)== 2){
								$firstname = $uname[0];
								$lastname = $uname[1];
								$email = $userData['email'];
							}else if(count($uname)== 3){
								$firstname = $uname[0];
								$middlename = $uname[1];
								$lastname = $uname[2];
								$email = $userData['email'];
							}
						}
						?>
						<div class="col s12 m4 l4">
							<div class="row">
								<div class="col s12 m4 l4">
									<label class="NameLable">First Name</label>
								</div>
								<div class="col s10 m6 l6 no-padding-origin">
									<input type="text" name="firstname"  id="firstname" value="<?php echo $firstname; ?>" maxlength="30">
									<span id="error1" style="color:red"></span>
								</div>
							</div>
						</div>
						<div class="col s12 m4 l4">
							<div class="row">
								<div class="col s12 m4 l4">
									<label class="NameLable">Middle Name</label>
								</div>
								<div class="col s10 m6 l6 no-padding-origin">
									<input type="text" name="middlename" id="middlename" value="<?php echo $middlename; ?>" maxlength="30">
									<span id="error2" style="color:red"></span>
								</div>
							</div>
						</div>
						<div class="col s12 m4 l4">
							<div class="row">
								<div class="col s12 m4 l4">
									<label class="NameLable">Last Name</label>
								</div>
								<div class="col s10 m6 l6 no-padding-origin">
									<input type="text" name="lastname"  id="lastname" value="<?php echo $lastname; ?>" maxlength="30" >
								</div>
							</div>
						</div>
						<div class="col s12 m4 l4">
							<div class="row">
								<div class="col s12 m4 l4">
									<label class="NameLable">Email</label>
								</div>
								<div class="col s10 m6 l6 no-padding-origin">
									<input type="text" name="email" id="email" value="<?php echo $email; ?>" maxlength="50" >
								</div>
							</div>
						</div>
						<div class="col s12 m4 l4">
							<div class="row">
								<div class="col s12 m4 l4">
									<label class="NameLable">Mobile</label>
								</div>
								<div class="col s10 m6 l6 no-padding-origin">
									<span value="+91" style="width: -40px;border-color:white;margin-left: -36px;" readonly>+91- </span><input  style="width: 204px;" type="text" name="mobile"  id="mobile" maxlength="12" value="<?php echo isset($personalData->spd_mobile) ? $personalData->spd_mobile : '';?>">
								</div>
							</div>
						</div>
						<div class="col s12 m4 l4">
							<div class="row">
								<div class="col s12 m4 l4">
									<label class="NameLable">Birth Date</label>
								</div>
								<div class="col s10 m6 l6 no-padding-origin">
									<input type="date" id="birthdate" name="birthdate" placeholder="--/--/----" value="<?php echo isset($personalData->spd_dob) ? $personalData->spd_dob : '';?>">
								</div>
							</div>
						</div>
					
						
						<div class="col s12 m4 l4">
							<div class="row">
								<div class="col s12 m4 l4" >
									<label class="NameLable">Country</label>
								</div>
								<div class="col s10 m6 l6 no-padding-origin">
									<select class="browser-default" name="country" id="country">
										<option value="" disabled selected>Select Country</option>
										<option <?php echo empty($personalData)? '' : ($personalData->spd_cm_id == '100') ? 'selected="selected"' : '';?> value="100">India</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col s12 m4 l4">
							<div class="row">
								<div class="col s12 m4 l4">
									<label class="NameLable">State</label>
								</div>
								<div class="col s10 m6 l6 no-padding-origin">
									<select class="browser-default" id="state" name="state">
										<option value="">Select State</option>
										<?php foreach($statedata as $sdata){ ?>
											<option <?php echo ($personalData->spd_sm_id == $sdata->sm_id) ? 'selected="selected"' : ''; ?> value="<?php echo $sdata->sm_id; ?>"><?php echo $sdata->sm_name; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
						</div>
						<div class="col s12 m4 l4">
							<div class="row">
								<div class="col s12 m4 l4">
									<label class="NameLable">City</label>
								</div>
								<div class="col s10 m6 l6 no-padding-origin">
									<select class="browser-default" id="city" name="city">
										<option value="">Select City</option>
										<?php foreach($citydata as $cdata){ ?>
											<option <?php echo ($personalData->spd_cim_id == $cdata->cim_id) ? 'selected="selected"' : ''; ?> value="<?php echo $cdata->cim_id; ?>"><?php echo $cdata->cim_name; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
						</div>
						
						<div class="col s12 m4 l4">
							<div class="row">
								<div class="col s12 m4 l4">
									<label class="NameLable">Address</label>
								</div>
								<div class="col s10 m6 l6 no-padding-origin">
									<textarea row="1" maxlength="150" id="address" name="address"><?php echo isset($personalData->spd_address) ? $personalData->spd_address : '';?></textarea>
								</div>
							</div>
						</div>
						<div class="col s12 m4 l4">
							<div class="row">
								<div class="col s12 m4 l4">
									<label class="NameLable">About You</label>
								</div>
								<div class="col s10 m6 l6 no-padding-origin">
								<textarea row="1" maxlength="200" id="about_you" name="about_you"><?php echo isset($personalData->spd_about_you) ? $personalData->spd_about_you : '';?></textarea>
								</div>
							</div>
						</div>
						<div class="col s12 m4 l4">
							<div class="row">
								<div class="col s12 m4 l4">
									<label class="NameLable">Pincode</label>
								</div>
								<div class="col s10 m6 l6 no-padding-origin">
									<input type="text" name="pincode" id="pincode" placeholder="" maxlength='6' value="<?php echo isset($personalData->spd_pincode) ? $personalData->spd_pincode : '';?>">
								</div>
							</div>
						</div>
							<div class="col s12 m4 l4">
							<div class="row">
								<div class="col s12 m4 l4">
									<label class="NameLable">Gender</label>
								</div>
								<div class="col s10 m6 l6 no-padding-origin">
									  <?php if(isset($personalData->spd_gender)){ ?>
									  <input name="gender" type="radio" id="test1" <?php echo ($personalData->spd_gender == 'Male') ? 'checked="checked"' : ''; ?> value="Male" />
									  <label for="test1">Male</label>
									  <input name="gender" type="radio" id="test2" <?php echo ($personalData->spd_gender == 'Female') ? 'checked="checked"' : ''; ?> value="Female"/>
									  <label for="test2">Female</label>
									  <?php }else{ ?>
									  <input name="gender" type="radio" id="test1"  value="Male" />
									  <label for="test1">Male</label>
									  <input name="gender" type="radio" id="test2"  value="Female"/>
									  <label for="test2">Female</label>
									  <?php } ?>
								</div>
							</div>
						</div>
						<div class="col s12 m12 l12 ">
							<div class="row file-field input-field">
								<div class="col s12 m3 l3">
								<div class="btn upload-btn">
									<span>Choose Profile Here</span>
									<input type="file" name="profile_pic" id="profile_pic" accept="image/jpeg,image/png">
								
									
									 
								</div>
								</div>
								<div class="col s10 m3 l3 no-padding-origin" style="margin-top:-17px">	
								<input type="hidden" class="file-path validate" name="file_selected" id="file_selected" type="text" value="<?php echo isset($personalData->spd_profile_pic) ? $personalData->spd_profile_pic : '';?>" />
								<!-- <img class="circle responsive-img" src="<?php echo base_url()?>uploads/users/<?php echo $personalData->spd_profile_pic?>" style="width:70px;height:70px"> -->
									<?php if(!empty($personalData->spd_profile_pic)){ ?>
								<img class="circle responsive-img" id="blah1" src="<?php echo base_url()?>uploads/users/<?php echo $personalData->spd_profile_pic?>" style="width:70px;height:70px">
								<?php  }else{ ?>
								<img class="circle responsive-img" id="blah2" src="" style="width:70px;height:70px;margin-top: 0px;">
								<?php  } ?>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col s12 m12 l12 center-align">
								<?php if(empty($personalData)){ ?>
								<button type="button" class="waves-effect waves-light btn btn-form-submit Searchbtn center-align save">Add</button>
								<a type="button" href="<?php echo base_url('dashboard/student');?>"class="waves-effect waves-light btn btn-form-submit Searchbtn center-align">Cancel</a>
								<div class="col s12 m12 l12 Space10"></div>
								<?php }else{ ?>
								<input type="hidden" name="spd_id" value="<?php echo $personalData->spd_id; ?>">
								<button type="button" class="waves-effect waves-light btn btn-form-submit Searchbtn center-align update">Update</button>
								<a type="button" href="<?php echo base_url('dashboard/student');?>"class="waves-effect waves-light btn btn-form-submit Searchbtn center-align">Cancel</a>
								<div class="col s12 m12 l12 Space10"></div>
								<?php } ?>
							</div>
						</div>
					</div>
				</form>
				</div>
			</div>					
		</div>
	</div>
</section>
<script type="text/javascript" src="<?php echo base_url('assets/js/plugins/jquery-1.11.2.min.js');?>"></script>
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/1.3.1/js/toastr.js"></script>
<script>
$('#mobile,#pincode').keyup(function(e)
                                {
  if (/\D/g.test(this.value))
  {
    // Filter non-digits from input value.
    toastr.error('Enter only number');
    this.value = this.value.replace(/\D/g, '');
  }
});
$('.update').click(function(){
	 var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	 var phoneno = /^(([1-9]*)|(([1-9]*).([0-9]*)))$/;  
	 var s=$('#mobile').val();
	 console.log(s);
if($('#firstname').val()==""){
	$('#firstname').focus();
	toastr.error('Enter First name');
	return false;
}else if($('#lastname').val()==""){
	$('#lastname').focus();
	toastr.error('Enter last name');
	return false;
}else if($('#email').val()==""){
	$('#email').focus();
	toastr.error('Enter Email Address');
	return false;
}else if(!re.test($('#email').val())){
	$('#email').focus();
	toastr.error('Enter Valid Email Address');
	return false;
}else if($('#mobile').val()==""){
	$('#mobile').focus();
	toastr.error('Enter Mobile No');
	return false;
}else if($('#birthdate').val()==""){
	$('#birthdate').focus();
	toastr.error('Enter Birth date');
	return false;
}else if($('#country').val()==""){
	$('#country').focus();
	toastr.error('Select Country');
	return false;
}else if($('#state').val()==""){
	$('#state').focus();
	toastr.error('Select State');
	return false;
}else if($('#city').val()==""){
	$('#city').focus();
	toastr.error('Select City');
	return false;
}else if($('#address').val()==""){
	$('#address').focus();
	toastr.error('Enter Address');
	return false;
}else if($('#about_you').val()==""){
	$('#about_you').focus();
	toastr.error('Enter About you');
	return false;
}else if($('#pincode').val()==""){
	$('#pincode').focus();
	toastr.error('Enter Pincode');
	return false;
}else{
	//toastr.success('Update Successfully');

	$('#editForm').submit();
}

	//alert('ih');
	
});

$('.save').click(function(){
	 var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	 var phoneno = /^[0-9]+$/;  
if($('#firstname').val()==""){
	$('#firstname').focus();
	toastr.error('Enter First name');
	return false;
}else if($('#lastname').val()==""){
	$('#lastname').focus();
	toastr.error('Enter last name');
	return false;
}else if($('#email').val()==""){
	$('#email').focus();
	toastr.error('Enter Email Address');
	return false;
}else if(!re.test($('#email').val())){
	$('#email').focus();
	toastr.error('Enter Valid Email Address');
	return false;
}else if($('#mobile').val()==""){
	$('#mobile').focus();
	toastr.error('Enter Mobile No');
	return false;
}else if($('#birthdate').val()==""){
	$('#birthdate').focus();
	toastr.error('Enter Birth date');
	return false;
}else if($('#country').val()==""){
	$('#country').focus();
	toastr.error('Select Country');
	return false;
}else if($('#state').val()==""){
	$('#state').focus();
	toastr.error('Select State');
	return false;
}else if($('#city').val()==""){
	$('#city').focus();
	toastr.error('Select City');
	return false;
}else if($('#address').val()==""){
	$('#address').focus();
	toastr.error('Enter Address');
	return false;
}else if($('#about_you').val()==""){
	$('#about_you').focus();
	toastr.error('Enter About you');
	return false;
}else if($('#pincode').val()==""){
	$('#pincode').focus();
	toastr.error('Enter Pincode');
	return false;
}else{
	//toastr.success('Update Successfully');

	$('#saveForm').submit();
}

	//alert('ih');
	
});



$('#country').change(function(){
	var country = $(this).val();
	$('#state').empty();
	$.ajax({
		type: "POST",
		url: "<?php echo base_url('student/getAjaxState'); ?>",
		data:{country:country},
		success: function(response){
			var res = JSON.parse(response);
			$('#state').append('<option value="" disabled selected>Select State</option>');
			$.each(res, function(key, value) {   
				 $('#state')
					 .append($("<option></option>")
								.attr("value",value.sm_id)
								.text(value.sm_name)); 
			});
		}
	});
});

$('#state').change(function(){
	var state = $(this).val();
	$('#city').empty();
	$.ajax({
		type: "POST",
		url: "<?php echo base_url('student/getAjaxCity'); ?>",
		data:{state:state},
		success: function(response){
			var res = JSON.parse(response);
			$('#city').append('<option value="" disabled selected>Select City</option>');
			$.each(res, function(key, value) {   
				 $('#city')
					 .append($("<option></option>")
								.attr("value",value.cim_id)
								.text(value.cim_name)); 
			});
		}
	});
});
 	$('#blah2').hide();

 function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            // $('#blah1').hide();
            reader.onload = function (e) {
            	
            	$('#blah1').show();
                $('#blah1').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    $("#profile_pic").change(function(){

        readURL(this);
    });


     function readURL1(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
             //$('#blah').hide();
            reader.onload = function (e) {
            	
            	$('#blah2').show();
                $('#blah2').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    $("#profile_pic").change(function(){

        readURL1(this);
    });
</script>

