<style>
a.active {
    background: #fff;
}
.tab-icon{
	font-size: 28px;
    vertical-align: middle;
}
.admission-form-remove-fields{
	padding-top: 10px;
}
.file-field input[type=file] {
    width: 77%;
}
label {
    color: #000000;
}
</style>
           
<section id="content"> 
	<div class="row"  id="side-menu-three-div">
		<div class="col s12 m8 l8 offset-l3 offset-m4">
			<div class="row row-margin-bottom">
				<div class="Space30 col l12 m12 s12"></div>
				<div class="col s12 m12 l12">
				<h4 class="admission-form-heading-margin">Educational Details</h4>
				</div>
				<div class="col s12 m3 l3"></div>	
			</div>	
		</div>

		<div class="col s12 m12 l12" style="margin-top: -6.8px;">	
		 <div class="card" style="height:800px">
			<div class="col s12 l12 m12 Space40"></div>
				<div class="col s12 m12 l12">
					<?php if(!isset($eduData)){ ?>
				<form action="<?php echo base_url('student/educational_details'); ?>" method="post" enctype="multipart/form-data" id="saveForm">
				<?php }else{ ?>
				<form action="<?php echo base_url('student/edit_educational_details'); ?>" method="post" enctype="multipart/form-data" id="editForm">
				<?php } ?>
								  <div class="rows">
		<div class="input-field col s12">
<h6><b>10th Details</b></h6>

		</div>
	</div>
					  <br>
<br>
		
					<div class="row">
					    <div class="input-field col s3">
					     
					      <?php $yearArray = range(2000, 2050); ?>
										<select class="browser-default" name="ssc_pass_year" id="ssc_pass_year" style="margin-top: 17px;">
											<option value="">Select Year</option>
											<?php
											foreach ($yearArray as $year) {
												if($eduData->sed_10th_passout_year == $year){
													$selected = 'selected';	
												}else{
													$selected = '';
												}
												echo '<option value="'.$year.'" '.$selected.'>'.$year.'</option>';
											}
											?>
										</select>
					      	<label class="10th Passing Year" style="margin-top: -16px;">10th Passing Year</label>
					    </div>
					    <div class="input-field col s3">
					          	<?php $perArray = range(35, 100); ?>
										<select class="browser-default" name="ssc_percent" id="ssc_percent" style="margin-top: 17px;">
											<option value="">Select Percentage</option>
											<?php
											foreach ($perArray as $percentage) {
												
												if($eduData->sed_10th_percent == $percentage){
													$selected = 'selected';	
												}else{
													$selected = '';
												}
												echo '<option value="'.$percentage.'%" '.$selected.'>'.$percentage.'%</option>';
											}
											?>
										</select>
					      	<label class="10th Passing Year" style="margin-top: -16px;">10th Percentage (%)</label>
					    </div>
					    <div class="col s3">
					    	<label class="10th Passing Year" style="margin-top: -32px;">10th School Name</label>
					    	<input type="text" name="ssc_school_name" id="ssc_school_name" value="<?php echo isset($eduData->sed_10th_school_name) ? $eduData->sed_10th_school_name : ''; ?>" style="margin-top: 14px;">
					     
					    </div>
					     <div class="input-field col s3" style="padding-top: 31px">
					     	<label class="10th Passing Year" style="margin-top: -11px;">10th Certificate</label>
					     <div class="file-field input-field">
								
								<div class="btn upload-btn">
									<span>Choose File</span>
									<input type="file" name="10th_certi" id="10th_certi" accept="image/jpeg,image/png">
								</div>
									<?php if(isset($eduData->tenth_certi)){ ?>
								<div class="col s10 m5 l5 no-padding-origin">	
								<input class="file-path validate" name="10th_certi" id="10th_certi" type="hidden" value="<?php echo isset($eduData->tenth_certi) ? $eduData->tenth_certi : ''; ?>" />
								<img class="square  responsive-img" id="tenth_1" src="<?php echo base_url(); ?>uploads/certificate/10th/<?php echo $eduData->tenth_certi; ?>" style="width: 48px;height: 30px;margin-left: 10px;">
									<?php } else{ ?>
								<img class="square responsive-img" id="tenth_2" src="" style="width: 48px;height: 30px;margin-left: 10px;">
								<?php  } ?>
								</div>
							</div>
					    </div>
					  </div>
					  <div class="rows">
		<div class="input-field col s12">
<h6><b>12th Details</b></h6>

		</div>
	</div>
					  <br>
<br>
					  <div class="rows">
					  	 <div class="input-field col s3">
					     <?php $yearArray = range(2000, 2050); ?>
										<select class="browser-default" name="hsc_pass_year" id="hsc_pass_year" style="margin-top:16px">
											<option value="">Select Year</option>
											<?php
											foreach ($yearArray as $year) {
												
												if($eduData->sed_12th_passout_year == $year){
													$selected = 'selected';	
												}else{
													$selected = '';
												}
												
												echo '<option '.$selected.' value="'.$year.'">'.$year.'</option>';
											}
											?>
										</select>
						<label class="10th Passing Year" style="margin-top: -16px;">12th Passing Year</label>
					    </div>
					     <div class="input-field col s3">
					     <?php $perArray = range(35, 100); ?>
										<select class="browser-default" name="hsc_percent" id="hsc_percent" style="margin-top:16px">
											<option value="">Select Percentage</option>
											<?php
											foreach ($perArray as $percentage) {
												
												if($eduData->sed_12th_percent == $percentage){
													$selected = 'selected';	
												}else{
													$selected = '';
												}
												
												echo '<option '.$selected.' value="'.$percentage.'%">'.$percentage.'%</option>';
											}
											?>
										</select>
						<label class="10th Passing Year" style="margin-top: -16px;">12th Percentage (%)</label>
					    </div>
					    <div class="col s2">
					    	<label class="10th Passing Year" style="margin-top: -32px;">12th College Name</label>
					     	<input type="text" name="hsc_college_name" id="hsc_college_name" value="<?php echo isset($eduData->sed_12th_college_name) ? $eduData->sed_12th_college_name : ''; ?>" style="margin-top: 14px;">
					    </div>
					    <div class="col s2">
					    	<label class="10th Passing Year" style="margin-top: -32px;">12th Board Name</label>
					    	<input type="text" name="hsc_university_name" id="hsc_university_name" value="<?php echo isset($eduData->sed_12th_university_name) ? $eduData->sed_12th_university_name : ''; ?>" style="margin-top: 14px;">
					     	
					    </div>
					    <div class="col s2">
					    	<label class="10th Passing Year" style="margin-top: -34px;">12th Certificate </label>
					    	  <div class="file-field input-field">
								
								<div class="btn upload-btn">
									<span>Choose File</span>
									<input type="file" name="12th_certi" id="12th_certi" accept="image/jpeg,image/png">
								</div>
								<?php if(isset($eduData->twelth_certi)){ ?>
								<div class="col s10 m5 l5 no-padding-origin">	
								<input class="file-path validate" name="12th_certi" id="12th_certi" type="hidden" value="<?php echo isset($eduData->twelth_certi) ? $eduData->twelth_certi : ''; ?>" />
								<img class="square  responsive-img" id="twelth_1" src="<?php echo base_url(); ?>uploads/certificate/12th/<?php echo $eduData->twelth_certi; ?>" style="width: 48px;height: 30px;margin-left: 10px;">
								</div>
								<?php } else{ ?>
								<img class="square responsive-img" id="twelth_2" src="" style="width: 48px;height: 30px;margin-left: 10px;">
								<?php  } ?>
							</div>
					     	
					    </div>
					  </div>
					  <br>
					  <div class="rows">
		<div class="input-field col s12">
<h6><b>Diploma Details</b></h6>

		</div>
	</div>
					    <div class="rows">
					  	 <div class="input-field col s3">
					    <?php $yearArray = range(2000, 2050); ?>
										<select class="browser-default" name="diploma_pass_year" id="diploma_pass_year" style="margin-top:38px">
											<option value="">Select Year</option>
											<?php
											foreach ($yearArray as $year) {
												
												if($eduData->sed_diploma_passout_year == $year){
													$selected = 'selected';	
												}else{
													$selected = '';
												}
												
												echo '<option '.$selected.' value="'.$year.'">'.$year.'</option>';
											}
											?>
										</select>
						<label class="10th Passing Year" style="margin-top: 0px;">Diploma Passing Year</label>
					    </div>
					     <div class="input-field col s3" style="width:215px">
					    <?php $perArray = range(35, 100); ?>
										<select class="browser-default" name="diploma_percent" id="diploma_percent" style="margin-top:38px">
											<option value="">Select Percentage</option>
											<?php
											foreach ($perArray as $percentage) {
												
												if($eduData->sed_diploma_percent == $percentage){
													$selected = 'selected';	
												}else{
													$selected = '';
												}
												echo '<option '.$selected.' value="'.$percentage.'%">'.$percentage.'%</option>';
											}
											?>
										</select>
						<label class="10th Passing Year" style="margin-top: 5px;">Diploma Percentage (%)</label>
					    </div>
					    <div class="col s2" style="margin-top: 21px;">
					    	<label class="10th Passing Year" style="margin-top: -32px;">Diploma College Name</label>
					     	
					     	<input type="text" name="diploma_college_name" id="diploma_college_name" value="<?php echo isset($eduData->sed_diploma_college_name) ? $eduData->sed_diploma_college_name : ''; ?>" style="margin-top: 14px;" >
					    </div>
					    <div class="col s2" style="margin-top: 21px;">
					    	<label class="10th Passing Year" style="margin-top: -32px;">Diploma University Name</label>
					    
					     	<input type="text" name="diploma_university_name" id="diploma_university_name"  value="<?php echo isset($eduData->sed_diploma_university) ? $eduData->sed_diploma_university : ''; ?>" style="margin-top: 14px;">
					    </div>
					    <div class="col s2" style="margin-top: 21px;width: 111px;">
					    	<label class="10th Passing Year" style="margin-top: -32px;">Specialization </label>
					    
					     	
					     	<input type="text" name="diploma_specialisation" id="diploma_specialisation" value="<?php echo isset($eduData->sed_diploma_specialisation) ? $eduData->sed_diploma_specialisation : ''; ?>" style="margin-top: 14px;">
					    </div>
					     <div class="col s2" style="margin-top: 21px;">
					    	<label class="10th Passing Year" style="margin-top: -34px;">Diploma Certificate </label>
					    	  <div class="file-field input-field">
								
								<div class="btn upload-btn">
									<span>Choose File</span>
									<input type="file" name="diploma_cer" id="diploma_cer" accept="image/jpeg,image/png">
								</div>
								<?php if(isset($eduData->diploma_certi)){ ?>
								<div class="col s10 m5 l5 no-padding-origin">	
								<input class="file-path validate" name="diploma_cer" id="diploma_cer" type="hidden" value="<?php echo isset($eduData->diploma_certi) ? $eduData->diploma_certi : ''; ?>" />
								<img class="square  responsive-img"  id="diploma_1" src="<?php echo base_url(); ?>uploads/certificate/diploma/<?php echo $eduData->diploma_certi; ?>" style="width: 48px;height: 30px;margin-left: 10px;">
								</div>
								<?php }else{ ?>
								<img class="square responsive-img" id="diploma_2" src="" style="width: 48px;height: 30px;margin-left: 10px;">
								<?php  } ?>
							</div>
					    </div>
					  </div>

					 <br>

<!--Graduation-->
	<div class="rows">
		<div class="input-field col s12">
<h6><b>Graduation Details</b></h6>

		</div>
	</div>
 			<div class="rows">
					  	 <div class="input-field col s3">
					    <?php $yearArray = range(2000, 2050); ?>
											<?php $yearArray = range(2000, 2050); ?>
										<select class="browser-default" name="grad_pass_year" id="grad_pass_year" style="margin-top:37px">
											<option value="">Select Year</option>
											<?php
											foreach ($yearArray as $year) {
												if($eduData->sed_graduate_passout_year == $year){
													$selected = 'selected';	
												}else{
													$selected = '';
												}
												echo '<option '.$selected.' value="'.$year.'">'.$year.'</option>';
											}
											?>
										</select>
						<label class="10th Passing Year" style="margin-top: -0px;">Graduation Passing Year</label>
					    </div>
					
					     <div class="input-field col s3" style="width:215px">
					    <?php $perArray = range(35, 100); ?>
										<select class="browser-default" name="grad_percent" id="grad_percent" style="margin-top:37px">
											<option value="">Select Percentage</option>
											<?php
											
											foreach ($perArray as $percentage) {
												
												if($eduData->sed_graduate_percent == $percentage){
													$selected = 'selected';	
												}else{
													$selected = '';
												}
												echo '<option '.$selected.' value="'.$percentage.'%">'.$percentage.'%</option>';
											}
											?>
										</select>
						<label class="10th Passing Year" style="margin-top: 8px;">Graduation Percentage (%)</label>
					    </div>
					    <div class="col s2" style="margin-top:19px">
					    	<label class="10th Passing Year" style="margin-top: -32px;">Graduation College Name</label>
					     	<input type="text" name="grad_college_name" id="grad_college_name" value="<?php echo isset($eduData->sed_graduate_college_name) ? $eduData->sed_graduate_college_name : ''; ?>" style="margin-top: 14px;">
					     	
					    </div>
					    <div class="col s2" style="margin-top:19px">
					    	<label class="10th Passing Year" style="margin-top: -32px;">Graduation University Name</label>
					    <input type="text" name="grad_university_name" id="grad_university_name" value="<?php echo isset($eduData->sed_graduate_university) ? $eduData->sed_graduate_university : ''; ?>" style="margin-top: 14px;">
					     
					    </div>
					    <div class="col s2" style="margin-top:19px;width:111px">
					    	<label class="10th Passing Year" style="margin-top: -32px;">Specialization</label>
					    <input type="text" name="grad_specialisation" id="grad_specialisation" value="<?php echo isset($eduData->sed_graduate_specialisation) ? $eduData->sed_graduate_specialisation : ''; ?>" style="margin-top: 14px;">
					     
					    </div>
					     <div class="col s2" style="margin-top: 21px;padding-top:10px" >
					    	<label class="10th Passing Year" style="margin-top: -34px;">University Certificate </label>
					    	  <div class="file-field input-field">
								
								<div class="btn upload-btn">
									<span>Choose File</span>
									<input type="file" name="univ_cer" id="univ_cer" accept="image/jpeg,image/png">
								</div>
								<?php if(isset($eduData->univer_certi)){ ?>
								<div class="col s10 m5 l5 no-padding-origin">	
								<input class="file-path validate" name="univ_cer" id="univ_cer" type="hidden" value="<?php echo isset($eduData->univer_certi) ? $eduData->univer_certi : ''; ?>" />
								<img class="square  responsive-img" id="uni_1" src="<?php echo base_url(); ?>uploads/certificate/university/<?php echo $eduData->univer_certi; ?>" style="width: 48px;height: 30px;margin-left: 10px;">
								</div>
								<?php }else{ ?>
								<img class="square responsive-img" id="uni_2" src="" style="width: 48px;height: 30px;margin-left: 10px;">
								<?php  } ?>
							</div>
					    </div>
					  </div>

					  <div class="rows">
					  	 <div class="input-field col s3">
					    
					    </div>
					     <div class="input-field col s3">
					    
					    </div>
					    <div class="col s2">
					    	<label class="10th Passing Year" style="margin-top: -32px;">Educational Achievements</label>
					    	<input type="text" name="edu_achieve" id="edu_achieve" value="<?php echo isset($eduData->sed_achievement) ? $eduData->sed_achievement : ''; ?>" style="margin-top: 14px;">
					    
					     	
					    </div>
					    
					  </div>
					  <div class="rows">
					  	 <div class="input-field col s6">
					   <?php if(!isset($eduData)){ ?>
								<button type="submit" class="waves-effect waves-light btn btn-form-submit Searchbtn center-align save" style="margin-top: 100px;align: center;margin-left: 322px;">Add</button>

								<div class="input-field col s6"></div>
								<?php }else{ ?>
								<input type="hidden" name="sed_id" value="<?php echo $eduData->sed_id; ?>">
								<button type="submit" class="waves-effect waves-light btn btn-form-submit Searchbtn center-align update" style="margin-top: 100px;align: center;margin-left: 306px;">Update</button>
								<div class="input-field col s6"></div>
								<?php } ?>
					    </div>
  <a type="button" href="<?php echo base_url('dashboard/student'); ?>" class="waves-effect waves-light btn btn-form-submit Searchbtn center-align " style="margin-top: 548px;align: center;margin-left: -636px;">Cancel</a>
					  </div>
					  
</form>
					
				</div>
			</div>
					</div>
				</form>
				</div>
			</div>					
		</div>
	</div>
</section>
<style>

</style>
<script type="text/javascript" src="<?php echo base_url('assets/js/plugins/jquery-1.11.2.min.js'); ?>"></script>
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/1.3.1/js/toastr.js"></script>
<script>


$('.update_te').click(function(){
	 var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	 var phoneno = /^(([1-9]*)|(([1-9]*).([0-9]*)))$/;  
	 var s=$('#mobile').val();
	 console.log(s);
//diplop or 12 






// $("#ssc_pass_year").css('border-color','green');

// if($('#ssc_pass_year').val()==""){
// 	$('#ssc_pass_year').focus();
// 	toastr.error('Select SSC Passing year');
// 	 $("#ssc_pass_year").css('border-color','red');
// 	return false;
// }else if($('#ssc_percent').val()==""){
// 	$('#ssc_percent').focus();
// 	toastr.error('Select SSC Percentage ');
// 	$("#ssc_pass_year").css('border-color','green');
// 	 $("#ssc_percent").css('border-color','red');
	 
// 	return false;
// }else if($('#ssc_school_name').val()==""){
// 	$('#ssc_school_name').focus();
// 	toastr.error('Enter Ssc school name');
// 	 $("#ssc_school_name").addClass("error");
// 	return false;
// }else if($('#10th_certi').val()==""){
// 	$('#10th_certi').focus();
// 	toastr.error('Please upload ssc Certificate');
// 	return false;
// }
// //college 

// else if($('#hsc_pass_year').val()=="" || $('#diploma_pass_year').val()=="" ){
// 	$('#hsc_pass_year').focus();
// 	toastr.error('Select hsc Passing year');
// 	return false;
// }else if($('#hsc_percent').val()=="" || $('#diploma_percent').val()==""){
// 	$('#hsc_percent').focus();
// 	toastr.error('Select hsc Percentage');
// 	return false;
// }else if($('#hsc_college_name').val()=="" || $('#diploma_college_name').val()==""){
// 	$('#hsc_college_name').focus();
// 	toastr.error('Enter hsc college name');
// 	return false;
// }else if($('#hsc_university_name').val()=="" || $('#diploma_university_name').val()==""){
// 	$('#hsc_university_name').focus();
// 	toastr.error('Enter hsc university name');
// 	return false;
// }else if($('#12th_certi').val()=="" || $('#diploma_specialisation').val()==""){
// 	$('#12th_certi').focus();
// 	toastr.error('Please upload hsc Certificate');
// 	return false;
// }
// //##############################################
// // else if($('#diploma_pass_year').val()==""){
// // 	$('#diploma_pass_year').focus();
// // 	toastr.error('Select diploma passing year');
// // 	return false;
// // }else if($('#diploma_percent').val()==""){
// // 	$('#diploma_percent').focus();
// // 	toastr.error('Select diploma percent');
// // 	return false;
// // }else if($('#diploma_college_name').val()==""){
// // 	$('#diploma_college_name').focus();
// // 	toastr.error('Enter diploma college name');
// // 	return false;
// // }else if($('#diploma_university_name').val()==""){
// // 	$('#diploma_university_name').focus();
// // 	toastr.error('Enter diploma university name');
// // 	return false;
// // }else if($('#diploma_specialisation').val()==""){
// // 	$('#diploma_specialisation').focus();
// // 	toastr.error('Enter diploma specialisation');
// // 	return false;
// // }else if($('#diploma_cer').val()==""){
// // 	$('#diploma_cer').focus();
// // 	toastr.error('Please upload diploma certificate');
// // 	return false;
// // }

// //end 
// else if($('#grad_pass_year').val()==""){
// 	$('#grad_pass_year').focus();
// 	toastr.error('Select grad pass year');
// 	return false;
// }else if($('#grad_percent').val()==""){
// 	$('#grad_percent').focus();
// 	toastr.error('Select grad percent');
// 	return false;
// }else if($('#grad_college_name').val()==""){
// 	$('#grad_college_name').focus();
// 	toastr.error('Enter grad college name');
// 	return false;
// }else if($('#grad_university_name').val()==""){
// 	$('#grad_university_name').focus();
// 	toastr.error('Enter grad university name');
// 	return false;
// }else if($('#grad_specialisation').val()==""){
// 	$('#grad_specialisation').focus();
// 	toastr.error('Enter diploma grad specialisation');
// 	return false;
// }else if($('#univ_cer').val()==""){
// 	$('#univ_cer').focus();
// 	toastr.error('Please upload universitt certificate');
// 	return false;
// }else if($('#edu_achieve').val()==""){
// 	$('#edu_achieve').focus();
// 	toastr.error('Enter education achievement');
// 	return false;
// }else{
	//toastr.success('Update Successfully');
$('input').css('border-color','green');
if($('#ssc_pass_year').val()==""){
	$('#ssc_pass_year').focus();
	toastr.error('Select ssc passing year');
	  $("#ssc_pass_year").css('border-color','red');
	return false;
}else if($('#ssc_percent').val()==""){
	$('#ssc_percent').focus();
	toastr.error('Select ssc percentage ');
	 $("#ssc_pass_year").css('border-color','green');
	 $("#ssc_percent").css('border-color','red');
	return false;
}else if($('#ssc_school_name').val()==""){
	$('#ssc_school_name').focus();
	toastr.error('Enter ssc school name');
	 $("#ssc_school_name").addClass("error");
	  $("#ssc_percent").css('border-color','green');
	 $("#ssc_school_name").css('border-color','red');
	return false;
}else if($('#10th_certi').val()==""){
	$('#10th_certi').focus();
	toastr.error('Please upload ssc certificate');
	 $("#10th_certi").css('border-color','green');
	 $("#ssc_school_name").css('border-color','green');
	return false;
}{
//college 

// else if($('#hsc_pass_year').val()==""){
// 	$('#hsc_pass_year').focus();
// 	toastr.error('Select hsc passing year');
// 	$("#hsc_pass_year").css('border-color','red');
// 	return false;
// }else if($('#hsc_percent').val()=="" ){
// 	$('#hsc_percent').focus();
// 	toastr.error('Select hsc percentage');
// 	$("#hsc_pass_year").css('border-color','green');
// 	$("#hsc_percent").css('border-color','red');
// 	return false;
// }else if($('#hsc_college_name').val()=="" ){
// 	$('#hsc_college_name').focus();
// 	toastr.error('Enter hsc college name');
// 	$("#hsc_percent").css('border-color','green');
// 	return false;
// }else if($('#hsc_university_name').val()==""){
// 	$('#hsc_university_name').focus();
// 	toastr.error('Enter hsc university name');
// 	return false;
// }else if($('#12th_certi').val()=="" ){
// 	$('#12th_certi').focus();
// 	toastr.error('Please upload hsc certificate');
// 	return false;
// }
// //##############################################
// else if($('#diploma_pass_year').val()==""){
// 	$('#diploma_pass_year').focus();
// 	//$("#diploma_pass_year").css('border-color','green');
// 	$("#diploma_pass_year").css('border-color','red');
// 	toastr.error('Select diploma passing year');
// 	return false;
// }else if($('#diploma_percent').val()==""){
// 	$('#diploma_percent').focus();
// 	toastr.error('Select diploma percent');
// 		$("#diploma_pass_year").css('border-color','green');
// 			$("#diploma_percent").css('border-color','red');
// 	return false;
// }else if($('#diploma_college_name').val()==""){
// 	$('#diploma_college_name').focus();
// 	toastr.error('Enter diploma college name');
// 	$("#diploma_percent").css('border-color','green');
// 	return false;
// }else if($('#diploma_university_name').val()==""){
// 	$('#diploma_university_name').focus();
// 	toastr.error('Enter diploma university name');
// 	return false;
// }else if($('#diploma_specialisation').val()==""){
// 	$('#diploma_specialisation').focus();
// 	toastr.error('Enter diploma specialisation');
// 	return false;
// }else if($('#diploma_cer').val()==""){
// 	$('#diploma_cer').focus();
// 	toastr.error('Please upload diploma certificate');
// 	return false;
// }

// //end 
// else if($('#grad_pass_year').val()==""){
// 	$('#grad_pass_year').focus();
// 	toastr.error('Select graduation passing year');

// 	 $("#grad_pass_year").css('border-color','red');
// 	return false;
// }else if($('#grad_percent').val()==""){
// 	$('#grad_percent').focus();
// 	$("#grad_pass_year").css('border-color','green');
// 	$("#grad_percent").css('border-color','red');
// 	toastr.error('Select graduation percent');
// 	return false;
// }else if($('#grad_college_name').val()==""){
// 	$('#grad_college_name').focus();
// 	$("#grad_percent").css('border-color','green');
// 	toastr.error('Enter grad college name');
// 	return false;
// }else if($('#grad_university_name').val()==""){
// 	$('#grad_university_name').focus();
// 	toastr.error('Enter graduation university name');
// 	return false;
// }else if($('#grad_specialisation').val()==""){
// 	$('#grad_specialisation').focus();
// 	toastr.error('Enter diploma grad specialisation');
// 	return false;
// }else if($('#univ_cer').val()==""){
// 	$('#univ_cer').focus();
// 	toastr.error('Please upload universitt certificate');
// 	return false;
// }else if($('#edu_achieve').val()==""){
// 	$('#edu_achieve').focus();
// 	toastr.error('Enter education achievement');
// 	return false;
// }else{
	$('#editForm').submit();
}

	//alert('ih');






	
});

$('.save_teat').click(function(){
 var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	 var phoneno = /^(([1-9]*)|(([1-9]*).([0-9]*)))$/;  
	 var s=$('#mobile').val();
	 console.log(s);
//diplop or 12 







$('input').css('border-color','green');
if($('#ssc_pass_year').val()==""){
	$('#ssc_pass_year').focus();
	toastr.error('Select SSC Passing year');
	  $("#ssc_pass_year").css('border-color','red');
	return false;
}else if($('#ssc_percent').val()==""){
	$('#ssc_percent').focus();
	toastr.error('Select SSC Percentage ');
	 $("#ssc_pass_year").css('border-color','green');
	 $("#ssc_percent").css('border-color','red');
	return false;
}else if($('#ssc_school_name').val()==""){
	$('#ssc_school_name').focus();
	toastr.error('Enter Ssc school name');
	 $("#ssc_school_name").addClass("error");
	  $("#ssc_percent").css('border-color','green');
	 $("#ssc_school_name").css('border-color','red');
	return false;
}else if($('#10th_certi').val()==""){
	$('#10th_certi').focus();
	toastr.error('Please upload ssc Certificate');
	 $("#10th_certi").css('border-color','green');
	 $("#ssc_school_name").css('border-color','green');
	return false;
}{
//college 

// else if($('#hsc_pass_year').val()==""){
// 	$('#hsc_pass_year').focus();
// 	toastr.error('Select hsc Passing year');
// 	$("#hsc_pass_year").css('border-color','red');
// 	return false;
// }else if($('#hsc_percent').val()=="" ){
// 	$('#hsc_percent').focus();
// 	toastr.error('Select hsc Percentage');
// 	$("#hsc_pass_year").css('border-color','green');
// 	$("#hsc_percent").css('border-color','red');
// 	return false;
// }else if($('#hsc_college_name').val()=="" ){
// 	$('#hsc_college_name').focus();
// 	toastr.error('Enter hsc college name');
// 	$("#hsc_percent").css('border-color','green');
// 	return false;
// }else if($('#hsc_university_name').val()==""){
// 	$('#hsc_university_name').focus();
// 	toastr.error('Enter hsc university name');
// 	return false;
// }else if($('#12th_certi').val()=="" ){
// 	$('#12th_certi').focus();
// 	toastr.error('Please upload hsc Certificate');
// 	return false;
// }
// //##############################################
// else if($('#diploma_pass_year').val()==""){
// 	$('#diploma_pass_year').focus();
// 	//$("#diploma_pass_year").css('border-color','green');
// 	$("#diploma_pass_year").css('border-color','red');
// 	toastr.error('Select diploma passing year');
// 	return false;
// }else if($('#diploma_percent').val()==""){
// 	$('#diploma_percent').focus();
// 	toastr.error('Select diploma percent');
// 		$("#diploma_pass_year").css('border-color','green');
// 			$("#diploma_percent").css('border-color','red');
// 	return false;
// }else if($('#diploma_college_name').val()==""){
// 	$('#diploma_college_name').focus();
// 	toastr.error('Enter diploma college name');
// 	$("#diploma_percent").css('border-color','green');
// 	return false;
// }else if($('#diploma_university_name').val()==""){
// 	$('#diploma_university_name').focus();
// 	toastr.error('Enter diploma university name');
// 	return false;
// }else if($('#diploma_specialisation').val()==""){
// 	$('#diploma_specialisation').focus();
// 	toastr.error('Enter diploma specialisation');
// 	return false;
// }else if($('#diploma_cer').val()==""){
// 	$('#diploma_cer').focus();
// 	toastr.error('Please upload diploma certificate');
// 	return false;
// }

// //end 
// else if($('#grad_pass_year').val()==""){
// 	$('#grad_pass_year').focus();
// 	toastr.error('Select grad pass year');

// 	 $("#grad_pass_year").css('border-color','red');
// 	return false;
// }else if($('#grad_percent').val()==""){
// 	$('#grad_percent').focus();
// 	$("#grad_pass_year").css('border-color','green');
// 	$("#grad_percent").css('border-color','red');
// 	toastr.error('Select grad percent');
// 	return false;
// }else if($('#grad_college_name').val()==""){
// 	$('#grad_college_name').focus();
// 	$("#grad_percent").css('border-color','green');
// 	toastr.error('Enter grad college name');
// 	return false;
// }else if($('#grad_university_name').val()==""){
// 	$('#grad_university_name').focus();
// 	toastr.error('Enter grad university name');
// 	return false;
// }else if($('#grad_specialisation').val()==""){
// 	$('#grad_specialisation').focus();
// 	toastr.error('Enter diploma grad specialisation');
// 	return false;
// }else if($('#univ_cer').val()==""){
// 	$('#univ_cer').focus();
// 	toastr.error('Please upload universitt certificate');
// 	return false;
// }else if($('#edu_achieve').val()==""){
// 	$('#edu_achieve').focus();
// 	toastr.error('Enter education achievement');
// 	return false;
// }else{
	//toastr.success('Update Successfully');

	$('#saveForm').submit();
}

	//alert('ih');
	
});

$('#country').change(function(){
	var country = $(this).val();
	$('#state').isset();
	$.ajax({
		type: "POST",
		url: "<?php echo base_url('student/getAjaxState'); ?>",
		data:{country:country},
		success: function(response){
			var res = JSON.parse(response);
			$('#state').append('<option value="" disabled selected>Select State</option>');
			$.each(res, function(key, value) {   
				 $('#state')
					 .append($("<option></option>")
								.attr("value",value.sm_id)
								.text(value.sm_name)); 
			});
		}
	});
});

$('#state').change(function(){
	var state = $(this).val();
	$('#city').isset();
	$.ajax({
		type: "POST",
		url: "<?php echo base_url('student/getAjaxCity'); ?>",
		data:{state:state},
		success: function(response){
			var res = JSON.parse(response);
			$('#city').append('<option value="" disabled selected>Select City</option>');
			$.each(res, function(key, value) {   
				 $('#city')
					 .append($("<option></option>")
								.attr("value",value.cim_id)
								.text(value.cim_name)); 
			});
		}
	});
});
$('#tenth_2').hide();

 function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            // $('#blah1').hide();
            reader.onload = function (e) {
            	
            	$('#tenth_1').show();
                $('#tenth_1').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }

     function readURL_12(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            // $('#blah1').hide();
            reader.onload = function (e) {
            	
            	$('#tenth_2').show();
                $('#tenth_2').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    $("#10th_certi").change(function(){

        readURL(this);
    });

    $("#10th_certi").change(function(){

        readURL_12(this);
    });

//Twelth

$('#twelth_2').hide();

 function readURL_13(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            // $('#blah1').hide();
            reader.onload = function (e) {
            	
            	$('#twelth_1').show();
                $('#twelth_1').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }
 $("#12th_certi").change(function(){

        readURL_13(this);
    });

     function readURL_14(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            // $('#blah1').hide();
            reader.onload = function (e) {
            	
            	$('#twelth_2').show();
                $('#twelth_2').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    $("#12th_certi").change(function(){

        readURL_14(this);
    });

//Diploma code

$('#diploma_2').hide();

 function readURL_15(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            // $('#blah1').hide();
            reader.onload = function (e) {
            	
            	$('#diploma_1').show();
                $('#diploma_1').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }
 $("#diploma_cer").change(function(){

        readURL_15(this);
    });

     function readURL_16(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            // $('#blah1').hide();
            reader.onload = function (e) {
            	
            	$('#diploma_2').show();
                $('#diploma_2').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    $("#diploma_cer").change(function(){

        readURL_16(this);
    });
//deegri

//Diploma code

$('#uni_2').hide();

 function readURL_18(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            // $('#blah1').hide();
            reader.onload = function (e) {
            	
            	$('#uni_1').show();
                $('#uni_1').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }
 $("#univ_cer").change(function(){

        readURL_18(this);
    });

     function readURL_19(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            // $('#blah1').hide();
            reader.onload = function (e) {
            	
            	$('#uni_2').show();
                $('#uni_2').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    $("#univ_cer").change(function(){

        readURL_19(this);
    });



</script>