<style>
a.active {
    background: #fff;
}
.tab-icon{
	font-size: 28px;
    vertical-align: middle;
}
.admission-form-remove-fields{
	padding-top: 10px;
}
.file-field input[type=file] {
    width: 77%;
}
</style>
           
            <section id="content"> 
			
			<?php //print_r($userData); ?>	
	
	<div class="row"  id="side-menu-three-div">
		<div class="col s12 m8 l8 offset-l3 offset-m4">
			<div class="row row-margin-bottom">
				<div class="Space30 col l12 m12 s12"></div>
				<div class="col s12 m12 l12">
				<h4 class="admission-form-heading-margin">Personal Details</h4>
				</div>
				<div class="col s12 m3 l3"></div>	
			</div>	
		</div>
		<div class="col s12 m12 l12" style="margin-top: -6.8px;">		
			<div class="card" >
				<div class="col s12 l12 m12 Space40"></div>
				<div class="col s12 m12 l12">
				<?php if(!isset($personalData)){ ?>
				<form method="post" action="<?php echo base_url('student/personal_details'); ?>" enctype="multipart/form-data">
				<?php }else{ ?>
				<form method="post" action="<?php echo base_url('student/edit_personal_details'); ?>" enctype="multipart/form-data">
				<?php } ?>
					<div class="row">
						<?php 
						$uname = explode(' ',$userData['name']); 
						$firstname = '';
						$middlename = '';
						$lastname = '';
						$email = '';
						if($personalData){
							
							$firstname = $personalData->spd_firstname;
							$middlename = $personalData->spd_middlename;
							$lastname = $personalData->spd_lastname;
							$email = $personalData->spd_email;
						}else{
							if(count($uname)== 2){
								$firstname = $uname[0];
								$lastname = $uname[1];
								$email = $userData['email'];
							}else if(count($uname)== 3){
								$firstname = $uname[0];
								$middlename = $uname[1];
								$lastname = $uname[2];
								$email = $userData['email'];
							}
						}
						?>
						<div class="col s12 m4 l4">
							<div class="row">
								<div class="col s12 m4 l4">
									<label class="NameLable">First Name</label>
								</div>
								<div class="col s10 m6 l6 no-padding-origin">
									<input type="text" name="firstname" value="<?php echo $firstname; ?>">
								</div>
							</div>
						</div>
						<div class="col s12 m4 l4">
							<div class="row">
								<div class="col s12 m4 l4">
									<label class="NameLable">Middle Name</label>
								</div>
								<div class="col s10 m6 l6 no-padding-origin">
									<input type="text" name="middlename" value="<?php echo $middlename; ?>">
								</div>
							</div>
						</div>
						<div class="col s12 m4 l4">
							<div class="row">
								<div class="col s12 m4 l4">
									<label class="NameLable">Last Name</label>
								</div>
								<div class="col s10 m6 l6 no-padding-origin">
									<input type="text" name="lastname" value="<?php echo $lastname; ?>">
								</div>
							</div>
						</div>
						<div class="col s12 m4 l4">
							<div class="row">
								<div class="col s12 m4 l4">
									<label class="NameLable">Email</label>
								</div>
								<div class="col s10 m6 l6 no-padding-origin">
									<input type="text" name="email" value="<?php echo $email; ?>">
								</div>
							</div>
						</div>
						<div class="col s12 m4 l4">
							<div class="row">
								<div class="col s12 m4 l4">
									<label class="NameLable">Mobile</label>
								</div>
								<div class="col s10 m6 l6 no-padding-origin">
									<input type="text" name="mobile" value="<?php echo isset($personalData->spd_mobile) ? $personalData->spd_mobile : '';?>">
								</div>
							</div>
						</div>
						<div class="col s12 m4 l4">
							<div class="row">
								<div class="col s12 m4 l4">
									<label class="NameLable">Birth Date</label>
								</div>
								<div class="col s10 m6 l6 no-padding-origin">
									<input type="text" name="birthdate" placeholder="--/--/----" value="<?php echo isset($personalData->spd_dob) ? $personalData->spd_dob : '';?>">
								</div>
							</div>
						</div>
						<div class="col s12 m4 l4">
							<div class="row">
								<div class="col s12 m4 l4">
									<label class="NameLable">Gender</label>
								</div>
								<div class="col s10 m6 l6 no-padding-origin">
									  <?php if(isset($personalData->spd_gender)){ ?>
									  <input name="gender" type="radio" id="test1" <?php echo ($personalData->spd_gender == 'Male') ? 'checked="checked"' : ''; ?> value="Male" />
									  <label for="test1">Male</label>
									  <input name="gender" type="radio" id="test2" <?php echo ($personalData->spd_gender == 'Female') ? 'checked="checked"' : ''; ?> value="Female"/>
									  <label for="test2">Female</label>
									  <?php }else{ ?>
									  <input name="gender" type="radio" id="test1"  value="Male" />
									  <label for="test1">Male</label>
									  <input name="gender" type="radio" id="test2"  value="Female"/>
									  <label for="test2">Female</label>
									  <?php } ?>
								</div>
							</div>
						</div>
						<div class="col s12 m4 l4 ">
							<div class="row file-field input-field">
								<div class="col s12 m4 l4">
								<div class="btn upload-btn">
									<span>File</span>
									<input type="file" name="profile_pic">
								</div>
								</div>
								<div class="col s10 m5 l5 no-padding-origin">	
								<input class="file-path validate" name="file_selected" type="text" value="<?php echo isset($personalData->spd_profile_pic) ? $personalData->spd_profile_pic : '';?>" />
								</div>
							</div>
						</div>
						<div class="col s12 m4 l4">
							<div class="row">
								<div class="col s12 m4 l4" >
									<label class="NameLable">Country</label>
								</div>
								<div class="col s10 m6 l6 no-padding-origin">
									<select class="browser-default" name="country" id="country">
										<option value="" disabled selected>Select Country</option>
										<option <?php echo empty($personalData)? '' : ($personalData->spd_cm_id == '100') ? 'selected="selected"' : '';?> value="100">India</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col s12 m4 l4">
							<div class="row">
								<div class="col s12 m4 l4">
									<label class="NameLable">State</label>
								</div>
								<div class="col s10 m6 l6 no-padding-origin">
									<select class="browser-default" id="state" name="state">
										<option value="">Select State</option>
										<?php foreach($statedata as $sdata){ ?>
											<option <?php echo ($personalData->spd_sm_id == $sdata->sm_id) ? 'selected="selected"' : ''; ?> value="<?php echo $sdata->sm_id; ?>"><?php echo $sdata->sm_name; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
						</div>
						<div class="col s12 m4 l4">
							<div class="row">
								<div class="col s12 m4 l4">
									<label class="NameLable">City</label>
								</div>
								<div class="col s10 m6 l6 no-padding-origin">
									<select class="browser-default" id="city" name="city">
										<option value="">Select City</option>
										<?php foreach($citydata as $cdata){ ?>
											<option <?php echo ($personalData->spd_cim_id == $cdata->cim_id) ? 'selected="selected"' : ''; ?> value="<?php echo $cdata->cim_id; ?>"><?php echo $cdata->cim_name; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
						</div>
						<div class="col s12 m4 l4">
							<div class="row">
								<div class="col s12 m4 l4">
									<label class="NameLable">Pincode</label>
								</div>
								<div class="col s10 m6 l6 no-padding-origin">
									<input type="text" name="pincode" value="<?php echo isset($personalData->spd_pincode) ? $personalData->spd_pincode : '';?>">
								</div>
							</div>
						</div>
						<div class="col s12 m4 l4">
							<div class="row">
								<div class="col s12 m4 l4">
									<label class="NameLable">Address</label>
								</div>
								<div class="col s10 m6 l6 no-padding-origin">
									<textarea row="1" name="address"><?php echo isset($personalData->spd_address) ? $personalData->spd_address : '';?></textarea>
								</div>
							</div>
						</div>
						<div class="col s12 m4 l4">
							<div class="row">
								<div class="col s12 m4 l4">
									<label class="NameLable">About You</label>
								</div>
								<div class="col s10 m6 l6 no-padding-origin">
								<textarea row="1" name="about_you"><?php echo isset($personalData->spd_about_you) ? $personalData->spd_about_you : '';?></textarea>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col s12 m12 l12 center-align">
								<?php if(empty($personalData)){ ?>
								<button type="submit" class="waves-effect waves-light btn btn-form-submit Searchbtn center-align">Add</button>
								<div class="col s12 m12 l12 Space10"></div>
								<?php }else{ ?>
								<input type="hidden" name="spd_id" value="<?php echo $personalData->spd_id; ?>">
								<button type="submit" class="waves-effect waves-light btn btn-form-submit Searchbtn center-align">Update</button>
								<div class="col s12 m12 l12 Space10"></div>
								<?php } ?>
							</div>
						</div>
					</div>
				</form>
				</div>
			</div>					
		</div>
	</div>
</section>
<script type="text/javascript" src="<?php echo base_url('assets/js/plugins/jquery-1.11.2.min.js');?>"></script>
<script>

$('#country').change(function(){
	var country = $(this).val();
	$('#state').empty();
	$.ajax({
		type: "POST",
		url: "<?php echo base_url('student/getAjaxState'); ?>",
		data:{country:country},
		success: function(response){
			var res = JSON.parse(response);
			$('#state').append('<option value="" disabled selected>Select State</option>');
			$.each(res, function(key, value) {   
				 $('#state')
					 .append($("<option></option>")
								.attr("value",value.sm_id)
								.text(value.sm_name)); 
			});
		}
	});
});

$('#state').change(function(){
	var state = $(this).val();
	$('#city').empty();
	$.ajax({
		type: "POST",
		url: "<?php echo base_url('student/getAjaxCity'); ?>",
		data:{state:state},
		success: function(response){
			var res = JSON.parse(response);
			$('#city').append('<option value="" disabled selected>Select City</option>');
			$.each(res, function(key, value) {   
				 $('#city')
					 .append($("<option></option>")
								.attr("value",value.cim_id)
								.text(value.cim_name)); 
			});
		}
	});
});
</script>