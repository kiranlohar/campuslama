
<style>
.loaded {
    background-color: #FFF !important;
}
p.step {
    color: #d40000;
}

</style>
<section id="content">
    <div class="container">
		<div class="col s12 m12 l12" id="Uonetab">
			<div class="col s12 m12 l12 Space10"></div>
			<div class="row">
				<div class="invoice-lable">
					<div class="col s12 m2 l2 cyan1 customBorder cust-cursor">
						<div class="row HeaderBg ">
						  <div class="col s12 m12 l12">
						   <div class="row Space20 switch-tab1"></div>
						   <div class="row switch-tab1 center-align ">	
							   <div class="col s1 l2 m2 switch-tab2 offset-s4 offset-l4 offset-m4">
							  <img class="switch-tab3" 
							  src="<?php echo base_url('assets/images/switch-button-icons-02.png'); ?>" style="    margin-left: -21px;
margin-top: -2px;" id="Student-applied"/>
							<div class="center-align applied-margin" id="firstactive">Applied</div>
							  </div>
							  <div class="col s1 l2 m2 switch-tab4">
							  <img class="switch-tab3"   src="<?php echo base_url('assets/images/switch-button-icons-01.png'); ?>" style="margin-left: -15px;
margin-top: -2px;" id="Student-shortlist"/>
							<div class="center-align shortlist-margin" id="switchactive">Shortlist</div>
							  </div>							  
						   </div>											   
						   
						   <div class="col s12 m12 l12 Space20"></div>
						  </div>
						  <div class="col s12 m12 l12 Space20"></div>
						</div>
					</div>
					<?php 
				$naCount = 0;
				$ipCount =0;
				$rejCount =0;
				$apprCount =0;
				$fpCount =0;
				$adpCount =0;
				$adoneCount =0;
				
				foreach($courseApp as $daf_app){ 
					$appStatus = $this->student_model->getAppStatus($daf_app->sa_id);
					//echo $appStatus->as_status==1;
						if($appStatus->as_status == 4){
							$ipCount++;	
						}else if($appStatus->as_status == 1){
							$naCount++;
						}else if($appStatus->as_status == 8){
							$rejCount++;	
						}else if($appStatus->as_status == 5){
							$apprCount++;					
						}else if($appStatus->as_status == 6){
							$fpCount++;
						}else if($appStatus->as_status == 3){
							$adpCount++;
						}else if($appStatus->as_status == 7){
							$adoneCount++;
						}
					}
					?>
					<div class="col s12 m10 l10 invoice-brief cyan1 white-text ">
					  <div class="row">
						<div class="col s12 m6 l6" id="applied-college-one">
						  <div class="row">
							<div class="col s6 m3 l3 center-align customBorder cust-cursor">
							  <p class="header step center-align"><b><?php echo empty($courseApp)? '0' : count($courseApp); ?></b></p>
							  <h6 class="strong center-align">Total Application</h6>
							 <div class="col s12 m12 l12 Space10"></div>
							  <div class="col s12 m12 l12 Space6"></div>
							</div>
							<div class="col s6 m3 l3 center-align customBorder cust-cursor">
							  <p class="header step center-align"><b><?php echo  $adpCount;?></b></p>
							  <h6 class="strong center-align">Under Review</h6>
							  <div class="col s12 m12 l12 Space10"></div>
							  <div class="col s12 m12 l12 Space6"></div>
							</div>
							<div class="col s6 m3 l3 center-align customBorder cust-cursor">
							  <p class="header step center-align"><b><?php echo  $adpCount;?></b></p>
							  <h6 class="strong center-align">Acknowledge</h6>
							  <div class="col s12 m12 l12 Space10"></div>
							  <div class="col s12 m12 l12 Space6"></div>
							</div>
							<div class="col s6 m3 l3 center-align customBorder cust-cursor">
							  <p class="header step center-align"><b><?php echo $ipCount;?></b></p>
							  <h6 class="strong center-align">In Process</h6>
							  <div class="col s12 m12 l12 Space10"></div>
							  <div class="col s12 m12 l12 Space6"></div>
							</div>
						 </div>
						</div>
						<div class="col s12 m6 l6" id="applied-college-two">
						 <div class="row">
							<div class="col s12 m3 l3 center-align customBorder cust-cursor">
							  <p class="header step center-align"><b><?php echo $fpCount;?></b></p>
							  <h6 class="strong center-align">Approved</h6>
							  <div class="col s12 m12 l12 Space10"></div>
							  <div class="col s12 m12 l12 Space6"></div>
							</div>
							<div class="col s12 m9 l9 center-align customBorder cust-cursor">
								<!-- <div class="row topBG">
								<div class="col s12 m12 l12 Space20"></div>
																		
									
								<div class="col s12 m12 l12 Space10"></div>
								<div class="col s12 m12 l12 Space10"></div>
								</div> -->	
							</div>
						 </div>
						</div>
						<div class="col s12 m12 l12" id="shortlisted-college">
							<div class="row">
							<div class="col s12 m6 l6">
							  <div class="row">
								<div class="col s6 m3 l3 center-align customBorder cust-cursor">
								  <p class="header step center-align"><b>0</b></p>
								  <h6 class="strong center-align">MBA</h6>
									<div class="col s12 m12 l12 Space10"></div>
								  <div class="col s12 m12 l12 Space6"></div>
								</div>
								<div class="col s6 m3 l3 center-align customBorder cust-cursor">
								  <p class="header step center-align"><b>0</b></p>
								  <h6 class="strong center-align">M.Sc</h6>
									<div class="col s12 m12 l12 Space10"></div>
								  <div class="col s12 m12 l12 Space6"></div>
								</div>
								<div class="col s6 m3 l3 center-align customBorder cust-cursor">
								  <p class="header step center-align"><b>0</b></p>
								  <h6 class="strong center-align">MCA</h6>
								  <div class="col s12 m12 l12 Space10"></div>
								  <div class="col s12 m12 l12 Space6"></div>
								</div>
								<div class="col s6 m3 l3 center-align customBorder cust-cursor">
								  <p class="header step center-align"><b><?php echo empty($courseShort)? '0' : count($courseShort); ?></b></p>
								  <h6 class="strong center-align">In Process</h6>
								  <div class="col s12 m12 l12 Space10"></div>
								  <div class="col s12 m12 l12 Space6"></div>
								</div>
							 </div>
							</div>
								<div class="col s12 m6 l6">
								  <div class="row">
									<div class="col s6 m3 l3 center-align customBorder cust-cursor">
									  <p class="header step center-align"><b>0</b></p>
									  <h6 class="strong center-align">Approved</h6>
									  <div class="col s12 m12 l12 Space10"></div>
									  <div class="col s12 m12 l12 Space6"></div>
									</div>
									<div class="col s6 m3 l3 center-align customBorder cust-cursor">
									  <p class="header step center-align"><b>0</b></p>
									  <h6 class="strong center-align">Pay Fees</h6>
									  <div class="col s12 m12 l12 Space10"></div>
									  <div class="col s12 m12 l12 Space6"></div>
									</div>
									<div class="col s12 m6 l6 center-align customBorder cust-cursor">
										<!-- <div class="row topBG">
											<div class="col s12 m12 l12 Space10"></div>
											<div class="col s12 m12 l12 topBG">
											<img src="<?php echo base_url('assets/images/collage-logo-01.jpg'); ?>" alt="">
											</div>										
											<div class="col s12 m12 l12 topBG student-admission-done no-padding">
											Admission Done<br/>
											</div>
											<div class="col s12 m12 l12 Space10"></div>	
										</div> -->	
									</div>
								 </div>
								</div>
							</div>
						</div>
					  </div>
					</div>  
				</div>
					
				<div class="col s12 m12 l12" id="applied-student">

					<?php $count=1;
					//echo '<pre>',print_r($courseApp);
					if($courseApp){
					foreach($courseApp as $capp){
					
				//	$mainCom = $this->student_model->getCourseByComId($capp->com_parent);
					
				///	$spe = $this->student_model->getSpeByComId($capp->uc_com_id);
					//echo '<pre>',print_r($capp);
					$appStatus = $this->student_model->getAppStatus($capp->sa_id);
//echo '<pre>',print_r($appStatus);					
					?>
					<div class="row student-row-br-color"> 
					  <div class="col s2 m12 l12 Space20"></div>
						<div class="col s4 m1 l1 center-align"><?php echo $count++;?></div>
						<div class="col s6 m1 l1 center-align">
							<img src="<?php echo base_url('uploads/logos/'.$capp->ui_logoimage); ?>" style="width: 100px;; " alt="">
						</div>
						<div class="col s12 m3 l3 clgColor">
								<?php $spe1=''; if(!empty($capp->course_spe)) {
									$spe1= ' - '.$capp->course_spe;

								}else{
									$spe1= '';
								}
								?>
							<?php echo $capp->spe .''.$spe1; ?>
							<?php //if(isset($capp))echo $capp->spe .' - '.$capp->course_spe; ?><br/>
							<span class="Top-uni-name-span"><?php echo $capp->um_name; ?></span>
						</div>
						<?php if($appStatus->as_status == 1){ ?>
						<div class="col s6 m1 l1 ">
							<div class="row "> 
								<div class="col s12 m12 l12 center-align">
								<div class="row">
								<?php if($appStatus->as_status == 1){?>
									<div class="col l6 m6 s12 ">
									<img src="<?php echo base_url('assets/images/timeline-icons-green-1.png'); ?>"/>
									</div>
								<?php }else {?>
									<div class="col l6 m6 hide-on-small-only dotted-line-img ">
									<img src="<?php echo base_url('assets/images/timeline-icons-1.png'); ?>"/>
									</div>
								<?php }?>
								
								</div>									
						
								</div>
								<div class="col s12 m12 l12 student-application-status center-align">
								Applied On
								</div>
								<div class="col s12 m12 l12 student-application-status-date center-align">
								<?php echo date('j M Y',strtotime($appStatus->updated_at)); ?>
								</div>
								
							</div>
						</div>
						<?php } if($appStatus->as_status >= 2){ ?>
						<div class="col s6 m1 l1 ">
							<div class="row "> 
								<div class="row">
									<div class="col l6 m6 s12 center-align ">
									<img src="<?php echo base_url('assets/images/timeline-icons-green-2.png'); ?>"/>
									</div>
									<div class="col l6 m6 hide-on-small-only ">
									<img src="<?php echo base_url('assets/images/dashed-icon-01.png'); ?>"/>
									</div>
									
								</div>		
								<div class="col s12 m12 l12 student-application-status center-align">
								Under Review
								</div>
								<div class="col s12 m12 l12 student-application-status-date center-align">
								<?php echo date('j M Y',strtotime($appStatus->updated_at)); ?>
								</div>
							</div>
						</div>
						<?php } if($appStatus->as_status >= 3){ ?>
						<div class="col s6 m1 l1 ">
							<div class="row "> 
								<div class="row">
									<div class="col l6 m6 s12 center-align ">
									<img src="<?php echo base_url('assets/images/timeline-icons-green-3.png'); ?>"/>
									</div>
									<div class="col l6 m6 hide-on-small-only ">
									<img src="<?php echo base_url('assets/images/dashed-icon-01.png'); ?>"/>
									</div>
								</div>
								<div class="col s12 m12 l12 student-application-status center-align">
								Acknowledge
								</div>
								<div class="col s12 m12 l12 student-application-status-date center-align">
								<?php echo date('j M Y',strtotime($appStatus->updated_at)); ?>
								</div>
							</div>
						</div>
						<?php } if($appStatus->as_status >= 4){ ?>
						<div class="col s6 m1 l1 ">
							<div class="row "> 
								<div class="row">
									<div class="col l6 m6 s12 center-align ">
									<img src="<?php echo base_url('assets/images/timeline-icons-green-4.png'); ?>"/>
									</div>
									<div class="col l6 m6 hide-on-small-only ">
									<img src="<?php echo base_url('assets/images/dashed-icon-01.png'); ?>"/>
									</div>
								</div>
								<div class="col s12 m12 l12 student-application-status center-align">
								In Process
								</div>
								<div class="col s12 m12 l12 student-application-status-date center-align">
								<?php echo date('j M Y',strtotime($appStatus->updated_at)); ?>
								</div>
							</div>
						</div>
						<?php } if($appStatus->as_status >= 5){ ?>
						<div class="col s6 m1 l1 ">
							<div class="row "> 
								<div class="row">
									<div class="col l6 m6 s12 center-align ">
									<img src="<?php echo base_url('assets/images/timeline-icons-green-5.png'); ?>"/>
									</div>
									<div class="col l6 m6 hide-on-small-only ">
									<img src="<?php echo base_url('assets/images/dashed-icon-01.png'); ?>"/>
									</div>
								</div>
								<div class="col s12 m12 l12 student-application-status center-align">
								Approved
								</div>
								<div class="col s12 m12 l12 student-application-status-date center-align">
								<?php echo date('j M Y',strtotime($appStatus->updated_at)); ?>
								</div>
							</div>
						</div>
						<?php } if($appStatus->as_status == 6){ ?>
						<div class="col s6 m1 l1 ">
							<div class="row "> 
								<div class="row">
									<div class="col l6 m6 s12 center-align ">
									<img src="<?php echo base_url('assets/images/timeline-icons-green-6.png'); ?>"/>
									</div>
									<div class="col l6 m6 hide-on-small-only ">
									<img src="<?php echo base_url('assets/images/dashed-icon-01.png'); ?>"/>
									</div>
								</div>
								<div class="col s12 m12 l12 student-application-status feespaid center-align">
								Fees Paid
								</div>
								<div class="col s12 m12 l12 student-application-status-date center-align">
								<?php echo date('j M Y',strtotime($appStatus->updated_at)); ?>
								</div>
							</div>
						</div>
						<?php } if($appStatus->as_status == 7){ ?>
						<div class="col s6 m1 l1 ">
							<div class="row "> 
								<div class="row">
									<div class="col l12 m12 s12 center-align ">
									<img src="<?php echo base_url('assets/images/timeline-icons-green-7.png'); ?>"/>
									</div>
								</div>
								<div class="col s12 m12 l12 student-application-status center-align">
								Admission Done
								</div>
								<div class="col s12 m12 l12 student-application-status-date center-align">
								<?php echo date('j M Y',strtotime($appStatus->updated_at)); ?>
								</div>
							</div>
						</div>
						<?php } if($appStatus->as_status == 8){ ?>
								<div class="col s6 m1 l1 ">
							<div class="row "> 
								<div class="row">
									<div class="col l12 m12 s12 center-align ">
									<img src="<?php echo base_url('assets/img/admission-tracker-icon-03.png'); ?>"/>
									</div>
								</div>
								<div class="col s12 m12 l12 student-application-status center-align">
								Admission Rejected
								</div>
								<div class="col s12 m12 l12 student-application-status-date center-align">
								<?php echo date('j M Y',strtotime($appStatus->updated_at)); ?>
								</div>
							</div>
						</div>	
						<?php } ?>
					  </div>
					<?php } 
					}?> 
				</div>
				<div class="col s12 m12 l12" id="shortlist-student">
					<?php $count=1;
					$userdata = $this->session->userdata('logged_in');
					if($courseShort){
					foreach($courseShort as $cshort){ 
				
					$user_id=$userdata['id'];
								
					$course_id=$cshort->uc_id;
					$query = $this->db->get_where('student_mstr',array('stm_user_id'=>$user_id));
					$smdata=$query->row();
					$userid=$smdata->stm_id;
					$query_1 = $this->db->get_where('student_applications',array('sa_stm_id'=>$userid,'sa_uc_id'=>$course_id));
					$query = $this->db->get_where('shortlisted_courses',array('sc_stm_id'=>$userid,'sc_uc_id'=>$course_id));
							
					if(!$query_1->num_rows() > 0 )
								{ 
							if($query->num_rows() > 0 )
								{ 
					//echo '<pre>',print_r($cshort);
					
					$shortStatus = $this->student_model->getShortStatus($cshort->sc_id);
					?>
					<div class="row student-row-br-color"> 
					  <div class="col s2 m12 l12 Space20"></div>
						<div class="col s4 m1 l1 center-align"><?php echo  $count++;?></div>
						<div class="col s6 m1 l1 center-align">
							<img src="<?php echo base_url('uploads/logos/'.$cshort->ui_logoimage); ?>" style="width:100px; " alt="">
						</div>
						<div class="col s12 m3 l3 clgColor">
							<?php $spe1=''; if(!empty($cshort->course_spe)) {
									$spe1= ' - '.$cshort->course_spe;

								}else{
									$spe1= '';
								}
								?>
							<?php echo $cshort->spe .''.$spe1; ?><br/>
							<span class="Top-uni-name-span"><?php echo $cshort->um_name; ?></span>
						</div>
						<?php if($shortStatus->as_status == 1){ ?>
						<div class="col s6 m1 l1 ">
							<div class="row "> 
								<div class="col s12 m12 l12 center-align">
								<div class="row">
									<div class="col l6 m6 s12 ">
									<img src="<?php echo base_url('assets/images/timeline-icons-green-1.png'); ?>"/>
									</div>
									<div class="col l6 m6 hide-on-small-only dotted-line-img ">
									<img src="<?php echo base_url('assets/images/dashed-icon-01.png'); ?>"/>
									</div>
								</div>									
						
								</div>
								<div class="col s12 m12 l12 student-application-status center-align">
								Shortlisted On
								</div>
								<div class="col s12 m12 l12 student-application-status-date center-align">
								<?php echo date('j M Y',strtotime($shortStatus->updated_at)); ?>
								</div>
							</div>
						</div>
						<?php } if($shortStatus->as_status == 2){ ?>
						<div class="col s6 m1 l1 ">
							<div class="row "> 
								<div class="row">
									<div class="col l6 m6 s12 center-align ">
									<img src="<?php echo base_url('assets/images/timeline-icons-green-2.png'); ?>"/>
									</div>
									<div class="col l6 m6 hide-on-small-only ">
									<img src="<?php echo base_url('assets/images/dashed-icon-01.png'); ?>"/>
									</div>
								</div>		
								<div class="col s12 m12 l12 student-application-status center-align">
								Under Review
								</div>
								<div class="col s12 m12 l12 student-application-status-date center-align">
								<?php echo date('j M Y',strtotime($shortStatus->updated_at)); ?>
								</div>
							</div>
						</div>
						<?php } if($shortStatus->as_status == 3){ ?>
						<div class="col s6 m1 l1 ">
							<div class="row "> 
								<div class="row">
									<div class="col l6 m6 s12 center-align ">
									<img src="<?php echo base_url('assets/images/timeline-icons-green-3.png'); ?>"/>
									</div>
									<div class="col l6 m6 hide-on-small-only ">
									<img src="<?php echo base_url('assets/images/dashed-icon-01.png'); ?>"/>
									</div>
								</div>
								<div class="col s12 m12 l12 student-application-status center-align">
								Acknowledge
								</div>
								<div class="col s12 m12 l12 student-application-status-date center-align">
								<?php echo date('j M Y',strtotime($shortStatus->updated_at)); ?>
								</div>
							</div>
						</div>
						<?php } if($shortStatus->as_status == 4){ ?>
						<div class="col s6 m1 l1 ">
							<div class="row "> 
								<div class="row">
									<div class="col l6 m6 s12 center-align ">
									<img src="<?php echo base_url('assets/images/timeline-icons-green-4.png'); ?>"/>
									</div>
									<div class="col l6 m6 hide-on-small-only ">
									<img src="<?php echo base_url('assets/images/dashed-icon-01.png'); ?>"/>
									</div>
								</div>
								<div class="col s12 m12 l12 student-application-status center-align">
								In Process
								</div>
								<div class="col s12 m12 l12 student-application-status-date center-align">
								<?php echo date('j M Y',strtotime($shortStatus->updated_at)); ?>
								</div>
							</div>
						</div>
						<?php } if($shortStatus->as_status == 5){ ?>
						<div class="col s6 m1 l1 ">
							<div class="row "> 
								<div class="row">
									<div class="col l6 m6 s12 center-align ">
									<img src="<?php echo base_url('assets/images/timeline-icons-green-5.png'); ?>"/>
									</div>
									<div class="col l6 m6 hide-on-small-only ">
									<img src="<?php echo base_url('assets/images/dashed-icon-01.png'); ?>"/>
									</div>
								</div>
								<div class="col s12 m12 l12 student-application-status center-align">
								Approved
								</div>
								<div class="col s12 m12 l12 student-application-status-date center-align">
								<?php echo date('j M Y',strtotime($shortStatus->updated_at)); ?>
								</div>
							</div>
						</div>
						<?php } if($shortStatus->as_status == 6){ ?>
						<div class="col s6 m1 l1 ">
							<div class="row "> 
								<div class="row">
									<div class="col l6 m6 s12 center-align ">
									<img src="<?php echo base_url('assets/images/timeline-icons-green-6.png'); ?>"/>
									</div>
									<div class="col l6 m6 hide-on-small-only ">
									<img src="<?php echo base_url('assets/images/dashed-icon-01.png'); ?>"/>
									</div>
								</div>
								<div class="col s12 m12 l12 student-application-status feespaid center-align">
								Fees Paid
								</div>
								<div class="col s12 m12 l12 student-application-status-date center-align">
								<?php echo date('j M Y',strtotime($shortStatus->updated_at)); ?>
								</div>
							</div>
						</div>
						<?php } if($shortStatus->as_status == 7){ ?>
						<div class="col s6 m1 l1 ">
							<div class="row "> 
								<div class="row">
									<div class="col l12 m12 s12 center-align ">
									<img src="<?php echo base_url('assets/images/timeline-icons-green-7.png'); ?>"/>
									</div>
								</div>
								<div class="col s12 m12 l12 student-application-status center-align">
								Admission Done
								</div>
								<div class="col s12 m12 l12 student-application-status-date center-align">
								<?php echo date('j M Y',strtotime($shortStatus->updated_at)); ?>
								</div>
							</div>
						</div>
						<?php }if($shortStatus->as_status == 8){ ?>
						<div class="col s6 m1 l1 ">
							<div class="row "> 
								<div class="row">
									<div class="col l12 m12 s12 center-align ">
									<img src="<?php echo base_url('assets/images/timeline-icons-green-7.png'); ?>"/>
									</div>
								</div>
								<div class="col s12 m12 l12 student-application-status center-align">
								jectr
								</div>
								<div class="col s12 m12 l12 student-application-status-date center-align">
								<?php echo date('j M Y',strtotime($shortStatus->updated_at)); ?>
								</div>
							</div>
						</div>
						<?php } 
					
					?>
					<div class="col s6 m1 l1 ">
							<div class="row "> 
								<div class="row">
									
								</div>
								<div class="col s12 m12 l12 student-application-status center-align">
								<form action="#" method="post">
								<input type="hidden" id="useid" value="<?php echo $user_id=$userdata['id'];
							?>">
								<input type="hidden" id="courceid" value="<?php echo $cshort->uc_id; ?>">
								<input type="button" value="Apply" id="cource_apply"/>
								<input type="button" value="Compare" id="cource_compare"/>
								</form>
								</div>
								<div class="col s12 m12 l12 student-application-status-date center-align">
							
								</div>
							</div>
						</div>
					  </div>
					  
					
					
					<?php 
					} } }
					}?> 
				</div>
			</div>
		</div>
	</div>
</section>	
<!-- END MAIN -->
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/1.3.1/js/toastr.js"></script>

<script>
	$(document).ready(function(){
		
		
		$('#cource_apply').click(function(){
			var courceid=$('#courceid').val();
			var userid=$('#useid').val();
			$.ajax({
				type:'POST',
				url:'<?php echo base_url()?>dashboard/course_apply',
				data:{courceid:courceid,userid:userid},
				success:function(res){
									toastr.success("Applied Successfully");
				
					setTimeout(function(){
		location.reload();
		}, 3000);
				}
			})
			
		});
		
		
		$('.close').click(function(){
			$('.button-collapse').sideNav('hide');
		});
		$("#applied-student").show();
		$("#applied-college-one").show();
		$("#applied-college-two").show();
		$("#shortlisted-college").hide();
		$("#firstactive").show();
		$("#switchactive").hide();
		$("#shortlist-student").hide();
		$("#Uonetab").show();
		$("#Utwotab").hide();
		 $("#formdetails").show();
		$("#Uthreetab").hide();
		$("#Ufourtab").hide();
		$("#Ufivetab").hide();
		$("#addstudentdetails").hide();
    $("#one").click(function(){
		$("#Uonetab").show();
        $("#Utwotab").hide();
		$("#Uthreetab").hide();
		$("#Ufourtab").hide();
		$("#Ufivetab").hide();
    });
    $("#two").click(function(){
		$("#Utwotab").show();
        $("#Uonetab").hide();
		$("#Uthreetab").hide();
		$("#Ufourtab").hide();
		$("#Ufivetab").hide();
    });
	$("#three").click(function(){
		$("#Uthreetab").show();
		$("#formdetails").show();
        $("#Uonetab").hide();
		$("#Utwotab").hide();
		$("#Ufourtab").hide();
		$("#Ufivetab").hide();
		$("#addstudentdetails").hide();
    });
	$("#four").click(function(){
		$("#Ufourtab").show();
		$("#formdetails").show();
        $("#Uonetab").hide();
		$("#Utwotab").hide();
		$("#Uthreetab").hide();
		$("#Ufivetab").hide();
		$("#addstudentdetails").hide();		
    });
	$("#five").click(function(){
		$("#Ufivetab").show();
		$("#formdetails").show();
        $("#Uonetab").hide();
		$("#Utwotab").hide();
		$("#Uthreetab").hide();
		$("#Ufourtab").hide();
		$("#addstudentdetails").hide();		
    });
	$("#newform").click(function(){
		$("#addstudentdetails").show();
        $("#formdetails").hide();	
    });
		$("#one").mouseover(function(){
		$('#statusIcon1').attr("src","<?php echo base_url('assets/imagesimages/university-login-icon-hover-1.png');?>");
		$('#sideMenuSpan1').addClass("side-menu-span-color");
		$('#sideMenuSpan1').removeClass("side-menu-span-initial-color");
		
    });
	$("#one").mouseout(function(){
		$('#statusIcon1').attr("src","<?php echo base_url('assets/images/university-login-1.png');?>");
		$('#sideMenuSpan1').removeClass("side-menu-span-color");
		$('#sideMenuSpan1').addClass("side-menu-span-initial-color");
    });
	$("#two").mouseover(function(){
		$('#statusIcon2').attr("src","<?php echo base_url('assets/images/university-login-icon-hover-2.png');?>");
		$('#sideMenuSpan2').addClass("side-menu-span-color");
		$('#sideMenuSpan2').removeClass("side-menu-span-initial-color");
    });
	$("#two").mouseout(function(){
		$('#statusIcon2').attr("src","<?php echo base_url('assets/images/university-login-2.png');?>");
		$('#sideMenuSpan2').removeClass("side-menu-span-color");
		$('#sideMenuSpan2').addClass("side-menu-span-initial-color");
    });
	$("#three").mouseover(function(){
		$('#statusIcon3').attr("src","<?php echo base_url('assets/images/university-login-icon-hover-3.png');?>");
		$('#sideMenuSpan3').addClass("side-menu-span-color");
		$('#sideMenuSpan3').removeClass("side-menu-span-initial-color");
    });
	$("#three").mouseout(function(){
		$('#statusIcon3').attr("src","<?php echo base_url('assets/images/university-login-3.png');?>");
		$('#sideMenuSpan3').removeClass("side-menu-span-color");
		$('#sideMenuSpan3').addClass("side-menu-span-initial-color");
    });
	$("#four").mouseover(function(){
		$('#statusIcon4').attr("src","<?php echo base_url('assets/images/university-login-icon-hover-4.png');?>");
		$('#sideMenuSpan4').addClass("side-menu-span-color");
		$('#sideMenuSpan4').removeClass("side-menu-span-initial-color");
    });
	$("#four").mouseout(function(){
		$('#statusIcon4').attr("src","<?php echo base_url('assets/images/university-login-4.png');?>");
		$('#sideMenuSpan4').removeClass("side-menu-span-color");
		$('#sideMenuSpan4').addClass("side-menu-span-initial-color");
    });
	$("#Student-applied").click(function(){
		$("#firstactive").show();
		$("#applied-college-one").show();
		$("#applied-college-two").show();
		$("#shortlisted-college").hide();
	    $("#switchactive").hide();	
		$('#Student-applied').attr("src","<?php echo base_url('assets/images/switch-button-icons-02.png');?>");
		$('#Student-shortlist').attr("src","<?php echo base_url('assets/images/switch-button-icons-01.png');?>");
		$("#applied-student").show();
        $("#shortlist-student").hide();	
    });
	$("#Student-shortlist").click(function(){
		$("#shortlist-student").show();
		$("#shortlisted-college").show();
		$("#applied-college-one").hide();
		$("#applied-college-two").hide();
        $("#applied-student").hide();
       	$("#firstactive").hide();
		$('#Student-applied').attr("src","<?php echo base_url('assets/images/switch-button-icons-4.png');?>");
		$('#Student-shortlist').attr("src","<?php echo base_url('assets/images/switch-button-icons-3.png');?>");	
		$("#switchactive").show();
    });
	 
	});
</script>