<style>
#add_program_form{
	display:none;
}
.select-dropdown {
     height: auto !important; 
}
.select-wrapper input.select-dropdown{
	line-height: 2rem;
}
.publish-approve{
	background-color: #38E98F !important;
    color: #ffffff !important;
    padding: 5px;
}
.publish-pending{
	background-color: #e4b331 !important;
    color: #ffffff !important;
    padding: 5px;
}
.publish-reject{
	background-color: #e93853 !important;
    color: #ffffff !important;
    padding: 5px;
}

</style>
<section id="content">
<div class="row"  id="side-menu-five-div">
	<div class="col s12 m12 l12" style="margin-top: -6.8px;">
		<div class="card">		
			<div class="">			
							  <!-- card Section Start-->
				<div id="card-stats" class="seaction">
					<div class="row">
						<div class="col s12 m8 l8 offset-l3 offset-m4">
							<div class="row row-margin-bottom">
								<div class="Space30 col l12 m12 s12"></div>
								<div class="col s12 m12 l12">
								<h4 class="admission-form-heading-margin">Manage Program</h4>
								</div>
								<div class="col s12 m3 l3"></div>	
							</div>	
						</div>
						<div id="card-stats" class="seaction">
							<div class="row">
								<div class="col s6 m8 l8 offset-l3 offset-m4">
									<div class="row row-margin-bottom" style="margin-left: 100px;">
										<div class="Space30 col l12 m12 s12"></div>
										<div class="col s6 m6 l6 ">
											
											<!--  <a class="btn" onClick="javascript:selectAll()">Select all</a>
  												<a class="btn" onClick="javascript:selectNone()">Select none</a> -->

										</div>
										<div class="col s12 m3 l3"></div>	
									</div>
									<div class="row row-margin-bottom" style="margin-left: 100px;">
										<div class="Space30 col l12 m12 s12"></div>
										<div class="col s6 m6 l6 ">
											Select University
											<select class="university form-control" id="university">
												<option disabled selected> <a class="btn" >Select all</a></option>
												<option value="0" selected >All</option>


											<?php foreach($university as $course){ ?>
												<option value="<?php echo $course->um_id; ?>"><?php echo $course->um_name; ?></option>
											<?php } ?>
											</select>
											<!--  <a class="btn" onClick="javascript:selectAll()">Select all</a>
  												<a class="btn" onClick="javascript:selectNone()">Select none</a> -->

										</div>
										<div class="col s12 m3 l3"></div>	
									</div>										
								</div>
							</div>
						</div>
						<div class="Space30 col l12 m12 s12"></div>
						<div class="col l12 m12 s12">
							<div id="responsive-table">
								<div class="row">
									<div class="col s12 l12 m12">
										<table class="responsive-table">
											<thead>
												<tr>
													<th class="upm-table-head-bg center-align">No.</th>
													<th class="upm-table-head-bg center-align">University Name</th>
													<th class="upm-table-head-bg center-align">Stream</th>
													<th class="upm-table-head-bg center-align">Program</th>
														<th class="upm-table-head-bg center-align">Specialization</th>
													<th class="upm-table-head-bg center-align">Duration</th>
													<th class="upm-table-head-bg center-align">Type</th>
												
													<th class="upm-table-head-bg center-align">Status</th>
													<th class="upm-table-head-bg center-align">Action</th>
												</tr>
											</thead>
											<tbody id="program_list">
												<?php $count=1;
												if(!empty($progrmManager)){
												foreach($progrmManager as $pMr){
												?>
												<tr>
												  <td class="upm-table-content-bg center-align"><?php echo  $count++; ?></td>
												  <td class="upm-table-content-bg center-align"><?php echo $pMr->um_name; ?></td>
												  <td class="upm-table-content-bg center-align"><?php echo $pMr->stream; ?></td>
												  <td class="upm-table-content-bg center-align"><?php echo $pMr->spe; ?></td>
												    <td class="upm-table-content-bg center-align"><?php echo $pMr->course_spe; ?></td>
												  <td class="upm-table-content-bg center-align"><?php echo $pMr->uc_course_length; ?></td>
												  <td class="upm-table-content-bg center-align">
												  <?php echo $pMr->typename; ?>
												  </td>
												  <td class="upm-table-content-bg center-align">
													<?php if($pMr->uc_is_approved == 1){?>
													<span class="publish-approve switch-tab3 cust-cursor">Approved</span>
													<?php }else if($pMr->uc_is_approved == 0){ ?>
													<span class="publish-pending switch-tab3 cust-cursor">Pending</span>
													<?php }else{ ?>
													<span class="publish-reject switch-tab3 cust-cursor">Rejected</span>
													<?php } ?>
												  </td>
												  <td class="upm-table-content-bg center-align">
												  <a href="<?php echo base_url('admin/mark_approval/'.$pMr->uc_id); ?>"><img class="sideIcon cust-cursor" src="<?php echo base_url('assets/images/icon-03.png'); ?>"/>
												  <span style="vertical-align: super;">Approved</span></a>&nbsp;&nbsp;
												  <a href="<?php echo base_url('admin/mark_reject/'.$pMr->uc_id); ?>"><img class="sideIcon cust-cursor" src="<?php echo base_url('assets/images/icon-03.jpg'); ?>"/>
												  <span style="vertical-align: super;">Reject</span></a>
												  </td>
												</tr>
												<?php } } ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>	
</div>

</section>
<script type="text/javascript" src="<?php echo base_url('assets/js/plugins/jquery-1.11.2.min.js');?>"></script>
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/1.3.1/js/toastr.js"></script>
<script type="text/javascript">
function ajaxindicatorstart(text)
{
	if(jQuery('body').find('#resultLoading').attr('id') != 'resultLoading'){
	jQuery('body').append('<div id="resultLoading" style="display:none"><div><img src="<?php echo base_url()?>uploads/ajax-loader.gif"><div>'+text+'</div></div><div class="bg"></div></div>');
	}

	jQuery('#resultLoading').css({
		'width':'100%',
		'height':'100%',
		'position':'fixed',
		'z-index':'10000000',
		'top':'0',
		'left':'0',
		'right':'0',
		'bottom':'0',
		'margin':'auto'
	});

	jQuery('#resultLoading .bg').css({
		'background':'#000000',
		'opacity':'0.7',
		'width':'100%',
		'height':'100%',
		'position':'absolute',
		'top':'0'
	});

	jQuery('#resultLoading>div:first').css({
		'width': '250px',
		'height':'75px',
		'text-align': 'center',
		'position': 'fixed',
		'top':'0',
		'left':'0',
		'right':'0',
		'bottom':'0',
		'margin':'auto',
		'font-size':'16px',
		'z-index':'10',
		'color':'#ffffff'

	});

    jQuery('#resultLoading .bg').height('100%');
       jQuery('#resultLoading').fadeIn(300);
    jQuery('body').css('cursor', 'wait');
}

function ajaxindicatorstop()
{
    jQuery('#resultLoading .bg').height('100%');
       jQuery('#resultLoading').fadeOut(300);
    jQuery('body').css('cursor', 'default');
}

</script>
<script type="text/javascript">
$(document).ready(function(){
$('#university').change(function(){
	//alert($(this).val());
	$.ajax({
		type: "POST",
		url: "<?php echo base_url('admin/get_program_list'); ?>",
		data:{'uid':$(this).val()},
		//dataType: "json",
		 beforeSend: function () {
           				  ajaxindicatorstart('Loading.....');
           			 },
		success: function(response){
			var obj = JSON.parse(response);
			console.log(obj);
			 var html='';
			 var count=1;
			 if(obj.length > 0){
				$.each(obj, function(ind,val){
					
					setTimeout(function(){
						  			 ajaxindicatorstop();
						  			
								
							}, 1000);

					var spe='';
					if(val.course_spe==null){
						spe='-';
					}else if(val.course_spe!=null){
						spe=val.course_spe;
					}
						var msg='';
						var msg1='';
						
						
						if(val.uc_is_approved==1)
						{
							msg+='<span class="publish-approve switch-tab3 cust-cursor">Approved</span>';
							
						}
						else if(val.uc_is_approved==0){
							msg+='<span class="publish-pending switch-tab3 cust-cursor">Pending</span>';
							
						}else{
							msg+='<span class="publish-reject switch-tab3 cust-cursor">Rejected</span>';
						}
						
						<!-- aproved-->
						if(val.uc_is_approved==2)
						{
							msg1+='<a href="#" onclick="aproval('+val.uc_id+')"><img class="sideIcon cust-cursor" src="<?php echo base_url('assets/images/icon-03.png'); ?>"/><span style="vertical-align: super;">  Approved</span></a>';
							
						}
						else if(val.uc_is_approved==1){
							msg1+='<a href="#" onclick="reject('+val.uc_id+')"><img class="sideIcon cust-cursor" src="<?php echo base_url('assets/images/icon-03.jpg'); ?>" style="margin-left:-28px"><span style="vertical-align: super;">  Reject</span></a>';
							
						}else if(val.uc_is_approved==0){
							msg1+='<a href="#" onclick="aproval('+val.uc_id+')"><img class="sideIcon cust-cursor" src="<?php echo base_url('assets/images/icon-03.png'); ?>"/><span style="vertical-align: super;">  Approved</span></a>';
						}
					
						html+='<tr ><td class="upm-table-content-bg center-align">'+count+++'</td>'+
						
						'<td class="upm-table-content-bg center-align">'+val.um_name+'</td>'+
						'<td class="upm-table-content-bg center-align">'+val.stream+'</td>'+
						'<td class="upm-table-content-bg center-align">'+val.spe+'</td>'+
						'<td class="upm-table-content-bg center-align">'+spe+'</td>'+
						'<td class="upm-table-content-bg center-align">'+val.uc_course_length+'</td>'+
						'<td class="upm-table-content-bg center-align">'+val.typename+'</td>'+
						'<td class="upm-table-content-bg center-align">'+msg+'</td>'+
						'<td class="upm-table-content-bg center-align">'+msg1+'</td>'+
					    
						'</tr>';
						
						
				});
}else
{
	html='<td colspan="6" class="upm-table-content-bg center-align">No Result Found</td>';
}
				$('#program_list').html(html);
			
			
		}
	});
});
});
</script>
<script>

get_program()
function get_program()
{
$.ajax({
		type: "POST",
		url: "<?php echo base_url('admin/program_list'); ?>",
		//data:data,
		//dataType: "json",
		success: function(response){
			var obj = JSON.parse(response);
			console.log(obj);
			 var html='';
			 var count=1;
			 if(obj.length > 0){
				$.each(obj, function(ind,val){
					var spe='';
					if(val.course_spe==null){
						spe='-';
					}else if(val.course_spe!=null){
						spe=val.course_spe;
					}
						var msg='';
						var msg1='';
						
						
						if(val.uc_is_approved==1)
						{
							msg+='<span class="publish-approve switch-tab3 cust-cursor">Approved</span>';
							
						}
						else if(val.uc_is_approved==0){
							msg+='<span class="publish-pending switch-tab3 cust-cursor">Pending</span>';
							
						}else{
							msg+='<span class="publish-reject switch-tab3 cust-cursor">Rejected</span>';
						}
						
						<!-- aproved-->
						if(val.uc_is_approved==2)
						{
							msg1+='<a href="#" onclick="aproval('+val.uc_id+')"><img class="sideIcon cust-cursor" src="<?php echo base_url('assets/images/icon-03.png'); ?>"/><span style="vertical-align: super;">  Approved</span></a>';
							
						}
						else if(val.uc_is_approved==1){
							msg1+='<a href="#" onclick="reject('+val.uc_id+')"><img class="sideIcon cust-cursor" src="<?php echo base_url('assets/images/icon-03.jpg'); ?>" style="margin-left:-28px"><span style="vertical-align: super;">  Reject</span></a>';
							
						}else if(val.uc_is_approved==0){
							msg1+='<a href="#" onclick="aproval('+val.uc_id+')"><img class="sideIcon cust-cursor" src="<?php echo base_url('assets/images/icon-03.png'); ?>"/><span style="vertical-align: super;">  Approved</span></a>';
						}
					
						html+='<tr ><td class="upm-table-content-bg center-align">'+count+++'</td>'+
						
						'<td class="upm-table-content-bg center-align">'+val.um_name+'</td>'+
						'<td class="upm-table-content-bg center-align">'+val.stream+'</td>'+
						'<td class="upm-table-content-bg center-align">'+val.spe+'</td>'+
						'<td class="upm-table-content-bg center-align">'+spe+'</td>'+
						'<td class="upm-table-content-bg center-align">'+val.uc_course_length+'</td>'+
						'<td class="upm-table-content-bg center-align">'+val.typename+'</td>'+
						'<td class="upm-table-content-bg center-align">'+msg+'</td>'+
						'<td class="upm-table-content-bg center-align">'+msg1+'</td>'+
					    
						'</tr>';
						
						
				});
}else
{
	html='<td colspan="6" class="upm-table-content-bg center-align">No Result Found</td>';
}
				$('#program_list').html(html);
			
			
		}
	});
	
}

function aproval(id)
{
	$.ajax({
			type:'POST',
			url:'<?php echo base_url()?>admin/mark_approval',
			data:{'id':id},
			success:function(res){
				//alert(res);
			if(res)
			{
				
			toastr.success("Course Approved for Listing");
			
						setTimeout(function(){
	//var menuid=$('#menuid').val();
					//console.log(menuid);
					 get_program();
		}, 100);
			}
		
			}
	})
}

function reject(id)
{
	$.ajax({
			type:'POST',
			url:'<?php echo base_url()?>admin/mark_reject',
			data:{'id':id},
			success:function(res){
				//alert(res);
			if(res)
			{
				
			toastr.error("Course Rejected for Listing");
			
						setTimeout(function(){
	//var menuid=$('#menuid').val();
					//console.log(menuid);
					 get_program();
		}, 100);
			}
		
			}
	})
}



$('#add_program').click(function(){
	$('#add_program_form').slideToggle( "slow" );
});

</script>
