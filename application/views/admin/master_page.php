<section id="content4">
<style>
#add_program_form_2{
display:none;
}
#edit_program_form_2{
display:none;
}
.select-dropdown {
 height: auto !important; 
}
.select-wrapper input.select-dropdown{
line-height: 2rem;
}
.side-nav .collapsible-body1 li.active,.side-nav.fixed .collapsible-body1 li.active{
background-color:#00bcd4
}
.side-nav .collapsible-body1 li.active a,.side-nav.fixed .collapsible-body1 li.active a{
color:#fff
}
.side-nav.fixed.leftside-navigation .collapsible-body1 li.active>a{
color:#00bcd4
}
</style>
<div class="row" id="side-menu-five-div">
<div class="col s12 m12 l12" style="margin-top: -6.8px;">
<div class="card">
<div class="">
 
<div id="card-stats" class="seaction">
<div class="row">
<div class="col s12 m8 l8 offset-l3 offset-m4">
<div class="row row-margin-bottom">
<div class="Space30 col l12 m12 s12"></div>
<div class="col s12 m12 l12">
<h4 class="admission-form-heading-margin">Manage Sub Category</h4>
</div>
<?php //echo $this->uri->segment(4);?>
<div class="col s12 m3 l3"></div>
</div>
</div>
<div class="Space30 col l12 m12 s12"></div>
<div class="col l12 m12 s12">
<div class="col l12 m12 s12">
<?php 
if('specialization'==$this->uri->segment(4))
{
?>
<a id="add_program_3" class="waves-effect waves-light btn btn-form-submit Searchbtn center-align" style="margin-left:12px">Add <?php echo $menu_data[0]->child_sub_menu_name?></a>

<div class="row col l12 m12 s12" id="add_program_form_3">
<form action="#" method="post" id="mform">
<div class="card col l5 m5 s12" style="margin-left:12px">
<div class="col s12 m12 l12 Space10"></div>
<div class="col s12 m3 l3">
<label class="NameLable"><?php echo $menu_data[0]->child_sub_menu_name?> Name</label>
</div>
<div class="col s12 m8 l8 no-padding-origin" style="margin-left:10px">
<input type="text" style="width:50%" name="masterpage1[]" id="masterpage1" class="fieldname1_sp" placeholder="Enter Name" maxlength="20">
<input type="hidden" name="chid1" id="chid1" value="<?php echo $menu_data[0]->id?>">
&nbsp;&nbsp;<input type="button" id="btnAdd_1" type="button" class="waves-button-input" value="+" onclick="AddTextBox_sp()" />
</div>
<div class="col s12 m12 l12 Space10"></div>

<div class="col s12 m12 l12 Space10"></div>
<div class="col s12 m3 l3">
<label class="NameLable"><b style="color:white">Menu Name</b></label>
</div>
<div class="col s12 m8 l8 no-padding-origin" id="TextBoxContainer_sp">

</div>

<div class="col s12 m12 l12 Space10"></div>

<?php 
if('specialization'==$this->uri->segment(4))
{
?>
<div class="col s12 m3 l3">
<label class="NameLable"><b style="color:black">Stream Name</b></label>
</div>
<div class="col s12 m4 l4 no-padding-origin" style="width:50%">
<select class="browser-default waves-effect waves-light programname_sp" name="program" >
<option value="">Select</option>
<?php foreach($program as $pro){?>
<option value="<?php echo $pro->id;?>"><?php echo $pro->name ?></option>

<?php } ?>
</select>
</div>

<div class="col s12 m12 l12 Space10"></div>
<?php 
}
?>

<div class="col s12 m12 l12 Space10"></div>
<div class="col s12 m8 l8 offset-l3 offset-m3 no-padding-origin" style="
 margin-bottom: 10px;
">
<input type="button" value="Save" class="waves-effect waves-light btn btn-form-submit Searchbtn center-align up-save-btn" id="save_master_spe"/>
<input type="button" value="Cancel" class="waves-effect waves-light btn btn-form-submit Searchbtn center-align up-save-btn" id="cal_master_spe"/>
</div>

</div>
</form>
<div class="col l7 m7 s12"></div>
</div>
<?php } else {?>
<a id="add_program_2" class="waves-effect waves-light btn btn-form-submit Searchbtn center-align" style="margin-left:12px">Add <?php echo $menu_data[0]->child_sub_menu_name?></a>

<?php }?>
</div>
<div class="col s12 m12 l12 Space10"></div>
<div class="row col l12 m12 s12" id="add_program_form_2">
<form action="#" method="post">
<div class="card col l5 m5 s12" style="margin-left:12px;">
<div class="col s12 m12 l12 Space10"></div>
<div class="col s12 m3 l3">
<label class="NameLable"><?php echo $menu_data[0]->child_sub_menu_name?> Name</label>
</div>
<div class="col s12 m8 l8 no-padding-origin" style="margin-left:10px">
<input type="text" style="width:50%" name="masterpage[]" id="masterpage" class="fieldname1" placeholder="Enter Name" maxlength="20">
<input type="hidden" name="chid" id="chid" value="<?php echo $menu_data[0]->id?>">
&nbsp;&nbsp;<input type="button" id="btnAdd_nor" type="button" class="waves-button-input" value="+" onclick="AddTextBox()" />
</div>

<div class="col s12 m3 l3">
<label class="NameLable"><b style="color:white">Menu Name</b></label>
</div>
<div class="col s12 m8 l8 no-padding-origin" id="TextBoxContainer">

</div>

<div class="col s12 m12 l12 Space10"></div>
<?php 

if($this->uri->segment(4)=='stream')
{
?>
<div class="col s12 m3 l3">
<label class="NameLable"><b style="color:black">Eligibilty</b></label>
</div>
<div class="col s12 m4 l4 " style="width:50%">
<select class="browser-default waves-effect waves-light eligibi" name="eligibi">
<option value="">Select</option>
<?php foreach($eligibility as $pro){?>
<option value="<?php echo $pro->id;?>"><?php echo $pro->name ?></option>

<?php } ?>
</select>
</div>


<?php 
}
?>
<?php 

if($this->uri->segment(4)=='cource_spe')
{
?>
<div class="col s12 m3 l3">
<label class="NameLable"><b style="color:black">Program Name</b></label>
</div>
<div class="col s12 m4 l4 " style="width:50%">
<select class="browser-default waves-effect waves-light course_spe" name="course_spe">
<option value="">Select</option>
<?php foreach($course_spe as $pro){?>
<option value="<?php echo $pro->id;?>"><?php echo $pro->name ?></option>

<?php } ?>
</select>
</div>


<?php 
}
?>

<div class="col s12 m12 l12 Space10" style="margin-top:40px;"></div>
<div class="col s12 m8 l8 offset-l3 offset-m3 no-padding-origin" style="
 margin-bottom: 10px;
">
<input type="button" value="Save" class="waves-effect waves-light btn btn-form-submit Searchbtn center-align up-save-btn" id="save_master"/>
<input type="button" value="Cancel" class="waves-effect waves-light btn btn-form-submit Searchbtn center-align up-save-btn" id="save_cancel"/>
</div>

</div>
</form>


<div class="col l7 m7 s12"></div>
</div>


<div class="row col l12 m12 s12" id="edit_program_form_2">
<form action="#" method="post" id="fform">
<div class="card col l5 m5 s12" style="margin-left:12px">
<div class="col s12 m12 l12 Space10"></div>
<div class="col s12 m3 l3">
<label class="NameLable">Menu Name</label>
</div>
<div class="col s12 m8 l8 no-padding-origin">
<input type="text" name="masterpages" id="masterpages">
<input type="hidden" name="mid" id="mid" >
<input type="hidden" name="chid" id="chid" value="<?php echo $menu_data[0]->id?>">
</div>
<div class="col s12 m12 l12 Space10"></div>
<?php 

if($this->uri->segment(4)=='stream')
{
?>
<div class="col s12 m3 l3">
<label class="NameLable"><b style="color:black">Eligibilty</b></label>
</div>
<div class="col s12 m4 l4 " style="width:50%">
<select class="browser-default waves-effect waves-light  eligibi_edit" name="eligibi" id="eligibi_edit">
<option value="">Select Eligibility</option>
<?php foreach($eligibility as $pro){?>
<option value="<?php echo $pro->id;?>"><?php echo $pro->name ?></option>

<?php } ?>
</select>
</div>


<?php 
}
?>
<?php 

if($this->uri->segment(4)=='cource_spe')
{
?>
<div class="col s12 m3 l3">
<label class="NameLable"><b style="color:black">Program Name</b></label>
</div>
<div class="col s12 m4 l4 " style="width:50%">
<select class="browser-default waves-effect waves-light course_spe_edit" name="course_spe_edit">
<option value="">Select</option>
<?php foreach($course_spe as $pro){?>
<option value="<?php echo $pro->id;?>"><?php echo $pro->name ?></option>

<?php } ?>
</select>
</div>


<?php 
}
?>
<?php 

if($this->uri->segment(4)!='stream' && $this->uri->segment(4)=='specialization')
{
?>
<div class="col s12 m3 l3">
<label class="NameLable"><b style="color:black">Specialization</b></label>
</div>
<div class="col s12 m4 l4 " style="width:50%">
<select class="browser-default waves-effect waves-light  eligibi_edit_sp" name="eligibi" id="eligibi_edit_sp">
<option value="">Select Spe </option>
<?php foreach($program as $pro){?>
<option value="<?php echo $pro->id;?>"><?php echo $pro->name ?></option>

<?php } ?> 
</select>
</div>


<?php 
}
?>




<div class="col s12 m12 l12 Space10"></div>
<div class="col s12 m8 l8 offset-l3 offset-m3 no-padding-origin" style="
 margin-bottom: 10px;
">
<input type="button" value="Update" class="waves-effect waves-light btn btn-form-submit Searchbtn center-align up-save-btn" id="Update_master"/>
<input type="button" value="cancel" class="waves-effect waves-light btn btn-form-submit Searchbtn center-align up-save-btn" id="Update_master_can"/>
</div>
<div class="col s12 m12 l12 Space10"></div>
</div>
</form>
<div class="col l7 m7 s12"></div>
</div>


<div class="col s12 m12 l12">
<div id="responsive-table">
 <div class="row">
<div class="col s12 l12 m12">
	<table id=""   class="responsive-table display" width="100%">
		<thead>
            <tr>
                <th class="upm-table-head-bg center-align"><?php echo $menu_data[0]->child_sub_menu_name?></th>
                <th class="upm-table-head-bg center-align">Status</th>
                <th class="upm-table-head-bg center-align">Action</th>
              
            </tr>
        </thead>
        <tbody id="mastertdata">
         
        </tobody>
	</table>


</div>
</div>
</div>
</div>

</div>
</div>
</div>
</div>
</div>
</div>
</div>

<script type="text/javascript" src="<?php echo base_url('assets/js/plugins/jquery-1.11.2.min.js');?>"></script>
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/1.3.1/js/toastr.js"></script>
<script type="text/javascript">
$(document).ready(function (){

     $('#example').dataTable();
     
     // $("#tableID").DataTable();
});

</script>

<script>
get_mst_data();
function get_mst_data(){
$.ajax({
type:'POST',
url:'<?php echo base_url()?>admin/master_program_test/'+<?php echo $this->uri->segment(3,0);?>,
//data:{'id':id},
success:function(res){

var obj=JSON.parse(res);
console.log(obj);
 var html='';
 var html_test='';
$.each(obj, function(ind,val){
//console.log(val);

var msg='';
if(val.status==1)
{
msg+='Active';
}
else{
msg+='InActive';
}
var msg1='';
if(val.status==1)
{
msg1+='<a href="#" onclick="del('+val.id+',<?php echo $menu_data[0]->id?>)"><img class="sideIcon cust-cursor" src="<?php echo base_url('assets/img/delete-icon-02.png'); ?>"/></a>';
}
else{
msg1+='<a href="#" onclick="active('+val.id+',<?php echo $menu_data[0]->id?>)"><img class="sideIcon cust-cursor" src="<?php echo base_url('assets/images/icon-03.png'); ?>"/></a>';
}

html+='<tr><td class="upm-table-content-bg ">'+val.name+'</td>'+

'<td class="upm-table-content-bg center-align">'+msg+'</td>'+
'<td class="upm-table-content-bg center-align"> '+
'<a href="#" onclick="edit('+val.id+',<?php echo $menu_data[0]->id?>)"><img class="sideIcon cust-cursor" src="<?php echo base_url('assets/img/edit-icon-01.png'); ?>"/></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+
 ''+msg1+''
'</td></tr>';



})
$('#mastertdata').html(html); 
$('#example').dataTable();
//$('#mastertdata_test').html(html_test);
 

}
})
}
$("#edit_program_form_2").hide();
$("#add_program_form_2").hide();
$("#add_program_form_3").hide();
$('#add_program_2').click(function(){
$('#add_program_form_2').slideToggle( "slow" );
$('#edit_program_form_2').hide( "slow" );

});
$('#add_program_3').click(function(){
$('#add_program_form_3').slideToggle( "slow" );
$('#edit_program_form_2').hide( "slow" );
//$('#edit_program_form_2').hide( "slow" );

});
$('#save_cancel').click(function(){
$("#edit_program_form_2").hide('slow');
$("#add_program_form_2").hide('slow');

});
$('#Update_master_can').click(function(){
$("#edit_program_form_2").hide('slow');

});
$('#cal_master_spe').click(function(){
$("#add_program_form_3").hide('slow');

});


$('#save_master').click(function(){
//$("body").load('');
var name=$('#masterpage').val();
var childid=$('#chid').val();
var master = [];
$.each($('.fieldname1'), function() {
master.push($(this).val());
});
var eligibi = [];
$.each($('.eligibi'), function() {
eligibi.push($(this).val());
});

// var course_s = [];
// $.each($('.fieldname1_sp'), function() {
// course_s.push($(this).val());
// });

var course_spe = [];
$.each($('.course_spe'), function() {
course_spe.push($(this).val());
});

// console.log(eligibi[0]);
 //console.log(course_s);

 //return false;
if(name=='')
{
$('#masterpage').focus();
toastr.error('Enter Mode');
$('#masterpage').css('border-color:red');
return false;
}
else
{
$.ajax({
type:'POST',
url:'<?php echo base_url()?>admin/add_program',
data:{'name':master,'childid':childid,'eligibi':eligibi[0],'course_id':course_spe[0]},
success:function(res){
var obj=JSON.parse(res);
console.log(obj);
if(obj==="Name Already exist")
{
 toastr.error("Name already exits");
}
if(obj==="Added Successfully")
{
toastr.success("Added succesfully");
setTimeout(function(){
 get_mst_data();
 $('#add_program_form_2').hide( "slow" );
 $('#fform')[0].reset();
 var name=$('#masterpage').val('');
}, 100);
}
}
})
}
});

 // $('.fieldname1').keydown(function (e) {
 //          if (e.shiftKey || e.ctrlKey || e.altKey) {
 //              e.preventDefault();
 //          } else {
 //              var key = e.keyCode;
 //              if (!((key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
 //                toastr.error('Enter only Alphabets');
 //                  e.preventDefault();
 //              }
 //          }
 //      });

 // $('#masterpages').keydown(function (e) {
 //          if (e.shiftKey || e.ctrlKey || e.altKey) {
 //              e.preventDefault();
 //          } else {
 //              var key = e.keyCode;
 //              if (!((key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
 //                toastr.error('Enter only Alphabets');
 //                  e.preventDefault();
 //              }
 //          }
 //      });

 $('#masterpage').change(function () {
         
             var regex = /^[a-zA-Z ]*$/;
        if (regex.test($('#masterpage').val())) {
          $('#save_master').prop( "disabled", false );
        } else {
            toastr.error('Enter Only Alphates');
      $('#masterpage').focus();
      $('#save_master').prop( "disabled", true  );
      return false;
        }
      
      });

$('#masterpages').change(function () {
         
             var regex = /^[a-zA-Z ]*$/;
        if (regex.test($('#masterpages').val())) {
          $('#Update_master').prop( "disabled", false );
        } else {
            toastr.error('Enter Only Alphates');
      $('#masterpages').focus();
      $('#Update_master').prop( "disabled", true  );
      return false;
        }
      
      });


$('#save_master_spe').click(function(){
//$("body").load('');
//alert('ji');
var name=$('#masterpage1').val();
var childid=$('#chid1').val();
var master = [];
$.each($('.fieldname1_sp'), function() {
master.push($(this).val());
});

var program = [];
$.each($('.programname_sp'), function() {
program.push($(this).val());
});
// console.log(program);
// console.log(master);
// return false;
if(name=='')	
{
$('#masterpage1').focus();
toastr.error("Enter Program Name");
$('#masterpage1').css('border-color:red');
return false;
}
else
{
$.ajax({
type:'POST',
url:'<?php echo base_url()?>admin/add_program_spe',
data:{'name':master,'childid':childid,'program':program[0]},
success:function(res){
//alert(res);
var obj=JSON.parse(res);
console.log(obj);
if(obj==="Name Already exist")
{
 toastr.error("Name already exits");
}
if(obj==="Added Successfully")
{
 // $("body").load(res);
toastr.success("Added succesfully");
//$('#ts1_'+id).closest('tr').add();
setTimeout(function(){
 get_mst_data();
 $('#add_program_form_3').hide( "slow" );
 var name=$('#masterpage1').val('');
$('#mform')[0].reset();
}, 100);
}

}
})
}
});
$('#Update_master').click(function(){

var name=$('#masterpages').val();
var id=$('#mid').val();
var childid=$('#chid').val();

var eligibi = [];
$.each($('.eligibi_edit'), function() {
eligibi.push($(this).val());
});

var eligibi_sp = [];
$.each($('.eligibi_edit_sp'), function() {
eligibi_sp.push($(this).val());
});

var course_sp = [];
$.each($('.course_spe_edit'), function() {
course_sp.push($(this).val());
});
// console.log(eligibi_sp[0]);
// return false;
if(name=='')
{
$('#masterpages').focus();
toastr.error('Enter Child Menuname');
return false;
}
else
{
$.ajax({
type:'POST',
url:'<?php echo base_url()?>admin/edit_program/update_master_page',
data:{'id':id,'name':name,'childid':childid,'eligibi':eligibi[0],'eligibi_sp':eligibi_sp[0],'course_id':course_sp[0]},
success:function(res){
//var obj=JSON.parse(res);
//console.log(obj);
//if(res==='200')
//{
// toastr.error("Name already exits");
//}
//else {
 toastr.success("Updated succesfully");
setTimeout(function(){
 get_mst_data();
 $('#edit_program_form_2').hide( "slow" );
$('#mform_1')[0].reset();
 var name=$('#masterpage').val('');
}, 100);
//}
}
})
}
});
function del(id,table)
{
$.ajax({
type:'POST',
url:'<?php echo base_url()?>admin/edit_program/del_master_page',
data:{'id':id,'table':table},
success:function(res){
//alert(res);
setTimeout(function(){
 $("body").load(res);
}, 100);
 toastr.error("Reject");

}
})
}
function active(id,table)
{
$.ajax({
type:'POST',
url:'<?php echo base_url()?>admin/edit_program/active_master_page',
data:{'id':id,'table':table},
success:function(res){
//alert(res);
setTimeout(function(){
 $("body").load(res);
}, 100);
 toastr.success("Approved");

//setTimeout(function(){
 // window.location.reload(1);
//}, 100);
}
})
}
function edit(id,tableid)
{
//alert(id);
//$('#add_program').show( "slow" );
$.ajax({
type:'POST',
url:'<?php echo base_url()?>admin/edit_program/edit_master_page',
data:{'id':id,'child_id':tableid},
success:function(res){
obj = JSON.parse(res);
console.log(obj);
$('#edit_program_form_2').show( "slow" );
$('#add_program_form_2').hide( "slow" );
$('#add_program_form_3').hide( "slow" );
//$('#add_program').hide( "slow" );
var name=$('#masterpages').val(obj[0].name);
var name=$('#mid').val(id);
console.log( obj[0].course_id);
$("#eligibi_edit_sp option[value='" + obj[0].program_id + "']").attr("selected","selected");
$("#eligibi_edit option[value='" + obj[0].eligi_cate_id + "']").attr("selected","selected");
$(".course_spe_edit option[value='" + obj[0].course_id + "']").attr("selected","selected");
//location.reload();
}
})
}
</script>
<script type="text/javascript">
$(function () {
 var values = 4;
 if (values != null) {
 var html = "";
 $(values).each(function () {
 var div = $("<div />");
 //div.html(GetDynamicTextBox(this));
 $("#TextBoxContainer").append(div);
 });
 }
 $("#btnAdd_nor").bind("click", function () {
 var div = $("<div />");
 div.html(GetDynamicTextBox_s(""));
 $("#TextBoxContainer").append(div);
 });
 $("body").on("click", ".remove", function () {
 $(this).closest("div").remove();
 });
});
function GetDynamicTextBox_s(value) {
 return '<input style="width:78%;margin-left: 10px;" name = "masterpage[]" id="masterpage" class="fieldname1" type="text" value = "' + value + '" />&nbsp;' +
 '<img src="<?php echo base_url('assets/images/cross-icon-2.png')?>" class="remove" />'
}
</script>
<script type="text/javascript">
$(function () {
 var values = 4;
 if (values != null) {
 var html = "";
 $(values).each(function () {
 var div = $("<div />");
 //div.html(GetDynamicTextBox(this));
 $("#TextBoxContainer1").append(div);
 });
 }
 $("#btnAdd_1").bind("click", function () {
 var div = $("<div />");
 div.html(GetDynamicTextBox(""));
 $("#TextBoxContainer_sp").append(div);
 });
 $("body").on("click", ".remove", function () {
 $(this).closest("div").remove();
 });
});
function GetDynamicTextBox(value) {
 return '<input style="width:78%;margin-left: 10px;" name = "masterpage1[]" id="masterpage" class="fieldname1_sp" type="text" value = "' + value + '" />&nbsp;' +
 '<img src="<?php echo base_url('assets/images/cross-icon-2.png')?>" class="remove" />'
}
</script>

</section>