<style>
#add_program_form{
	display:none;
}
.select-dropdown {
     height: auto !important; 
}
.select-wrapper input.select-dropdown{
	line-height: 2rem;
}
.publish-approve{
	background-color: #38E98F !important;
    color: #ffffff !important;
    padding: 5px;
}
.publish-pending{
	background-color: #e4b331 !important;
    color: #ffffff !important;
    padding: 5px;
}
.publish-reject{
	background-color: #e93853 !important;
    color: #ffffff !important;
    padding: 5px;
}

</style>
<section id="content">
<div class="row"  id="side-menu-five-div">
	<div class="col s12 m12 l12" style="margin-top: -6.8px;">
		<div class="card">		
			<div class="">			
							  <!-- card Section Start-->
				<div id="card-stats" class="seaction">
					<div class="row">
						<div class="col s12 m8 l8 offset-l3 offset-m4">
							<div class="row row-margin-bottom">
								<div class="Space30 col l12 m12 s12"></div>
								<div class="col s12 m12 l12">
								<h5 class="admission-form-heading-margin">Manage University</h5>
								</div>
								<div class="col s12 m3 l3"></div>	
							</div>	
						</div>
						<div class="Space30 col l12 m12 s12"></div>
						<div class="col l12 m12 s12">
							
							<a id="add_program" style="margin-left:10px" class="waves-effect waves-light btn btn-form-submit Searchbtn center-align">Add Academic User</a>
						
							</div>
							<div class="col s12 m12 l12 Space10"></div>
							<div class="row col l12 m12 s12" id="add_program_form">
								<form action="#" method="post" id="academic_form">
								<div class="card col l5 m5 s12" style="margin-left:12px">
									<div class="col s12 m12 l12 Space10"></div>
									<div class="col s12 m3 l3">
										<label class="NameLable">Name</label>
									</div>
									<div class="col s12 m8 l8 no-padding-origin">
										<input type="text" name="name" id="name" Placeholder="Enter Name">
									
									</div>
									<div class="col s12 m3 l3">
										<label class="NameLable">Last Name</label>
									</div>
									<div class="col s12 m8 l8 no-padding-origin">
										<input type="text" name="laname" id="lname" Placeholder="Enter Last Name">
									
									</div>
									<div class="col s12 m3 l3">
										<label class="NameLable">Email</label>
									</div>
									<div class="col s12 m8 l8 no-padding-origin">
										<input type="text" name="email" id="email" Placeholder="Enter Email">
									
									</div>
									<div class="col s12 m3 l3">
										<label class="NameLable">Password</label>
									</div>
									<div class="col s12 m8 l8 no-padding-origin">
										<input type="password" name="password" id="password" Placeholder="Enter Password">
									
									</div>
									<div class="col s12 m3 l3">
										<label class="NameLable">Confirm Password</label>
									</div>
									<div class="col s12 m8 l8 no-padding-origin">
										<input type="password" name="cpassword" id="cpassword" Placeholder="Enter Confirm Password">
									
									</div>
									<div class="col s12 m12 l12 Space10"></div>
								
									
									<div class="col s12 m12 l12 Space10"></div>
									<div class="col s12 m8 l8 offset-l3 offset-m3 no-padding-origin">
										<input type="button" value="Save" class="waves-effect waves-light btn btn-form-submit Searchbtn center-align up-save-btn" id="save">
												<input type="button" value="cancel" class="waves-effect waves-light btn btn-form-submit Searchbtn center-align up-save-btn" id="cancel_save">

									</div>
									<div class="col s12 m12 l12 Space10"></div>
								</div>
								</form>
								<div class="col l7 m7 s12"></div>
							</div>
						<div class="row col l12 m12 s12" id="edit_program_form">
								<form action="#" method="post" id="edit_academic_form">
								<div class="card col l5 m5 s12" style="margin-left:12px">
									<div class="col s12 m12 l12 Space10"></div>
									<div class="col s12 m3 l3">
										<label class="NameLable">Name</label>
									</div>
									<div class="col s12 m8 l8 no-padding-origin">
										<input type="text" name="name" id="name_edit" Placeholder="Enter Name">
									
									</div>
									<!-- <div class="col s12 m3 l3">
										<label class="NameLable">Last Name</label>
									</div-->
					
										<input type="hidden" name="acid" id="acid" >
									
								
									<!-- <div class="col s12 m3 l3">
										<label class="NameLable">Email</label>
									</div>
									<div class="col s12 m8 l8 no-padding-origin">
										<input type="text" name="email" id="email_edit" Placeholder="Enter Email">
									
									</div> -->
									<!-- <div class="col s12 m3 l3">
										<label class="NameLable">Password</label>
									</div>
									<div class="col s12 m8 l8 no-padding-origin">
										<input type="password" name="password" id="password_edit" Placeholder="Enter Password">
									
									</div>
									<div class="col s12 m3 l3">
										<label class="NameLable">Confirm Password</label>
									</div>
									<div class="col s12 m8 l8 no-padding-origin">
										<input type="password" name="cpassword" id="cpassword_edit" Placeholder="Enter Confirm Password">
									
									</div> -->
									<div class="col s12 m12 l12 Space10"></div>
								
									
									<div class="col s12 m12 l12 Space10"></div>
									<div class="col s12 m8 l8 offset-l3 offset-m3 no-padding-origin">
										<input type="button" value="Update" class="waves-effect waves-light btn btn-form-submit Searchbtn center-align up-save-btn" id="update">
												<input type="button" value="cancel" class="waves-effect waves-light btn btn-form-submit Searchbtn center-align up-save-btn" id="cancel_update">

									</div>
									<div class="col s12 m12 l12 Space10"></div>
								</div>
								</form>
								<div class="col l7 m7 s12"></div>
							</div>	
						<div class="col l12 m12 s12">
							<div id="responsive-table">
								<div class="row">
									<div class="col s12 l12 m12">
										<table class="responsive-table">
											<thead>
												<tr>
													<th class="upm-table-head-bg center-align">NO</th>
													<th class="upm-table-head-bg center-align">Name</th>
													<th class="upm-table-head-bg center-align">Email</th>
													<!-- <th class="upm-table-head-bg center-align">Role</th> -->
													<th class="upm-table-head-bg center-align">Status</th>
													<th class="upm-table-head-bg center-align" colspan='1'>Action</th>
												</tr>
											</thead>
											<tbody id="program_list">
												<?php $count=1;
												if(!empty($academic)){
												foreach($academic as $pMr){
												?>
												<tr><td class="upm-table-content-bg center-align"><?php echo $count++; ?></td>
												  <td class="upm-table-content-bg center-align"><?php echo $pMr->name; ?></td>
												  <td class="upm-table-content-bg center-align"><?php echo $pMr->email; ?></td>
												  <!-- <td class="upm-table-content-bg center-align"><?php echo $pMr->role; ?></td> -->
										
												  <td class="upm-table-content-bg center-align">
													<?php if($pMr->status == 1){?>
													<span class="publish-approve switch-tab3 cust-cursor">Approved</span>
													<?php }else if($pMr->status == 0){ ?>
													<span class="publish-pending switch-tab3 cust-cursor">Pending</span>
													<?php }else{ ?>
													<span class="publish-reject switch-tab3 cust-cursor">Rejected</span>
													<?php } ?>
												  </td>
												  <td class="upm-table-content-bg center-align">
												  <!-- <a href="<?php echo base_url('admin/mark_approval/'.$pMr->uc_id); ?>"><img class="sideIcon cust-cursor" src="<?php echo base_url('assets/images/icon-03.png'); ?>"/>
												  <span style="vertical-align: super;">Approved</span></a>&nbsp;&nbsp;
												  <a href="<?php echo base_url('admin/mark_reject/'.$pMr->uc_id); ?>"><img class="sideIcon cust-cursor" src="<?php echo base_url('assets/images/icon-03.jpg'); ?>"/>
												  <span style="vertical-align: super;">Reject</span></a> -->
												  </td>
												</tr>
												<?php } } ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>	
</div>
<?php 
$userdata = $this->session->userdata('logged_in');
?>
</section>
<script type="text/javascript" src="<?php echo base_url('assets/js/plugins/jquery-1.11.2.min.js');?>"></script>
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/1.3.1/js/toastr.js"></script>

<script>

get_program()
function get_program()
{
$.ajax({
		type: "POST",
		url: "<?php echo base_url('admin/academin_user_list'); ?>",
		//data:data,
		//dataType: "json",
		success: function(response){
			var obj = JSON.parse(response);
			console.log(obj);
			 var html='';
			 var count=1;
				$.each(obj, function(ind,val){
					
						var msg='';
						var msg1='';
						var action='<a href="#" onclick="edit('+val.id+')"><img class="sideIcon cust-cursor" src="<?php echo base_url('assets/img/edit-icon-01.png'); ?>"/></a>';
							var action1='<a href="<?php echo base_url()?>admin/switch_to_academic/'+val.id+'/<?php echo $userdata['id']?>"><span class="publish-approve switch-tab3 cust-cursor">Manage</span></a>';

						if(val.status==1)
						{
							msg+='<span class="publish-approve switch-tab3 cust-cursor">Active</span>';
							
						}
						else if(val.status==0){
							msg+='<span class="publish-pending switch-tab3 cust-cursor">Pending</span>';
							
						}else{
							msg+='<span class="publish-reject switch-tab3 cust-cursor">Rejected</span>';
						}
		
					 if(val.status==1){
							msg1+='<a href="#" onclick="reject('+val.id+')"><img class="sideIcon cust-cursor" src="<?php echo base_url('assets/images/icon-03.jpg'); ?>" style="margin-left:-28px"><span style="vertical-align: super;">  Reject</span></a>';
							
						}else if(val.status==0){
							msg1+='<a href="#" onclick="aproval('+val.id+')"><img class="sideIcon cust-cursor" src="<?php echo base_url('assets/images/icon-03.png'); ?>"/><span style="vertical-align: super;">  Approved</span></a>';
						}
					
						html+='<tr ><td class="upm-table-content-bg center-align">'+ count++ +'</td>'+
						'<td class="upm-table-content-bg center-align">'+val.name+'</td>'+
						'<td class="upm-table-content-bg center-align">'+val.email+'</td>'+
					//	'<td class="upm-table-content-bg center-align">'+val.role+'</td>'+
						'<td class="upm-table-content-bg center-align">'+msg+'</td>'+
						'<td class="upm-table-content-bg center-align">'+action+' &nbsp;&nbsp;&nbsp;&nbsp; '+ action1 +'</td>'+
						//'<td class="upm-table-content-bg center-align">'+msg1+'</td>'+
					    
						'</tr>';
						
						
				});
				$('#program_list').html(html);
			
			
		}
	});
	
}
$('#save').click(function(){
var name=$('#name').val();
var lname=$('#lname').val();
var email=$('#email').val();
var password=$('#password').val();
var cpassword=$('#cpassword').val();
 var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(name==''){
    	toastr.error("Enter name");
    	$('#name').focus();
		return false;
    }else if(lname==''){
    	toastr.error("Enter last name");
    	$('#lname').focus();
		return false;
    }
	else if(email=='')
	{
			$('#email').focus();
		toastr.error("Enter Email-Id");
		return false;
	}else if(!re.test(email))
	{
			$('#email').focus();
		toastr.error("Enter Valid Email-Id");
			return false;
	}else if(password=='')
	{
			$('#password').focus();
		toastr.error("Enter  Password");
		$('#password').focus();
		return false;
	}else if(cpassword=='')
	{
		$('#cpassword').focus();
		toastr.error("Enter Confirm Password");
		$('#cpassword').focus();
			return false;
	}else if(password!=cpassword)
	{
		$('#cpassword').focus();
		toastr.error("Enter Does not match");
		$('#cpassword').focus();
			return false;
	}else{
		//toastr.success("success");
var dataS={
	'name':name,
	'lname':lname,
	'email':email,
	'password':password,
	'role':1
}
// console.log(dataS);
$.ajax({
			type:'POST',
			url:'<?php echo base_url()?>admin/academic_user_reg',
			data:dataS,
			success:function(res){
				//console.log(res);
				var obj=JSON.parse(res);
				//console.log(obj);
				if(obj.msg=='Successfull'){
				
					toastr.success('Successfully Added..!');
					setTimeout(function(){
					 get_program();
					 $('#academic_form').trigger("reset");;
					}, 100);
				}
	
		
			}
	})
}

});

$('#update').click(function(){
var name=$('#name_edit').val();
var acid=$('#acid').val();
$.ajax({
			type:'POST',
			url:'<?php echo base_url()?>admin/academic_user_update',
			data:{'name':name,'id':acid},
			success:function(res){
				//console.log(res);
				var obj=JSON.parse(res);
				//console.log(obj);
				if(obj.msg=='Successfull'){
				
					toastr.success('Successfully Updated..!');
					setTimeout(function(){
					 get_program();
					 $('#edit_program_form').hide();
					 $('#edit_academic_form').trigger("reset");;
					}, 100);
				}
	
		
			}
	})
});
$('#email').on('focusout', function() {
	
	var email=$('#email').val();
	$.ajax({
			type:'POST',
			url:'<?php echo base_url()?>admin/check_email',
			data:{'email':email},
			success:function(res){
				//alert(res);
				var obj=JSON.parse(res);
				console.log(obj);
				if(obj.msg=='Exitst'){
					$('#email').focus();
					toastr.error('Email already Exist');
					$('#save').prop('disabled',false);
				}
	
		
			}
	})
   
});


function edit(id){
	$('#add_program_form').hide();
	$('#edit_program_form').show( "slow" );
$.ajax({
			type:'POST',
			url:'<?php echo base_url()?>admin/academic_user_edit',
			data:{'id':id},
			success:function(res){
				var obj=JSON.parse(res);
				console.log(obj);
				$('#name_edit').val(obj[0].name);
				$('#acid').val(obj[0].id);
				//$('#email_edit').val(obj[0].email);
		
			}
	})
}
function aproval(id)
{
	$.ajax({
			type:'POST',
			url:'<?php echo base_url()?>admin/mark_approval',
			data:{'id':id},
			success:function(res){
				//alert(res);
			if(res)
			{
				
			toastr.success("Course Approved for Listing");
			
						setTimeout(function(){
					 get_program();
		}, 100);
			}
		
			}
	})
}

function reject(id)
{
	$.ajax({
			type:'POST',
			url:'<?php echo base_url()?>admin/mark_reject',
			data:{'id':id},
			success:function(res){
				//alert(res);
			if(res)
			{
				
			toastr.error("Course Rejected for Listing");
			
						setTimeout(function(){
					 get_program();
		}, 100);
			}
		
			}
	})
}

$('#stream').change(function(){
	var streamid = $(this).val();
	$('#program').empty();
	$.ajax({
		type: "POST",
		url: "<?php echo base_url('academic/stream_program'); ?>",
		data:{streamid:streamid},
		success: function(response){
			var res = JSON.parse(response);
			$('#program').append('<option value="" disabled selected>Choose Program</option>');
			$.each(res, function (i, itm) {
				$('#program').append($('<option>', { 
					value: itm.com_id,
					text : itm.com_name 
				}));
			});
		}
	});
});

$('#add_program').click(function(){
	$('#add_program_form').slideToggle( "slow" );
	$('#edit_program_form').hide('slow');
});

$('#cancel_save').click(function(){
	//$('#add_program_form').slideToggle( "slow" );
	$('#add_program_form').hide('slow');
});
$('#cancel_update').click(function(){
	//$('#add_program_form').slideToggle( "slow" );
	
	$('#edit_program_form').hide('slow');
});

</script>