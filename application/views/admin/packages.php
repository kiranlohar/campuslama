<style>
#add_program_form{
	display:none;
}
.select-dropdown {
     height: auto !important; 
}
.select-wrapper input.select-dropdown{
	line-height: 2rem;
}
</style>
<section id="content">
<div class="row"  id="side-menu-five-div">
	<div class="col s12 m12 l12" style="margin-top: -6.8px;">
		<div class="card">		
			<div class="">			
							  <!-- card Section Start-->
				<div id="card-stats" class="seaction">
					<div class="row">
						<div class="col s12 m8 l8 offset-l3 offset-m4">
							<div class="row row-margin-bottom">
								<div class="Space30 col l12 m12 s12"></div>
								<div class="col s12 m12 l12">
								<h4 class="admission-form-heading-margin">Packages</h4>
								</div>
								<div class="col s12 m3 l3"></div>	
							</div>	
						</div>
						<div class="Space30 col l12 m12 s12"></div>
						<div class="col l12 m12 s12">
							<div class="col l12 m12 s12">
							<a id="add_program" class="waves-effect waves-light btn btn-form-submit Searchbtn center-align">Add New Package</a>
							</div>
							<div class="col s12 m12 l12 Space10"></div>
							<div class="row col l12 m12 s12" id="add_program_form">
								<form action="<?php echo base_url('admin/packages')?>" method="post">
								<div class="card col l5 m5 s12">
									<div class="col s12 m12 l12 Space10"></div>
									<div class="col s12 m3 l3">
										<label class="NameLable">Package Name</label>
									</div>
									<div class="col s12 m8 l8 no-padding-origin">
										<input type="text" name="package_name">
									</div>
									<div class="col s12 m12 l12 Space10"></div>
									<div class="col s12 m3 l3">
										<label class="NameLable">Package Amount</label>
									</div>
									<div class="col s12 m8 l8 no-padding-origin">
										<input type="text" name="package_amount">
									</div>
									
									<div class="col s12 m12 l12 Space10"></div>
									<div class="col s12 m3 l3">
										<label class="NameLable">Validity</label>
									</div>
									<div class="col s12 m4 l4 no-padding-origin">
										<input type="text" name="valid">
									</div>
									<div class="col s12 m4 l4 no-padding-origin">
										<select id="valid-unit" name="valid-unit" class="browser-default Ustory select-dropdown" >
											<option value="" disabled selected>Choose Year/Month</option>
											<option value="Months">Months</option>
											<option value="Years">Years</option>
										</select>
									</div>
									<div class="col s12 m12 l12 Space10"></div>
									<div class="col s12 m3 l3">
										<label class="NameLable">No.of Listing</label>
									</div>
									<div class="col s12 m4 l4 no-padding-origin">
										<input type="text" name="num_listout">
									</div>
									<div class="col s12 m12 l12 Space10"></div>
									<div class="col s12 m3 l3">
										<label class="NameLable">University</label>
									</div>
									<div class="col s12 m8 l8 no-padding-origin">
										<select id="university" name="university" class="browser-default Ustory select-dropdown">
											<option value="" disabled selected>Choose University</option>
											<?php foreach($uniData as $uData){ ?>
											<option value="<?php echo $uData->um_id; ?>"><?php echo $uData->um_name; ?></option>
											<?php } ?>
										</select>
									</div>
									<div class="col s12 m12 l12 Space10"></div>
									<div class="col s12 m8 l8 offset-l3 offset-m3 no-padding-origin">
										<input type="submit" value="Save" class="waves-effect waves-light btn btn-form-submit Searchbtn center-align up-save-btn"/>
									</div>
									<div class="col s12 m12 l12 Space10"></div>
								</div>
								</form>
								<div class="col l7 m7 s12"></div>
							</div>			
			<div class="col s12 m12 l12">
					<div id="responsive-table">
					  <div class="row">
						<div class="col s12 l12 m12">
						  <table class="responsive-table">
							  <thead>
									<tr>
										<th class="upm-table-head-bg center-align">Package Name</th>
										<th class="upm-table-head-bg center-align">Package Amount</th>
										<th class="upm-table-head-bg center-align">Package Duration</th>
										<th class="upm-table-head-bg center-align">List Allowed</th>
										<th class="upm-table-head-bg center-align">University</th>
										<th class="upm-table-head-bg center-align">Edit / Delete</th>
									</tr>
									<tr class="Space5"></tr>
							  </thead>
							<tbody>
								<?php 
								if(!empty($packageData)){
								foreach($packageData as $pData){
								?>
								<tr>
								  <td class="upm-table-content-bg center-align"><?php echo $pData->pack_name; ?></td>
								  <td class="upm-table-content-bg center-align"><?php echo $pData->pack_amount; ?></td>
								  <td class="upm-table-content-bg center-align"><?php echo $pData->pack_valid; ?></td>
								  <td class="upm-table-content-bg center-align"><?php echo $pData->num_list; ?></td>
								  <td class="upm-table-content-bg center-align"><?php echo $pData->um_name; ?></td>
								  <td class="upm-table-content-bg center-align">
								  <a href="<?php echo base_url('admin/edit_package/'.$pData->pack_id); ?>"><img class="sideIcon cust-cursor" src="<?php echo base_url('assets/img/edit-icon-01.png'); ?>"/></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								  <a href="<?php echo base_url('admin/delete_package/'.$pData->pack_id); ?>"><img class="sideIcon cust-cursor" src="<?php echo base_url('assets/img/delete-icon-02.png'); ?>"/></a>
								  </td>
								</tr>
								<?php } } ?>
						  </tbody>
						</table>
					</div>
					</div>
				</div>
			</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>	
</div>

</section>
<script type="text/javascript" src="<?php echo base_url('assets/js/plugins/jquery-1.11.2.min.js');?>"></script>
<script>
$('#add_program').click(function(){
	$('#add_program_form').slideToggle( "slow" );
});
</script>