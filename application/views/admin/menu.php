
<section id="content">

<style>
#add_program_form{
	display:none;
}
#edit_program_form{
	display:none;
}
.select-dropdown {
     height: auto !important; 
}
.select-wrapper input.select-dropdown{
	line-height: 2rem;
}
</style>

<div class="row"  id="side-menu-five-div">
	<div class="col s12 m12 l12" style="margin-top: -6.8px;">
		<div class="card">		
			<div class="">			
							  <!-- card Section Start-->
				<div id="card-stats" class="seaction">
					<div class="row">
						<div class="col s12 m8 l8 offset-l3 offset-m4">
							<div class="row row-margin-bottom">
								<div class="Space30 col l12 m12 s12"></div>
								<div class="col s12 m12 l12">
								<h4 class="admission-form-heading-margin">Menu</h4>
								</div>
								<div class="col s12 m3 l3"></div>	
							</div>	
						</div>
						<div class="Space30 col l12 m12 s12"></div>
						<div class="col l12 m12 s12">
							<!--div class="col l12 m12 s12">
							<a id="add_program" class="waves-effect waves-light btn btn-form-submit Searchbtn center-align" style="margin-left:12px">Add New Menu</a>
							</div>
							<!--div class="col s12 m12 l12 Space10"></div>
							<div class="row col l12 m12 s12" id="add_program_form">
								<form action="#" method="post">
								<div class="card col l5 m5 s12" style="margin-left:12px">
									<div class="col s12 m12 l12 Space10"></div>
									<div class="col s12 m3 l3">
										<label class="NameLable">Menu Name</label>
									</div>
									<div class="col s12 m8 l8 no-padding-origin">
										<input type="text" name="menuname" id="menuname">
									</div>
									<div class="col s12 m12 l12 Space10"></div>

									
									<!--div class="col s12 m4 l4 no-padding-origin">
										<select id="valid-unit" name="valid-unit" class="browser-default Ustory select-dropdown" >
											<option value="" disabled selected>Choose Year/Month</option>
											<option value="Months">Months</option>
											<option value="Years">Years</option>
										</select>
									</div>
								
									<div class="col s12 m12 l12 Space10"></div>
									
									
									<div class="col s12 m12 l12 Space10"></div>
									<div class="col s12 m8 l8 offset-l3 offset-m3 no-padding-origin">
										<input type="button" value="Save" class="waves-effect waves-light btn btn-form-submit Searchbtn center-align up-save-btn" id="save"/>
									</div>
									<div class="col s12 m12 l12 Space10"></div>
								</div>
								</form>
								<div class="col l7 m7 s12"></div>
							</div-->	
							
							
<div class="row col l12 m12 s12" id="edit_program_form">
								<form action="#" method="post">
								<div class="card col l5 m5 s12" style="margin-left:12px">
									<div class="col s12 m12 l12 Space10"></div>
									<div class="col s12 m3 l3">
										<label class="NameLable">Menu Name</label>
									</div>
									<div class="col s12 m8 l8 no-padding-origin">
										<input type="text" name="menuname" id="menun">
										<input type="hidden" name="mid" id="mid">
									</div>
									<div class="col s12 m12 l12 Space10"></div>
								
									
									<div class="col s12 m12 l12 Space10"></div>
									<div class="col s12 m8 l8 offset-l3 offset-m3 no-padding-origin">
										<input type="button" value="Update" class="waves-effect waves-light btn btn-form-submit Searchbtn center-align up-save-btn" id="Update"/>
									</div>
									<div class="col s12 m12 l12 Space10"></div>
								</div>
								</form>
								<div class="col l7 m7 s12"></div>
							</div>								
			<div class="col s12 m12 l12">
					<div id="responsive-table">
					  <div class="row">
						<div class="col s12 l12 m12">
						  <table class="responsive-table">
							  <thead>
									<tr>
										<th class="upm-table-head-bg center-align">Menu Name</th>
										
										<th class="upm-table-head-bg center-align">Status</th>
										
										<th class="upm-table-head-bg center-align">Action</th>
									</tr>
									<tr class="Space5"></tr>
							  </thead>
							<tbody>
								<?php 
								if(!empty($mData)){
								foreach($mData as $pData){
								?>
								<tr>
								  <td class="upm-table-content-bg center-align"><?php echo $pData->menu_name; ?></td>
								  <td class="upm-table-content-bg center-align">
								  <?php 
								  //echo $pData->status;
								  if($pData->status==1){
									  echo 'Active';
								  }
								  else{
									   echo 'InActive';
								  }
								  ?>
								  </td>
								 
								  
								  <td class="upm-table-content-bg center-align">
								   <a href="<?php echo base_url('admin/subMenu/'.$pData->id); ?>">Add sub menu</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								  <a href="#" onclick="edit(<?php echo $pData->id?>)"><img class="sideIcon cust-cursor" src="<?php echo base_url('assets/img/edit-icon-01.png'); ?>"/></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								  <a href="#" onclick="del(<?php echo $pData->id?>)"><img class="sideIcon cust-cursor" src="<?php echo base_url('assets/img/delete-icon-02.png'); ?>"/></a>
								  </td>
								</tr>
								<?php } } ?>
						  </tbody>
						</table>
					</div>
					</div>
				</div>
			</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>	
</div>


<script type="text/javascript" src="<?php echo base_url('assets/js/plugins/jquery-1.11.2.min.js');?>"></script>
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/1.3.1/js/toastr.js"></script>

<script>
	
$("#add_program_form").hide();
$("#edit_program_form").hide();
$('#add_program').click(function(){
	$('#add_program_form').slideToggle( "slow" );
	
});
$('#save').click(function(){
	var name=$('#menuname').val();
	if(name=='')
	{
	
		$('#menuname').focus();
		$('#menuname').css('border-color:red');
		return false;
	}
	else
	{
		$.ajax({
			type:'POST',
			url:'<?php echo base_url()?>admin/add_menu/add_master',
			data:{'name':name},
			success:function(res){
				//alert(res);
				location.reload();
			}
		})
	}
});
$('#Update').click(function(){
	
	var name=$('#menun').val();
	var id=$('#mid').val();
	if(name=='')
	{
		$('#menun').focus();
		$('#menun').css('border-color:red');
		return false;
	}
	else
	{
		$.ajax({
			type:'POST',
			url:'<?php echo base_url()?>admin/add_menu/update_master',
			data:{'id':id,'name':name},
			success:function(res){
				//alert(res);
				location.reload();
			}
		})
	}
});

function del(id)
{
	$.ajax({
			type:'POST',
			url:'<?php echo base_url()?>admin/add_menu/del_master',
			data:{'id':id},
			success:function(res){
				//alert(res);
				location.reload();
			}
		})
}
function edit(id)
{
	//$('#add_program').show( "slow" );
	$.ajax({
			type:'POST',
			url:'<?php echo base_url()?>admin/add_menu/edit_master',
			data:{'id':id},
			success:function(res){
				obj = JSON.parse(res);
				console.log(obj[0].name);
				$('#edit_program_form').slideToggle( "slow" );
				$('#add_program_form').hide( "slow" );
				//$('#add_program').hide( "slow" );
					var name=$('#menun').val(obj[0].menu_name);
					var name=$('#mid').val(id);
				//location.reload();
			}
		})
}
</script>

</section>
 