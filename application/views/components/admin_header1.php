<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
    <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
    <title></title>

    <!-- Favicons-->
    <link rel="icon" href="<?php echo base_url('assets/img/favicon/favicon-32x32.png');?>" sizes="32x32">
    <!-- Favicons-->
    <link rel="apple-touch-icon-precomposed" href="<?php echo base_url('assets/img/favicon/apple-touch-icon-152x152.png');?>">
    <!-- For iPhone -->
    <meta name="msapplication-TileColor" content="#00bcd4">
    <meta name="msapplication-TileImage" content="<?php echo base_url('assets/img/favicon/mstile-144x144.png');?>">
    <!-- For Windows Phone -->


    <!-- CORE CSS-->    
    <link href="<?php echo base_url('assets/css/materialize.min.css');?>" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="<?php echo base_url('assets/css/style.min.css');?>" type="text/css" rel="stylesheet" media="screen,projection">
    <!--<link href="<?php echo base_url('assets/css/style_back.min.css');?>" type="text/css" rel="stylesheet" media="screen,projection">-->
    <link href="<?php echo base_url('assets/css/custom_back.css');?>" type="text/css" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/custom.css');?>" type="text/css" rel="stylesheet">
    <!-- CSS for full screen (Layout-2)-->    
    <link href="<?php echo base_url('assets/css/layouts/style-fullscreen.css');?>" type="text/css" rel="stylesheet" media="screen,projection">
    <!-- Custome CSS-->    
    <link href="<?php echo base_url('assets/css/custom/custom.min.css');?>" type="text/css" rel="stylesheet" media="screen,projection">


    <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
    <link href="<?php echo base_url('assets/js/plugins/perfect-scrollbar/perfect-scrollbar.css');?>" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="<?php echo base_url('assets/js/plugins/jvectormap/jquery-jvectormap.css');?>" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="<?php echo base_url('assets/js/plugins/chartist-js/chartist.min.css');?>" type="text/css" rel="stylesheet" media="screen,projection">
	 <link href="<?php echo base_url('assets/js/plugins/dropify/css/dropify.min.css');?>" type="text/css" rel="stylesheet" media="screen,projection">
	<style>
	.active_tab
	{
		background-color: #1874B3 !important;
		color: #ffffff !important;
	}
	
	</style>
	<style>
input[type="submit"] {
    padding-top: 8px !important;
}
</style>
</head>

<body>
    <!-- Start Page Loading -->
    <div id="loader-wrapper">
        <div id="loader"></div>        
        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
    </div>
    <!-- End Page Loading -->

    <!-- //////////////////////////////////////////////////////////////////////////// -->

    <!-- START HEADER -->
     <!--<header id="header" class="page-topbar">
      
        <div class="navbar-fixed header-bg">
            <nav class="navbar-color header-bg">
                <div class="nav-wrapper header-bg">
                    <ul class="left">                      
                      <li><h1 class="logo-wrapper"><span class="center-align brand-logo logoClor">HELLO <span class="logoClor1">ADMISSION</span></span></h1></li>
					   <li><h1 class="logo-wrapper"><a href="index-2.html" class="brand-logo darken-1"><img src="img/logo-01.png" alt="materialize logo"></a> <span class="logo-text">Materialize</span></h1></li>
                    </ul>
                 <!--div class="header-search-wrapper hide-on-med-and-down">
                        <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Search By Name, Email , Mobile" />
						
                    </div-->
					<div class="header-search-wrapper hide-on-med-and-down">
                        <i class="mdi-action-search"></i>
                        <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize"/>
                    </div>
                    <ul class="right hide-on-med-and-down">
                     
					   <!--<li class="header-right-icon" >
						<i class="mdi-action-search" style="margin-right: 100px;padding-top: 6px;">
						</i>
                        </li>
						
                        <li class="header-right-icon">Apurva 
                        </li>
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light notification-button header-right-icon" data-activates="notifications-dropdown"><i class="mdi-social-notifications header-right-icon"></i>
                        
                        </a>
                        </li>  -->                      
                        <!--<li><a class='dropdown-button' href="#" data-activates='dropdown2'><i class="mdi-navigation-more-vert header-right-icon"></i></a>
                        </li>-->
                    <!--</ul>
					<ul id='dropdown2' class='dropdown-content'>
						<li><a href="#!">Logout</a></li>
					  </ul>
                </div>
            </nav>
        </div>
    </header>-->
	   <header id="header" class="page-topbar">
        <!-- start header nav-->
        <div class="navbar-fixed">
            <nav class="navbar-color ">
                <div class="nav-wrapper">
                    <ul class="left">                      
                      <li><h1 class="logo-wrapper"><a href="index-2.html" class="brand-logo darken-1"><img src="<?php echo base_url('assets/img/logo-01.png');?>" alt="materialize logo"></a> <span class="logo-text">Materialize</span></h1></li>
                    </ul>
                    <div class="header-search-wrapper hide-on-med-and-down center-align">
                        <i class="mdi-action-search search-icon"></i>
                        <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Search By Name, Email, Mobile No"/>
                    </div>
					<?php $userdata = $this->session->userdata('logged_in'); ?>
                    <ul class="right hide-on-med-and-down">
                        <li class="header-right-icon header-icon-color"><a href="<?php echo base_url('admin/dashboard'); ?>"><?php echo $userdata['name']; ?></a> 
                        </li>
						<li><a href="<?php echo base_url('login/logout'); ?>">Logout</a></li>
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light notification-button header-right-icon" data-activates="notifications-dropdown"><i class="mdi-social-notifications header-right-icon header-icon-color"></i>
                        </a>
                        <!--<li><a class='dropdown-button' href="#" data-activates='dropdown2'><i class="mdi-navigation-more-vert header-right-icon"></i></a>
                        </li>-->
                    <!--</ul>
					<ul id='dropdown2' class='dropdown-content'>
						
					  </ul>
                    </ul>
                    <!-- translation-button -->
                    
                    
                </div>
            </nav>
        </div>
        <!-- end header nav-->
    </header>
    <!-- END HEADER -->
