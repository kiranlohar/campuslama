	<!-- jQuery Library -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/plugins/jquery-1.11.2.min.js');?>"></script>
    <!--materialize js-->
    <script type="text/javascript" src="<?php echo base_url('assets/js/materialize.min.js');?>"></script>
	
    <!--scrollbar-->
    <script type="text/javascript" src="<?php echo base_url('assets/js/plugins/perfect-scrollbar/perfect-scrollbar.min.js');?>"></script>

    <!-- sparkline --> 
    <script type="text/javascript" src="<?php echo base_url('assets/js/plugins/sparkline/jquery.sparkline.min.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/plugins/sparkline/sparkline-script.js');?>"></script>
    
   
    <!--jvectormap-->
    <script type="text/javascript" src="<?php echo base_url('assets/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/plugins/jvectormap/vectormap-script.js');?>"></script>    

    
    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <script type="text/javascript" src="<?php echo base_url('assets/js/plugins.min.js');?>"></script>
    <!--custom-script.js - Add your own theme custom JS-->
	 <script type="text/javascript" src="<?php echo base_url('assets/js/plugins/dropify/js/dropify.min.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/custom-script.js');?>"></script>
   
	</body>
</html>