<!-- START MAIN -->
<style>
ul#slide-out {
    background-color: #756d6d;
	color: #fff;
}
li.bold {
    background: #f75050;
}
li.bold >a{
    color:#fff;
}
.side-nav a:hover{
    background-color: #fff !important;
    color: #f75050;
}
</style>
    <div id="main">
        <!-- START WRAPPER -->
        <div class="wrapper">
			<?php $userdata = $this->session->userdata('logged_in'); ?>
            <!-- START LEFT SIDEBAR NAV-->
            <aside id="left-sidebar-nav">
                <ul id="slide-out" class="side-nav leftside-navigation">
					<?php 
						
						$userdata = $this->session->userdata('logged_in');
						$query = $this->db->query("SELECT * FROM student_mstr JOIN student_personal_details ON student_personal_details.spd_stm_id = student_mstr.stm_id WHERE student_mstr.stm_user_id = '".$userdata['id']."'");
						
						$userData = $query->row();
					?>
                    <li style="height:100px !important;">
					<?php if(isset($userData)){ ?>
					<img class="circle responsive-img"  src="<?php echo base_url('uploads/users/'.$userData->spd_profile_pic);?>" alt="" style="width: 100px;padding: 9px;" class=" responsive-img valign profile-image">
					<?php }else{ ?>
					 <img class="circle responsive-img" src="<?php echo base_url('assets/img/default_user.png');?>" alt="" style="width: 100px;padding: 9px;" class=" responsive-img valign profile-image">
					<?php } ?>
					 <span style="vertical-align: text-bottom;"><?php echo $userdata['name']; ?></span>
                    </li>
					<li class="li-hover menu_tabs"><div class="side-menu-top-line"></div></li>
					<li class="bold close" id="side-menu-one">
					<a href="<?php echo base_url('dashboard/student'); ?>" class="waves-effect waves-cyan">
						<i class="mdi-action-dashboard"></i> Dashboard</a>
					</a>
					</li>
					<li class="li-hover"><div class="divider"></div></li>
                    <li class="bold close" id="side-menu-one">
					<a href="<?php echo base_url('student/personal_details'); ?>" class="waves-effect waves-cyan">
						<i class="mdi-action-account-circle"></i> Personal Details</a>
					</a>
					</li>
					<li class="li-hover"><div class="divider"></div></li>
					<li class="bold menu_tabs close" id="side-menu-four">
						<a href="<?php echo base_url('student/educational_details'); ?>" class="waves-effect waves-cyan">
						  <i class="mdi-social-school"></i>
							Educational Details
							</span>
						</a>
					</li>
					<li class="li-hover"><div class="divider"></div></li>
					<li class="bold menu_tabs close" id="side-menu-five">
						<a href="<?php echo base_url('student/work_details'); ?>" class="waves-effect waves-cyan">
							<i class="mdi-action-work"></i>
							Work Details
						</a>
					</li>
					<li class="li-hover"><div class="divider"></div></li>
					<li class="bold menu_tabs close" id="side-menu-five">
						<a href="<?php echo base_url('student/course_packages'); ?>" class="waves-effect waves-cyan">
							<i class="mdi-editor-format-list-bulleted"></i>
							Application Packages
						</a>
					</li>
					<li class="li-hover"><div class="divider"></div></li>
					<li class="bold menu_tabs close" id="side-menu-six">
						<a href="<?php echo base_url('student/testimonial'); ?>" class="waves-effect waves-cyan">
							<i class="mdi-editor-insert-comment"></i>
								Testimonial
						</a>
					</li>
					<li class="li-hover"><div class="divider"></div></li>
					<li class="bold menu_tabs close" id="side-menu-six">
						<a href="<?php echo base_url('dashboard/user_setting'); ?>" class="waves-effect waves-cyan">
							<i class="mdi-action-settings"></i>
								Change password
						</a>
					</li>
					<li class="li-hover"><div class="divider"></div></li>
					<li class="bold menu_tabs close" id="side-menu-seven">
						<a href="<?php echo base_url('login/logout'); ?>" class="waves-effect waves-cyan">
							<i class="mdi-action-settings-power"></i>
							Logout 
						</a>
					</li>
					<li class="li-hover"><div class="divider"></div></li>
                </ul>
                <a href="#" data-activates="slide-out" class="sidebar-collapse btn-floating btn-medium waves-effect waves-light "><i class="mdi-navigation-menu"></i></a>
            </aside>