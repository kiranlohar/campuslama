

<!-- START MAIN -->
<style>
ul#slide-out {
    
	color: #fff;
}
li.bold {
    background: #f75050;
}
li.bold >a{
    color:#fff;
}
.side-nav .collapsible-body1 li.active,.side-nav.fixed .collapsible-body1 li.active{
	background-color:#00bcd4
}

.side-nav .collapsible-body1 li.active a,.side-nav.fixed .collapsible-body1 li.active a{
	color:#fff
}

.side-nav.fixed.leftside-navigation .collapsible-body1 li.active>a{
	color:#00bcd4
}
</style>
    <div id="main">
        <!-- START WRAPPER -->
        <div class="wrapper">
			<?php $userdata = $this->session->userdata('logged_in'); ?>
            <!-- START LEFT SIDEBAR NAV-->
            <aside id="left-sidebar-nav">
                <ul id="slide-out" class="side-nav leftside-navigation">
					
                    <!--li style="height:100px !important;">
					
					 <img src="<?php echo base_url('assets/img/default_user.png');?>" alt="" style="width: 100px;" class=" responsive-img valign profile-image">
					 <span style="vertical-align: text-bottom;"><?php echo $userdata['name']; ?></span>
                    </li-->
					<li class="li-hover menu_tabs"><div class="side-menu-top-line"></div></li>
					
					<ul class="collapsible collapsible-accordion" id="menuload">
					<!--li class="bold close" id="side-menu-one">
					<a href="<?php echo base_url('admin/menu'); ?>" class="waves-effect waves-cyan">
						<i class="small icon icon-ha-menu-black-icon-02 change-color"></i>Add menu</a>
					</a>
					</li-->
					<?php if(isset($front))?>
					<?php foreach($front as $f){?>
						<li class="bold menu_tabs" id="side-menu-two" id="master">
							<a class="collapsible-header  waves-effect waves-cyan"  href="#" id="master">
								<i class="small icon mdi-content-content-paste "></i>
								<?php echo $f['menu_name'];?>
								
								
							</a>
                            <div class="collapsible-body" style="background-color:#7b6c6c;color:white" id="sub_master">
                                <ul class="collapsible collapsible-accordion">
								<?php 
								$id=$f['id'];
								$q=$this->db->where(array('menu_id'=>$id,'status'=>1))->get('sub_menu_mstr');
								
								foreach($q->result() as $subm){
								?>
                                    <li class="menu_tabs"  onClick="sub_menu(<?php echo $subm->id;?>)">
									    <a class="collapsible-header  waves-effect waves-cyan" href="#" id="master_1" >
															
							
									<?php echo $subm->name;?>
									  
									</a>
									
									<ul class="collapsible-body" id="" class="side-nav leftside-navigation" id="sub_master_sub">
								
									<?php 
									$id=$subm->id;
									$mid=$subm->menu_id;
									$q1=$this->db->where(array('sub_menu_id'=>$id,'status'=>1,'menu_id'=>$mid))->get('child_sub_menu_mstr');
									
									foreach($q1->result() as $csubm){
									?>
									
										<li id="ted">
												<a class="waves-effect waves-cyan"  href="<?php echo base_url();?>admin/master_program/<?php echo $csubm->id;?>/<?php echo $csubm->mname;?>"><?php echo $csubm->child_sub_menu_name;?></a>
										</li>
												
											
									
									<?php } ?> 
									
										</ul>									
									
									</li>
									
								<?php } ?>
                                </ul>
                            
                        </li>
						<li class="li-hover"><div class="divider"></div></li>
					<?php } ?>
					</ul>
                    <li class="bold close" id="side-menu-one">
					<a href="<?php echo base_url('admin/course_list'); ?>" class="waves-effect waves-cyan">
						<i class="small icon mdi-action-view-headline change-color"></i>Manage Program</a>
					</a>
					</li>
					<li class="li-hover"><div class="divider"></div></li>
					  <li class="bold close" id="side-menu-one">

						<a href="<?php echo base_url('admin/university'); ?>" class="waves-effect waves-cyan">
						<i class="small icon mdi-maps-local-library change-color"></i>Manage Universities</a>
					</a>
					</li>
						<li class="li-hover"><div class="divider"></div></li>
					<li class="bold close" id="side-menu-one">
					<a href="<?php echo base_url('admin/user_setting'); ?>" class="waves-effect waves-cyan">
						<i class="small icon mdi-action-settings change-color"></i>Change Password</a>
					</a>
					</li>
					
					<!-- <li class="li-hover"><div class="divider"></div></li> -->
					<!--li class="bold menu_tabs close" id="side-menu-four">
						<a href="<?php echo base_url('admin/category'); ?>" class="waves-effect waves-cyan">
						  <i class="small icon icon-ha-menu-black-icon-04 change-color"></i>
							Category
							</span>
						</a>
					</li>
					<li class="li-hover"><div class="divider"></div></li>
					<li class="bold menu_tabs close" id="side-menu-four">
						<a href="<?php echo base_url('admin/packages'); ?>" class="waves-effect waves-cyan">
						  <i class="mdi-editor-format-list-bulleted"></i>
							Packages
							</span>
						</a>
					</li>
					<li class="bold menu_tabs close" id="side-menu-four">
						<a href="<?php echo base_url('admin/menu'); ?>" class="waves-effect waves-cyan">
						  <i class="mdi-editor-format-list-bulleted"></i>
							Manage Master
							</span>
						</a>
					</li>
					<li class="li-hover"><div class="divider"></div></li>
					<li class="bold menu_tabs close" id="side-menu-seven">
						<a href="<?php echo base_url('login/logout'); ?>" class="waves-effect waves-cyan">
							<i class="mdi-action-settings-power"></i>
							Logout 
						</a>
					</li -->
					<li class="li-hover"><div class="divider"></div></li>
                </ul>
					<input type="hidden" id="menuid" >
                <a href="#" data-activates="slide-out" class="sidebar-collapse btn-floating btn-medium waves-effect waves-light " id="side"><i class="mdi-navigation-menu"></i></a>
            </aside>
		
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
			
<script>
$(document).ready(function(){
	$('#side').click(function(e){
	
	})

		$("#add_program_form").hide();
		$("#edit_program_form").hide();
		
    $("#master").click(function(){
		$("#add_program_form").hide();
		$("#edit_program_form").hide();

    $.ajax({
      url: '<?php echo base_url(); ?>admin/subMenu/1',
      async: false,
      type: "POST",
      //data: "type=" + type,
      dataType: "html",
      success: function(data) {
 
	   var success =  $(data).find("section"); 
	//	$('body').load();
		//$('section').empty().replaceWith(success);
		$('section').empty();
		$('section').append(success);
		$('body').removeClass('collapsible-body');
	
      }
    });
 
	
    }); 

	
});
function sub_menu(id){	
get_child_d(id);
//$('section').html(null);
$("#edit_program_form").hide();
$("#add_program_form").hide();
	
	$('body').load();
    $.ajax({
      url: '<?php echo base_url(); ?>admin/child_subMenu/'+id,
      async: false,
      type: "POST",
      //data: "type=" + type,
      dataType: "html",
      success: function(data) {
	    var success =  $(data).find("#content3"); 
		$('section').empty();
		$('section').append(success);
		$('body').removeClass('collapsible-body');
      }
    });
 
		
    }
	
function get_child_d(id)
{
	$('#menuid').val(id);
//	alert(id);
	$.ajax({
			type:'POST',
			url:'<?php echo base_url()?>admin/get_child_data/'+id,
			//data:{'id':id},
			success:function(res){
				//alert(res);
				var obj=JSON.parse(res);
				console.log(obj);
			   var html='';
				$.each(obj, function(ind,val){
					
						var msg='';
						if(val.status==1)
						{
							msg+='Active';
						}
						else{
							msg+='InActive';
						}
						var msg1='';
						if(val.status==1)
						{
							msg1+='<a href="#" onclick="del_ch('+val.id+')"><img class="sideIcon cust-cursor" src="<?php echo base_url('assets/img/delete-icon-02.png'); ?>"/></a>';
						}
						else{
							msg1+='<a href="#" onclick="active('+val.id+')"><img class="sideIcon cust-cursor" src="<?php echo base_url('assets/images/icon-03.png'); ?>"/></a>';
						}
						
						html+='<tr ><td class="upm-table-content-bg center-align">'+val.child_sub_menu_name+'</td>'+
						'<td class="upm-table-content-bg center-align">'+msg+'</td>'+
						'<td class="upm-table-content-bg center-align">  '+
						'<a href="#" onclick="edit_ch('+val.id+')"><img class="sideIcon cust-cursor" src="<?php echo base_url('assets/img/edit-icon-01.png'); ?>"/></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+
					    ''+msg1+''
						'</td></tr>';
						
						
				})
				$('#showdata').html(html);
					
			}
		})
}

//  master append


</script>
<script type="text/javascript" src="<?php echo base_url('assets/js/plugins/jquery-1.11.2.min.js');?>"></script>


<script>
$("#edit_program_form_1").hide();
$("#add_program_form_1").hide();

$('#add_program').click(function(){
	//alert('1');
	$('#add_program_form_1').slideToggle( "slow" );
});

$("#edit_program_form").hide();
$("#add_program_form").hide();

$('#add_program').click(function(){
	$('#add_program_form').slideToggle( "slow" );
});


 $('#update').click(function(){
	//alert('HI');
	var name=$('#menun').val();
	var id=$('#mid').val();
	if(name=='')
	{
		$('#menun').focus();
		toastr.error('Enter Program Name');
		$('#menun').css('border-color:red');
		return false;
	}
	else
	{
		$.ajax({
			type:'POST',
			url:'<?php echo base_url()?>admin/add_menu/update_child_sub_master',
			data:{'id':id,'name':name},
			success:function(res){
				//alert(res);
				location.reload();
				
			}
		})
	}
});




function del_ch(id)
{
	 var $tr = $(this).closest('tr'); 
	$.ajax({
			type:'POST',
			url:'<?php echo base_url()?>admin/add_menu/del_child_sub_master',
			data:{'id':id},
			success:function(res){
				//alert(res);
				 toastr.success("Deleted succesfully");
				// sub_menu(id);
			//$('body').load('<?php echo base_url(); ?>admin/child_subMenu/'+id);
					//$('#ts_'+id).closest('tr').append();
					var menuid=$('#menuid').val();
					console.log(menuid);
					 get_child_d(menuid);
			}
		})
}
function edit_ch(id)
{
	//$('#add_program').show( "slow" );
	$.ajax({
			type:'POST',
			url:'<?php echo base_url()?>admin/add_menu/edit_child_sub_master',
			data:{'id':id},
			success:function(res){
				obj = JSON.parse(res);
				//console.log(obj);
				$('#edit_program_form_1').show( "slow" );
				$('#add_program_form_1').hide( "slow" );
				//$('#add_program').hide( "slow" );
					$('#menun1').val(obj[0].child_sub_menu_name);
					$('#mid1').val(id);
				//location.reload();
			}
		})
}
</script>

