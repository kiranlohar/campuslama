<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="">
      <meta name="keywords" content="">
      <meta name="author" content="">
      <meta name="robots" content="noindex, nofollow" />
      <title>Hello Admission</title>
      <style type="text/css">
         /*  @import url('//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');*/
         .menuzord-menu>li.active:hover .megamenu {
         display: block;
         right: 0px;
         }
         .nopaddingleft {
         padding-left: 0;
         }
         .nopaddingright {
         padding-right: 0;
         }
         .overflownone {
         overflow: hidden!important;
         }
         .information {
         position: relative;
         height: 400px;
         }
         .information h3 {
         font-size: 34px;
         font-size: 3.4rem;
         font-family: "roboto", sans-serif;
         font-weight: 500;
         color: #FFF;
         padding: 0 15px;
         }
         .information .information_menu {
         padding: 0;
         }
         .information .information_menu li {
         list-style: none;
         position: relative;
         }
         .information .information_menu li.active {
         background: #0383c5;
         }
         .information .information_menu li.active:after {
         width: 0;
         height: 0;
         border-top: 27px solid transparent;
         border-bottom: 27px solid transparent;
         border-left: 22px solid #0383c5;
         position: absolute;
         right: -22px;
         z-index: 1000;
         top: 0;
         }
         .information .information_menu li a {
         color: #FFF;
         display: block;
         padding: 16px 20px;
         font-size: 15px;
         font-size: 1.5rem;
         text-decoration: none;
         }
         .information .btn-emergency {
         background: #f89406;
         border: none;
         margin: 20px;
         display: block;
         width: 88%;
         color: #fff;
         text-shadow: none;
         padding: 0;
         font-size: 20px;
         font-size: 2rem;
         }
         .information .btn-emergency span {
         padding: 15px;
         display: block;
         background-size: 30px 30px;
         background-image: -webkit-gradient(linear, left top, right bottom, color-stop(0.25, rgba(255, 255, 255, .15)), color-stop(0.25, transparent), color-stop(0.5, transparent), color-stop(0.5, rgba(255, 255, 255, .15)), color-stop(0.75, rgba(255, 255, 255, .15)), color-stop(0.75, transparent), to(transparent));
         background-image: -webkit-linear-gradient(135deg, rgba(255, 255, 255, .15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .15) 50%, rgba(255, 255, 255, .15) 75%, transparent 75%, transparent);
         background-image: -o-linear-gradient(135deg, rgba(255, 255, 255, .15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .15) 50%, rgba(255, 255, 255, .15) 75%, transparent 75%, transparent);
         background-image: linear, 135deg, rgba(255, 255, 255, .15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .15) 50%, rgba(255, 255, 255, .15) 75%, transparent 75%, transparent;
         -webkit-animation: animate-stripes 3s linear infinite;
         -moz-animation: animate-stripes 3s linear infinite;
         }
         .mobile-menu-bg {
         background: #2a3d46;
         }
         .information_menu_mobile {
         margin: 0px;
         padding: 0;
         }
         .information_menu_mobile li {
         padding: 0;
         float: left;
         width: 25%;
         list-style: none;
         text-align: center;
         }
         .information_menu_mobile li a {
         display: block;
         padding: 10px;
         color: #FFF;
         text-decaration: none;
         }
         .information_menu_mobile li:hover a {
         background: #0383c5;
         text-decaration: none;
         }
         .slideshow {
         overflow: auto;
         height: 480px;
         }
         .slideshow .overlay-item  {
         position: absolute;
         top: 0;
         z-index: 999;
         width: 90%;
         background: rgba(158, 158, 158, 0.15);
         height: 500px;
         padding: 10px 30px;
         display: none;
         -webkit-animation: opendoor 0.75s ease 0s alternate;
         -webkit-transform-style: preserve-3d;
         transform-style: preserve-3d;
         }
         .slideshow .overlay-item.active {
         display: block;
         -webkit-animation: opendoor 0.75s ease 0s alternate;
         -webkit-transform-style: preserve-3d;
         transform-style: preserve-3d;
         }
         .slideshow .overlay-item.inactive {
         display: block;
         -webkit-animation: closedoor 0.75s ease 0s alternate;
         transform-style: preserve-3d;
         opacity:0;
         }
         .slideshow .overlay-item .quickmenu  {
         padding: 0;
         }
         .slideshow .overlay-item .quickmenu li {
         list-style: none;
         }
         .slideshow .overlay-item .quickmenu li a {
         color: #FFF;
         display: block;
         padding: 8px 20px;
         font-size: 15px;
         font-size: 1.5rem;
         text-decoration: none;
         }
         #slider {
         padding: 0px;
         margin: 0px
         }
         #slider li {
         padding: 0px;
         list-style: none;
         position: absolute;
         }
         #carousel-example-generic {
         height: 400px
         }
         .carousel-fade .carousel-inner .item1 {
         -webkit-transition-property: opacity;
         transition-property: opacity;
         }
         .carousel-fade .carousel-inner .item1,
         .carousel-fade .carousel-inner .active.left,
         .carousel-fade .carousel-inner .active.right {
         opacity: 0;
         filter: alpha(opacity=0);
         }
         .carousel-fade .carousel-inner .active,
         .carousel-fade .carousel-inner .next.left,
         .carousel-fade .carousel-inner .prev.right {
         opacity: 1;
         filter: alpha(opacity=100);
         }
         .carousel-fade .carousel-inner .next,
         .carousel-fade .carousel-inner .prev,
         .carousel-fade .carousel-inner .active.left,
         .carousel-fade .carousel-inner .active.right {
         left: 0;
         -webkit-transform: translate3d(0, 0, 0);
         transform: translate3d(0, 0, 0);
         }
         .carousel-fade .carousel-control {
         z-index: 2;
         }
         html,
         body,
         .carousel,
         .carousel-inner,
         .carousel-inner .item1 {
         height: 100%;
         }
         .item1:nth-child(1) {
         background: #74C390;
         }
         .item1:nth-child(2) {
         background: #51BCE8;
         }
         .item1:nth-child(3) {
         background: #E46653;
         }
         @-webkit-keyframes animate-stripes {
         0% {
         background-position: 0 0;
         }
         100% {
         background-position: 60px 0;
         }
         }
         @-webkit-keyframes in {
         from {
         -webkit-transform: scale(1.3);
         }
         to {
         -webkit-transform: scale(1);
         }
         }
         @-webkit-keyframes out {
         0% {
         -webkit-transform: scale(1);
         }
         100% {
         -webkit-transform: scale(1.3);
         }
         }
         @-webkit-keyframes opendoor {
         from {
         -webkit-transform: perspective(1000px) rotateY(90deg);
         -webkit-transform-origin: 0% 50%;
         }
         to {
         -webkit-transform: perspective(1000px) rotateY(0deg);
         -webkit-transform-origin: 0% 50%;
         }
         }
         @-webkit-keyframes closedoor {
         from {
         opacity:1;
         -webkit-transform-origin: 0% 50%;
         }
         to {
         opacity:0;
         -webkit-transform-origin: 0% 50%;
         }
         }
         .menuzord-menu>li>.megamenu .dropdown-menu{
         display: block;
         }
         .img {
         background-image    : url('http://lp.campuslama.com/assets/img/MBA.jpg');
         background-repeat: no-repeat;
         background-size: 92%;
         }
         .active1 {
         position: absolute;
         content: '';
         top: 50%;
         margin-top: -9px;
         right: 0;
         display: inline-block;
         border-right: 9px solid #E3E3E3;
         border-top: 9px solid transparent;
         border-bottom: 9px solid transparent;
         }
         .nav-subtext {
         display: block;
         font-size: 10px;
         line-height: 13px;
         font-weight: 400;
         color: #999;
         white-space: normal;
         }
      </style>
      <!--  favicon -->
      <link rel="shortcut icon" href="<?php echo base_url('assets/img/fav.png'); ?>">
      <!--  apple-touch-icon -->
      <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url()?>assets_new/img/ico/apple-touch-icon-144-precomposed.png">
      <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url()?>assets_new/img/ico/apple-touch-icon-114-precomposed.png">
      <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url()?>assets_new/img/ico/apple-touch-icon-72-precomposed.png">
      <link rel="apple-touch-icon-precomposed" href="<?php echo base_url()?>assets_new/img/ico/apple-touch-icon-57-precomposed.png">
      <link href='https://fonts.googleapis.com/css?family=Raleway:400,300,500,700,900' rel='stylesheet' type='text/css'>
      <!-- Material Icons CSS -->
      <link href="<?php echo base_url()?>assets_new_2/fonts/iconfont/material-icons.css" rel="stylesheet">
      <!-- FontAwesome CSS -->
      <link href="<?php echo base_url()?>assets_new_2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet">
      <!-- materialize -->
      <link href="<?php echo base_url()?>assets_new_2/materialize/css/materialize.min.css" rel="stylesheet">
      <!-- Bootstrap -->
      <link href="<?php echo base_url()?>assets_new_2/bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <!-- shortcodes -->
      <link href="<?php echo base_url('assets/explore/css/frontend/quiz_mycustom.css');?>" type="text/css" rel="stylesheet">
      <link href="<?php echo base_url('assets/explore/css/frontend/quiz_mycustom_media.css');?>" type="text/css" rel="stylesheet">
      <link href="<?php echo base_url()?>assets_new_2/css/shortcodes/shortcodes.css" rel="stylesheet">
      <link href="<?php echo base_url()?>assets_new_2/css/shortcodes/login.css" rel="stylesheet">
      <link rel="stylesheet" href="<?php echo base_url('assets/css/matexcss/main.css');?>">
      <link href="<?php echo base_url()?>assets_new_2/style.css" rel="stylesheet">
      <link href="<?php echo base_url('assets/explore/css/animate.css');?>" type="text/css" rel="stylesheet">
      <link href="<?php echo base_url('assets/explore/css/ion.rangeSlider.css');?>" type="text/css" rel="stylesheet"/>
      <link href="<?php echo base_url('assets/explore/css/ion.rangeSlider.skinHTML5.css');?>" type="text/css" rel="stylesheet"/>
      <link href="<?php echo base_url('assets/explore/css/frontend/quiz_mycustom.css');?>" type="text/css" rel="stylesheet">
      <link href="<?php echo base_url('assets/explore/css/frontend/quiz_mycustom_media.css');?>" type="text/css" rel="stylesheet">
      <link href="<?php echo base_url('assets_new/css/line.css');?>" type="text/css" rel="stylesheet">
      <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css" type="text/css" rel="stylesheet">
  
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo base_url('assets/explore/js/ion.rangeSlider.js');?>"></script>
   <script type="text/javascript" src="<?php echo base_url('assets/explore/js/frontend/mobile_detect.js');?>"></script>
   <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/1.3.1/js/toastr.js"></script>
    </head>
   <body id="top" class="has-header-search">
      <?php 
         $userdata = $this->session->userdata('logged_in');
         //print_r($userdata);
         ?>
      <!--header start-->
      <?php
         if(!empty($userdata)){
         $verify = $this->db->get_where('users',array('id'=>$userdata['id']));
          $emailverify=$verify->result();
         // echo $emailverify[0]->email_confirmed;
         if($emailverify[0]->email_confirmed=='NO'){
         ?>
      <header id="error_msg" class="second " style="background-color:#e64848;height: 50px;color: white;padding-top: 10px;font-weight:  bold;text-align:  center;">
         Please Verify Your Email Address.     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" class="swapbutton btn blue div15-btn" style="color:white;padding-top: 0px;height: 31px;" onclick="verify(<?php echo $userdata['id'] ?>)"><b> VERIFY</b></a>
      </header>
      <?php } } ?>
      <header id="" class="tt-nav transparent-header">
         <div class="header-sticky light-header">
            <div class="container">
               <div class="search-wrapper">
                  <div class="search-trigger">
                     <!--div class='search-btn'></div>
                        <i class="material-icons">&#xE8B6;</i-->
                  </div>
                  <!-- Modal Search Form -->
                  <a href="<?php echo base_url('search')?>"><i class="search-close material-icons">&#xE5CD;</i></a>
               </div>
               <!-- /.search-wrapper -->
               <!-- <div id="materialize-menu" class="menuzord"> -->
                    <div id="materialize-menu" class="menuzord">
                  <a href="javascript:void(0)" class="showhide">
                     <em></em>
                     <em></em>
                     <em></em>
                  </a>
                  <!--logo start-->
                  <a href="<?php echo base_url('home')?>" class="logo-brand" style="margin-top:0px">
                  <img src="<?php echo base_url()?>assets/img/logo-01.png" alt="Hello Admission"  style=" /* padding-top: 11px; */  margin-top: 33px;">
                  </a>
               <!--    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                    <i class="fa fa-bars"></i>
                </button> -->
                  <!--logo end-->
                  <!--mega menu start-->
                  <ul class="menuzord-menu pull-right">
                     <li class="menu">
                        <a href="javascript:void(0)" onmouseover="get_Megamenu(1)">Management <span class="indicator"><i class="fa fa-angle-down"></i></span></a>
                        <div class="megamenu dropdown-menu line-navbar-two" role="menu" style="width: auto;">
                           <div class="">
                              <div class="lnt-dropdown-mega-menu" style="margin-top:-20px;margin-left: -20px;margin-bottom: -20px;margin-right: -20px;">
                                 <!-- List of categories -->
                                 <ul class="lnt-category list-unstyled master_menu" id="master_menu">
                                    <li class="active"><a href="#subcategory-home">MBA</a></li>
                                    <li><a href="#subcategory-sports">Sports and outdoors </a></li>
                                    <li><a href="#subcategory-music">Digital music</a></li>
                                    <li><a href="#subcategory-books">Books </a></li>
                                    <li><a href="#subcategory-fashion">Fashion and beauty</a></li>
                                    <li><a href="#subcategory-movies">Movies and games</a></li>
                                 </ul>
                                 <!-- Subcategory and carousel wrap -->
                                 <div class="lnt-subcategroy-carousel-wrap container-fluid img">
                                    <div id="subcategory-home" class="active">
                                       <!-- Sub categories list-->
                                       <div class="lnt-subcategory col-sm-12 col-md-12 master_sub" style="padding-bottom:0px;font-weight:bold" id="master_sub">
                                       </div>
                                    </div>
                                    <!-- /.subcategory-home -->
                                    <div id="subcategory-sports">
                                       <!-- Sub categories list-->
                                       <div class="lnt-subcategory col-sm-8 col-md-8" style="padding-bottom: 120px;">
                                          <h3 class="lnt-category-name">Sports and outdoors</h3>
                                          <ul class="list-unstyled col-sm-6">
                                             <li><a href="#">Exercise</a></li>
                                             <li><a href="#">Fitness</a></li>
                                             <li><a href="#">Hunting</a></li>
                                             <li><a href="#">Fishing</a> <span class="label label-primary">Trending</span></li>
                                          </ul>
                                          <ul class="list-unstyled col-sm-6">
                                             <li><a href="#">Golf</a></li>
                                             <li><a href="#">Outdoor clothing</a></li>
                                             <li><a href="#">Cycling</a></li>
                                          </ul>
                                       </div>
                                       <!-- Carousel -->
                                       <div class="col-sm-4 col-md-4">
                                       </div>
                                    </div>
                                    <!-- /.subcategory-sports -->
                                 </div>
                                 <!-- /.lnt-subcategroy-carousel-wrap -->
                              </div>
                              <!-- /.lnt-dropdown-mega-menu -->
                           </div>
                        </div>
                     </li>
                     <li>  
                        <a href="<?php echo base_url('home'); ?>" class="black-text">Home</a>&nbsp;&nbsp;
                     </li>
                     <li> 
                        <a href="<?php echo base_url('explorer'); ?>" class="black-text">Explore</a>&nbsp;&nbsp;
                     </li>
                     <?php if(!$this->session->userdata('logged_in')){ ?>
                     <li>  
                        <a href="<?php echo base_url('login'); ?>" class="black-text">Login</a>&nbsp;&nbsp;
                     </li>
                     <!-- <li> 
                        <a href="<?php echo base_url('login'); ?>" class="black-text">SignUp</a>&nbsp;&nbsp;
                        </li> -->
                     <?php }else{ 
                        $userdata = $this->session->userdata('logged_in');
                        ?>
                     <li>
                        <?php if($userdata['role'] == 2){ ?>
                        <a href="<?php echo base_url('dashboard/student'); ?>" class="black-text"><?php echo $userdata['name']; ?></a>&nbsp;&nbsp;
                        <?php }else if($userdata['role'] == 1){ ?>
                        <a href="<?php echo base_url('dashboard/au'); ?>" class="black-text"><?php echo $userdata['name']; ?></a>&nbsp;&nbsp;
                        <?php }else if($userdata['role'] == 4){ ?>
                        <a href="<?php echo base_url('admin/dashboard'); ?>" class="black-text"><?php echo $userdata['name']; ?></a>&nbsp;&nbsp;
                        <?php } ?>
                     </li>
                     <li>  
                        <a href="<?php echo base_url('logout'); ?>" class="black-text">Logout</a>&nbsp;&nbsp;
                     </li>
                     <?php } ?>
                     <?php 
                        $userdata = $this->session->userdata('logged_in');
                        if(!$userdata){
                        ?>
                     <!-- <li> 
                        <a href="<?php echo base_url('login/academic_register'); ?>" class="black-text">College Signup</a>&nbsp;&nbsp;
                        </li> -->
                     <?php }?>
                     <li class="">  
                        <a href="<?php echo base_url('search'); ?>" class="black-text" style="display:inline;"><img src="<?php echo base_url('assets_new/img/seach_icon.png');?>" ></a>
                     </li>
                  </ul>
                  <!--mega menu end-->
               </div>
            </div>
         </div>
      </header>
      <!--header end-->
      <script type="text/javascript">
         function ajaxindicatorstart(text)
         {
             if(jQuery('body').find('#resultLoading').attr('id') != 'resultLoading'){
             jQuery('body').append('<div id="resultLoading" style="display:none"><div><img src="<?php echo base_url()?>uploads/ajax-loader.gif"><div>'+text+'</div></div><div class="bg"></div></div>');
             }
         
             jQuery('#resultLoading').css({
                 'width':'100%',
                 'height':'100%',
                 'position':'fixed',
                 'z-index':'10000000',
                 'top':'0',
                 'left':'0',
                 'right':'0',
                 'bottom':'0',
                 'margin':'auto'
             });
         
             jQuery('#resultLoading .bg').css({
                 'background':'#000000',
                 'opacity':'0.7',
                 'width':'100%',
                 'height':'100%',
                 'position':'absolute',
                 'top':'0'
             });
         
             jQuery('#resultLoading>div:first').css({
                 'width': '250px',
                 'height':'75px',
                 'text-align': 'center',
                 'position': 'fixed',
                 'top':'0',
                 'left':'0',
                 'right':'0',
                 'bottom':'0',
                 'margin':'auto',
                 'font-size':'16px',
                 'z-index':'10',
                 'color':'#ffffff'
         
             });
         
             jQuery('#resultLoading .bg').height('100%');
                jQuery('#resultLoading').fadeIn(300);
             jQuery('body').css('cursor', 'wait');
         }
         
         function ajaxindicatorstop()
         {
             jQuery('#resultLoading .bg').height('100%');
                jQuery('#resultLoading').fadeOut(300);
             jQuery('body').css('cursor', 'default');
         }
         
      </script>
      <script type="text/javascript">
         function verify(id)
         {
         //  toastr["info"]('ok');
         // return false;
         var email="<?php echo trim($userdata['email']);?>";
         var name="<?php echo trim($userdata['name']);?>";
         var dataString={id:id,email:email,name:name};
         $.ajax({
                     type:'POST',
                     url:'<?php echo base_url(); ?>student/email_verify',
                     data:dataString,
                      beforeSend: function () {
                          ajaxindicatorstart('Please wait..');
                     },
                     success:function(res)
                     {
                        var obj = JSON.parse(res);
                        console.log(obj);
                         if(obj.msg=='Please check your email at to verify the email address')
                          { 
                             //toastr["error"]('Please verify email &nbsp;&nbsp;'+'Click here to <a href="<?php echo base_url() ?>login/verify/<?php echo md5($userdata['name']); ?>/<?php echo base64_encode($userdata['id']); ?>" target="_blank">Verify</a>');
                              toastr["info"](obj.msg).css("width","500px");
                              ajaxindicatorstop();
                         }
         
                     }
                 });
                 
         }
         
         
         
         function get_Megamenu(id){
            var streamid = id;
            var smid=id+'-Management-active';
           var steam_Id  = smid.split('-');
            if(steam_Id[0]==1){
               get_sub_megamenu(steam_Id[0]); 
               $('.menu').addClass('active');
               $('.menu1').removeClass('active');
            }else{
                get_sub_megamenu(); 
                $('.menu').removeClass('active');
                $('.menu1').addClass('active');
            }
         
         get_sub_megamenu(smid);
         $("#submenu"+smid).removeClass('active');
         $('.master_menu').empty();
         $.ajax({
         type: "POST",
         url: "<?php echo base_url('academic/stream_program'); ?>",
         data:{streamid:streamid},
         success: function(response){
            var res = JSON.parse(response);
            var html='';
            var j=1;
            $.each(res, function (i, itm) {
            html+='<li id="submenu'+itm.id+'"><a href="#" id="'+itm.id+'-'+itm.name+'"  onmouseover="get_sub_megamenu(this.id)" onmouseout="normalImg(this.id)">'+itm.name+'</a></li>';                               
            });
         
            $('.master_menu').html(html);
         }
         });
         }
         
         
         function  normalImg(id){
         var streamid = id;
         steam_Id = streamid.split('-');
         $("#submenu"+steam_Id[0]).removeClass('active');
         }
         function get_sub_megamenu(id){
            var streamid = id;
         
         steam_Id = streamid.split('-');
         console.log(steam_Id[0]);
         
         $("#submenu"+steam_Id[0]).addClass('active');
         $.ajax({
         type: "POST",
         url: "<?php echo base_url('academic/stream_program_spe'); ?>",
         data:{streamid:steam_Id[0]},
         success: function(response){
            var res = JSON.parse(response);
           console.log(res);
            // $('#program').append('<option value="" disabled selected>Choose Program</option>');
            var html='';
            var j=1;
             html+='<h3 class="lnt-category-name">'+steam_Id[1]+'</h3><ul class="list-unstyled col-md-4" style="font-weight:bold">';
            $.each(res, function (i, itm) {
               html+='<li><u><a href="<?php echo base_url()?>listing/viewcource/'+itm.id+'/'+steam_Id[1]+'"><b>'+itm.name +'</b></a></u></li>';
                                         
         
                   if((i+1)%4 ===0){
         
                  html+= '</ul><ul class="list-unstyled col-md-4" >';
                }                             
            });
            html+='</ul>';   
            $('.master_sub').html(html);
         }
         });
         }
      </script>
      <script type="text/javascript">
         $( document ).ready(function(){
           $(".button-collapse").sideNav();
         });
           
      </script>
      <script type="text/javascript">
         !function(){"use strict";function a(){n.addClass("lnl-show").removeClass("lnl-hide"),s.addClass(i).css({height:o,overflow:"hidden"}),$("html, body").css("overflow","hidden"),l.find("span").removeClass("fa-bars").addClass("fa-remove")}function t(){n.removeClass("lnl-show").addClass("lnl-hide"),s.removeClass(i).css({height:"auto",overflow:"auto"}),$("html, body").css("overflow","auto"),l.find("span").removeClass("fa-remove").addClass("fa-bars")}function e(){try{return document.createEvent("TouchEvent"),!0}catch(a){return!1}}var n=$(".line-navbar-left"),l=$(".lno-btn-toggle"),s=($(".lno-btn-collapse"),$(".content-wrap")),i=s.data("effect"),o=$(window).height()-95;n.addClass("lnl-hide"),l.click(function(){n.hasClass("lnl-hide")?a():t()}),l.click(function(a){a.preventDefault(),n.hasClass("lnl-collapsed")?(n.removeClass("lnl-collapsed"),s.removeClass("lnl-collapsed"),$(this).find(".lnl-link-icon").removeClass("fa-arrow-right").addClass("fa-arrow-left")):(n.addClass("lnl-collapsed"),s.addClass("lnl-collapsed"),$(this).find(".lnl-link-icon").removeClass("fa-arrow-left").addClass("fa-arrow-right"))}),1==e()&&$(window).swipe({swipeRight:function(){a(),$(".navbar-collapse").removeClass("in")},swipeLeft:function(){t()},threshold:75}),$(window).resize(function(){$(window).width()>=767&&t()}),$(".lnt-search-input").focusin(function(){$(".lnt-search-suggestion").find(".dropdown-menu").slideDown()}),$(".lnt-search-input").focusout(function(){$(".lnt-search-suggestion").find(".dropdown-menu").slideUp()});var r=$(" .lnt-search-category ").find(" .dropdown-menu ").find(" li ");r.click(function(){var a=$(this).find(" a ").text(),t=$(" .selected-category-text ");t.text(a)}),$(".lnt-search-input").bind("keyup change",function(){var a=$(this).val(),t=$(".lnt-search-suggestion").find(".dropdown-menu > li > a");t.unhighlight({element:"strong",className:"important"}),a&&t.highlight(a,{element:"strong",className:"important"})}),$(".add-to-cart").click(function(){var a=randomColor({luminosity:"light",format:"rgb"}),t=$(".lnt-cart").find(".cart-item-quantity").text();$(".lnt-cart").css("backgroundColor",a),$(".lnt-cart").find("span").eq(0).addClass("item-added"),$(".lnt-cart").find(".cart-item-quantity").text(++t)}),$(".add-to-cart").click(function(){var a=randomColor({luminosity:"light",format:"rgb"}),t=$(".lno-cart").find(".cart-item-quantity").text();$(".lno-cart").css("backgroundColor",a),$(".lno-cart").find("span").eq(0).addClass("item-added"),$(".lno-cart").find(".cart-item-quantity").text(++t)});var c=$(".lnt-category").find("li").find("a"),d=$(".lnt-category").find("li");c.mouseenter(function(){d.removeClass("active"),$(this).parent().addClass("active");var a=$(this).attr("href");$(".lnt-subcategroy-carousel-wrap").find("> div").removeClass("active"),$(a).addClass("active")}),c.on("touchstart, touchend",function(a){a.preventDefault(),d.removeClass("active"),$(this).parent().addClass("active");var t=$(this).attr("href");$(".lnt-subcategroy-carousel-wrap").find("> div").removeClass("active"),$(t).addClass("active")}),1==e()&&($(window).swipe({swipeLeft:function(){$(".carousel").carousel("next")},swipeRight:function(){$(".carousel").carousel("prev")},threshold:75}),$(".carousel-indicators").hide()),$(".carousel-indicators").find("li").mouseenter(function(){var a=$(this).data("slide-to");$(this).parents(".carousel").carousel(a)}),String.prototype.capitalize=function(){return this.replace(/(?:^|\s)\S/g,function(a){return a.toUpperCase()})},$(".lnt-category > li").each(function(){var a=$(this).find("a");$(".lnl-nav").append("<li><a class='collapsed' data-toggle='collapse' href='#collapse"+a.text().capitalize().replace(/[, ]+/g,"")+"' aria-expanded='false' aria-controls='collapse"+a.text().capitalize().replace(/[, ]+/g,"")+"' data-subcategory="+a.attr("href").replace("#","")+"><span class='lnl-link-text'>"+$(this).text()+"</span><span class='fa fa-angle-up lnl-btn-sub-collapse'></span></a></li>")}),$(".lnl-nav li").each(function(){var a=$(this).find("a");$(this).append("<ul class='lnl-sub-one collapse' id='"+a.attr("href").replace("#","")+"' data-subcategory='"+a.data("subcategory")+"'></ul>")}),$(".lnt-subcategroy-carousel-wrap > div").each(function(){var a=$(this).attr("id"),t=$(this).find("ul").map(function(){return $(this).html()}).get(),e=$("ul[data-subcategory='"+a+"']");e.append(t)}),$(".navbar-toggle").click(function(){$(this).hasClass("collapsed")&&t()}),l.click(function(){$(".navbar-collapse").removeClass("in")})}();
           
      </script>
   </body>
</html>