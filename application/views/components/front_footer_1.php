 <footer class="footer footer-four" style="padding:0px 0px 0px; ">
            <div class="primary-footer brand-bg text-center">
                <div class="container">

                  <ul class="social-link tt-animate ltr mt-20">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-tumblr"></i></a></li>
                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                    <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                    <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                    <li><a href="#"><i class="fa fa-rss"></i></a></li>
                  </ul>

                  <hr class="mt-15">
                  <div class="row">
                      <div class="col-md-12">
                          <div class="footer-logo">
                            <img src="<?php echo base_url();?>assets/img/logo-01.png" alt="">
                          </div>

                          <span class="copy-text" style="color:white"> &copy; 2017-18  | <a href="#">Hello Admission</a> &nbsp; | &nbsp;  All Rights Reserved &nbsp; | &nbsp;  Designed By <a href="http://infinite1.in"><img style="width: 100px;padding-bottom: 2px;" src="<?php echo base_url() ?>assets/img/infinite1-logo-128X80-01.png" alt=""></a></span>
                          <p></p>
                          <div class="footer-intro">
                            <p></p>
                          </div>
                    </div><!-- /.col-md-12 -->
                  </div><!-- /.row -->
                </div><!-- /.container -->
            </div><!-- /.primary-footer -->

            <div class="secondary-footer brand-bg darken-2 text-center">
                <div class="container">
                    <ul>
                      <li><a href="<?php echo base_url()?>home">Home</a></li>
                      <li><a href="<?php echo base_url()?>about-us">About us</a></li>
                      <li><a href="<?php echo base_url()?>services">Services</a></li>
                      <li><a href="<?php echo base_url()?>term-condition">Terrms & Condition</a></li>
                      <li><a href="<?php echo base_url()?>contact">Contact us</a></li>
                    </ul>
                </div><!-- /.container -->
            </div><!-- /.secondary-footer -->
        </footer>



        <!-- Preloader -->
        <div id="preloader">
          <div class="preloader-position"> 
		  Hello Addmission
            <!--img src="<?php echo base_url()?>assets_new/img/logo-colored.png" alt="logo" -->
            <div class="progress">
              <div class="indeterminate"></div>
            </div>
          </div>
        </div>
        <!-- End Preloader -->

        

        <!-- jQuery -->
        <script src="<?php echo base_url()?>assets_new/js/jquery-2.1.3.min.js"></script>
		 <script src="<?php echo base_url()?>assets_new/js/scripts.js"></script>
        <script src="<?php echo base_url()?>assets_new/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url()?>assets_new/materialize/js/materialize.min.js"></script>
        <script src="<?php echo base_url()?>assets_new/js/menuzord.js"></script>
        <script src="<?php echo base_url()?>assets_new/js/jquery.easing.min.js"></script>
        <script src="<?php echo base_url()?>assets_new/js/jquery.sticky.min.js"></script>
        <script src="<?php echo base_url()?>assets_new/js/smoothscroll.min.js"></script>
        <script src="<?php echo base_url()?>assets_new/js/jquery.stellar.min.js"></script>
        <script src="<?php echo base_url()?>assets_new/js/imagesloaded.js"></script>
        <script src="<?php echo base_url()?>assets_new/js/animated-headline.js"></script>
       

        <script>
            $('.toggle').on('click', function() {
              $('.login-wrapper').stop().addClass('active');
            });

            $('.close').on('click', function() {
              $('.login-wrapper').stop().removeClass('active');
            });

        </script>
     <!-- RS5.0 Core JS Files -->
        <script src="<?php echo base_url();?>assets_new/revolution/js/jquery.themepunch.tools.min.js"></script>
        <script src="<?php echo base_url();?>assets_new/revolution/js/jquery.themepunch.revolution.min.js"></script>

        <!-- RS5.0 Init  -->
        <script type="text/javascript">
            jQuery(document).ready(function() {
               jQuery(".materialize-slider").revolution({
                    sliderType:"standard",
                    sliderLayout:"fullscreen",
                    delay:9000,
                    navigation: {
                        keyboardNavigation: "on",
                        keyboard_direction: "horizontal",
                        mouseScrollNavigation: "off",
                        onHoverStop: "off",
                        touch: {
                            touchenabled: "on",
                            swipe_threshold: 75,
                            swipe_min_touches: 1,
                            swipe_direction: "horizontal",
                            drag_block_vertical: false
                        },
                        arrows: {
                            style: "gyges",
                            enable: true,
                            hide_onmobile: false,
                            hide_onleave: true,
                            tmp: '',
                            left: {
                                h_align: "left",
                                v_align: "center",
                                h_offset: 10,
                                v_offset: 0
                            },
                            right: {
                                h_align: "right",
                                v_align: "center",
                                h_offset: 10,
                                v_offset: 0
                            }
                        }
                    },
                    responsiveLevels:[1240,1024,778,480],
                    gridwidth:[1240,1024,778,480],
                    gridheight:[700,600,500,500],
                    disableProgressBar:"on",
                    parallax: {
                        type:"mouse",
                        origo:"slidercenter",
                        speed:2000,
                        levels:[2,3,4,5,6,7,12,16,10,50],
                    }

                });
            });
        </script>


        <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems! The following part can be removed on Server for On Demand Loading) -->
         
        <script type="text/javascript" src="<?php echo base_url();?>assets_new/revolution/js/extensions/revolution.extension.video.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>assets_new/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>assets_new/revolution/js/extensions/revolution.extension.actions.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>assets_new/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>assets_new/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>assets_new/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>assets_new/revolution/js/extensions/revolution.extension.migration.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>assets_new/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
        
    </body>
  
</html>