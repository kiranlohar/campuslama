<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="materialize is a material design based mutipurpose responsive template">
        <meta name="keywords" content="material design, card style, material template, portfolio, corporate, business, creative, agency">
        <meta name="author" content="trendytheme.net">

        <title>Hello Admission</title>

        <!--  favicon -->
       <link rel="shortcut icon" href="<?php echo base_url('assets/img/fav.png' ); ?>">
        <!--  apple-touch-icon -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url()?>assets_new/img/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url()?>assets_new/img/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url()?>assets_new/img/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="<?php echo base_url()?>assets_new/img/ico/apple-touch-icon-57-precomposed.png">


        
        <link href='https://fonts.googleapis.com/css?family=Raleway:400,300,500,700,900' rel='stylesheet' type='text/css'>
        <!-- Material Icons CSS -->
        <link href="<?php echo base_url()?>assets_new/fonts/iconfont/material-icons.css" rel="stylesheet">
        <!-- FontAwesome CSS -->
        <link href="<?php echo base_url()?>assets_new/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- materialize -->
         <link href="<?php echo base_url()?>assets_new/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url()?>assets_new/materialize/css/materialize.min.css" rel="stylesheet">
        <!-- Bootstrap -->
        <link href="<?php echo base_url()?>assets_new/css/shortcodes/header.css" rel="stylesheet">
        <!-- shortcodes -->
        <link href="<?php echo base_url()?>assets_new/css/shortcodes/shortcodes.css" rel="stylesheet">
        
        <link href="<?php echo base_url()?>assets_new/css/shortcodes/login.css" rel="stylesheet">
        <!-- Style CSS -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/1.3.1/css/toastr.css" rel="stylesheet">
        <link href="<?php echo base_url()?>assets_new/style.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <![endif]-->
        <script type="text/javascript" src="<?php echo base_url('assets/explore/js/ion.rangeSlider.js');?>"></script>
        
        <script type="text/javascript" src="<?php echo base_url('assets/explore/js/frontend/mobile_detect.js');?>"></script>
        </head>
        <style type="text/css">
          .btn {
    height: 50px;
    padding-top: -4px;
    font-size: 16px;
    line-height: 26px;
    border-radius: 2px;
    background-color: #03a9f4 !important;
    border: 0;
}
.nopaddingleft {
    padding-left: 0;
}
.nopaddingright {
    padding-right: 0;
}
.overflownone {
    overflow: hidden!important;
}
.information {
    
    position: relative;
    height: 400px;
}
.information h3 {
    font-size: 34px;
    font-size: 3.4rem;
    font-family: "roboto", sans-serif;
    font-weight: 500;
    color: #FFF;
    padding: 0 15px;
}
.information .information_menu {
    padding: 0;
}
.information .information_menu li {
    list-style: none;
    position: relative;
}
.information .information_menu li.active {
    background: #0383c5;
}
.information .information_menu li.active:after {
   
    width: 0;
    height: 0;
    border-top: 27px solid transparent;
    border-bottom: 27px solid transparent;
    border-left: 22px solid #0383c5;
    position: absolute;
    right: -22px;
    z-index: 1000;
    top: 0;
}
.information .information_menu li a {
    color: #FFF;
    display: block;
    padding: 16px 20px;
    font-size: 15px;
    font-size: 1.5rem;
    text-decoration: none;
}
.information .btn-emergency {
    background: #f89406;
    border: none;
    margin: 20px;
    display: block;
    width: 88%;
    color: #fff;
    text-shadow: none;
    padding: 0;
    font-size: 20px;
    font-size: 2rem;
}
.information .btn-emergency span {
    padding: 15px;
    display: block;
    background-size: 30px 30px;
    background-image: -webkit-gradient(linear, left top, right bottom, color-stop(0.25, rgba(255, 255, 255, .15)), color-stop(0.25, transparent), color-stop(0.5, transparent), color-stop(0.5, rgba(255, 255, 255, .15)), color-stop(0.75, rgba(255, 255, 255, .15)), color-stop(0.75, transparent), to(transparent));
    background-image: -webkit-linear-gradient(135deg, rgba(255, 255, 255, .15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .15) 50%, rgba(255, 255, 255, .15) 75%, transparent 75%, transparent);
    background-image: -o-linear-gradient(135deg, rgba(255, 255, 255, .15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .15) 50%, rgba(255, 255, 255, .15) 75%, transparent 75%, transparent);
    background-image: linear, 135deg, rgba(255, 255, 255, .15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .15) 50%, rgba(255, 255, 255, .15) 75%, transparent 75%, transparent;
    -webkit-animation: animate-stripes 3s linear infinite;
    -moz-animation: animate-stripes 3s linear infinite;
}
.mobile-menu-bg {
    background: #2a3d46;
}
.information_menu_mobile {
    margin: 0px;
    padding: 0;
}
.information_menu_mobile li {
    padding: 0;
    float: left;
    width: 25%;
    list-style: none;
    text-align: center;
}
.information_menu_mobile li a {
    display: block;
    padding: 10px;
    color: #FFF;
    text-decaration: none;
}
.information_menu_mobile li:hover a {
    background: #0383c5;
    text-decaration: none;
}
.slideshow {
    overflow: hidden;
}
.slideshow .overlay-item  {
    position: absolute;
    top: 0;
    z-index: 999;
    width: 90%;
    background: rgba(158, 158, 158, 0.15);
    height: 400px;
    padding: 10px 30px;
    display: none;
    -webkit-animation: opendoor 0.75s ease 0s alternate;
    -webkit-transform-style: preserve-3d;
            transform-style: preserve-3d;
}
.slideshow .overlay-item.active {
    display: block;
    -webkit-animation: opendoor 0.75s ease 0s alternate;
    -webkit-transform-style: preserve-3d;
            transform-style: preserve-3d;
}
.slideshow .overlay-item.inactive {
  display: block;
  -webkit-animation: closedoor 0.75s ease 0s alternate;
  transform-style: preserve-3d;
  opacity:0;
}
.slideshow .overlay-item .quickmenu  {
    padding: 0;
}
.slideshow .overlay-item .quickmenu li {
    list-style: none;
}
.slideshow .overlay-item .quickmenu li a {
    color: #FFF;
    display: block;
    padding: 8px 20px;
    font-size: 15px;
    font-size: 1.5rem;
    text-decoration: none;
}
#slider {
    padding: 0px;
    margin: 0px
}
#slider li {
    padding: 0px;
    list-style: none;
    position: absolute;
}
#carousel-example-generic {
    height: 400px
}
.carousel-fade .carousel-inner .item1 {
    -webkit-transition-property: opacity;
    transition-property: opacity;
}
.carousel-fade .carousel-inner .item1,
.carousel-fade .carousel-inner .active.left,
.carousel-fade .carousel-inner .active.right {
    opacity: 0;
    filter: alpha(opacity=0);
}
.carousel-fade .carousel-inner .active,
.carousel-fade .carousel-inner .next.left,
.carousel-fade .carousel-inner .prev.right {
    opacity: 1;
    filter: alpha(opacity=100);
}
.carousel-fade .carousel-inner .next,
.carousel-fade .carousel-inner .prev,
.carousel-fade .carousel-inner .active.left,
.carousel-fade .carousel-inner .active.right {
    left: 0;
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
}
.carousel-fade .carousel-control {
    z-index: 2;
}
html,
body,
.carousel,
.carousel-inner,
.carousel-inner .item1 {
    height: 100%;
}
.item1:nth-child(1) {
    background: #74C390;
}
.item1:nth-child(2) {
    background: #51BCE8;
}
.item1:nth-child(3) {
    background: #E46653;
}
@-webkit-keyframes animate-stripes {
    0% {
        background-position: 0 0;
    }
    100% {
        background-position: 60px 0;
    }
}
@-webkit-keyframes in {
    from {
        -webkit-transform: scale(1.3);
    }
    to {
        -webkit-transform: scale(1);
    }
}
@-webkit-keyframes out {
    0% {
        -webkit-transform: scale(1);
    }
    100% {
        -webkit-transform: scale(1.3);
    }
}
@-webkit-keyframes opendoor {
    from {
        -webkit-transform: perspective(1000px) rotateY(90deg);
        -webkit-transform-origin: 0% 50%;
    }
    to {
        -webkit-transform: perspective(1000px) rotateY(0deg);
        -webkit-transform-origin: 0% 50%;
    }
}
@-webkit-keyframes closedoor {
    from {
        opacity:1;
         -webkit-transform-origin: 0% 50%;
    }
    to {
        opacity:0;
        -webkit-transform-origin: 0% 50%;
    }
}
        </style>

    <body id="top" class="has-header-search">

         <?php 
          $current_url = current_url();
    $userdata = $this->session->userdata('logged_in');
                //print_r($userdata);
    ?>
        <!--header start-->
        <?php
            if(!empty($userdata)){
            $verify = $this->db->get_where('users',array('id'=>$userdata['id']));
             $emailverify=$verify->result();
           // echo $emailverify[0]->email_confirmed;
            if($emailverify[0]->email_confirmed=='NO'){
        ?>
        <header id="error_msg" class="second"  style="background-color:#e64848;height: 50px;color: white;padding-top: 10px;font-weight:  bold;text-align:  center;">
            Please Verify Your Email Address.     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <a href="#" onclick="verify_email(<?php echo $userdata['id'] ?>)" style="padding: -1px;height: 27px;padding-top: 1px;margin-top: -4px;" class="btn btn-sm text-capitalize waves-effect waves-light"><b>Verify</b></a>
        </header>
        <?php } } ?>
        <!--header start-->

        <header id="header" class="tt-nav transparent-header">

            <div class="header-sticky light-header">

                <div class="container">

                    <div class="search-wrapper">
                        <div class="search-trigger pull-right">
                            <div class='search-btn'></div>
                            <i class="material-icons">&#xE8B6;</i>
                        </div>

                        <!-- Modal Search Form -->
                        <i class="search-close material-icons">&#xE5CD;</i>
                        <div class="search-form-wrapper">
                            <form action="#" class="white-form">
                                <div class="input-field">
                                    <input type="text" name="search" id="search">
                                    <label for="search" class="">Search Here...</label>
                                </div>
                                <button class="btn pink search-button waves-effect waves-light" type="submit"><i class="material-icons">&#xE8B6;</i></button>
                                
                            </form>
                        </div>
                    </div><!-- /.search-wrapper -->
                
                    <div id="materialize-menu" class="menuzord">

                        <!--logo start-->
                        <a href="index.html" class="logo-brand">
                            <img src="assets/img/logo.png" alt=""/>
                        </a>
                        <!--logo end-->

                        <!--mega menu start-->
                        <ul class="menuzord-menu pull-right">
                            <li class="active"><a href="javascript:void(0)">Home</a>
                                <div class="megamenu">
                                    <div class="megamenu-row">
                                        <div class="col3">
                                            <h2>Default Demos</h2>
                                            <ul class="list-unstyled clearfix">
                                                <li class="active"><a href="index.html">Home Default</a></li>
                                                <li><a href="index-2.html">Home Canvas</a></li>
                                                <li><a href="index-3.html">Home Elegent</a></li>
                                                <li><a href="index-4.html">Home Startup</a></li>
                                                <li><a href="index-5-consulting.html">Home Consulting</a></li>
                                                <li><a href="op-index-coffeeshop.html" target="_blank">Home Coffee Shop</a></li>
                                            </ul>
                                        </div>
                                        <div class="col3">
                                            <h2>Complete Demos</h2>
                                            <ul class="list-unstyled clearfix">
                                                <li><a href="creative-index.html" target="_blank">Creative Demo</a></li>
                                                <li><a href="corporate-index.html" target="_blank">Corporate Demo</a></li>
                                                <li><a href="agency-index.html" target="_blank">Agency Demo</a></li>
                                                <li><a href="construction-index.html" target="_blank">Construction Demo</a></li>
                                                <li><a href="seo-index.html" target="_blank">SEO Demo</a></li>
                                                <li><a href="restaurant-index.html" target="_blank">Restaurant Demo</a></li>
                                            </ul>
                                        </div>
                                        <div class="col3">
                                            <h2>Onepage Demo</h2>
                                            <ul class="list-unstyled clearfix">
                                                <li><a href="op-index-1.html" target="_blank">materialize Onepage</a></li>
                                                <li><a href="op-index-portfolio.html" target="_blank">Onepage Portfoilo</a></li>
                                                <li><a href="op-index-event.html" target="_blank">Onepage Event</a></li>
                                                <li><a href="op-index-book.html" target="_blank">Book Landing Page</a></li>
                                                <li><a href="op-index-app.html" target="_blank">App Landing Page 1</a></li>
                                                <li><a href="op-index-app-2.html" target="_blank">App Landing Page 2</a></li>
                                            </ul>
                                        </div>
                                        <div class="col3">
                                            <h2>Specialized Home</h2>
                                            <ul class="list-unstyled clearfix">
                                                <li><a href="index-8-portfolio.html">Home Portfolio</a></li>
                                                <li><a href="index-7-blog.html">Home Blog</a></li>
                                                <li><a href="index-6-charity.html">Home Charity</a></li>
                                                <li><a href="coming-soon-1.html" target="_blank">Coming Soon One</a></li>
                                                <li><a href="coming-soon-2.html" target="_blank">Coming Soon Two</a></li>
                                            </ul>
                                        </div>

                                    </div>
                                </div>
                            </li>

                            <li><a href="javascript:void(0)">Pages</a>
                                <div class="megamenu">
                                    <div class="megamenu-row">
                                        <div class="col3">
                                            <h2>Menu Type Page</h2>
                                            <ul class="list-unstyled clearfix">
                                                <li><a href="about-us.html">About Us 1</a></li>
                                                <li><a href="about-us-2.html">About Us 2</a></li>
                                                <li><a href="about-us-3.html">About Us 3</a></li>
                                                <li><a href="service.html">Services 1</a></li>
                                                <li><a href="service-2.html">Services 2</a></li>
                                            </ul>
                                        </div>
                                        <div class="col3">
                                            <h2>Menu Type Page</h2>
                                            <ul class="list-unstyled clearfix">
                                                <li><a href="team-1.html">Our Team 1</a></li>
                                                <li><a href="team-2.html">Our Team 2</a></li>
                                                <li><a href="faq.html">FAQ Page 1</a></li>
                                                <li><a href="faq-2.html">FAQ Page 2</a></li>
                                                <li><a href="support.html">Support Page</a></li>
                                            </ul>
                                        </div>
                                        <div class="col3">
                                            <h2>Menu Type Page</h2>
                                            <ul class="list-unstyled clearfix">
                                                <li><a href="clients.html">Our Clients</a></li>
                                                <li><a href="contact-us.html">Contact 1</a></li>
                                                <li><a href="contact-us-2.html">Contact 2</a></li>
                                                <li><a href="404.html">Error 404</a></li>
                                                <li><a href="404-alt.html">Error 404 Alt</a></li>
                                            </ul>
                                        </div>
                                        <div class="col3">
                                            <h2>Menu Type Page</h2>
                                            <ul class="list-unstyled clearfix">
                                                <li><a href="typography.html">Typography</a></li>
                                                <li><a href="login-form.html">Login/Register 1</a></li>
                                                <li><a href="login-form-2.html" target="_blank">Login/Register 2</a></li>
                                            </ul>
                                        </div>

                                    </div>
                                </div>
                            </li>

                            <li><a href="javascript:void(0)">Features</a>
                                <ul class="dropdown">
                                    <li><a href="#">Sliders</a>
                                        <ul class="dropdown">
                                            <li><a href="#">Slider Revolution</a>
                                                <ul class="dropdown">
                                                    <li><a href="slider-revoulation-fullscreen.html">Full Screen</a></li>
                                                    <li><a href="slider-revoulation-fullwidth.html">Full Width</a></li>
                                                    <li><a href="slider-revoulation-kenburns.html">Kenburns</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#">Parallax</a>
                                                <ul class="dropdown">
                                                    <li><a href="slider-revoulation-parallax-1.html">Parallax Slider</a></li>
                                                    <li><a href="slider-revoulation-parallax-book.html">Parallax Book</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#">Slider Rotate Text</a>
                                                <ul class="dropdown">
                                                    <li><a href="text-rotator-1.html">Rotate Text 1</a></li>
                                                    <li><a href="text-rotator-2.html">Rotate Text 2</a></li>
                                                    <li><a href="text-rotator-3.html">Rotate Text 3</a></li>
                                                    <li><a href="text-rotator-4.html">Rotate Text 4</a></li>
                                                    <li><a href="text-rotator-5.html">Rotate Text 5</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#">Hero Unit</a>
                                                <ul class="dropdown">
                                                    <li><a href="hero-static.html">Hero Static</a></li>
                                                    <li><a href="hero-static-left.html">Hero Static Left</a></li>
                                                    <li><a href="hero-static-right.html">Hero Static Right</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="hero-cloud.html">Animate Cloud</a></li>
                                            <li><a href="hero-html-video.html">HTML5 Video</a> </li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Menu Style Group 1</a>
                                        <ul class="dropdown">
                                            <li><a href="menu-transparent.html">Transparent</a></li>
                                            <li><a href="menu-semi-transparent.html">Semi Transparent</a></li>
                                            <li><a href="menu-center.html">Menu Center</a></li>
                                            <li><a href="menu-bottom.html">Bottom Position</a></li>
                                            <li><a href="menu-floating.html">Floating Menu</a></li>
                                            <li><a href="agency-index.html" target="_blank">Full Screen Menu</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Menu Style Group 2</a>
                                        <ul class="dropdown">
                                            <li><a href="menu-light.html">Menu Light</a></li>
                                            <li><a href="menu-topbar.html">Menu Topbar</a></li>
                                            <li><a href="menu-box.html">Menu Box</a></li>
                                            <li><a href="menu-border-box.html">Menu Border Box</a></li>
                                            <li><a href="menu-dark.html">Menu Dark</a></li>
                                            <li><a href="menu-topbar-alt.html">Menu Brand Color</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Page Title</a>
                                        <ul class="dropdown">
                                            <li><a href="page-title.html"> Default Title</a></li>
                                            <li><a href="page-title-bg.html"> Background Title</a></li>
                                            <li><a href="page-title-fixed-bg.html"> Background Fixed</a></li>
                                            <li><a href="page-title-center.html"> Center Title</a></li>
                                            <li><a href="page-title-dark.html"> Dark Title</a></li>
                                            <li><a href="page-title-parallax-bg.html"> Parallax Title</a></li>
                                            <li><a href="page-title-no-bg.html"> No Background</a></li>
                                            <li><a href="page-title-pattern-bg.html"> Pattern Title</a></li>
                                            <li><a href="page-title-right.html"> Right Title</a></li>
                                            <li><a href="page-title-mini.html"> Mini Title</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Footer</a>
                                        <ul class="dropdown">
                                            <li><a href="footer-1.html">Footer Style 1</a></li>
                                            <li><a href="footer-2.html">Footer Style 2</a></li>
                                            <li><a href="footer-3.html">Footer Style 3</a></li>
                                            <li><a href="footer-4.html">Footer Style 4</a></li>
                                            <li><a href="footer-5.html">Footer Style 5</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>

                            <li><a href="javascript:void(0)">Portfolio</a>
                                <ul class="dropdown">
                                    <li><a href="#">Boxed Style</a>
                                        <ul class="dropdown">
                                            <li><a href="#">Boxed Gutter Style</a>
                                                <ul class="dropdown">
                                                    <li><a href="portfolio-4.html">4 Column</a></li>
                                                    <li><a href="portfolio-3.html">3 Column</a></li>
                                                    <li><a href="portfolio-2.html">2 Column</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#">Boxed Gutter Less</a>
                                                <ul class="dropdown">
                                                    <li><a href="portfolio-4-gutterless.html">4 Column</a></li>
                                                    <li><a href="portfolio-3-gutterless.html">3 Column</a></li>
                                                    <li><a href="portfolio-2-gutterless.html">2 Column</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Wide Style</a>
                                        <ul class="dropdown">
                                            <li><a href="#">Wide Gutter Style</a>
                                                <ul class="dropdown">
                                                    <li><a href="portfolio-6-fullwidth.html">6 Column</a></li>
                                                    <li><a href="portfolio-5-fullwidth.html">5 Column</a></li>
                                                    <li><a href="portfolio-4-fullwidth.html">4 Column</a></li>
                                                    <li><a href="portfolio-3-fullwidth.html">3 Column</a></li>
                                                    <li><a href="portfolio-2-fullwidth.html">2 Column</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#">Wide Gutter Less</a>
                                                <ul class="dropdown">
                                                    <li><a href="portfolio-6-fullwidth-gutterless.html">6 Column</a></li>
                                                    <li><a href="portfolio-5-fullwidth-gutterless.html">5 Column</a></li>
                                                    <li><a href="portfolio-4-fullwidth-gutterless.html">4 Column</a></li>
                                                    <li><a href="portfolio-3-fullwidth-gutterless.html">3 Column</a></li>
                                                    <li><a href="portfolio-2-fullwidth-gutterless.html">2 Column</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Card Style</a>
                                        <ul class="dropdown">
                                            <li><a href="#">Card Boxed Style</a>
                                                <ul class="dropdown">
                                                    <li><a href="portfolio-card-4.html">4 Column</a></li>
                                                    <li><a href="portfolio-card-3.html">3 Column</a></li>
                                                    <li><a href="portfolio-card-2.html">2 Column</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#">Card Full Width</a>
                                                <ul class="dropdown">
                                                    <li><a href="portfolio-card-6-fullwidth.html">6 Column</a></li>
                                                    <li><a href="portfolio-card-5-fullwidth.html">5 Column</a></li>
                                                    <li><a href="portfolio-card-4-fullwidth.html">4 Column</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Title Style</a>
                                        <ul class="dropdown">
                                            <li><a href="#">Title Boxed Style</a>
                                                <ul class="dropdown">
                                                    <li><a href="portfolio-title-4.html">4 Column</a></li>
                                                    <li><a href="portfolio-title-3.html">3 Column</a></li>
                                                    <li><a href="portfolio-title-2.html">2 Column</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#">Title Width Style</a>
                                                <ul class="dropdown">
                                                    <li><a href="portfolio-title-5-fullwidth.html">5 Column</a></li>
                                                    <li><a href="portfolio-title-4-fullwidth.html">4 Column</a></li>
                                                    <li><a href="portfolio-title-3-fullwidth.html">3 Column</a></li>
                                                    <li><a href="portfolio-title-2-fullwidth.html">2 Column</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Masonry Style</a>
                                        <ul class="dropdown">
                                            <li><a href="#">Masonry Boxed Style</a>
                                                <ul class="dropdown">
                                                    <li><a href="portfolio-masonry-3.html">3 Column</a></li>
                                                    <li><a href="portfolio-masonry-2.html">2 Column</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#">Masonry Full Width</a>
                                                <ul class="dropdown">
                                                    <li><a href="portfolio-masonry-3-fullwidth.html">3 Column</a></li>
                                                    <li><a href="portfolio-masonry-2-fullwidth.html">2 Column</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#">Masonry Gutter Less</a>
                                                <ul class="dropdown">
                                                    <li><a href="portfolio-masonry-3-gutterless.html">3 Column</a></li>
                                                    <li><a href="portfolio-masonry-2-gutterless.html">2 Column</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#">Masonry Gutterless Full</a>
                                                <ul class="dropdown">
                                                    <li><a href="portfolio-masonry-3-gutterless-full.html">3 Column</a></li>
                                                    <li><a href="portfolio-masonry-2-gutterless-full.html">2 Column</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a href="portfolio-details-gallery.html">Portfolio Details Gallery</a></li>
                                    <li><a href="portfolio-details-2.html">Portfolio Details Image</a></li>
                                </ul>
                            </li>

                            <li><a href="javascript:void(0)">Blog</a>
                                <ul class="dropdown">
                                    <li><a href="#">Standard</a>
                                        <ul class="dropdown ">
                                            <li><a href="blog-sidebar-right.html">Sidebar Right</a></li>
                                            <li><a href="blog-sidebar-left.html">Sidebar Left</a></li>
                                            <li><a href="blog-fullwidth.html">Fullwidth</a></li>
                                        </ul>
                                    </li>

                                    <li><a href="#">Masonry</a>
                                        <ul class="dropdown ">
                                            <li><a href="blog-grid-masonry-2.html">Masonry 2 Column</a></li>
                                            <li><a href="blog-grid-masonry-2-sidebar.html">Masonry 2 Sidebar</a></li>
                                            <li><a href="blog-grid-masonry-3.html">Masonry 3 Column</a></li>
                                            <li><a href="blog-grid-masonry-4.html">Masonry 4 Column</a></li>
                                            <li><a href="blog-grid-masonry-4-fullwidth.html">Masonry Fullwidth</a></li>
                                        </ul>
                                    </li>

                                    <li><a href="#">Card Style</a>
                                        <ul class="dropdown ">
                                            <li><a href="blog-card-2.html">Card 2 Column</a></li>
                                            <li><a href="blog-card-3.html">Card 3 Column</a></li>
                                            <li><a href="blog-card-4.html">Card 4 Column</a></li>
                                        </ul>
                                    </li>

                                    <li><a href="#">Grid</a>
                                        <ul class="dropdown ">
                                            <li><a href="blog-grid.html">Blog Grid Column</a></li>
                                            <li><a href="blog-grid-right-sidebar.html">Grid Right Sidebar</a></li>
                                            <li><a href="blog-grid-left-sidebar.html">Grid Left Sidebar</a></li>
                                        </ul>
                                    </li>

                                    <li><a href="#">Blog Details</a>
                                        <ul class="dropdown ">
                                            <li><a href="blog-single-sidebar-right.html">Sidebar Right</a></li>
                                            <li><a href="blog-single-sidebar-left.html">Sidebar Left</a></li>
                                            <li><a href="blog-single-fullwidth.html">Fullwidth</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>

                            <li><a href="javascript:void(0)">Shortcodes</a>
                                <div class="megamenu">
                                    <div class="megamenu-row">
                                        <div class="col3">
                                            <h2>Shortcode List One</h2>
                                            <ul class="list-unstyled clearfix">
                                                <li><a href="shortcode-accordion.html">Accordion</a></li>
                                                <li><a href="shortcode-alert.html">Alert</a></li>
                                                <li><a href="shortcode-blog.html">Blog</a></li>
                                                <li><a href="shortcode-button.html">Button</a></li>
                                                <li><a href="shortcode-clients.html">Clients</a></li>
                                            </ul>
                                        </div>
                                        <div class="col3">
                                            <h2>Shortcode List Two</h2>
                                            <ul class="list-unstyled clearfix">
                                                <li><a href="shortcode-featured-box.html">Featured Box</a></li>
                                                <li><a href="shortcode-fun-facts.html">Fun Facts</a></li>
                                                <li><a href="shortcode-gallery.html">Gallery</a></li>
                                                <li><a href="shortcode-hero-block.html">Hero Block</a></li>
                                                <li><a href="shortcode-progressbar.html">Progress Bar</a></li>
                                            </ul>
                                        </div>
                                        <div class="col3">
                                            <h2>Shortcode List Three</h2>
                                            <ul class="list-unstyled clearfix">
                                                <li><a href="shortcode-promo-box.html">Promo Block</a></li>
                                                <li><a href="shortcode-tab.html">Tab</a></li>
                                                <li><a href="shortcode-table.html">Table </a></li>
                                                <li><a href="shortcode-team.html">Team </a></li>
                                                <li><a href="shortcode-testimonial.html">Testimonial </a></li>
                                            </ul>
                                        </div>
                                        <div class="col3">
                                            <h2>Hero Text Rotate</h2>
                                            <ul class="list-unstyled clearfix">
                                                <li><a href="text-rotator-1.html">Rotate Text 1</a></li>
                                                <li><a href="text-rotator-2.html">Rotate Text 2</a></li>
                                                <li><a href="text-rotator-3.html">Rotate Text 3</a></li>
                                                <li><a href="text-rotator-4.html">Rotate Text 4</a></li>
                                                <li><a href="text-rotator-5.html">Rotate Text 5</a></li>
                                            </ul>
                                        </div>

                                    </div>
                                </div>
                            </li>

                            <li><a href="javascript:void(0)">Contact</a>
                                <ul class="dropdown">
                                    <li><a href="contact-us.html">Contact One</a></li>
                                    <li><a href="contact-us-2.html">Contact Two</a></li>
                                </ul>
                            </li>
                        </ul>
                        <!--mega menu end-->

                    </div>
                </div>
            </div>
        </header>
       
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/1.3.1/js/toastr.js"></script>

      <script type="text/javascript">
         function verify_email(id)
                 {
                     // toastr['success']('Mail has been send on your register email address..').css("width","500px");;

                 
                     var email="<?php echo trim($userdata['email']);?>";
                     var name="<?php echo trim($userdata['name']);?>";
                     var url="<?php echo $current_url; ?>";
                     var dataString={id:id,email:email,name:name,url:url};
                    $.ajax({
                                 type:'POST',
                                 url:'<?php echo base_url(); ?>student/email_verify',
                                 data:dataString,
                                 success:function(res)
                                 {
                                    var obj = JSON.parse(res);
                                    console.log(obj);
                                     if(obj.msg=='Please check your email at to verify the email address')
                                      { 
                                       toastr['success']('Mail has been send on your register email address..').css("width","500px").options = {
                                          "closeButton": false,
                                          "debug": false,
                                          "newestOnTop": true,
                                          "progressBar": false,
                                          "positionClass": "toast-top-right",
                                          "preventDuplicates": false,
                                          "onclick": null,
                                          "showDuration": "300",
                                          "hideDuration": "1000",
                                          "timeOut": "5000",
                                          "extendedTimeOut": "1000",
                                          "showEasing": "swing",
                                          "hideEasing": "linear",
                                          "showMethod": "fadeIn",
                                          "hideMethod": "fadeOut"
                                        }
                                        $('#error_msg').hide('slow');
                                     }
                                    
                    //                      setTimeout(function(){
                    //  location.reload();
                    // }, 4000);
                                      
                                 }
                             });
                             
                 }
        </script>
      

 
  </script>
       
  <script type="text/javascript">
 
   $(document).ready(function() {
    $('.information_menu').find('li').hover(function(e) {
         $('.information_menu').find('li').removeClass('active');
        $(this).addClass('active');
        $(".overlay-item").removeClass("active");
        $(".overlay-item").removeClass("inactive");
    $(".overlay-id"+$(this).data("id")).addClass("active").removeClass("inactive");
        
         $(".overlay-id"+$(this).data("id")).prev().addClass("inactive")
    });     
  
    $('.slideshow').children().on('mouseleave',function(e) {
    $(this).removeClass("active");
  });    
    
    $('.carousel').carousel();
   });


 
  </script>