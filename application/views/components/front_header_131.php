<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="materialize is a material design based mutipurpose responsive template">
        <meta name="keywords" content="material design, card style, material template, portfolio, corporate, business, creative, agency">
        <meta name="author" content="trendytheme.net">

        <title>Hello Admission</title>
         <link rel="shortcut icon" href="<?php echo base_url('assets/images/favicon/favicon-32x32.png'); ?>">
        <!--  apple-touch-icon -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url()?>assets_new/img/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url()?>assets_new/img/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url()?>assets_new/img/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="<?php echo base_url()?>assets_new/img/ico/apple-touch-icon-57-precomposed.png">


        
        <link href='https://fonts.googleapis.com/css?family=Raleway:400,300,500,700,900' rel='stylesheet' type='text/css'>
        <!-- Material Icons CSS -->
        <link href="<?php echo base_url()?>assets_new_2/fonts/iconfont/material-icons.css" rel="stylesheet">
        <!-- FontAwesome CSS -->
        <link href="<?php echo base_url()?>assets_new_2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- materialize -->
        <link href="<?php echo base_url()?>assets_new_2/materialize/css/materialize.min.css" rel="stylesheet">
        <!-- Bootstrap -->
        <link href="<?php echo base_url()?>assets_new_2/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- shortcodes -->
        <link href="<?php echo base_url('assets/explore/css/frontend/quiz_mycustom.css');?>" type="text/css" rel="stylesheet">
     
        <link href="<?php echo base_url('assets/explore/css/frontend/quiz_mycustom_media.css');?>" type="text/css" rel="stylesheet">
       <link href="<?php echo base_url()?>assets_new_2/css/shortcodes/shortcodes.css" rel="stylesheet">
        <link href="<?php echo base_url()?>assets_new_2/css/shortcodes/login.css" rel="stylesheet">
        
           <link rel="stylesheet" href="<?php echo base_url('assets/css/matexcss/main.css');?>">
            <link href="<?php echo base_url()?>assets_new_2/style.css" rel="stylesheet">


        <link href="<?php echo base_url('assets/explore/css/animate.css');?>" type="text/css" rel="stylesheet">
        
        <link href="<?php echo base_url('assets/explore/css/ion.rangeSlider.css');?>" type="text/css" rel="stylesheet"/>
        <link href="<?php echo base_url('assets/explore/css/ion.rangeSlider.skinHTML5.css');?>" type="text/css" rel="stylesheet"/>
        
        <link href="<?php echo base_url('assets/explore/css/frontend/quiz_mycustom.css');?>" type="text/css" rel="stylesheet">
        <link href="<?php echo base_url('assets/explore/css/frontend/quiz_mycustom_media.css');?>" type="text/css" rel="stylesheet">
         <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css" type="text/css" rel="stylesheet">
        
       </head>
        
         <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

       

        <script type="text/javascript" src="<?php echo base_url('assets/explore/js/ion.rangeSlider.js');?>"></script>
        
        <script type="text/javascript" src="<?php echo base_url('assets/explore/js/frontend/mobile_detect.js');?>"></script>
        <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/1.3.1/js/toastr.js"></script>
    
<style type="text/css">

        /*  @import url('//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');*/
/*.menuzord-menu>li.active:hover .megamenu .dropdown-menu {
     display: block;
     right: 0px;    
}*/
.menuzord-menu>li>.megamenu .dropdown-menu{
display: block;
}


/*!
 * Line - Bootstrap Ecommerce Mega Menu - v1.0 (http://html.codbits.com/line)
 * Copyright CodBits (http://codbits.com)
 * Licensed under CodeCanyon (http://codecanyon.net/licenses)
 * 
 * Notes:
 * It is strongly recommended to use LESS for CSS customizations
 * lno is abbreviation of line navbar one
 * lnt is abbreviation of line navbar two
 * lnl is abbreviation of line navbar left
 */
/* Global
/ -------------------------------------------------- */
body {
  //background-color: #F7F7F7;
}
/* Line navbar one (lno)
/ -------------------------------------------------- */
.line-navbar-one {
  background-color: #FFF;
  border-top: 4px solid #fb4e4e;
  border-bottom: 1px solid #eeeeee;
  margin-bottom: 0;
  z-index: 1001;
  min-height: 95px;
}
.line-navbar-one .navbar-toggle {
  border: 2px solid #333333;
  border-radius: 0;
  margin-top: 23px;
  line-height: 21px;
  width: 45px;
  height: 45px;
  font-size: 20px;
}
.line-navbar-one .lno-cart {
  position: relative;
  overflow: hidden;
  float: right;
  border: 2px solid #333;
  margin-top: 23px;
  margin-right: 6px;
  line-height: 41px;
  width: 45px;
  height: 45px;
  font-size: 16px;
  color: #333;
  text-align: center;
  font-weight: bold;
}
@media (min-width: 768px) {
  .line-navbar-one .lno-cart {
    display: none;
  }
}
.line-navbar-one .lno-cart .item-added {
  position: absolute;
  opacity: .03;
  font-size: 45px;
  left: 7px;
  top: 2px;
  transform: rotate(-35deg);
  -webkit-transition: all 300ms;
  -o-transition: all 300ms;
  transition: all 300ms;
}
.line-navbar-one .lno-cart:hover,
.line-navbar-one .lno-cart:focus {
  text-decoration: none;
}
.line-navbar-one .lno-btn-toggle {
  position: absolute;
  left: 4px;
  padding: 0px 7px;
  top: 24px;
  font-size: 21px;
  background-color: #FFF;
  border-width: 0;
  color: #333;
  outline: 0;
  display: none;
  border-right: 2px solid #333;
  width: 45px;
  height: 40px;
}
@media (max-width: 767px) {
  .line-navbar-one .lno-btn-toggle {
    display: block;
  }
}
.line-navbar-one .navbar-brand {
  padding-top: 24px;
  padding-bottom: 24px;
}
@media (max-width: 767px) {
  .line-navbar-one .navbar-brand {
    padding-left: 60px;
  }
}
.line-navbar-one .nav > li > a {
  font-weight: bold;
  color: #333333;
  padding-top: 13px;
  padding-bottom: 13px;
  padding: 15px 17px;
}
@media (min-width: 768px) {
  .line-navbar-one .nav > li > a {
    padding: 35px 20px;
  }
}
@media (min-width: 768px) {
  .line-navbar-one .nav > li.active > a,
  .line-navbar-one .nav > li:hover > a,
  .line-navbar-one .nav > li:focus > a {
    background-color: #f8f8f8;
  }
}
@media (min-width: 768px) {
  .line-navbar-one .nav .open > a,
  .line-navbar-one .nav .open > a:hover,
  .line-navbar-one .nav .open > a:focus {
    background-color: #f8f8f8;
  }
}
.line-navbar-one .navbar-right {
  margin-right: 0;
}
.line-navbar-one .open .dropdown-menu {
  border-radius: 0;
  padding: 0;
}
.line-navbar-one .open .dropdown-menu li a {
  padding: 10px 20px;
  border-bottom: 1px solid #eeeeee;
}
@media (min-width: 768px) {
  .line-navbar-one .open .dropdown-menu:before {
    position: absolute;
    top: -9px;
    left: 9px;
    display: inline-block;
    border-right: 9px solid rgba(0, 0, 0, 0);
    border-bottom: 9px solid #CCC;
    border-left: 9px solid rgba(0, 0, 0, 0);
    border-bottom-color: rgba(0, 0, 0, 0.2);
    content: '';
  }
  .line-navbar-one .open .dropdown-menu:after {
    position: absolute;
    top: -8px;
    left: 10px;
    display: inline-block;
    border-right: 8px solid rgba(0, 0, 0, 0);
    border-bottom: 8px solid #FFF;
    border-left: 8px solid rgba(0, 0, 0, 0);
    content: '';
  }
}
@media (max-width: 767px) {
  .line-navbar-one .navbar-collapse {
    margin-top: 7px;
  }
}
.line-navbar-one .lno-socials {
  padding-top: 20px;
  padding-bottom: 20px;
}
.line-navbar-one .lno-socials li a {
  color: #FFF;
  border: 2px solid #eeeeee;
  width: 50px;
  height: 50px;
  display: inline-block;
  text-align: center;
  padding: 13px;
}
.line-navbar-one .lno-socials li a:hover,
.line-navbar-one .lno-socials li a:focus {
  background-color: transparent;
  -webkit-box-shadow: 0 0 0;
  box-shadow: 0 0 0;
}
.line-navbar-one .lno-socials li a.facebook {
  background-color: #3b5998;
  border-color: #3b5998;
}
.line-navbar-one .lno-socials li a.facebook:hover,
.line-navbar-one .lno-socials li a.facebook:focus {
  background-color: #FFF;
  color: #3b5998;
}
.line-navbar-one .lno-socials li a.twitter {
  background-color: #55acee;
  border-color: #55acee;
}
.line-navbar-one .lno-socials li a.twitter:hover,
.line-navbar-one .lno-socials li a.twitter:focus {
  background-color: #FFF;
  color: #55acee;
}
.line-navbar-one .lno-socials li a.google-plus {
  background-color: #dd4b39;
  border-color: #dd4b39;
}
.line-navbar-one .lno-socials li a.google-plus:hover,
.line-navbar-one .lno-socials li a.google-plus:focus {
  background-color: #FFF;
  color: #dd4b39;
}
.line-navbar-one .lno-socials li a.pinterest {
  background-color: #cc2127;
  border-color: #cc2127;
}
.line-navbar-one .lno-socials li a.pinterest:hover,
.line-navbar-one .lno-socials li a.pinterest:focus {
  background-color: #FFF;
  color: #cc2127;
}
@media (max-width: 767px) {
  .line-navbar-one .lno-socials li {
    display: inline-block;
  }
}
@media (max-width: 767px) {
  .line-navbar-one .lno-socials {
    margin-left: 0;
  }
}
.line-navbar-one .lno-search-form {
  position: relative;
  padding-bottom: 0;
}
.line-navbar-one .lno-search-form .form-control {
  border: 2px solid #333333;
  border-radius: 0;
  height: 50px;
}
.line-navbar-one .lno-search-form .form-control:focus {
  -webkit-box-shadow: 0 0 0, 0 0 0;
  box-shadow: 0 0 0, 0 0 0;
}
.line-navbar-one .lno-search-form .btn-search {
  position: absolute;
  top: 17px;
  right: 21px;
  z-index: 9;
  background-color: rgba(0, 0, 0, 0);
  font-size: 16px;
}
/* Line navbar two (lnt)
/ -------------------------------------------------- */
.line-navbar-two {
  background-color: #ffffff;
  padding-top: 15px;
  padding-bottom: 15px;
}
@media (max-width: 767px) {
  .line-navbar-two {
    display: none;
  }
}
.line-navbar-two .navbar-right {
  margin-right: 0;
}
.line-navbar-two .navbar-collapse {
  position: relative;
}
.line-navbar-two .nav > li > a {
  border: 2px solid #333333;
  padding: 13px 20px;
  color: #333333;
  font-weight: bold;
}
.line-navbar-two .nav > li > .dropdown-menu {
  margin-top: 16px;
}
@media (min-width: 768px) {
  .line-navbar-two .nav.navbar-right .dropdown-menu:before {
    right: 15px;
    left: auto;
  }
  .line-navbar-two .nav.navbar-right .dropdown-menu:after {
    right: 16px;
    left: auto;
  }
}
.line-navbar-two .dropdown-menu {
  border-radius: 0;
}
@media (min-width: 768px) {
  .line-navbar-two .dropdown-menu:before {
    position: absolute;
    top: -9px;
    left: 14px;
    display: inline-block;
    border-right: 9px solid rgba(0, 0, 0, 0);
    border-bottom: 9px solid #CCC;
    border-left: 9px solid rgba(0, 0, 0, 0);
    border-bottom-color: rgba(0, 0, 0, 0.2);
    content: '';
  }
  .line-navbar-two .dropdown-menu:after {
    position: absolute;
    top: -8px;
    left: 15px;
    display: inline-block;
    border-right: 8px solid rgba(0, 0, 0, 0);
    border-bottom: 8px solid #FFF;
    border-left: 8px solid rgba(0, 0, 0, 0);
    content: '';
  }
}
.line-navbar-two .lnt-nav-mega li {
  position: static;
}
.line-navbar-two .lnt-nav-mega li .dropdown-menu {
  width: 100%;
  padding: 0;
}
.line-navbar-two .lnt-nav-mega li .dropdown-menu:after {
  border-bottom: 8px solid #f6f6f6;
}
.line-navbar-two .lnt-dropdown-mega-menu {
  padding: 0;
  background-color: #f6f6f6;
}
.line-navbar-two .lnt-dropdown-mega-menu .label {
  font-weight: normal;
  font-size: 10px;
  padding: 4px 7px;
}
.line-navbar-two .lnt-dropdown-mega-menu > .lnt-category {
  width: 250px;
  border-right: 1px solid #eeeeee;
  float: left;
}
@media (min-width: 768px) and (max-width: 992px) {
  .line-navbar-two .lnt-dropdown-mega-menu > .lnt-category {
    width: 190px;
  }
}
.line-navbar-two .lnt-dropdown-mega-menu > .lnt-category > li {
  position: relative;
}
.line-navbar-two .lnt-dropdown-mega-menu > .lnt-category > li > a {
  color: #333333;
  padding: 10px 20px;
  border-bottom: 1px solid #eeeeee;
  display: block;
  white-space: nowrap;
  width: 100%;
  overflow: hidden;
  text-overflow: ellipsis;
}
.line-navbar-two .lnt-dropdown-mega-menu > .lnt-category > li > a:hover,
.line-navbar-two .lnt-dropdown-mega-menu > .lnt-category > li > a:focus {
  text-decoration: none;
  background-color: #eeeeee;
}
.line-navbar-two .lnt-dropdown-mega-menu > .lnt-category > li.active:before {
  position: absolute;
  content: '';
  top: 50%;
  margin-top: -9px;
  right: 0;
  display: inline-block;
  border-right: 9px solid #E3E3E3;
  border-top: 9px solid transparent;
  border-bottom: 9px solid transparent;
}
.line-navbar-two .lnt-dropdown-mega-menu > .lnt-category > li.active:after {
  position: absolute;
  content: '';
  top: 50%;
  margin-top: -9px;
  right: -1px;
  display: inline-block;
  border-right: 9px solid #FFF;
  border-top: 9px solid transparent;
  border-bottom: 9px solid transparent;
}
.line-navbar-two .lnt-dropdown-mega-menu .lnt-subcategroy-carousel-wrap {
  margin-left: 250px;
  position: relative;
  background-color: #ffffff;
}
@media (min-width: 768px) and (max-width: 992px) {
  .line-navbar-two .lnt-dropdown-mega-menu .lnt-subcategroy-carousel-wrap {
    margin-left: 190px;
  }
}
.line-navbar-two .lnt-dropdown-mega-menu .lnt-subcategroy-carousel-wrap > div {
  display: none;
}
.line-navbar-two .lnt-dropdown-mega-menu .lnt-subcategroy-carousel-wrap > div.active {
  display: block;
}
.line-navbar-two .lnt-dropdown-mega-menu .lnt-subcategroy-carousel-wrap.container-fluid {
  padding: 0;
}
.line-navbar-two .lnt-dropdown-mega-menu .lnt-subcategroy-carousel-wrap [class*=col-] {
  padding: 0;
}
.line-navbar-two .lnt-dropdown-mega-menu .lnt-subcategroy-carousel-wrap .lnt-subcategory {
  padding-bottom: 10px;
}
.line-navbar-two .lnt-dropdown-mega-menu .lnt-subcategroy-carousel-wrap .lnt-subcategory .lnt-category-name {
  font-size: 14px;
  font-weight: bold;
  padding: 12px 20px 11px;
  margin-top: 0;
  border-bottom: 1px solid #eeeeee;
}
.line-navbar-two .lnt-dropdown-mega-menu .lnt-subcategroy-carousel-wrap .lnt-subcategory ul li {
  padding: 6px 20px;
}
.line-navbar-two .lnt-dropdown-mega-menu .lnt-subcategroy-carousel-wrap .lnt-subcategory ul li a {
  color: #333333;
}
.line-navbar-two .lnt-dropdown-mega-menu .lnt-subcategroy-carousel-wrap .carousel-indicators li {
  width: 15px;
  height: 15px;
  margin: 0 5px;
}
.line-navbar-two .lnt-search-form {
  position: relative;
  display: block;
  padding: 0;
  margin: 0 20px;
}
.line-navbar-two .lnt-search-form .input-group-btn > .btn {
  position: relative;
  border-radius: 0;
  border: 2px solid #333333;
  border-right-width: 0;
  padding: 13px 15px;
}
.line-navbar-two .lnt-search-form .form-control {
  border: 2px solid #333333;
  border-radius: 0;
  height: 50px;
  width: 215px;
}
@media (min-width: 992px) {
  .line-navbar-two .lnt-search-form .form-control {
    width: 400px;
  }
}
@media (min-width: 1200px) {
  .line-navbar-two .lnt-search-form .form-control {
    width: 600px;
  }
}
.line-navbar-two .lnt-search-form .form-control:focus {
  -webkit-box-shadow: 0 0 0, 0 0 0;
  box-shadow: 0 0 0, 0 0 0;
}
.line-navbar-two .lnt-search-form .btn-search {
  position: absolute;
  top: 7px;
  right: 10px;
  z-index: 9;
  background-color: rgba(0, 0, 0, 0);
  font-size: 16px;
}
.line-navbar-two .lnt-search-form .lnt-search-category .dropdown-menu {
  margin-top: 16px;
  padding: 0;
}
.line-navbar-two .lnt-search-form .lnt-search-category .dropdown-menu li a {
  padding: 10px 20px;
  border-bottom: 1px solid #eeeeee;
}
.line-navbar-two .lnt-search-form .lnt-search-suggestion strong {
  color: #fb4e4e;
}
.line-navbar-two .lnt-search-form .lnt-search-suggestion .dropdown-menu {
  width: 100%;
  margin-top: 16px;
  padding: 0;
}
.line-navbar-two .lnt-search-form .lnt-search-suggestion .dropdown-menu > li > a {
  padding: 10px 20px;
  border-bottom: 1px solid #eeeeee;
}
.line-navbar-two .lnt-search-form .lnt-search-suggestion .dropdown-menu:before {
  top: -9px;
  right: 17px;
  left: auto;
}
.line-navbar-two .lnt-search-form .lnt-search-suggestion .dropdown-menu:after {
  top: -8px;
  right: 18px;
  left: auto;
}
.line-navbar-two .lnt-search-form .lnt-search-suggestion .lnt-search-bottom-links {
  background-color: #F7F7F7;
  padding: 15px 20px;
  text-align: center;
}
.line-navbar-two .lnt-search-form .lnt-search-suggestion .lnt-search-bottom-links li {
  padding: 0 10px;
}
.line-navbar-two .lnt-search-form .lnt-search-suggestion .lnt-search-bottom-links li a {
  color: #333333;
  font-weight: bold;
  font-size: 12px;
}
.line-navbar-two .lnt-shopping-cart .dropdown-menu {
  min-width: 300px;
  padding: 0;
  margin-top: 16px;
}
.line-navbar-two .lnt-shopping-cart .dropdown-menu > li > div {
  white-space: nowrap;
  width: 300px;
  overflow: hidden;
  text-overflow: ellipsis;
}
.line-navbar-two .lnt-shopping-cart .btn-group .btn {
  border: 2px solid #333333;
  padding: 13px 19px;
  border-radius: 0;
  font-weight: bold;
}
.line-navbar-two .lnt-shopping-cart .btn-group .lnt-cart {
  font-size: 16px;
  padding: 0;
  vertical-align: inherit;
  width: 55px;
  height: 50px;
  margin-right: -1px;
}
.line-navbar-two .lnt-shopping-cart .btn-group .lnt-cart .item-added {
  position: absolute;
  opacity: .03;
  font-size: 45px;
  left: 7px;
  top: 2px;
  transform: rotate(-35deg);
  -webkit-transition: all 300ms;
  -o-transition: all 300ms;
  transition: all 300ms;
}
.line-navbar-two .lnt-shopping-cart .lnt-cart-products {
  border-bottom: 1px solid #eeeeee;
  padding: 15px 20px;
  position: relative;
}
.line-navbar-two .lnt-shopping-cart .lnt-cart-products .lnt-cart-total {
  position: absolute;
  top: 15px;
  right: 15px;
}
.line-navbar-two .lnt-shopping-cart .lnt-cart-products img {
  position: absolute;
  top: 14px;
  left: 20px;
  width: 60px;
  height: 60px;
  border: 2px solid #FFF;
  -webkit-box-shadow: 0 0 0 2px #333333;
  box-shadow: 0 0 0 2px #333333;
}
.line-navbar-two .lnt-shopping-cart .lnt-cart-products .lnt-product-info {
  padding-left: 75px;
}
.line-navbar-two .lnt-shopping-cart .lnt-cart-products .lnt-product-info .lnt-product-name {
  font-weight: bold;
}
.line-navbar-two .lnt-shopping-cart .lnt-cart-products .lnt-product-info .lnt-product-price {
  display: block;
  margin-left: 75px;
  margin-top: 2px;
  margin-bottom: -2px;
  color: #ADADAD;
}
.line-navbar-two .lnt-shopping-cart .lnt-cart-products .lnt-product-info .lnt-product-remove {
  margin-left: 67px;
  text-transform: uppercase;
  font-size: 10px;
  color: #fb4e4e;
}
.line-navbar-two .lnt-shopping-cart .lnt-cart-actions a {
  display: inline-block;
  width: 50%;
  text-align: center;
  padding: 10px;
  color: #FFF;
  background-color: #507BDF;
  margin: 0 -1px;
}
.line-navbar-two .lnt-shopping-cart .lnt-cart-actions a.lnt-view-cart-btn {
  background-color: #fb4e4e;
}
/* Line left navbar (lnl)
/ -------------------------------------------------- */
.line-navbar-left {
  position: absolute;
  background-color: #ffffff;
  width: 250px;
  top: 95px;
  bottom: 0;
  overflow-y: auto;
  z-index: 999;
  padding-top: 5px;
  border-right: 1px solid #EEE;
  -webkit-transition: left 300ms;
  -o-transition: left 300ms;
  transition: left 300ms;
  left: -250px;
}
.line-navbar-left.lnl-show {
  left: 0;
}
.line-navbar-left .lnl-nav-title {
  color: #333333;
  border-bottom: 1px solid #eeeeee;
  padding: 10px 25px;
  font-size: 16px;
  margin-bottom: 0;
  font-weight: bold;
}
.line-navbar-left .lnl-nav {
  padding-left: 0;
  list-style: none;
}
.line-navbar-left .lnl-nav ul {
  padding-left: 0;
  list-style: none;
}
.line-navbar-left .lnl-nav li {
  position: relative;
}
.line-navbar-left .lnl-nav li a {
  display: block;
  outline: 0;
  padding: 13px 25px;
  font-size: 13px;
  color: #333333;
}
.line-navbar-left .lnl-nav li a:hover,
.line-navbar-left .lnl-nav li a:focus {
  color: #333333;
  text-decoration: none;
  background-color: #eeeeee;
}
.line-navbar-left .lnl-nav li a > .lnl-link-icon {
  padding-right: 8px;
}
.line-navbar-left .lnl-nav li a > .lnl-btn-sub-collapse {
  position: absolute;
  right: 25px;
  top: 16px;
}
.line-navbar-left .lnl-nav li a.collapsed .lnl-btn-sub-collapse {
  -webkit-transform: rotate(180deg);
  -ms-transform: rotate(180deg);
  -o-transform: rotate(180deg);
  transform: rotate(180deg);
}
.line-navbar-left .lnl-nav li > .label {
  position: absolute;
  top: 15px;
  right: 25px;
}
.line-navbar-left .lnl-nav li.active > a {
  background-color: #3e3c50;
  -webkit-box-shadow: inset 3px 0 0 #eb4f9a;
  box-shadow: inset 3px 0 0 #eb4f9a;
}
.line-navbar-left .lnl-nav li .lnl-sub-one {
  background-color: #111015;
}
.line-navbar-left .lnl-nav li .lnl-sub-one li {
  border-top: 1px solid #17171e;
}
.line-navbar-left .lnl-nav li .lnl-sub-one li a {
  display: block;
  color: #a7a9ac;
}
.line-navbar-left .lnl-nav li .lnl-sub-one li a:hover,
.line-navbar-left .lnl-nav li .lnl-sub-one li a:focus {
  color: #eeeeee;
  background-color: #17171e;
  -webkit-box-shadow: inset 3px 0 0 #17171e;
  box-shadow: inset 3px 0 0 #17171e;
}
.line-navbar-left .lnl-nav li .lnl-sub-one li a > .lnl-link-icon {
  padding-right: 8px;
}
.line-navbar-left .lnl-nav li .lnl-sub-one li.active > a {
  background-color: #17171e;
  -webkit-box-shadow: inset 3px 0 0 #17171e;
  box-shadow: inset 3px 0 0 #17171e;
}
.line-navbar-left .lnl-nav li .lnl-sub-two {
  background-color: #1c1b24;
}
.line-navbar-left .lnl-nav li .lnl-sub-two li {
  border-top-color: 1px solid #23222d;
}
.line-navbar-left .lnl-nav li .lnl-sub-two li a {
  color: #a7a9ac;
}
.line-navbar-left .lnl-nav li .lnl-sub-two li a:hover,
.line-navbar-left .lnl-nav li .lnl-sub-two li a:focus {
  color: #eeeeee;
  background-color: #23222d;
  -webkit-box-shadow: inset 3px 0 0 #23222d;
  box-shadow: inset 3px 0 0 #23222d;
}
.line-navbar-left .lnl-nav li .lnl-sub-two li.active > a {
  background-color: #17171e;
  -webkit-box-shadow: inset 3px 0 0 #17171e;
  box-shadow: inset 3px 0 0 #17171e;
}
/* Content wrap
/ -------------------------------------------------- */
.content-wrap {
  position: relative;
  margin-left: 0;
  -webkit-transition: all 300ms;
  -o-transition: all 300ms;
  transition: all 300ms;
}
.content-wrap.lnl-push {
  -webkit-transform: translate(250px, 0);
  -ms-transform: translate(250px, 0);
  -o-transform: translate(250px, 0);
  transform: translate(250px, 0);
  opacity: 0.1;
  filter: alpha(opacity=10);
}
.content-wrap.lnl-overlay {
  opacity: 0.1;
  filter: alpha(opacity=10);
}

.img {
    background-image    : url('http://lp.campuslama.com/assets/img/BCA.jpg');

    background-repeat: no-repeat;
    
    background-size: 100%;

}
.active1 {
    position: absolute;
    content: '';
    top: 50%;
    margin-top: -9px;
    right: 0;
    display: inline-block;
    border-right: 9px solid #E3E3E3;
    border-top: 9px solid transparent;
    border-bottom: 9px solid transparent;
}
.nav-subtext {
    display: block;
    font-size: 10px;
    line-height: 13px;
    font-weight: 400;
    color: #999;
    white-space: normal;
}
    </style>

    </style>
        <!--  favicon -->
       
  
    <body id="top" class="has-header-search">
    <?php 
    $userdata = $this->session->userdata('logged_in');
                //print_r($userdata);
    ?>
        <!--header start-->
        <?php
            if(!empty($userdata)){
            $verify = $this->db->get_where('users',array('id'=>$userdata['id']));
             $emailverify=$verify->result();
           // echo $emailverify[0]->email_confirmed;
            if($emailverify[0]->email_confirmed=='NO'){
        ?>
        <header id="error_msg" class="second " style="background-color:#e64848;height: 50px;color: white;padding-top: 10px;font-weight:  bold;text-align:  center;">
            Please Verify Your Email Address.     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" class="swapbutton btn blue div15-btn" style="color:white;padding-top: 0px;height: 31px;" onclick="verify(<?php echo $userdata['id'] ?>)"><b> VERIFY</b></a>
        </header>
        <?php } } ?>
        <header id="" class="tt-nav transparent-header">

            <div class="header-sticky light-header">

                <div class="container">

                    <div class="search-wrapper">
                        <div class="search-trigger">
                            <!--div class='search-btn'></div>
                            <i class="material-icons">&#xE8B6;</i-->
                        </div>

                        <!-- Modal Search Form -->
                        <a href="<?php echo base_url('search')?>"><i class="search-close material-icons">&#xE5CD;</i></a>
                       
                    </div><!-- /.search-wrapper -->
                
                    <div id="materialize-menu" class="menuzord">

                        <!--logo start-->
                        <a href="<?php echo base_url('home')?>" class="logo-brand" style="margin-top:0px">
						
                            <img src="<?php echo base_url()?>assets/img/logo-01.png" alt="Hello Admission"  style=" /* padding-top: 11px; */  margin-top: 33px;">
                        </a>
                        <!--logo end-->

                        <!--mega menu start-->
                        <ul class="menuzord-menu pull-right">

                       <li class="menu"><a href="javascript:void(0)" onmouseover="get_Megamenu(1)">Management <span class="indicator"><i class="fa fa-angle-down"></i></span></a>
                            <div class="megamenu dropdown-menu line-navbar-two" role="menu" style="width: 116%;">
                                    <div class="">
           
                                        <div class="lnt-dropdown-mega-menu" style="margin-top:-20px;margin-left: -20px;margin-bottom: -20px;margin-right: -20px;">
                                          <!-- List of categories -->
                                          <ul class="lnt-category list-unstyled master_menu" id="master_menu">

                                            <li class="active"><a href="#subcategory-home">MBA</a></li>
                                            <li><a href="#subcategory-sports">Sports and outdoors </a></li>
                                            <li><a href="#subcategory-music">Digital music</a></li>
                                            <li><a href="#subcategory-books">Books </a></li>
                                            <li><a href="#subcategory-fashion">Fashion and beauty</a></li>
                                            <li><a href="#subcategory-movies">Movies and games</a></li>
                                          </ul>
                                          <!-- Subcategory and carousel wrap -->
                                          <div class="lnt-subcategroy-carousel-wrap container-fluid img">
                                            <div id="subcategory-home" class="active">
                                              <!-- Sub categories list-->

                                              <div class="lnt-subcategory col-sm-12 col-md-12 master_sub" style="padding-bottom:0px;font-weight:bold" id="master_sub">
                                      
                                              </div>
                                            
                                            </div> <!-- /.subcategory-home -->
                                            <div id="subcategory-sports">
                                              <!-- Sub categories list-->
                                              <div class="lnt-subcategory col-sm-8 col-md-8" style="padding-bottom: 120px;">
                                                <h3 class="lnt-category-name">Sports and outdoors</h3>
                                                <ul class="list-unstyled col-sm-6">
                                                  <li><a href="#">Exercise</a></li>
                                                  <li><a href="#">Fitness</a></li>
                                                  <li><a href="#">Hunting</a></li>
                                                  <li><a href="#">Fishing</a> <span class="label label-primary">Trending</span></li>
                                                
                                                </ul>
                                                <ul class="list-unstyled col-sm-6">
                                                  <li><a href="#">Golf</a></li>
                                                  <li><a href="#">Outdoor clothing</a></li>
                                                  <li><a href="#">Cycling</a></li>
                                                 
                                                </ul>
                                              </div>
                                              <!-- Carousel -->
                                              <div class="col-sm-4 col-md-4">
                                              </div>
                                            </div> <!-- /.subcategory-sports -->
                                           
                                          </div> <!-- /.lnt-subcategroy-carousel-wrap -->
                                        </div> <!-- /.lnt-dropdown-mega-menu -->
                                    </div>
                         
                          </div>
                       </li>   
                   <!--  <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 marketing" style="position: absolute; left: 25%; top: 0px;">
                   
                </div> -->
                        <li class="menu1"><a href="javascript:void(0)" onmouseover="get_Megamenu(2)">Engineering <span class="indicator"><i class="fa fa-angle-down"></i></span></a>
                               <div class="megamenu dropdown-menu line-navbar-two" role="menu" style="display:block;width: 116%;">
                                    <div class="">
           
                                        <div class="lnt-dropdown-mega-menu" style="margin-top:-20px;margin-left: -20px;margin-bottom: -20px;margin-right: -20px;">
                                          <!-- List of categories -->
                                          <ul class="lnt-category list-unstyled master_menu" id="master_menu">

                                            <li class="active"><a href="#subcategory-home">MBA</a></li>
                                            <li><a href="#subcategory-sports">Sports and outdoors </a></li>
                                            <li><a href="#subcategory-music">Digital music</a></li>
                                            <li><a href="#subcategory-books">Books </a></li>
                                            <li><a href="#subcategory-fashion">Fashion and beauty</a></li>
                                            <li><a href="#subcategory-movies">Movies and games</a></li>
                                          </ul>
                                          <!-- Subcategory and carousel wrap -->
                                          <div class="lnt-subcategroy-carousel-wrap container-fluid img">
                                            <div id="subcategory-home" class="active">
                                              <!-- Sub categories list-->

                                              <div class="lnt-subcategory col-sm-12 col-md-12 master_sub" style="padding-bottom:0px;font-weight:bold" id="master_sub">
                                      
                                              </div>
                                            
                                            </div> <!-- /.subcategory-home -->
                                            <div id="subcategory-sports">
                                              <!-- Sub categories list-->
                                              <div class="lnt-subcategory col-sm-8 col-md-8" style="padding-bottom: 120px;">
                                                <h3 class="lnt-category-name">Sports and outdoors</h3>
                                                <ul class="list-unstyled col-sm-6">
                                                  <li><a href="#">Exercise</a></li>
                                                  <li><a href="#">Fitness</a></li>
                                                  <li><a href="#">Hunting</a></li>
                                                  <li><a href="#">Fishing</a> <span class="label label-primary">Trending</span></li>
                                                
                                                </ul>
                                                <ul class="list-unstyled col-sm-6">
                                                  <li><a href="#">Golf</a></li>
                                                  <li><a href="#">Outdoor clothing</a></li>
                                                  <li><a href="#">Cycling</a></li>
                                                 
                                                </ul>
                                              </div>
                                              <!-- Carousel -->
                                              <div class="col-sm-4 col-md-4">
                                              </div>
                                            </div> <!-- /.subcategory-sports -->
                                           
                                          </div> <!-- /.lnt-subcategroy-carousel-wrap -->
                                        </div> <!-- /.lnt-dropdown-mega-menu -->
                                    </div>
                         
                          </div> 
                       </li> 
						<li>	
							<a href="<?php echo base_url('home'); ?>" class="black-text">Home</a>&nbsp;&nbsp;
						</li>
                         <li>	
							<a href="<?php echo base_url('explorer'); ?>" class="black-text">Explore</a>&nbsp;&nbsp;
						</li>
						<?php if(!$this->session->userdata('logged_in')){ ?>
						<li>	
						
							<a href="<?php echo base_url('login'); ?>" class="black-text">Login</a>&nbsp;&nbsp;
						</li>
						<!-- <li>	
							<a href="<?php echo base_url('login'); ?>" class="black-text">SignUp</a>&nbsp;&nbsp;
						</li> -->
						<?php }else{ 
						 $userdata = $this->session->userdata('logged_in');
						
						?>
						<li>
							<?php if($userdata['role'] == 2){ ?>
							<a href="<?php echo base_url('dashboard/student'); ?>" class="black-text"><?php echo $userdata['name']; ?></a>&nbsp;&nbsp;
							<?php }else if($userdata['role'] == 1){ ?>
							<a href="<?php echo base_url('dashboard/au'); ?>" class="black-text"><?php echo $userdata['name']; ?></a>&nbsp;&nbsp;
									
							<?php }else if($userdata['role'] == 4){ ?>
							<a href="<?php echo base_url('admin/dashboard'); ?>" class="black-text"><?php echo $userdata['name']; ?></a>&nbsp;&nbsp;
							<?php } ?>
						</li>
						<li>	
							<a href="<?php echo base_url('logout'); ?>" class="black-text">Logout</a>&nbsp;&nbsp;
						</li>
						<?php } ?>
						<?php 
						 $userdata = $this->session->userdata('logged_in');
						if(!$userdata){
						?>
						<!-- <li>	
							<a href="<?php echo base_url('login/academic_register'); ?>" class="black-text">College Signup</a>&nbsp;&nbsp;
						</li> -->
						<?php }?>
					<li>	
							<a href="<?php echo base_url('search'); ?>" class="black-text"><i class="fa fa-search" style="margin-top: 41px;margin-left: 36px;"></i></a>&nbsp;&nbsp;
						</li>
                        </ul>
                        <!--mega menu end-->

                    </div>
                </div>
            </div>
        </header>
        <!--header end-->
        <script type="text/javascript">
function ajaxindicatorstart(text)
{
    if(jQuery('body').find('#resultLoading').attr('id') != 'resultLoading'){
    jQuery('body').append('<div id="resultLoading" style="display:none"><div><img src="<?php echo base_url()?>uploads/ajax-loader.gif"><div>'+text+'</div></div><div class="bg"></div></div>');
    }

    jQuery('#resultLoading').css({
        'width':'100%',
        'height':'100%',
        'position':'fixed',
        'z-index':'10000000',
        'top':'0',
        'left':'0',
        'right':'0',
        'bottom':'0',
        'margin':'auto'
    });

    jQuery('#resultLoading .bg').css({
        'background':'#000000',
        'opacity':'0.7',
        'width':'100%',
        'height':'100%',
        'position':'absolute',
        'top':'0'
    });

    jQuery('#resultLoading>div:first').css({
        'width': '250px',
        'height':'75px',
        'text-align': 'center',
        'position': 'fixed',
        'top':'0',
        'left':'0',
        'right':'0',
        'bottom':'0',
        'margin':'auto',
        'font-size':'16px',
        'z-index':'10',
        'color':'#ffffff'

    });

    jQuery('#resultLoading .bg').height('100%');
       jQuery('#resultLoading').fadeIn(300);
    jQuery('body').css('cursor', 'wait');
}

function ajaxindicatorstop()
{
    jQuery('#resultLoading .bg').height('100%');
       jQuery('#resultLoading').fadeOut(300);
    jQuery('body').css('cursor', 'default');
}

</script>
		<script type="text/javascript">
         function verify(id)
     {
        //alert(id);
       // return false;
         var email="<?php echo trim($userdata['email']);?>";
         var name="<?php echo trim($userdata['name']);?>";
         var dataString={id:id,email:email,name:name};
        $.ajax({
                     type:'POST',
                     url:'<?php echo base_url(); ?>student/email_verify',
                     data:dataString,
                      beforeSend: function () {
                          ajaxindicatorstart('Please wait..');
                     },
                     success:function(res)
                     {
                        var obj = JSON.parse(res);
                        console.log(obj);
                         if(obj.msg=='Please check your email at to verify the email address')
                          { 
                             //toastr["error"]('Please verify email &nbsp;&nbsp;'+'Click here to <a href="<?php echo base_url() ?>login/verify/<?php echo md5($userdata['name']); ?>/<?php echo base64_encode($userdata['id']); ?>" target="_blank">Verify</a>');
                              toastr["info"](obj.msg).css("width","500px");
                              ajaxindicatorstop();
                         }
 
                     }
                 });
                 
     }
function get_Megamenu(id){
            var streamid = id;
     var smid=id+'-Management-active';
    get_sub_megamenu(smid);
   $("#submenu"+smid).removeClass('active');
    $('.master_menu').empty();
    $.ajax({
        type: "POST",
        url: "<?php echo base_url('academic/stream_program'); ?>",
        data:{streamid:streamid},
        success: function(response){
            var res = JSON.parse(response);
            var html='';
            var j=1;
            $.each(res, function (i, itm) {
            html+='<li id="submenu'+itm.id+'"><a href="#subcategory-sports" id="'+itm.id+'-'+itm.name+'"  onmouseover="get_sub_megamenu(this.id)" onmouseout="normalImg(this.id)">'+itm.name+'</a></li>';                               
            });

            $('.master_menu').html(html);
        }
    });
     }

function  normalImg(id){
       var streamid = id;
    steam_Id = streamid.split('-');
    $("#submenu"+steam_Id[0]).removeClass('active');
}
     function get_sub_megamenu(id){
            var streamid = id;
  
    steam_Id = streamid.split('-');
     console.log(steam_Id[0]);

    $("#submenu"+steam_Id[0]).addClass('active');
      $.ajax({
        type: "POST",
        url: "<?php echo base_url('academic/stream_program_spe'); ?>",
        data:{streamid:steam_Id[0]},
        success: function(response){
            var res = JSON.parse(response);
          //  console.log(res);
            // $('#program').append('<option value="" disabled selected>Choose Program</option>');
            var html='';
            var j=1;
             html+='<h3 class="lnt-category-name">'+steam_Id[1]+'</h3><ul class="list-unstyled col-md-4" style="font-weight:bold">';
            $.each(res, function (i, itm) {
               html+='<li><a href="#"><b>'+itm.name +'</b><span class="nav-subtext">'+steam_Id[1]+'</span></a></li>';
                                         
               //  html+=' <li  data-id="'+j+++'"><a href="#" >'+itm.name+' </a></li>';
                // html+='<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3" style="padding-left:0px;padding-right: 2px;">'+
                //                              '<figure class="cp-gallery">'+
                //                              '<span class="hover-ripple right" data-hover-color="#000" data-hover-opacity="0.6"></span>'+
                //                              '<img src="http://localhost/helloadmission/assets/img/blog/blog-18.jpg" class="img-responsive" alt="" style="width:210px;height:163px"><figcaption>'+
                //                              '<span class="text-uppercase"><a href="#" class="white-link link-underline" style="color:white">'+itm.name+'</a></span>'+
                //                             '</figcaption>'+
                //                         '</figure>'+
                //                         '</div> ';
                
                    // html+=' <div class="col-md-4">'+
                    //         '<div class="thumbnail ">'+
                    //             '<img src="http://localhost/helloadmission/assets/img/blog/blog-18.jpg"  alt="" style="width:350px;height:130px">'+
                    //             '<div class="caption">'+
                    //                 '<b>'+itm.name+'</b>'+
                                   
                    //             '</div></div></div>';
                    //  html+='<div class="col-md-4 " ><figure class="portfolio style-11 relative transition ov-hidden" style="box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);">'+
             
                    //     '<img src="<?php echo base_url()?>assets_new/img/01.jpg" class="img-responsive" alt="MBA Branding and Advertising">'+
                    //     '<div class="portfolio-center" style="display: block;">'+
                    //         '<div class="portfolio-center-inner" style="color:black;font-weight:bold;margin-top:105px">'+itm.name+'</div>'+
                    //     '</div>'+
                    //     '<a href="http://lp.campuslama.com/programme-detail/mba/branding-and-advertising" class="title-link" style="color: #fff;">'+
                    //     '</a><figcaption class="transition" style="color: #fff; padding: 45px 61px; */"><a href="#" class="title-link" style="color: #fff;">'+
                    //         '<span class="mb-" style="color:black;font-weight:bold"></span>'+
                    //         '</a><p><a href="#" class="title-link" style="color: #fff;"></a><a style="background-color: #0e2a4b;color:white" class="btn" href="#">View More</a></p>'+
                    //     '</figcaption>'+
                    // '</figure></div>';
                   if((i+1)%3 ===0){

                  html+= '</ul><ul class="list-unstyled col-md-4" >';
                }                             
            });
            html+='</ul>';   
            $('.master_sub').html(html);
        }
    });
     }
    //  function get_Megamenu(id){
    //      //$(".megamenu").css("display","block");
    //         var streamid = id;
    //         if(id==1){
    //            get_sub_megamenu(1); 
    //            $('.menu').addClass('active');
    //            $('.menu1').removeClass('active');
    //         }else{
    //             get_sub_megamenu(); 
    //             $('.menu').removeClass('active');
    //             $('.menu1').addClass('active');
    //         }
             
    // //alert(streamid);
    
    
    // $('.information_menu').empty();
    // $.ajax({
    //     type: "POST",
    //     url: "<?php echo base_url('academic/stream_program'); ?>",
    //     data:{streamid:streamid},
    //     success: function(response){
    //         var res = JSON.parse(response);
    //         console.log(res);
    //         // $('#program').append('<option value="" disabled selected>Choose Program</option>');
    //         var html='';
    //         var j=1;
    //         $.each(res, function (i, itm) {
    //             html+=' <li class="active" data-id="'+j+++'"><a href="#" onmouseover="get_sub_megamenu('+itm.id+')">'+itm.name+' </a></li>';
                                                    
    //         });

    //         $('.information_menu').html(html);
    //     }
    // });
    //  }

    //  function get_sub_megamenu(id){
    //         var streamid = id;
    // //alert(streamid);
    // $('.slideshow').empty();
    // //$('.slideshow').html(html);
    // $.ajax({
    //     type: "POST",
    //     url: "<?php echo base_url('academic/stream_program_spe'); ?>",
    //     data:{streamid:streamid},
    //     success: function(response){
    //         var res = JSON.parse(response);
    //         console.log(res);
    //         // $('#program').append('<option value="" disabled selected>Choose Program</option>');
    //         var html='';
    //         var j=1;
    //          html+=' <div class="overlay-id'+j+' overlay-item active" style="overflow-y:scroll"><div class="flex-row row">';
    //         $.each(res, function (i, itm) {
               
    //            //  html+=' <li  data-id="'+j+++'"><a href="#" >'+itm.name+' </a></li>';
    //             // html+='<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3" style="padding-left:0px;padding-right: 2px;">'+
    //             //                              '<figure class="cp-gallery">'+
    //             //                              '<span class="hover-ripple right" data-hover-color="#000" data-hover-opacity="0.6"></span>'+
    //             //                              '<img src="http://localhost/helloadmission/assets/img/blog/blog-18.jpg" class="img-responsive" alt="" style="width:210px;height:163px"><figcaption>'+
    //             //                              '<span class="text-uppercase"><a href="#" class="white-link link-underline" style="color:white">'+itm.name+'</a></span>'+
    //             //                             '</figcaption>'+
    //             //                         '</figure>'+
    //             //                         '</div> ';
                
    //                 // html+=' <div class="col-md-4">'+
    //                 //         '<div class="thumbnail ">'+
    //                 //             '<img src="http://localhost/helloadmission/assets/img/blog/blog-18.jpg"  alt="" style="width:350px;height:130px">'+
    //                 //             '<div class="caption">'+
    //                 //                 '<b>'+itm.name+'</b>'+
                                   
    //                 //             '</div></div></div>';
    //                  html+='<div class="col-md-4 " ><figure class="portfolio style-11 relative transition ov-hidden" style="box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);">'+
             
    //                     '<img src="<?php echo base_url()?>assets_new/img/01.jpg" class="img-responsive" alt="MBA Branding and Advertising">'+
    //                     '<div class="portfolio-center" style="display: block;">'+
    //                         '<div class="portfolio-center-inner" style="color:black;font-weight:bold;margin-top:105px">'+itm.name+'</div>'+
    //                     '</div>'+
    //                     '<a href="http://lp.campuslama.com/programme-detail/mba/branding-and-advertising" class="title-link" style="color: #fff;">'+
    //                     '</a><figcaption class="transition" style="color: #fff; padding: 45px 61px; */"><a href="#" class="title-link" style="color: #fff;">'+
    //                         '<span class="mb-" style="color:black;font-weight:bold"></span>'+
    //                         '</a><p><a href="#" class="title-link" style="color: #fff;"></a><a style="background-color: #0e2a4b;color:white" class="btn" href="<?php echo base_url()?>listing/view_cource/'+itm.id+'">View More</a></p>'+
    //                     '</figcaption>'+
    //                 '</figure></div>';
                                               
    //         });
    //         html+='</div>';   
    //         $('.slideshow').html(html);
    //     }
    // });
    //  }
        </script>
        
	<script type="text/javascript">
     
   $(document).ready(function() {
//$(".megamenu").css("display","none");
    $('.information_menu').find('li').hover(function(e) {
         $('.information_menu').find('li').removeClass('active');
        $(this).addClass('active');
        $(".overlay-item").removeClass("active");
        $(".overlay-item").removeClass("inactive");
    $(".overlay-id"+$(this).data("id")).addClass("active").removeClass("inactive");
        
         $(".overlay-id"+$(this).data("id")).prev().addClass("inactive")
    });     
  
    $('.slideshow').children().on('mouseleave',function(e) {
    $(this).removeClass("active");
  });    
    
    $('.carousel').carousel();
   });


 
  </script>
    <script type="text/javascript">
!function(){"use strict";function a(){n.addClass("lnl-show").removeClass("lnl-hide"),s.addClass(i).css({height:o,overflow:"hidden"}),$("html, body").css("overflow","hidden"),l.find("span").removeClass("fa-bars").addClass("fa-remove")}function t(){n.removeClass("lnl-show").addClass("lnl-hide"),s.removeClass(i).css({height:"auto",overflow:"auto"}),$("html, body").css("overflow","auto"),l.find("span").removeClass("fa-remove").addClass("fa-bars")}function e(){try{return document.createEvent("TouchEvent"),!0}catch(a){return!1}}var n=$(".line-navbar-left"),l=$(".lno-btn-toggle"),s=($(".lno-btn-collapse"),$(".content-wrap")),i=s.data("effect"),o=$(window).height()-95;n.addClass("lnl-hide"),l.click(function(){n.hasClass("lnl-hide")?a():t()}),l.click(function(a){a.preventDefault(),n.hasClass("lnl-collapsed")?(n.removeClass("lnl-collapsed"),s.removeClass("lnl-collapsed"),$(this).find(".lnl-link-icon").removeClass("fa-arrow-right").addClass("fa-arrow-left")):(n.addClass("lnl-collapsed"),s.addClass("lnl-collapsed"),$(this).find(".lnl-link-icon").removeClass("fa-arrow-left").addClass("fa-arrow-right"))}),1==e()&&$(window).swipe({swipeRight:function(){a(),$(".navbar-collapse").removeClass("in")},swipeLeft:function(){t()},threshold:75}),$(window).resize(function(){$(window).width()>=767&&t()}),$(".lnt-search-input").focusin(function(){$(".lnt-search-suggestion").find(".dropdown-menu").slideDown()}),$(".lnt-search-input").focusout(function(){$(".lnt-search-suggestion").find(".dropdown-menu").slideUp()});var r=$(" .lnt-search-category ").find(" .dropdown-menu ").find(" li ");r.click(function(){var a=$(this).find(" a ").text(),t=$(" .selected-category-text ");t.text(a)}),$(".lnt-search-input").bind("keyup change",function(){var a=$(this).val(),t=$(".lnt-search-suggestion").find(".dropdown-menu > li > a");t.unhighlight({element:"strong",className:"important"}),a&&t.highlight(a,{element:"strong",className:"important"})}),$(".add-to-cart").click(function(){var a=randomColor({luminosity:"light",format:"rgb"}),t=$(".lnt-cart").find(".cart-item-quantity").text();$(".lnt-cart").css("backgroundColor",a),$(".lnt-cart").find("span").eq(0).addClass("item-added"),$(".lnt-cart").find(".cart-item-quantity").text(++t)}),$(".add-to-cart").click(function(){var a=randomColor({luminosity:"light",format:"rgb"}),t=$(".lno-cart").find(".cart-item-quantity").text();$(".lno-cart").css("backgroundColor",a),$(".lno-cart").find("span").eq(0).addClass("item-added"),$(".lno-cart").find(".cart-item-quantity").text(++t)});var c=$(".lnt-category").find("li").find("a"),d=$(".lnt-category").find("li");c.mouseenter(function(){d.removeClass("active"),$(this).parent().addClass("active");var a=$(this).attr("href");$(".lnt-subcategroy-carousel-wrap").find("> div").removeClass("active"),$(a).addClass("active")}),c.on("touchstart, touchend",function(a){a.preventDefault(),d.removeClass("active"),$(this).parent().addClass("active");var t=$(this).attr("href");$(".lnt-subcategroy-carousel-wrap").find("> div").removeClass("active"),$(t).addClass("active")}),1==e()&&($(window).swipe({swipeLeft:function(){$(".carousel").carousel("next")},swipeRight:function(){$(".carousel").carousel("prev")},threshold:75}),$(".carousel-indicators").hide()),$(".carousel-indicators").find("li").mouseenter(function(){var a=$(this).data("slide-to");$(this).parents(".carousel").carousel(a)}),String.prototype.capitalize=function(){return this.replace(/(?:^|\s)\S/g,function(a){return a.toUpperCase()})},$(".lnt-category > li").each(function(){var a=$(this).find("a");$(".lnl-nav").append("<li><a class='collapsed' data-toggle='collapse' href='#collapse"+a.text().capitalize().replace(/[, ]+/g,"")+"' aria-expanded='false' aria-controls='collapse"+a.text().capitalize().replace(/[, ]+/g,"")+"' data-subcategory="+a.attr("href").replace("#","")+"><span class='lnl-link-text'>"+$(this).text()+"</span><span class='fa fa-angle-up lnl-btn-sub-collapse'></span></a></li>")}),$(".lnl-nav li").each(function(){var a=$(this).find("a");$(this).append("<ul class='lnl-sub-one collapse' id='"+a.attr("href").replace("#","")+"' data-subcategory='"+a.data("subcategory")+"'></ul>")}),$(".lnt-subcategroy-carousel-wrap > div").each(function(){var a=$(this).attr("id"),t=$(this).find("ul").map(function(){return $(this).html()}).get(),e=$("ul[data-subcategory='"+a+"']");e.append(t)}),$(".navbar-toggle").click(function(){$(this).hasClass("collapsed")&&t()}),l.click(function(){$(".navbar-collapse").removeClass("in")})}();
  </script>