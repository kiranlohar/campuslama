<?php if(isset($element)){ ?>
	<li class="dd-item dd3-item" data-id="{{$dataid}}">
		<div class="row">
			<div class="col s3 m3 l3">&nbsp;</div>
			<div class="col s6 m6 l6">
				@if($element->fe_type == 'TEXT')
					<div class="input-field">
						<input id="name" type="text" name="first_name">
						<label for="first_name">{{$element->fe_label}}</label>
					</div>
				@endif
				@if($element->fe_type == 'SELECT')
					<div class="input-field">
						<select>
							<option>{{$element->fe_label}}</option>
						</select>
						
					</div>
				@endif	
			</div>	
			<div class="col s3 m3 l3">&nbsp;</div>
		</div>
	</li>	