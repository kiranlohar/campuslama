<!DOCTYPE html>
  <html>
    <head>
		
		
		<meta charset="utf-8">
          <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <meta name="robots" content="noindex, nofollow" />
     
     
           <!--  favicon -->
       <link rel="shortcut icon" href="<?php echo base_url('assets/img/fav.png' ); ?>">
        <!--  apple-touch-icon -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url('assets/img/ico/apple-touch-icon-144-precomposed.png'); ?>">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/img/ico/apple-touch-icon-114-precomposed.png'); ?>">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/img/ico/apple-touch-icon-72-precomposed.png'); ?>">
        <link rel="apple-touch-icon-precomposed" href="assets/img/ico/apple-touch-icon-57-precomposed.png'); ?>">

		<link href="<?php echo base_url('assets/explore/css/animate.css');?>" type="text/css" rel="stylesheet">
		
		<link href="<?php echo base_url('assets/explore/css/ion.rangeSlider.css');?>" type="text/css" rel="stylesheet"/>
		<link href="<?php echo base_url('assets/explore/css/ion.rangeSlider.skinHTML5.css');?>" type="text/css" rel="stylesheet"/>
		
		<link href="<?php echo base_url('assets/explore/css/frontend/quiz_mycustom.css');?>" type="text/css" rel="stylesheet">
		<link href="<?php echo base_url('assets/explore/css/frontend/quiz_mycustom_media.css');?>" type="text/css" rel="stylesheet">
	
		
		<script type="text/javascript" src="<?php echo base_url('assets/explore/js/jquery-2.1.4.min.js');?>"></script>
	
		<script type="text/javascript" src="<?php echo base_url('assets/explore/js/ion.rangeSlider.js');?>"></script>
		
		<script type="text/javascript" src="<?php echo base_url('assets/explore/js/frontend/mobile_detect.js');?>"></script>
    </head>
<style>
#mobilenumberdiv {
    background-color: #fff;
}


</style>
<body>
	<input type="text" id="previous_div" value=""/>
	<input type="hidden" id="question1" value="yes"/>
	<input type="hidden" id="question2" value="yes"/>
	
	<form id="frontend_quiz" method="POST" action="<?php echo base_url('explore/explore'); ?>">
		<input type="hidden" name="45" id="input_1" value=""/>
		<input type="hidden" name="12" id="input_2" value=""/>
		<input type="hidden" name="44" id="input_3" value=""/>
		<input type="hidden" name="38" id="input_4" value=""/>
		<input type="hidden" name="48" id="input_13" value=""/>
		<input type="hidden" name="49" id="input_14" value=""/>
		
		<input type="hidden" name="50" id="input_15" value=""/>
		<input type="hidden" name="40" id="input_5" value=""/>
		<input type="hidden" name="39" id="input_6" value=""/>
		<input type="hidden" name="10" id="input_7" value=""/>
		<input type="hidden" name="1" id="input_8" value=""/>
		<input type="hidden" name="5" id="input_9_1" value=""/>
		<input type="hidden" name="6" id="input_9_2" value=""/>
		<input type="hidden" name="7" id="input_9_3"  value=""/>
		<input type="hidden" name="41" id="input_10" value=""/>
		<input type="hidden" name="43" id="input_11" value=""/>
		<input type="hidden" name="4" id="input_12_1" value=""/>
		<input type="hidden" name="password" id="input_12_2" value=""/>
		<input type="hidden" name="rep_password" id="input_12_3" value=""/>
		<input type="hidden" name="8" id="input_12_4" value=""/>
	</form>
	
	<div class="BackgroundImg">
		<!-- START HEADER -->
<!--
    <header id="header" class="page-topbar">
        <div class="navbar-fixed">
            <nav class="navbar-color">
                <div class="nav-wrapper">
                    <ul class="left">                      
                      <li><h1 class="logo-wrapper"><a href="<?php //echo base_url('home/index'); ?>" class="LogoMargin"><span class="red-text">Hello</span><span class="black-text">Admission</span></a></h1></li>
					  
                    </ul>                   
                    <ul class="right right-nav"> 
						<li>	
							<a onclick="FBLogin();" class="btn btn-large waves-effect waves-light blue">Facebook</a>&nbsp;&nbsp;					
						</li>
						<li>
							<a onclick="GLogin();" class="btn btn-large waves-effect waves-light red">Google</a>&nbsp;&nbsp;
						</li>
						<li>	
							<a href="<?php //echo base_url('explore/index'); ?>" class="black-text">Explore</a>&nbsp;&nbsp;
						</li>
						<?php //if(!$this->session->userdata('logged_in')){ ?>
						<li>	
							<a href="<?php //echo base_url('login/index'); ?>" class="black-text">Login</a>&nbsp;&nbsp;
						</li>
						<li>	
							<a href="<?php //echo base_url('login/student_register'); ?>" class="black-text">SignUp</a>&nbsp;&nbsp;
						</li>
						<?php //}else{ 
						// $userdata = $this->session->userdata('logged_in');
						?>
						<li>	
							<a href="<?php //echo base_url('dashboard'); ?>" class="black-text"><?php echo $userdata['name']; ?></a>&nbsp;&nbsp;
						</li>
						<li>	
							<a href="<?php //echo base_url('login/logout'); ?>" class="black-text">Logout</a>&nbsp;&nbsp;
						</li>
						<?php //} ?>
						<li>	
							<a href="<?php //echo base_url('login/academic_register'); ?>" class="black-text">College Signup</a>&nbsp;&nbsp;
						</li>
						
					</ul>                    
                </div>
            </nav>
        </div>
    </header>
-->

    <!-- END HEADER -->
		
		
		
		
    <header id="header" class="page-topbar">
        <div class="navbar-fixed" >
            <nav class="navbar-color" style="position: relative">
                <div class="nav-wrapper">
					
					<div class="container">

                    <div class="search-wrapper">
                        <!-- Modal Search Form -->
                        <i class="search-close material-icons">&#xE5CD;</i>
                        <div class="search-form-wrapper">
                            <form action="#" class="white-form">
                                <div class="input-field">
                                    <input type="text" name="search" id="search">
                                    <label for="search" class="">Search Here...</label>
                                </div>
                                <button class="btn pink search-button waves-effect waves-light" type="submit"><i class="material-icons">&#xE8B6;</i></button>
                                
                            </form>
                        </div>
                    </div><!-- /.search-wrapper -->
                
                    <div  class="menuzord">

                        <!--logo start-->

                        <!-- <a href="index.html" class="">
									<a href="<?php echo base_url('home/index'); ?>" class="LogoMargin">
										<span class="red-text">Hello</span>
										<span class="black-text">Admission</span>
									</a>
                        </a> -->
                        <!--logo end-->

                        <!--mega menu start-->

                    <!--     <ul class="menuzord-menu pull-right">

                            
                            <li>
								<a onclick="GLogin();" class="black-text">Google</a>&nbsp;&nbsp;
                            </li>
                            
                            <li>
								<a onclick="FBLogin();" class="black-text">Facebook</a>&nbsp;&nbsp;		
                            </li>
                            
                            <li class="active">
									<a href="<?php echo base_url('explore/index'); ?>" class="black-text">Explore</a>&nbsp;&nbsp;
                            </li>
                            
                            
                            <li class="">
									<a href="<?php echo base_url('login/index'); ?>" class="black-text">Login</a>&nbsp;&nbsp;
                            </li>
                            
                            <li class="">
									<a href="<?php echo base_url('login/student_register'); ?>" class="black-text">SignUp</a>&nbsp;&nbsp;
                            </li>

                        </ul>
 -->
                     

                    </div>
    
    
    </div>
            </nav>
        </div>
    </header-->

		<div id="div1" class="animated bounceInRight">
			<div class="row">
				<div class="col m3">&nbsp;</div>
				<div class="col s12 m6" style="margin-top:-97px">
					<div class="card-panel" id="div1-strip" style="background-image:url('<?php echo base_url()?>assets/explore/images/frontend/blue-strip.jpg');">
						<div id="div1-strip-text" class="center">How are You?</div>
					</div>
					<div class="card-panel" id="div1-content">
						<div id="div1-content-action">
							<div class="row">
								<div class="col s6 l6">
									<div class="row center" id="div_good">
										<a class="btn-floating div1-btn" id="div1-good">
											<img src="<?php echo base_url()?>assets/explore/images/frontend/howareyouicon1.png">
										</a>
										<h5 class="black-text">Good</h5>											
									</div>											
								</div>
								<div class="col s6 l6">
									<div class="row center" id="div_not_good">
										<a class="btn-floating div1-btn" id="div1-bad">
											<img src="<?php echo base_url()?>assets/explore/images/frontend/howareyouicon2.png">
										</a>
										<h5 class="black-text">Naah</h5>		
									</div>	
								</div>								
							</div>
						</div>
						<div id="div1-content-msg">
							<div class="center" id="div1-msg">
								
							</div>
						</div>
					</div>
				</div>
				<div class="col m3">
					<a class="btn-floating btn-large waves-effect waves-light blue darken-3" id="div_back_1_p" style="margin-left:100px;background-color: #3374ac"><i class="mdi-hardware-keyboard-arrow-right"></i></a>
				</div>
			</div>
			<input type="hidden" id="div1-trigger"/>
		</div>
		
		<div id="div2" class="">
			<div class="row">
				<div class="col m2">
					<a class="btn-floating btn-large waves-effect waves-light green darken-3" id="div_back_2" style=""><i class="mdi-hardware-keyboard-arrow-left"></i></a>
				</div>
				<div class="col s12 m8" style="margin-top:-130px">
					<div class="card-panel" id="div2-strip" style="background-image:url('<?php echo base_url()?>assets/explore/images/frontend/wheredoyoulivebg.jpg');" data-img-width="1600" data-img-height="78">
						<div id="div2-strip-text" class="center">Where Do You Live?</div>
					</div>
					<div class="card-panel" id="div2-content">
						<div id="div2-content-action">
							<div class="row center">									
								<div class="col l1 div2-btn" id="div2-Delhi">
									<img src="<?php echo base_url()?>assets/explore/images/frontend/wheredoyoulive-1.png" width="80" height="80">
									<h6 class="black-text">Delhi</h6>	
								</div>
								<div class="col l1 div2-btn" id="div2-Mumbai">
									<img src="<?php echo base_url()?>assets/explore/images/frontend/wheredoyoulive-2.png" width="80" height="80">
									<h6 class="black-text">Mumbai</h6>
								</div>
								<div class="col l1 div2-btn" id="div2-Pune">
									<img src="<?php echo base_url()?>assets/explore/images/frontend/wheredoyoulive-3.png" width="80" height="80">
									<h6 class="black-text">Pune</h6>
								</div>
								<div class="col l1 div2-btn" id="div2-Kolkata">
									<img src="<?php echo base_url()?>assets/explore/images/frontend/wheredoyoulive-4.png" width="80" height="80">
									<h6 class="black-text">Kolkata</h6>
								</div>
								<div class="col l1 div2-btn" id="div2-Hyderabad">
									<img src="<?php echo base_url()?>assets/explore/images/frontend/wheredoyoulive-5.png" width="80" height="80">
									<h6 class="black-text">Hyderabad</h6>
								</div>
								<div class="col l1 div2-btn" id="div2-Ahmedabad">
									<img src="<?php echo base_url()?>assets/explore/images/frontend/wheredoyoulive-6.png" width="80" height="80">
									<h6 class="black-text">Ahmedabad</h6>
								</div>
								<div class="col l1 div2-btn" id="div2-Chandigarh">
									<img src="<?php echo base_url()?>assets/explore/images/frontend/wheredoyoulive-7.png" width="80" height="80">
									<h6 class="black-text">Chandigarh</h6>
								</div>
								<div class="col l1 div2-btn" id="div2-Bhubanehswar">
									<img src="<?php echo base_url()?>assets/explore/images/frontend/wheredoyoulive-8.png" width="80" height="80">
									<h6 class="black-text">Bhubanehswar</h6>
								</div>
																
								<div class="col s6 l1 div2-btn" id="div2-Jaipur">
									<img src="<?php echo base_url()?>assets/explore/images/frontend/wheredoyoulive-9.png" width="80" height="80">
									<h6 class="black-text">Jaipur</h6>
								</div>
								<div class="col s6 l1 div2-btn" id="div2-Gurgaon">
									<img src="<?php echo base_url()?>assets/explore/images/frontend/wheredoyoulive-10.png" width="80" height="80">
									<h6 class="black-text">Gurgaon</h6>
								</div>
								<div class="col s6 l1 div2-btn" id="div2-Kochi">
									<img src="<?php echo base_url()?>assets/explore/images/frontend/wheredoyoulive-11.png" width="80" height="80">
									<h6 class="black-text">Kochi</h6>
								</div>
								<div class="col s6 l1 div2-btn" id="div2-Luckhnow">
									<img src="<?php echo base_url()?>assets/explore/images/frontend/wheredoyoulive-12.png" width="80" height="80">
									<h6 class="black-text">Luckhnow</h6>
								</div>
								<div class="col s6 l1 div2-btn" id="div2-Surat">
									<img src="<?php echo base_url()?>assets/explore/images/frontend/wheredoyoulive-13.png" width="80" height="80">
									<h6 class="black-text">Surat</h6>
								</div>
								<div class="col s6 l1 div2-btn" id="div2-Banglore">
									<img src="<?php echo base_url()?>assets/explore/images/frontend/wheredoyoulive-14.png" width="80" height="80">
									<h6 class="black-text">Banglore</h6>
								</div>
								<div class="col s6 l1 div2-btn" id="div2-Chennai">
									<img src="<?php echo base_url()?>assets/explore/images/frontend/wheredoyoulive-15.png" width="80" height="80">
									<h6 class="black-text">Chennai</h6>
								</div>
								<div class="col s6 l1 div2-btn" id="div2-Other">
									<img src="<?php echo base_url()?>assets/explore/images/frontend/wheredoyoulive-16.png" width="80" height="80">
									<h6 class="black-text">Other City</h6>
								</div>
							</div>
						</div>
						<div id="div2-content-msg">
							<div class="center" id="div2-msg">
								
							</div>
						</div>
					</div>
				</div>
				<div class="col m2">
					<a class="btn-floating btn-large waves-effect waves-light green darken-3" id="div_back_2_p" style="margin-left:100px"><i class="mdi-hardware-keyboard-arrow-right"></i></a>
				</div>
				
			</div>
			<input type="hidden" id="div2-trigger"/>
		</div>
		
		<div id="div3" class="">
			<div class="row">
				<div class="col m3">
					<a class="btn-floating btn-large waves-effect waves-light blue darken-3" id="div_back_3"><i class="mdi-hardware-keyboard-arrow-left"></i></a>
				</div>
				<div class="col s12 m6" style="margin-top:-100px">
					<div class="card-panel" id="div3-strip" style="background-image:url('<?php echo base_url()?>assets/explore/images/frontend/blue-strip.jpg');" data-img-width="1600" data-img-height="78">
						<div id="div3-strip-text" class="center">Do You Have Any Course In Mind?</div>
					</div>
					<div class="card-panel" id="div3-content">
						<div class="row" id="div3-content-action">
							<div class="col s6 l6">
								<div class="row center" id="div3-yes-1">
									<a class="btn-floating div3-btn white" id="div3-yes">
										<img src="<?php echo base_url()?>assets/explore/images/frontend/doyouhaveanycourseinyourmindicon-02-01.png">
									</a>
									<h5 class="black-text">Yes</h5>											
								</div>											
							</div>
							<div class="col s6 l6">
								<div class="row center"id="div3-no-1">
									<a class="btn-floating div3-btn white" id="div3-no">
										<img src="<?php echo base_url()?>assets/explore/images/frontend/doyouhaveanycourseinyourmindicon-02-02.png">
									</a>
									<h5 class="black-text">No</h5>		
								</div>	
							</div>
						</div>	
						<div id="div3-content-msg">
							<div class="center" id="div3-msg">
							
							</div>
						</div>
					</div>
				</div>
				<div class="col m3"><a class="btn-floating btn-large waves-effect waves-light blue darken-3" id="div_back_3_p" style="margin-left:100px"><i class="mdi-hardware-keyboard-arrow-right"></i></a></div>
			</div>
			<input type="hidden" id="div3-trigger"/>
		</div>
		<!--Education-->
		<div id="div15" class="" style="margin-top:100px"> 
			<div class="row">
				<div class="col m3">
					<a class="btn-floating btn-large waves-effect waves-light blue darken-3" id="div_back_15" style="margin-top: 150px;"><i class="mdi-hardware-keyboard-arrow-left" ></i></a>
				</div>
				<div class="col s12 m6">
					<div class="card-panel" id="div4-strip" style="background-image:url('<?php echo base_url()?>assets/explore/images/frontend/blue-strip.jpg');" data-img-width="1600" data-img-height="78">
						<div id="div4-strip-text" class="center">Please Select Education</div>
					</div>
					<div class="card-panel" id="div4-content">
						<div id="div15-content-action">
							<div class="row center">
								
									<img src="<?php echo base_url()?>assets/explore/images/frontend/plese-select-courc-icon-01.png" width="111" height="111">
							
							</div>
							<div class="row center">
								<select class="browser-default black-text" id="select_education">
								
									<?php //foreach($mst_type as $crs_mstr){ ?>
									<!--option value="<?php //echo $crs_mstr->id .'-'.$crs_mstr->name; ?>"><?php //echo $crs_mstr->name; ?></option-->
									<?php// } ?>	
									<!-- <option value="1-Scc">Scc</option>
									<option value="2-Hsc">Hsc</option>	
									<option value="3-Diploma">Diploma</option>	
									<option value="4-Graduate">Graduate</option>
									<option value="5-Graduate">Open</option> -->
									<option value="">Select Education</option>
									<?php foreach($eligibility as $crs_mstr){ ?>
									<option value="<?php echo '1'.'-'.$crs_mstr->id.'-'.$crs_mstr->name; ?>"><?php echo $crs_mstr->name; ?></option>
									<?php }  ?>	
									<!-- <option value="1-Scc">Scc</option>
									<option value="1-Hsc">Hsc</option>	
									<option value="1-Diploma">Diploma</option>	
									<option value="4-Graduate">Graduate</option>	
									<option value="0-open">open</option> -->										
								</select>	
							</div>
							<div class="row center validation_msg" id="div15-validation"></div>
							<br/>
							<div class="row center">
								<a class="btn waves-effect waves-light blue div15-btn">Next</a>
							</div>
						</div>
						<div id="div15-content-msg">
							<div class="center" id="div4-msg-3">
							
							</div>
						</div>
					</div>
				</div>
				<div class="col m3"><a class="btn-floating btn-large waves-effect waves-light blue darken-3" id="div_back_15_p" style="margin-left:100px;margin-top:150px"><i class="mdi-hardware-keyboard-arrow-right"></i></a></div>
			</div>
			<input type="hidden" id="div15-trigger"/>
		</div>
		<!--EndEducation-->
		<!--type-->
	
		<div id="div14" class="" style="margin-top:100px"> 
			<div class="row">
				<div class="col m3">
					<a class="btn-floating btn-large waves-effect waves-light blue darken-3" id="div_back_14" style="margin-top:150px"><i class="mdi-hardware-keyboard-arrow-left"></i></a>
				</div>
				<div class="col s12 m6">
					<div class="card-panel" id="div4-strip" style="background-image:url('<?php echo base_url()?>assets/explore/images/frontend/blue-strip.jpg');" data-img-width="1600" data-img-height="78">
						<div id="div4-strip-text" class="center">Please Select type</div>
					</div>
					<div class="card-panel" id="div4-content">
						<div id="div14-content-action">
							<div class="row center">
								
									<img src="<?php echo base_url()?>assets/explore/images/frontend/plese-select-courc-icon-01.png" width="111" height="111">
							
							</div>
							<div class="row center">
								<select class="browser-default black-text" id="select_type">
									<option value="">Select Type</option>
									<?php foreach($mst_type as $crs_mstr){ ?>
									<option value="<?php echo $crs_mstr->id .'-'.$crs_mstr->name; ?>"><?php echo $crs_mstr->name; ?></option>
									<?php } ?>									
								</select>	
							</div>
							<div class="row center validation_msg" id="div14-validation"></div>
							<br/>
							<div class="row center">
								<a class="btn waves-effect waves-light blue div14-btn">Next</a>
							</div>
						</div>
						<div id="div14-content-msg">
							<div class="center" id="div4-msg-2">
							
							</div>
						</div>
					</div>
				</div>
				<div class="col m3"><a class="btn-floating btn-large waves-effect waves-light blue darken-3" id="div_back_14_p" style="margin-left:100px;margin-top:150px"><i class="mdi-hardware-keyboard-arrow-right"></i></a></div>
			</div>
			<input type="hidden" id="div14-trigger"/>
		</div>
		<!--End type-->
		<!--End-->
		<!--Stream-->
		<div id="div13" class="" style="margin-top:100px"> 
			<div class="row">
				<div class="col m3">
				<a class="btn-floating btn-large waves-effect waves-light blue darken-3" id="div_back_13" style="margin-top:150px"><i class="mdi-hardware-keyboard-arrow-left"></i></a>
				</div>
				<div class="col s12 m6">
					<div class="card-panel" id="div4-strip" style="background-image:url('<?php echo base_url()?>assets/explore/images/frontend/blue-strip.jpg');" data-img-width="1600" data-img-height="78">
						<div id="div4-strip-text" class="center">Please Select Stream</div>
					</div>
					<div class="card-panel" id="div13-content" style="margin-top: -15px;height: 350px">
						<div id="div13-content-action">
							<div class="row center">
								
									<img src="<?php echo base_url()?>assets/explore/images/frontend/plese-select-courc-icon-01.png" width="111" height="111">
							
							</div>
							<div class="row center">
								<select class="browser-default black-text" id="select_stream">
									<option value="">Select Stream</option>
									<?php foreach($course_master as $crs_mstr){ ?>
									<option value="<?php echo $crs_mstr->id .'-'.$crs_mstr->name; ?>"><?php echo $crs_mstr->name; ?></option>
									<?php } ?>									
								</select>	
							</div>
							<div class="row center validation_msg" id="div13-validation"></div>
							<br/>
							<div class="row center">
								<a class="btn waves-effect waves-light blue div13-btn">Next</a>
							</div>
						</div>
						<div id="div13-content-msg" >
							<div class="center" id="div4-msg-1">
							
							</div>
						</div>
					</div>
				</div>
				<div class="col m3"><a class="btn-floating btn-large waves-effect waves-light blue darken-3" id="div_back_13_p" style="margin-left:100px;margin-top:150px"><i class="mdi-hardware-keyboard-arrow-right"></i></a></div>
			</div>
			<input type="hidden" id="div13-trigger"/>
		</div>
		<!--End Stream-->


		<div id="div4" class="" style="margin-top:9%">
			<div class="row">
				<div class="col m3">
					<a class="btn-floating btn-large waves-effect waves-light blue darken-3" id="div_back_4" style="margin-top:150px"><i class="mdi-hardware-keyboard-arrow-left"></i></a>
				</div>
				<div class="col s12 m6">
					<div class="card-panel" id="div4-strip" style="background-image:url('<?php echo base_url()?>assets/explore/images/frontend/blue-strip.jpg');" data-img-width="1600" data-img-height="78">
						<div id="div4-strip-text" class="center">Please Select Your Course</div>
					</div>
					<div class="card-panel" id="div4-content">
						<div id="div4-content-action">
							<div class="row center">
								
									<img src="<?php echo base_url()?>assets/explore/images/frontend/plese-select-courc-icon-01.png" width="111" height="111">
							
							</div>
							<div class="row center">
								<select class="browser-default black-text" id="select_course">
									<!-- <option value="">Course</option>
									<?php foreach($course_master as $crs_mstr){ ?>
									<option value="<?php echo $crs_mstr->id .'-'.$crs_mstr->name; ?>"><?php echo $crs_mstr->name; ?></option>
									<?php } ?>	 -->								
								</select>	
							</div>
							<div class="row center validation_msg" id="div4-validation"></div>
							<br/>
							<div class="row center">
								<a class="btn waves-effect waves-light blue div4-btn">Next</a>
							</div>
						</div>
						<div id="div4-content-msg">
							<div class="center" id="div4-msg">
							
							</div>
						</div>
					</div>
				</div>
				<div class="col m3"><a class="btn-floating btn-large waves-effect waves-light blue darken-3" id="div_back_4_p" style="margin-left:100px;margin-top:150px"><i class="mdi-hardware-keyboard-arrow-right"></i></a></div>
			</div>
			<input type="hidden" id="div4-trigger"/>
		</div>
		<div id="div5" class="">
			<div class="row">
				<div class="col m2">
					<a class="btn-floating btn-large waves-effect waves-light blue darken-3" id="div_back_5" style="margin-top:150px"><i class="mdi-hardware-keyboard-arrow-left"></i></a>
				</div>
				<div class="col s12 m8">
					<div class="card-panel" id="div5-strip" style="background-image:url('<?php echo base_url()?>assets/explore/images/frontend/blue-strip.jpg');" data-img-width="1600" data-img-height="78">
						<div id="div5-strip-text" class="center">Where You Want To Study?</div>
					</div>
					<div class="card-panel" id="div5-content">
						<div id="div5-content-action">
							<div class="row center">									
								<div class="col l1 div5-btn-1" id="div5-Delhi">
									<img src="<?php echo base_url()?>assets/explore/images/frontend/Whereyouwanttostudy-1.png" width="80" height="80">
									<h6 class="black-text">Delhi</h6>	
								</div>
								<div class="col l1 div5-btn-1" id="div5-Mumbai">
									<img src="<?php echo base_url()?>assets/explore/images/frontend/Whereyouwanttostudy-2.png" width="80" height="80">
									<h6 class="black-text">Mumbai</h6>
								</div>
								<div class="col l1 div5-btn-1" id="div5-Pune">
									<img src="<?php echo base_url()?>assets/explore/images/frontend/Whereyouwanttostudy-3.png" width="80" height="80">
									<h6 class="black-text">Pune</h6>
								</div>
								<div class="col l1 div5-btn-1" id="div5-Kolkata">
									<img src="<?php echo base_url()?>assets/explore/images/frontend/Whereyouwanttostudy-4.png" width="80" height="80">
									<h6 class="black-text">Kolkata</h6>
								</div>
								<div class="col l1 div5-btn-1" id="div5-Hyderabad">
									<img src="<?php echo base_url()?>assets/explore/images/frontend/Whereyouwanttostudy-5.png" width="80" height="80">
									<h6 class="black-text">Hyderabad</h6>
								</div>
								<div class="col l1 div5-btn-1" id="div5-Ahmedabad">
									<img src="<?php echo base_url()?>assets/explore/images/frontend/Whereyouwanttostudy-6.png" width="80" height="80">
									<h6 class="black-text">Ahmedabad</h6>
								</div>
								<div class="col l1 div5-btn-1" id="div5-Chandigarh">
									<img src="<?php echo base_url()?>assets/explore/images/frontend/Whereyouwanttostudy-7.png" width="80" height="80">
									<h6 class="black-text">Chandigarh</h6>
								</div>
								<div class="col l1 div5-btn-1" id="div5-Bhubanehswar">
									<img src="<?php echo base_url()?>assets/explore/images/frontend/Whereyouwanttostudy-8.png" width="80" height="80">
									<h6 class="black-text">Bhubanehswar</h6>
								</div>
																
								<div class="col l1 div5-btn-1" id="div5-Jaipur">
									<img src="<?php echo base_url()?>assets/explore/images/frontend/Whereyouwanttostudy-9.png" width="80" height="80">
									<h6 class="black-text">Jaipur</h6>
								</div>
								<div class="col l1 div5-btn-1" id="div5-Gurgaon">
									<img src="<?php echo base_url()?>assets/explore/images/frontend/Whereyouwanttostudy-10.png" width="80" height="80">
									<h6 class="black-text">Gurgaon</h6>
								</div>
								<div class="col l1 div5-btn-1" id="div5-Kochi">
									<img src="<?php echo base_url()?>assets/explore/images/frontend/Whereyouwanttostudy-11.png" width="80" height="80">
									<h6 class="black-text">Kochi</h6>
								</div>
								<div class="col l1 div5-btn-1" id="div5-Luckhnow">
									<img src="<?php echo base_url()?>assets/explore/images/frontend/Whereyouwanttostudy-12.png" width="80" height="80">
									<h6 class="black-text">Luckhnow</h6>
								</div>
								<div class="col l1 div5-btn-1" id="div5-Surat">
									<img src="<?php echo base_url()?>/explore/images/frontend/Whereyouwanttostudy-13.png" width="80" height="80">
									<h6 class="black-text">Surat</h6>
								</div>
								<div class="col l1 div5-btn-1" id="div5-Banglore">
									<img src="<?php echo base_url()?>/explore/images/frontend/Whereyouwanttostudy-14.png" width="80" height="80">
									<h6 class="black-text">Banglore</h6>
								</div>
								<div class="col l1 div5-btn-1" id="div5-Chennai">
									<img src="<?php echo base_url()?>/explore/images/frontend/Whereyouwanttostudy-15.png" width="80" height="80">
									<h6 class="black-text">Chennai</h6>
								</div>
								<div class="col l1 div5-btn-1" id="div5-Other">
									<img src="<?php echo base_url()?>assets/explore/images/frontend/Whereyouwanttostudy-16.png" width="80" height="80">
									<h6 class="black-text">Other City</h6>
								</div>
							</div>
							<div class="row center validation_msg" id="div5-validation"></div>
							<div class="row center">
								<a class="btn waves-effect waves-light blue div5-btn"  style="margin-top: -74px;">Next</a>
								<a class="btn-floating btn-large waves-effect waves-light blue"><p id="div5_count_location" style="margin-top: 0px; font-size:50px;margin-top: 9px;">0</p></a>
							</div>
						</div>
						<div id="div5-content-msg">
							<div class="center" id="div5-msg">
							
							</div>
						</div>
					</div>
				</div>
				<div class="col m2"><a class="btn-floating btn-large waves-effect waves-light blue darken-3" id="div_back_5_p" style="margin-left:100px;margin-top:150px"><i class="mdi-hardware-keyboard-arrow-right"></i></a></div>
			</div>
			<input type="hidden" id="div5-trigger"/>
		</div>
		<div id="div6" class="animated bounceInRight">
			<div class="row">
				<div class="col m3">
					<a class="btn-floating btn-large waves-effect waves-light green darken-3" id="div_back_6"><i class="mdi-hardware-keyboard-arrow-left"></i></a>
				</div>
				<div class="col s12 m6">
					<div class="card-panel" id="div6-strip" style="background-image:url('<?php echo base_url()?>assets/explore/images/frontend/green-strip.jpg');" data-img-width="1600" data-img-height="78">
						<div id="div6-strip-text" class="center">What's Your highest Qualification?</div>
					</div>
					<div class="card-panel" id="div6-content">
						<div id="div6-content-action">
							<div class="row center">
								<div class="col l2 div6-btn" id="div6-SSC">
									<img class="div6-content-img" src="<?php echo base_url()?>assets/explore/images/frontend/whats-your-highest-qualification-icons-01.png"/>
									<b>SSC</b>
								</div>
								<div class="col l2 div6-btn" id="div6-HSC">
									<img class="div6-content-img" src="<?php echo base_url()?>assets/explore/images/frontend/whats-your-highest-qualification-icons-02.png"/>
									<b>HSC</b>
								</div>
								<div class="col l2 div6-btn" id="div6-GRADUATION">
									<img class="div6-content-img" src="<?php echo base_url()?>assets/explore/images/frontend/whats-your-highest-qualification-icons-03.png"/>
									<b>Graduation</b>
								</div>
								<div class="col l2 div6-btn" id="div6-POST GRADUATION">
									<img class="div6-content-img" src="<?php echo base_url()?>assets/explore/images/frontend/whats-your-highest-qualification-icons-04.png"/>
									<b>Post Graduation</b>
								</div>
								<div class="col l2 div6-btn" id="div6-PHD">
									<img class="div6-content-img" src="<?php echo base_url()?>assets/explore/images/frontend/whats-your-highest-qualification-icons-05.png"/>
									<b>PHD</b>
								</div>							
							</div>
						</div>
						<div id="div6-content-msg">
							<div class="center" id="div6-msg">
							
							</div>
						</div>
					</div>
				</div>
				<div class="col m3">&nbsp;</div>
			</div>
			<input type="hidden" id="div6-trigger"/>
		</div>
		<div id="div7" class="animated bounceInRight">
			<div class="row">
				<div class="col m3">
					<a class="btn-floating btn-large waves-effect waves-light blue darken-3" id="div_back_7" style="margin-top:150px"><i class="mdi-hardware-keyboard-arrow-left"></i></a>
				</div>
				<div class="col s12 m6">
					<div class="card-panel" id="div7-strip" style="background-image:url('<?php echo base_url()?>assets/explore/images/frontend/blue-strip.jpg');" data-img-width="1600" data-img-height="78">
						<div id="div7-strip-text" class="center">Who Are You?</div> 
					</div>
					<div class="card-panel" id="div7-content">
						<div id="div7-content-action">
							<div class="row center">
								<div class="col s6 l6 div7-btn" id="div7-BOY">
									<img class="div7-content-img" src="<?php echo base_url()?>assets/explore/images/frontend/who-are-you-icon-01.png"/>
									<b><br/>Boy</b>
								</div>
								<div class="col s6 l6 div7-btn" id="div7-GIRL">
									<img class="div7-content-img" src="<?php echo base_url()?>assets/explore/images/frontend/who-are-you-icon-02.png"/>
									<b><br/>Girl</b>
								</div>				
							</div>
						</div>
						<div id="div7-content-msg">
							<div class="center" id="div7-msg">
							
							</div>
						</div>
					</div>
				</div>
				<div class="col m3">
					<a class="btn-floating btn-large waves-effect waves-light blue darken-3" id="div_back_7_p" style="margin-left:100px;margin-top:150px"><i class="mdi-hardware-keyboard-arrow-right"></i></a>
				</div>
			</div>
			<input type="hidden" id="div7-trigger"/>
		</div>
		<div id="div8" class="animated bounceInRight">
			<div class="row">
				<div class="col m3">
						<a class="btn-floating btn-large waves-effect waves-light blue darken-3" id="div_back_8" style="margin-top:94px"><i class="mdi-hardware-keyboard-arrow-left"></i></a>
				</div>
				<div class="col s12 m6">
					<div class="card-panel" id="div8-strip" style="background-image:url('<?php echo base_url()?>assets/explore/images/frontend/blue-strip.jpg');" data-img-width="1600" data-img-height="78">
						<div id="div8-strip-text" class="center">Ohh!! We Forget To Catch Your Name</div> 
					</div>
					<div class="card-panel" id="div8-content">
						<div id="div8-content-action">
							<div class="row center">
								<div class="col s2 l3">&nbsp;</div>
								<div class="input-field col s8 l6 black-text">
									<input id="first_name" type="text" placeholder="Your Name">
								</div>
								<div class="col s2 l3">&nbsp;</div>
							</div>
							<div class="row center validation_msg" id="div8-validation"></div>
							<div class="row center">
								<a class="btn waves-effect waves-light blue div8-btn">Next</a>
							</div>
						</div>
						<div id="div8-content-msg">
							<div class="center" id="div8-msg">
							
							</div>
						</div>
					</div>
					
				</div>
				<div class="col m3"><a class="btn-floating btn-large waves-effect waves-light blue darken-3" id="div_back_8_p" style="margin-left:100px;margin-top:94px"><i class="mdi-hardware-keyboard-arrow-right"></i></a></div>
			</div>
			<input type="hidden" id="div8-trigger"/>
		</div>
		<div id="div9" class="animated bounceInRight">
			<div class="row">
				<div class="col m3">
					<a class="btn-floating btn-large waves-effect waves-light blue darken-3" id="div_back_9" style="margin-top:200px"><i class="mdi-hardware-keyboard-arrow-left"></i></a>
				</div>
				<div class="col s12 m6">
					<div class="card-panel" id="div9-strip" style="background-image:url('<?php echo base_url()?>assets/explore/images/frontend/blue-strip.jpg');" data-img-width="1600" data-img-height="78">
						<div id="div9-strip-text" class="center">What's Your Age?</div> 
					</div>
					<div class="card-panel" id="div9-content">
						<div id="div9-content-action">
							<div class="row">
								<div class="col s12 l12">
									<div id="year_selector"></div>
								</div>	
							</div>	
							<div class="row center-align" style="margin-top:10px;">
								<div class="col s12 l5" id="months-div" style="display:none;">
									<div class="row">
										<div class="col s3 l3"><div class="month_select" id="month_1">JAN</div></div>
										<div class="col s3 l3"><div class="month_select" id="month_2">FEB</div></div>
										<div class="col s3 l3"><div class="month_select" id="month_3">MAR</div></div>
										<div class="col s3 l3"><div class="month_select" id="month_4">APR</div></div>
									</div>
									<div class="row">
										<div class="col s3 l3"><div class="month_select" id="month_5">MAY</div></div>
										<div class="col s3 l3"><div class="month_select" id="month_6">JUN</div></div>
										<div class="col s3 l3"><div class="month_select" id="month_7">JUL</div></div>
										<div class="col s3 l3"><div class="month_select" id="month_8">AUG</div></div>
									</div>
									<div class="row">
										<div class="col s3 l3"><div class="month_select" id="month_9">SEP</div></div>
										<div class="col s3 l3"><div class="month_select" id="month_10">OCT</div></div>
										<div class="col s3 l3"><div class="month_select" id="month_11">NOV</div></div>
										<div class="col s3 l3"><div class="month_select" id="month_12">DEC</div></div>
									</div>
								</div>
								<style>
								.day_select
								{
									display:none;
								}
								</style>
								<div class="col s12 l6" id="days-div" style="display:none;" style="height:-53px">
									<div class="row center-align" id="days-div-row" >
										<div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_1">1&nbsp;</div></div>
										<div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_2">2&nbsp;</div></div>
										<div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_3">3&nbsp;</div></div>
										<div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_4">4&nbsp;</div></div>
										<div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_5">5&nbsp;</div></div>
										<div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_6">6&nbsp;&nbsp;</div></div>
										<br/>
										<div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_7">7&nbsp;</div></div>
										<div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_8">8&nbsp;</div></div>
										<div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_9">9&nbsp;</div></div>
										<div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_10">10</div></div>
										<div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_11">11</div></div>
										<div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_12">12</div></div>
										<br/>
										<div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_13">13</div></div>
										<div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_14">14</div></div>
										<div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_15">15</div></div>
										<div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_16">16</div></div>
										<div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_17">17</div></div>
										<div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_18">18</div></div>
										<br/>
										<div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_19">19</div></div>
										<div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_20">20</div></div>
										<div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_21">21</div></div>
										<div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_22">22</div></div>
										<div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_23">23</div></div>
										<div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_24">24</div></div>
										<br/>
										<div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_25">25</div></div>
										<div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_26">26</div></div>
										<div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_27">27</div></div>
										<div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_28">28</div></div>
										<div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_29">29</div></div>
										<div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_30">30</div></div>
										<br/>
										<div class="col s2 sl2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_31">31</div></div>
									</div>
									
								</div>
							</div>
							<div class="row  center-align" id="finaldate-div" style="display:none">
								<div class="col s12 l8">
									<h1 class="center black-text" style="margin-top: -125px;margin-left: -183px;">
										<span id="year"></span>
										<span id="month"></span>
										<span id="day"></span>
									</h1>											
								</div>
								<div class="row center validation_msg" id="div9-validation"></div>								
								<div class="col s12 l4">
									<a class="btn waves-effect waves-light blue div9-btn">Next</a>
								</div>
							</div>
						</div>
						<div id="div9-content-msg">
							<div class="center" id="div9-msg">
							
							</div>
						</div>
					</div>
				</div>
				<div class="col m3">	<a class="btn-floating btn-large waves-effect waves-light blue darken-3" id="div_back_9_p" style="margin-left:100px;margin-top:200px"><i class="mdi-hardware-keyboard-arrow-right"></i></a></div>
			</div>
			<input type="hidden" id="div9-trigger"/>
		</div>
		<div id="div10" class="animated bounceInRight">
			<div class="row">
				<div class="col m3">
					<a class="btn-floating btn-large waves-effect waves-light green darken-3" id="div_back_10"><i class="mdi-hardware-keyboard-arrow-left"></i></a>
				</div>
				<div class="col s12 m6">
					<div class="card-panel" id="div10-strip" style="background-image:url('<?php echo base_url()?>assets/explore/images/frontend/green-strip.jpg');" data-img-width="1600" data-img-height="78">
						<div id="div10-strip-text" class="center">Are You Studying Currently?</div> 
					</div>
					<div class="card-panel" id="div10-content">
						<div id="div10-content-action">
							<div class="row">
								<div class="col s6 l6">
									<div class="row center">
										<a class="btn-floating div10-btn white" id="div10-yes">
											<img src="<?php echo base_url()?>assets/explore/images/frontend/are-yo-studying-currently-icon-01.png">
										</a>
										<h5 class="black-text">Yes</h5>											
									</div>											
								</div>
								<div class="col s6 l6">
									<div class="row center">
										<a class="btn-floating div10-btn white" id="div10-no">
											<img src="<?php echo base_url()?>assets/explore/images/frontend/are-yo-studying-currently-icon-02.png">
										</a>
										<h5 class="black-text">No</h5>		
									</div>	
								</div>	
							</div>	
						</div>
						<div id="div10-content-msg">
							<div class="center" id="div10-msg">
							
							</div>
						</div>						
					</div>
				</div>
				<div class="col m3">&nbsp;</div>
			</div>
			<input type="hidden" id="div10-trigger"/>
		</div>
		<div id="div11" class="animated bounceInRight">
			<div class="row">
				<div class="col m3">
					<a class="btn-floating btn-large waves-effect waves-light blue darken-3" id="div_back_11"><i class="mdi-hardware-keyboard-arrow-left"></i></a>
				</div>
				<div class="col s12 m6">
					<div class="card-panel" id="div11-strip" style="background-image:url('<?php echo base_url()?>assets/explore/images/frontend/blue-strip.jpg');" data-img-width="1600" data-img-height="78">
						<div id="div11-strip-text" class="center">What Course Are You Pursuing?</div>
					</div>
					<div class="card-panel" id="div11-content">
						<div id="div11-content-action">
							<div class="row center">
								
									<img src="<?php echo base_url()?>assets/explore/images/frontend/what-course-are-you-pursuing-icon-01.png" width="111" height="111">
							
							</div>
							<div class="row center">
								<select class="browser-default black-text" id="div11-course-select">
									<option value="">Course</option>
									<?php foreach($course_master as $crs_mstr){ ?>
									<option value="<?php echo $crs_mstr->id .'-'.$crs_mstr->name; ?>"><?php echo $crs_mstr->name; ?></option>
									<?php } ?>
								</select>	
							</div>
							<div class="row center validation_msg" id="div11-validation"></div>
							<div class="row center">
								<a class="btn waves-effect waves-light blue div11-btn">Next</a>
							</div>
						</div>
						<div id="div11-content-msg">
							<div class="center" id="div11-msg">
							
							</div>
						</div>
					</div>
				</div>
				<div class="col m3">&nbsp;</div>
			</div>
			<input type="hidden" id="div11-trigger"/>
		</div>
		<div id="div12" class="animated bounceInRight">
			<div class="row">
				<div class="col m3">
					<a class="btn-floating btn-large waves-effect waves-light green darken-3" id="div_back_12" style="margin-top:200px"><i class="mdi-hardware-keyboard-arrow-left"></i></a>
				</div>
				<div class="col s12 m6">
					<div class="card-panel" id="div12-strip" style="background-image:url('<?php echo base_url()?>assets/explore/images/frontend/green-strip.jpg');" data-img-width="1600" data-img-height="78">
						<div id="div12-strip-text" class="center">Contact</div>
					</div>
					<div class="card-panel" id="div12-content">	
						<div id="div12-content-action">
							<div class="row center">
								<div class="row center black-text">
									<div class="col l7">
										<div class="row" id="phone-row">
											<div id="errorMsg" style="color:red"></div>
											<?php $udata=$this->session->userdata('logged_in')?>
											<div class="input-field col l12">
												<?php if($udata!=''){?>
												<input id="email" type="text" placeholder="Email" value="<?php echo $udata['email']?>" readonly><br/>
												<?php }else{?>
												<input id="email" type="text" placeholder="Email" value=""><br/>
												<?php } ?>
												<!-- <input id="email" type="text" placeholder="Email"><br/> -->

											</div>										
										</div>
									</div>	
										<?php if($this->session->userdata('logged_in')==''){?>
										<div class="col l7" style="margin-top:-175px">
										<div class="row" id="phone-row">
											<div id="errorMsg" style="color:red"></div>
											<div class="input-field col l12">
												
												
												<input id="password" type="password" placeholder="Password">
												<input id="rep_password" type="password" placeholder="Repeat Password">											
											</div>										
										</div>
									</div>							
									<div class="col s12 l5" style="margin-top:-218px">
										<div class="row">
											<div class="input-field col s10 l10">											
												<div id="mobilenumberdiv">
													<span id="mobilenumber">
														<input type="tel" name="" id="mobile_num" placeholder="Mobile"/>
													</span>													
												</div>												
											</div>
											<div class="col s2 l2">											
												<i class="mdi-content-backspace right" style="margin-left:1px;font-size: 35px;margin-top: 18px;" id="phone_delete_btn"></i>												
											</div>
										</div>
										<div class="row">
											<div class="col s12 l12" id="phonenumber-div">
												<div class="row">
													<div class="col s3 l3 green phonenumber" id="phone_7">7</div>
													<div class="col s3 l3 green phonenumber" id="phone_8">8</div>
													<div class="col s3 l3 green phonenumber" id="phone_9">9</div>
												</div>
												<div class="row">
													<div class="col s3 l3 green phonenumber" id="phone_4">4</div>
													<div class="col s3 l3 green phonenumber" id="phone_5">5</div>
													<div class="col s3 l3 green phonenumber" id="phone_6">6</div>
												</div>
												<div class="row">
													<div class="col s3 l3 green phonenumber" id="phone_1">1</div>
													<div class="col s3 l3 green phonenumber" id="phone_2">2</div>
													<div class="col s3 l3 green phonenumber" id="phone_3">3</div>
												</div>
												<div class="row">
													<div class="col s3 l3 green phonenumber" id="phone_*">*</div>
													<div class="col s3 l3 green phonenumber" id="phone_0">0</div>
													<div class="col s3 l3 green phonenumber" id="phone_#">#</div>
												</div>
											</div>
										</div>
									</div>
									<?php }?>
								</div>
							</div>
							<div class="row center">
								<button type="button" class="btn waves-effect waves-light green div12-btn" style="margin-top:-100px">Done</button>
							</div>
						</div>
						<div id="div12-content-msg">
							<div class="center" id="div12-msg">
							
							</div>
						</div>
					</div>
				</div>
				<div class="col m3">&nbsp;</div>
			</div>
			<input type="hidden" id="div12-trigger"/>
		</div>
		<!--div id="div12" class="animated bounceInRight">
			<div class="row">
				<div class="col m3">
					<a class="btn-floating btn-large waves-effect waves-light green darken-3" id="div_back_12"><i class="mdi-hardware-keyboard-arrow-left"></i></a>
				</div>
				<div class="col s12 m6">
					<div class="card-panel" id="div12-strip" style="background-image:url('<?php echo base_url()?>assets/explore/images/frontend/green-strip.jpg');" data-img-width="1600" data-img-height="78">
						<div id="div12-strip-text" class="center">Contact</div>
					</div>
					<div class="card-panel" id="div12-content">	
						<div id="div12-content-action">
							<div class="row center">
								<div class="row center black-text">
									<div class="col l7">
										<div class="row" id="phone-row">
											<div id="errorMsg" style="color:red"></div>
											<div class="input-field col l12">
												<input id="email" type="text" placeholder="Email"><br/>
												<input id="password" type="password" placeholder="Password">
												<input id="rep_password" type="password" placeholder="Repeat Password">											
											</div>										
										</div>
									</div>								
									<div class="col s12 l5">
										<div class="row">
											<div class="input-field col s10 l10">											
												<div id="mobilenumberdiv">
													<span id="mobilenumber">
														<input type="tel" name="" id="mobile_num" placeholder="Mobile"/>
													</span>													
												</div>												
											</div>
											<div class="col s2 l2">											
												<i class="mdi-content-backspace right" style="margin-left:1px;font-size: 35px;margin-top: 18px;" id="phone_delete_btn"></i>												
											</div>
										</div>
										<div class="row">
											<div class="col s12 l12" id="phonenumber-div">
												<div class="row">
													<div class="col s3 l3 green phonenumber" id="phone_7">7</div>
													<div class="col s3 l3 green phonenumber" id="phone_8">8</div>
													<div class="col s3 l3 green phonenumber" id="phone_9">9</div>
												</div>
												<div class="row">
													<div class="col s3 l3 green phonenumber" id="phone_4">4</div>
													<div class="col s3 l3 green phonenumber" id="phone_5">5</div>
													<div class="col s3 l3 green phonenumber" id="phone_6">6</div>
												</div>
												<div class="row">
													<div class="col s3 l3 green phonenumber" id="phone_1">1</div>
													<div class="col s3 l3 green phonenumber" id="phone_2">2</div>
													<div class="col s3 l3 green phonenumber" id="phone_3">3</div>
												</div>
												<div class="row">
													<div class="col s3 l3 green phonenumber" id="phone_*">*</div>
													<div class="col s3 l3 green phonenumber" id="phone_0">0</div>
													<div class="col s3 l3 green phonenumber" id="phone_#">#</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row center">
								<button type="button" class="btn waves-effect waves-light green div12-btn" style="margin-top:-100px">Done</button>
							</div>
						</div>
						<div id="div12-content-msg">
							<div class="center" id="div12-msg">
							
							</div>
						</div>
					</div>
				</div>
				<div class="col m3">&nbsp;</div>
			</div>
			<input type="hidden" id="div12-trigger"/>
		</div-->
<!--
		<footer>
			<div class="row row1 !important col m12 row_padding" style="margin-bottom:0px">
						 
			</div>         
		</footer>
-->

<div class="template_section" style="background-color: #fff;">
    <body id="top" class="has-header-search">



        

</div>


  <footer class="footer footer-four" style="padding:0px 0px 0px; ">
            <div class="primary-footer brand-bg text-center">
                <div class="container">

                  <ul class="social-link tt-animate ltr mt-20">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-tumblr"></i></a></li>
                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                    <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                    <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                    <li><a href="#"><i class="fa fa-rss"></i></a></li>
                  </ul>

                  <hr class="mt-15">

                  <div class="row">
                      <div class="col-md-12">
                          <div class="footer-logo">
                            <img src="<?php echo base_url();?>assets/img/logo-01.png" alt="">
                          </div>

                          <span class="copy-text" style="color:white"> &copy; 2017-18  | <a href="#">Hello Admission</a> &nbsp; | &nbsp;  All Rights Reserved &nbsp; | &nbsp;  Designed By <a href="http://infinite1.in"><img style="width: 100px;padding-bottom: 2px;" src="<?php echo base_url() ?>assets/img/infinite1-logo-128X80-01.png" alt=""></a></span>
                          <p></p>
                          <div class="footer-intro">
                            <p></p>
                          </div>
                    </div><!-- /.col-md-12 -->
                  </div><!-- /.row -->
                </div><!-- /.container -->
            </div><!-- /.primary-footer -->

            <div class="secondary-footer brand-bg darken-2 text-center">
                <div class="container">
                    <ul>
                      <li><a href="<?php echo base_url()?>home">Home</a></li>
                      <li><a href="<?php echo base_url()?>about-us">About us</a></li>
                      <li><a href="<?php echo base_url()?>services">Services</a></li>
                      <li><a href="<?php echo base_url()?>term-condition">Terrms & Condition</a></li>
                      <li><a href="<?php echo base_url()?>contact">Contact us</a></li>
                    </ul>
                </div><!-- /.container -->
            </div><!-- /.secondary-footer -->
        </footer>



	</div>
  <script type="text/javascript">
		$(document).ready(function(){
				$('#select_education').change(function(){
	var cate_id_1= $(this).val();
	var cate_id  = $(this).val().split('-');
	//alert(cate_id_1);
	console.log(cate_id);
	$('#select_stream').empty();
	$.ajax({
		type: "POST",
		url: "<?php echo base_url('explore/education_category'); ?>",
		data:{cate_id:cate_id[1]},
		success: function(response){
			var res = JSON.parse(response);
			$('#select_stream').append('<option value="">Choose stream</option>');
			$.each(res, function (i, itm) {
				// $('#select_course').append($('<option>', { 
				// 	value: itm.id,
				// 	text : itm.name 
				// }));
			$('#select_stream').append('<option value="'+itm.id+'-'+itm.name+'">'+itm.name+'</option>');

			});
		}
	});
});

			$('#select_stream').change(function(){
	var streamid = $(this).val();
	//alert(streamid);
	$('#select_course').empty();
	$.ajax({
		type: "POST",
		url: "<?php echo base_url('explore/stream_program'); ?>",
		data:{streamid:streamid},
		success: function(response){
			var res = JSON.parse(response);
			$('#select_course').append('<option value="">Choose cource</option>');
			$.each(res, function (i, itm) {
				// $('#select_course').append($('<option>', { 
				// 	value: itm.id,
				// 	text : itm.name 
				// }));
			$('#select_course').append('<option value="'+itm.id+'-'+itm.name+'">'+itm.name+'</option>');

			});
		}
	});
});
			$('.button-collapse').sideNav({edge:'left'});
			$('#div2').hide();
			$('#div3').hide();
			$('#div4').hide();
			$('#div5').hide();
			$('#div6').hide();
			$('#div7').hide();
			$('#div8').hide();
			$('#div9').hide();
			$('#div10').hide();
			$('#div11').hide();
			$('#div12').hide();
			$('#div13').hide();
			$('#div14').hide();
			$('#div15').hide();
			//~ if(jQuery.browser.mobile)
			//~ {
			   
			   //~ $('.BackgroundImg').addClass('noBackground');
			   //~ $('.noBackground').removeClass('BackgroundImg');
			   //~ $('.noBackground').css('background-color','#47494b');
			//~ }
			//~ else
			//~ {
			   $('.BackgroundImg').css('background-image','url("<?php echo base_url()?>assets/explore/images/frontend/howareyoubg.jpg")');
			//~ }
		});
		
		jQuery("#div1 #div1-content").click(function(){
			jQuery(".template_section").hide();
		});
		
	</script>
	<script type="text/javascript">
	// 	$(document).ready(function(){
	// 		$('.button-collapse').sideNav({edge:'left'});
	// 		$('#div2').hide();
	// 		$('#div3').hide();
	// 		$('#div4').hide();
	// 		$('#div5').hide();
	// 		$('#div6').hide();
	// 		$('#div7').hide();
	// 		$('#div8').hide();
	// 		$('#div9').hide();
	// 		$('#div10').hide();
	// 		$('#div11').hide();
	// 		$('#div12').hide();
	// 		//~ if(jQuery.browser.mobile)
	// 		//~ {
			   
	// 		   //~ $('.BackgroundImg').addClass('noBackground');
	// 		   //~ $('.noBackground').removeClass('BackgroundImg');
	// 		   //~ $('.noBackground').css('background-color','#47494b');
	// 		//~ }
	// 		//~ else
	// 		//~ {
	// 		   $('.BackgroundImg').css('background-image','url("<?php echo base_url()?>assets/explore/images/frontend/howareyoubg.jpg")');
	// 		//~ }
	// 	});
		
	// 	jQuery("#div1 #div1-content").click(function(){
	// 		jQuery(".template_section").hide();
	// 	});
		
	// </script>
	<script type="text/javascript" src="<?php echo base_url('assets/explore/js/frontend/frontend_quiz.js');?>"></script>
	
	    <script src="<?php echo base_url('assets_new/js/jquery-2.1.3.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets_new/bootstrap/js/bootstrap.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets_new/materialize/js/materialize.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets_new/js/jquery.easing.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets_new/js/jquery.sticky.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets_new/js/smoothscroll.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets_new/js/imagesloaded.js'); ?>"></script>
        <script src="<?php echo base_url('assets_new/js/jquery.stellar.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets_new/js/jquery.inview.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets_new/js/jquery.shuffle.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets_new/js/menuzord.js'); ?>"></script>
        <script src="<?php echo base_url('assets_new/js/bootstrap-tabcollapse.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets_new/owl.carousel/owl.carousel.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets_new/flexSlider/jquery.flexslider-min.js'); ?>"></script>
        <script src="<?php echo base_url('assets_new/magnific-popup/jquery.magnific-popup.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets_new/js/scripts.js'); ?>"></script>
		
		
        
        
    </body>
 </html>
