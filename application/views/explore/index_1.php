<!DOCTYPE html>
  <html>
    <head>
		
		
		<meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="materialize is a material design based mutipurpose responsive template">
        <meta name="keywords" content="material design, card style, material template, portfolio, corporate, business, creative, agency">
        <meta name="author" content="trendytheme.net">
     
     
           <!--  favicon -->
        <link rel="shortcut icon" href="<?php echo base_url('assets/img/fav.png' ); ?>">
        <!--  apple-touch-icon -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url('assets/img/ico/apple-touch-icon-144-precomposed.png'); ?>">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/img/ico/apple-touch-icon-114-precomposed.png'); ?>">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/img/ico/apple-touch-icon-72-precomposed.png'); ?>">
        <link rel="apple-touch-icon-precomposed" href="assets/img/ico/apple-touch-icon-57-precomposed.png'); ?>">

		<link href="<?php echo base_url('assets/explore/css/animate.css');?>" type="text/css" rel="stylesheet">
		
		<link href="<?php echo base_url('assets/explore/css/ion.rangeSlider.css');?>" type="text/css" rel="stylesheet"/>
		<link href="<?php echo base_url('assets/explore/css/ion.rangeSlider.skinHTML5.css');?>" type="text/css" rel="stylesheet"/>
		
		<link href="<?php echo base_url('assets/explore/css/frontend/quiz_mycustom.css');?>" type="text/css" rel="stylesheet">
		<link href="<?php echo base_url('assets/explore/css/frontend/quiz_mycustom_media.css');?>" type="text/css" rel="stylesheet">
		
		<link rel="stylesheet" href="<?php echo base_url('assets/css/matexcss/style.minified.css');?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/css/matexcss/main.css');?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/css/matexcss/style.html');?>">
          <link rel="stylesheet" href="<?php echo base_url('assets/css/matexcss/tabs.css');?>">
		
		<script type="text/javascript" src="<?php echo base_url('assets/explore/js/jquery-2.1.4.min.js');?>"></script>
	
		<script type="text/javascript" src="<?php echo base_url('assets/explore/js/ion.rangeSlider.js');?>"></script>
		
		<script type="text/javascript" src="<?php echo base_url('assets/explore/js/frontend/mobile_detect.js');?>"></script>
		
		
    </head>
<style>
#mobilenumberdiv {
    background-color: #fff;
}
.card-panel {
    padding: 40px !important;
}
.icon-tab .nav>li>a {
    width: 100%;
    height: 100%;
}
.flex-control-thumbs{
	display:none;
}
.flex-direction-nav{
	display:none;
}
.no-gutter>[class^="col"] {
    padding-left: 10px;
    padding-bottom: 10px;
}
#ui-id-1{
    background: #fff;
    width: 43%;
    padding: 8px;
	max-height: 300px;
    overflow-y: auto;
}
.ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active {
	padding: 7px;
    background: #03205c;
    color: #fff;
}
.hover-ripple {
    background-color: #2f2e2d7a;
    border-top-right-radius: 100px;
    bottom: 0;
    height: 100px;
    left: 0;
    position: absolute;
    -webkit-transform: scale(0);
    -moz-transform: scale(0);
    -ms-transform: scale(0);
    -o-transform: scale(0);
    transform: scale(0);
    -webkit-transform-origin: left bottom 0;
    -moz-transform-origin: left bottom 0;
    -ms-transform-origin: left bottom 0;
    -o-transform-origin: left bottom 0;
    transform-origin: left bottom 0;
    -webkit-transition: all 0.4s cubic-bezier(0.4, 0, 0.2, 1) 0s;
    -moz-transition: all 0.4s cubic-bezier(0.4, 0, 0.2, 1) 0s;
    -o-transition: all 0.4s cubic-bezier(0.4, 0, 0.2, 1) 0s;
    transition: all 0.4s cubic-bezier(0.4, 0, 0.2, 1) 0s;
    width: 100px;
}
</style>
<body>
	<input type="text" id="previous_div" value=""/>
	<input type="hidden" id="question1" value="yes"/>
	<input type="hidden" id="question2" value="yes"/>
	
	<form id="frontend_quiz" method="POST" action="<?php echo base_url('home/explore'); ?>">
		<input type="hidden" name="45" id="input_1" value=""/>
		<input type="hidden" name="12" id="input_2" value=""/>
		<input type="hidden" name="44" id="input_3" value=""/>
		<input type="hidden" name="38" id="input_4" value=""/>
		<input type="hidden" name="48" id="input_13" value=""/>
		<input type="hidden" name="49" id="input_14" value=""/>
		
		<input type="hidden" name="50" id="input_15" value=""/>
		<input type="hidden" name="40" id="input_5" value=""/>
		<input type="hidden" name="39" id="input_6" value=""/>
		<input type="hidden" name="10" id="input_7" value=""/>
		<input type="hidden" name="1" id="input_8" value=""/>
		<input type="hidden" name="5" id="input_9_1" value=""/>
		<input type="hidden" name="6" id="input_9_2" value=""/>
		<input type="hidden" name="7" id="input_9_3"  value=""/>
		<input type="hidden" name="41" id="input_10" value=""/>
		<input type="hidden" name="43" id="input_11" value=""/>
		<input type="hidden" name="4" id="input_12_1" value=""/>
		<input type="hidden" name="password" id="input_12_2" value=""/>
		<input type="hidden" name="rep_password" id="input_12_3" value=""/>
		<input type="hidden" name="8" id="input_12_4" value=""/>
	</form>
	
	<div class="BackgroundImg">
    <header id="header" class="page-topbar">
        <!--<div class="navbar-fixed" >
            <nav class="navbar-color" style="position: relative">
                <div class="nav-wrapper">-->
					
					<div class="container">

                    <div class="search-wrapper">
                        <!-- Modal Search Form -->
                        <i class="search-close material-icons">&#xE5CD;</i>
                        <div class="search-form-wrapper">
                            <form action="#" class="white-form">
                                <div class="input-field">
                                    <input type="text" name="search" id="search">
                                    <label for="search" class="">Search Here...</label>
                                </div>
                                <button class="btn pink search-button waves-effect waves-light" type="submit"><i class="material-icons">&#xE8B6;</i></button>
                                
                            </form>
                        </div>
                    </div><!-- /.search-wrapper -->
                
                    <div  class="menuzord">

                        <!--logo start-->

                        <!-- <a href="index.html" class="">
									<a href="<?php echo base_url('home/index'); ?>" class="LogoMargin">
										<span class="red-text">Hello</span>
										<span class="black-text">Admission</span>
									</a>
                        </a> -->
                        <!--logo end-->

                        <!--mega menu start-->

                    <!--     <ul class="menuzord-menu pull-right">

                            
                            <li>
								<a onclick="GLogin();" class="black-text">Google</a>&nbsp;&nbsp;
                            </li>
                            
                            <li>
								<a onclick="FBLogin();" class="black-text">Facebook</a>&nbsp;&nbsp;		
                            </li>
                            
                            <li class="active">
									<a href="<?php echo base_url('explore/index'); ?>" class="black-text">Explore</a>&nbsp;&nbsp;
                            </li>
                            
                            
                            <li class="">
									<a href="<?php echo base_url('login/index'); ?>" class="black-text">Login</a>&nbsp;&nbsp;
                            </li>
                            
                            <li class="">
									<a href="<?php echo base_url('login/student_register'); ?>" class="black-text">SignUp</a>&nbsp;&nbsp;
                            </li>

                        </ul>
 -->
                     

                    </div>
    
    
    </div>
           <!-- </nav>
        </div>-->
    </header>

		<div id="div1" class="animated bounceInRight">
			<div class="row">
				<div class="col m3">&nbsp;&nbsp;<a class="swapbutton btn indigo" id="explore"><i class="material-icons left">&#xE87A;</i>Explore</a><br/><br/>&nbsp;&nbsp;<a class="swapbutton btn blue div15-btn" id="search"><i class="material-icons left">&#xE8B6;</i>Search</a></div>
				<div class="col s12 m6" style="margin-top:-97px">
					<div class="card-panel explore" id="div1-strip" style="background-image:url('<?php echo base_url()?>assets/explore/images/frontend/blue-strip.jpg');">
						<div id="div1-strip-text" class="center">How are You?</div>
					</div>
					<div class="card-panel explore" id="div1-content" style="padding: 40px;">
						<div id="div1-content-action">
							<div class="row">
								<div class="col s6 l6">
									<div class="row center" id="div_good">
										<a class="btn-floating div1-btn" id="div1-good">
											<img src="<?php echo base_url()?>assets/explore/images/frontend/howareyouicon1.png">
										</a>
										<h5 class="black-text">Good</h5>											
									</div>											
								</div>
								<div class="col s6 l6">
									<div class="row center" id="div_not_good">
										<a class="btn-floating div1-btn" id="div1-bad">
											<img src="<?php echo base_url()?>assets/explore/images/frontend/howareyouicon2.png">
										</a>
										<h5 class="black-text">Naah</h5>		
									</div>	
								</div>								
							</div>
						</div>
						<div id="div1-content-msg">
							<div class="center" id="div1-msg">
								
							</div>
						</div>
					</div>
					<div class="card-panel search" id="search-head-div" style="background-image:url('<?php echo base_url()?>assets/explore/images/frontend/blue-strip.jpg');display:none;">
						<div id="div1-strip-text" class="center">Search</div>
					</div>
					<div class="card-panel search" id="search-div" style="display:none;">
						<div id="div1-content-action">
							<div class="row">
								<div class="col s6 l12">
									<div class="widget widget_search">
									<form role="search" method="get" class="search-form">
									  <input type="text" class="form-control" value="" name="s" id="search_text" placeholder="Enter Cources Name">
									  <button type="submit"><i class="fa fa-search"></i></button>
									</form>
									<div class="widget widget_search" >
									<ul class="dropdown" style="right: auto; " id="search_result">
											<li>
											  
											</li>
										   
										</ul>
									</div>
									
								  </div>								
								</div>							
							</div>
						</div>
					</div>
				</div>
				<div class="col m3">
					<a class="btn-floating btn-large waves-effect waves-light blue darken-3" id="div_back_1_p" style="margin-left:100px;background-color: #3374ac"><i class="mdi-hardware-keyboard-arrow-right"></i></a>
				</div>
			</div>
			<input type="hidden" id="div1-trigger"/>
		</div>
		
		<div id="div2" class="">
			<div class="row">
				<div class="col m2">
					<a class="btn-floating btn-large waves-effect waves-light green darken-3" id="div_back_2" style=""><i class="mdi-hardware-keyboard-arrow-left"></i></a>
				</div>
				<div class="col s12 m8" style="margin-top:-130px">
					<div class="card-panel" id="div2-strip" style="background-image:url('<?php echo base_url()?>assets/explore/images/frontend/wheredoyoulivebg.jpg');" data-img-width="1600" data-img-height="78">
						<div id="div2-strip-text" class="center">Where Do You Live?</div>
					</div>
					<div class="card-panel" id="div2-content">
						<div id="div2-content-action">
							<div class="row center">									
								<div class="col l1 div2-btn" id="div2-Delhi">
									<img src="<?php echo base_url()?>assets/explore/images/frontend/wheredoyoulive-1.png" width="80" height="80">
									<h6 class="black-text">Delhi</h6>	
								</div>
								<div class="col l1 div2-btn" id="div2-Mumbai">
									<img src="<?php echo base_url()?>assets/explore/images/frontend/wheredoyoulive-2.png" width="80" height="80">
									<h6 class="black-text">Mumbai</h6>
								</div>
								<div class="col l1 div2-btn" id="div2-Pune">
									<img src="<?php echo base_url()?>assets/explore/images/frontend/wheredoyoulive-3.png" width="80" height="80">
									<h6 class="black-text">Pune</h6>
								</div>
								<div class="col l1 div2-btn" id="div2-Kolkata">
									<img src="<?php echo base_url()?>assets/explore/images/frontend/wheredoyoulive-4.png" width="80" height="80">
									<h6 class="black-text">Kolkata</h6>
								</div>
								<div class="col l1 div2-btn" id="div2-Hyderabad">
									<img src="<?php echo base_url()?>assets/explore/images/frontend/wheredoyoulive-5.png" width="80" height="80">
									<h6 class="black-text">Hyderabad</h6>
								</div>
								<div class="col l1 div2-btn" id="div2-Ahmedabad">
									<img src="<?php echo base_url()?>assets/explore/images/frontend/wheredoyoulive-6.png" width="80" height="80">
									<h6 class="black-text">Ahmedabad</h6>
								</div>
								<div class="col l1 div2-btn" id="div2-Chandigarh">
									<img src="<?php echo base_url()?>assets/explore/images/frontend/wheredoyoulive-7.png" width="80" height="80">
									<h6 class="black-text">Chandigarh</h6>
								</div>
								<div class="col l1 div2-btn" id="div2-Bhubanehswar">
									<img src="<?php echo base_url()?>assets/explore/images/frontend/wheredoyoulive-8.png" width="80" height="80">
									<h6 class="black-text">Bhubanehswar</h6>
								</div>
																
								<div class="col s6 l1 div2-btn" id="div2-Jaipur">
									<img src="<?php echo base_url()?>assets/explore/images/frontend/wheredoyoulive-9.png" width="80" height="80">
									<h6 class="black-text">Jaipur</h6>
								</div>
								<div class="col s6 l1 div2-btn" id="div2-Gurgaon">
									<img src="<?php echo base_url()?>assets/explore/images/frontend/wheredoyoulive-10.png" width="80" height="80">
									<h6 class="black-text">Gurgaon</h6>
								</div>
								<div class="col s6 l1 div2-btn" id="div2-Kochi">
									<img src="<?php echo base_url()?>assets/explore/images/frontend/wheredoyoulive-11.png" width="80" height="80">
									<h6 class="black-text">Kochi</h6>
								</div>
								<div class="col s6 l1 div2-btn" id="div2-Luckhnow">
									<img src="<?php echo base_url()?>assets/explore/images/frontend/wheredoyoulive-12.png" width="80" height="80">
									<h6 class="black-text">Luckhnow</h6>
								</div>
								<div class="col s6 l1 div2-btn" id="div2-Surat">
									<img src="<?php echo base_url()?>assets/explore/images/frontend/wheredoyoulive-13.png" width="80" height="80">
									<h6 class="black-text">Surat</h6>
								</div>
								<div class="col s6 l1 div2-btn" id="div2-Banglore">
									<img src="<?php echo base_url()?>assets/explore/images/frontend/wheredoyoulive-14.png" width="80" height="80">
									<h6 class="black-text">Banglore</h6>
								</div>
								<div class="col s6 l1 div2-btn" id="div2-Chennai">
									<img src="<?php echo base_url()?>assets/explore/images/frontend/wheredoyoulive-15.png" width="80" height="80">
									<h6 class="black-text">Chennai</h6>
								</div>
								<div class="col s6 l1 div2-btn" id="div2-Other">
									<img src="<?php echo base_url()?>assets/explore/images/frontend/wheredoyoulive-16.png" width="80" height="80">
									<h6 class="black-text">Other City</h6>
								</div>
							</div>
						</div>
						<div id="div2-content-msg">
							<div class="center" id="div2-msg">
								
							</div>
						</div>
					</div>
				</div>
				<div class="col m2">
					<a class="btn-floating btn-large waves-effect waves-light green darken-3" id="div_back_2_p" style="margin-left:100px"><i class="mdi-hardware-keyboard-arrow-right"></i></a>
				</div>
				
			</div>
			<input type="hidden" id="div2-trigger"/>
		</div>
		
		<div id="div3" class="">
			<div class="row">
				<div class="col m3">
					<a class="btn-floating btn-large waves-effect waves-light blue darken-3" id="div_back_3"><i class="mdi-hardware-keyboard-arrow-left"></i></a>
				</div>
				<div class="col s12 m6" style="margin-top:-100px">
					<div class="card-panel" id="div3-strip" style="background-image:url('<?php echo base_url()?>assets/explore/images/frontend/blue-strip.jpg');" data-img-width="1600" data-img-height="78">
						<div id="div3-strip-text" class="center">Do You Have Any Course In Mind?</div>
					</div>
					<div class="card-panel" id="div3-content">
						<div class="row" id="div3-content-action">
							<div class="col s6 l6">
								<div class="row center" id="div3-yes-1">
									<a class="btn-floating div3-btn white" id="div3-yes">
										<img src="<?php echo base_url()?>assets/explore/images/frontend/doyouhaveanycourseinyourmindicon-02-01.png">
									</a>
									<h5 class="black-text">Yes</h5>											
								</div>											
							</div>
							<div class="col s6 l6">
								<div class="row center" id="div3-no-1">
									<a class="btn-floating div3-btn white" id="div3-no">
										<img src="<?php echo base_url()?>assets/explore/images/frontend/doyouhaveanycourseinyourmindicon-02-02.png">
									</a>
									<h5 class="black-text">No</h5>		
								</div>	
							</div>
						</div>	
						<div id="div3-content-msg">
							<div class="center" id="div3-msg">
							
							</div>
						</div>
					</div>
				</div>
				<div class="col m3"><a class="btn-floating btn-large waves-effect waves-light blue darken-3" id="div_back_3_p" style="margin-left:100px"><i class="mdi-hardware-keyboard-arrow-right"></i></a></div>
			</div>
			<input type="hidden" id="div3-trigger"/>
		</div>
		<!--Education-->
		<div id="div15" class="" style="margin-top:100px"> 
			<div class="row">
				<div class="col m3">
					<a class="btn-floating btn-large waves-effect waves-light blue darken-3" id="div_back_15" style="margin-top: 150px;"><i class="mdi-hardware-keyboard-arrow-left" ></i></a>
				</div>
				<div class="col s12 m6">
					<div class="card-panel" id="div4-strip" style="background-image:url('<?php echo base_url()?>assets/explore/images/frontend/blue-strip.jpg');" data-img-width="1600" data-img-height="78">
						<div id="div4-strip-text" class="center">Please Select Education</div>
					</div>
					<div class="card-panel" id="div4-content">
						<div id="div15-content-action">
							<div class="row center">
								
									<img src="<?php echo base_url()?>assets/explore/images/frontend/plese-select-courc-icon-01.png" width="111" height="111">
							
							</div>
							<div class="row center">
								<select class="browser-default black-text" id="select_education">
									<option value="">Select Education</option>
									<?php foreach($eligibility as $crs_mstr){ ?>
									<option value="<?php echo '1'.'-'. $crs_mstr->id.'-'.$crs_mstr->name; ?>"><?php echo $crs_mstr->name; ?></option>
									<?php }  ?>	
														
								</select>	
							</div>
							<div class="row center validation_msg" id="div15-validation"></div>
							<br/>
							<div class="row center">
								<a class="btn waves-effect waves-light blue div15-btn">Next</a>
							</div>
						</div>
						<div id="div15-content-msg">
							<div class="center" id="div4-msg-3">
							
							</div>
						</div>
					</div>
				</div>
				<div class="col m3"><a class="btn-floating btn-large waves-effect waves-light blue darken-3" id="div_back_15_p" style="margin-left:100px;margin-top:150px"><i class="mdi-hardware-keyboard-arrow-right"></i></a></div>
			</div>
			<input type="hidden" id="div15-trigger"/>
		</div>
		<!--EndEducation-->
		<!--type-->
	
		<div id="div14" class="" style="margin-top:100px"> 
			<div class="row">
				<div class="col m3">
					<a class="btn-floating btn-large waves-effect waves-light blue darken-3" id="div_back_14" style="margin-top:150px"><i class="mdi-hardware-keyboard-arrow-left"></i></a>
				</div>
				<div class="col s12 m6">
					<div class="card-panel" id="div4-strip" style="background-image:url('<?php echo base_url()?>assets/explore/images/frontend/blue-strip.jpg');" data-img-width="1600" data-img-height="78">
						<div id="div4-strip-text" class="center">Please Select type</div>
					</div>
					<div class="card-panel" id="div4-content">
						<div id="div14-content-action">
							<div class="row center">
								
									<img src="<?php echo base_url()?>assets/explore/images/frontend/plese-select-courc-icon-01.png" width="111" height="111">
							
							</div>
							<div class="row center">
								<select class="browser-default black-text" id="select_type">
									<option value="">Select Type</option>
									<?php foreach($mst_type as $crs_mstr){ ?>
									<option value="<?php echo $crs_mstr->id .'-'.$crs_mstr->name; ?>"><?php echo $crs_mstr->name; ?></option>
									<?php } ?>									
								</select>	
							</div>
							<div class="row center validation_msg" id="div14-validation"></div>
							<br/>
							<div class="row center">
								<a class="btn waves-effect waves-light blue div14-btn">Next</a>
							</div>
						</div>
						<div id="div14-content-msg">
							<div class="center" id="div4-msg-2">
							
							</div>
						</div>
					</div>
				</div>
				<div class="col m3"><a class="btn-floating btn-large waves-effect waves-light blue darken-3" id="div_back_14_p" style="margin-left:100px;margin-top:150px"><i class="mdi-hardware-keyboard-arrow-right"></i></a></div>
			</div>
			<input type="hidden" id="div14-trigger"/>
		</div>
		<!--End type-->
		<!--End-->
		<!--Stream-->
		<div id="div13" class="" style="margin-top:100px"> 
			<div class="row">
				<div class="col m3">
					<a class="btn-floating btn-large waves-effect waves-light blue darken-3" id="div_back_13" style="margin-top:150px"><i class="mdi-hardware-keyboard-arrow-left"></i></a>
				</div>
				<div class="col s12 m6">
					<div class="card-panel" id="div4-strip" style="background-image:url('<?php echo base_url()?>assets/explore/images/frontend/blue-strip.jpg');" data-img-width="1600" data-img-height="78">
						<div id="div4-strip-text" class="center">Please Select Stream</div>
					</div>
					<div class="card-panel" id="div4-content">
						<div id="div13-content-action">
							<div class="row center">
								
									<img src="<?php echo base_url()?>assets/explore/images/frontend/plese-select-courc-icon-01.png" width="111" height="111">
							
							</div>
							<div class="row center">
								<select class="browser-default black-text" id="select_stream">
									<option value="">Select Stream</option>
									<?php foreach($course_master as $crs_mstr){ ?>
									<option value="<?php echo $crs_mstr->id .'-'.$crs_mstr->name; ?>"><?php echo $crs_mstr->name; ?></option>
									<?php } ?>									
								</select>	
							</div>
							<div class="row center validation_msg" id="div13-validation"></div>
							<br/>
							<div class="row center">
								<a class="btn waves-effect waves-light blue div13-btn">Next</a>
							</div>
						</div>
						<div id="div4-content-msg">
							<div class="center" id="div4-msg-1">
							
							</div>
						</div>
					</div>
				</div>
				<div class="col m3"><a class="btn-floating btn-large waves-effect waves-light blue darken-3" id="div_back_13_p" style="margin-left:100px;margin-top:150px"><i class="mdi-hardware-keyboard-arrow-right"></i></a></div>
			</div>
			<input type="hidden" id="div13-trigger"/>
		</div>
		<!--End Stream-->


		<div id="div4" class="" style="margin-top:9%">
			<div class="row">
				<div class="col m3">
				<a class="btn-floating btn-large waves-effect waves-light blue darken-3" id="div_back_4" style="margin-top:150px"><i class="mdi-hardware-keyboard-arrow-left"></i></a>
				</div>
				<div class="col s12 m6">
					<div class="card-panel" id="div4-strip" style="background-image:url('<?php echo base_url()?>assets/explore/images/frontend/blue-strip.jpg');" data-img-width="1600" data-img-height="78">
						<div id="div4-strip-text" class="center">Please Select Your Course</div>
					</div>
					<div class="card-panel" id="div4-content">
						<div id="div4-content-action">
							<div class="row center">
								
									<img src="<?php echo base_url()?>assets/explore/images/frontend/plese-select-courc-icon-01.png" width="111" height="111">
							
							</div>
							<div class="row center">
								<select class="browser-default black-text" id="select_course">
									<!-- <option value="">Course</option>
									<?php foreach($course_master as $crs_mstr){ ?>
									<option value="<?php echo $crs_mstr->id .'-'.$crs_mstr->name; ?>"><?php echo $crs_mstr->name; ?></option>
									<?php } ?>	 -->								
								</select>	
							</div>
							<div class="row center validation_msg" id="div4-validation"></div>
							<br/>
							<div class="row center">
								<a class="btn waves-effect waves-light blue div4-btn">Next</a>
							</div>
						</div>
						<div id="div4-content-msg">
							<div class="center" id="div4-msg">
							
							</div>
						</div>
					</div>
				</div>
				<div class="col m3"><a class="btn-floating btn-large waves-effect waves-light blue darken-3" id="div_back_4_p" style="margin-left:100px;margin-top:150px"><i class="mdi-hardware-keyboard-arrow-right"></i></a></div>
			</div>
			<input type="hidden" id="div4-trigger"/>
		</div>
		<div id="div5" class="">
			<div class="row">
				<div class="col m2">
					<a class="btn-floating btn-large waves-effect waves-light blue darken-3" id="div_back_5" style="margin-top:150px"><i class="mdi-hardware-keyboard-arrow-left"></i></a>
				</div>
				<div class="col s12 m8">
					<div class="card-panel" id="div5-strip" style="background-image:url('<?php echo base_url()?>assets/explore/images/frontend/blue-strip.jpg');" data-img-width="1600" data-img-height="78">
						<div id="div5-strip-text" class="center">Where You Want To Study?</div>
					</div>
					<div class="card-panel" id="div5-content">
						<div id="div5-content-action">
							<div class="row center">									
								<div class="col l1 div5-btn-1" id="div5-Delhi">
									<img src="<?php echo base_url()?>assets/explore/images/frontend/Whereyouwanttostudy-1.png" width="80" height="80">
									<h6 class="black-text">Delhi</h6>	
								</div>
								<div class="col l1 div5-btn-1" id="div5-Mumbai">
									<img src="<?php echo base_url()?>assets/explore/images/frontend/Whereyouwanttostudy-2.png" width="80" height="80">
									<h6 class="black-text">Mumbai</h6>
								</div>
								<div class="col l1 div5-btn-1" id="div5-Pune">
									<img src="<?php echo base_url()?>assets/explore/images/frontend/Whereyouwanttostudy-3.png" width="80" height="80">
									<h6 class="black-text">Pune</h6>
								</div>
								<div class="col l1 div5-btn-1" id="div5-Kolkata">
									<img src="<?php echo base_url()?>assets/explore/images/frontend/Whereyouwanttostudy-4.png" width="80" height="80">
									<h6 class="black-text">Kolkata</h6>
								</div>
								<div class="col l1 div5-btn-1" id="div5-Hyderabad">
									<img src="<?php echo base_url()?>assets/explore/images/frontend/Whereyouwanttostudy-5.png" width="80" height="80">
									<h6 class="black-text">Hyderabad</h6>
								</div>
								<div class="col l1 div5-btn-1" id="div5-Ahmedabad">
									<img src="<?php echo base_url()?>assets/explore/images/frontend/Whereyouwanttostudy-6.png" width="80" height="80">
									<h6 class="black-text">Ahmedabad</h6>
								</div>
								<div class="col l1 div5-btn-1" id="div5-Chandigarh">
									<img src="<?php echo base_url()?>assets/explore/images/frontend/Whereyouwanttostudy-7.png" width="80" height="80">
									<h6 class="black-text">Chandigarh</h6>
								</div>
								<div class="col l1 div5-btn-1" id="div5-Bhubanehswar">
									<img src="<?php echo base_url()?>assets/explore/images/frontend/Whereyouwanttostudy-8.png" width="80" height="80">
									<h6 class="black-text">Bhubanehswar</h6>
								</div>
																
								<div class="col l1 div5-btn-1" id="div5-Jaipur">
									<img src="<?php echo base_url()?>assets/explore/images/frontend/Whereyouwanttostudy-9.png" width="80" height="80">
									<h6 class="black-text">Jaipur</h6>
								</div>
								<div class="col l1 div5-btn-1" id="div5-Gurgaon">
									<img src="<?php echo base_url()?>assets/explore/images/frontend/Whereyouwanttostudy-10.png" width="80" height="80">
									<h6 class="black-text">Gurgaon</h6>
								</div>
								<div class="col l1 div5-btn-1" id="div5-Kochi">
									<img src="<?php echo base_url()?>assets/explore/images/frontend/Whereyouwanttostudy-11.png" width="80" height="80">
									<h6 class="black-text">Kochi</h6>
								</div>
								<div class="col l1 div5-btn-1" id="div5-Luckhnow">
									<img src="<?php echo base_url()?>assets/explore/images/frontend/Whereyouwanttostudy-12.png" width="80" height="80">
									<h6 class="black-text">Luckhnow</h6>
								</div>
								<div class="col l1 div5-btn-1" id="div5-Surat">
									<img src="<?php echo base_url()?>assets/explore/images/frontend/Whereyouwanttostudy-13.png" width="80" height="80">
									<h6 class="black-text">Surat</h6>
								</div>
								<div class="col l1 div5-btn-1" id="div5-Banglore">
									<img src="<?php echo base_url()?>assets/explore/images/frontend/Whereyouwanttostudy-14.png" width="80" height="80">
									<h6 class="black-text">Banglore</h6>
								</div>
								<div class="col l1 div5-btn-1" id="div5-Chennai">
									<img src="<?php echo base_url()?>assets/explore/images/frontend/Whereyouwanttostudy-15.png" width="80" height="80">
									<h6 class="black-text">Chennai</h6>
								</div>
								<div class="col l1 div5-btn-1" id="div5-Other">
									<img src="<?php echo base_url()?>assets/explore/images/frontend/Whereyouwanttostudy-16.png" width="80" height="80">
									<h6 class="black-text">Other City</h6>
								</div>
							</div>
							<div class="row center validation_msg" id="div5-validation"></div>
							<div class="row center">
								<a class="btn waves-effect waves-light blue div5-btn"  style="margin-top: -74px;">Next</a>
								<a class="btn-floating btn-large waves-effect waves-light blue"><p id="div5_count_location" style="margin-top: 0px; font-size:50px;margin-top: 9px;">0</p></a>
							</div>
						</div>
						<div id="div5-content-msg">
							<div class="center" id="div5-msg">
							
							</div>
						</div>
					</div>
				</div>
				<div class="col m2"><a class="btn-floating btn-large waves-effect waves-light blue darken-3" id="div_back_5_p" style="margin-left:100px;margin-top:150px"><i class="mdi-hardware-keyboard-arrow-right"></i></a></div>
			</div>
			<input type="hidden" id="div5-trigger"/>
		</div>
		<div id="div6" class="animated bounceInRight">
			<div class="row">
				<div class="col m3">
					<a class="btn-floating btn-large waves-effect waves-light green darken-3" id="div_back_6"><i class="mdi-hardware-keyboard-arrow-left"></i></a>
				</div>
				<div class="col s12 m6">
					<div class="card-panel" id="div6-strip" style="background-image:url('<?php echo base_url()?>assets/explore/images/frontend/green-strip.jpg');" data-img-width="1600" data-img-height="78">
						<div id="div6-strip-text" class="center">What's Your highest Qualification?</div>
					</div>
					<div class="card-panel" id="div6-content">
						<div id="div6-content-action">
							<div class="row center">
								<div class="col l2 div6-btn" id="div6-SSC">
									<img class="div6-content-img" src="<?php echo base_url()?>assets/explore/images/frontend/whats-your-highest-qualification-icons-01.png"/>
									<b>SSC</b>
								</div>
								<div class="col l2 div6-btn" id="div6-HSC">
									<img class="div6-content-img" src="<?php echo base_url()?>assets/explore/images/frontend/whats-your-highest-qualification-icons-02.png"/>
									<b>HSC</b>
								</div>
								<div class="col l2 div6-btn" id="div6-GRADUATION">
									<img class="div6-content-img" src="<?php echo base_url()?>assets/explore/images/frontend/whats-your-highest-qualification-icons-03.png"/>
									<b>Graduation</b>
								</div>
								<div class="col l2 div6-btn" id="div6-POST GRADUATION">
									<img class="div6-content-img" src="<?php echo base_url()?>assets/explore/images/frontend/whats-your-highest-qualification-icons-04.png"/>
									<b>Post Graduation</b>
								</div>
								<div class="col l2 div6-btn" id="div6-PHD">
									<img class="div6-content-img" src="<?php echo base_url()?>assets/explore/images/frontend/whats-your-highest-qualification-icons-05.png"/>
									<b>PHD</b>
								</div>							
							</div>
						</div>
						<div id="div6-content-msg">
							<div class="center" id="div6-msg">
							
							</div>
						</div>
					</div>
				</div>
				<div class="col m3">
					<a class="btn-floating btn-large waves-effect waves-light blue darken-3" id="div_back_7_p" style="margin-left:100px;margin-top:150px"><i class="mdi-hardware-keyboard-arrow-right"></i></a>
				</div>
			</div>
			<input type="hidden" id="div6-trigger"/>
		</div>
		<div id="div7" class="animated bounceInRight">
			<div class="row">
				<div class="col m3">
					<a class="btn-floating btn-large waves-effect waves-light blue darken-3" id="div_back_7" style="margin-top:150px"><i class="mdi-hardware-keyboard-arrow-left"></i></a>
				</div>
				<div class="col s12 m6">
					<div class="card-panel" id="div7-strip" style="background-image:url('<?php echo base_url()?>assets/explore/images/frontend/blue-strip.jpg');" data-img-width="1600" data-img-height="78">
						<div id="div7-strip-text" class="center">Who Are You?</div> 
					</div>
					<div class="card-panel" id="div7-content">
						<div id="div7-content-action">
							<div class="row center">
								<div class="col s6 l6 div7-btn" id="div7-BOY">
									<img class="div7-content-img" src="<?php echo base_url()?>assets/explore/images/frontend/who-are-you-icon-01.png"/>
									<b><br/>Boy</b>
								</div>
								<div class="col s6 l6 div7-btn" id="div7-GIRL">
									<img class="div7-content-img" src="<?php echo base_url()?>assets/explore/images/frontend/who-are-you-icon-02.png"/>
									<b><br/>Girl</b>
								</div>				
							</div>
						</div>
						<div id="div7-content-msg">
							<div class="center" id="div7-msg">
							
							</div>
						</div>
					</div>
				</div>
				<div class="col m3">
					<a class="btn-floating btn-large waves-effect waves-light blue darken-3" id="div_back_7_p" style="margin-left:100px;margin-top:150px"><i class="mdi-hardware-keyboard-arrow-right"></i></a>
				</div>
			</div>
			<input type="hidden" id="div7-trigger"/>
		</div>
		<div id="div8" class="animated bounceInRight">
			<div class="row">
				<div class="col m3">
					<a class="btn-floating btn-large waves-effect waves-light blue darken-3" id="div_back_8" style="margin-top:94px"><i class="mdi-hardware-keyboard-arrow-left"></i></a>
				</div>
				<div class="col s12 m6">
					<div class="card-panel" id="div8-strip" style="background-image:url('<?php echo base_url()?>assets/explore/images/frontend/blue-strip.jpg');" data-img-width="1600" data-img-height="78">
						<div id="div8-strip-text" class="center">Ohh!! We Forget To Catch Your Name</div> 
					</div>
					<div class="card-panel" id="div8-content">
						<div id="div8-content-action">
							<div class="row center">
								<div class="col s2 l3">&nbsp;</div>
								<div class="input-field col s8 l6 black-text">
									<input id="first_name" type="text" placeholder="Your Name">
								</div>
								<div class="col s2 l3">&nbsp;</div>
							</div>
							<div class="row center validation_msg" id="div8-validation"></div>
							<div class="row center">
								<a class="btn waves-effect waves-light blue div8-btn">Next</a>
							</div>
						</div>
						<div id="div8-content-msg">
							<div class="center" id="div8-msg">
							
							</div>
						</div>
					</div>
					
				</div>
				<div class="col m3"><a class="btn-floating btn-large waves-effect waves-light blue darken-3" id="div_back_8_p" style="margin-left:100px;margin-top:94px"><i class="mdi-hardware-keyboard-arrow-right"></i></a></div>
			</div>
			<input type="hidden" id="div8-trigger"/>
		</div>
		<div id="div9" class="animated bounceInRight">
			<div class="row">
				<div class="col m3">
					<a class="btn-floating btn-large waves-effect waves-light blue darken-3" id="div_back_9" style="margin-top:200px"><i class="mdi-hardware-keyboard-arrow-left"></i></a>
				</div>
				<div class="col s12 m6">
					<div class="card-panel" id="div9-strip" style="background-image:url('<?php echo base_url()?>assets/explore/images/frontend/blue-strip.jpg');" data-img-width="1600" data-img-height="78">
						<div id="div9-strip-text" class="center">What's Your Age?</div> 
					</div>
					<div class="card-panel" id="div9-content">
						<div id="div9-content-action">
							<div class="row">
								<div class="col s12 l12">
									<div id="year_selector"></div>
								</div>	
							</div>	
							<div class="row center-align" style="margin-top:10px;">
								<div class="col s12 l5" id="months-div" style="display:none;">
									<div class="row">
										<div class="col s3 l3"><div class="month_select" id="month_1">JAN</div></div>
										<div class="col s3 l3"><div class="month_select" id="month_2">FEB</div></div>
										<div class="col s3 l3"><div class="month_select" id="month_3">MAR</div></div>
										<div class="col s3 l3"><div class="month_select" id="month_4">APR</div></div>
									</div>
									<div class="row">
										<div class="col s3 l3"><div class="month_select" id="month_5">MAY</div></div>
										<div class="col s3 l3"><div class="month_select" id="month_6">JUN</div></div>
										<div class="col s3 l3"><div class="month_select" id="month_7">JUL</div></div>
										<div class="col s3 l3"><div class="month_select" id="month_8">AUG</div></div>
									</div>
									<div class="row">
										<div class="col s3 l3"><div class="month_select" id="month_9">SEP</div></div>
										<div class="col s3 l3"><div class="month_select" id="month_10">OCT</div></div>
										<div class="col s3 l3"><div class="month_select" id="month_11">NOV</div></div>
										<div class="col s3 l3"><div class="month_select" id="month_12">DEC</div></div>
									</div>
								</div>
								<style>
								.day_select
								{
									display:none;
								}
								</style>
								<div class="col s12 l6" id="days-div" style="display:none;" style="height:-53px">
									<div class="row center-align" id="days-div-row" >
										<div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_1">1&nbsp;</div></div>
										<div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_2">2&nbsp;</div></div>
										<div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_3">3&nbsp;</div></div>
										<div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_4">4&nbsp;</div></div>
										<div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_5">5&nbsp;</div></div>
										<div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_6">6&nbsp;&nbsp;</div></div>
										<br/>
										<div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_7">7&nbsp;</div></div>
										<div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_8">8&nbsp;</div></div>
										<div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_9">9&nbsp;</div></div>
										<div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_10">10</div></div>
										<div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_11">11</div></div>
										<div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_12">12</div></div>
										<br/>
										<div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_13">13</div></div>
										<div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_14">14</div></div>
										<div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_15">15</div></div>
										<div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_16">16</div></div>
										<div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_17">17</div></div>
										<div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_18">18</div></div>
										<br/>
										<div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_19">19</div></div>
										<div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_20">20</div></div>
										<div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_21">21</div></div>
										<div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_22">22</div></div>
										<div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_23">23</div></div>
										<div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_24">24</div></div>
										<br/>
										<div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_25">25</div></div>
										<div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_26">26</div></div>
										<div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_27">27</div></div>
										<div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_28">28</div></div>
										<div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_29">29</div></div>
										<div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_30">30</div></div>
										<br/>
										<div class="col s2 sl2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_31">31</div></div>
									</div>
									
								</div>
							</div>
							<div class="row  center-align" id="finaldate-div" style="display:none">
								<div class="col s12 l8">
									<h1 class="center black-text" style="margin-top: -125px;margin-left: -183px;">
										<span id="year"></span>
										<span id="month"></span>
										<span id="day"></span>
									</h1>											
								</div>
								<div class="row center validation_msg" id="div9-validation"></div>								
								<div class="col s12 l4">
									<a class="btn waves-effect waves-light blue div9-btn" style="margin-top:-46px">Next</a>
								</div>
							</div>
						</div>
						<div id="div9-content-msg">
							<div class="center" id="div9-msg">
							
							</div>
						</div>
					</div>
				</div>
				<div class="col m3">	<a class="btn-floating btn-large waves-effect waves-light blue darken-3" id="div_back_9_p" style="margin-left:100px;margin-top:200px"><i class="mdi-hardware-keyboard-arrow-right"></i></a></div>
			</div>
			<input type="hidden" id="div9-trigger"/>
		</div>
		<div id="div10" class="animated bounceInRight">
			<div class="row">
				<div class="col m3">
					<a class="btn-floating btn-large waves-effect waves-light green darken-3" id="div_back_10"><i class="mdi-hardware-keyboard-arrow-left"></i></a>
				</div>
				<div class="col s12 m6">
					<div class="card-panel" id="div10-strip" style="background-image:url('<?php echo base_url()?>assets/explore/images/frontend/green-strip.jpg');" data-img-width="1600" data-img-height="78">
						<div id="div10-strip-text" class="center">Are You Studying Currently?</div> 
					</div>
					<div class="card-panel" id="div10-content">
						<div id="div10-content-action">
							<div class="row">
								<div class="col s6 l6">
									<div class="row center">
										<a class="btn-floating div10-btn white" id="div10-yes">
											<img src="<?php echo base_url()?>assets/explore/images/frontend/are-yo-studying-currently-icon-01.png">
										</a>
										<h5 class="black-text">Yes</h5>											
									</div>											
								</div>
								<div class="col s6 l6">
									<div class="row center">
										<a class="btn-floating div10-btn white" id="div10-no">
											<img src="<?php echo base_url()?>assets/explore/images/frontend/are-yo-studying-currently-icon-02.png">
										</a>
										<h5 class="black-text">No</h5>		
									</div>	
								</div>	
							</div>	
						</div>
						<div id="div10-content-msg">
							<div class="center" id="div10-msg">
							
							</div>
						</div>						
					</div>
				</div>
				<div class="col m3">&nbsp;</div>
			</div>
			<input type="hidden" id="div10-trigger"/>
		</div>
		<div id="div11" class="animated bounceInRight">
			<div class="row">
				<div class="col m3">
					<a class="btn-floating btn-large waves-effect waves-light blue darken-3" id="div_back_11"><i class="mdi-hardware-keyboard-arrow-left"></i></a>
				</div>
				<div class="col s12 m6">
					<div class="card-panel" id="div11-strip" style="background-image:url('<?php echo base_url()?>assets/explore/images/frontend/blue-strip.jpg');" data-img-width="1600" data-img-height="78">
						<div id="div11-strip-text" class="center">What Course Are You Pursuing?</div>
					</div>
					<div class="card-panel" id="div11-content">
						<div id="div11-content-action">
							<div class="row center">
								
									<img src="<?php echo base_url()?>assets/explore/images/frontend/what-course-are-you-pursuing-icon-01.png" width="111" height="111">
							
							</div>
							<div class="row center">
								<select class="browser-default black-text" id="div11-course-select">
									<option value="">Course</option>
									<?php foreach($course_master as $crs_mstr){ ?>
									<option value="<?php echo $crs_mstr->id .'-'.$crs_mstr->name; ?>"><?php echo $crs_mstr->name; ?></option>
									<?php } ?>
								</select>	
							</div>
							<div class="row center validation_msg" id="div11-validation"></div>
							<div class="row center">
								<a class="btn waves-effect waves-light blue div11-btn">Next</a>
							</div>
						</div>
						<div id="div11-content-msg">
							<div class="center" id="div11-msg">
							
							</div>
						</div>
					</div>
				</div>
				<div class="col m3">&nbsp;</div>
			</div>
			<input type="hidden" id="div11-trigger"/>
		</div>
		<div id="div12" class="animated bounceInRight">
			<div class="row">
				<div class="col m3">
					<a class="btn-floating btn-large waves-effect waves-light green darken-3" id="div_back_12" style="margin-top:200px"><i class="mdi-hardware-keyboard-arrow-left"></i></a>
				</div>
				<div class="col s12 m6">
					<div class="card-panel" id="div12-strip" style="background-image:url('<?php echo base_url()?>assets/explore/images/frontend/green-strip.jpg');" data-img-width="1600" data-img-height="78">
						<div id="div12-strip-text" class="center">Contact</div>
					</div>
					<div class="card-panel" id="div12-content">	
						<div id="div12-content-action">
							<div class="row center">
								<div class="row center black-text">
									<div class="col l7">
										<div class="row" id="phone-row">
											<div id="errorMsg" style="color:red"></div>
											<div class="input-field col l12">
												<!-- <input id="email" type="text" placeholder="Email"><br/> -->
												<?php $udata=$this->session->userdata('logged_in');?>
													<?php if($udata!=''){?>
												<input id="email" type="text" placeholder="Email" value="<?php echo $udata['email']?>" readonly><br/>
												<?php }else{?>
												<input id="email" type="text" placeholder="Email" value=""><br/>
												<?php } ?>
											</div>										
										</div>
									</div>	
										<?php if($this->session->userdata('logged_in')==''){?>
										<div class="col l7" style="margin-top:-175px">
										<div class="row" id="phone-row">
											<div id="errorMsg" style="color:red"></div>
											<div class="input-field col l12">
												
												
												<input id="password" type="password" placeholder="Password">
												<input id="rep_password" type="password" placeholder="Repeat Password">											
											</div>										
										</div>
									</div>							
									<div class="col s12 l5" style="margin-top:-218px">
										<div class="row">
											<div class="input-field col s10 l10">											
												<div id="mobilenumberdiv">
													<span id="mobilenumber">
														<input type="tel" name="" id="mobile_num" placeholder="Mobile"/>
													</span>													
												</div>												
											</div>
											<div class="col s2 l2">											
												<i class="mdi-content-backspace right" style="margin-left:1px;font-size: 35px;margin-top: 18px;" id="phone_delete_btn"></i>												
											</div>
										</div>
										<div class="row">
											<div class="col s12 l12" id="phonenumber-div">
												<div class="row">
													<div class="col s3 l3 green phonenumber" id="phone_7">7</div>
													<div class="col s3 l3 green phonenumber" id="phone_8">8</div>
													<div class="col s3 l3 green phonenumber" id="phone_9">9</div>
												</div>
												<div class="row">
													<div class="col s3 l3 green phonenumber" id="phone_4">4</div>
													<div class="col s3 l3 green phonenumber" id="phone_5">5</div>
													<div class="col s3 l3 green phonenumber" id="phone_6">6</div>
												</div>
												<div class="row">
													<div class="col s3 l3 green phonenumber" id="phone_1">1</div>
													<div class="col s3 l3 green phonenumber" id="phone_2">2</div>
													<div class="col s3 l3 green phonenumber" id="phone_3">3</div>
												</div>
												<div class="row">
													<div class="col s3 l3 green phonenumber" id="phone_*">*</div>
													<div class="col s3 l3 green phonenumber" id="phone_0">0</div>
													<div class="col s3 l3 green phonenumber" id="phone_#">#</div>
												</div>
											</div>
										</div>
									</div>
									<?php }?>
								</div>
							</div>
							<div class="row center">
								<button type="button" class="btn waves-effect waves-light green div12-btn" style="margin-top:-100px">Done</button>
							</div>
						</div>
						<div id="div12-content-msg">
							<div class="center" id="div12-msg">
							
							</div>
						</div>
					</div>
				</div>
				<div class="col m3">&nbsp;</div>
			</div>
			<input type="hidden" id="div12-trigger"/>
		</div>

<div class="template_section" style="background-color: #fff;">
    <body id="top" class="has-header-search">
       <section>
            <div class="container">
			  <br/>
			  <h3 class="text-center text-bold mb-40">Our Setps</h3>
			  <br/>
			  <br/>
			  <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-5 hidden-xs">
                            <div class="s-service-carousel-nav mt-40">
                                <div class="item roboto-slab fw300 fz-18 transition">Graphics Design</div>
                                <div class="item roboto-slab fw300 fz-18 transition">Web Design</div>
                                <div class="item roboto-slab fw300 fz-18 transition">Web Development</div>
                                <div class="item roboto-slab fw300 fz-18 transition">Digital Marketing</div>
                                <div class="item roboto-slab fw300 fz-18 transition">Video Editing</div>
                                <div class="item roboto-slab fw300 fz-18 transition">Illustration</div>
                                <div class="item roboto-slab fw300 fz-18 transition">Motion Graphics</div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-8 col-lg-7 flex disable-flex-xs">
                            <div class="iphone-holder hidden-sm hidden-xs">
                                <img src="<?php echo base_url('assets/images/iphone-shadow-2.png'); ?>" alt="">
                            </div>
                            <div class="s-service-carousel-for mt-60 ml-30">
                                <div class="item">
                                    <h3 class="fz-24 fw400 mb-20">Graphics Design</h3>
                                    <p class="fw300 no-ls">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.</p>
                                    <a href="#!" class="btn btn-large waves-effect waves-light mt-15 mb-10">LEARN MORE</a>
                                </div>
                                <div class="item">
                                    <h3 class="fz-24 fw400 mb-20">Web Design</h3>
                                    <p class="fw300 no-ls">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.</p>
                                    <a href="#!" class="btn btn-large waves-effect waves-light mt-15 mb-10">LEARN MORE</a>
                                </div>
                                <div class="item">
                                    <h3 class="fz-24 fw400 mb-20">Web Development</h3>
                                    <p class="fw300 no-ls">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.</p>
                                    <a href="#!" class="btn btn-large waves-effect waves-light mt-15 mb-10">LEARN MORE</a>
                                </div>
                                <div class="item">
                                    <h3 class="fz-24 fw400 mb-20">Digital Marketing</h3>
                                    <p class="fw300 no-ls">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.</p>
                                    <a href="#!" class="btn btn-large waves-effect waves-light mt-15 mb-10">LEARN MORE</a>
                                </div>
                                <div class="item">
                                    <h3 class="fz-24 fw400 mb-20">Video Editing</h3>
                                    <p class="fw300 no-ls">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.</p>
                                    <a href="#!" class="btn btn-large waves-effect waves-light mt-15 mb-10">LEARN MORE</a>
                                </div>
                                <div class="item">
                                    <h3 class="fz-24 fw400 mb-20">Illustration</h3>
                                    <p class="fw300 no-ls">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.</p>
                                    <a href="#!" class="btn btn-large waves-effect waves-light mt-15 mb-10">LEARN MORE</a>
                                </div>
                                <div class="item">
                                    <h3 class="fz-24 fw400 mb-20">Motion Graphics</h3>
                                    <p class="fw300 no-ls">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.</p>
                                    <a href="#!" class="btn btn-large waves-effect waves-light mt-15 mb-10">LEARN MORE</a>
                                </div>
                            </div>
                        </div>
                    </div> <!-- end .row -->
					<br/>
					<br/>
					<br/>
					<br/>
              <div class="row">
                <div class="col-md-12">

                    <div class="icon-tab">

                      <!-- Nav tabs -->
                      <div class="text-center container">
                        <ul class="nav nav-pills" role="tablist">

                          <li role="presentation" class="active"><a href="#icontab-1" class="waves-effect waves-light"  role="tab" data-toggle="tab" onclick="getCource(2)"> Under Graduate</a></li>

                          <li role="presentation"><a href="#icontab-2" class="waves-effect waves-light" role="tab" data-toggle="tab" onclick="getCource(4)">Graduate</a></li>

                          <li role="presentation"><a href="#icontab-3" class="waves-effect waves-light" role="tab" data-toggle="tab" onclick="getCource(4)"> Post Graduate </a></li>
						  
                        <!--   <li role="presentation"><a href="#icontab-4" class="waves-effect waves-light" role="tab" data-toggle="tab" onclick="getCource(0)"> Masters </a></li> -->
                        </ul>
                      </div>

                      <!-- Tab panes -->
                      <div class="panel-body mt-40">
                        <div class="tab-content">
                          <div role="tabpanel" class="tab-pane fade in active" id="icontab-1">

                            
                          </div>

                          <div role="tabpanel" class="tab-pane fade" id="icontab-2">
                            <div class="row">
								<div id="testimonial-two" class="carousel slide carousel-testimonial text-center" data-ride="carousel">

								  <!-- Wrapper for slides -->
								  <div class="carousel-inner" role="listbox">
									<div class="item active">
										<div class="row latest-news-card">
										  <div class="col-xs-12 col-sm-6 col-md-4">
											<article class="card">
											  <div class="card-image waves-effect waves-block waves-light">
												<img class="activator" src="<?php echo base_url('assets/img/blog/blog-18.jpg'); ?>" alt="image">
											  </div>
											  <div class="card-content">
												<h2 class="entry-title activator">Why Material Design is our best attempt for a unifying theory…</h2>
											  </div>
											  <div class="card-reveal overlay-blue">
												<span class="card-title close-button"><i class="fa fa-times"></i></span>
												<a class="posted-on" href="#">Branding</a>
												<h2 class="entry-title"><a href="#">Material Design is best</a></h2>
												<p>Authoritatively grow quality technologies for strategic sources. Dramatically evolve front-end services quality.</p>
												<a href="#" class="readmore">Read Full Post <i class="fa fa-long-arrow-right"></i></a>
											  </div>
											</article><!-- /.card -->
										  </div><!-- /.col-md-4 -->

										  <div class="col-xs-12 col-sm-6 col-md-4">
											<article class="card">
											  <div class="card-image waves-effect waves-block waves-light">
												<img class="activator" src="<?php echo base_url('assets/img/blog/1.jpg'); ?>" alt="image">
											  </div>
											  <div class="card-content">
												<h2 class="entry-title activator">How To Be More Organized While Designing UI ...</h2>
											  </div>
											  <div class="card-reveal overlay-blue">
												<span class="card-title close-button"><i class="fa fa-times"></i></span>
												<a class="posted-on" href="#">Branding</a>
												<h2 class="entry-title"><a href="#">How To Be More Organized</a></h2>
												<p>Authoritatively grow quality technologies for strategic sources. Dramatically evolve front-end services quality.</p>
												<a href="#" class="readmore">Read Full Post <i class="fa fa-long-arrow-right"></i></a>
											  </div>
											</article><!-- /.card -->
										  </div><!-- /.col-md-4 -->

										  <div class="col-xs-12 col-sm-6 col-md-4">
											<article class="card">
											  <div class="card-image waves-effect waves-block waves-light">
												<img class="activator" src="<?php echo base_url('assets/img/blog/3.jpg'); ?>" alt="image">
											  </div>
											  <div class="card-content">
												<h2 class="entry-title activator">Exploring the Product Design of the Stripe ...</h2>
											  </div>
											  <div class="card-reveal overlay-blue">
												<span class="card-title close-button"><i class="fa fa-times"></i></span>
												<a class="posted-on" href="#">Photography</a>
												<h2 class="entry-title"><a href="#">Exploring the Product Design7</a></h2>
												<p>Authoritatively grow quality technologies for strategic sources. Dramatically evolve front-end services quality.</p>
												<a href="#" class="readmore">Read Full Post <i class="fa fa-long-arrow-right"></i></a>
											  </div>
											</article><!-- /.card -->
										  </div><!-- /.col-md-4 -->
									  </div>
									</div>
									<div class="item">
										<div class="row latest-news-card">
										  <div class="col-xs-12 col-sm-6 col-md-4">
											<article class="card">
											  <div class="card-image waves-effect waves-block waves-light">
												<img class="activator" src="<?php echo base_url('assets/img/blog/blog-18.jpg'); ?>" alt="image">
											  </div>
											  <div class="card-content">
												<h2 class="entry-title activator">Why Material Design is our best attempt for a unifying theory…</h2>
											  </div>
											  <div class="card-reveal overlay-blue">
												<span class="card-title close-button"><i class="fa fa-times"></i></span>
												<a class="posted-on" href="#">Branding</a>
												<h2 class="entry-title"><a href="#">Material Design is best</a></h2>
												<p>Authoritatively grow quality technologies for strategic sources. Dramatically evolve front-end services quality.</p>
												<a href="#" class="readmore">Read Full Post <i class="fa fa-long-arrow-right"></i></a>
											  </div>
											</article><!-- /.card -->
										  </div><!-- /.col-md-4 -->

										  <div class="col-xs-12 col-sm-6 col-md-4">
											<article class="card">
											  <div class="card-image waves-effect waves-block waves-light">
												<img class="activator" src="<?php echo base_url('assets/img/blog/1.jpg'); ?>" alt="image">
											  </div>
											  <div class="card-content">
												<h2 class="entry-title activator">How To Be More Organized While Designing UI ...</h2>
											  </div>
											  <div class="card-reveal overlay-blue">
												<span class="card-title close-button"><i class="fa fa-times"></i></span>
												<a class="posted-on" href="#">Branding</a>
												<h2 class="entry-title"><a href="#">How To Be More Organized</a></h2>
												<p>Authoritatively grow quality technologies for strategic sources. Dramatically evolve front-end services quality.</p>
												<a href="#" class="readmore">Read Full Post <i class="fa fa-long-arrow-right"></i></a>
											  </div>
											</article><!-- /.card -->
										  </div><!-- /.col-md-4 -->

										  <div class="col-xs-12 col-sm-6 col-md-4">
											<article class="card">
											  <div class="card-image waves-effect waves-block waves-light">
												<img class="activator" src="<?php echo base_url('assets/img/blog/3.jpg'); ?>" alt="image">
											  </div>
											  <div class="card-content">
												<h2 class="entry-title activator">Exploring the Product Design of the Stripe ...</h2>
											  </div>
											  <div class="card-reveal overlay-blue">
												<span class="card-title close-button"><i class="fa fa-times"></i></span>
												<a class="posted-on" href="#">Photography</a>
												<h2 class="entry-title"><a href="#">Exploring the Product Design7</a></h2>
												<p>Authoritatively grow quality technologies for strategic sources. Dramatically evolve front-end services quality.</p>
												<a href="#" class="readmore">Read Full Post <i class="fa fa-long-arrow-right"></i></a>
											  </div>
											</article><!-- /.card -->
										  </div><!-- /.col-md-4 -->
									  </div>
									</div>
								  </div>
								  
								  <!-- Controls 
								  <a class="left carousel-control" href="#testimonial-two" role="button" data-slide="prev">
									<span class="material-icons" aria-hidden="true"></span>
									<span class="sr-only">Previous</span>
								  </a>
								  <a class="right carousel-control" href="#testimonial-two" role="button" data-slide="next">
									<span class="material-icons" aria-hidden="true"></span>
									<span class="sr-only">Next</span>
								  </a>-->
								</div>
                            </div>
                          </div>

                          <div role="tabpanel" class="tab-pane fade" id="icontab-3">
                            <div class="row">
								<div id="testimonial-two" class="carousel slide carousel-testimonial text-center" data-ride="carousel">

								  <!-- Wrapper for slides -->
								  <div class="carousel-inner" role="listbox">
									<div class="item active">
										<div class="row latest-news-card">
										  <div class="col-xs-12 col-sm-6 col-md-4">
											<article class="card">
											  <div class="card-image waves-effect waves-block waves-light">
												<img class="activator" src="<?php echo base_url('assets/img/blog/blog-18.jpg'); ?>" alt="image">
											  </div>
											  <div class="card-content">
												<h2 class="entry-title activator">Why Material Design is our best attempt for a unifying theory…</h2>
											  </div>
											  <div class="card-reveal overlay-blue">
												<span class="card-title close-button"><i class="fa fa-times"></i></span>
												<a class="posted-on" href="#">Branding</a>
												<h2 class="entry-title"><a href="#">Material Design is best</a></h2>
												<p>Authoritatively grow quality technologies for strategic sources. Dramatically evolve front-end services quality.</p>
												<a href="#" class="readmore">Read Full Post <i class="fa fa-long-arrow-right"></i></a>
											  </div>
											</article><!-- /.card -->
										  </div><!-- /.col-md-4 -->

										  <div class="col-xs-12 col-sm-6 col-md-4">
											<article class="card">
											  <div class="card-image waves-effect waves-block waves-light">
												<img class="activator" src="<?php echo base_url('assets/img/blog/1.jpg'); ?>" alt="image">
											  </div>
											  <div class="card-content">
												<h2 class="entry-title activator">How To Be More Organized While Designing UI ...</h2>
											  </div>
											  <div class="card-reveal overlay-blue">
												<span class="card-title close-button"><i class="fa fa-times"></i></span>
												<a class="posted-on" href="#">Branding</a>
												<h2 class="entry-title"><a href="#">How To Be More Organized</a></h2>
												<p>Authoritatively grow quality technologies for strategic sources. Dramatically evolve front-end services quality.</p>
												<a href="#" class="readmore">Read Full Post <i class="fa fa-long-arrow-right"></i></a>
											  </div>
											</article><!-- /.card -->
										  </div><!-- /.col-md-4 -->

										  <div class="col-xs-12 col-sm-6 col-md-4">
											<article class="card">
											  <div class="card-image waves-effect waves-block waves-light">
												<img class="activator" src="<?php echo base_url('assets/img/blog/3.jpg'); ?>" alt="image">
											  </div>
											  <div class="card-content">
												<h2 class="entry-title activator">Exploring the Product Design of the Stripe ...</h2>
											  </div>
											  <div class="card-reveal overlay-blue">
												<span class="card-title close-button"><i class="fa fa-times"></i></span>
												<a class="posted-on" href="#">Photography</a>
												<h2 class="entry-title"><a href="#">Exploring the Product Design7</a></h2>
												<p>Authoritatively grow quality technologies for strategic sources. Dramatically evolve front-end services quality.</p>
												<a href="#" class="readmore">Read Full Post <i class="fa fa-long-arrow-right"></i></a>
											  </div>
											</article><!-- /.card -->
										  </div><!-- /.col-md-4 -->
									  </div>
									</div>
									<div class="item">
										<div class="row latest-news-card">
										  <div class="col-xs-12 col-sm-6 col-md-4">
											<article class="card">
											  <div class="card-image waves-effect waves-block waves-light">
												<img class="activator" src="<?php echo base_url('assets/img/blog/blog-18.jpg'); ?>" alt="image">
											  </div>
											  <div class="card-content">
												<h2 class="entry-title activator">Why Material Design is our best attempt for a unifying theory…</h2>
											  </div>
											  <div class="card-reveal overlay-blue">
												<span class="card-title close-button"><i class="fa fa-times"></i></span>
												<a class="posted-on" href="#">Branding</a>
												<h2 class="entry-title"><a href="#">Material Design is best</a></h2>
												<p>Authoritatively grow quality technologies for strategic sources. Dramatically evolve front-end services quality.</p>
												<a href="#" class="readmore">Read Full Post <i class="fa fa-long-arrow-right"></i></a>
											  </div>
											</article><!-- /.card -->
										  </div><!-- /.col-md-4 -->

										  <div class="col-xs-12 col-sm-6 col-md-4">
											<article class="card">
											  <div class="card-image waves-effect waves-block waves-light">
												<img class="activator" src="<?php echo base_url('assets/img/blog/1.jpg'); ?>" alt="image">
											  </div>
											  <div class="card-content">
												<h2 class="entry-title activator">How To Be More Organized While Designing UI ...</h2>
											  </div>
											  <div class="card-reveal overlay-blue">
												<span class="card-title close-button"><i class="fa fa-times"></i></span>
												<a class="posted-on" href="#">Branding</a>
												<h2 class="entry-title"><a href="#">How To Be More Organized</a></h2>
												<p>Authoritatively grow quality technologies for strategic sources. Dramatically evolve front-end services quality.</p>
												<a href="#" class="readmore">Read Full Post <i class="fa fa-long-arrow-right"></i></a>
											  </div>
											</article><!-- /.card -->
										  </div><!-- /.col-md-4 -->

										  <div class="col-xs-12 col-sm-6 col-md-4">
											<article class="card">
											  <div class="card-image waves-effect waves-block waves-light">
												<img class="activator" src="<?php echo base_url('assets/img/blog/3.jpg'); ?>" alt="image">
											  </div>
											  <div class="card-content">
												<h2 class="entry-title activator">Exploring the Product Design of the Stripe ...</h2>
											  </div>
											  <div class="card-reveal overlay-blue">
												<span class="card-title close-button"><i class="fa fa-times"></i></span>
												<a class="posted-on" href="#">Photography</a>
												<h2 class="entry-title"><a href="#">Exploring the Product Design7</a></h2>
												<p>Authoritatively grow quality technologies for strategic sources. Dramatically evolve front-end services quality.</p>
												<a href="#" class="readmore">Read Full Post <i class="fa fa-long-arrow-right"></i></a>
											  </div>
											</article><!-- /.card -->
										  </div><!-- /.col-md-4 -->
									  </div>
									</div>
								  </div>
								  
								  <!-- Controls 
								  <a class="left carousel-control" href="#testimonial-two" role="button" data-slide="prev">
									<span class="material-icons" aria-hidden="true"></span>
									<span class="sr-only">Previous</span>
								  </a>
								  <a class="right carousel-control" href="#testimonial-two" role="button" data-slide="next">
									<span class="material-icons" aria-hidden="true"></span>
									<span class="sr-only">Next</span>
								  </a>-->
								</div>
                            </div>
                          </div>
                          
                          <div role="tabpanel" class="tab-pane fade" id="icontab-4">
                            <div class="row">
								<div id="testimonial-two" class="carousel slide carousel-testimonial text-center" data-ride="carousel">

								  <!-- Wrapper for slides -->
								  <div class="carousel-inner" role="listbox">
									<div class="item active">
										<div class="row latest-news-card">
										  <div class="col-xs-12 col-sm-6 col-md-4">
											<article class="card">
											  <div class="card-image waves-effect waves-block waves-light">
												<img class="activator" src="<?php echo base_url('assets/img/blog/blog-18.jpg'); ?>" alt="image">
											  </div>
											  <div class="card-content">
												<h2 class="entry-title activator">Why Material Design is our best attempt for a unifying theory…</h2>
											  </div>
											  <div class="card-reveal overlay-blue">
												<span class="card-title close-button"><i class="fa fa-times"></i></span>
												<a class="posted-on" href="#">Branding</a>
												<h2 class="entry-title"><a href="#">Material Design is best</a></h2>
												<p>Authoritatively grow quality technologies for strategic sources. Dramatically evolve front-end services quality.</p>
												<a href="#" class="readmore">Read Full Post <i class="fa fa-long-arrow-right"></i></a>
											  </div>
											</article><!-- /.card -->
										  </div><!-- /.col-md-4 -->

										  <div class="col-xs-12 col-sm-6 col-md-4">
											<article class="card">
											  <div class="card-image waves-effect waves-block waves-light">
												<img class="activator" src="<?php echo base_url('assets/img/blog/1.jpg'); ?>" alt="image">
											  </div>
											  <div class="card-content">
												<h2 class="entry-title activator">How To Be More Organized While Designing UI ...</h2>
											  </div>
											  <div class="card-reveal overlay-blue">
												<span class="card-title close-button"><i class="fa fa-times"></i></span>
												<a class="posted-on" href="#">Branding</a>
												<h2 class="entry-title"><a href="#">How To Be More Organized</a></h2>
												<p>Authoritatively grow quality technologies for strategic sources. Dramatically evolve front-end services quality.</p>
												<a href="#" class="readmore">Read Full Post <i class="fa fa-long-arrow-right"></i></a>
											  </div>
											</article><!-- /.card -->
										  </div><!-- /.col-md-4 -->

										  <div class="col-xs-12 col-sm-6 col-md-4">
											<article class="card">
											  <div class="card-image waves-effect waves-block waves-light">
												<img class="activator" src="<?php echo base_url('assets/img/blog/3.jpg'); ?>" alt="image">
											  </div>
											  <div class="card-content">
												<h2 class="entry-title activator">Exploring the Product Design of the Stripe ...</h2>
											  </div>
											  <div class="card-reveal overlay-blue">
												<span class="card-title close-button"><i class="fa fa-times"></i></span>
												<a class="posted-on" href="#">Photography</a>
												<h2 class="entry-title"><a href="#">Exploring the Product Design7</a></h2>
												<p>Authoritatively grow quality technologies for strategic sources. Dramatically evolve front-end services quality.</p>
												<a href="#" class="readmore">Read Full Post <i class="fa fa-long-arrow-right"></i></a>
											  </div>
											</article><!-- /.card -->
										  </div><!-- /.col-md-4 -->
									  </div>
									</div>
									<div class="item">
										<div class="row latest-news-card">
										  <div class="col-xs-12 col-sm-6 col-md-4">
											<article class="card">
											  <div class="card-image waves-effect waves-block waves-light">
												<img class="activator" src="<?php echo base_url('assets/img/blog/blog-18.jpg'); ?>" alt="image">
											  </div>
											  <div class="card-content">
												<h2 class="entry-title activator">Why Material Design is our best attempt for a unifying theory…</h2>
											  </div>
											  <div class="card-reveal overlay-blue">
												<span class="card-title close-button"><i class="fa fa-times"></i></span>
												<a class="posted-on" href="#">Branding</a>
												<h2 class="entry-title"><a href="#">Material Design is best</a></h2>
												<p>Authoritatively grow quality technologies for strategic sources. Dramatically evolve front-end services quality.</p>
												<a href="#" class="readmore">Read Full Post <i class="fa fa-long-arrow-right"></i></a>
											  </div>
											</article><!-- /.card -->
										  </div><!-- /.col-md-4 -->

										  <div class="col-xs-12 col-sm-6 col-md-4">
											<article class="card">
											  <div class="card-image waves-effect waves-block waves-light">
												<img class="activator" src="<?php echo base_url('assets/img/blog/1.jpg'); ?>" alt="image">
											  </div>
											  <div class="card-content">
												<h2 class="entry-title activator">How To Be More Organized While Designing UI ...</h2>
											  </div>
											  <div class="card-reveal overlay-blue">
												<span class="card-title close-button"><i class="fa fa-times"></i></span>
												<a class="posted-on" href="#">Branding</a>
												<h2 class="entry-title"><a href="#">How To Be More Organized</a></h2>
												<p>Authoritatively grow quality technologies for strategic sources. Dramatically evolve front-end services quality.</p>
												<a href="#" class="readmore">Read Full Post <i class="fa fa-long-arrow-right"></i></a>
											  </div>
											</article><!-- /.card -->
										  </div><!-- /.col-md-4 -->

										  <div class="col-xs-12 col-sm-6 col-md-4">
											<article class="card">
											  <div class="card-image waves-effect waves-block waves-light">
												<img class="activator" src="<?php echo base_url('assets/img/blog/3.jpg'); ?>" alt="image">
											  </div>
											  <div class="card-content">
												<h2 class="entry-title activator">Exploring the Product Design of the Stripe ...</h2>
											  </div>
											  <div class="card-reveal overlay-blue">
												<span class="card-title close-button"><i class="fa fa-times"></i></span>
												<a class="posted-on" href="#">Photography</a>
												<h2 class="entry-title"><a href="#">Exploring the Product Design7</a></h2>
												<p>Authoritatively grow quality technologies for strategic sources. Dramatically evolve front-end services quality.</p>
												<a href="#" class="readmore">Read Full Post <i class="fa fa-long-arrow-right"></i></a>
											  </div>
											</article><!-- /.card -->
										  </div><!-- /.col-md-4 -->
									  </div>
									</div>
								  </div>
								  
								  <!-- Controls 
								  <a class="left carousel-control" href="#testimonial-two" role="button" data-slide="prev">
									<span class="material-icons" aria-hidden="true"></span>
									<span class="sr-only">Previous</span>
								  </a>
								  <a class="right carousel-control" href="#testimonial-two" role="button" data-slide="next">
									<span class="material-icons" aria-hidden="true"></span>
									<span class="sr-only">Next</span>
								  </a>-->
								</div>
                            </div>
                          </div>
                        </div>
                      </div>
                      
                    </div><!-- /.icon-tab -->

                </div><!-- /.col-md-12 -->
              </div><!-- /.row -->

            </div><!-- /.container -->
        </section>


        <!--section class="gray-bg">
            <div class="container">
				<div class="text-center">
				  <h2 class="text-uppercase" style="padding: 15px;">Top Universities</h2>
				</div>
				<div class="row no-gutter">
					<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
						<figure class="cp-gallery">
							<span class="hover-ripple right" data-hover-color="#000" data-hover-opacity="0.6"></span>
							<img src="<?php echo base_url('assets/img/gallery/5.jpg'); ?>" class="img-responsive" alt="">
							<figcaption>
								<span class="text-uppercase">Montanna Bulevard</span>
								<h3 class="text-uppercase"><a href="#" class="white-link link-underline">United States</a></h3>
							</figcaption>
						</figure>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-3 col-lg-2">
						<figure class="cp-gallery">
							<span class="hover-ripple right" data-hover-color="#000" data-hover-opacity="0.6"></span>
							<img src="<?php echo base_url('assets/img/gallery/5.jpg'); ?>" class="img-responsive" alt="">
							<figcaption>
								<span class="text-uppercase">Montanna Bulevard</span>
								<h3 class="text-uppercase"><a href="#" class="white-link link-underline">United States</a></h3>
							</figcaption>
						</figure>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-3 col-lg-2">
						<figure class="cp-gallery">
							<span class="hover-ripple right" data-hover-color="#000" data-hover-opacity="0.6"></span>
							<img src="<?php echo base_url('assets/img/gallery/6.jpg'); ?>" class="img-responsive" alt="">
							<figcaption>
								<span class="text-uppercase">Montanna Bulevard</span>
								<h3 class="text-uppercase"><a href="#" class="white-link link-underline">United States</a></h3>
							</figcaption>
						</figure>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-3 col-lg-2">
						<figure class="cp-gallery">
							<span class="hover-ripple right" data-hover-color="#000" data-hover-opacity="0.6"></span>
							<img src="<?php echo base_url('assets/img/gallery/7.jpg'); ?>" class="img-responsive" alt="">
							<figcaption>
								<span class="text-uppercase">Montanna Bulevard</span>
								<h3 class="text-uppercase"><a href="#" class="white-link link-underline">United States</a></h3>
							</figcaption>
						</figure>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-3 col-lg-2">
						<figure class="cp-gallery">
							<span class="hover-ripple right" data-hover-color="#000" data-hover-opacity="0.6"></span>
							<img src="<?php echo base_url('assets/img/gallery/8.jpg'); ?>" class="img-responsive" alt="">
							<figcaption>
								<span class="text-uppercase">Montanna Bulevard</span>
								<h3 class="text-uppercase"><a href="#" class="white-link link-underline">United States</a></h3>
							</figcaption>
						</figure>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-3 col-lg-2">
						<figure class="cp-gallery">
							<span class="hover-ripple right" data-hover-color="#000" data-hover-opacity="0.6"></span>
							<img src="<?php echo base_url('assets/img/gallery/9.jpg'); ?>" class="img-responsive" alt="">
							<figcaption>
								<span class="text-uppercase">Montanna Bulevard</span>
								<h3 class="text-uppercase"><a href="#" class="white-link link-underline">United States</a></h3>
							</figcaption>
						</figure>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-3 col-lg-2">
						<figure class="cp-gallery">
							<span class="hover-ripple right" data-hover-color="#000" data-hover-opacity="0.6"></span>
							<img src="<?php echo base_url('assets/img/gallery/10.jpg'); ?>" class="img-responsive" alt="">
							<figcaption>
								<span class="text-uppercase">Montanna Bulevard</span>
								<h3 class="text-uppercase"><a href="#" class="white-link link-underline">United States</a></h3>
							</figcaption>
						</figure>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-3 col-lg-2">
						<figure class="cp-gallery">
							<span class="hover-ripple right" data-hover-color="#000" data-hover-opacity="0.6"></span>
							<img src="<?php echo base_url('assets/img/gallery/11.jpg'); ?>" class="img-responsive" alt="">
							<figcaption>
								<span class="text-uppercase">Montanna Bulevard</span>
								<h3 class="text-uppercase"><a href="#" class="white-link link-underline">United States</a></h3>
							</figcaption>
						</figure>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-3 col-lg-2">
						<figure class="cp-gallery">
							<span class="hover-ripple right" data-hover-color="#000" data-hover-opacity="0.6"></span>
							<img src="<?php echo base_url('assets/img/gallery/12.jpg'); ?>" class="img-responsive" alt="">
							<figcaption>
								<span class="text-uppercase">Montanna Bulevard</span>
								<h3 class="text-uppercase"><a href="#" class="white-link link-underline">United States</a></h3>
							</figcaption>
						</figure>
					</div>
				</div>
            </div>
        </section-->
		<?php
$data_ar = $top_uni;//your array here;
$temp_ar = array();

foreach($data_ar as $value)
{
    $tempCheck = 0;
    foreach($temp_ar as $val)
    {
        if($value['um_name'] == $val['um_name']) // whatever the item value you want to check wheather its uc_um_id or um_name
        {
            $tempCheck = 1;
        }
    }
    if($tempCheck === 0)
    $temp_ar[] = $value;
}

 ?>
             <section class="gray-bg">
            <div class="container">
				<div class="text-center mb-40 wow fadeInUp" style="visibility: visible;">
				  <h3 class="text-uppercase text-center text-bold mb-40" style="padding: 15px;">Top Universities</h3>
				</div>
				<div class="row no-gutter">
					<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
						<figure class="cp-gallery">
							<span class="hover-ripple right" data-hover-color="#000" data-hover-opacity="0.6"></span>
							<img src="<?php echo base_url('assets/img/gateway.jpg'); ?>" class="img-responsive" alt="">
							<figcaption>
								<span class="text-uppercase">INDIA</span>
								<h3 class="text-uppercase"><a href="#" class="white-link link-underline">India</a></h3>
							</figcaption>
						</figure>
					</div>
					<!--Start foreach-->
					<?php //for($i=0;$i<count($unique_data);$i++) {
			
						foreach ($temp_ar as  $value) {

						?>
					<div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
						<figure class="cp-gallery">
							<span class="hover-ripple right" data-hover-color="#000" data-hover-opacity="0.6"></span>
							<img src="<?php echo base_url(''); ?>uploads/coverimages/<?php echo $value['ui_coverimage'];?>" class="img-responsive" alt="" style="width:195px;height:163px">
							<figcaption>
								<span class="text-uppercase white-link" style="color:white"><a href="<?php echo base_url()?>listing/index/<?php echo $value['um_name']; ?>" class="white-link link-underline"><?php echo $value['um_name'];?></a></span>
								<h3 class="text-uppercase"><a href="<?php echo base_url()?>listing/index/<?php echo $value['um_name']; ?>" class="white-link link-underline"></a></h3>
							</figcaption>
						</figure>
					</div>
					<?php }?>
				<!-- 	<div class="col-xs-12 col-sm-6 col-md-3 col-lg-2">
						<figure class="cp-gallery">
							<span class="hover-ripple right" data-hover-color="#000" data-hover-opacity="0.6"></span>
							<img src="<?php echo base_url('assets/img/gallery/6.jpg'); ?>" class="img-responsive" alt="">
							<figcaption>
								<span class="text-uppercase">Montanna Bulevard</span>
								<h3 class="text-uppercase"><a href="#" class="white-link link-underline">United States</a></h3>
							</figcaption>
						</figure>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-3 col-lg-2">
						<figure class="cp-gallery">
							<span class="hover-ripple right" data-hover-color="#000" data-hover-opacity="0.6"></span>
							<img src="<?php echo base_url('assets/img/gallery/7.jpg'); ?>" class="img-responsive" alt="">
							<figcaption>
								<span class="text-uppercase">Montanna Bulevard</span>
								<h3 class="text-uppercase"><a href="#" class="white-link link-underline">United States</a></h3>
							</figcaption>
						</figure>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-3 col-lg-2">
						<figure class="cp-gallery">
							<span class="hover-ripple right" data-hover-color="#000" data-hover-opacity="0.6"></span>
							<img src="<?php echo base_url('assets/img/gallery/8.jpg'); ?>" class="img-responsive" alt="">
							<figcaption>
								<span class="text-uppercase">Montanna Bulevard</span>
								<h3 class="text-uppercase"><a href="#" class="white-link link-underline">United States</a></h3>
							</figcaption>
						</figure>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-3 col-lg-2">
						<figure class="cp-gallery">
							<span class="hover-ripple right" data-hover-color="#000" data-hover-opacity="0.6"></span>
							<img src="<?php echo base_url('assets/img/gallery/9.jpg'); ?>" class="img-responsive" alt="">
							<figcaption>
								<span class="text-uppercase">Montanna Bulevard</span>
								<h3 class="text-uppercase"><a href="#" class="white-link link-underline">United States</a></h3>
							</figcaption>
						</figure>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-3 col-lg-2">
						<figure class="cp-gallery">
							<span class="hover-ripple right" data-hover-color="#000" data-hover-opacity="0.6"></span>
							<img src="<?php echo base_url('assets/img/gallery/10.jpg'); ?>" class="img-responsive" alt="">
							<figcaption>
								<span class="text-uppercase">Montanna Bulevard</span>
								<h3 class="text-uppercase"><a href="#" class="white-link link-underline">United States</a></h3>
							</figcaption>
						</figure>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-3 col-lg-2">
						<figure class="cp-gallery">
							<span class="hover-ripple right" data-hover-color="#000" data-hover-opacity="0.6"></span>
							<img src="<?php echo base_url('assets/img/gallery/11.jpg'); ?>" class="img-responsive" alt="">
							<figcaption>
								<span class="text-uppercase">Montanna Bulevard</span>
								<h3 class="text-uppercase"><a href="#" class="white-link link-underline">United States</a></h3>
							</figcaption>
						</figure>
					</div>
 -->
					<!--end foreach-->
					<!-- <div class="col-xs-12 col-sm-6 col-md-3 col-lg-2">
						<figure class="cp-gallery">
							<span class="hover-ripple right" data-hover-color="#000" data-hover-opacity="0.6"></span>
							<img src="<?php echo base_url('assets/img/gallery/12.jpg'); ?>" class="img-responsive" alt="">
							<figcaption>
								<span class="text-uppercase">Montanna Bulevard</span>
								<h3 class="text-uppercase"><a href="#" class="white-link link-underline">United States</a></h3>
							</figcaption>
						</figure>
					</div> -->
				</div>
            </div><!-- /.container -->
        </section>
		<section id="about" class="section-padding ">
            <div class="container">

              <div class="text-center mb-80 wow fadeInUp" style="visibility: visible;">
                  <h3 class="text-uppercase text-bold mb-40">Top Courses</h3>
                 
              </div>

              <div class="vertical-tab">
                <div class="row">
                  <div class="col-sm-3">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs nav-stacked hidden-xs" role="tablist">
                          <!-- <li role="presentation" class="active"><a href="#tab-5" class="waves-effect waves-light js-tabcollapse-panel-heading" role="tab" data-toggle="tab" data-parent="" onclick='get_top_city(1851)'>Pune</a></li>
                          <li role="presentation"><a href="#tab-6" class="waves-effect waves-light js-tabcollapse-panel-heading" role="tab" data-toggle="tab" data-parent="" onclick='get_top_city()'>Mumbai</a></li>
                          <li role="presentation"><a href="#tab-7" class="waves-effect waves-light js-tabcollapse-panel-heading" role="tab" data-toggle="tab" data-parent="" onclick='get_top_city()'>Delhi</a></li>
                          <li role="presentation"><a href="#tab-8" class="waves-effect waves-light js-tabcollapse-panel-heading" role="tab" data-toggle="tab" data-parent="" onclick='get_top_city()'>Banglore</a></li>
                          <li role="presentation"><a href="#tab-9" class="waves-effect waves-light js-tabcollapse-panel-heading" role="tab" data-toggle="tab" data-parent="" onclick='get_top_city()'>Hyderabad</a></li> -->
                          <?php $count=5;
                        		foreach ($top_cities as $city) {
                        			$active='';
                        			//echo  $city->city;
                        			if($city->city==='Pune'){
                        				$active.="active";
                        			}
                        		?>
                        		<li role="presentation" class="<?php echo $active;?>"><a href="#tab-<?php echo $count++?>" class="waves-effect waves-light js-tabcollapse-panel-heading" role="tab" data-toggle="tab" data-parent="" onclick='get_top_city(<?php echo $city->city_id?>)'><?php echo $city->city?></a></li>
                        		<?php
                        		}
                        	?>
                          <li role="presentation"><a href="<?php echo base_url() ?>listing/index/Management" class="waves-effect waves-light js-tabcollapse-panel-heading"  style="background-color:#3f51b5">view all &nbsp;&nbsp;&nbsp;<i class="fa fa-long-arrow-right" aria-hidden="true"></i></a></li> 
                        </ul><div class="panel-group visible-xs" id="undefined-accordion"></div>                      
                  </div><!-- /.col-md-3 -->

                  <div class="col-sm-9">
                        <!-- Tab panes -->
                        <div class="panel-body">
                          <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="tab-5">
                 				
                        	</div>
                            <div role="tabpanel" class="tab-pane fade" id="tab-6">
    
                            
                        	</div>
                            <div role="tabpanel" class="tab-pane fade" id="tab-7">
                             
                         	</div>
                            <div role="tabpanel" class="tab-pane fade" id="tab-8">
                           
                            
                        	</div>
                            <div role="tabpanel" class="tab-pane fade" id="tab-9">
                             
                         	</div>
                          </div>
                        </div>
                  </div>
                </div><!-- /.row -->
              </div><!-- /.vertical-tab -->

            </div><!-- /.container -->
        </section>
        <section id="about" class="gray-bg">
            <div class="container">

              <div class="text-center mb-80 wow fadeInUp" style="visibility: visible;">
                  <h3 class="text-uppercase text-bold mb-40" style="padding: 15px;">Top University</h3>
                 
              </div>

              <div class="vertical-tab">
                <div class="row">
                  <div class="col-sm-3">
                        <!-- Nav tabs -->
                      <!--   <ul class="nav nav-tabs nav-stacked hidden-xs" role="tablist">
                        	
                          <li role="presentation" class="active"><a href="#tab-10" class="waves-effect waves-light js-tabcollapse-panel-heading" role="tab" data-toggle="tab" data-parent="" onclick='get_university_city(1851)'>Pune</a></li>
                          <li role="presentation"><a href="#tab-11" class="waves-effect waves-light js-tabcollapse-panel-heading" role="tab" data-toggle="tab" data-parent="" onclick='get_university_city()'>Mumbai</a></li>
                          <li role="presentation"><a href="#tab-12" class="waves-effect waves-light js-tabcollapse-panel-heading" role="tab" data-toggle="tab" data-parent="" onclick='get_university_city()'>Delhi</a></li>
                          <li role="presentation"><a href="#tab-13" class="waves-effect waves-light js-tabcollapse-panel-heading" role="tab" data-toggle="tab" data-parent="" onclick='get_university_city()'>Banglore</a></li>
                          <li role="presentation"><a href="#tab-14" class="waves-effect waves-light js-tabcollapse-panel-heading" role="tab" data-toggle="tab" data-parent="" onclick='get_university_city()'>Hyderabad</a></li>
                          
                        </ul><div class="panel-group visible-xs" id="undefined-accordion"></div>    -->  
                         <ul class="nav nav-tabs nav-stacked hidden-xs" role="tablist">
                        	<?php //echo'<pre>',print_r($top_cities);?>
                        	<?php $count=5;
                        		foreach ($top_cities as $city) {
                        			$active='';
                        			//echo  $city->city;
                        			if($city->city==='Pune'){
                        				$active.="active";
                        			}
                        		?>
                        		<li role="presentation" class="<?php echo $active;?>"><a href="#tab-<?php echo $count++?>" class="waves-effect waves-light js-tabcollapse-panel-heading" role="tab" data-toggle="tab" data-parent="" onclick='get_university_city(<?php echo $city->city_id?>)'><?php echo $city->city?></a></li>
                        		<?php
                        		}
                        	?>
                          <li role="presentation"><a href="<?php echo base_url() ?>explore/university_view/<?php  echo $top_cities[0]->city_id?>" class="waves-effect waves-light js-tabcollapse-panel-heading" style="background-color:#3f51b5">view all &nbsp;&nbsp;&nbsp;<i class="fa fa-long-arrow-right" aria-hidden="true"></i></a></li> 
                          <!-- <li role="presentation"><a href="#tab-6" class="waves-effect waves-light js-tabcollapse-panel-heading" role="tab" data-toggle="tab" data-parent="" onclick='get_top_city()'>Mumbai</a></li>
                          <li role="presentation"><a href="#tab-7" class="waves-effect waves-light js-tabcollapse-panel-heading" role="tab" data-toggle="tab" data-parent="" onclick='get_top_city()'>Delhi</a></li>
                          <li role="presentation"><a href="#tab-8" class="waves-effect waves-light js-tabcollapse-panel-heading" role="tab" data-toggle="tab" data-parent="" onclick='get_top_city()'>Banglore</a></li>
                          <li role="presentation"><a href="#tab-9" class="waves-effect waves-light js-tabcollapse-panel-heading" role="tab" data-toggle="tab" data-parent="" onclick='get_top_city()'>Hyderabad</a></li> -->
                          
                        </ul>                 
                  </div><!-- /.col-md-3 -->

                  <div class="col-sm-9">
                        <!-- Tab panes -->
                        <div class="panel-body">
                          <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="tab-10">
                 				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3" style="padding-left:0px;padding-right: 2px;">
									<figure class="cp-gallery">
										<span class="hover-ripple right" data-hover-color="#000" data-hover-opacity="0.6"></span>
										<img src="<?php echo base_url('assets/img/gateway.jpg'); ?>" class="img-responsive" alt="">
										<figcaption>
											<span class="text-uppercase">Montanna Bulevard</span>

										</figcaption>
									</figure>
								</div>
							
                        	</div>
                            <div role="tabpanel" class="tab-pane fade" id="tab-11">
    
                            
                        	</div>
                            <div role="tabpanel" class="tab-pane fade" id="tab-12">
                             
                         	</div>
                            <div role="tabpanel" class="tab-pane fade" id="tab-13">
                           
                            
                        	</div>
                            <div role="tabpanel" class="tab-pane fade" id="tab-14">
                             
                         	</div>
                          </div>
                        </div>
                  </div>
                </div><!-- /.row -->
              </div><!-- /.vertical-tab -->

            </div><!-- /.container -->
        </section>
		<section class="section-padding ">
          <div class="container">
              <div class="text-center mb-40 wow fadeInUp" style="visibility: visible;" >
                  <h3 class="text-uppercase text-bold mb-40">Student Testimonial </h3>
                  <div class="icon">
                            <img src="<?php echo base_url();?>assets/img/quote-dark.png" alt="Customer Thumb" draggable="false">
                        </div>
              </div>
<?php if(!empty($testimonial));?>
              <div class="row">
                <div class="col-md-10 col-md-offset-1">

                  <div class="thumb-carousel square-thumb text-center">
                    <ul class="slides">
                 <?php foreach ($testimonial as $value) {
            
                  ?>
                     
                      <li>
                  <div class="content">
                            <p><?php echo $value->testimonial;?></p>

                            <div class="testimonial-meta">
                                <?php echo $value->name;?>
                                <span>- Student</span>
                            </div>
                        </div>
                      </li>
                      <?php } ?>
                     <!--  <li>
                        <div class="content">
                            <p>Vivamus magna a nulla felis porttitor praesent ullamcorper ut sed porttitor parturient per a sapien at sed quisque vestibulum. Parturient ipsum parturient a condimentum consequat donec dictum facilisis consectetur molestie lobortis parturient ullamcorper conubia. Pretium quisque morbi adipiscing .</p>

                            <div class="testimonial-meta">
                                Jonathan Doe
                                <span>Funder of TrendyTheme</span>
                            </div>
                        </div>
                      </li>
                      <li>
                        <div class="content">
                            <p>Vivamus magna a nulla felis porttitor praesent ullamcorper ut sed porttitor parturient per a sapien at sed quisque vestibulum. Parturient ipsum parturient a condimentum consequat donec dictum facilisis consectetur molestie lobortis parturient ullamcorper conubia. Pretium quisque morbi adipiscing .</p>

                            <div class="testimonial-meta">
                                Jonathan Doe
                                <span>Funder of TrendyTheme</span>
                            </div>
                        </div>
                      </li>
                      <li>
                        <div class="content">
                            <p>Vivamus magna a nulla felis porttitor praesent ullamcorper ut sed porttitor parturient per a sapien at sed quisque vestibulum. Parturient ipsum parturient a condimentum consequat donec dictum facilisis consectetur molestie lobortis parturient ullamcorper conubia. Pretium quisque morbi adipiscing .</p>

                            <div class="testimonial-meta">
                                Jonathan Doe
                                <span>Funder of TrendyTheme</span> 
                            </div>
                        </div>
                      </li> -->
                    </ul>
                  </div>

                </div>
              </div><!-- /.row -->

          </div><!-- /.container -->
        </section>

</div>


 <footer class="footer footer-four">
            <div class="primary-footer brand-bg text-center">
                <div class="container">

                  <ul class="social-link tt-animate ltr mt-20">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-tumblr"></i></a></li>
                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                    <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                    <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                    <li><a href="#"><i class="fa fa-rss"></i></a></li>
                  </ul>

                  <hr class="mt-15">

                  <div class="row">
                    <div class="col-md-12">
                          <div class="footer-logo">
                            <img src="<?php echo base_url();?>assets/img/logo-01.png" alt="">
                          </div>

                          <span class="copy-text"> &copy; 2017-18 <a href="#">Hello Admission</a> &nbsp; | &nbsp;  All Rights Reserved &nbsp; | &nbsp;  Designed By <a href="#">Infinite1</a></span>
                          <div class="footer-intro">
                            <p></p>
                          </div>
                    </div><!-- /.col-md-12 -->
                  </div><!-- /.row -->
                </div><!-- /.container -->
            </div><!-- /.primary-footer -->

            <div class="secondary-footer brand-bg darken-2 text-center">
                <div class="container">
                    <ul>
                      <li><a href="#">Home</a></li>
                      <li><a href="#">About us</a></li>
                      <li><a href="#">Services</a></li>
                      <li><a href="#">Portfolio</a></li>
                      <li><a href="#">Contact us</a></li>
                    </ul>
                </div><!-- /.container -->
            </div><!-- /.secondary-footer -->
        </footer>


	</div>
	<script src="<?php echo base_url('assets/js/matexjs/scripts.minified.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/matexjs/main.js'); ?>"></script>
    
	<?php 

	/* Course Master Search Data START */
	
	$query = $this->db->query("SELECT * FROM master_program");
	$course_mstr = $query->result();
	
	$new_crs_mstr = array();  
	foreach($course_mstr as $crs_mstr){
		$crsSearch = trim($crs_mstr->name);
		array_push($new_crs_mstr,$crsSearch);
	}
	$new_crs_mstr = array_unique($new_crs_mstr);
	$newCMstr = implode('", "',$new_crs_mstr);
	
	/* Course Master Search Data END */
	
	/* University Master Search Data START */
	
	$query = $this->db->query("SELECT * FROM uni_mstr");
	$uni_mstr = $query->result();
			
  
	$new_uni_mstr = array();  
	foreach($uni_mstr as $u_mstr){
		$uniSearch = trim($u_mstr->um_name);
		array_push($new_uni_mstr,$uniSearch);
	}
	  
	$new_uni_mstr = array_unique($new_uni_mstr);
	  
	$newUMstr = implode('", "',$new_uni_mstr);
	  
	/* University Master Search Data START */

	//spelizatiopn
	  $query1 = $this->db->query("SELECT * FROM master_specialization");
	$uni_mstr1 = $query1->result();
			
  
	$new_uni_mstr1 = array();  
	foreach($uni_mstr1 as $u_mstr1){
		$uniSearch1 = trim($u_mstr1->name);
		array_push($new_uni_mstr1,$uniSearch1);
	}
	  
	$new_uni_mstr1 = array_unique($new_uni_mstr1);
	  
	$newUMstr1 = implode('", "',$new_uni_mstr1);
  ?>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> 
<script type="text/javascript">
	
    var availableTags = ["<?php echo $newCMstr; ?>","<?php echo $newUMstr; ?>","<?php echo $newUMstr1; ?>"];
//var availableTags = ["<?php echo $newCMstr; ?>"];

	
    $( "#search_text" ).autocomplete({
      source: availableTags,
	  select: function(event, ui) {
		//  alert(ui.item.value);
			window.location.href = "<?php echo base_url(); ?>listing/index/"+ui.item.value;
	  }
    });
	
	$( ".swapbutton" ).click(function(){
		
		var swvl = $(this).attr('id');
		
		if(swvl == 'explore'){
			$('.explore').show();
			$('.search').hide();
		}else{
			$('.explore').hide();
			$('.search').show();
		}
		
	});
	
</script>
<script type="text/javascript">
    function getCource(id){
$('#icontab-1').empty();
$('#icontab-2').empty();
    	$.ajax({
    		type:'POST',
    		url:'<?php echo base_url()?>explore/get_top_cource',
    		data:{'id':id},
    		success:function(res){
    			var obj=JSON.parse(res);
    			if (obj.length==0) {
					  html+= '<p>No Record Found</p>';
					  console.log(html);
					}
    			var html='<div class="row"><div id="testimonial-two" class="carousel slide carousel-testimonial text-center" data-ride="carousel"><div class="carousel-inner" role="listbox"><div class="item active"><div class="row latest-news-card">';
    			$.each(obj,function(i,item){
    				console.log(item);
    			var cource='';
					if(item.course_spe==null){
						cource='-';
					}else if(item.course_spe!=null){
						cource='-'+item.course_spe;
					}
    				html+='<div class="col-xs-12 col-sm-6 col-md-4">'+
						  '<article class="card">'+
							'<div class="card-image waves-effect waves-block waves-light">'+
							'<img class="activator" src="<?php echo base_url("assets/img/blog/blog-18.jpg"); ?>" alt="image">'+
											  '</div>'+
											  '<div class="card-content">'+
												'<h4 class="entry-title activator"><b>'+item.spe+'</b> '+cource+'</h4>'+
											  '</div>'+
											  '<div class="card-reveal overlay-blue">'+
												'<span class="card-title close-button"><i class="fa fa-times"></i></span>'+
												'<a class="posted-on" href="#">'+item.spe+'</a>'+
												'<h2 class="entry-title"><a href="#">'+cource+'</a></h2>'+
												'<p>'+item.uc_info+'</p>'+
												'<a href="<?php echo base_url()?>listing/view_cource/'+item.uc_id+'" class="readmore">View <i class="fa fa-long-arrow-right"></i></a>'+
											  '</div>'+
											'</article>'+
										  '</div>';
    			});
    			html+=' </div></div></div></div></div>';
    			$('#icontab-1').html(html);

    			$('#icontab-2').html(html);
    			$('#icontab-3').html(html);
    		}
    	});
    }
        function get_top_city(id){
	$('#tab-5').empty();
//$('#icontab-2').empty();
    	$.ajax({
    		type:'POST',
    		url:'<?php echo base_url()?>explore/get_top_city',
    		data:{'id':id},
    		success:function(res){
    			var obj=JSON.parse(res);
    			//console.log(obj);
    			var html='';
    			if (obj.length==0) {
					  html+= '<p>No University Found</p>';
					  console.log(html);
					}
    			$.each(obj,function(i,item){
    				console.log(item);
    			var cource='';
					if(item.course_spe==null){
						cource='-';
					}else if(item.course_spe!=null){
						cource='-'+item.course_spe;
					}
    			
						html+='<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3" style="padding-left:0px;padding-right: 2px;">'+
									'<figure class="cp-gallery">'+
										'<span class="hover-ripple right" data-hover-color="#000" data-hover-opacity="0.6"></span>'+
										'<img src="<?php echo base_url(); ?>assets/img/blog/blog-18.jpg" class="img-responsive" alt="" style="width:210px;height:163px">'+
										'<figcaption>'+
											'<span class="text-uppercase"><a href="<?php echo base_url()?>listing/view_cource/'+item.uc_id+'" class="white-link link-underline">'+item.spe+' '+cource+'</a></span>'+

										'</figcaption>'+
									'</figure>'+
								'</div>'
    			});
    			//html+=' </div></div></div></div></div>';
    			$('#tab-5').html(html);

    			//$('#icontab-2').html(html);
    			//$('#icontab-3').html(html);
    		}
    	});
    }

       function get_university_city(id){
	$('#tab-10').empty();
//$('#icontab-2').empty();
    	$.ajax({
    		type:'POST',
    		url:'<?php echo base_url()?>explore/get_top_city_view',
    		data:{'id':id},
    		success:function(res){
    			var obj=JSON.parse(res);
    			//console.log(obj);
    			var html='';
    			$.each(obj,function(i,item){
    				console.log(item);
    			var cource='';
					if(item.course_spe==null){
						cource='-';
					}else if(item.course_spe!=null){
						cource='-'+item.course_spe;
					}
    			
						html+='<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3" style="padding-left:0px;padding-right: 2px;">'+
									'<figure class="cp-gallery">'+
										'<span class="hover-ripple right" data-hover-color="#000" data-hover-opacity="0.6"></span>'+
										'<img src="<?php echo base_url(); ?>uploads/coverimages/'+item.ui_coverimage+'" class="img-responsive" alt="" style="width:210px;height:163px">'+
										'<figcaption>'+
											'<span class="text-uppercase"><a href="<?php echo base_url()?>listing/index/'+item.um_name+'" class="white-link link-underline">'+item.um_name+'</a></span>'+

										'</figcaption>'+
									'</figure>'+
								'</div>'
    			});
    			//html+=' </div></div></div></div></div>';
    			$('#tab-10').html(html);

    			//$('#icontab-2').html(html);
    			//$('#icontab-3').html(html);
    		}
    	});
    }
    </script>
	<script>
	getCource(2);
	get_top_city(1851);
	get_university_city(1851);
            (function($) {
                $(".s-service-carousel-for").slick({
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                    vertical: true,
                    autoplay: true,
                    asNavFor: ".s-service-carousel-nav",
                    focusOnSelect: true,
                    responsive: [
                      {
                        breakpoint: 767,
                        settings: {
                            dots: true,
                            vertical: false
                        }
                      },
                    ]
                });
                $(".s-service-carousel-nav").slick({
                    slidesToShow: 5,
                    slidesToScroll: 1,
                    focusOnSelect: true,
                    asNavFor: ".s-service-carousel-for",
                    vertical: true,
                    autoplay: true,
                    arrows: false,
                    centerMode: true,
                    centerPadding: 0,
                });
            })(jQuery);
        </script>
	<script type="text/javascript">
$(document).ready(function(){
$('#select_education').change(function(){
	//var cate_id = $(this).val();
	var cate_id  = $(this).val().split('-');
	//alert(type);
	console.log(cate_id[0]);
	$('#select_stream').empty();
	$.ajax({
		type: "POST",
		url: "<?php echo base_url('explore/education_category'); ?>",
		data:{cate_id:cate_id[1]},
		success: function(response){
			var res = JSON.parse(response);
			$('#select_stream').append('<option value="">Choose stream</option>');
			$.each(res, function (i, itm) {
				// $('#select_course').append($('<option>', { 
				// 	value: itm.id,
				// 	text : itm.name 
				// }));
			$('#select_stream').append('<option value="'+itm.id+'-'+itm.name+'">'+itm.name+'</option>');

			});
		}
	});
});

			

$('#select_stream').change(function(){
	var streamid = $(this).val();
	//alert(streamid);
	$('#select_course').empty();
	$.ajax({
		type: "POST",
		url: "<?php echo base_url('explore/stream_program'); ?>",
		data:{streamid:streamid},
		success: function(response){
			var res = JSON.parse(response);
			$('#select_course').append('<option value="">Choose cource</option>');
			$.each(res, function (i, itm) {
				// $('#select_course').append($('<option>', { 
				// 	value: itm.id,
				// 	text : itm.name 
				// }));
			$('#select_course').append('<option value="'+itm.id+'-'+itm.name+'">'+itm.name+'</option>');

			});
		}
	});
});
			$('.button-collapse').sideNav({edge:'left'});
			$('#div2').hide();
			$('#div3').hide();
			$('#div4').hide();
			$('#div5').hide();
			$('#div6').hide();
			$('#div7').hide();
			$('#div8').hide();
			$('#div9').hide();
			$('#div10').hide();
			$('#div11').hide();
			$('#div12').hide();
			$('#div13').hide();
			$('#div14').hide();
			$('#div15').hide();
			//~ if(jQuery.browser.mobile)
			//~ {
			   
			   //~ $('.BackgroundImg').addClass('noBackground');
			   //~ $('.noBackground').removeClass('BackgroundImg');
			   //~ $('.noBackground').css('background-color','#47494b');
			//~ }
			//~ else
			//~ {
			   $('.BackgroundImg').css('background-image','url("<?php echo base_url()?>assets/explore/images/frontend/howareyoubg.jpg")');
			//~ }
		});
		
		jQuery("#div1 #div1-content").click(function(){
			jQuery(".template_section").hide();
		});
		
	</script>
	<script type="text/javascript" src="<?php echo base_url('assets/explore/js/frontend/frontend_quiz.js');?>"></script>
	
	    <script src="<?php echo base_url('assets_new/js/jquery-2.1.3.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets_new/bootstrap/js/bootstrap.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets_new/materialize/js/materialize.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets_new/js/jquery.easing.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets_new/js/jquery.sticky.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets_new/js/smoothscroll.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets_new/js/imagesloaded.js'); ?>"></script>
        <script src="<?php echo base_url('assets_new/js/jquery.stellar.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets_new/js/jquery.inview.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets_new/js/jquery.shuffle.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets_new/js/menuzord.js'); ?>"></script>
        <script src="<?php echo base_url('assets_new/js/bootstrap-tabcollapse.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets_new/owl.carousel/owl.carousel.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets_new/flexSlider/jquery.flexslider-min.js'); ?>"></script>
        <script src="<?php echo base_url('assets_new/magnific-popup/jquery.magnific-popup.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets_new/js/scripts.js'); ?>"></script>
        
        
    </body>
 </html>
