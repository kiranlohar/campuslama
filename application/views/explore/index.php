<!DOCTYPE html>
  <html>
    <head>
    <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <meta name="robots" content="noindex, nofollow" />
     
           <!--  favicon -->
       <link rel="shortcut icon" href="<?php echo base_url('assets/img/fav.png'); ?>">
        <!--  apple-touch-icon -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url('assets/img/ico/apple-touch-icon-144-precomposed.png'); ?>">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/img/ico/apple-touch-icon-114-precomposed.png'); ?>">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/img/ico/apple-touch-icon-72-precomposed.png'); ?>">
        <link rel="apple-touch-icon-precomposed" href="assets/img/ico/apple-touch-icon-57-precomposed.png'); ?>">

    <link href="<?php echo base_url('assets/explore/css/animate.css');?>" type="text/css" rel="stylesheet">
    
    <link href="<?php echo base_url('assets/explore/css/ion.rangeSlider.css');?>" type="text/css" rel="stylesheet"/>
    <link href="<?php echo base_url('assets/explore/css/ion.rangeSlider.skinHTML5.css');?>" type="text/css" rel="stylesheet"/>
    
    <link href="<?php echo base_url('assets/explore/css/frontend/quiz_mycustom.css');?>" type="text/css" rel="stylesheet">
    <link href="<?php echo base_url('assets/explore/css/frontend/quiz_mycustom_media.css');?>" type="text/css" rel="stylesheet">
    
    <link rel="stylesheet" href="<?php echo base_url('assets/css/matexcss/style.minified.css');?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/css/matexcss/main.css');?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/css/matexcss/style.html');?>">
          <link rel="stylesheet" href="<?php echo base_url('assets/css/matexcss/tabs.css');?>">
    <!-- Add the slick-theme.css if you want default styling -->
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.css"/>
<!-- Add the slick-theme.css if you want default styling -->
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick-theme.css"/>
     <script type="text/javascript" src="<?php echo base_url('assets/explore/js/jquery-2.1.4.min.js');?>"></script>
  
    <script type="text/javascript" src="<?php echo base_url('assets/explore/js/ion.rangeSlider.js');?>"></script>
    
    <script type="text/javascript" src="<?php echo base_url('assets/explore/js/frontend/mobile_detect.js');?>"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.min.js"></script>
    
    </head>
<style>
#mobilenumberdiv {
    background-color: #fff;
}
.card-panel {
    padding: 40px !important;
}
.icon-tab .nav>li>a {
    width: 100%;
    height: 100%;
}
.flex-control-thumbs{
  display:none;
}
.flex-direction-nav{
  display:none;
}
.no-gutter>[class^="col"] {
    padding-left: 10px;
    padding-bottom: 10px;
}
#ui-id-1{
    background: #fff;
    width: 43%;
    padding: 8px;
  max-height: 300px;
    overflow-y: auto;
}
.ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active {
  padding: 7px;
    background: #03205c;
    color: #fff;
}
.hover-ripple {
    background-color: #2f2e2d7a;
    border-top-right-radius: 100px;
    bottom: 0;
    height: 100px;
    left: 0;
    position: absolute;
    -webkit-transform: scale(0);
    -moz-transform: scale(0);
    -ms-transform: scale(0);
    -o-transform: scale(0);
    transform: scale(0);
    -webkit-transform-origin: left bottom 0;
    -moz-transform-origin: left bottom 0;
    -ms-transform-origin: left bottom 0;
    -o-transform-origin: left bottom 0;
    transform-origin: left bottom 0;
    -webkit-transition: all 0.4s cubic-bezier(0.4, 0, 0.2, 1) 0s;
    -moz-transition: all 0.4s cubic-bezier(0.4, 0, 0.2, 1) 0s;
    -o-transition: all 0.4s cubic-bezier(0.4, 0, 0.2, 1) 0s;
    transition: all 0.4s cubic-bezier(0.4, 0, 0.2, 1) 0s;
    width: 100px;
}
/* carousel */
.media-carousel 
{
  margin-bottom: 0;
  padding: 0 40px 30px 40px;
  margin-top: 30px;
}
/* Previous button  */
.media-carousel .carousel-control.left 
{
 // left: -12px;
  background-image: none;
  background: none repeat scroll 0 0 #222222;
  border: 4px solid #FFFFFF;
  border-radius: 23px 23px 23px 23px;
  height: 40px;
  width : 40px;
  margin-top: 60px
}
/* Next button  */
.media-carousel .carousel-control.right 
{
  //right: -12px !important;
  background-image: none;
  background: none repeat scroll 0 0 #222222;
  border: 4px solid #FFFFFF;
  border-radius: 23px 23px 23px 23px;
  height: 40px;
  width : 40px;
  margin-top: 60px
}
/* Changes the position of the indicators */
.media-carousel .carousel-indicators 
{
  right: 50%;
  top: auto;
  bottom: 0px;
  margin-right: -19px;
}
/* Changes the colour of the indicators */
.media-carousel .carousel-indicators li 
{
  background: #c0c0c0;
}
.media-carousel .carousel-indicators .active 
{
  background: #333333;
}
.media-carousel img
{
  width: 250px;
  height: 100px
}
@import "https://fonts.googleapis.com/css?family=Raleway:400,300,200,500,600,700";
.fa-spin-fast {
  -webkit-animation: fa-spin-fast 0.2s infinite linear;
  animation: fa-spin-fast 0.2s infinite linear;
}
@-webkit-keyframes fa-spin-fast {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(359deg);
    transform: rotate(359deg);
  }
}
@keyframes fa-spin-fast {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(359deg);
    transform: rotate(359deg);
  }
}
.material-card {
  position: relative;
  height: 0;
  padding-bottom: calc(100% - 16px);
  margin-bottom: 6.6em;
}
.material-card h2 {
  position: absolute;
  top: calc(100% - 16px);
  left: 0;
  width: 100%;
  padding: 10px 16px;
  color: #fff;
  font-size:12px;
  font-weight: bold;
  line-height: 1.6em;
  margin: 0;
  z-index: 10;
  -webkit-transition: all 0.3s;
  -moz-transition: all 0.3s;
  -ms-transition: all 0.3s;
  -o-transition: all 0.3s;
  transition: all 0.3s;
}
.material-card h2 span {
  display: block;
}
.material-card h2 strong {
  font-weight: 400;
  display: block;
  font-size: .8em;
}
.material-card h2:before,
.material-card h2:after {
  content: ' ';
  position: absolute;
  left: 0;
  top: -16px;
  width: 0;
  border: 8px solid;
  -webkit-transition: all 0.3s;
  -moz-transition: all 0.3s;
  -ms-transition: all 0.3s;
  -o-transition: all 0.3s;
  transition: all 0.3s;
}
.material-card h2:after {
  top: auto;
  bottom: 0;
}
@media screen and (max-width: 767px) {
  .material-card.mc-active {
    padding-bottom: 0;
    height: auto;
  }
}
.material-card.mc-active h2 {
  top: 0;
  padding: 10px 16px 10px 90px;
}
.material-card.mc-active h2:before {
  top: 0;
}
.material-card.mc-active h2:after {
  bottom: -16px;
}
.material-card .mc-content {
  position: absolute;
  right: 0;
  top: 0;
  bottom: 16px;
  left: 16px;
  -webkit-transition: all 0.3s;
  -moz-transition: all 0.3s;
  -ms-transition: all 0.3s;
  -o-transition: all 0.3s;
  transition: all 0.3s;
}
.material-card .mc-btn-action {
  position: absolute;
  right: 16px;
  top: 15px;
  -webkit-border-radius: 50%;
  -moz-border-radius: 50%;
  border-radius: 50%;
  border: 5px solid;
  width: 54px;
  height: 54px;
  line-height: 44px;
  text-align: center;
  color: #fff;
  cursor: pointer;
  z-index: 20;
  -webkit-transition: all 0.3s;
  -moz-transition: all 0.3s;
  -ms-transition: all 0.3s;
  -o-transition: all 0.3s;
  transition: all 0.3s;
}
.material-card.mc-active .mc-btn-action {
  top: 35px;
}
.material-card .mc-description {
  position: absolute;
  top: 100%;
  right: 30px;
  left: 30px;
  bottom: 54px;
  overflow: hidden;
  opacity: 0;
  filter: alpha(opacity=0);
  -webkit-transition: all 1.2s;
  -moz-transition: all 1.2s;
  -ms-transition: all 1.2s;
  -o-transition: all 1.2s;
  transition: all 1.2s;
}
.material-card .mc-footer {
  height: 0;
  overflow: hidden;
  -webkit-transition: all 0.3s;
  -moz-transition: all 0.3s;
  -ms-transition: all 0.3s;
  -o-transition: all 0.3s;
  transition: all 0.3s;
}
.material-card .mc-footer h4 {
  position: absolute;
  top: 200px;
  left: 30px;
  padding: 0;
  margin: 0;
  font-size: 16px;
  font-weight: 700;
  -webkit-transition: all 1.4s;
  -moz-transition: all 1.4s;
  -ms-transition: all 1.4s;
  -o-transition: all 1.4s;
  transition: all 1.4s;
}
.material-card .mc-footer a {
  display: block;
  float: left;
  position: relative;
  width: 52px;
  height: 52px;
  margin-left: 5px;
  margin-bottom: 15px;
  font-size: 28px;
  color: #fff;
  line-height: 52px;
  text-decoration: none;
  top: 200px;
}
.material-card .mc-footer a:nth-child(1) {
  -webkit-transition: all 0.5s;
  -moz-transition: all 0.5s;
  -ms-transition: all 0.5s;
  -o-transition: all 0.5s;
  transition: all 0.5s;
}
.material-card .mc-footer a:nth-child(2) {
  -webkit-transition: all 0.6s;
  -moz-transition: all 0.6s;
  -ms-transition: all 0.6s;
  -o-transition: all 0.6s;
  transition: all 0.6s;
}
.material-card .mc-footer a:nth-child(3) {
  -webkit-transition: all 0.7s;
  -moz-transition: all 0.7s;
  -ms-transition: all 0.7s;
  -o-transition: all 0.7s;
  transition: all 0.7s;
}
.material-card .mc-footer a:nth-child(4) {
  -webkit-transition: all 0.8s;
  -moz-transition: all 0.8s;
  -ms-transition: all 0.8s;
  -o-transition: all 0.8s;
  transition: all 0.8s;
}
.material-card .mc-footer a:nth-child(5) {
  -webkit-transition: all 0.9s;
  -moz-transition: all 0.9s;
  -ms-transition: all 0.9s;
  -o-transition: all 0.9s;
  transition: all 0.9s;
}
.material-card .img-container {
  overflow: hidden;
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  z-index: 3;
  -webkit-transition: all 0.3s;
  -moz-transition: all 0.3s;
  -ms-transition: all 0.3s;
  -o-transition: all 0.3s;
  transition: all 0.3s;
}
.material-card.mc-active .img-container {
  -webkit-border-radius: 50%;
  -moz-border-radius: 50%;
  border-radius: 50%;
  left: 0;
  top: 12px;
  width: 60px;
  height: 60px;
  z-index: 20;
}
.material-card.mc-active .mc-content {
  padding-top: 5.6em;
}
@media screen and (max-width: 767px) {
  .material-card.mc-active .mc-content {
    position: relative;
    margin-right: 16px;
  }
}
.material-card.mc-active .mc-description {
  top: 50px;
  padding-top: 5.6em;
  opacity: 1;
  filter: alpha(opacity=100);
}
@media screen and (max-width: 767px) {
  .material-card.mc-active .mc-description {
    position: relative;
    top: auto;
    right: auto;
    left: auto;
    padding: 50px 30px 70px 30px;
    bottom: 0;
  }
}
.material-card.mc-active .mc-footer {
  overflow: visible;
  position: absolute;
  top: calc(100% - 58px);
  left: 16px;
  right: 0;
  height: 82px;
  padding-top: 15px;
  padding-left: 25px;
}
.material-card.mc-active .mc-footer a {
  top: 0;
}
.material-card.mc-active .mc-footer h4 {
  top: -32px;
}
.material-card.Red h2 {
  background-color: #F44336;
}
.material-card.Red h2:after {
  border-top-color: #F44336;
  border-right-color: #F44336;
  border-bottom-color: transparent;
  border-left-color: transparent;
}
.material-card.Red h2:before {
  border-top-color: transparent;
  border-right-color: #B71C1C;
  border-bottom-color: #B71C1C;
  border-left-color: transparent;
}
.material-card.Red.mc-active h2:before {
  border-top-color: transparent;
  border-right-color: #F44336;
  border-bottom-color: #F44336;
  border-left-color: transparent;
}
.material-card.Red.mc-active h2:after {
  border-top-color: #B71C1C;
  border-right-color: #B71C1C;
  border-bottom-color: transparent;
  border-left-color: transparent;
}
.material-card.Red .mc-btn-action {
  background-color: #F44336;
}
.material-card.Red .mc-btn-action:hover {
  background-color: #B71C1C;
}
.material-card.Red .mc-footer h4 {
  color: #B71C1C;
}
.material-card.Red .mc-footer a {
  background-color: #B71C1C;
}
.material-card.Red.mc-active .mc-content {
  background-color: #FFEBEE;
}
.material-card.Red.mc-active .mc-footer {
  background-color: #FFCDD2;
}
.material-card.Red.mc-active .mc-btn-action {
  border-color: #FFEBEE;
}
.material-card.Blue-Grey h2 {
  background-color: #607D8B;
}
.material-card.Blue-Grey h2:after {
  border-top-color: #607D8B;
  border-right-color: #607D8B;
  border-bottom-color: transparent;
  border-left-color: transparent;
}
.material-card.Blue-Grey h2:before {
  border-top-color: transparent;
  border-right-color: #263238;
  border-bottom-color: #263238;
  border-left-color: transparent;
}
.material-card.Blue-Grey.mc-active h2:before {
  border-top-color: transparent;
  border-right-color: #607D8B;
  border-bottom-color: #607D8B;
  border-left-color: transparent;
}
.material-card.Blue-Grey.mc-active h2:after {
  border-top-color: #263238;
  border-right-color: #263238;
  border-bottom-color: transparent;
  border-left-color: transparent;
}
.material-card.Blue-Grey .mc-btn-action {
  background-color: #607D8B;
}
.material-card.Blue-Grey .mc-btn-action:hover {
  background-color: #263238;
}
.material-card.Blue-Grey .mc-footer h4 {
  color: #263238;
}
.material-card.Blue-Grey .mc-footer a {
  background-color: #263238;
}
.material-card.Blue-Grey.mc-active .mc-content {
  background-color: #ECEFF1;
}
.material-card.Blue-Grey.mc-active .mc-footer {
  background-color: #CFD8DC;
}
.material-card.Blue-Grey.mc-active .mc-btn-action {
  border-color: #ECEFF1;
}
.material-card.Pink h2 {
  background-color: #E91E63;
}
.material-card.Pink h2:after {
  border-top-color: #E91E63;
  border-right-color: #E91E63;
  border-bottom-color: transparent;
  border-left-color: transparent;
}
.material-card.Pink h2:before {
  border-top-color: transparent;
  border-right-color: #880E4F;
  border-bottom-color: #880E4F;
  border-left-color: transparent;
}
.material-card.Pink.mc-active h2:before {
  border-top-color: transparent;
  border-right-color: #E91E63;
  border-bottom-color: #E91E63;
  border-left-color: transparent;
}
.material-card.Pink.mc-active h2:after {
  border-top-color: #880E4F;
  border-right-color: #880E4F;
  border-bottom-color: transparent;
  border-left-color: transparent;
}
.material-card.Pink .mc-btn-action {
  background-color: #E91E63;
}
.material-card.Pink .mc-btn-action:hover {
  background-color: #880E4F;
}
.material-card.Pink .mc-footer h4 {
  color: #880E4F;
}
.material-card.Pink .mc-footer a {
  background-color: #880E4F;
}
.material-card.Pink.mc-active .mc-content {
  background-color: #FCE4EC;
}
.material-card.Pink.mc-active .mc-footer {
  background-color: #F8BBD0;
}
.material-card.Pink.mc-active .mc-btn-action {
  border-color: #FCE4EC;
}
.material-card.Purple h2 {
  background-color: #9C27B0;
}
.material-card.Purple h2:after {
  border-top-color: #9C27B0;
  border-right-color: #9C27B0;
  border-bottom-color: transparent;
  border-left-color: transparent;
}
.material-card.Purple h2:before {
  border-top-color: transparent;
  border-right-color: #4A148C;
  border-bottom-color: #4A148C;
  border-left-color: transparent;
}
.material-card.Purple.mc-active h2:before {
  border-top-color: transparent;
  border-right-color: #9C27B0;
  border-bottom-color: #9C27B0;
  border-left-color: transparent;
}
.material-card.Purple.mc-active h2:after {
  border-top-color: #4A148C;
  border-right-color: #4A148C;
  border-bottom-color: transparent;
  border-left-color: transparent;
}
.material-card.Purple .mc-btn-action {
  background-color: #9C27B0;
}
.material-card.Purple .mc-btn-action:hover {
  background-color: #4A148C;
}
.material-card.Purple .mc-footer h4 {
  color: #4A148C;
}
.material-card.Purple .mc-footer a {
  background-color: #4A148C;
}
.material-card.Purple.mc-active .mc-content {
  background-color: #F3E5F5;
}
.material-card.Purple.mc-active .mc-footer {
  background-color: #E1BEE7;
}
.material-card.Purple.mc-active .mc-btn-action {
  border-color: #F3E5F5;
}
.material-card.Deep-Purple h2 {
  background-color: #673AB7;
}
.material-card.Deep-Purple h2:after {
  border-top-color: #673AB7;
  border-right-color: #673AB7;
  border-bottom-color: transparent;
  border-left-color: transparent;
}
.material-card.Deep-Purple h2:before {
  border-top-color: transparent;
  border-right-color: #311B92;
  border-bottom-color: #311B92;
  border-left-color: transparent;
}
.material-card.Deep-Purple.mc-active h2:before {
  border-top-color: transparent;
  border-right-color: #673AB7;
  border-bottom-color: #673AB7;
  border-left-color: transparent;
}
.material-card.Deep-Purple.mc-active h2:after {
  border-top-color: #311B92;
  border-right-color: #311B92;
  border-bottom-color: transparent;
  border-left-color: transparent;
}
.material-card.Deep-Purple .mc-btn-action {
  background-color: #673AB7;
}
.material-card.Deep-Purple .mc-btn-action:hover {
  background-color: #311B92;
}
.material-card.Deep-Purple .mc-footer h4 {
  color: #311B92;
}
.material-card.Deep-Purple .mc-footer a {
  background-color: #311B92;
}
.material-card.Deep-Purple.mc-active .mc-content {
  background-color: #EDE7F6;
}
.material-card.Deep-Purple.mc-active .mc-footer {
  background-color: #D1C4E9;
}
.material-card.Deep-Purple.mc-active .mc-btn-action {
  border-color: #EDE7F6;
}
.material-card.Indigo h2 {
  background-color: #3F51B5;
}
.material-card.Indigo h2:after {
  border-top-color: #3F51B5;
  border-right-color: #3F51B5;
  border-bottom-color: transparent;
  border-left-color: transparent;
}
.material-card.Indigo h2:before {
  border-top-color: transparent;
  border-right-color: #1A237E;
  border-bottom-color: #1A237E;
  border-left-color: transparent;
}
.material-card.Indigo.mc-active h2:before {
  border-top-color: transparent;
  border-right-color: #3F51B5;
  border-bottom-color: #3F51B5;
  border-left-color: transparent;
}
.material-card.Indigo.mc-active h2:after {
  border-top-color: #1A237E;
  border-right-color: #1A237E;
  border-bottom-color: transparent;
  border-left-color: transparent;
}
.material-card.Indigo .mc-btn-action {
  background-color: #3F51B5;
}
.material-card.Indigo .mc-btn-action:hover {
  background-color: #1A237E;
}
.material-card.Indigo .mc-footer h4 {
  color: #1A237E;
}
.material-card.Indigo .mc-footer a {
  background-color: #1A237E;
}
.material-card.Indigo.mc-active .mc-content {
  background-color: #E8EAF6;
}
.material-card.Indigo.mc-active .mc-footer {
  background-color: #C5CAE9;
}
.material-card.Indigo.mc-active .mc-btn-action {
  border-color: #E8EAF6;
}
.material-card.Blue h2 {
  background-color: #2196F3;
}
.material-card.Blue h2:after {
  border-top-color: #2196F3;
  border-right-color: #2196F3;
  border-bottom-color: transparent;
  border-left-color: transparent;
}
.material-card.Blue h2:before {
  border-top-color: transparent;
  border-right-color: #0D47A1;
  border-bottom-color: #0D47A1;
  border-left-color: transparent;
}
.material-card.Blue.mc-active h2:before {
  border-top-color: transparent;
  border-right-color: #2196F3;
  border-bottom-color: #2196F3;
  border-left-color: transparent;
}
.material-card.Blue.mc-active h2:after {
  border-top-color: #0D47A1;
  border-right-color: #0D47A1;
  border-bottom-color: transparent;
  border-left-color: transparent;
}
.material-card.Blue .mc-btn-action {
  background-color: #2196F3;
}
.material-card.Blue .mc-btn-action:hover {
  background-color: #0D47A1;
}
.material-card.Blue .mc-footer h4 {
  color: #0D47A1;
}
.material-card.Blue .mc-footer a {
  background-color: #0D47A1;
}
.material-card.Blue.mc-active .mc-content {
  background-color: #E3F2FD;
}
.material-card.Blue.mc-active .mc-footer {
  background-color: #BBDEFB;
}
.material-card.Blue.mc-active .mc-btn-action {
  border-color: #E3F2FD;
}
.material-card.Light-Blue h2 {
  background-color: #03A9F4;
}
.material-card.Light-Blue h2:after {
  border-top-color: #03A9F4;
  border-right-color: #03A9F4;
  border-bottom-color: transparent;
  border-left-color: transparent;
}
.material-card.Light-Blue h2:before {
  border-top-color: transparent;
  border-right-color: #01579B;
  border-bottom-color: #01579B;
  border-left-color: transparent;
}
.material-card.Light-Blue.mc-active h2:before {
  border-top-color: transparent;
  border-right-color: #03A9F4;
  border-bottom-color: #03A9F4;
  border-left-color: transparent;
}
.material-card.Light-Blue.mc-active h2:after {
  border-top-color: #01579B;
  border-right-color: #01579B;
  border-bottom-color: transparent;
  border-left-color: transparent;
}
.material-card.Light-Blue .mc-btn-action {
  background-color: #03A9F4;
}
.material-card.Light-Blue .mc-btn-action:hover {
  background-color: #01579B;
}
.material-card.Light-Blue .mc-footer h4 {
  color: #01579B;
}
.material-card.Light-Blue .mc-footer a {
  background-color: #01579B;
}
.material-card.Light-Blue.mc-active .mc-content {
  background-color: #E1F5FE;
}
.material-card.Light-Blue.mc-active .mc-footer {
  background-color: #B3E5FC;
}
.material-card.Light-Blue.mc-active .mc-btn-action {
  border-color: #E1F5FE;
}
.material-card.Cyan h2 {
  background-color: #00BCD4;
}
.material-card.Cyan h2:after {
  border-top-color: #00BCD4;
  border-right-color: #00BCD4;
  border-bottom-color: transparent;
  border-left-color: transparent;
}
.material-card.Cyan h2:before {
  border-top-color: transparent;
  border-right-color: #006064;
  border-bottom-color: #006064;
  border-left-color: transparent;
}
.material-card.Cyan.mc-active h2:before {
  border-top-color: transparent;
  border-right-color: #00BCD4;
  border-bottom-color: #00BCD4;
  border-left-color: transparent;
}
.material-card.Cyan.mc-active h2:after {
  border-top-color: #006064;
  border-right-color: #006064;
  border-bottom-color: transparent;
  border-left-color: transparent;
}
.material-card.Cyan .mc-btn-action {
  background-color: #00BCD4;
}
.material-card.Cyan .mc-btn-action:hover {
  background-color: #006064;
}
.material-card.Cyan .mc-footer h4 {
  color: #006064;
}
.material-card.Cyan .mc-footer a {
  background-color: #006064;
}
.material-card.Cyan.mc-active .mc-content {
  background-color: #E0F7FA;
}
.material-card.Cyan.mc-active .mc-footer {
  background-color: #B2EBF2;
}
.material-card.Cyan.mc-active .mc-btn-action {
  border-color: #E0F7FA;
}
.material-card.Teal h2 {
  background-color: #009688;
}
.material-card.Teal h2:after {
  border-top-color: #009688;
  border-right-color: #009688;
  border-bottom-color: transparent;
  border-left-color: transparent;
}
.material-card.Teal h2:before {
  border-top-color: transparent;
  border-right-color: #004D40;
  border-bottom-color: #004D40;
  border-left-color: transparent;
}
.material-card.Teal.mc-active h2:before {
  border-top-color: transparent;
  border-right-color: #009688;
  border-bottom-color: #009688;
  border-left-color: transparent;
}
.material-card.Teal.mc-active h2:after {
  border-top-color: #004D40;
  border-right-color: #004D40;
  border-bottom-color: transparent;
  border-left-color: transparent;
}
.material-card.Teal .mc-btn-action {
  background-color: #009688;
}
.material-card.Teal .mc-btn-action:hover {
  background-color: #004D40;
}
.material-card.Teal .mc-footer h4 {
  color: #004D40;
}
.material-card.Teal .mc-footer a {
  background-color: #004D40;
}
.material-card.Teal.mc-active .mc-content {
  background-color: #E0F2F1;
}
.material-card.Teal.mc-active .mc-footer {
  background-color: #B2DFDB;
}
.material-card.Teal.mc-active .mc-btn-action {
  border-color: #E0F2F1;
}
.material-card.Green h2 {
  background-color: #4CAF50;
}
.material-card.Green h2:after {
  border-top-color: #4CAF50;
  border-right-color: #4CAF50;
  border-bottom-color: transparent;
  border-left-color: transparent;
}
.material-card.Green h2:before {
  border-top-color: transparent;
  border-right-color: #1B5E20;
  border-bottom-color: #1B5E20;
  border-left-color: transparent;
}
.material-card.Green.mc-active h2:before {
  border-top-color: transparent;
  border-right-color: #4CAF50;
  border-bottom-color: #4CAF50;
  border-left-color: transparent;
}
.material-card.Green.mc-active h2:after {
  border-top-color: #1B5E20;
  border-right-color: #1B5E20;
  border-bottom-color: transparent;
  border-left-color: transparent;
}
.material-card.Green .mc-btn-action {
  background-color: #4CAF50;
}
.material-card.Green .mc-btn-action:hover {
  background-color: #1B5E20;
}
.material-card.Green .mc-footer h4 {
  color: #1B5E20;
}
.material-card.Green .mc-footer a {
  background-color: #1B5E20;
}
.material-card.Green.mc-active .mc-content {
  background-color: #E8F5E9;
}
.material-card.Green.mc-active .mc-footer {
  background-color: #C8E6C9;
}
.material-card.Green.mc-active .mc-btn-action {
  border-color: #E8F5E9;
}
.material-card.Light-Green h2 {
  background-color: #8BC34A;
}
.material-card.Light-Green h2:after {
  border-top-color: #8BC34A;
  border-right-color: #8BC34A;
  border-bottom-color: transparent;
  border-left-color: transparent;
}
.material-card.Light-Green h2:before {
  border-top-color: transparent;
  border-right-color: #33691E;
  border-bottom-color: #33691E;
  border-left-color: transparent;
}
.material-card.Light-Green.mc-active h2:before {
  border-top-color: transparent;
  border-right-color: #8BC34A;
  border-bottom-color: #8BC34A;
  border-left-color: transparent;
}
.material-card.Light-Green.mc-active h2:after {
  border-top-color: #33691E;
  border-right-color: #33691E;
  border-bottom-color: transparent;
  border-left-color: transparent;
}
.material-card.Light-Green .mc-btn-action {
  background-color: #8BC34A;
}
.material-card.Light-Green .mc-btn-action:hover {
  background-color: #33691E;
}
.material-card.Light-Green .mc-footer h4 {
  color: #33691E;
}
.material-card.Light-Green .mc-footer a {
  background-color: #33691E;
}
.material-card.Light-Green.mc-active .mc-content {
  background-color: #F1F8E9;
}
.material-card.Light-Green.mc-active .mc-footer {
  background-color: #DCEDC8;
}
.material-card.Light-Green.mc-active .mc-btn-action {
  border-color: #F1F8E9;
}
.material-card.Lime h2 {
  background-color: #CDDC39;
}
.material-card.Lime h2:after {
  border-top-color: #CDDC39;
  border-right-color: #CDDC39;
  border-bottom-color: transparent;
  border-left-color: transparent;
}
.material-card.Lime h2:before {
  border-top-color: transparent;
  border-right-color: #827717;
  border-bottom-color: #827717;
  border-left-color: transparent;
}
.material-card.Lime.mc-active h2:before {
  border-top-color: transparent;
  border-right-color: #CDDC39;
  border-bottom-color: #CDDC39;
  border-left-color: transparent;
}
.material-card.Lime.mc-active h2:after {
  border-top-color: #827717;
  border-right-color: #827717;
  border-bottom-color: transparent;
  border-left-color: transparent;
}
.material-card.Lime .mc-btn-action {
  background-color: #CDDC39;
}
.material-card.Lime .mc-btn-action:hover {
  background-color: #827717;
}
.material-card.Lime .mc-footer h4 {
  color: #827717;
}
.material-card.Lime .mc-footer a {
  background-color: #827717;
}
.material-card.Lime.mc-active .mc-content {
  background-color: #F9FBE7;
}
.material-card.Lime.mc-active .mc-footer {
  background-color: #F0F4C3;
}
.material-card.Lime.mc-active .mc-btn-action {
  border-color: #F9FBE7;
}
.material-card.Yellow h2 {
  background-color: #FFEB3B;
}
.material-card.Yellow h2:after {
  border-top-color: #FFEB3B;
  border-right-color: #FFEB3B;
  border-bottom-color: transparent;
  border-left-color: transparent;
}
.material-card.Yellow h2:before {
  border-top-color: transparent;
  border-right-color: #F57F17;
  border-bottom-color: #F57F17;
  border-left-color: transparent;
}
.material-card.Yellow.mc-active h2:before {
  border-top-color: transparent;
  border-right-color: #FFEB3B;
  border-bottom-color: #FFEB3B;
  border-left-color: transparent;
}
.material-card.Yellow.mc-active h2:after {
  border-top-color: #F57F17;
  border-right-color: #F57F17;
  border-bottom-color: transparent;
  border-left-color: transparent;
}
.material-card.Yellow .mc-btn-action {
  background-color: #FFEB3B;
}
.material-card.Yellow .mc-btn-action:hover {
  background-color: #F57F17;
}
.material-card.Yellow .mc-footer h4 {
  color: #F57F17;
}
.material-card.Yellow .mc-footer a {
  background-color: #F57F17;
}
.material-card.Yellow.mc-active .mc-content {
  background-color: #FFFDE7;
}
.material-card.Yellow.mc-active .mc-footer {
  background-color: #FFF9C4;
}
.material-card.Yellow.mc-active .mc-btn-action {
  border-color: #FFFDE7;
}
.material-card.Amber h2 {
  background-color: #FFC107;
}
.material-card.Amber h2:after {
  border-top-color: #FFC107;
  border-right-color: #FFC107;
  border-bottom-color: transparent;
  border-left-color: transparent;
}
.material-card.Amber h2:before {
  border-top-color: transparent;
  border-right-color: #FF6F00;
  border-bottom-color: #FF6F00;
  border-left-color: transparent;
}
.material-card.Amber.mc-active h2:before {
  border-top-color: transparent;
  border-right-color: #FFC107;
  border-bottom-color: #FFC107;
  border-left-color: transparent;
}
.material-card.Amber.mc-active h2:after {
  border-top-color: #FF6F00;
  border-right-color: #FF6F00;
  border-bottom-color: transparent;
  border-left-color: transparent;
}
.material-card.Amber .mc-btn-action {
  background-color: #FFC107;
}
.material-card.Amber .mc-btn-action:hover {
  background-color: #FF6F00;
}
.material-card.Amber .mc-footer h4 {
  color: #FF6F00;
}
.material-card.Amber .mc-footer a {
  background-color: #FF6F00;
}
.material-card.Amber.mc-active .mc-content {
  background-color: #FFF8E1;
}
.material-card.Amber.mc-active .mc-footer {
  background-color: #FFECB3;
}
.material-card.Amber.mc-active .mc-btn-action {
  border-color: #FFF8E1;
}
.material-card.Orange h2 {
  background-color: #FF9800;
}
.material-card.Orange h2:after {
  border-top-color: #FF9800;
  border-right-color: #FF9800;
  border-bottom-color: transparent;
  border-left-color: transparent;
}
.material-card.Orange h2:before {
  border-top-color: transparent;
  border-right-color: #E65100;
  border-bottom-color: #E65100;
  border-left-color: transparent;
}
.material-card.Orange.mc-active h2:before {
  border-top-color: transparent;
  border-right-color: #FF9800;
  border-bottom-color: #FF9800;
  border-left-color: transparent;
}
.material-card.Orange.mc-active h2:after {
  border-top-color: #E65100;
  border-right-color: #E65100;
  border-bottom-color: transparent;
  border-left-color: transparent;
}
.material-card.Orange .mc-btn-action {
  background-color: #FF9800;
}
.material-card.Orange .mc-btn-action:hover {
  background-color: #E65100;
}
.material-card.Orange .mc-footer h4 {
  color: #E65100;
}
.material-card.Orange .mc-footer a {
  background-color: #E65100;
}
.material-card.Orange.mc-active .mc-content {
  background-color: #FFF3E0;
}
.material-card.Orange.mc-active .mc-footer {
  background-color: #FFE0B2;
}
.material-card.Orange.mc-active .mc-btn-action {
  border-color: #FFF3E0;
}
.material-card.Deep-Orange h2 {
  background-color: #FF5722;
}
.material-card.Deep-Orange h2:after {
  border-top-color: #FF5722;
  border-right-color: #FF5722;
  border-bottom-color: transparent;
  border-left-color: transparent;
}
.material-card.Deep-Orange h2:before {
  border-top-color: transparent;
  border-right-color: #BF360C;
  border-bottom-color: #BF360C;
  border-left-color: transparent;
}
.material-card.Deep-Orange.mc-active h2:before {
  border-top-color: transparent;
  border-right-color: #FF5722;
  border-bottom-color: #FF5722;
  border-left-color: transparent;
}
.material-card.Deep-Orange.mc-active h2:after {
  border-top-color: #BF360C;
  border-right-color: #BF360C;
  border-bottom-color: transparent;
  border-left-color: transparent;
}
.material-card.Deep-Orange .mc-btn-action {
  background-color: #FF5722;
}
.material-card.Deep-Orange .mc-btn-action:hover {
  background-color: #BF360C;
}
.material-card.Deep-Orange .mc-footer h4 {
  color: #BF360C;
}
.material-card.Deep-Orange .mc-footer a {
  background-color: #BF360C;
}
.material-card.Deep-Orange.mc-active .mc-content {
  background-color: #FBE9E7;
}
.material-card.Deep-Orange.mc-active .mc-footer {
  background-color: #FFCCBC;
}
.material-card.Deep-Orange.mc-active .mc-btn-action {
  border-color: #FBE9E7;
}
.material-card.Brown h2 {
  background-color: #795548;
}
.material-card.Brown h2:after {
  border-top-color: #795548;
  border-right-color: #795548;
  border-bottom-color: transparent;
  border-left-color: transparent;
}
.material-card.Brown h2:before {
  border-top-color: transparent;
  border-right-color: #3E2723;
  border-bottom-color: #3E2723;
  border-left-color: transparent;
}
.material-card.Brown.mc-active h2:before {
  border-top-color: transparent;
  border-right-color: #795548;
  border-bottom-color: #795548;
  border-left-color: transparent;
}
.material-card.Brown.mc-active h2:after {
  border-top-color: #3E2723;
  border-right-color: #3E2723;
  border-bottom-color: transparent;
  border-left-color: transparent;
}
.material-card.Brown .mc-btn-action {
  background-color: #795548;
}
.material-card.Brown .mc-btn-action:hover {
  background-color: #3E2723;
}
.material-card.Brown .mc-footer h4 {
  color: #3E2723;
}
.material-card.Brown .mc-footer a {
  background-color: #3E2723;
}
.material-card.Brown.mc-active .mc-content {
  background-color: #EFEBE9;
}
.material-card.Brown.mc-active .mc-footer {
  background-color: #D7CCC8;
}
.material-card.Brown.mc-active .mc-btn-action {
  border-color: #EFEBE9;
}
.material-card.Grey h2 {
  background-color: #9E9E9E;
}
.material-card.Grey h2:after {
  border-top-color: #9E9E9E;
  border-right-color: #9E9E9E;
  border-bottom-color: transparent;
  border-left-color: transparent;
}
.material-card.Grey h2:before {
  border-top-color: transparent;
  border-right-color: #212121;
  border-bottom-color: #212121;
  border-left-color: transparent;
}
.material-card.Grey.mc-active h2:before {
  border-top-color: transparent;
  border-right-color: #9E9E9E;
  border-bottom-color: #9E9E9E;
  border-left-color: transparent;
}
.material-card.Grey.mc-active h2:after {
  border-top-color: #212121;
  border-right-color: #212121;
  border-bottom-color: transparent;
  border-left-color: transparent;
}
.material-card.Grey .mc-btn-action {
  background-color: #9E9E9E;
}
.material-card.Grey .mc-btn-action:hover {
  background-color: #212121;
}
.material-card.Grey .mc-footer h4 {
  color: #212121;
}
.material-card.Grey .mc-footer a {
  background-color: #212121;
}
.material-card.Grey.mc-active .mc-content {
  background-color: #FAFAFA;
}
.material-card.Grey.mc-active .mc-footer {
  background-color: #F5F5F5;
}
.material-card.Grey.mc-active .mc-btn-action {
  border-color: #FAFAFA;
}
.material-card.Blue-Grey h2 {
  background-color: #607D8B;
}
.material-card.Blue-Grey h2:after {
  border-top-color: #607D8B;
  border-right-color: #607D8B;
  border-bottom-color: transparent;
  border-left-color: transparent;
}
.material-card.Blue-Grey h2:before {
  border-top-color: transparent;
  border-right-color: #263238;
  border-bottom-color: #263238;
  border-left-color: transparent;
}
.material-card.Blue-Grey.mc-active h2:before {
  border-top-color: transparent;
  border-right-color: #607D8B;
  border-bottom-color: #607D8B;
  border-left-color: transparent;
}
.material-card.Blue-Grey.mc-active h2:after {
  border-top-color: #263238;
  border-right-color: #263238;
  border-bottom-color: transparent;
  border-left-color: transparent;
}
.material-card.Blue-Grey .mc-btn-action {
  background-color: #607D8B;
}
.material-card.Blue-Grey .mc-btn-action:hover {
  background-color: #263238;
}
.material-card.Blue-Grey .mc-footer h4 {
  color: #263238;
}
.material-card.Blue-Grey .mc-footer a {
  background-color: #263238;
}
.material-card.Blue-Grey.mc-active .mc-content {
  background-color: #ECEFF1;
}
.material-card.Blue-Grey.mc-active .mc-footer {
  background-color: #CFD8DC;
}
.material-card.Blue-Grey.mc-active .mc-btn-action {
  border-color: #ECEFF1;
}

.nav-pills>li.active>a, .nav-pills>li.active>a:hover, .nav-pills>li.active>a:focus{
    color: #000 !important;
    background-color: #fff !important;
    margin-left: -4px !important;
    width: 150px;
    text-align: center;
}

.nav-pills>li.active>a{
    color: #000 !important;
    background-color: #fff !important;
    margin-left: -4px !important;
    width: 150px;
    text-align: center;
}

.nav>li>a{
    color: #fff !important;
    background: #0000006b !important;
    border: 1px solid #fff !important;
    margin-left: -4px !important;
    width: 150px;
    text-align: center;
}


/* End carousel */
</style>
<body>
  <input type="text" id="previous_div" value=""/>
  <input type="hidden" id="question1" value="yes"/>
  <input type="hidden" id="question2" value="yes"/>
  
  <form id="frontend_quiz" method="POST" action="<?php echo base_url('home/explore'); ?>">
    <input type="hidden" name="45" id="input_1" value=""/>
    <input type="hidden" name="12" id="input_2" value=""/>
    <input type="hidden" name="44" id="input_3" value=""/>
    <input type="hidden" name="38" id="input_4" value=""/>
    <input type="hidden" name="48" id="input_13" value=""/>
    <input type="hidden" name="49" id="input_14" value=""/>
    
    <input type="hidden" name="50" id="input_15" value=""/>
    <input type="hidden" name="40" id="input_5" value=""/>
    <input type="hidden" name="39" id="input_6" value=""/>
    <input type="hidden" name="10" id="input_7" value=""/>
    <input type="hidden" name="1" id="input_8" value=""/>
    <input type="hidden" name="5" id="input_9_1" value=""/>
    <input type="hidden" name="6" id="input_9_2" value=""/>
    <input type="hidden" name="7" id="input_9_3"  value=""/>
    <input type="hidden" name="41" id="input_10" value=""/>
    <input type="hidden" name="43" id="input_11" value=""/>
    <input type="hidden" name="4" id="input_12_1" value=""/>
    <input type="hidden" name="password" id="input_12_2" value=""/>
    <input type="hidden" name="rep_password" id="input_12_3" value=""/>
    <input type="hidden" name="8" id="input_12_4" value=""/>
  </form>
  
  <div class="BackgroundImg">
    <header id="header" class="page-topbar">
        <!--<div class="navbar-fixed" >
            <nav class="navbar-color" style="position: relative">
                <div class="nav-wrapper">-->
          
          <div class="container">

                    <div class="search-wrapper">
                        <!-- Modal Search Form -->
                        <i class="search-close material-icons">&#xE5CD;</i>
                        <div class="search-form-wrapper">
                            <form action="#" class="white-form">
                                <div class="input-field">
                                    <input type="text" name="search" id="search">
                                    <label for="search" class="">Search Here...</label>
                                </div>
                                <button class="btn pink search-button waves-effect waves-light" type="submit"><i class="material-icons">&#xE8B6;</i></button>
                                
                            </form>
                        </div>
                    </div><!-- /.search-wrapper -->
                
                    <div  class="menuzord">

                        <!--logo start-->

                        <!-- <a href="index.html" class="">
                  <a href="<?php echo base_url('home/index'); ?>" class="LogoMargin">
                    <span class="red-text">Hello</span>
                    <span class="black-text">Admission</span>
                  </a>
                        </a> -->
                        <!--logo end-->

                        <!--mega menu start-->

                    <!--     <ul class="menuzord-menu pull-right">

                            
                            <li>
                <a onclick="GLogin();" class="black-text">Google</a>&nbsp;&nbsp;
                            </li>
                            
                            <li>
                <a onclick="FBLogin();" class="black-text">Facebook</a>&nbsp;&nbsp;   
                            </li>
                            
                            <li class="active">
                  <a href="<?php echo base_url('explore/index'); ?>" class="black-text">Explore</a>&nbsp;&nbsp;
                            </li>
                            
                            
                            <li class="">
                  <a href="<?php echo base_url('login/index'); ?>" class="black-text">Login</a>&nbsp;&nbsp;
                            </li>
                            
                            <li class="">
                  <a href="<?php echo base_url('login/student_register'); ?>" class="black-text">SignUp</a>&nbsp;&nbsp;
                            </li>

                        </ul>
 -->
                     

                    </div>
    
    
    </div>
           <!-- </nav>
        </div>-->
    </header>
    

    <div id="div1" class="animated bounceInRight">

      <div class="row">
          <div class="col m5">&nbsp;</a></div>
        <div class="col m7" style="margin-top: -132px;margin-left: 40%;">
            <ul class="nav nav-pills" role="tablist">
                        <li role="presentation" class="active"><a href="#tab-25" class="waves-light swapbutton" role="tab" data-toggle="tab" aria-expanded="true" id="explore"  style="border-bottom-right-radius: 0px;border-top-right-radius: 0px"><b> <i class="fa fa-anchor"></i>  Explore </b></a></li>
                        <li role="presentation" class=""><a href="#tab-26" class="waves-light swapbutton" role="tab" data-toggle="tab" aria-expanded="false"  id="search"  style="border-bottom-left-radius: 0px;border-top-left-radius: 0px"> <b><i class="fa fa-search"></i> Search</b></a></li>
              </ul>
        </div>
        <!-- <div class="col m3"></div> -->
        </div>
          <div class="row">
             <div class="col m3">&nbsp;&nbsp;</a></div>
        <div class="col s12 m6" style="margin-top:-97px">
          <div class="card-panel explore" id="div1-strip" style="background-image:url('<?php echo base_url()?>assets/explore/images/frontend/blue-strip.jpg');">
            <div id="div1-strip-text" class="center">How are You?</div>
          </div>
          <div class="card-panel explore" id="div1-content" style="padding: 40px;">
            <div id="div1-content-action">
              <div class="row">
                <div class="col s6 l6">
                  <div class="row center" id="div_good">
                    <a class="btn-floating div1-btn" id="div1-good">
                      <img src="<?php echo base_url()?>assets/explore/images/frontend/howareyouicon1.png">
                    </a>
                    <h5 class="black-text">Good</h5>                      
                  </div>                      
                </div>
                <div class="col s6 l6">
                  <div class="row center" id="div_not_good">
                    <a class="btn-floating div1-btn" id="div1-bad">
                      <img src="<?php echo base_url()?>assets/explore/images/frontend/howareyouicon2.png">
                    </a>
                    <h5 class="black-text">Naah</h5>    
                  </div>  
                </div>                
              </div>
            </div>
            <div id="div1-content-msg">
              <div class="center" id="div1-msg">
                
              </div>
            </div>
          </div>
          <div class="card-panel search" id="search-head-div" style="background-image:url('<?php echo base_url()?>assets/explore/images/frontend/blue-strip.jpg');display:none;padding:34px !important">
            <div id="div1-strip-text" class="center">Search</div>
          </div>
          <div class="card-panel search" id="search-div" style="display:none;">
            <div id="div1-content-action">
              <div class="row">
                <div class="col s6 l12">
                  <div class="widget widget_search">
                  <form role="search" method="get" class="search-form">
                    <input type="text" class="form-control" value="" name="s" id="search_text" placeholder="Enter Course Name">
                    <button type="submit"><i class="fa fa-search"></i></button>
                  </form>
                  <div class="widget widget_search" >
                  <ul class="dropdown" style="right: auto; " id="search_result">
                      <li>
                        
                      </li>
                       
                    </ul>
                  </div>
                  
                  </div>                
                </div>              
              </div>
            </div>
          </div>
        </div>
        <div class="col m3">
          <a class="btn-floating btn-large waves-effect waves-light blue darken-3" id="div_back_1_p" style="margin-left:100px;background-color: #3374ac"><i class="mdi-hardware-keyboard-arrow-right"></i></a>
        </div>
      </div>
      <input type="hidden" id="div1-trigger"/>
    </div>
    
    <div id="div2" class="">
      <div class="row">
        <div class="col m2">
          <a class="btn-floating btn-large waves-effect waves-light green darken-3" id="div_back_2" style=""><i class="mdi-hardware-keyboard-arrow-left"></i></a>
        </div>
        <div class="col s12 m8" style="margin-top:-130px">
          <div class="card-panel" id="div2-strip" style="background-image:url('<?php echo base_url()?>assets/explore/images/frontend/wheredoyoulivebg.jpg');" data-img-width="1600" data-img-height="78">
            <div id="div2-strip-text" class="center">Where Do You Live?</div>
          </div>
          <div class="card-panel" id="div2-content">
            <div id="div2-content-action">
              <div class="row center">                  
                <div class="col l1 div2-btn" id="div2-Delhi">
                  <img src="<?php echo base_url()?>assets/explore/images/frontend/wheredoyoulive-1.png" width="80" height="80">
                  <h6 class="black-text">Delhi</h6> 
                </div>
                <div class="col l1 div2-btn" id="div2-Mumbai">
                  <img src="<?php echo base_url()?>assets/explore/images/frontend/wheredoyoulive-2.png" width="80" height="80">
                  <h6 class="black-text">Mumbai</h6>
                </div>
                <div class="col l1 div2-btn" id="div2-Pune">
                  <img src="<?php echo base_url()?>assets/explore/images/frontend/wheredoyoulive-3.png" width="80" height="80">
                  <h6 class="black-text">Pune</h6>
                </div>
                <div class="col l1 div2-btn" id="div2-Kolkata">
                  <img src="<?php echo base_url()?>assets/explore/images/frontend/wheredoyoulive-4.png" width="80" height="80">
                  <h6 class="black-text">Kolkata</h6>
                </div>
                <div class="col l1 div2-btn" id="div2-Hyderabad">
                  <img src="<?php echo base_url()?>assets/explore/images/frontend/wheredoyoulive-5.png" width="80" height="80">
                  <h6 class="black-text">Hyderabad</h6>
                </div>
                <div class="col l1 div2-btn" id="div2-Ahmedabad">
                  <img src="<?php echo base_url()?>assets/explore/images/frontend/wheredoyoulive-6.png" width="80" height="80">
                  <h6 class="black-text">Ahmedabad</h6>
                </div>
                <div class="col l1 div2-btn" id="div2-Chandigarh">
                  <img src="<?php echo base_url()?>assets/explore/images/frontend/wheredoyoulive-7.png" width="80" height="80">
                  <h6 class="black-text">Chandigarh</h6>
                </div>
                <div class="col l1 div2-btn" id="div2-Bhubanehswar">
                  <img src="<?php echo base_url()?>assets/explore/images/frontend/wheredoyoulive-8.png" width="80" height="80">
                  <h6 class="black-text">Bhubanehswar</h6>
                </div>
                                
                <div class="col s6 l1 div2-btn" id="div2-Jaipur">
                  <img src="<?php echo base_url()?>assets/explore/images/frontend/wheredoyoulive-9.png" width="80" height="80">
                  <h6 class="black-text">Jaipur</h6>
                </div>
                <div class="col s6 l1 div2-btn" id="div2-Gurgaon">
                  <img src="<?php echo base_url()?>assets/explore/images/frontend/wheredoyoulive-10.png" width="80" height="80">
                  <h6 class="black-text">Gurgaon</h6>
                </div>
                <div class="col s6 l1 div2-btn" id="div2-Kochi">
                  <img src="<?php echo base_url()?>assets/explore/images/frontend/wheredoyoulive-11.png" width="80" height="80">
                  <h6 class="black-text">Kochi</h6>
                </div>
                <div class="col s6 l1 div2-btn" id="div2-Luckhnow">
                  <img src="<?php echo base_url()?>assets/explore/images/frontend/wheredoyoulive-12.png" width="80" height="80">
                  <h6 class="black-text">Luckhnow</h6>
                </div>
                <div class="col s6 l1 div2-btn" id="div2-Surat">
                  <img src="<?php echo base_url()?>assets/explore/images/frontend/wheredoyoulive-13.png" width="80" height="80">
                  <h6 class="black-text">Surat</h6>
                </div>
                <div class="col s6 l1 div2-btn" id="div2-Banglore">
                  <img src="<?php echo base_url()?>assets/explore/images/frontend/wheredoyoulive-14.png" width="80" height="80">
                  <h6 class="black-text">Banglore</h6>
                </div>
                <div class="col s6 l1 div2-btn" id="div2-Chennai">
                  <img src="<?php echo base_url()?>assets/explore/images/frontend/wheredoyoulive-15.png" width="80" height="80">
                  <h6 class="black-text">Chennai</h6>
                </div>
                <div class="col s6 l1 div2-btn" id="div2-Other">
                  <img src="<?php echo base_url()?>assets/explore/images/frontend/wheredoyoulive-16.png" width="80" height="80">
                  <h6 class="black-text">Other City</h6>
                </div>
              </div>
            </div>
            <div id="div2-content-msg">
              <div class="center" id="div2-msg">
                
              </div>
            </div>
          </div>
        </div>
        <div class="col m2">
          <a class="btn-floating btn-large waves-effect waves-light green darken-3" id="div_back_2_p" style="margin-left:100px"><i class="mdi-hardware-keyboard-arrow-right"></i></a>
        </div>
        
      </div>
      <input type="hidden" id="div2-trigger"/>
    </div>
    
    <div id="div3" class="">
      <div class="row">
        <div class="col m3">
          <a class="btn-floating btn-large waves-effect waves-light blue darken-3" id="div_back_3"><i class="mdi-hardware-keyboard-arrow-left"></i></a>
        </div>
        <div class="col s12 m6" style="margin-top:-100px">
          <div class="card-panel" id="div3-strip" style="background-image:url('<?php echo base_url()?>assets/explore/images/frontend/blue-strip.jpg');" data-img-width="1600" data-img-height="78">
            <div id="div3-strip-text" class="center">Do You Have Any Course In Mind?</div>
          </div>
          <div class="card-panel" id="div3-content">
            <div class="row" id="div3-content-action">
              <div class="col s6 l6">
                <div class="row center" id="div3-yes-1">
                  <a class="btn-floating div3-btn white" id="div3-yes">
                    <img src="<?php echo base_url()?>assets/explore/images/frontend/doyouhaveanycourseinyourmindicon-02-01.png">
                  </a>
                  <h5 class="black-text">Yes</h5>                     
                </div>                      
              </div>
              <div class="col s6 l6">
                <div class="row center" id="div3-no-1">
                  <a class="btn-floating div3-btn white" id="div3-no">
                    <img src="<?php echo base_url()?>assets/explore/images/frontend/doyouhaveanycourseinyourmindicon-02-02.png">
                  </a>
                  <h5 class="black-text">No</h5>    
                </div>  
              </div>
            </div>  
            <div id="div3-content-msg">
              <div class="center" id="div3-msg">
              
              </div>
            </div>
          </div>
        </div>
        <div class="col m3"><a class="btn-floating btn-large waves-effect waves-light blue darken-3" id="div_back_3_p" style="margin-left:100px"><i class="mdi-hardware-keyboard-arrow-right"></i></a></div>
      </div>
      <input type="hidden" id="div3-trigger"/>
    </div>
    <!--Education-->
    <div id="div15" class="" style="margin-top:100px"> 
      <div class="row">
        <div class="col m3">
          <a class="btn-floating btn-large waves-effect waves-light blue darken-3" id="div_back_15" style="margin-top: 150px;"><i class="mdi-hardware-keyboard-arrow-left" ></i></a>
        </div>
        <div class="col s12 m6">
          <div class="card-panel" id="div4-strip" style="background-image:url('<?php echo base_url()?>assets/explore/images/frontend/blue-strip.jpg');" data-img-width="1600" data-img-height="78">
            <div id="div4-strip-text" class="center">Please Select Education</div>
          </div>
          <div class="card-panel" id="div4-content">
            <div id="div15-content-action">
              <div class="row center">
                
                  <img src="<?php echo base_url()?>assets/explore/images/frontend/plese-select-courc-icon-01.png" width="111" height="111">
              
              </div>
              <div class="row center">
                <select class="browser-default black-text" id="select_education">
                  <option value="">Select Education</option>
                  <?php foreach($eligibility as $crs_mstr){ ?>
                  <option value="<?php echo '1'.'-'. $crs_mstr->id.'-'.$crs_mstr->name; ?>"><?php echo $crs_mstr->name; ?></option>
                  <?php }  ?> 
                            
                </select> 
              </div>
              <div class="row center validation_msg" id="div15-validation"></div>
              <br/>
              <div class="row center">
                <a class="btn waves-effect waves-light blue div15-btn">Next</a>
              </div>
            </div>
            <div id="div15-content-msg">
              <div class="center" id="div4-msg-3">
              
              </div>
            </div>
          </div>
        </div>
        <div class="col m3"><a class="btn-floating btn-large waves-effect waves-light blue darken-3" id="div_back_15_p" style="margin-left:100px;margin-top:150px"><i class="mdi-hardware-keyboard-arrow-right"></i></a></div>
      </div>
      <input type="hidden" id="div15-trigger"/>
    </div>
    <!--EndEducation-->
    <!--type-->
  
    <div id="div14" class="" style="margin-top:100px"> 
      <div class="row">
        <div class="col m3">
          <a class="btn-floating btn-large waves-effect waves-light blue darken-3" id="div_back_14" style="margin-top:150px"><i class="mdi-hardware-keyboard-arrow-left"></i></a>
        </div>
        <div class="col s12 m6">
          <div class="card-panel" id="div4-strip" style="background-image:url('<?php echo base_url()?>assets/explore/images/frontend/blue-strip.jpg');" data-img-width="1600" data-img-height="78">
            <div id="div4-strip-text" class="center">Please Select type</div>
          </div>
          <div class="card-panel" id="div4-content">
            <div id="div14-content-action">
              <div class="row center">
                
                  <img src="<?php echo base_url()?>assets/explore/images/frontend/plese-select-courc-icon-01.png" width="111" height="111">
              
              </div>
              <div class="row center">
                <select class="browser-default black-text" id="select_type">
                  <option value="">Select Type</option>
                  <?php foreach($mst_type as $crs_mstr){ ?>
                  <option value="<?php echo $crs_mstr->id .'-'.$crs_mstr->name; ?>"><?php echo $crs_mstr->name; ?></option>
                  <?php } ?>                  
                </select> 
              </div>
              <div class="row center validation_msg" id="div14-validation"></div>
              <br/>
              <div class="row center">
                <a class="btn waves-effect waves-light blue div14-btn">Next</a>
              </div>
            </div>
            <div id="div14-content-msg">
              <div class="center" id="div4-msg-2">
              
              </div>
            </div>
          </div>
        </div>
        <div class="col m3"><a class="btn-floating btn-large waves-effect waves-light blue darken-3" id="div_back_14_p" style="margin-left:100px;margin-top:150px"><i class="mdi-hardware-keyboard-arrow-right"></i></a></div>
      </div>
      <input type="hidden" id="div14-trigger"/>
    </div>
    <!--End type-->
    <!--End-->
    <!--Stream-->
    <div id="div13" class="" style="margin-top:100px"> 
      <div class="row">
        <div class="col m3">
          <a class="btn-floating btn-large waves-effect waves-light blue darken-3" id="div_back_13" style="margin-top:150px"><i class="mdi-hardware-keyboard-arrow-left"></i></a>
        </div>
        <div class="col s12 m6">
          <div class="card-panel" id="div4-strip" style="background-image:url('<?php echo base_url()?>assets/explore/images/frontend/blue-strip.jpg');" data-img-width="1600" data-img-height="78">
            <div id="div4-strip-text" class="center">Please Select Stream</div>
          </div>
          <div class="card-panel" id="div4-content">
            <div id="div13-content-action">
              <div class="row center">
                
                  <img src="<?php echo base_url()?>assets/explore/images/frontend/plese-select-courc-icon-01.png" width="111" height="111">
              
              </div>
              <div class="row center">
                <select class="browser-default black-text" id="select_stream">
                  <option value="">Select Stream</option>
                  <?php foreach($course_master as $crs_mstr){ ?>
                  <option value="<?php echo $crs_mstr->id .'-'.$crs_mstr->name; ?>"><?php echo $crs_mstr->name; ?></option>
                  <?php } ?>                  
                </select> 
              </div>
              <div class="row center validation_msg" id="div13-validation"></div>
              <br/>
              <div class="row center">
                <a class="btn waves-effect waves-light blue div13-btn">Next</a>
              </div>
            </div>
            <div id="div4-content-msg">
              <div class="center" id="div4-msg-1">
              
              </div>
            </div>
          </div>
        </div>
        <div class="col m3"><a class="btn-floating btn-large waves-effect waves-light blue darken-3" id="div_back_13_p" style="margin-left:100px;margin-top:150px"><i class="mdi-hardware-keyboard-arrow-right"></i></a></div>
      </div>
      <input type="hidden" id="div13-trigger"/>
    </div>
    <!--End Stream-->


    <div id="div4" class="" style="margin-top:9%">
      <div class="row">
        <div class="col m3">
        <a class="btn-floating btn-large waves-effect waves-light blue darken-3" id="div_back_4" style="margin-top:150px"><i class="mdi-hardware-keyboard-arrow-left"></i></a>
        </div>
        <div class="col s12 m6">
          <div class="card-panel" id="div4-strip" style="background-image:url('<?php echo base_url()?>assets/explore/images/frontend/blue-strip.jpg');" data-img-width="1600" data-img-height="78">
            <div id="div4-strip-text" class="center">Please Select Your Course</div>
          </div>
          <div class="card-panel" id="div4-content">
            <div id="div4-content-action">
              <div class="row center">
                
                  <img src="<?php echo base_url()?>assets/explore/images/frontend/plese-select-courc-icon-01.png" width="111" height="111">
              
              </div>
              <div class="row center">
                <select class="browser-default black-text" id="select_course">
                  <!-- <option value="">Course</option>
                  <?php foreach($course_master as $crs_mstr){ ?>
                  <option value="<?php echo $crs_mstr->id .'-'.$crs_mstr->name; ?>"><?php echo $crs_mstr->name; ?></option>
                  <?php } ?>   -->                
                </select> 
              </div>
              <div class="row center validation_msg" id="div4-validation"></div>
              <br/>
              <div class="row center">
                <a class="btn waves-effect waves-light blue div4-btn">Next</a>
              </div>
            </div>
            <div id="div4-content-msg">
              <div class="center" id="div4-msg">
              
              </div>
            </div>
          </div>
        </div>
        <div class="col m3"><a class="btn-floating btn-large waves-effect waves-light blue darken-3" id="div_back_4_p" style="margin-left:100px;margin-top:150px"><i class="mdi-hardware-keyboard-arrow-right"></i></a></div>
      </div>
      <input type="hidden" id="div4-trigger"/>
    </div>
    <div id="div5" class="">
      <div class="row">
        <div class="col m2">
          <a class="btn-floating btn-large waves-effect waves-light blue darken-3" id="div_back_5" style="margin-top:150px"><i class="mdi-hardware-keyboard-arrow-left"></i></a>
        </div>
        <div class="col s12 m8">
          <div class="card-panel" id="div5-strip" style="background-image:url('<?php echo base_url()?>assets/explore/images/frontend/blue-strip.jpg');" data-img-width="1600" data-img-height="78">
            <div id="div5-strip-text" class="center">Where Do You Want To Study?</div>
          </div>
          <div class="card-panel" id="div5-content">
            <div id="div5-content-action">
              <div class="row center">                  
                <div class="col l1 div5-btn-1" id="div5-Delhi">
                  <img src="<?php echo base_url()?>assets/explore/images/frontend/Whereyouwanttostudy-1.png" width="80" height="80">
                  <h6 class="black-text">Delhi</h6> 
                </div>
                <div class="col l1 div5-btn-1" id="div5-Mumbai">
                  <img src="<?php echo base_url()?>assets/explore/images/frontend/Whereyouwanttostudy-2.png" width="80" height="80">
                  <h6 class="black-text">Mumbai</h6>
                </div>
                <div class="col l1 div5-btn-1" id="div5-Pune">
                  <img src="<?php echo base_url()?>assets/explore/images/frontend/Whereyouwanttostudy-3.png" width="80" height="80">
                  <h6 class="black-text">Pune</h6>
                </div>
                <div class="col l1 div5-btn-1" id="div5-Kolkata">
                  <img src="<?php echo base_url()?>assets/explore/images/frontend/Whereyouwanttostudy-4.png" width="80" height="80">
                  <h6 class="black-text">Kolkata</h6>
                </div>
                <div class="col l1 div5-btn-1" id="div5-Hyderabad">
                  <img src="<?php echo base_url()?>assets/explore/images/frontend/Whereyouwanttostudy-5.png" width="80" height="80">
                  <h6 class="black-text">Hyderabad</h6>
                </div>
                <div class="col l1 div5-btn-1" id="div5-Ahmedabad">
                  <img src="<?php echo base_url()?>assets/explore/images/frontend/Whereyouwanttostudy-6.png" width="80" height="80">
                  <h6 class="black-text">Ahmedabad</h6>
                </div>
                <div class="col l1 div5-btn-1" id="div5-Chandigarh">
                  <img src="<?php echo base_url()?>assets/explore/images/frontend/Whereyouwanttostudy-7.png" width="80" height="80">
                  <h6 class="black-text">Chandigarh</h6>
                </div>
                <div class="col l1 div5-btn-1" id="div5-Bhubanehswar">
                  <img src="<?php echo base_url()?>assets/explore/images/frontend/Whereyouwanttostudy-8.png" width="80" height="80">
                  <h6 class="black-text">Bhubanehswar</h6>
                </div>
                                
                <div class="col l1 div5-btn-1" id="div5-Jaipur">
                  <img src="<?php echo base_url()?>assets/explore/images/frontend/Whereyouwanttostudy-9.png" width="80" height="80">
                  <h6 class="black-text">Jaipur</h6>
                </div>
                <div class="col l1 div5-btn-1" id="div5-Gurgaon">
                  <img src="<?php echo base_url()?>assets/explore/images/frontend/Whereyouwanttostudy-10.png" width="80" height="80">
                  <h6 class="black-text">Gurgaon</h6>
                </div>
                <div class="col l1 div5-btn-1" id="div5-Kochi">
                  <img src="<?php echo base_url()?>assets/explore/images/frontend/Whereyouwanttostudy-11.png" width="80" height="80">
                  <h6 class="black-text">Kochi</h6>
                </div>
                <div class="col l1 div5-btn-1" id="div5-Luckhnow">
                  <img src="<?php echo base_url()?>assets/explore/images/frontend/Whereyouwanttostudy-12.png" width="80" height="80">
                  <h6 class="black-text">Luckhnow</h6>
                </div>
                <div class="col l1 div5-btn-1" id="div5-Surat">
                  <img src="<?php echo base_url()?>assets/explore/images/frontend/Whereyouwanttostudy-13.png" width="80" height="80">
                  <h6 class="black-text">Surat</h6>
                </div>
                <div class="col l1 div5-btn-1" id="div5-Banglore">
                  <img src="<?php echo base_url()?>assets/explore/images/frontend/Whereyouwanttostudy-14.png" width="80" height="80">
                  <h6 class="black-text">Banglore</h6>
                </div>
                <div class="col l1 div5-btn-1" id="div5-Chennai">
                  <img src="<?php echo base_url()?>assets/explore/images/frontend/Whereyouwanttostudy-15.png" width="80" height="80">
                  <h6 class="black-text">Chennai</h6>
                </div>
                <div class="col l1 div5-btn-1" id="div5-Other">
                  <img src="<?php echo base_url()?>assets/explore/images/frontend/Whereyouwanttostudy-16.png" width="80" height="80">
                  <h6 class="black-text">Other City</h6>
                </div>
              </div>
              <div class="row center validation_msg" id="div5-validation"></div>
              <div class="row center">
                <a class="btn waves-effect waves-light blue div5-btn"  style="margin-top: -74px;">Next</a>
                <a class="btn-floating btn-large waves-effect waves-light blue"><p id="div5_count_location" style="margin-top: 0px; font-size:50px;margin-top: 9px;">0</p></a>
              </div>
            </div>
            <div id="div5-content-msg">
              <div class="center" id="div5-msg">
              
              </div>
            </div>
          </div>
        </div>
        <div class="col m2"><a class="btn-floating btn-large waves-effect waves-light blue darken-3" id="div_back_5_p" style="margin-left:100px;margin-top:150px"><i class="mdi-hardware-keyboard-arrow-right"></i></a></div>
      </div>
      <input type="hidden" id="div5-trigger"/>
    </div>
    <div id="div6" class="animated bounceInRight">
      <div class="row">
        <div class="col m3">
          <a class="btn-floating btn-large waves-effect waves-light green darken-3" id="div_back_6"><i class="mdi-hardware-keyboard-arrow-left"></i></a>
        </div>
        <div class="col s12 m6">
          <div class="card-panel" id="div6-strip" style="background-image:url('<?php echo base_url()?>assets/explore/images/frontend/green-strip.jpg');" data-img-width="1600" data-img-height="78">
            <div id="div6-strip-text" class="center">What's Your highest Qualification?</div>
          </div>
          <div class="card-panel" id="div6-content">
            <div id="div6-content-action">
              <div class="row center">
                <div class="col l2 div6-btn" id="div6-SSC">
                  <img class="div6-content-img" src="<?php echo base_url()?>assets/explore/images/frontend/whats-your-highest-qualification-icons-01.png"/>
                  <b>SSC</b>
                </div>
                <div class="col l2 div6-btn" id="div6-HSC">
                  <img class="div6-content-img" src="<?php echo base_url()?>assets/explore/images/frontend/whats-your-highest-qualification-icons-02.png"/>
                  <b>HSC</b>
                </div>
                <div class="col l2 div6-btn" id="div6-GRADUATION">
                  <img class="div6-content-img" src="<?php echo base_url()?>assets/explore/images/frontend/whats-your-highest-qualification-icons-03.png"/>
                  <b>Graduation</b>
                </div>
                <div class="col l2 div6-btn" id="div6-POST GRADUATION">
                  <img class="div6-content-img" src="<?php echo base_url()?>assets/explore/images/frontend/whats-your-highest-qualification-icons-04.png"/>
                  <b>Post Graduation</b>
                </div>
                <div class="col l2 div6-btn" id="div6-PHD">
                  <img class="div6-content-img" src="<?php echo base_url()?>assets/explore/images/frontend/whats-your-highest-qualification-icons-05.png"/>
                  <b>PHD</b>
                </div>              
              </div>
            </div>
            <div id="div6-content-msg">
              <div class="center" id="div6-msg">
              
              </div>
            </div>
          </div>
        </div>
        <div class="col m3">
          <a class="btn-floating btn-large waves-effect waves-light blue darken-3" id="div_back_7_p" style="margin-left:100px;margin-top:150px"><i class="mdi-hardware-keyboard-arrow-right"></i></a>
        </div>
      </div>
      <input type="hidden" id="div6-trigger"/>
    </div>
    <div id="div7" class="animated bounceInRight">
      <div class="row">
        <div class="col m3">
          <a class="btn-floating btn-large waves-effect waves-light blue darken-3" id="div_back_7" style="margin-top:150px"><i class="mdi-hardware-keyboard-arrow-left"></i></a>
        </div>
        <div class="col s12 m6">
          <div class="card-panel" id="div7-strip" style="background-image:url('<?php echo base_url()?>assets/explore/images/frontend/blue-strip.jpg');" data-img-width="1600" data-img-height="78">
            <div id="div7-strip-text" class="center">Who Are You?</div> 
          </div>
          <div class="card-panel" id="div7-content">
            <div id="div7-content-action">
              <div class="row center">
                <div class="col s6 l6 div7-btn" id="div7-BOY">
                  <img class="div7-content-img" src="<?php echo base_url()?>assets/explore/images/frontend/who-are-you-icon-01.png"/>
                  <b><br/>Boy</b>
                </div>
                <div class="col s6 l6 div7-btn" id="div7-GIRL">
                  <img class="div7-content-img" src="<?php echo base_url()?>assets/explore/images/frontend/who-are-you-icon-02.png"/>
                  <b><br/>Girl</b>
                </div>        
              </div>
            </div>
            <div id="div7-content-msg">
              <div class="center" id="div7-msg">
              
              </div>
            </div>
          </div>
        </div>
        <div class="col m3">
          <a class="btn-floating btn-large waves-effect waves-light blue darken-3" id="div_back_7_p" style="margin-left:100px;margin-top:150px"><i class="mdi-hardware-keyboard-arrow-right"></i></a>
        </div>
      </div>
      <input type="hidden" id="div7-trigger"/>
    </div>
    <div id="div8" class="animated bounceInRight">
      <div class="row">
        <div class="col m3">
          <a class="btn-floating btn-large waves-effect waves-light blue darken-3" id="div_back_8" style="margin-top:94px"><i class="mdi-hardware-keyboard-arrow-left"></i></a>
        </div>
        <div class="col s12 m6">
          <div class="card-panel" id="div8-strip" style="background-image:url('<?php echo base_url()?>assets/explore/images/frontend/blue-strip.jpg');" data-img-width="1600" data-img-height="78">
            <div id="div8-strip-text" class="center">Ohh!! We Forget To Catch Your Name</div> 
          </div>
          <div class="card-panel" id="div8-content">
            <div id="div8-content-action">
              <div class="row center">
                <div class="col s2 l3">&nbsp;</div>
                <div class="input-field col s8 l6 black-text">
                  <?php
                   $udata=$this->session->userdata('logged_in');
                   ?>
                     <?php if($udata!=''){?>
                        <!-- <input id="email" type="text" placeholder="Email" value="<?php echo $udata['email']?>" readonly><br/> -->
                          <input id="first_name" type="text" placeholder="Your Name"  value="<?php echo $udata['name']?>" readonly>
                        <?php }else{?>
                        <input id="first_name" type="text" placeholder="Your Name" ><br/>
                        <?php } ?>
                
                </div>

                <div class="col s2 l3">&nbsp;</div>
              </div>
              <div class="row center validation_msg" id="div8-validation" style="margin-top:-64px"></div>
              <div class="row center">
                <a class="btn waves-effect waves-light blue div8-btn">Next</a>
              </div>
            </div>
            <div id="div8-content-msg">
              <div class="center" id="div8-msg">
              
              </div>
            </div>
          </div>
          
        </div>
        <div class="col m3"><a class="btn-floating btn-large waves-effect waves-light blue darken-3" id="div_back_8_p" style="margin-left:100px;margin-top:94px"><i class="mdi-hardware-keyboard-arrow-right"></i></a></div>
      </div>
      <input type="hidden" id="div8-trigger"/>
    </div>
    <div id="div9" class="animated bounceInRight">
      <div class="row">
        <div class="col m3">
          <a class="btn-floating btn-large waves-effect waves-light blue darken-3" id="div_back_9" style="margin-top:200px"><i class="mdi-hardware-keyboard-arrow-left"></i></a>
        </div>
        <div class="col s12 m6">
          <div class="card-panel" id="div9-strip" style="background-image:url('<?php echo base_url()?>assets/explore/images/frontend/blue-strip.jpg');" data-img-width="1600" data-img-height="78">
            <div id="div9-strip-text" class="center">What's Your Age?</div> 
          </div>
          <div class="card-panel" id="div9-content">
            <div id="div9-content-action">
              <div class="row">
                <div class="col s12 l12">
                  <div id="year_selector"></div>
                </div>  
              </div>  
              <div class="row center-align" style="margin-top:10px;">
                <div class="col s12 l5" id="months-div" style="display:none;">
                  <div class="row">
                    <div class="col s3 l3"><div class="month_select" id="month_1">JAN</div></div>
                    <div class="col s3 l3"><div class="month_select" id="month_2">FEB</div></div>
                    <div class="col s3 l3"><div class="month_select" id="month_3">MAR</div></div>
                    <div class="col s3 l3"><div class="month_select" id="month_4">APR</div></div>
                  </div>
                  <div class="row">
                    <div class="col s3 l3"><div class="month_select" id="month_5">MAY</div></div>
                    <div class="col s3 l3"><div class="month_select" id="month_6">JUN</div></div>
                    <div class="col s3 l3"><div class="month_select" id="month_7">JUL</div></div>
                    <div class="col s3 l3"><div class="month_select" id="month_8">AUG</div></div>
                  </div>
                  <div class="row">
                    <div class="col s3 l3"><div class="month_select" id="month_9">SEP</div></div>
                    <div class="col s3 l3"><div class="month_select" id="month_10">OCT</div></div>
                    <div class="col s3 l3"><div class="month_select" id="month_11">NOV</div></div>
                    <div class="col s3 l3"><div class="month_select" id="month_12">DEC</div></div>
                  </div>
                </div>
                <style>
                .day_select
                {
                  display:none;
                }
                </style>
                <div class="col s12 l6" id="days-div" style="display:none;" style="height:-53px">
                  <div class="row center-align" id="days-div-row" >
                    <div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_1">1&nbsp;</div></div>
                    <div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_2">2&nbsp;</div></div>
                    <div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_3">3&nbsp;</div></div>
                    <div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_4">4&nbsp;</div></div>
                    <div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_5">5&nbsp;</div></div>
                    <div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_6">6&nbsp;&nbsp;</div></div>
                    <br/>
                    <div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_7">7&nbsp;</div></div>
                    <div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_8">8&nbsp;</div></div>
                    <div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_9">9&nbsp;</div></div>
                    <div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_10">10</div></div>
                    <div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_11">11</div></div>
                    <div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_12">12</div></div>
                    <br/>
                    <div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_13">13</div></div>
                    <div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_14">14</div></div>
                    <div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_15">15</div></div>
                    <div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_16">16</div></div>
                    <div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_17">17</div></div>
                    <div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_18">18</div></div>
                    <br/>
                    <div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_19">19</div></div>
                    <div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_20">20</div></div>
                    <div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_21">21</div></div>
                    <div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_22">22</div></div>
                    <div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_23">23</div></div>
                    <div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_24">24</div></div>
                    <br/>
                    <div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_25">25</div></div>
                    <div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_26">26</div></div>
                    <div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_27">27</div></div>
                    <div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_28">28</div></div>
                    <div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_29">29</div></div>
                    <div class="col s2 l2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_30">30</div></div>
                    <br/>
                    <div class="col s2 sl2"><div class="waves-effect waves-teal btn white black-text day_select" id="day_31">31</div></div>
                  </div>
                  
                </div>
              </div>
              <div class="row  center-align" id="finaldate-div" style="display:none">
                <div class="col s12 l8">
                  <h5 class="center black-text"  style="margin-top: -50px;margin-left: -206px;font-weight:  bold;">
                    <span id="year"></span>
                    <span id="month"></span>
                    <span id="day"></span>
                  </h5>                     
                </div>
                <div class="row center validation_msg" id="div9-validation"></div>                
                <div class="col s12 l4">
                  <a class="btn waves-effect waves-light blue div9-btn" style="margin-top:-46px">Next</a>
                </div>
              </div>
            </div>
            <div id="div9-content-msg">
              <div class="center" id="div9-msg">
              
              </div>
            </div>
          </div>
        </div>
        <div class="col m3">  <a class="btn-floating btn-large waves-effect waves-light blue darken-3" id="div_back_9_p" style="margin-left:100px;margin-top:200px"><i class="mdi-hardware-keyboard-arrow-right"></i></a></div>
      </div>
      <input type="hidden" id="div9-trigger"/>
    </div>
    <div id="div10" class="animated bounceInRight">
      <div class="row">
        <div class="col m3">
          <a class="btn-floating btn-large waves-effect waves-light green darken-3" id="div_back_10"><i class="mdi-hardware-keyboard-arrow-left"></i></a>
        </div>
        <div class="col s12 m6">
          <div class="card-panel" id="div10-strip" style="background-image:url('<?php echo base_url()?>assets/explore/images/frontend/green-strip.jpg');" data-img-width="1600" data-img-height="78">
            <div id="div10-strip-text" class="center">Are You Studying Currently?</div> 
          </div>
          <div class="card-panel" id="div10-content">
            <div id="div10-content-action">
              <div class="row">
                <div class="col s6 l6">
                  <div class="row center">
                    <a class="btn-floating div10-btn white" id="div10-yes">
                      <img src="<?php echo base_url()?>assets/explore/images/frontend/are-yo-studying-currently-icon-01.png">
                    </a>
                    <h5 class="black-text">Yes</h5>                     
                  </div>                      
                </div>
                <div class="col s6 l6">
                  <div class="row center">
                    <a class="btn-floating div10-btn white" id="div10-no">
                      <img src="<?php echo base_url()?>assets/explore/images/frontend/are-yo-studying-currently-icon-02.png">
                    </a>
                    <h5 class="black-text">No</h5>    
                  </div>  
                </div>  
              </div>  
            </div>
            <div id="div10-content-msg">
              <div class="center" id="div10-msg">
              
              </div>
            </div>            
          </div>
        </div>
        <div class="col m3">&nbsp;</div>
      </div>
      <input type="hidden" id="div10-trigger"/>
    </div>
    <div id="div11" class="animated bounceInRight">
      <div class="row">
        <div class="col m3">
          <a class="btn-floating btn-large waves-effect waves-light blue darken-3" id="div_back_11"><i class="mdi-hardware-keyboard-arrow-left"></i></a>
        </div>
        <div class="col s12 m6">
          <div class="card-panel" id="div11-strip" style="background-image:url('<?php echo base_url()?>assets/explore/images/frontend/blue-strip.jpg');" data-img-width="1600" data-img-height="78">
            <div id="div11-strip-text" class="center">What Course Are You Pursuing?</div>
          </div>
          <div class="card-panel" id="div11-content">
            <div id="div11-content-action">
              <div class="row center">
                
                  <img src="<?php echo base_url()?>assets/explore/images/frontend/what-course-are-you-pursuing-icon-01.png" width="111" height="111">
              
              </div>
              <div class="row center">
                <select class="browser-default black-text" id="div11-course-select">
                  <option value="">Course</option>
                  <?php foreach($course_master as $crs_mstr){ ?>
                  <option value="<?php echo $crs_mstr->id .'-'.$crs_mstr->name; ?>"><?php echo $crs_mstr->name; ?></option>
                  <?php } ?>
                </select> 
              </div>
              <div class="row center validation_msg" id="div11-validation"></div>
              <div class="row center">
                <a class="btn waves-effect waves-light blue div11-btn">Next</a>
              </div>
            </div>
            <div id="div11-content-msg">
              <div class="center" id="div11-msg">
              
              </div>
            </div>
          </div>
        </div>
        <div class="col m3">&nbsp;</div>
      </div>
      <input type="hidden" id="div11-trigger"/>
    </div>
    <div id="div12" class="animated bounceInRight">
      <div class="row">
        <div class="col m3">
          <a class="btn-floating btn-large waves-effect waves-light green darken-3" id="div_back_12" style="margin-top:200px"><i class="mdi-hardware-keyboard-arrow-left"></i></a>
        </div>
        <div class="col s12 m6">
          <div class="card-panel" id="div12-strip" style="background-image:url('<?php echo base_url()?>assets/explore/images/frontend/green-strip.jpg');" data-img-width="1600" data-img-height="78">
            <div id="div12-strip-text" class="center">Contact</div>
          </div>
          <div class="card-panel" id="div12-content"> 
            <div id="div12-content-action">
              <div class="row center">
                <div class="row center black-text">
                  <div class="col l7">
                    <div class="row" id="phone-row">
                      <div id="errorMsg" style="color:red"></div>
                      <div class="input-field col l12">
                        <!-- <input id="email" type="text" placeholder="Email"><br/> -->
                        <?php $udata=$this->session->userdata('logged_in');?>
                          <?php if($udata!=''){?>
                        <input id="email" type="text" placeholder="Email" value="<?php echo $udata['email']?>" readonly><br/>
                        <?php }else{?>
                        <input id="email" type="text" placeholder="Email" value=""><br/>
                        <?php } ?>
                      </div>                    
                    </div>
                  </div>  
                    <?php if($this->session->userdata('logged_in')==''){?>
                    <div class="col l7" style="margin-top:-175px">
                    <div class="row" id="phone-row">
                      <div id="errorMsg" style="color:red"></div>
                      <div class="input-field col l12">
                        
                        
                        <input id="password" type="password" placeholder="Password">
                        <input id="rep_password" type="password" placeholder="Repeat Password">                     
                      </div>                    
                    </div>
                  </div>              
                  <div class="col s12 l5" style="margin-top:-218px">
                    <div class="row">
                      <div class="input-field col s10 l10">                     
                        <div id="mobilenumberdiv">
                          <span id="mobilenumber">
                            <input type="tel" name="" id="mobile_num" placeholder="Mobile"/>
                          </span>                         
                        </div>                        
                      </div>
                      <div class="col s2 l2">                     
                        <i class="mdi-content-backspace right" style="margin-left:1px;font-size: 35px;margin-top: 18px;" id="phone_delete_btn"></i>                       
                      </div>
                    </div>
                    <div class="row">
                      <div class="col s12 l12" id="phonenumber-div">
                        <div class="row">
                          <div class="col s3 l3 green phonenumber" id="phone_7">7</div>
                          <div class="col s3 l3 green phonenumber" id="phone_8">8</div>
                          <div class="col s3 l3 green phonenumber" id="phone_9">9</div>
                        </div>
                        <div class="row">
                          <div class="col s3 l3 green phonenumber" id="phone_4">4</div>
                          <div class="col s3 l3 green phonenumber" id="phone_5">5</div>
                          <div class="col s3 l3 green phonenumber" id="phone_6">6</div>
                        </div>
                        <div class="row">
                          <div class="col s3 l3 green phonenumber" id="phone_1">1</div>
                          <div class="col s3 l3 green phonenumber" id="phone_2">2</div>
                          <div class="col s3 l3 green phonenumber" id="phone_3">3</div>
                        </div>
                        <div class="row">
                          <div class="col s3 l3 green phonenumber" id="phone_*">*</div>
                          <div class="col s3 l3 green phonenumber" id="phone_0">0</div>
                          <div class="col s3 l3 green phonenumber" id="phone_#">#</div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <?php }?>
                </div>
              </div>
              <div class="row center">
                <button type="button" class="btn waves-effect waves-light green div12-btn" style="margin-top:-100px">Done</button>
              </div>
            </div>
            <div id="div12-content-msg">
              <div class="center" id="div12-msg">
              
              </div>
            </div>
          </div>
        </div>
        <div class="col m3">&nbsp;</div>
      </div>
      <input type="hidden" id="div12-trigger"/>
    </div>

<div class="template_section" style="background-color: #fff;">
    <body id="top" class="has-header-search">
       <section>
            
              <div class="row">
                <div class="col-md-12">
             
                    <div class="border-bottom-tab">
 <div class="text-center  wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
                  <h3 class="text-uppercase text-bold mb-40" style="margin-top:40px">Most Preferred  Courses</h3>
                 
              </div>
                      <!-- Nav tabs -->
                      <ul class=" nav-tabs hidden-xs" role="tablist" style="  margin-left: 120px;">
                         <li role="presentation" class="active"><a href="#tab-1" role="tab" class=" waves-dark" data-toggle="tab" aria-expanded="true" onclick="getCource(0)">All</a></li>
                        <li role="presentation" class=""><a href="#tab-1" role="tab" class="waves-dark" data-toggle="tab" aria-expanded="true" onclick="getCource(2)">Under Graguate</a></li>
                        <li role="presentation" class=""><a href="#tab-2" role="tab" class="waves-dark" data-toggle="tab" aria-expanded="false" onclick="getCource(4)">Graduate</a></li>
                        <li role="presentation" class=""><a href="#tab-3" role="tab" class=" waves-dark" data-toggle="tab" aria-expanded="false" onclick="getCource(4)">Post Graduate</a></li>
                      <!--   <li role="presentation" class=""><a href="#tab-4" role="tab" class="waves-effect waves-dark" data-toggle="tab" aria-expanded="false">Masters</a></li> -->
                      </ul><div class="panel-group visible-xs" id="undefined-accordion"></div>

                      <!-- Tab panes -->

                      <div class="panel-body">
                        <div class="tab-content">
                          <div role="tabpanel" class="tab-pane fade active in" id="tab-1">
                              <div class="container">
                                  <div class='row'>
                                  <div class='col-md-12'>
                                    <div class="carousel slide media-carousel" id="media" style="height:200px;padding: 0px;">
                                      <div class="carousel-inner flex-active-slide" id="topcourse" >
                                           
                                  </div>
                                </div>
                            </div>
                          </div>
                          
                        </div>
                      </div>

                    </div><!-- /.border-bottom-tab -->

                </div>
                <div class="col-md-12">

                    <div class="icon-tab">

                      <!-- Nav tabs -->
                      <!-- <div class="text-center container">
                        <ul class="nav nav-pills" role="tablist">

                          <li role="presentation" class="active"><a href="#icontab-1" class="waves-effect waves-light"  role="tab" data-toggle="tab" onclick="getCource(2)"> Under Graduate</a></li>

                          <li role="presentation"><a href="#icontab-2" class="waves-effect waves-light" role="tab" data-toggle="tab" onclick="getCource(4)">Graduate</a></li>

                          <li role="presentation"><a href="#icontab-3" class="waves-effect waves-light" role="tab" data-toggle="tab" onclick="getCource(4)"> Post Graduate </a></li>
              
                        <!--   <li role="presentation"><a href="#icontab-4" class="waves-effect waves-light" role="tab" data-toggle="tab" onclick="getCource(0)"> Masters </a></li> -->
                      <!--   </ul>
                      </div>  -->

                      <!-- Tab panes -->
                      <div class="panel-body mt-40">
            
                        <div class="tab-content">
                          <div role="tabpanel" class="tab-pane fade in active" id="icontab-1-1">

                            
                          </div>

                          

                          <div role="tabpanel" class="tab-pane fade" id="icontab-3-1">

                            <div class="row">
                <div id="testimonial-two" class="carousel slide carousel-testimonial text-center" data-ride="carousel">

                  <!-- Wrapper for slides -->
                  <div class="carousel-inner" role="listbox">
                  
                  </div>
                  
                  <!-- Controls 
                  <a class="left carousel-control" href="#testimonial-two" role="button" data-slide="prev">
                  <span class="material-icons" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                  </a>
                  <a class="right carousel-control" href="#testimonial-two" role="button" data-slide="next">
                  <span class="material-icons" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                  </a>-->
                </div>
                            </div>
                          </div>
                          
                          <div role="tabpanel" class="tab-pane fade" id="icontab-4-1">
                           
                          </div>
                        </div>
                      </div>
                      
                    </div><!-- /.icon-tab -->

                </div><!-- /.col-md-12 -->
              </div><!-- /.row -->

            </div><!-- /.container -->
        </section>
 <section class="container">
   
    <div class="row active-with-click">
        
      </div>
</section>


    <?php
    // print_r($top_uni);
$data_ar = $top_uni;//your array here;
$temp_ar = array();

foreach($data_ar as $value)
{
    $tempCheck = 0;
    foreach($temp_ar as $val)
    {
        if($value['um_name'] == $val['um_name']) // whatever the item value you want to check wheather its uc_um_id or um_name
        {
            $tempCheck = 1;
        }
    }
    if($tempCheck === 0)
    $temp_ar[] = $value;
}

 ?>
             <section class="gray-bg">
                  <div class="container">
                    <div class="row">

              <div class="text-center mb-40 wow fadeInUp" style="visibility: visible;">
                <h3 class="text-uppercase text-center text-bold mb-40" style="padding: 15px;">most preferred  Universities</h3>
              </div>
              <div class="row no-gutter">
               <!--  <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                  <figure class="cp-gallery">
                    <span class="hover-ripple right" data-hover-color="#000" data-hover-opacity="0.6"></span>
                    <img src="<?php echo base_url('assets/img/gateway.jpg'); ?>" class="img-responsive" alt="">
                    <figcaption>
                      <span class="text-uppercase">INDIA</span>
                      <h3 class="text-uppercase"><a href="#" class="white-link link-underline">India</a></h3>
                    </figcaption>
                  </figure>
                </div> -->
                <!--Start foreach-->
                <?php //for($i=0;$i<count($unique_data);$i++) {
                    foreach ($temp_ar as  $value) {
                  ?>
              <!--  <div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
                  <figure class="cp-gallery">
                    <span class="hover-ripple right" data-hover-color="#000" data-hover-opacity="0.6"></span>
                    <img src="<?php echo base_url(''); ?>uploads/coverimages/<?php echo $value['ui_coverimage'];?>" class="img-responsive" alt="" style="width:195px;height:163px">
                    <figcaption>
                      <span class="text-uppercase white-link" style="color:white"><a href="<?php echo base_url()?>listing/index/<?php echo $value['um_name']; ?>" class="white-link link-underline"><?php echo $value['um_name'];?></a></span>
                      <h3 class="text-uppercase"><a href="<?php echo base_url()?>listing/index/<?php echo $value['um_name']; ?>" class="white-link link-underline"></a></h3>
                    </figcaption>
                  </figure>
                </div> -->

                <div class="col-md-3">
                          <article class="material-card Light-Blue">
                             <h2>
                                  <span><?php echo $value['um_name'];?></span>
                                  
                              </h2>
                              <div class="mc-content">
                                  <div class="img-container" style=" box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
                                      <img class="img-responsive "  src="<?php echo base_url(''); ?>uploads/coverimages/<?php echo $value['ui_coverimage'];?>" style="height:100%; box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
                                  </div>
                                  <div class="mc-description">
                                    <?php echo $value['um_name'];?>
                                  </div>
                              </div>
                              <a class="mc-btn-action">
                                  <i class="fa fa-bars"></i>
                              </a>
                              <div class="mc-footer">
                                <a style="background-color: #0e2a4b;color:white;height: 52px;width: 170px;font-size: 17px;padding-top: -9px;margin-left: 32px;" class="btn" href="<?php echo base_url()?>listing/index/<?php echo $value['um_name']; ?>">View More   <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                               <!-- 
                                  <a class="fa fa-fw fa-facebook"></a>
                                  <a class="fa fa-fw fa-twitter"></a>
                                  <a class="fa fa-fw fa-linkedin"></a>
                                  <a class="fa fa-fw fa-google-plus"></a> -->
                              </div>
                          </article>
                </div>
                <?php }?>

              </div>
  </div>                </div><!-- /.container -->
        </section>
          <section id="about" class="gray-bg" style="background-color:#fff!important;">
            <div class="container">

              <div class="text-center mb-80 wow fadeInUp" style="visibility: visible;">
                  <h3 class="text-uppercase text-bold mb-40" style="padding: 15px;">most preferred  University in cities</h3>
                 
              </div>
<?php //echo '<pre>',print_r($top_cities_5); ?>
              <div class="vertical-tab">
                <div class="row">
                  <div class="col-sm-4">
                        <!-- Nav tabs -->
                      <!--   <ul class="nav nav-tabs nav-stacked hidden-xs" role="tablist">
                          
                          <li role="presentation" class="active"><a href="#tab-10" class="waves-effect waves-light js-tabcollapse-panel-heading" role="tab" data-toggle="tab" data-parent="" onclick='get_university_city(1851)'>Pune</a></li>
                          <li role="presentation"><a href="#tab-11" class="waves-effect waves-light js-tabcollapse-panel-heading" role="tab" data-toggle="tab" data-parent="" onclick='get_university_city()'>Mumbai</a></li>
                          <li role="presentation"><a href="#tab-12" class="waves-effect waves-light js-tabcollapse-panel-heading" role="tab" data-toggle="tab" data-parent="" onclick='get_university_city()'>Delhi</a></li>
                          <li role="presentation"><a href="#tab-13" class="waves-effect waves-light js-tabcollapse-panel-heading" role="tab" data-toggle="tab" data-parent="" onclick='get_university_city()'>Banglore</a></li>
                          <li role="presentation"><a href="#tab-14" class="waves-effect waves-light js-tabcollapse-panel-heading" role="tab" data-toggle="tab" data-parent="" onclick='get_university_city()'>Hyderabad</a></li>
                          
                        </ul><div class="panel-group visible-xs" id="undefined-accordion"></div>    -->  
                       <!--   <ul class="nav nav-tabs nav-stacked hidden-xs" role="tablist">
                          <?php //echo'<pre>',print_r($top_cities);?>
                          <?php $count=5;
                            foreach ($top_cities as $city) {
                              $active='';
                              //echo  $city->city;
                              if($city->city==='Pune'){
                                $active.="active";
                              }
                            ?>
                            <li role="presentation" class="<?php echo $active;?>"><a href="#tab-<?php echo $count++?>" class="waves-effect waves-light js-tabcollapse-panel-heading" role="tab" data-toggle="tab" data-parent="" onclick='get_university_city(<?php echo $city->city_id?>)'><?php echo $city->city?></a></li>
                            <?php
                            }
                          ?> -->

                      <div class="panel-group" id="accordion1" style="color:white">
                          <?php  $count=5;
                          $img='';
                            foreach ($top_cities_5 as $city) {
                         
                          $active='';
                              $img='';
                         
                                if(trim($city->cim_name)==='Mumbai'){
                                $active.="in";
                              $img='<img src="https://lonelyplanetimages.imgix.net/a/g/hi/t/7485b46b2aa78a9c4e8384f3f40dca15-mumbai-bombay.jpg?sharp=10&vib=20&w=1200" class="img-responsive" alt="" style="width: 357px;height: 200px;margin-bottom: -23px;">';
                              }else if(trim($city->cim_name)==='Pune'){
                                $img='<img src="https://pbs.twimg.com/profile_images/1360408852/shaniwar-wada-prasad-dharmadhikari_400x400.jpg" class="img-responsive" alt="" style="width: 357px;height: 200px;margin-bottom: -23px;">';
                              }else if(trim($city->cim_name)==='Hyderabad'){
                                  $img='<img src="http://www.culturalindia.net/iliimages/Charminar-ili-45-img-4.jpg" class="img-responsive" alt="" style="width: 357px;height: 200px;margin-bottom: -23px;">';
                              }else if(trim($city->cim_name)==='Jaipur'){
                                  $img='<img src="https://images.thrillophilia.com/image/upload/s--nDJNyNO0--/c_fill,f_auto,fl_strip_profile,h_446,q_auto,w_750/v1/images/photos/000/046/200/original/rambagh-20palace.jpg.jpg?1453316315" class="img-responsive" alt="" style="width: 357px;height: 200px;margin-bottom: -23px;">';
                              }


                              
                              
                            ?>
                      <div class="panel panel-default" style="box-shadow:none">
                          <a data-toggle="collapse" data-parent="#accordion1" href="#collapsetwo<?=$city->uc_id?>" onclick='get_university_city(<?php echo $city->cim_id?>)'><div class="panel-heading" style="border-top-right-radius: 0px;border-top-left-radius: 0px;    background-color: #F5f5f5;color:black">
                              <h4 class="panel-title">
                                  
                                  </span><b><?=$city->cim_name?></b>
                              </h4>
                          </div></a>
                          <div id="collapsetwo<?=$city->uc_id?>" class="panel-collapse collapse <?=$active?>">
                              <div class="panel-body">
                                  <table class="table">
                                      <tr>
                                          <td>
                                             <?php

                                                  echo $img;
                                             ?>
                                          
                                               <!-- <img src="https://pbs.twimg.com/profile_images/1360408852/shaniwar-wada-prasad-dharmadhikari_400x400.jpg" class="img-responsive" alt="" style="width: 357px;height: 200px;margin-bottom: -23px;"> -->
                                          </td>
                                      </tr>
                                     
                                  </table>
                              </div>
                          </div>
                      </div>
                      <?php } ?>

                        <ul class=" nav-tabs nav-stacked hidden-xs" role="tablist">
                          <li ><a  href="<?php echo base_url() ?>explore/university_view/<?php  echo $top_cities[0]->city_id?>" class="waves-effect waves-light js-tabcollapse-panel-heading" style="background-color:#3f51b5;width:100%;">view all &nbsp;&nbsp;&nbsp;<i class="fa fa-long-arrow-right" aria-hidden="true"></i></a></li>
                     </ul>
        
            </div>
                          <!-- <li role="presentation"><a href="<?php echo base_url() ?>explore/university_view/<?php  echo $top_cities[0]->city_id?>" class="waves-effect waves-light js-tabcollapse-panel-heading" style="background-color:#3f51b5">view all &nbsp;&nbsp;&nbsp;<i class="fa fa-long-arrow-right" aria-hidden="true"></i></a></li>  -->
                          <!-- <li role="presentation"><a href="#tab-6" class="waves-effect waves-light js-tabcollapse-panel-heading" role="tab" data-toggle="tab" data-parent="" onclick='get_top_city()'>Mumbai</a></li>
                          <li role="presentation"><a href="#tab-7" class="waves-effect waves-light js-tabcollapse-panel-heading" role="tab" data-toggle="tab" data-parent="" onclick='get_top_city()'>Delhi</a></li>
                          <li role="presentation"><a href="#tab-8" class="waves-effect waves-light js-tabcollapse-panel-heading" role="tab" data-toggle="tab" data-parent="" onclick='get_top_city()'>Banglore</a></li>
                          <li role="presentation"><a href="#tab-9" class="waves-effect waves-light js-tabcollapse-panel-heading" role="tab" data-toggle="tab" data-parent="" onclick='get_top_city()'>Hyderabad</a></li> -->
                          
                        </ul>                 
                  </div><!-- /.col-md-3 -->

                  <div class="col-sm-8">
                        <!-- Tab panes -->
                        <div class="panel-body">
                          <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="tab-10">
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3" style="padding-left:0px;padding-right: 2px;">
                  <figure class="cp-gallery">
                    <span class="hover-ripple right" data-hover-color="#000" data-hover-opacity="0.6"></span>
                    <img src="<?php echo base_url('assets/img/gateway.jpg'); ?>" class="img-responsive" alt="">
                    <figcaption>
                      <span class="text-uppercase">Montanna Bulevard</span>

                    </figcaption>
                  </figure>
                </div>
              
                          </div>
                            <div role="tabpanel" class="tab-pane fade" id="tab-11">
    
                            
                          </div>
                            <div role="tabpanel" class="tab-pane fade" id="tab-12">
                             
                          </div>
                            <div role="tabpanel" class="tab-pane fade" id="tab-13">
                           
                            
                          </div>
                            <div role="tabpanel" class="tab-pane fade" id="tab-14">
                             
                          </div>
                          </div>
                        </div>
                  </div>
                </div><!-- /.row -->
              </div><!-- /.vertical-tab -->

            </div><!-- /.container -->
        </section>
    <section id="about" class="section-padding " style="background-color: rgba(0, 0, 0, 0.12)!important;padding:17px 0px" >
            <div class="container">

              <div class="text-center mb-80 wow fadeInUp" style="visibility: visible;">
                  <h3 class="text-uppercase text-bold mb-40">Most preferred Courses in cities</h3>
                 
              </div>

              <div class="vertical-tab">
                <div class="row">
                  <div class="col-sm-4">
                         <div class="panel-group" id="accordion">
                          <?php  $count=5; //print_r($top_cities_5);
                            foreach ($top_cities_5 as $city) {
                              $active='';
                              $img='';
                         
                                if(trim($city->cim_name)==='Mumbai'){
                                $active.="in";
                              $img='<img src="https://lonelyplanetimages.imgix.net/a/g/hi/t/7485b46b2aa78a9c4e8384f3f40dca15-mumbai-bombay.jpg?sharp=10&vib=20&w=1200" class="img-responsive" alt="" style="width: 357px;height: 200px;margin-bottom: -23px;">';
                              }else if(trim($city->cim_name)==='Pune'){
                                $img='<img src="https://pbs.twimg.com/profile_images/1360408852/shaniwar-wada-prasad-dharmadhikari_400x400.jpg" class="img-responsive" alt="" style="width: 357px;height: 200px;margin-bottom: -23px;">';
                              }else if(trim($city->cim_name)==='Hyderabad'){
                                  $img='<img src="http://www.culturalindia.net/iliimages/Charminar-ili-45-img-4.jpg" class="img-responsive" alt="" style="width: 357px;height: 200px;margin-bottom: -23px;">';
                              }else if(trim($city->cim_name)==='Jaipur'){
                                  $img='<img src="https://images.thrillophilia.com/image/upload/s--nDJNyNO0--/c_fill,f_auto,fl_strip_profile,h_446,q_auto,w_750/v1/images/photos/000/046/200/original/rambagh-20palace.jpg.jpg?1453316315" class="img-responsive" alt="" style="width: 357px;height: 200px;margin-bottom: -23px;">';
                              }
                              
                            ?>
                      <div class="panel panel-default" style="box-shadow:none">
                          <a href="#" style="color:black">
                            <div class="panel-heading">
                            <a style="color:black" data-toggle="collapse" data-parent="#accordion" id="click-<?=$city->uc_id?>" href="#collapseOne<?=$city->uc_id?>" onclick='get_top_city(<?php echo $city->cim_id?>)'>
                              <h4 class="panel-title">
                                  
                                  </span><b><?=$city->cim_name?></b>
                              </h4>
                          </div>
                        </a>
                          <div id="collapseOne<?=$city->uc_id?>" class="panel-collapse collapse <?=$active?>">
                              <div class="panel-body">
                                  <table class="table">
                                      <tr>
                                          <td>
                                            <?php echo $img;?>
                                               <!-- <img src="https://pbs.twimg.com/profile_images/1360408852/shaniwar-wada-prasad-dharmadhikari_400x400.jpg" class="img-responsive" alt="" style="width: 357px;height: 200px;margin-bottom: -23px;"> -->
                                          </td>
                                      </tr>
                                     
                                  </table>
                              </div>
                          </div>
                      </div>
                      <?php } ?>
                        <ul class=" nav-tabs nav-stacked hidden-xs" role="tablist">
                         <li role="presentation"><a href="<?php echo base_url() ?>explore/university_view/<?php  echo $top_cities[0]->city_id?>" class="waves-effect waves-light js-tabcollapse-panel-heading" style="background-color:#3f51b5;width:100%;">view all &nbsp;&nbsp;&nbsp;<i class="fa fa-long-arrow-right" aria-hidden="true"></i></a></li> 
                     </ul>
        
            </div>
                  
                  </div><!-- /.col-md-3 -->

                  <div class="col-sm-8">
                        <!-- Tab panes -->
                        <div class="panel-body">
                          <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="tab-5">
                        
                          </div>
                            <div role="tabpanel" class="tab-pane fade" id="tab-6">
    
                            
                          </div>
                            <div role="tabpanel" class="tab-pane fade" id="tab-7">
                             
                          </div>
                            <div role="tabpanel" class="tab-pane fade" id="tab-8">
                           
                            
                          </div>
                            <div role="tabpanel" class="tab-pane fade" id="tab-9">
                             
                          </div>
                          </div>
                        </div>
                  </div>
                </div><!-- /.row -->
              </div><!-- /.vertical-tab -->

            </div><!-- /.container -->
        </section>
      
    <section class="section-padding ">
          <div class="container">
              <div class="text-center mb-40 wow fadeInUp" style="visibility: visible;" >
                  <h3 class="text-uppercase text-bold mb-40">Student Testimonial </h3>
                  <div class="icon">
                            <img src="<?php echo base_url();?>assets/img/quote-dark.png" alt="Customer Thumb" draggable="false">
                        </div>
              </div>
<?php if(!empty($testimonial));?>
              <div class="row">
                <div class="col-md-10 col-md-offset-1">

                  <div class="thumb-carousel square-thumb text-center">
                    <ul class="slides">
                 <?php foreach ($testimonial as $value) {
            
                  ?>
                     
                      <li>
                  <div class="content">
                            <p><?php echo $value->testimonial;?></p>

                            <div class="testimonial-meta">
                                <?php echo $value->name;?>
                                <span>- Student</span>
                            </div>
                        </div>
                      </li>
                      <?php } ?>
                   
                    </ul>
                  </div>

                </div>
              </div><!-- /.row -->

          </div><!-- /.container -->
        </section>

</div>


 <footer class="footer footer-four" style="padding:0px 0px 0px; ">
            <div class="primary-footer brand-bg text-center">
                <div class="container">

                  <ul class="social-link tt-animate ltr mt-20">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-tumblr"></i></a></li>
                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                    <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                    <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                    <li><a href="#"><i class="fa fa-rss"></i></a></li>
                  </ul>

                  <hr class="mt-15">

                  <div class="row">
                    <div class="col-md-12">
                          <div class="footer-logo">
                            <img src="<?php echo base_url();?>assets/img/logo-01.png" alt="">
                          </div>

                          <span class="copy-text" style="color:white"> &copy; 2017-18  | <a href="#">Hello Admission</a> &nbsp; | &nbsp;  All Rights Reserved &nbsp; | &nbsp;  Designed By <a href="http://infinite1.in"><img style="width: 100px;padding-bottom: 2px;" src="<?php echo base_url() ?>assets/img/infinite1-logo-128X80-01.png" alt=""></a></span>
                          <p></p>
                          <div class="footer-intro">
                            <p></p>
                          </div>
                    </div><!-- /.col-md-12 -->
                  </div><!-- /.row -->
                </div><!-- /.container -->
            </div><!-- /.primary-footer -->

            <div class="secondary-footer brand-bg darken-2 text-center">
                <div class="container">
                    <ul>
                      <li><a href="<?php echo base_url()?>home">Home</a></li>
                      <li><a href="<?php echo base_url()?>about-us">About us</a></li>
                      <li><a href="<?php echo base_url()?>services">Services</a></li>
                      <li><a href="<?php echo base_url()?>term-condition">Terrms & Condition</a></li>
                      <li><a href="<?php echo base_url()?>contact">Contact us</a></li>
                    </ul>
                </div><!-- /.container -->
            </div><!-- /.secondary-footer -->
        </footer>


  </div>
  <style type="text/css">
  .active_tab{
    background-color: white;
    color:black;
  }
  </style>
  <script src="<?php echo base_url('assets/js/matexjs/scripts.minified.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/matexjs/main.js'); ?>"></script>
    
  <?php 

  /* Course Master Search Data START */
  
  $query = $this->db->query("SELECT * FROM master_program");
  $course_mstr = $query->result();
  
  $new_crs_mstr = array();  
  foreach($course_mstr as $crs_mstr){
    $crsSearch = trim($crs_mstr->name);
    array_push($new_crs_mstr,$crsSearch);
  }
  $new_crs_mstr = array_unique($new_crs_mstr);
  $newCMstr = implode('", "',$new_crs_mstr);
  
  /* Course Master Search Data END */
  
  /* University Master Search Data START */
  
  $query = $this->db->query("SELECT * FROM uni_mstr where um_status='ACCEPTED'");
  $uni_mstr = $query->result();
      
  
  $new_uni_mstr = array();  
  foreach($uni_mstr as $u_mstr){
    $uniSearch = trim($u_mstr->um_name);
    array_push($new_uni_mstr,$uniSearch);
  }
    
  $new_uni_mstr = array_unique($new_uni_mstr);
    
  $newUMstr = implode('", "',$new_uni_mstr);
    
  /* University Master Search Data START */

  //spelizatiopn
    $query1 = $this->db->query("SELECT * FROM master_specialization");
  $uni_mstr1 = $query1->result();
      
  
  $new_uni_mstr1 = array();  
  foreach($uni_mstr1 as $u_mstr1){
    $uniSearch1 = trim($u_mstr1->name);
    array_push($new_uni_mstr1,$uniSearch1);
  }
    
  $new_uni_mstr1 = array_unique($new_uni_mstr1);
    
  $newUMstr1 = implode('", "',$new_uni_mstr1);
  ?>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> 
<script type="text/javascript">
  
    var availableTags = ["<?php echo $newCMstr; ?>","<?php echo $newUMstr; ?>","<?php echo $newUMstr1; ?>"];
//var availableTags = ["<?php echo $newCMstr; ?>"];

  
    $( "#search_text" ).autocomplete({
      source: availableTags,
    select: function(event, ui) {
    //  alert(ui.item.value);
      window.location.href = "<?php echo base_url(); ?>listing/index/"+ui.item.value;
    }
    });

  $(".swapbutton").click(function(){
    
    var swvl = $(this).attr('id');
    if(swvl == 'explore'){
        $('.explore').show();
        $('.search').hide();

    }else{
      $('.search').show();
      $('.explore').hide();
    }
   
  });
  

  
  
</script>
<script type="text/javascript">
    function getCource(id){
$('#topcourse').empty();
//$('#icontab-2').empty();
      $.ajax({
        type:'POST',
        url:'<?php echo base_url()?>explore/get_top_cource',
        data:{'id':id},
        success:function(res){
          var obj=JSON.parse(res);
          if (obj.length==0) {
            html+= '<p>No Record Found</p>';
            console.log(html);
          }
          var html='';
          //var html='<div class="row"><div id="testimonial-two" class="carousel slide carousel-testimonial text-center" data-ride="carousel"><div class="carousel-inner" role="listbox"><div class="item active"><div class="row latest-news-card">';
          $.each(obj,function(i,item){
            console.log(i);
          var cource='';
          if(item.course_spe==null){
            cource='-';
          }else if(item.course_spe!=null){
            cource='-'+item.course_spe;
          }
      
                                 html+=' <div class="item active"  ><div class="col-md-4 "><figure class="portfolio style-11 relative transition ov-hidden" style="box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);">'+
             
                                    '<img src="<?php echo base_url()?>assets_new/img/01.jpg" class="img-responsive" alt="MBA Branding and Advertising" style="height:170px">'+
                                    '<div class="portfolio-center" style="display: block;">'+
                                        '<div class="portfolio-center-inner" style="color:black;font-weight:bold;margin-top:134px;font-size:12px">'+item.spe+' '+cource+'</div>'+
                                    '</div>'+
                                    '<a href="http://lp.campuslama.com/programme-detail/mba/branding-and-advertising" class="title-link" style="color: #fff;">'+
                                    '</a><figcaption class="transition" style="color: #fff; padding:66px 126px; */"><a href="#" class="title-link" style="color: #fff;">'+
                                        '<span class="mb-" style="color:black;font-weight:bold"></span>'+
                                        '</a><p><a href="#" class="title-link" style="color: #fff;"></a><a style="background-color: #0e2a4b;color:white" class="btn" href="<?php echo base_url()?>listing/viewcource/'+item.uc_id+'/'+item.spe+'">View More</a></p>'+
                                    '</figcaption>'+
                                '</figure></div></div>';
                                html+=' <div class="item"><div class="col-md-4 "><figure class="portfolio style-11 relative transition ov-hidden" style="box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);">'+
             
                                    '<img src="<?php echo base_url()?>assets_new/img/01.jpg" class="img-responsive" alt="MBA Branding and Advertising" style="height:170px">'+
                                    '<div class="portfolio-center" style="display: block;">'+
                                        '<div class="portfolio-center-inner" style="color:black;font-weight:bold;margin-top:134px;font-size:12px">'+item.spe+' '+cource+'</div>'+
                                    '</div>'+
                                    '<a href="http://lp.campuslama.com/programme-detail/mba/branding-and-advertising" class="title-link" style="color: #fff;">'+
                                    '</a><figcaption class="transition" style="color: #fff; padding:66px 126px; */"><a href="#" class="title-link" style="color: #fff;">'+
                                        '<span class="mb-" style="color:black;font-weight:bold"></span>'+
                                        '</a><p><a href="#" class="title-link" style="color: #fff;"></a><a style="background-color: #0e2a4b;color:white" class="btn" href="<?php echo base_url()?>listing/viewcource/'+item.uc_id+'/'+item.spe+'">View More</a></p>'+
                                    '</figcaption>'+
                                '</figure></div></div>';
          });

           html+='  <a data-slide="prev" href="#media" class="left carousel-control">‹</a>   <a data-slide="next" href="#media" class="right carousel-control">›</a>';
      
          $('#topcourse').html(html);

          // $('#icontab-2').html(html);
          // $('#icontab-3').html(html);
        }
      });
    }
        function get_top_city(id){
  $('#tab-5').empty();
//$('#icontab-2').empty();
      $.ajax({
        type:'POST',
        url:'<?php echo base_url()?>explore/get_top_city',
        data:{'id':id},
        success:function(res){
          var obj=JSON.parse(res);
          //console.log(obj);
          var html='';
          if (obj.length==0) {
            html+= '<p>No Course Found</p>';
            console.log(html);
          }
          $.each(obj,function(i,item){
            console.log(item);
          var cource='';
          if(item.course_spe==null){
            cource='-';
          }else if(item.course_spe!=null){
            cource='-'+item.course_spe;
          }
          
       
                 html+='<div class="col-md-4" ><figure class="portfolio style-11 relative transition ov-hidden" style="box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);">'+
             
                        '<img src="<?php echo base_url()?>assets_new/img/01.jpg" class="img-responsive" alt="MBA Branding and Advertising">'+
                        '<div class="portfolio-center" style="display: block;">'+
                            '<div class="portfolio-center-inner" style="color:black;font-weight:bold;margin-top:105px;font-size:12px">'+item.spe+' '+cource+'</div>'+
                        '</div>'+
                        '<a href="http://lp.campuslama.com/programme-detail/mba/branding-and-advertising" class="title-link" style="color: #fff;">'+
                        '</a><figcaption class="transition" style="color: #fff; padding: 45px 61px; */"><a href="#" class="title-link" style="color: #fff;">'+
                            '<span class="mb-" style="color:black;font-weight:bold"></span>'+
                            '</a><p><a href="#" class="title-link" style="color: #fff;"></a><a style="background-color: #0e2a4b;color:white" class="btn" href="<?php echo base_url()?>listing/viewcource/'+item.uc_id+'/'+item.spe+'">View More</a></p>'+
                        '</figcaption>'+
                    '</figure></div>';

          });
          //html+=' </div></div></div></div></div>';
          $('#tab-5').html(html);

        }
      });
    }

       function get_university_city(id){
  $('#tab-10').empty();
//$('#icontab-2').empty();
      $.ajax({
        type:'POST',
        url:'<?php echo base_url()?>explore/get_top_city_view',
        data:{'id':id},
        success:function(res){
          var obj=JSON.parse(res);
          //console.log(obj);
          var html='';
           if (obj.length==0) {
            html+= '<p>No University Found</p>';
            console.log(html);
          }
          $.each(obj,function(i,item){
            console.log(item);
          var cource='';
          if(item.course_spe==null){
            cource='-';
          }else if(item.course_spe!=null){
            cource='-'+item.course_spe;
          }
          
            html+='<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3" style="padding-left:0px;padding-right: 2px;">'+
                  '<figure class="cp-gallery">'+
                    '<span class="hover-ripple right" data-hover-color="#000" data-hover-opacity="0.6"></span>'+
                    '<img src="<?php echo base_url(); ?>uploads/coverimages/'+item.ui_coverimage+'" class="img-responsive" alt="" style="width:210px;height:163px">'+
                    '<figcaption>'+
                      '<span class="text-uppercase"><a href="<?php echo base_url()?>listing/index/'+item.um_name+'" class="white-link link-underline">'+item.um_name+'</a></span>'+

                    '</figcaption>'+
                  '</figure>'+
                '</div>'
          });
          //html+=' </div></div></div></div></div>';
          $('#tab-10').html(html);

          //$('#icontab-2').html(html);
          //$('#icontab-3').html(html);
        }
      });
    }
    </script>
  <script>
  getCource(0);
  get_top_city(1807);
  get_university_city(1807);
            (function($) {
                $(".s-service-carousel-for").slick({
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                    vertical: true,
                    autoplay: true,
                    asNavFor: ".s-service-carousel-nav",
                    focusOnSelect: true,
                    responsive: [
                      {
                        breakpoint: 767,
                        settings: {
                            dots: true,
                            vertical: false
                        }
                      },
                    ]
                });
                $(".s-service-carousel-nav").slick({
                    slidesToShow: 5,
                    slidesToScroll: 1,
                    focusOnSelect: true,
                    asNavFor: ".s-service-carousel-for",
                    vertical: true,
                    autoplay: true,
                    arrows: false,
                    centerMode: true,
                    centerPadding: 0,
                });
            })(jQuery);
        </script>
  <script type="text/javascript">
$(document).ready(function(){
$('#select_education').change(function(){
  //var cate_id = $(this).val();
  var cate_id  = $(this).val().split('-');
  //alert(type);
  console.log(cate_id[0]);
  $('#select_stream').empty();
  $.ajax({
    type: "POST",
    url: "<?php echo base_url('explore/education_category'); ?>",
    data:{cate_id:cate_id[1]},
    success: function(response){
      var res = JSON.parse(response);
      $('#select_stream').append('<option value="">Choose stream</option>');
      $.each(res, function (i, itm) {
        // $('#select_course').append($('<option>', { 
        //  value: itm.id,
        //  text : itm.name 
        // }));
      $('#select_stream').append('<option value="'+itm.id+'-'+itm.name+'">'+itm.name+'</option>');

      });
    }
  });
});

      

$('#select_stream').change(function(){
  var streamid = $(this).val();
  //alert(streamid);
  $('#select_course').empty();
  $.ajax({
    type: "POST",
    url: "<?php echo base_url('explore/stream_program'); ?>",
    data:{streamid:streamid},
    success: function(response){
      var res = JSON.parse(response);
      $('#select_course').append('<option value="">Choose Course</option>');
      $.each(res, function (i, itm) {
        // $('#select_course').append($('<option>', { 
        //  value: itm.id,
        //  text : itm.name 
        // }));
      $('#select_course').append('<option value="'+itm.id+'-'+itm.name+'">'+itm.name+'</option>');

      });
    }
  });
});
      $('.button-collapse').sideNav({edge:'left'});
      $('#div2').hide();
      $('#div3').hide();
      $('#div4').hide();
      $('#div5').hide();
      $('#div6').hide();
      $('#div7').hide();
      $('#div8').hide();
      $('#div9').hide();
      $('#div10').hide();
      $('#div11').hide();
      $('#div12').hide();
      $('#div13').hide();
      $('#div14').hide();
      $('#div15').hide();
      //~ if(jQuery.browser.mobile)
      //~ {
         
         //~ $('.BackgroundImg').addClass('noBackground');
         //~ $('.noBackground').removeClass('BackgroundImg');
         //~ $('.noBackground').css('background-color','#47494b');
      //~ }
      //~ else
      //~ {
         $('.BackgroundImg').css('background-image','url("<?php echo base_url()?>assets/explore/images/frontend/howareyoubg.jpg")');
      //~ }
    });
    
    jQuery("#div1 #div1-content").click(function(){
      jQuery(".template_section").hide();
    });
    
  </script>
  <script>


$(document).ready(function(){
 $('#accordion , .collapse').collapse({
     toggle: false
   });
$("accordion1").closest(".collapse").collapse({
     toggle: false
   });
// $('#accordion1 , .collapse').collapse({
//      toggle: false
//    });

$(function() {
        $('.material-card > .mc-btn-action').click(function () {
            var card = $(this).parent('.material-card');
            var icon = $(this).children('i');
            icon.addClass('fa-spin-fast');

            if (card.hasClass('mc-active')) {
                card.removeClass('mc-active');

                window.setTimeout(function() {
                    icon
                        .removeClass('fa-arrow-left')
                        .removeClass('fa-spin-fast')
                        .addClass('fa-bars');

                }, 800);
            } else {
                card.addClass('mc-active');

                window.setTimeout(function() {
                    icon
                        .removeClass('fa-bars')
                        .removeClass('fa-spin-fast')
                        .addClass('fa-arrow-left');

                }, 800);
            }
        });
    });
});
</script>
  <script type="text/javascript" src="<?php echo base_url('assets/explore/js/frontend/frontend_quiz.js');?>"></script>
  
      <script src="<?php echo base_url('assets_new/js/jquery-2.1.3.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets_new/bootstrap/js/bootstrap.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets_new/materialize/js/materialize.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets_new/js/jquery.easing.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets_new/js/jquery.sticky.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets_new/js/smoothscroll.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets_new/js/imagesloaded.js'); ?>"></script>
        <script src="<?php echo base_url('assets_new/js/jquery.stellar.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets_new/js/jquery.inview.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets_new/js/jquery.shuffle.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets_new/js/menuzord.js'); ?>"></script>
        <script src="<?php echo base_url('assets_new/js/bootstrap-tabcollapse.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets_new/owl.carousel/owl.carousel.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets_new/flexSlider/jquery.flexslider-min.js'); ?>"></script>
        <script src="<?php echo base_url('assets_new/magnific-popup/jquery.magnific-popup.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets_new/js/scripts.js'); ?>"></script>
        
        
    </body>
 </html>
