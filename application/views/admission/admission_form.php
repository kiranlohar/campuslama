<style>
a.active {
    background: #fff;
}
.tab-icon{
	font-size: 28px;
    vertical-align: middle;
}
.admission-form-remove-fields{
	padding-top: 10px;
}
.file-field input[type=file] {
    width: 77%;
}
</style>
           
            <section id="content"> 
			
				
	
	<div class="row"  id="side-menu-three-div">
		<div class="col s12 m12 l12" style="margin-top: -6.8px;">
			<div class="card">			
						  <!-- card Section Start-->
				<div class="row" style="margin-bottom: 0px !important;">
					<div class="col s12 m8 l8 offset-l3 offset-m4">
						<div class="row row-margin-bottom">
							<div class="Space30 col l12 m12 s12"></div>
							<div class="col s12 m12 l12">
							<h4 class="admission-form-heading-margin">Admission Form <span style="font-size: 23px;color:#147eb5;">(<?php echo urldecode($this->uri->segment(3)); ?>)</span></h4>
							</div>
							<div class="col s12 m3 l3"></div>	
						</div>	
					</div>
					<!--Basic Tabs-->
					<div id="basic-tabs" class="section">
						<div class="row" style="margin-bottom: 0px !important;">
						  <div class="col s12 m8 l12">
							<div class="row" style="margin-bottom: 0px !important;">
							  <div class="col s12">
								<ul class="tabs tab-demo z-depth-1">
								<?php foreach($categories as $ck=>$category){ 
									if($ck=0){
										$active = 'active';
									}else{
										$active = '';
									}
								?>
								  <li class="tab col s3"><a class="<?php echo $active; ?>" href="#<?php echo $category->fica_name; ?>"> <?php echo $category->fica_name; ?></a>
								  </li>
								<?php } ?>
								</ul>
							  </div>
							  <div class="col s12">
							  <?php foreach($categories as $category){ ?>
								<div id="<?php echo $category->fica_name; ?>" class="col s12">
									<div class="row col s12 m11 l12 admission-form-one-div" style="margin-top: 10px;">
									<?php foreach($catfields as $catfield){ 
										if($category->fica_id == $catfield->fm_fica_id){?>
										<button class="btn waves-effect waves-light text-lowercase admission-form-add-fields btn-admission-field" id="field-<?php echo $catfield->fm_id;?>" name="action"><?php echo $catfield->fm_label; ?>
											<i class="mdi-content-add right"></i>
										 </button>
									   <?php }
										} ?>
									</div>
								</div>
							  <?php } ?>
							  </div>
							</div>
						  </div>
						</div>
					</div>
					<div class="col s12 m12 l12">
						<form action="<?php echo base_url('admission/admission_form'); ?>" method="post">
							<div class="row">
								<div class="row" id="formfielddiv"></div>
								<input type="hidden" name="prog_name" value="<?php echo urldecode($this->uri->segment(3)); ?>">
								<div class="row">
									<div class="col s1 m1 l1 center-align">
										<label>Form Link</label>
									</div>
									<div class="col s4 m4 l4 center-align">
										<input type="text" name="form_link" value="<?php echo base_url(); ?>">
									</div>
									<div class="col s12 m12 l12 center-align">
										<input type="submit" class="waves-effect waves-light btn btn-form-submit Searchbtn center-align" value="Add This Form">
										<div class="col s12 m12 l12 Space10"></div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>					
		</div>
	</div>
</section>
<script type="text/javascript" src="<?php echo base_url('assets/js/plugins/jquery-1.11.2.min.js');?>"></script>
<script>
var addcount = 0;
$('.admission-form-add-fields').click(function(){
	
	var bttnId = $(this).attr('id');
	
	if ($('.row.extrarow').is(':empty')) { 
		$('.row.extrarow').remove();
	}
	
	bttnId = bttnId.split('-');
	formfId = bttnId[1];
	
	var formHtml = '';
	
	if (addcount % 3 == 2){
	formHtml += '</div><div class="row extrarow">';
	}
	$.ajax({
		type: "POST",
		url: "<?php echo base_url('admission/addFormField'); ?>",
		data:{formfId:formfId},
		success: function(response){
			formHtml += response;
			$('#formfielddiv').append(formHtml);
		}
	});
	addcount++;
	$('#field-'+bttnId[1]).hide();
});

$("#formfielddiv").on('click','.admission-form-remove-fields',function(){
		var rmbttnId = $(this).attr('id');
		if ($('.row.extrarow').is(':empty')) {
			$('.row.extrarow').remove();
		}
		rmbttnId = rmbttnId.split('-');
		$(this).parent().parent().parent().remove();
		$('#field-'+rmbttnId[1]).show();
});
</script>