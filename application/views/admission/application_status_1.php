<style>
.select-dropdown {
    height: initial !important;
}
.select-wrapper input.select-dropdown{
	padding-left:15px !important;
}
.card{
	height:400px;
}
</style>
<section id="content"> 
	<div class="row"  id="side-menu-two-div">
		<div class="col s12 m12 l12" style="margin-top: -6.8px;">
			<div class="card" style="height:1860px">		
				<div class="">			
					<!-- card Section Start-->
					<div id="card-stats" class="seaction">
						<div class="row">
							<!-- <div class="col s12 m8 l8 offset-l3 offset-m4">
								<div class="row row-margin-bottom">
									<div class="Space30 col l12 m12 s12"></div>
									<div class="col s12 m12 l12">
									<h4 class="admission-form-heading-margin">Application Status</h4>
									</div>
									<div class="col s12 m3 l3"></div>	
								</div>										
							</div> -->
						</div>
					</div>
				</div>
				<?php //print_r($appStat); ?>
				<div class="row" >
					<div class="col s12 m12 l12">
						<!-- div table start -->
						<div class="col s12 l0 m10 offset-l1 offset-l1">
							<div class="row at-table-row">
								<div id="basic-tabs" class="section">
            <h4 class="header">Student Details</h4>
            <div class="row">
             
              <div class="col s12 ">

                <div class="row">
                  <div class="col s12">
                    <ul class="tabs tab-demo z-depth-1" style="width: 100%;">
                      <li class="tab col s4"><a  href="#test1">Personal Details</a>
                      </li>
                      <li class="tab col s4"><a href="#test2" class="">Education Details</a>
                      </li>
                      <li class="tab col s4"><a href="#test3" class="">work details</a>
                      </li>
                      <li class="tab col s4"><a class="active" href="#test4" class="">Status</a>
                      </li>
                    <div class="indicator" style="right: 357px; left: 0px;"></div><div class="indicator" style="right: 357px; left: 0px;"></div></ul>
                  </div>
                  <div class="col s12">
                    <div id="test1" class="col s12" style="display: none;">
                      <div class="rows">
                      	<div class="col s12">     	
                      <ul id="profile-page-about-details" class="collection z-depth-1" style="margin-left: -22px;margin-right: -22px;">
                      	<li class="collection-item">
                    <div class="row">
                      <div class="col s4 grey-text darken-1">
                        <?php if(!empty($personalData->spd_profile_pic)){?>
                        <img src="<?php echo base_url()?>uploads/users/<?php echo isset($personalData->spd_profile_pic) ? $personalData->spd_profile_pic : '';?>" style="width: 60px;height: 60px;margin-bottom: -21px;" class="circle responsive-img"></div>
                        <?php } ?>
                      <div class="col s3 grey-text text-darken-4 left-align"></div>
                    </div>
                  </li>
                  <li class="collection-item">
                    <div class="row">
                      <div class="col s4 grey-text darken-1">Full Name</div>
                      <div class="col s3 grey-text text-darken-4 left-align"><?php echo isset($personalData->spd_firstname) ? $personalData->spd_firstname : '';?> <?php echo isset($personalData->spd_middlename) ? $personalData->spd_middlename : '';?> <?php echo isset($personalData->spd_lastname) ? $personalData->spd_lastname : '';?></div>
                    </div>
                  </li>
                  <li class="collection-item">
                    <div class="row">
                      <div class="col s4 grey-text darken-1"> Email</div>
                      <div class="col s3 grey-text text-darken-4 left-align"><?php echo isset($personalData->spd_email) ? $personalData->spd_email : '';?></div>
                    </div>
                  </li>
                  <li class="collection-item">
                    <div class="row">
                      <div class="col s4 grey-text darken-1"> Mobile</div>
                      <div class="col s3grey-text text-darken-4 left-align"><?php echo isset($personalData->spd_mobile) ? $personalData->spd_mobile : '';?></div>
                    </div>
                  </li>
                  <li class="collection-item">
                    <div class="row">
                      <div class="col s4 grey-text darken-1">Birth date</div>
                      <div class="col s3 grey-text text-darken-4 left-align"><?php echo isset($personalData->spd_dob) ? $personalData->spd_dob : '';?></div>
                    </div>
                  </li>
  				<li class="collection-item">
                    <div class="row">
                      <div class="col s4 grey-text darken-1"> Gender</div>
                      <div class="col s3 grey-text text-darken-4 left-align"><?php echo isset($personalData->spd_gender) ? $personalData->spd_gender : '';?></div>
                    </div>
                  </li>
                 
               <!--  </ul></div>
                      		
                <div class="col s6">     
                      <ul id="profile-page-about-details" class="collection z-depth-1" style="margin-right: -22px;"s>
                -->

                  <li class="collection-item">
                    <div class="row">
                      <div class="col s4 grey-text darken-1"> Address</div>
                      <div class="col s3 grey-text text-darken-4 left-align"><?php echo isset($personalData->spd_address) ? $personalData->spd_address : '';?></div>
                    </div>
                  </li>
                   <li class="collection-item">
                    <div class="row">
                      <div class="col s4 grey-text darken-1"> About You</div>
                      <div class="col s3 grey-text text-darken-4 left-align"><?php echo isset($personalData->spd_about_you) ? $personalData->spd_about_you : '';?></div>
                    </div>
                  </li>
                  <li class="collection-item">
                    <div class="row">
                      <div class="col s4 grey-text darken-1">Country</div>
                      <div class="col s3 grey-text text-darken-4 left-align"><option <?php echo empty($personalData)? '' : ($personalData->spd_cm_id == '100') ? 'selected="selected"' : '';?> value="100">India</option></div>
                    </div>
                  </li>
                  <li class="collection-item">
                    <div class="row">
                      <div class="col s4 grey-text darken-1">State</div>
                      <div class="col s3 grey-text text-darken-4 left-align">
                        <?php if(!empty($personalData->spd_sm_id)){?>
                        <?php foreach($statedata as $sdata){ ?>
											 <?php
                       echo ($personalData->spd_sm_id == $sdata->sm_id) ? $sdata->sm_name : ''; ?>
										<?php
                     }
                   }else{
                    echo '-';
                   }
                     ?></div>
                    </div>
                  </li>
                    <li class="collection-item">
                    <div class="row">
                      <div class="col s4 grey-text darken-1"> City</div>
                      <div class="col s3 grey-text text-darken-4 left-align">
                         <?php if(!empty($personalData->spd_sm_id)){?>
                        <?php foreach($citydata as $sdata){ ?>
											 <?php echo ($personalData->spd_sm_id == $sdata->cim_id) ? $sdata->cim_name : ''; ?>
										<?php 
                  }
}
                   ?></div>
                    </div>
                  </li>
                </ul>
            </div>
                 
                      </div>


                    </div>
                    <div class="col s6"></div>
               <div id="test2" class="col s12" style="display: none;">
                   <div class="rows">
                     <div class="col s12">     	
                      <ul id="profile-page-about-details" class="collection z-depth-1" style="margin-left: -22px;margin-right: -22px;">
					                  <li class="collection-item">
					                    <div class="row">
					                      <div class="col s3 "><b> 10TH DETAILS</b></div>
					                      
					                    </div>
					                  </li>
					                  <li class="collection-item">
					                    <div class="row">
					                      <div class="col s4 grey-text darken-1"> Passing Year</div>
					                      <div class="col s3 grey-text text-darken-4 left-align"><?php echo isset($eduData->sed_10th_passout_year) ? $eduData->sed_10th_passout_year : '';?></div>
					                    </div>
					                  </li>
					                  <li class="collection-item">
					                    <div class="row">
					                      <div class="col s4 grey-text darken-1"> Pecentage</div>
					                      <div class="col s3grey-text text-darken-4 left-align"><?php echo isset($eduData->sed_10th_percent) ? $eduData->sed_10th_percent : '';?></div>
					                    </div>
					                  </li>
					                  <li class="collection-item">
					                    <div class="row">
					                      <div class="col s4 grey-text darken-1">School Name</div>
					                      <div class="col s3 grey-text text-darken-4 left-align"><?php echo isset($eduData->sed_10th_school_name) ? $eduData->sed_10th_school_name : '';?></div>
					                    </div>
					                  </li>
								  	<li class="collection-item">
					                    <div class="row">
					                      <div class="col s4 grey-text darken-1"></i> Cetificate</div>
                                <?php if(!empty($eduData->tenth_certi)){?>
					                      <div class="col s3 grey-text text-darken-4 left-align"><img src="<?php echo base_url()?>uploads/certificate/10th/<?php echo isset($eduData->tenth_certi) ? $eduData->tenth_certi : '';?>" style="width: 60px;height: 60px;margin-bottom: 0px;" class=" responsive-img">
                                &nbsp;&nbsp;&nbsp;<a href="<?php echo base_url()?>admission/download_ten/<?php echo  $eduData->tenth_certi ;?>">
                                  <i class="mdi-file-cloud-download" style="font-size:30px"></i></a>&nbsp;&nbsp;&nbsp;<a href="#modal1"  class="modal-trigger" onclick="view('<?php echo base_url()?>uploads/certificate/10th/<?php echo $eduData->tenth_certi;?>')"><i class="mdi-action-open-in-new" style="font-size:27px"></i></a></div>
                                <?php }?>
					                    </div>
					                  </li>
					                 
                </ul>
           	<!--  </div>
                      		
                     <div class="col s6">  -->    
                      	<ul id="profile-page-about-details" class="collection z-depth-1" style="margin-left: -22px;margin-right: -22px;">
               	           <li class="collection-item">
		                    <div class="row">
		                      <div class="col s4 "><b>12TH DETAILS</b></div>
		                      <div class="col s3 grey-text text-darken-4 left-align"></div>
		                    </div>
		                  </li>
		                   <li class="collection-item">
		                    <div class="row">
		                      <div class="col s4 grey-text darken-1">Passing Year</div>
		                      <div class="col s3 grey-text text-darken-4 left-align"><?php echo isset($eduData->sed_12th_passout_year) ? $eduData->sed_12th_passout_year : '';?></div>
		                    </div>
		                  </li>
		                  <li class="collection-item">
		                    <div class="row">
		                      <div class="col s4 grey-text darken-1">Percentage</div>
		                      <div class="col s3 grey-text text-darken-4 left-align"><?php echo isset($eduData->sed_12th_percent) ? $eduData->sed_12th_percent : '';?></div>
		                    </div>
		                  </li>
		                  <li class="collection-item">
		                    <div class="row">
		                      <div class="col s4 grey-text darken-1"> College name</div>
		                      <div class="col s3 grey-text text-darken-4 left-align"><?php echo isset($eduData->sed_12th_college_name) ? $eduData->sed_12th_college_name : '';?></div>
		                    </div>
		                  </li>
		                    <li class="collection-item">
		                    <div class="row">
		                      <div class="col s4 grey-text darken-1"> Cetificate</div>
		                      <?php if(!empty($eduData->twelth_certi)){?>
                                <div class="col s3 grey-text text-darken-4 left-align"><img src="<?php echo base_url()?>uploads/certificate/12th/<?php echo isset($eduData->twelth_certi) ? $eduData->twelth_certi : '';?>" style="width: 60px;height: 60px;margin-bottom: 0px;" class="au-target responsive-img">
                                 &nbsp;&nbsp;&nbsp;<a href="<?php echo base_url()?>admission/download_twel/<?php echo  $eduData->twelth_certi ;?>">
                                   <i class="mdi-file-cloud-download" style="font-size:30px"></i></a>&nbsp;&nbsp;&nbsp;<a href="#modal1"  class="modal-trigger" onclick="view('<?php echo base_url()?>uploads/certificate/12th/<?php echo $eduData->twelth_certi;?>')"><i class="mdi-action-open-in-new" style="font-size:27px"></i></a></div>
                                <?php }?>
		                    </div>
		                  </li>
                </ul>
            </div>
                 
           <!-- </div>
           <div class="rows">
              <div class="col s6">   -->   	
                <ul id="profile-page-about-details" class="collection z-depth-1" style="margin-left: -10px;margin-right: -10px;">
			                  <li class="collection-item">
			                    <div class="row">
			                      <div class="col s4"><b>DIPLOMA DETAILS</b></div>
			                      <div class="col s3 grey-text text-darken-4 left-align"></div>
			                    </div>
			                  </li>
			                  <li class="collection-item">
			                    <div class="row">
			                      <div class="col s4 grey-text darken-1">Passing Year</div>
			                      <div class="col s3 grey-text text-darken-4 left-align"><?php echo isset($eduData->sed_diploma_passout_year) ? $eduData->sed_diploma_passout_year : '-';?></div>
			                    </div>
			                  </li>
			                  <li class="collection-item">
			                    <div class="row">
			                      <div class="col s4 grey-text darken-1">Percentage</div>
			                      <div class="col s3grey-text text-darken-4 left-align"><?php echo isset($eduData->sed_diploma_percent) ? $eduData->sed_diploma_percent : '-';?></div>
			                    </div>
			                  </li>
			                  <li class="collection-item">
			                    <div class="row">
			                      <div class="col s4 grey-text darken-1">College Name</div>
			                      <div class="col s3 grey-text text-darken-4 left-align"><?php echo isset($eduData->sed_diploma_college_name) ? $eduData->sed_diploma_college_name : '-';?></div>
			                    </div>
			                  </li>
			  				<li class="collection-item">
			                    <div class="row">
			                      <div class="col s4 grey-text darken-1">Specialization</div>
			                      <div class="col s3 grey-text text-darken-4 left-align"><?php echo isset($eduData->sed_diploma_specialisation) ? $eduData->sed_diploma_specialisation : '-';?></div>
			                    </div>
			                  </li>
			                  <li class="collection-item">
			                    <div class="row">
			                      <div class="col s4 grey-text darken-1">Certificate</div>
			                         <?php if(!empty($eduData->diploma_certi)){?>
                                <div class="col s3 grey-text text-darken-4 left-align"><img src="<?php echo base_url()?>uploads/certificate/diploma/<?php echo isset($eduData->diploma_certi) ? $eduData->diploma_certi : '';?>" style="width: 60px;height: 60px;margin-bottom: 0px;" class="img-thumbnail responsive-img">
                                 &nbsp;&nbsp;&nbsp;<a href="<?php echo base_url()?>admission/download_dip/<?php echo  $eduData->diploma_certi ;?>">
                                  <i class="mdi-file-cloud-download" style="font-size:30px"></i></a>&nbsp;&nbsp;&nbsp;<a href="#modal1"  class="modal-trigger" onclick="view('<?php echo base_url()?>uploads/certificate/diploma/<?php echo $eduData->diploma_certi;?>')"><i class="mdi-action-open-in-new" style="font-size:27px"></i></a></div>
                                <?php }?>
			                    </div>
			                  </li>
                 
                </ul>
           <!--  </div> -->
                      		
            <!--   <div class="col s6">  -->    
                 <ul id="profile-page-about-details" class="collection z-depth-1" style="margin-left: -10px;margin-right: -10px;">
                    <li class="collection-item">
                    <div class="row">
                      <div class="col s4"><b>GRADUAT DETAILS</b></div>
                      <div class="col s3 grey-text text-darken-4 left-align"></div>
                    </div>
                  </li>
                   <li class="collection-item">
                    <div class="row">
                      <div class="col s4 grey-text darken-1">Passing Year</div>
                      <div class="col s3 grey-text text-darken-4 left-align"><?php echo isset($eduData->sed_graduate_passout_year) ? $eduData->sed_graduate_passout_year : '-';?></div>
                    </div>
                  </li>
                  <li class="collection-item">
                    <div class="row">
                      <div class="col s4 grey-text darken-1">Percentage</div>
                      <div class="col s3 grey-text text-darken-4 left-align"><?php echo isset($eduData->sed_graduate_percent) ? $eduData->sed_graduate_percent : '-';?></div>
                    </div>
                  </li>
                  <li class="collection-item">
                    <div class="row">
                      <div class="col s4 grey-text darken-1"> College Name</div>
                      <div class="col s3 grey-text text-darken-4 left-align"><?php echo isset($eduData->sed_graduate_college_name) ? $eduData->sed_graduate_college_name : '-';?></div>
                    </div>
                  </li>
                    <li class="collection-item">
                    <div class="row">
                      <div class="col s4 grey-text darken-1">University Name</div>
                      <div class="col s3 grey-text text-darken-4 left-align"><?php echo isset($eduData->sed_graduate_university) ? $eduData->sed_graduate_university : '-';?></div>
                    </div>
                  </li>
                  <li class="collection-item">
			                    <div class="row">
			                      <div class="col s4 grey-text darken-1">Specialization</div>
			                      <div class="col s3 grey-text text-darken-4 left-align"><?php echo isset($eduData->sed_graduate_specialisation) ? $eduData->sed_graduate_specialisation : '-';?></div>
			                    </div>
			                  </li>
			                  <li class="collection-item">
			                    <div class="row">
			                      <div class="col s4 grey-text darken-1">Certificate</div>
			                      <?php if(!empty($eduData->univer_certi)){?>
                                <div class="col s3 grey-text text-darken-4 left-align"><img src="<?php echo base_url()?>uploads/certificate/university/<?php echo isset($eduData->univer_certi) ? $eduData->univer_certi : '';?>" style="width: 60px;height: 60px;margin-bottom: 0px;" class=" responsive-img">
                                 &nbsp;&nbsp;&nbsp; <a href="<?php echo base_url()?>admission/download/<?php echo  $eduData->univer_certi ;?>">
                                 <i class="mdi-file-cloud-download" style="font-size:30px"></i></a>&nbsp;&nbsp;&nbsp;<a href="#modal1"  class="modal-trigger" onclick="view('<?php echo base_url()?>uploads/certificate/university/<?php echo $eduData->univer_certi;?>')"><i class="mdi-action-open-in-new" style="font-size:27px"></i></a>
                                </div>
                                <?php }?>
			                    </div>
			                  </li>
                </ul>
            </div>
                 
          </div>
     </div>
                    <div id="test3" class="col s12" style="display: none;">
                    	<div class="rows">
              <div class="col s12">     	
                <ul id="profile-page-about-details" class="collection z-depth-1" style="margin-left: -10px;margin-right: -10px;">
			                  <li class="collection-item">
			                    <div class="row">
			                       <div class="col s4 grey-text darken-1">Experience</div>
			                      <div class="col s3 grey-text text-darken-4 left-align"><?php if(isset($workData->swd_is_work_experience)) echo ($workData->swd_is_work_experience == '1') ? 'YES' : '-'; ?><?php  if(isset($workData->swd_is_work_experience)) echo ($workData->swd_is_work_experience == '0') ? 'NO': '-'; ?></div>
			                    </div>
			                  </li>
			                  <li class="collection-item">
			                    <div class="row">
			                      <div class="col s4 grey-text darken-1">Total Exp</div>
			                      <div class="col s3 grey-text text-darken-4 left-align">  <?php if(!empty($workData->swd_work_experience)){
                $workexperiece = array(); 
                $workexperiece = explode(' ', $workData->swd_work_experience); 

            } ?><?php echo isset($workexperiece[0]) ? $workexperiece[0] : '';?></div>
			                    </div>
			                  </li>
			                  <li class="collection-item">
			                    <div class="row">
			                      <div class="col s4 grey-text darken-1">Previous Company</div>
			                      <div class="col s3grey-text text-darken-4 left-align"><?php echo isset($workData->swd_prev_comp_name) ? $workData->swd_prev_comp_name : '';?></div>
			                    </div>
			                  </li>
			                  <li class="collection-item">
			                    <div class="row">
			                      <div class="col s4 grey-text darken-1">Previous Company Designation</div>
			                      <div class="col s3 grey-text text-darken-4 left-align"><?php echo isset($workData->swd_prev_comp_desi) ? $workData->swd_prev_comp_desi : '';?></div>
			                    </div>
			                  </li>
			  				<li class="collection-item">
			                    <div class="row">
			                      <div class="col s4 grey-text darken-1">Current Company</div>
			                      <div class="col s3 grey-text text-darken-4 left-align"><?php echo isset($workData->swd_curr_comp_name) ? $workData->swd_curr_comp_name : '';?></div>
			                    </div>
			                  </li>
			                  <li class="collection-item">
			                    <div class="row">
			                      <div class="col s4 grey-text darken-1">Current Company Designation</div>
			                      <div class="col s3 grey-text text-darken-4 left-align"><?php echo isset($workData->swd_curr_comp_desi) ? $workData->swd_curr_comp_desi : '';?></div>
			                    </div>
			                  </li>
                 
                </ul>
            </div>
                     </div>
                 </div>
                    </div>
                    <div id="test4" class="col s12" style="display: block;">
                      <ul id="profile-page-about-details" class="collection z-depth-1" style="margin-left: -2px;margin-right: -2px;margin-top: -13px;height:374px">
                        <li class="collection-item">
                          <div class="row">
                            <div class="col s4 grey-text darken-1"><?php echo $compparent?></div>
                            <div class="col s3 grey-text text-darken-4 left-align"><?php echo $spe?></div>
                          </div>
                        </li>
                        <li class="collection-item">
                        
                             <div class="col s4 left-align"><b>Application Status</b></div>
                            <div class="col s3 ">
                              <select class="com_spec" id="app_status">
                    <option value="" disabled selected>Change Status</option>
                    <option <?php echo ($appStat->as_status ==1) ? 'selected' : ''; ?> value="1">New Application</option>
                    <option <?php echo ($appStat->as_status ==3) ? 'selected' : ''; ?> value="3">Under Review</option>
                    <option <?php echo ($appStat->as_status ==4) ? 'selected' : ''; ?> value="4">In Process</option>
                    <option <?php echo ($appStat->as_status ==5) ? 'selected' : ''; ?> value="5">Application Approved</option>
                    <option <?php echo ($appStat->as_status ==6) ? 'selected' : ''; ?> value="6">Fees Paid</option>
                    <option <?php echo ($appStat->as_status ==7) ? 'selected' : ''; ?> value="7">Admission Done</option>
                    <option <?php echo ($appStat->as_status ==8) ? 'selected' : ''; ?> value="8">Rejected</option>
                  </select>
                         
                        </div>
                        <div class="col s4 ">
                         <!--  <i class="btn submit-button waves-effect waves-light  waves-input-wrapper" style="margin-top: 0px;height: 44px;">
                            <input class="waves-button-input save_status" id="" type="button" value="save" style="margin-top:10px">
                          </i> -->
                          <a class="btn waves-effect waves-light green save_status">Save</a>
                          <a href="<?php echo base_url('admission/tracker/'.$compparent); ?>" class="btn waves-effect waves-light red">Cancel</a>
                          <a href="<?php echo base_url('admission/tracker/'.$compparent); ?>" class="btn waves-effect waves-light blue">close</a>
                        </div>
                        </li>
                      </ul>
                   
               
                
              </div>
                    </div>
                  </div>
                </div>

              </div>
            </div>
          </div>
							</div>
						</div>
						<!-- div table End -->
					</div>
				</div>
			</div>	
		</div>	
	</div>
  <!--  <div id="modal1" class="modal" style="overflow:hidden"> -->
  
     <div id="modal1" class="modal" >
<a class="btn-floating waves-effect waves-red modal-action modal-close" style="float: right;margin-top: 14px;background-color: #ff575a !important;"><i class="mdi-content-clear"></i></a>
      
     <div class="modal-footer">
                    
                    
                   <!--  <a href="#" class="waves-effect waves-green btn-flat modal-action modal-close">Agree</a> -->
                  </div>
                  <div class="modal-content img" >
                 
                  </div>
                  <div class="modal-footer">
                    <!-- <a href="#" class="waves-effect waves-red btn-flat modal-action modal-close">Close</a> -->
                   <!--  <a href="#" class="waves-effect waves-green btn-flat modal-action modal-close">Agree</a> -->
                  </div>
  </div>
</section>	
<!-- END MAIN -->
<script type="text/javascript" src="<?php echo base_url('assets/js/plugins/jquery-1.11.2.min.js');?>"></script>
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/1.3.1/js/toastr.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('#modal1').hide();
	$('.save_status').click(function(){
		
		var appstatus = $('#app_status').val();
    // console.log(appstatus);
    // return false;
		var as_id = <?php echo $this->uri->segment(4); ?>;
		
		$.ajax({
			type: "POST",
			url: "<?php echo base_url('admission/updateAppStatus'); ?>",
			data:{appstatus:appstatus,as_id:as_id},
			success: function(response){
				
				if(response == 'success'){
         // toastr.success('Updated success')
				window.location.reload();
			//		window.location.href = "<?php echo base_url('admission/tracker/'.$compparent); ?>";
					
				}else{
					
				//	window.location.href = "<?php echo base_url('admission/application_status'); ?>";
				}
				
			}
		});
	});
});

function view(data)
{

  $('.img').html('<img src="'+data+'" style="width:100%"/>');
  $('#test').val(data);
  $('#modal1').show();
}
</script>