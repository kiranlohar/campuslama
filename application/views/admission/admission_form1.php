
           
            <section id="content"> 
			
				
	
	<div class="row"  id="side-menu-three-div">
		<div class="col s12 m12 l12" style="margin-top: -6.8px;">
					<div class="card">			
								  <!-- card Section Start-->
						<div class="row">
							<div class="col s12 m8 l8 offset-l3 offset-m4">
								<div class="row row-margin-bottom">
									<div class="Space30 col l12 m12 s12"></div>
									<div class="col s12 m12 l12">
									<h4 class="admission-form-heading-margin">Admission Form-MBA</h4>
									</div>
									<div class="col s12 m3 l3"></div>	
								</div>	
							</div>
							<div class="col s12 mobile-magin-progress-bar hide-on-large-only visible-on-med-and-down"></div>	
							<div class="col s12 m6 l2 offset-l1 tab-padding cust-cursor">
								<div class="card">
									<div class="card-content  university-profile-card white-text"  id="click-admission-form-card-1">
										<p class="card-stats-title">
										<i class="icon icon-admission-form-black-icon-01"></i> &nbsp;&nbsp;Personal</p>
									</div>
								</div>
							</div>	
							<div class="col s12 m6 l2 tab-padding cust-cursor">
								<div class="card">
									<div class="card-content university-profile-card white-text"  id="click-admission-form-card-2">
										<p class="card-stats-title">
										<i class="icon icon-admission-form-black-icon-02"></i>
										&nbsp;&nbsp;Educational</p>
									</div>
								</div>
							</div>                            
							<div class="col s12 m6 l2 tab-padding cust-cursor">
								<div class="card">
									<div class="card-content university-profile-card white-text"  id="click-admission-form-card-3">
										<p class="card-stats-title">
										<i class="icon icon-admission-form-black-icon-02"></i>
										&nbsp;&nbsp;Educational</p>	
									</div>
								</div>
							</div>
							<div class="col s12 m6 l2 tab-padding cust-cursor">
								<div class="card">
									<div class="card-content university-profile-card white-text" id="click-admission-form-card-4">
										<p class="card-stats-title">
										<i class="icon icon-admission-form-black-icon-03"></i>
										&nbsp;&nbsp;Work</p>	
									</div>
								</div>
							</div>
							<div class="col s12 m6 l2 tab-padding cust-cursor">
								<div class="card">
									<div class="card-content university-profile-card white-text" id="click-admission-form-card-5">
										<p class="card-stats-title">
										<i class="icon icon-admission-form-black-icon-03"></i>
										&nbsp;&nbsp;Biz</p>	
									</div>
								</div>
							</div>
						</div>
						<div class="row col s12 m11 l11 admission-form-one-div  offset-l1">
							<div class="col s12 l12 m12">
								<button class="btn waves-effect waves-light text-lowercase admission-form-add-fields btn-admission-field" type="submit" name="action">First Name
									<i class="mdi-content-add right"></i>
								 </button>
								 <button class="btn waves-effect waves-light text-lowercase admission-form-add-fields btn-admission-field" type="submit" name="action">Middle Name
									<i class="mdi-content-add right"></i>
								 </button>
								 <button class="btn waves-effect waves-light text-lowercase admission-form-add-fields btn-admission-field" type="submit" name="action">Last Name
									<i class="mdi-content-add right"></i>
								 </button>
								 <button class="btn waves-effect waves-light text-lowercase admission-form-add-fields btn-admission-field" type="submit" name="action">Birth Date
									<i class="mdi-content-add right"></i>
								 </button>
								 <button class="btn waves-effect waves-light text-lowercase admission-form-add-fields btn-admission-field" type="submit" name="action">Gender
									<i class="mdi-content-add right"></i>
								 </button>
								 <button class="btn waves-effect waves-light text-lowercase admission-form-add-fields btn-admission-field" type="submit" name="action">Country
									<i class="mdi-content-add right"></i>
								 </button>
								 <button class="btn waves-effect waves-light text-lowercase admission-form-add-fields btn-admission-field" type="submit" name="action">State
									<i class="mdi-content-add right"></i>
								 </button>
								 <button class="btn waves-effect waves-light text-lowercase admission-form-add-fields btn-admission-field" type="submit" name="action">City
									<i class="mdi-content-add right"></i>
								 </button>
								 <button class="btn waves-effect waves-light text-lowercase admission-form-add-fields btn-admission-field" type="submit" name="action">Mobile
									<i class="mdi-content-add right"></i>
								 </button> 
								<button class="btn waves-effect waves-light text-lowercase admission-form-add-fields btn-admission-field" type="submit" name="action">Email
									<i class="mdi-content-add right"></i>
								 </button>
						   </div>
							<div class="col s12 l12 m12 Space10"></div>
						   <div class="col s12 l12 m12">
								 <button class="btn waves-effect waves-light text-lowercase admission-form-add-fields btn-admission-field" type="submit" name="action">Pincode
									<i class="mdi-content-add right"></i>
								 </button>
								 <button class="btn waves-effect waves-light text-lowercase admission-form-add-fields btn-admission-field" type="submit" name="action">Street
									<i class="mdi-content-add right"></i>
								 </button>
								 <button class="btn waves-effect waves-light text-lowercase admission-form-add-fields btn-admission-field" type="submit" name="action">Landmark
									<i class="mdi-content-add right"></i>
								 </button>
								 <button class="btn waves-effect waves-light text-lowercase admission-form-add-fields btn-admission-field" type="submit" name="action">Landline
									<i class="mdi-content-add right"></i>
								 </button>
												 
							</div>
						</div>
						<div class="row col s12 m11 l11 admission-form-two-div offset-l1">
							<div class="col s12 l12 m12">
								<button class="btn waves-effect waves-light text-lowercase admission-form-add-fields btn-admission-field" type="submit" name="action">First Name
									<i class="mdi-content-add right"></i>
								 </button>
								 <button class="btn waves-effect waves-light text-lowercase admission-form-add-fields btn-admission-field" type="submit" name="action">Middle Name
									<i class="mdi-content-add right"></i>
								 </button>
								 <button class="btn waves-effect waves-light text-lowercase admission-form-add-fields btn-admission-field" type="submit" name="action">Last Name
									<i class="mdi-content-add right"></i>
								 </button>
								 <button class="btn waves-effect waves-light text-lowercase admission-form-add-fields btn-admission-field" type="submit" name="action">Birth Date
									<i class="mdi-content-add right"></i>
								 </button>
								 <button class="btn waves-effect waves-light text-lowercase admission-form-add-fields btn-admission-field" type="submit" name="action">Gender
									<i class="mdi-content-add right"></i>
								 </button>
								 <button class="btn waves-effect waves-light text-lowercase admission-form-add-fields btn-admission-field" type="submit" name="action">Country
									<i class="mdi-content-add right"></i>
								 </button>
								 <button class="btn waves-effect waves-light text-lowercase admission-form-add-fields btn-admission-field" type="submit" name="action">State
									<i class="mdi-content-add right"></i>
								 </button>
								 <button class="btn waves-effect waves-light text-lowercase admission-form-add-fields btn-admission-field" type="submit" name="action">City
									<i class="mdi-content-add right"></i>
								 </button>
								 <button class="btn waves-effect waves-light text-lowercase admission-form-add-fields btn-admission-field" type="submit" name="action">Mobile
									<i class="mdi-content-add right"></i>
								 </button> 
								<button class="btn waves-effect waves-light text-lowercase admission-form-add-fields btn-admission-field" type="submit" name="action">Email
									<i class="mdi-content-add right"></i>
								 </button>
						   </div>
						</div>
						<div class="row col s12 m11 l11 admission-form-three-div offset-l1">
							<div class="col s12 l12 m12">
								<button class="btn waves-effect waves-light text-lowercase admission-form-add-fields btn-admission-field" type="submit" name="action">First Name
									<i class="mdi-content-add right"></i>
								 </button>
								 <button class="btn waves-effect waves-light text-lowercase admission-form-add-fields btn-admission-field" type="submit" name="action">Middle Name
									<i class="mdi-content-add right"></i>
								 </button>
								 <button class="btn waves-effect waves-light text-lowercase admission-form-add-fields btn-admission-field" type="submit" name="action">Last Name
									<i class="mdi-content-add right"></i>
								 </button> 
						   </div>
						</div>
						<div class="row col s12 m11 l11 admission-form-four-div offset-l1">
							<div class="col s12 l12 m12">
								<button class="btn waves-effect waves-light text-lowercase admission-form-add-fields btn-admission-field" type="submit" name="action">First Name
									<i class="mdi-content-add right"></i>
								 </button>
								 <button class="btn waves-effect waves-light text-lowercase admission-form-add-fields btn-admission-field" type="submit" name="action">Middle Name
									<i class="mdi-content-add right"></i>
								 </button>
						   </div>
						</div>
						<div class="row col s12 m11 l11 admission-form-five-div offset-l1">
							<div class="col s12 l12 m12">
								<button class="btn waves-effect waves-light text-lowercase admission-form-add-fields btn-admission-field" type="submit" name="action">First Name
									<i class="mdi-content-add right"></i>
								 </button>
						   </div>
						</div>
					</div>

					<!--Desktop Admission Form Srart -->		
					<div class="card" >
						<div class="">
							<div class="col s12 l12 m12 Space40"></div>
							<div class="col s12 m12 l12">
								<div class="row">
									<div class="col s12 m4 l4">
										<div class="row">
											<div class="col s12 m4 l4">
												<label class="NameLable">First Name</label>
											</div>
											<div class="col s10 m6 l6 no-padding-origin">
												<input type="text" name="firstname">
											</div>
											<div class="col s2 m2 l2">
											<img 
												src="<?php echo base_url('assets/img/cross-icon-1.png'); ?>" onmouseover="this.src='<?php echo base_url('assets/img/cross-icon-2.png'); ?>'" onmouseout="this.src='<?php echo base_url('assets/img/cross-icon-1.png'); ?>'"
												/>
											</div>
										</div>
									</div>
									<div class="col s12 m4 l4">
										<div class="row">
											<div class="col s12 m4 l4">
												<label class="NameLable">Middle Name</label>
											</div>
											<div class="col s10 m6 l6 no-padding-origin">
												<input type="text" name="middlename" >
											</div>
											<div class="col s2 m2 l2">
											<img 
												src="<?php echo base_url('assets/img/cross-icon-1.png'); ?>" onmouseover="this.src='<?php echo base_url('assets/img/cross-icon-2.png'); ?>'" onmouseout="this.src='<?php echo base_url('assets/img/cross-icon-1.png'); ?>'"
												/>
											</div>
										</div>
									</div>
									<div class="col s12 m4 l4">
										<div class="row">
											<div class="col s12 m4 l4">
												<label class="NameLable">Last Name</label>
											</div>
											<div class="col s10 m6 l6 no-padding-origin">
												<input type="text" name="Lastname" >
											</div>
											<div class="col s2 m2 l2">
											<img 
												src="<?php echo base_url('assets/img/cross-icon-1.png'); ?>" onmouseover="this.src='<?php echo base_url('assets/img/cross-icon-2.png'); ?>'" onmouseout="this.src='<?php echo base_url('assets/img/cross-icon-1.png'); ?>'"
												/>
											</div>
										</div>
									</div>
									<div class="col s12 m4 l4">
										<div class="row">
											<div class="col s12 m4 l4">
												<label class="NameLable">Birth Date</label>
											</div>
											<div class="col s10 m6 l6 no-padding-origin">
												<input type="text" name="birthdate" placeholder="--/--/----">
											</div>
											<div class="col s2 m2 l2">
											<img 
												src="<?php echo base_url('assets/img/cross-icon-1.png'); ?>" onmouseover="this.src='<?php echo base_url('assets/img/cross-icon-2.png'); ?>'" onmouseout="this.src='<?php echo base_url('assets/img/cross-icon-1.png'); ?>'"
												/>
											</div>
										</div>
									</div>
									<div class="col s12 m4 l4">
										<div class="row">
											<div class="col s12 m4 l4">
												<label class="NameLable">Gender</label>
											</div>
											<div class="col s10 m6 l6 no-padding-origin">
												  <input name="group1" type="radio" id="test1" />
												  <label for="test1">Male</label>
												  <input name="group1" type="radio" id="test2" />
												  <label for="test2">Female</label>
												  <!--<input name="group1" type="radio" id="test3" />
												  <label for="test3">Trans</label>-->
											</div>
											<div class="col s2 m2 l2">
											<img 
												src="<?php echo base_url('assets/img/cross-icon-1.png'); ?>" onmouseover="this.src='<?php echo base_url('assets/img/cross-icon-2.png'); ?>'" onmouseout="this.src='<?php echo base_url('assets/img/cross-icon-1.png'); ?>'"
												/>
											</div>
										</div>
									</div>
									<div class="col s12 m4 l4">
										<div class="row">
											<div class="col s12 m4 l4" >
												<label class="NameLable">Country</label>
											</div>
											<div class="col s10 m6 l6 no-padding-origin">
												<input type="text" name="country">
											</div>
											<div class="col s2 m2 l2">
											<img 
												src="<?php echo base_url('assets/img/cross-icon-1.png'); ?>" onmouseover="this.src='<?php echo base_url('assets/img/cross-icon-2.png'); ?>'" onmouseout="this.src='<?php echo base_url('assets/img/cross-icon-1.png'); ?>'"
												/>
											</div>
										</div>
									</div>
									<div class="col s12 m4 l4">
										<div class="row">
											<div class="col s12 m4 l4">
												<label class="NameLable">Country</label>
											</div>
											<div class="col s10 m6 l6 no-padding-origin">
												<input type="text" name="country">
											</div>
											<div class="col s2 m2 l2">
											<img 
												src="<?php echo base_url('assets/img/cross-icon-1.png'); ?>" onmouseover="this.src='<?php echo base_url('assets/img/cross-icon-2.png'); ?>'" onmouseout="this.src='<?php echo base_url('assets/img/cross-icon-1.png'); ?>'"
												/>
											</div>
										</div>
									</div>
									<div class="col s12 m4 l4">
										<div class="row">
											<div class="col s12 m4 l4">
												<label class="NameLable">State</label>
											</div>
											<div class="col s10 m6 l6 no-padding-origin">
												<input type="text" name="state">
											</div>
											<div class="col s2 m2 l2">
											<img 
												src="<?php echo base_url('assets/img/cross-icon-1.png'); ?>" onmouseover="this.src='<?php echo base_url('assets/img/cross-icon-2.png'); ?>'" onmouseout="this.src='<?php echo base_url('assets/img/cross-icon-1.png'); ?>'"
												/>
											</div>
										</div>
									</div>
									<div class="col s12 m4 l4">
										<div class="row">
											<div class="col s12 m4 l4">
												<label class="NameLable">City</label>
											</div>
											<div class="col s10 m6 l6 no-padding-origin">
												<input type="text" name="city">
											</div>
											<div class="col s2 m2 l2">
											<img 
												src="<?php echo base_url('assets/img/cross-icon-1.png'); ?>" onmouseover="this.src='<?php echo base_url('assets/img/cross-icon-2.png'); ?>'" onmouseout="this.src='<?php echo base_url('assets/img/cross-icon-1.png'); ?>'"
												/>
											</div>
										</div>
									</div>
									<div class="col s12 m4 l4">
										<div class="row">
											<div class="col s12 m4 l4">
												<label class="NameLable">Pincode</label>
											</div>
											<div class="col s10 m6 l6 no-padding-origin">
												<input type="text" name="pincode">
											</div>
											<div class="col s2 m2 l2">
											<img 
												src="<?php echo base_url('assets/img/cross-icon-1.png'); ?>" onmouseover="this.src='<?php echo base_url('assets/img/cross-icon-2.png'); ?>'" onmouseout="this.src='<?php echo base_url('assets/img/cross-icon-1.png'); ?>'"
												/>
											</div>
										</div>
									</div>
									<div class="col s12 m4 l4">
										<div class="row">
											<div class="col s12 m4 l4">
												<label class="NameLable">street Address</label>
											</div>
											<div class="col s10 m6 l6 no-padding-origin">
												<input type="text" name="address">
											</div>
											<div class="col s2 m2 l2">
											<img 
												src="<?php echo base_url('assets/img/cross-icon-1.png'); ?>" onmouseover="this.src='<?php echo base_url('assets/img/cross-icon-2.png'); ?>'" onmouseout="this.src='<?php echo base_url('assets/img/cross-icon-1.png'); ?>'"
												/>
											</div>
										</div>
									</div>
									<div class="col s12 m4 l4">
										<div class="row">
											<div class="col s12 m4 l4">
												<label class="NameLable">Landmark</label>
											</div>
											<div class="col s10 m6 l6 no-padding-origin">
												<input type="text" name="landmark">
											</div>
											<div class="col s2 m2 l2">
											<img 
												src="<?php echo base_url('assets/img/cross-icon-1.png'); ?>" onmouseover="this.src='<?php echo base_url('assets/img/cross-icon-2.png'); ?>'" onmouseout="this.src='<?php echo base_url('assets/img/cross-icon-1.png'); ?>'"
												/>
											</div>
										</div>
									</div>
									<div class="col s12 m4 l4">
										<div class="row">
											<div class="col s12 m4 l4">
												<label class="NameLable">Landline</label>
											</div>
											<div class="col s10 m6 l6 no-padding-origin">
												<input type="text" name="landline">
											</div>
											<div class="col s2 m2 l2">
											<img 
												src="<?php echo base_url('assets/img/cross-icon-1.png'); ?>" onmouseover="this.src='<?php echo base_url('assets/img/cross-icon-2.png'); ?>'" onmouseout="this.src='<?php echo base_url('assets/img/cross-icon-1.png'); ?>'"
												/>
											</div>
										</div>
									</div>
									<div class="col s12 m4 l4">
										<div class="row">
											<div class="col s12 m4 l4">
												<label class="NameLable">Mobile 1</label>
											</div>
											<div class="col s10 m6 l6 no-padding-origin">
												<input type="text" name="mobile1">
											</div>
											<div class="col s2 m2 l2">
											<img 
												src="<?php echo base_url('assets/img/cross-icon-1.png'); ?>" onmouseover="this.src='<?php echo base_url('assets/img/cross-icon-2.png'); ?>'" onmouseout="this.src='<?php echo base_url('assets/img/cross-icon-1.png'); ?>'"
												/>
											</div>
										</div>
									</div>
									<div class="col s12 m4 l4">
										<div class="row">
											<div class="col s12 m4 l4">
												<label class="NameLable">mobile 2</label>
											</div>
											<div class="col s10 m6 l6 no-padding-origin">
												<input type="text" name="mobile2">
											</div>
											<div class="col s2 m2 l2">
											<img 
												src="<?php echo base_url('assets/img/cross-icon-1.png'); ?>" onmouseover="this.src='<?php echo base_url('assets/img/cross-icon-2.png'); ?>'" onmouseout="this.src='<?php echo base_url('assets/img/cross-icon-1.png'); ?>'"
												/>
											</div>
										</div>
									</div>
									<div class="col s12 m4 l4">
										<div class="row">
											<div class="col s12 m4 l4">
												<label class="NameLable">Email 1</label>
											</div>
											<div class="col s10 m6 l6 no-padding-origin">
												<input type="text" name="email1">
											</div>
											<div class="col s2 m2 l2">
											<img 
												src="<?php echo base_url('assets/img/cross-icon-1.png'); ?>" onmouseover="this.src='<?php echo base_url('assets/img/cross-icon-2.png'); ?>'" onmouseout="this.src='<?php echo base_url('assets/img/cross-icon-1.png'); ?>'"
												/>
											</div>
										</div>
									</div>
									<div class="col s12 m4 l4">
										<div class="row">
											<div class="col s12 m4 l4">
												<label class="NameLable">Email 2</label>
											</div>
											<div class="col s10 m6 l6 no-padding-origin">
												<input type="text" name="email2">
											</div>
											<div class="col s2 m2 l2">
											<img 
												src="<?php echo base_url('assets/img/cross-icon-1.png'); ?>" onmouseover="this.src='<?php echo base_url('assets/img/cross-icon-2.png'); ?>'" onmouseout="this.src='<?php echo base_url('assets/img/cross-icon-1.png'); ?>'"
												/>
											</div>
										</div>
									</div>
									<div class="col s12 m4 l4">
										<div class="row">
											<div class="col s12 m4 l4">
												<label class="NameLable">Checkbox</label>
											</div>
											<div class="col s10 m6 l6 no-padding-origin">
												 <input type="checkbox" id="test5" />
												  <label for="test5">Red</label>
												   <input type="checkbox" id="test6" />
												  <label for="test6">Blue</label>
											</div>
											<div class="col s2 m2 l2">
											<img 
												src="<?php echo base_url('assets/img/cross-icon-1.png'); ?>" onmouseover="this.src='<?php echo base_url('assets/img/cross-icon-2.png'); ?>'" onmouseout="this.src='<?php echo base_url('assets/img/cross-icon-1.png'); ?>'"
												/>
											</div>
										</div>
									</div>
									<div class="col s12 m4 l4">
										<div class="row">
											<div class="col s12 m4 l4">
												<label class="NameLable">Dropdown</label>
											</div>
											<div class="col s10 m6 l6 no-padding-origin">
												<select class="browser-default Ustory">
															<option value="" disabled selected>Choose your option</option>
															<option value="1">Option 1</option>
															<option value="2">Option 2</option>
															<option value="3">Option 3</option>
														  </select>
											</div>
											<div class="col s2 m2 l2">
											<img 
												src="<?php echo base_url('assets/img/cross-icon-1.png'); ?>" onmouseover="this.src='<?php echo base_url('assets/img/cross-icon-2.png'); ?>'" onmouseout="this.src='<?php echo base_url('assets/img/cross-icon-1.png'); ?>'"
												/>
											</div>
										</div>
									</div>
									<div class="col s12 m4 l4">
										<div class="row">
											<div class="col s12 m4 l4">
												<label class="NameLable">Text Area</label>
											</div>
											<div class="col s10 m6 l6 no-padding-origin">
											<textarea row="1"></textarea>
											<label for="textarea1">Textarea</label>
											</div>
											<div class="col s2 m2 l2">
											<img 
												src="<?php echo base_url('assets/img/cross-icon-1.png'); ?>" onmouseover="this.src='<?php echo base_url('assets/img/cross-icon-2.png'); ?>'" onmouseout="this.src='<?php echo base_url('assets/img/cross-icon-1.png'); ?>'"
												/>
											</div>
										</div>
									</div>
									<div class="col s12 m4 l4 ">
										<div class="row file-field input-field">
											<div class="col s12 m4 l4">
											<div class="btn upload-btn">
												<span>File</span>
												<input type="file">
											</div>
											</div>
											<div class="col s10 m5 l5 no-padding-origin">	
											<input class="file-path validate" type="text"/>
											</div>
											<div class="col s2 m2 l2">
											<img 
												src="<?php echo base_url('assets/img/cross-icon-1.png'); ?>" onmouseover="this.src='<?php echo base_url('assets/img/cross-icon-2.png'); ?>'" onmouseout="this.src='<?php echo base_url('assets/img/cross-icon-1.png'); ?>'"
												/>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col s12 m12 l12 center-align">
											<a class="waves-effect waves-light btn btn-form-submit Searchbtn center-align">Add This Form</a>
											<div class="col s12 m12 l12 Space10"></div>
										</div>
									  </div>
								</div>
							</div>
						</div>
					</div>
				<!--Desktop Admission Form End -->			
		</div>
	</div>
</section>