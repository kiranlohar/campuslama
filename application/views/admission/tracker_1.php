
<?php //$userdata = $this->session->userdata('logged_in'); print_r($userdata);?>
<style>
.select-dropdown {
    height: initial !important;
}
.select-wrapper input.select-dropdown{
	padding-left:15px !important;
}
</style>

<section id="content"> 

	<div class="row"  id="side-menu-two-div">
		<div class="col s12 m12 l12" style="margin-top: -6.8px;">
				<div class="card">		
					<div class="">			
									  <!-- card Section Start-->
						<div id="card-stats" class="seaction">
							<div class="row">
								<div class="col s12 m8 l8 offset-l3 offset-m4">
									<div class="row row-margin-bottom">
										<div class="Space30 col l12 m12 s12"></div>
										<div class="col s12 m12 l12">
										<h4 class="admission-form-heading-margin">Application Tracker</h4>
										</div>
										<div class="col s12 m3 l3"></div>	
									</div>
									<div class="row row-margin-bottom">
										<div class="Space30 col l12 m12 s12"></div>
										<div class="col s12 m12 l12 ">
											<select class="com_spec form-control" >
												<option disabled selected> <a class="btn" >Select all</a></option>
												<option value="all" selected >All</option>


											<?php foreach($courses as $course){ ?>
												<option value="<?php echo $course->id; ?>"><?php echo $course->name; ?></option>
											<?php } ?>
											</select>
											<!--  <a class="btn" onClick="javascript:selectAll()">Select all</a>
  												<a class="btn" onClick="javascript:selectNone()">Select none</a> -->

										</div>
										<div class="col s12 m3 l3"></div>	
									</div>										
								</div>
							</div>
						</div>
					</div>
				<div class="row" >
					<div class="col s6 m3 l2 tab-padding cust-cursor">
						<div class="card">
							<div class="card-content admission-tracker-card white-text admission-tracker" >
								<p class="card-stats-title center-align at-text-color" style="font-size:30px;"><?php echo urldecode($this->uri->segment(3)); ?></p>
								<div class="col s12 m12 l12" style="height:20px;"></div>
							</div>
						</div>
					</div>
					<?php $introCount = count($dafault_app); ?>
					<div class="col s6 m3 l2 tab-padding cust-cursor">
						<div class="card" onclick="getStatus(0)">
							<div class="card-content admission-tracker-no-card white-text">
								<p class="card-stats-title center-align at-cards at-no-weight at-text-color" id="introCount"><?php echo $introCount; ?>
								</p>
								<div class="col s12 m12 l12" style="height:22px;"></div>
								<div class="center-align at-text-color">Introduction
								</div>
								
							</div>
						</div>
					</div>
				<?php 
				$naCount = 0;
				$ipCount =0;
				$rejCount =0;
				$apprCount =0;
				$fpCount =0;
				$adpCount =0;
				$adoneCount =0;
				
				foreach($dafault_app as $daf_app){ 
						if($daf_app->as_status == 4){
							$ipCount++;	
						}else if($daf_app->as_status == 1){
							$naCount++;
						}else if($daf_app->as_status == 8){
							$rejCount++;	
						}else if($daf_app->as_status == 5){
							$apprCount++;					
						}else if($daf_app->as_status == 6){
							$fpCount++;
						}else if($daf_app->as_status == 3){
							$adpCount++;
						}else if($daf_app->as_status == 7){
							$adoneCount++;
						}
					}
					?>
					<div class="col s6 m3 l1 tab-padding cust-cursor newapp">
						<div class="card"  onclick="getStatus(1)">
							<div class="card-content admission-tracker-no-card white-text " id="newapp">
								<p class="card-stats-title center-align at-cards at-no-weight at-digit-red" id="naCount"><?php echo $naCount; ?>
								</p>
								<!-- <input type="hidden" id="napp" value="<?php //echo $daf_app->as_status;?>"> -->
								<div class="col s12 m12 l12 space22 hide-on-med-and-up"></div>
								<div class="center-align">New Application</div>
							</div>
						</div>
					</div>
					<div class="col s6 m3 l1 tab-padding cust-cursor ur">
						<div class="card" onclick="getStatus(3)">
							<div class="card-content admission-tracker-no-card white-text">
								<p class="card-stats-title center-align at-cards at-no-weight at-digit-red" id="adpCount"><?php echo $adpCount; ?>
								</p>
								
								<div class="col s12 m12 l12 space22"></div>
								<div class="center-align">Under Review</div>
							</div>
						</div>
					</div>
					<div class="col s6 m3 l1 tab-padding cust-cursor">
						<div class="card" onclick="getStatus(4)">
							<div class="card-content admission-tracker-no-card white-text">
								<p class="card-stats-title center-align at-cards at-no-weight at-digit-red" id="ipCount"><?php echo $ipCount; ?>
								</p>
								<div class="col s12 m12 l12 space22"></div>
								<div class="center-align">In Process</div>
							</div>
						</div>
					</div>
					<div class="col s6 m3 l1 tab-padding cust-cursor">
						<div class="card" onclick="getStatus(8)">
							<div class="card-content admission-tracker-no-card white-text">
								<p class="card-stats-title center-align at-cards at-no-weight at-digit-red" id="rejCount"><?php echo $rejCount; ?>
								</p>
								<div class="col s12 m12 l12 space22"></div>
								<div class="center-align">Rejected</div>
							</div>
						</div>
					</div>
					<div class="col s6 m3 l1 tab-padding cust-cursor">
						<div class="card" onclick="getStatus(5)">
							<div class="card-content admission-tracker-no-card white-text">
								<p class="card-stats-title center-align at-cards at-no-weight at-digit-red" id="apprCount"><?php echo $apprCount; ?>
								</p>
								<div class="col s12 m12 l12 space22"></div>
								<div class="center-align">Approved</div>
							</div>
						</div>
					</div>
					<div class="col s6 m3 l1 tab-padding cust-cursor">
						<div class="card" onclick="getStatus(6)">
							<div class="card-content admission-tracker-no-card white-text">
								<p class="card-stats-title center-align at-cards at-no-weight at-digit-red" id="fpCount"><?php echo $fpCount; ?>
								</p>
								<div class="col s12 m12 l12 space22"></div>
								<div class="center-align">Fees Paid</div>
							</div>
						</div>
					</div>
					<div class="col s12 m3 l2 tab-padding cust-cursor">
						<div class="card" onclick="getStatus(7)">
							<div class="card-content admission-tracker-card white-text center-align">
								<p class="card-stats-title center-align at-no-weight at-digit-red" id="adoneCount"><?php echo $adoneCount; ?>
								</p>
								<div class="center-align space24" >Admission Done</div>
							</div>
						</div>
					</div>
						
					<!--div class="col s12 m12 l12" id='pstatus'>
						<div class="col s12 l0 m10 offset-l1 offset-l1" id="" style="width:98%">
					<div class="row at-table-row">
								<div class="col s12 m3 l0  at-table-align">
									
								</div>
								<div class="col s12 m2 l0  at-table-align">
									
								</div>
								<div class="col s12 m12 l4" >
									<select class="browser-default waves-effect waves-light" id="app_status">
									<option value="0" disabled selected>Change Status</option>
									<?php 
									foreach($all_status as $s){
									?>
									<option value="<?php echo $s['id'] ?>"><?php echo $s['staus'] ?></option>
									<?php } ?>
									
									</select>
								</div>
								<div class="col s12 m6 l3  at-table-align">
								<input type="hidden" id="asid">
									<i class="btn submit-button waves-effect waves-light  waves-input-wrapper"  style="margin-top: -12px;height: 44px;">
									<input class="waves-button-input save_status" type="button" value="save" style="margin-top:10px"></i>
								</div>
							</div>
					
					</div>
					</div-->
					<div class="col s12 m12 l12">
					<!-- div table start -->
				
					<div class="col s12 l0 m10 offset-l1 offset-l1" id="appdataDiv" style="margin-left: 207px;">
					
					</div>
					<!-- div table End -->
					</div>
				</div>
			</div>	
		</div>	
	</div>
</section>	
<!-- END MAIN -->
<script type="text/javascript" src="<?php echo base_url('assets/js/plugins/jquery-1.11.2.min.js');?>"></script>
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/1.3.1/js/toastr.js"></script>

<script type="text/javascript">
<?php 
foreach($dafault_app as $daf_app){
?>

<?php } ?>
function getStatus(id)
	{
	
	$('#appdataDiv').empty();
	//if
		var specVal = id;
	
		var cVal = '<?php echo $this->uri->segment(3); ?>';
		$('.psdata_psdata_29').css('display','none');
		
		$('#appdataDiv').empty();
		$.ajax({
			type: "POST",
			url: "<?php echo base_url('admission/getAppTrackDataStatus'); ?>",
			data:{specVal:specVal,cname:cVal},
			success: function(response){
				var res = JSON.parse(response);
				console.log(res);
				
				var appHtml = '';
				var stat = '';
				var introCount = 0;
				var naCount = 0;
				var ipCount =0;
				var rejCount =0;
				var apprCount =0;
				var fpCount =0;
				var adpCount =0;
				var adoneCount =0;
				
				$.each(res, function(key, value) {
						//decodeURIComponent(value.spe);
					//var url='<?php echo base_url()?>admission/student_details/'+value.stm_user_id+'/'+value.as_id+'/<?php echo $this->uri->segment(3)?>/'+decodeURIComponent(value.spe);
						var url='<?php echo base_url()?>admission/student_details/<?php echo $this->uri->segment(3)?>/'+value.stm_user_id+'/'+value.as_id+'/'+decodeURIComponent(value.spe)+'/'+decodeURIComponent(value.course_spe);
					 appHtml += '<div class="row at-table-row"><div class="col s12 m4 l1 upm-table-content-bg at-table-align" style="width:250px"><a href="'+url+'">'+value.stuname+'</a></div><div class="col s12 m3 l4 upm-table-content-bg at-table-align">'+value.spe+' - '+value.course_spe+'</div><div class="col s12 m4 l4 upm-table-content-bg at-table-align" style="width: 240px;"><div class="col s3 m3 l3 offset-l1 offset-m1 offset-s1 ">';
					 
		
					 
					 if(value.as_status == 4){
						appHtml += '<img  src="<?php echo base_url('assets/img/admission-tracker-icon-01.png'); ?>" class="at-table-image"/>';
						stat = 'In Process';
						ipCount++;
					 }else if(value.as_status == 1){
						appHtml += '<img  src="<?php echo base_url('assets/img/admission-tracker-icon-02.png'); ?>" class="at-table-image"/>';
						stat = 'New Application';
						naCount++;
					 }else if(value.as_status == 8){
						 appHtml += '<img  src="<?php echo base_url('assets/img/admission-tracker-icon-03.png'); ?>" class="at-table-image"/>';
						stat = 'Rejected';
						rejCount++;						
					 }else if(value.as_status == 5){
						 appHtml += '<img  src="<?php echo base_url('assets/img/admission-tracker-icon-04.png'); ?>" class="at-table-image"/>';
						stat = 'Application Approved';
						apprCount++;						
					 }else if(value.as_status == 6){
						 appHtml += '<img  src="<?php echo base_url('assets/img/admission-tracker-icon-05.png'); ?>" class="at-table-image"/>';
						stat = 'Fees Paid'; 
						fpCount++;
					 }else if(value.as_status == 3){
						 appHtml += '<img  src="<?php echo base_url('assets/img/admission-tracker-icon-06.png'); ?>" class="at-table-image"/>';
						stat = 'Under Review';
						 adpCount++;
					 }else if(value.as_status == 7){
						 appHtml += '<img  src="<?php echo base_url('assets/img/admission-tracker-icon-07.png'); ?>" class="at-table-image"/>';
						stat = 'Admission Done';
						adoneCount++;
					 }
					 
					appHtml += '</div><div class="col s8 m8 l8">'+stat+'</div></div>';
					appHtml +='<div class="col s12 m1 l1 upm-table-content-bg at-table-align hiderowdiv"  style="width: 216px;" id="updiv-'+value.as_id+'"><select style="margin-top: -15px;" class="browser-default waves-effect waves-light app_status_'+value.as_id+'" ><option value="0" disabled selected>Change Status</option>'+
									<?php 
									foreach($all_status as $s){
									?>
									'<option value="<?php echo $s['id'] ?>"><?php echo $s['staus'] ?></option>'+
									<?php } ?>
									
				'</select></div>';	 
				appHtml +='<div class="col s12 m1 l1 upm-table-content-bg at-table-align saveButton" style="width: 125px;"  id="upbutton-'+value.as_id+'"><input type="hidden" class="asid" value="'+value.as_id+'"><i class="btn submit-button waves-effect waves-light  waves-input-wrapper"  style="margin-top: -12px;height: 44px;" onclick="save('+value.as_id+')"><input class="waves-button-input save_status" id="" type="button" value="save" style="margin-top:10px"></i></div>';
					
					appHtml += '<div class="col s12 m1 l1 upm-table-content-bg at-table-align editButton" id="upedit-'+value.as_id+'"><a href="'+url+'" ><img class="sideIcon cust-cursor" src="<?php echo base_url('assets/img/edit-icon-01.png'); ?>" class="at-table-image"/></a></div></div>';
		
				});
				
				$('#appdataDiv').append(appHtml);
				
				introCount = parseInt(naCount)+parseInt(ipCount)+parseInt(rejCount)+parseInt(apprCount)+parseInt(fpCount)+parseInt(adpCount)+parseInt(adoneCount);
				
				
				
				// $('#ipCount').text(parseInt(ipCount));
				// $('#rejCount').text(parseInt(rejCount));
				// $('#apprCount').text(parseInt(apprCount));
				// $('#fpCount').text(parseInt(fpCount));
				// $('#adpCount').text(parseInt(adpCount));
				// $('#adoneCount').text(parseInt(adoneCount)); 
				// $('#introCount').text(introCount);
				$('.hiderowdiv').hide();
				$('.saveButton').hide();
			}
		});
	}
$(document).ready(function(){

getStatus(0);
	$('.com_spec').change(function(){
		$('#appdataDiv').empty();
		var specVal = $(this).val();
		//	alert(specVal);
		 // this.value = (Number(this.checked));
		if(specVal=='all')
		 {
			getStatus(0);

		 	//$(this).prop('checked',this.checked);
			//  $('select option:not(:disabled)').is(':selected').prop('selected', false);
		// 
		}
		//$('select option:not(:disabled)').not(':selected').prop('selected', true);
		 //$(this).not(':selected').prop('selected', false);
		//$(this).prop("checked");
 //$('.dropdown-content.multiple-select-dropdown input[type="checkbox"]:checked').not(':disabled').prop('checked', true);
			//selectNone();
		//}
		var cVal = '<?php echo $this->uri->segment(3); ?>';
		//console.log(cVal);
		
		$.ajax({
			type: "POST",
			url: "<?php echo base_url('admission/getAppTrackData'); ?>",
			data:{specVal:specVal},
			success: function(response){
				//$('#appdataDiv').empty();
				var res = JSON.parse(response);
				//console.log(res);
				
				var appHtml = '';
				var stat = '';
				var introCount = 0;
				var naCount = 0;
				var ipCount =0;
				var rejCount =0;
				var apprCount =0;
				var fpCount =0;
				var adpCount =0;
				var adoneCount =0;
				
				$.each(res, function(key, value) {   
					var url='<?php echo base_url()?>admission/student_details/'+value.stm_user_id+'/'+value.as_id+'/<?php echo $this->uri->segment(3)?>/'+decodeURIComponent(value.spe);
					 appHtml += '<div class="row at-table-row"><div class="col s12 m4 l1 upm-table-content-bg at-table-align" style="width:250px"><a href="'+url+'">'+value.stuname+'</a></div><div class="col s12 m3 l4 upm-table-content-bg at-table-align">'+cVal+' - '+value.spe+'</div><div class="col s12 m4 l4 upm-table-content-bg at-table-align" style="width: 240px;"><div class="col s3 m3 l3 offset-l1 offset-m1 offset-s1 ">';
					 
					 if(value.as_status == 4){
						appHtml += '<img  src="<?php echo base_url('assets/img/admission-tracker-icon-01.png'); ?>" class="at-table-image"/>';
						stat = 'In Process';
						ipCount++;
					 }else if(value.as_status == 1){
						appHtml += '<img  src="<?php echo base_url('assets/img/admission-tracker-icon-02.png'); ?>" class="at-table-image"/>';
						stat = 'New Application';
						naCount++;
					 }else if(value.as_status == 8){
						 appHtml += '<img  src="<?php echo base_url('assets/img/admission-tracker-icon-03.png'); ?>" class="at-table-image"/>';
						stat = 'Rejected';
						rejCount++;						
					 }else if(value.as_status == 5){
						 appHtml += '<img  src="<?php echo base_url('assets/img/admission-tracker-icon-04.png'); ?>" class="at-table-image"/>';
						stat = 'Application Approved';
						apprCount++;						
					 }else if(value.as_status == 6){
						 appHtml += '<img  src="<?php echo base_url('assets/img/admission-tracker-icon-05.png'); ?>" class="at-table-image"/>';
						stat = 'Fees Paid'; 
						fpCount++;
					 }else if(value.as_status == 3){
						 appHtml += '<img  src="<?php echo base_url('assets/img/admission-tracker-icon-06.png'); ?>" class="at-table-image"/>';
						stat = 'Under Review';
						 adpCount++;
					 }else if(value.as_status == 7){
						 appHtml += '<img  src="<?php echo base_url('assets/img/admission-tracker-icon-07.png'); ?>" class="at-table-image"/>';
						stat = 'Admission Done';
						adoneCount++;
					 }
					 
					 
					appHtml += '</div><div class="col s8 m8 l8">'+stat+'</div></div>';
					appHtml +='<div class="col s12 m1 l1 upm-table-content-bg at-table-align hiderowdiv"  style="width: 216px;" id="updiv-'+value.as_id+'"><select style="margin-top: -15px;" class="browser-default waves-effect waves-light app_status_'+value.as_id+'" ><option value="0" disabled selected>Change Status</option>'+
									<?php 
									foreach($all_status as $s){
									?>
									'<option value="<?php echo $s['id'] ?>"><?php echo $s['staus'] ?></option>'+
									<?php } ?>
									
				'</select></div>';	 
				appHtml +='<div class="col s12 m1 l1 upm-table-content-bg at-table-align saveButton" style="width: 125px;"  id="upbutton-'+value.as_id+'"><input type="hidden" class="asid" value="'+value.as_id+'"><i class="btn submit-button waves-effect waves-light  waves-input-wrapper"  style="margin-top: -12px;height: 44px;" onclick="save('+value.as_id+')"><input class="waves-button-input save_status" id="" type="button" value="save" style="margin-top:10px"></i></div>';
					
					appHtml += '<div class="col s12 m1 l1 upm-table-content-bg at-table-align editButton" id="upedit-'+value.as_id+'"><a href="'+url+'" ><img class="sideIcon cust-cursor" src="<?php echo base_url('assets/img/edit-icon-01.png'); ?>" class="at-table-image"/></a></div></div>';
		
				});
				
				$('#appdataDiv').append(appHtml);
				
				introCount = parseInt(naCount)+parseInt(ipCount)+parseInt(rejCount)+parseInt(apprCount)+parseInt(fpCount)+parseInt(adpCount)+parseInt(adoneCount);
				
				// $('#naCount').text(parseInt(naCount));
				// $('#ipCount').text(parseInt(ipCount));
				// $('#rejCount').text(parseInt(rejCount));
				// $('#apprCount').text(parseInt(apprCount));
				// $('#fpCount').text(parseInt(fpCount));
				// $('#adpCount').text(parseInt(adpCount));
				// $('#adoneCount').text(parseInt(adoneCount)); 
				// $('#introCount').text(introCount);
				
				$('.hiderowdiv').hide();
				$('.saveButton').hide();
			}
		});
//}
	});
});
function edit(id)
{
	//$('.ps_'+id).show();
	$('.hiderowdiv').hide();
	$('.saveButton').hide();
	$('.editButton').show();
	$('#upedit-'+id).css("display","none");
	$('#upbutton-'+id).css("display","block");
	$('#updiv-'+id).css("display","block");
	// $('.psdata_'+id).css("display","block");
	
	$('.asid').val(id);
	$.ajax({
			type: "POST",
			url: "<?php echo base_url('admission/application_status'); ?>",
			data:{asid:id},
			success: function(response){
				var res = JSON.parse(response);
				console.log(res);
				$("#app_status option[value='" + res.as_status + "']").attr("selected","selected");
			}
	});
}


function save(id)
{
	var as_stat=$('.app_status_'+id).val();
//console.log(as_stat);
//return false;
			$.ajax({
			type: "POST",
			url: "<?php echo base_url('admission/updateAppStatus'); ?>",
			data:{appstatus:as_stat,as_id:id},
			success: function(response){
				// var res = JSON.parse(response);
				console.log(response);
					toastr.success("Status updated successfully");
				location.reload();
					setTimeout(function(){
		 getStatus(0)
		//$('#add_program_form').hide( "slow" );
		 //$('#pstatus').hide("slow");
		}, 100);
				
			}
	});
}
</script>
<script type="text/javascript">

	var activateOption = function(collection, newOption) {
    if (newOption) {
        collection.find('li.selected').removeClass('selected');

        var option = $(newOption);
        option.addClass('selected');
    }
};

$(document).ready(function() {
    $('select').material_select();
    $('.test-input > .select-wrapper > .select-dropdown').prepend('<li class="toggle selectall"><span><label></label>Select all</span></li>');
    $('.test-input > .select-wrapper > .select-dropdown').prepend('<li style="display:none" class="toggle selectnone"><span><label></label>Select none</span></li>');
    $('.test-input > .select-wrapper > .select-dropdown .selectall').on('click', function() {
        selectAll();
        $('.test-input > .select-wrapper > .select-dropdown .toggle').toggle();
    });
    $('.test-input > .select-wrapper > .select-dropdown .selectnone').on('click', function() {
        selectNone();
        $('.test-input > .select-wrapper > .select-dropdown .toggle').toggle();
    });
});

function selectNone() {
    $('select option:selected').not(':disabled').prop('selected', false);

    $('.dropdown-content.multiple-select-dropdown input[type="checkbox"]:checked').not(':disabled').prop('checked', false);
    //$('.dropdown-content.multiple-select-dropdown input[type="checkbox"]:checked').not(':disabled').trigger('click');
    var values = $('.dropdown-content.multiple-select-dropdown input[type="checkbox"]:disabled').parent().text();
    $('input.select-dropdown').val(values);
    console.log($('select').val());
};

function selectAll() {
    $('select option:not(:disabled)').not(':selected').prop('selected', true);
    $('.dropdown-content.multiple-select-dropdown input[type="checkbox"]:not(:checked)').not(':disabled').prop('checked',true);
    //$('.dropdown-content.multiple-select-dropdown input[type="checkbox"]:not(:checked)').not(':disabled').trigger('click');
    var values = $('.dropdown-content.multiple-select-dropdown input[type="checkbox"]:checked').not(':disabled').parent().map(function() {
        return $(this).text();
    }).get();
    $('input.select-dropdown').val(values.join(', '));
    console.log($('select').val());
};


</script>