

        <section class="section-padding gray-bg">
		
            <div class="container">

                <div class="login-wrapper">
                  <div class="card-wrapper"></div>
				  
                  <div class="card-wrapper">
                    <h1 class="title">Student Login</h1>
                 <form  action="<?php echo base_url('login/login'); ?>" method="post">
                      <div class="input-container">
                        <input  id="username" type="text" name="email" required="required" />
                        <label for="username">Email</label>
                        <div class="bar"></div>
                      </div>
                      <div class="input-container">
                        <input id="password" type="password" name="password" required="required" />
                        <label for="password">Password</label>
                        <div class="bar"></div>
                      </div>
                      <div class="button-container">
                        <button type="submit" class="btn btn-lg btn-block waves-effect waves-light">Login</button>
                      </div> 
					 
                       <div class="footer"><a href="<?php echo base_url('login/forgot_password')?>">Forgot your password?</a></div>
                    
					
                    </form>
                  </div>
                  <div class="card-wrapper alt">
                    <div class="toggle" ></div>
                    <h4 class="title"> Student 
                      <div class="close"></div>
                    </h4>
                     <form id="registerForm" action="<?php echo base_url('login/student_register'); ?>" method="post" style="margin-top: -39px;">
                      <div class="input-container">
                        <input id="uname" type="text" name="uname" required="required" autocomplete="off" />
                        <label for="newusername">Name</label>
                        <div class="bar"></div>
                      </div>
                      <div class="input-container">
                        <input  type="text" name="cemail" required="required" autocomplete="off" />
                        <label for="email">Email</label>
                        <div class="bar"></div>
                      </div>
					    <div class="input-container">
                        <input id="password" type="password" name="password" required="required" autocomplete="off" />
                        <label for="newpassword">Password</label>
                        <div class="bar"></div>
                      </div>
                      <div class="input-container">
                        <input id="password" type="password" name="confirm_password" required="required" autocomplete="off" />
                        <label for="repeat-password">Repeat Password</label>
                        <div class="bar"></div>
                      </div>
                      <div class="button-container">
                        <button class="btn btn-lg btn-block white waves-effect waves-red">Registar</button>
                      </div>
                    </form>
                  </div>
                </div>

            </div>
        </section>
   <script type="text/javascript" src="<?php echo base_url('assets/js/plugins/jquery-validation/jquery.validate.min.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/plugins/jquery-validation/additional-methods.min.js');?>"></script>
    <script type="text/javascript">
$(document).ready(function(){
  //$('.student').click(function(){
    //alert($(this).text());
    console.log($(this).id);
    //if($(this).text()=='SingUp'){
     
      $('.login-wrapper').addClass('active');
   // }

 // })
});
    </script>
<script>
$("#registerForm").validate({
        rules: {
            uname: {
                required: true,
                minlength: 5
            },
            cemail: {
                required: true,
                email:true
            },
            password: {
				required: true,
				minlength: 5
			},
			confirm_password: {
				required: true,
				minlength: 5,
				//equalTo: "#password"
			},
			role: { 
				required: true
			}
        },
        //For custom messages
        messages: {
            uname:{
                required: "Enter a username",
                minlength: "Enter at least 5 characters"
            },
			cemail: {
                required: "Enter a email address",
                email:"Enter valid email address"
            },
			password: {
				required: "Enter a password",
				minlength: "Enter at least 5 characters"
			},
			confirm_password: {
				required: "Enter a confirm password",
				minlength: "Enter at least 5 characters",
				//equalTo: "Please enter the same value again"
			}
        },
        errorElement : 'div',
        errorPlacement: function(error, element) {
          var placement = $(element).data('error');
          if (placement) {
            $(placement).append(error)
          } else {
            error.insertAfter(element);
          }
        }
     });

</script>


       