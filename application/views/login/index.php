

<style type="text/css">



 .error{
  color: red;
 }
p {
  font-feature-settings: "kern" 1; /* 5 */
  font-kerning: normal; /* 5 */
  hyphens: auto; /* 6 */
}
 


h2 { font-size: 30px; font-weight: 300; color: #03A9F4; }



.control-label {
    font-size: 16px;
    font-weight: 400;
    color: #202020;
    pointer-events: none;
    position: absolute;
    -webkit-transform: translate3d(0, 0px, 0) scale(1);
    -moz-transform: translate3d(0, 22px, 0) scale(1);
    transform: translate3d(0, 0px, 0) scale(1);
    -webkit-transform-origin: left top;
    -moz-transform-origin: left top;
    transform-origin: left top;
    -webkit-transition: 260ms;
    -moz-transition: 260ms;
    transition: 260ms;
}

.form-group.focused .control-label {
    opacity: 1;
    -
    color: #03A9F4 !important;
}

.form-control {
    align-self: flex-end;
}

.form-control::-webkit-input-placeholder {
    color: transparent;
    -webkit-transition: 240ms;
    -moz-transition: 240ms;
    transition: 240ms;
}

.form-control:focus::-webkit-input-placeholder {
    -webkit-transition: none
    -moz-transition: none;
    transition: none;
}

.form-group.focused .form-control::-webkit-input-placeholder {
    color: #bbb;
}

#textbox { width: 50%; max-height: 100%; height: 100%; overflow: hidden; margin-left: 50%; position: absolute; }
.toplam { width: 200%; height: 100%; position: relative; left: 10;   left: -100%;}
.left { width: 50%; height: 100%; background: #2C3034;  left: 0; position: absolute;}
.right { width: 50%; height: 100%; background: #F9F9F9; right: 0; position: absolute;}
#ic { width: 62%; margin: 0 auto; top: 50%; margin-top: -162px; position: absolute; left: 50%; margin-left: -186px; }
.left #ic { margin-top: -174px;}
#ic p {  font-size: 15px; font-weight: 400; line-height: 26px; color: #757575; margin-bottom: 30px;}

form#girisyap input {
  border-bottom: 1px solid #cdcdcd;
  color: #959595;
  font-size: 14px;
  width: 100%;
  resize: none;
  max-height: 20px;
  outline: 0;
  border-left: 0;
  border-right: 0;
  padding: 8px 0;
  margin-top: 20px !important;
  background: none;
  border-top: 0;
  overflow:hidden;
 transition: border .2s;
 -webkit-transition: border .2s;
}

#girisyap .form-group { width: 100%; float: left; margin-bottom: 0px;}
#girisyap h2 { font-size: 28px; font-weight: 400; color: #0BAFBF; line-height: 35px; }
#girisyap .soninpt { margin-bottom: 30px; }
.left form#girisyap input { border-bottom: 1px solid #45494C; }
.left .control-label { color: #959595; }
.left .yarim { width: 46% !important; float: left; margin-right: 8%; }
.left .sn { margin-right: 0;}


form#girisyap .girisbtn{   width: auto;
  float: right;
  background: #03A9F4;
  padding: 10px 16px;
  cursor: pointer;
  font-size: 14px;
  font-weight: 600;
  color: #fff;
  margin: 0 !important;
  line-height: 16px;
  font-family: Roboto;
  text-transform: uppercase;
  max-height: 36px;
  height: 36px;
  letter-spacing: 0.5px;
  -webkit-border-radius: 2px;
  -moz-border-radius: 2px;
  border-radius: 2px;
  -webkit-box-shadow: 0 2px 6px rgba(0,0,0,0.1), 0 3px 6px rgba(0,0,0,0.1);
  -moz-box-shadow: 0 2px 6px rgba(0,0,0,0.1), 0 3px 6px rgba(0,0,0,0.1);
  box-shadow: 0 2px 6px rgba(0,0,0,0.1), 0 3px 6px rgba(0,0,0,0.1);
  -webkit-transition-duration: 0.25s;
  transition-duration: 0.25s;
  -webkit-transition-property: background-color,-webkit-box-shadow;
  transition-property: background-color,box-shadow;
  border: 0; }

form#girisyap .girisbtn:hover{ background: #0288D1;  -webkit-box-shadow: 0 4px 7px rgba(0,0,0,0.1), 0 3px 6px rgba(0,0,0,0.1); -moz-box-shadow: 0 4px 7px rgba(0,0,0,0.1), 0 3px 6px rgba(0,0,0,0.1); box-shadow: 0 4px 7px rgba(0,0,0,0.1), 0 3px 6px rgba(0,0,0,0.1);}

#moveleft, #moveright{ width: auto;
  float: right;
  background: none;
  padding: 10px 16px;
  font-family: Roboto;
  font-size: 14px;
  color: #03A9F4;
  font-weight: 600;
  margin: 0 10px !important;
  line-height: 16px;
  text-transform: uppercase;
  cursor: pointer;
  max-height: 36px;
  height: 36px;
  letter-spacing: 0.5px;
  -webkit-border-radius: 2px;
  -moz-border-radius: 2px;
  border-radius: 2px;
  -webkit-transition-duration: 0.25s;
  transition-duration: 0.25s;
  -webkit-transition-property: background-color;
  transition-property: background-color;
  border: 0; 
  outline: 0;}

#moveleft:hover { background: #eee; }
#moveright:hover { background: #363A3D; }

#fback { width: 100%; height: 100%; position: absolute; z-index: -9999; }
.girisback {right: 0; width: 50%; background: #03A9F4 url('<?php echo base_url()?>/assets/img/signup.jpg'); background-size: cover; background-position: 10% 10%;}
.kayitback { left: 0; width: 50%; background: #03A9F4 url('<?php echo base_url()?>/assets/img/login.png'); background-size: cover; background-position: 61% 10%;}
/*! normalize.css v3.0.2 | MIT License | git.io/normalize */

/**
 * 1. Set default font family to sans-serif.
 * 2. Prevent iOS text size adjust after orientation change, without disabling
 *    user zoom.
 */



/**
 * Remove default margin.
 */
 .btn_f {
  margin-top: -15px;
    height: 50px;
    padding: 12px 20px;
    font-size: 16px;
    line-height: 26px;
    border-radius: 2px;
    background-color: #03a9f4;
    border: 0;
    width: 50%;
  }
  .btn_g {
  margin-top: -15px;
    height: 50px;
    padding: 12px 20px;
    font-size: 16px;
    line-height: 26px;
    border-radius: 2px;
    background-color: #03a9f4;
    border: 0;
    width: 48%;
  }

</style> 
        <section class="gray-bg " style="height: 100%; overflow: hidden; width:100% !important;">
    
            <div id="">
        <!--       <div class="row">
                  <div class="col-md-6">1</div>
                   <div class="col-md-6">1</div>
              </div> -->
              <div id="fback">
                <div class="girisback" style="backgroud:red"> dssada</div>
                <div class="kayitback">dsadada</div>
              </div>
        <!--         <div class="login-wrapper ">
                  <div class="card-wrapper"></div>
          
                  <div class="card-wrapper"> -->
                
        
    <div id="textbox" >
      <div class="toplam">
        <div class="left">
                <div id="ic">
        <h3 style="color:#03A9F4">Sign Up</h3><hr>
                <form id="girisyap" name="signup_form" id="signup_form" action="<?php echo base_url('login/student_register'); ?>" method="post" enctype="multipart/form-data" >

          <div class=" form-group">
                    <label class="control-label" for="inputNormal" style="color:#fff">Name</label>
                    <!-- <input type="text" name="signup_username" id="signup_username" class="bp-suggestions form-control" cols="50" rows="10" ></input> -->
                      <input id="uname" type="text" name="uname" required="required" autocomplete="off" />
          </div>
        
          <div class="form-group">
                    <label class="control-label" for="inputNormal" style="color:#fff">Email</label>
                   <input  type="text" name="cemail" required="required" autocomplete="off" />
          </div>
          <div class="form-group">
                    <label class="control-label" for="inputNormal" style="color:#fff">Password</label>
                      <input id="password" type="password" name="password" required="required" autocomplete="off" />
          </div>
          <div class="form-group soninpt">
                <label class="control-label" for="inputNormal" style="color:#fff">Confirm Password</label>
            <input id="password" type="password" name="confirm_password" required="required" autocomplete="off" />
          </div>
          <button type="submit" class="btn btn-lg btn-block waves-effect waves-light">SIGN UP</button>
          </form>
         <p >
          
          You have an account ?<a href="#" id="moveright">Login</a></p>
        </div>
        </div>

        <div class="right">
        <div id="ic">
          <h2>Login</h2>
          <hr>
          <!-- <p>Synth polaroid bitters chillwave pickled. Vegan disrupt tousled.</p> -->
          <form name="login-form" style="margin-top:20px" id="girisyap" id="sidebar-user-login" action="<?php echo base_url('login/login'); ?>"  method="post" >

          <div class="form-group">
                    <label class="control-label" for="inputNormal">Username</label>
                   <input  id="username" type="text" name="email" required="required" />
          <div class="form-group soninpt">
                    <label class="control-label" for="inputNormal">Password</label>
                      <input id="password" type="password" name="password" required="required" />
          </div>
          <button type="submit" class="btn btn-lg btn-block waves-effect waves-light">LOGIN</button>
          </form>
        <p style="margin-left:0%;margin-top:17px"><a style="color:#999;font-weight:bold" href="<?php echo base_url()?>/forgot-password">Forgot Password?</a><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OR</b></p>
                </div>
                <?php 
                $fb='';
                if(!empty($authUrl)){
                   $fb=$authUrl;
                } 

                $ga='';
                if(!empty($loginURL)){
                   $ga=$loginURL;
                } 

                ?>
          <p style="margin-left:0%">
            <a href="<?php echo $fb; ?>" class="btn_f waves-effect" style="background-color: #3a589b;color:white;font-weight:bold"><i class="fa fa-facebook" aria-hidden="true" style="font-size:20px;color:white"></i>acebook</a>  
            <a href="<?php echo $ga; ?>" class="btn_g waves-effect" style="background-color: #ed2524;color:white;font-weight:bold"><i class="fa fa-google" aria-hidden="true" style="font-size:20px;color:white"></i>oogle</a>  
          </p>
           You Don't have an accout ?<a href="#" id="moveleft">Sign Up</a>
                </div>
        </div>
      </div>
   </div>
                  <!--   <h1 class="title">Student Login</h1>
                 <form  action="<?php echo base_url('login/login'); ?>" method="post">
                      <div class="input-container">
                        <input  id="username" type="text" name="email" required="required" />
                        <label for="username">Username</label>
                        <div class="bar"></div>
                      </div>
                      <div class="input-container">
                        <input id="password" type="password" name="password" required="required" />
                        <label for="password">Password</label>
                        <div class="bar"></div>
                      </div>
                      <div class="button-container">
                        <button type="submit" class="btn btn-lg btn-block waves-effect waves-light">Login</button>
                      </div> 
           
                       <div class="footer" style="background-color:rgba(255,255,255,0.87);"><a href="<?php echo base_url('login/forgot_password')?>">Forgot your password?</a></div>
                    
          
                    </form> -->
                  <!-- </div> -->
                 <!--  <div class="card-wrapper alt">
                    <div class="toggle" ></div>
                    <h4 class="title"> Student 
                      <div class="close"></div>
                    </h4>
                     <form id="registerForm" action="<?php echo base_url('login/student_register'); ?>" method="post" style="margin-top: -39px;">
                      <div class="input-container">
                        <input id="uname" type="text" name="uname" required="required" autocomplete="off" />
                        <label for="newusername">Username</label>
                        <div class="bar"></div>
                      </div>
                      <div class="input-container">
                        <input  type="text" name="cemail" required="required" autocomplete="off" />
                        <label for="email">Email</label>
                        <div class="bar"></div>
                      </div>
              <div class="input-container">
                        <input id="password" type="password" name="password" required="required" autocomplete="off" />
                        <label for="newpassword">Password</label>
                        <div class="bar"></div>
                      </div>
                      <div class="input-container">
                        <input id="password" type="password" name="confirm_password" required="required" autocomplete="off" />
                        <label for="repeat-password">Repeat Password</label>
                        <div class="bar"></div>
                      </div>
                      <div class="button-container">
                        <button class="btn btn-lg btn-block white waves-effect waves-red">Registar</button>
                      </div>
                    </form>
                  </div> -->
                </div>

            </div>
        </section>
   <script type="text/javascript" src="<?php echo base_url('assets/js/plugins/jquery-validation/jquery.validate.min.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/plugins/jquery-validation/additional-methods.min.js');?>"></script>
      <script type="text/javascript">//<![CDATA[ 
$(window).load(function(){
$('.form-control').on('focus blur', function (e) {
    $(this).parents('.form-group').toggleClass('focused', (e.type === 'focus' || this.value.length > 0));
}).trigger('blur');
});//]]>  

</script> 
<script>
$(document).ready(function() {

   $('section').addClass('kayitback');
  $('#moveleft').click(function(){
     $('section').removeClass('kayitback');
     $('section').addClass('girisback');
  });
  $('#moveright').click(function(){
    $('section').removeClass('girisback');
     $('section').addClass('kayitback');
  });
    $('#moveleft').click(function() {
        $('#textbox').animate({
        'marginLeft' : "0" //moves left
        });
        
        $('.toplam').animate({
        'marginLeft' : "100%" //moves right
        });
    });
    
    $('#moveright').click(function() {
        $('#textbox').animate({
        'marginLeft' : "50%" //moves right
        });
        
        $('.toplam').animate({
        'marginLeft' : "0" //moves right
        });
    });
    
});
$("#girisyap").validate({
        rules: {
            uname: {
                required: true,
                minlength: 5
            },
            cemail: {
                required: true,
                email:true
            },
            password: {
        required: true,
        minlength: 5
      },
      confirm_password: {
        required: true,
        minlength: 5,
        //equalTo: "#password"
      },
      role: { 
        required: true
      }
        },
        //For custom messages
        messages: {
            uname:{
                required: "Enter a username",
                minlength: "Enter at least 5 characters"
            },
      cemail: {
                required: "Enter a email address",
                email:"Enter valid email address"
            },
      password: {
        required: "Enter a password",
        minlength: "Enter at least 5 characters"
      },
      confirm_password: {
        required: "Enter a confirm password",
        minlength: "Enter at least 5 characters",
        //equalTo: "Please enter the same value again"
      }
        },
        errorElement : 'div',
        errorPlacement: function(error, element) {
          var placement = $(element).data('error');
          if (placement) {
            $(placement).append(error)
          } else {
            error.insertAfter(element);
          }
        }
     });

</script>


       