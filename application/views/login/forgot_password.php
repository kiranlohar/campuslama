

        <section class="section-padding gray-bg">
		
            <div class="container">

                <div class="login-wrapper">
                  <div class="card-wrapper"></div>
				  
                  <div class="card-wrapper">
                    <h2 class="title">Forgot Password</h1>
                 <form  action="#" method="post" id="forgotForm">
                      <div class="input-container">
                        <input  id="email" type="text" name="email" required="required" />
                        <label for="username">Email</label>
                        <div class="bar"></div>
                      </div>
                      
                      <div class="button-container">
                        <button  type="button" id="forgotpass" class="btn btn-lg btn-block waves-effect waves-light">Reset</button>
                      </div> 
					 
                      <div class="footer" style="background:none">You have an account? <a href="<?php echo base_url('login')?>">Login</a></div>
                    
					
                    </form>
                  </div>
                
                </div>

            </div>
        </section>
   <script type="text/javascript" src="<?php echo base_url('assets/js/plugins/jquery-validation/jquery.validate.min.js');?>"></script>
 <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/1.3.1/js/toastr.js"></script>

<script>


</script>
<script>
$('#forgotpass').click(function(){
	var email=$('#email').val();
	 var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    
	if(email=='')
	{
		toastr.error("Enter Email-Id");
		return false;
	}else if(!re.test(email))
	{
		toastr.error("Enter Valid Email-Id");
			return false;
	}else
	{
		// toastr.success("Go Head");
		$.ajax({
			type:'POST',
			url:'<?php echo base_url()?>login/forgot',
			data:{email:email},
			success:function(res){
				var obj=JSON.parse(res);
				console.log(obj);
				if(obj.msg=='Enter email does not exits')
				{
					toastr.error(obj.msg);
					return false;
				}else if(obj.msg=='Mail send on your register email Please check'){
					toastr.success(obj.msg);
					$('#email').val('');
				}
			}
		});
	}
});
</script>


       