<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
    <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
    <title>Hello Admission</title>

    <!-- Favicons-->
    <link rel="icon" href="<?php echo base_url('assets/images/favicon/favicon-32x32.png');?>" sizes="32x32">
    <!-- Favicons-->
    <link rel="apple-touch-icon-precomposed" href="images/favicon/apple-touch-icon-152x152.png">
    <!-- For iPhone -->
    <meta name="msapplication-TileColor" content="#00bcd4">
    <meta name="msapplication-TileImage" content="<?php echo base_url('assets/images/favicon/mstile-144x144.png');?>">
    <!-- For Windows Phone -->
	
	
	<link href="<?php echo base_url('assets/css/materialize.min.css');?>" type="text/css" rel="stylesheet" media="screen,projection">
	<link href="<?php echo base_url('assets/css/style.min.css');?>" type="text/css" rel="stylesheet" media="screen,projection">
    <!-- Custome CSS-->    
    <link href="<?php echo base_url('assets/css/custom/custom.min.css');?>" type="text/css" rel="stylesheet" media="screen,projection">
	
    <link href="<?php echo base_url('assets/css/layouts/page-center.css');?>" type="text/css" rel="stylesheet" media="screen,projection">

    <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
    <link href="<?php echo base_url('assets/js/plugins/perfect-scrollbar/perfect-scrollbar.css');?>" type="text/css" rel="stylesheet');?>" media="screen,projection">
    <link href="<?php echo base_url('assets/js/plugins/jvectormap/jquery-jvectormap.css');?>" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="<?php echo base_url('assets/js/plugins/chartist-js/chartist.min.css');?>" type="text/css" rel="stylesheet" media="screen,projection">
	
	

</head>
<style type="text/css">
  .input-field div.error{
    position: relative;
    top: -1rem;
    left: 3rem;
    font-size: 0.8rem;
    color:#FF4081;
    -webkit-transform: translateY(0%);
    -ms-transform: translateY(0%);
    -o-transform: translateY(0%);
    transform: translateY(0%);
  }
  .input-field label.active{
      width:100%;
  }
  .left-alert input[type=text] + label:after, 
  .left-alert input[type=password] + label:after, 
  .left-alert input[type=email] + label:after, 
  .left-alert input[type=url] + label:after, 
  .left-alert input[type=time] + label:after,
  .left-alert input[type=date] + label:after, 
  .left-alert input[type=datetime-local] + label:after, 
  .left-alert input[type=tel] + label:after, 
  .left-alert input[type=number] + label:after, 
  .left-alert input[type=search] + label:after, 
  .left-alert textarea.materialize-textarea + label:after{
      left:0px;
  }
  .right-alert input[type=text] + label:after, 
  .right-alert input[type=password] + label:after, 
  .right-alert input[type=email] + label:after, 
  .right-alert input[type=url] + label:after, 
  .right-alert input[type=time] + label:after,
  .right-alert input[type=date] + label:after, 
  .right-alert input[type=datetime-local] + label:after, 
  .right-alert input[type=tel] + label:after, 
  .right-alert input[type=number] + label:after, 
  .right-alert input[type=search] + label:after, 
  .right-alert textarea.materialize-textarea + label:after{
      right:70px;
  }
  </style>
<body class="cyan">
  <!-- Start Page Loading -->
  <div id="loader-wrapper">
      <div id="loader"></div>        
      <div class="loader-section section-left"></div>
      <div class="loader-section section-right"></div>
  </div>
  <!-- End Page Loading -->
  <div id="login-page" class="row">
    <div class="col s12 z-depth-4 card-panel">
      <form id="registerForm" class="login-form" action="<?php echo base_url('login/student_register'); ?>" method="post">
        <div class="row">
          <div class="input-field col s12 center">
            <h4>Student Register</h4>
            <p class="center">Join <a href="<?php echo base_url(); ?>" class="LogoMargin"><span class="red-text">Hello</span><span class="black-text">Admission</span></a> Now ! </p>
          </div>
        </div>
        <div class="row margin">
          <div class="input-field col s12">
            <i class="mdi-social-person-outline prefix"></i>
            <input id="uname" type="text" name="uname">
            <label for="uname">Name</label>
          </div>
        </div>
        <div class="row margin">
          <div class="input-field col s12">
            <i class="mdi-communication-email prefix"></i>
            <input id="cemail" type="email" name="cemail">
            <label for="cemail">Email</label>
          </div>
        </div>
        <div class="row margin">
          <div class="input-field col s12">
            <i class="mdi-action-lock-outline prefix"></i>
            <input id="password" type="password" name="password">
            <label for="password">Password</label>
          </div>
        </div>
        <div class="row margin">
          <div class="input-field col s12">
            <i class="mdi-action-lock-outline prefix"></i>
            <input  type="password" name="confirm_password">
            <label for="password-again">Confirm Password</label>
          </div>
        </div>
        <div class="row">
          <div class="input-field col s12">
            <input type="submit" class="btn waves-effect waves-light col s12" value="Register Now"/>
          </div>
          <div class="input-field col s12">
            <p class="margin center medium-small sign-up">Already have an account? <a href="<?php echo base_url('login/index'); ?>">Login</a></p>
          </div>
        </div>
      </form>
    </div>
  </div>


	<script src="<?php echo base_url('assets/js/plugins/jquery-1.11.2.min.js');?>"></script>
	 <!-- chartist -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/plugins/jquery-validation/jquery.validate.min.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/plugins/jquery-validation/additional-methods.min.js');?>"></script>
<script>
$("#registerForm").validate({
        rules: {
            uname: {
                required: true,
                minlength: 3
            },
            cemail: {
                required: true,
                email:true
            },
            password: {
				required: true,
				minlength: 5
			},
			confirm_password: {
				required: true,
				minlength: 5,
				//equalTo: "#password"
			},
			role: { 
				required: true
			}
        },
        //For custom messages
        messages: {
            uname:{
                required: "Enter a username",
                minlength: "Enter at least 5 characters"
            },
			cemail: {
                required: "Enter a email address",
                email:"Enter valid email address"
            },
			password: {
				required: "Enter a password",
				minlength: "Enter at least 5 characters"
			},
			confirm_password: {
				required: "Enter a confirm password",
				minlength: "Enter at least 5 characters",
				//equalTo: "Please enter the same value again"
			}
        },
        errorElement : 'div',
        errorPlacement: function(error, element) {
          var placement = $(element).data('error');
          if (placement) {
            $(placement).append(error)
          } else {
            error.insertAfter(element);
          }
        }
     });

</script>
	
</body>
</html>