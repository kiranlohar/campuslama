

        <section class="section-padding gray-bg">
		
            <div class="container">

                <div class="login-wrapper">
                  <div class="card-wrapper"></div>
				  
                  <div class="card-wrapper">
                    <h2 class="title">Change Password </h1>
                 <form  action="#" method="post" id="forgotForm">
                      <div class="input-container">
                        <input  id="password" type="password" name="password" required="required" />
                        <label for="username">New Password</label>
                        <div class="bar"></div>
                      </div>
                      <div class="input-container">
                        <input  id="cpassword" type="password" name="cpassword" required="required" />
                        <label for="username">Confirm Password</label>
                        <div class="bar"></div>
                      </div>
                      <div class="button-container">
                        <button  type="button" id="change_password" class="btn btn-lg btn-block waves-effect waves-light">submit</button>
                      </div> 
					 
                      <!--div class="footer">You have an account? <a href="<?php echo base_url('login')?>">Login</a></div-->
                    
					
                    </form>
                  </div>
                
                </div>

            </div>
        </section>
   <script type="text/javascript" src="<?php echo base_url('assets/js/plugins/jquery-validation/jquery.validate.min.js');?>"></script>
 <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/1.3.1/js/toastr.js"></script>

<script>


</script>
<script>
$('#change_password').click(function(){
	var userid='<?php echo $user_id;?>';
	var password=$('#password').val();
	var cpassword=$('#cpassword').val();
	 var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    
	if(password=='')
	{
		toastr.error("Enter New Password");
		$('#password').focus();
		return false;
	}else if(cpassword=='')
	{
		toastr.error("Enter Confirm Password");
		$('#cpassword').focus();
			return false;
	}else if(password!=cpassword)
	{
		toastr.error("Enter Does not match");
		$('#cpassword').focus();
			return false;
	}
	else
	{
		//toastr.success("Go Head");
		$.ajax({
			type:'POST',
			url:'<?php echo base_url()?>login/update_password',
			data:{npassword:password,userid:userid},
			success:function(res){
				var obj=JSON.parse(res);
				 console.log(obj);
				if(obj.msg=='Password change Successfully')
				{
					toastr.success(obj.msg);
					$('#password').val('');
					$('#cpassword').val('');
					setTimeout(function(){
						location.href='<?php echo base_url('login')?>'
					}, 3000);
				}
				//else if(obj.msg=='Mail send on your register email Please check'){
					// toastr.error(obj.msg);
				// }
			}
		});
	}
});
</script>


       