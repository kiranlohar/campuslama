<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
    <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
    <title>Hello Admission</title>

    <!-- Favicons-->
    <link rel="icon" href="<?php echo base_url('assets/images/favicon/favicon-32x32.png');?>" sizes="32x32">
    <!-- Favicons-->
    <link rel="apple-touch-icon-precomposed" href="images/favicon/apple-touch-icon-152x152.png">
    <!-- For iPhone -->
    <meta name="msapplication-TileColor" content="#00bcd4">
    <meta name="msapplication-TileImage" content="<?php echo base_url('assets/images/favicon/mstile-144x144.png');?>">
    <!-- For Windows Phone -->


    <!-- New Css Add--> 
	<link href="css/mycustom.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo base_url('assets//css/ion.rangeSlider.css');?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets//css/ion.rangeSlider.skinHTML5.css');?>" />
	
	
    <link href="<?php echo base_url('assets//css/main.css');?>" type="text/css" rel="stylesheet" />
   
	 <!-- CORE CSS--> 
    <link href="<?php echo base_url('assets/css/animate.min.css');?>" type="text/css" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/fadeinrightfinal.css');?>" type="text/css" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/materialize.min.css');?>" type="text/css" rel="stylesheet" media="screen,projection">
	<link href="<?php echo base_url('assets/css/style.min.css');?>" type="text/css" rel="stylesheet" media="screen,projection">
    <!-- Custome CSS-->    
    <link href="<?php echo base_url('assets/css/custom/custom.min.css');?>" type="text/css" rel="stylesheet" media="screen,projection">
	
    <link href="<?php echo base_url('assets/css/layouts/page-center.css');?>" type="text/css" rel="stylesheet" media="screen,projection">

    <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
    <link href="<?php echo base_url('assets/js/plugins/perfect-scrollbar/perfect-scrollbar.css');?>" type="text/css" rel="stylesheet');?>" media="screen,projection">
    <link href="<?php echo base_url('assets/js/plugins/jvectormap/jquery-jvectormap.css');?>" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="<?php echo base_url('assets/js/plugins/chartist-js/chartist.min.css');?>" type="text/css" rel="stylesheet" media="screen,projection">
	
	

</head>

<body class="cyan">
  <!-- Start Page Loading -->
  <div id="loader-wrapper">
      <div id="loader"></div>        
      <div class="loader-section section-left"></div>
      <div class="loader-section section-right"></div>
  </div>
  <!-- End Page Loading -->



  <div id="login-page" class="row">
    <div class="col s12 z-depth-4 card-panel">
      <form class="login-form">
        <div class="row">
          <div class="input-field col s12 center">
            <h4>Forgot Password</h4>
            <p class="center">You can reset your password</p>
          </div>
        </div>
        <div class="row margin">
          <div class="input-field col s12">
            <i class="mdi-social-person-outline prefix"></i>
            <input id="email" type="text">
            <label for="email" class="center-align">Email Id.</label>
          </div>
        </div>
        <div class="row">
          <div class="input-field col s12">
            <a href="index-2.html" class="btn waves-effect waves-light col s12">Reset Password</a>
          </div>
          <div class="input-field col s12">
            <p class="margin sign-up"><a href="<?php echo base_url('login/index'); ?>">Login</a> <a href="<?php echo base_url('login/register'); ?>" class="right">Register</a></p>
          </div>
        </div>
      </form>
    </div>
  </div>


<script src="<?php echo base_url('assets/js/plugins/jquery-1.11.2.min.js');?>"></script>
	<script src="<?php echo base_url('assets/js/ion.rangeslider.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/materialize.min.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/plugins/perfect-scrollbar/perfect-scrollbar.min.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/plugins/sparkline/jquery.sparkline.min.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/plugins/sparkline/sparkline-script.js');?>"></script> 
    <script type="text/javascript" src="<?php echo base_url('assets/js/plugins.min.js');?>"></script>
	<script src="<?php echo base_url('assets/js/modernizr.min.js');?>"></script>
</body>
</html>