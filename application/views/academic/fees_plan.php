<style>
.instam{
	display:none;
}
#add_fees_form{
	display:none;
}
</style>
<section id="content">
	<div class="row" id="side-menu-four-div" style="margin-top: -6.8px;">
		<div class="Space30 col l12 m12 s12"></div>
		<h4 style="text-align:center;">Fees Plan</h4>	
		<div class="col s12 m12 l12">	
			<div class="card">
				<a id="add_fees" class="waves-effect waves-light btn btn-form-submit Searchbtn center-align">Add Fees Plan</a>
				<div class="academy-card-one-div" id="add_fees_form">
					<form action="<?php echo base_url('academic/fees_plan'); ?>" method="post">
						<div class="row">
						  <div class="col s12 m12 l6 card">
							<div class="col s12 m12 l12 Space30"></div>	
								  <div class="row">
									<div class="col s12 m3 l3">
										<label class="NameLable">Plan Name</label>
									</div>
									<div class="col s12 m8 l8 no-padding-origin">
										<input type="text" placeholder=" Eg. One Year Plan" name="plan_name" value="<?php echo isset($eduData->um_name) ? $eduData->um_name : '';?>" >
									</div>
								  </div>
								  <div class="row">
									<div class="col s12 m3 l3">
										<label class="NameLable">Plan Type</label>
									</div>
									<div class="col s12 m8 l8 no-padding-origin">
										<input name="plan_type" class="plan_type" type="radio" id="test1"  value="Lumpsum" checked="checked"/>
										<label for="test1">Lumpsum</label>
										<input name="plan_type" class="plan_type" type="radio" id="test2"  value="Installment"/>
										<label for="test2">Installment</label>
									</div>
								  </div>
								  <div class="row instam">
									<div class="col s12 m3 l3">
										<label class="NameLable">No.of Installment</label>
									</div>
									<div class="col s12 m8 l8 no-padding-origin">
										<select class="browser-default" name="num_install" id="num_install">
										<option value="" disabled selected>Select No.of Installment</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
										<option value="6">6</option>
										<option value="7">7</option>
										<option value="8">8</option>
										</select>
									</div>
								 </div>
								 <div class="row instam" id="installDiv">
									
								 </div>
								 <div class="row lampm" id="lumpsumDiv">
									<div class="col s12 m3 l3">
										<label class="NameLable">Lumpsum Amount</label>
									</div>
									<div class="col s12 m8 l8 no-padding-origin">
										<input type="text" name="lumpsum_amount" >
									</div>
								 </div>
								 <div class="row">
									<button type="submit" class="waves-effect waves-light btn btn-form-submit Searchbtn center-align">Add</button>
								 </div>
						  </div>
						</div>
					</form>
				</div>
				<div class="col s12 m12 l12">
					<div id="responsive-table">
					  <div class="row">
						<div class="col s12 l12 m12">
							<table class="responsive-table">
								<thead>
									<tr>
										<th class="upm-table-head-bg center-align">Fees Plan Name</th>
										<th class="upm-table-head-bg center-align">Fees Plan type</th>
										<th class="upm-table-head-bg center-align">Amount</th>
										<th class="upm-table-head-bg center-align">Action</th>
									</tr>
								</thead>
								<tbody>
									<?php if($feesdata){ foreach($feesdata as $feedata){ ?>
									<tr>
									  <td class="upm-table-content-bg center-align"><?php echo $feedata->ufeem_plan_name; ?></td>
									  <td class="upm-table-content-bg center-align"><?php echo $feedata->ufeem_plan_type; ?></td>
									  <td class="upm-table-content-bg center-align"><?php $fee_amount = explode(',',$feedata->ufeem_amount); 
									  echo array_sum($fee_amount);
									  ?>
									  </td>
									  <td class="upm-table-content-bg center-align">
									  <a href="<?php echo base_url('academic/edit_fees_plan/'.$feedata->ufeem_id); ?>"><img class="sideIcon cust-cursor" src="<?php echo base_url('assets/img/edit-icon-01.png'); ?>"/></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									  <a href="<?php echo base_url('academic/edit_program_manager/'.$feedata->ufeem_id); ?>"><img class="sideIcon cust-cursor" src="<?php echo base_url('assets/img/delete-icon-02.png'); ?>"/></a>
									  </td>
									</tr>
									<?php } } ?>
								</tbody>
							</table>
						</div>
					  </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<script type="text/javascript" src="<?php echo base_url('assets/js/plugins/jquery-1.11.2.min.js');?>"></script>
<script>
$('#add_fees').click(function(){
	$('#add_fees_form').slideToggle( "slow" );
});

$("#num_install").change(function(){
	var ninsta = $(this).val();
	var instaHtml = '';
	$('#installDiv').empty();
	 for(var i=0;i<(ninsta);i++){
		instaHtml += '<div class="col s12 m3 l3"><label class="NameLable">Installment '+(i+1)+'</label></div><div class="col s12 m8 l8 no-padding-origin"><input type="text" name="install_amount[]" ></div>';
	 }
	 $('#installDiv').append(instaHtml);
});

$(".plan_type").change(function(){
	
	var isPlanType = $(this).val();
	
	if(isPlanType == 'Installment'){
		$('.instam').show();
		$('.lampm').hide();
	}else{
		$('.instam').hide();
		$('.lampm').show();
		$('#installDiv').empty();
		$('#num_install').prop('selectedIndex',0);
	}
});
</script>