<style>
#add_program_form{
	display:none;
}
.select-dropdown {
     height: auto !important; 
}
.select-wrapper input.select-dropdown{
	line-height: 2rem;
}
.publish-approve{
	background-color: #38E98F !important;
    color: #ffffff !important;
    padding: 5px;
}
.publish-pending{
	background-color: #e4b331 !important;
    color: #ffffff !important;
    padding: 5px;
}
.publish-reject{
	background-color: #e93853 !important;
    color: #ffffff !important;
    padding: 5px;
}
</style>
<section id="content">
<div class="row"  id="side-menu-five-div">
	<div class="col s12 m12 l12" style="margin-top: -6.8px;">
		<div class="card">		
			<div class="">		
<?php $userdata = $this->session->userdata('logged_in'); 
//print_r($userdata['name']);
?>			
							  <!-- card Section Start-->
				<div id="card-stats" class="seaction">
					<div class="row">
						<div class="col s12 m8 l8 offset-l3 offset-m4">
							<?php if(empty($university)){ ?>
								<div id="card-alert" class="card-alert-facility red" ><div class="card-content white-text"> <p>Sorry..! Please Update Academic Profile First</p></div><button type="button" class="close white-text" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button></div>
							<?php } ?>
							<div class="row row-margin-bottom">
								<div class="Space30 col l12 m12 s12"></div>
								<div class="col s12 m12 l12">
								<h4 class="admission-form-heading-margin">Program Manager</h4>
								</div>
								<div class="col s12 m3 l3"></div>	
							</div>	
						</div>
						<div class="Space30 col l12 m12 s12"></div>
						<div class="col l12 m12 s12">
							<div class="col l12 m12 s12">
							<?php if(!empty($university)){ ?>
							<a id="add_program" class="waves-effect waves-light btn btn-form-submit Searchbtn center-align">Add New Program</a>
							<?php } ?>
							</div>
							<div class="col s12 m12 l12 Space10"></div>
							<div class="row col l12 m12 s12" id="add_program_form">
								<form id="addform" action="#" method="post">
								<div class="card col l5 m5 s12" style="width:70%">
									<div class="col s12 m12 l12 Space10"></div>
									<div class="col s12 m3 l3">
										<label class="NameLable">Type</label>
									</div>
									<div class="col s12 m8 l8 no-padding-origin">
										<select id="program_type" name="program_type" class="browser-default Ustory select-dropdown">
											
											<?php foreach($type as $stream1){ ?>
											<option value="<?php echo $stream1->id; ?>"><?php echo $stream1->name; ?></option>
											<?php } ?>
										</select>
									</div>
									<div class="col s12 m12 l12 Space10"></div>
									<div class="col s12 m3 l3">
												<label class="NameLable">Eligibility</label>
											</div>
											<div class="col s12 m8 l8">
									  <div class="input-field col s12"  style="margin-left: -21px;width: 362px;">
										<select name="eligibility[]" multiple  class="tet">
										 <option value="" disabled selected>Select Education</option>
											<?php foreach($eligiblity as $fee){ ?>
											<option value="<?php echo $fee->id; ?>"><?php echo $fee->name; ?> </option>
											<?php } ?>
										</select>
									  </div>
									</div>
									<div class="col s12 m12 l12 Space10"></div>
									<div class="col s12 m3 l3">
										<label class="NameLable">Stream</label>
									</div>
									<div class="col s12 m8 l8 no-padding-origin">
										<select id="stream" name="stream" class="browser-default Ustory select-dropdown">
											<option value="" disabled selected>Choose Stream</option>
											<?php foreach($streams as $stream){ ?>
											<option value="<?php echo $stream->id; ?>"><?php echo $stream->name; ?></option>
											<?php } ?>
										</select>
									</div>
									<div class="col s12 m12 l12 Space10"></div>
									<div class="col s12 m3 l3">
										<label class="NameLable">Program</label>
									</div>
									<div class="col s12 m8 l8 no-padding-origin">
										<select id="program" name="program" class="browser-default Ustory select-dropdown">
											<option value="" disabled selected>Choose Program</option>
										  </select>
									</div>
									<div class="col s12 m12 l12 Space10"></div>
									<div class="col s12 m3 l3">
										<label class="NameLable">Specilization</label>
									</div>
									<div class="col s12 m8 l8 no-padding-origin">
										<select id="course_spe" name="course_spe" class="browser-default Ustory select-dropdown">
											<option value="" disabled selected>Choose Spe</option>
										  </select>
									</div>
									<!-- <div class="col s12 m12 l12 Space10"></div>
									<div class="col s12 m3 l3">
										<label class="NameLable">Fees Plan</label>
									</div>
									
									<div class="col s12 m8 l8">
									  <div class="input-field col s12" style="margin-left: -22px;">
										<select name="fees[]" multiple>
										 <option value="" disabled selected>Choose Fees Plan</option>
											<?php foreach($fees as $fee){ ?>
											<option value="<?php echo $fee->ufeem_id; ?>"><?php echo $fee->ufeem_plan_name; ?> (<?php echo $fee->ufeem_plan_type; ?> - <?php echo $fee->ufeem_amount; ?> )</option>
											<?php } ?>
										</select>
									  </div>
									</div> -->
             <div class="col s12 m12 l12 Space10"></div>
									<div class="col s12 m3 l3">
										<label class="NameLable">About</label>
									</div>
									
									<div class="col s12 m8 l8">
									  <div class="input-field col s12" style="margin-left: -22px;">
										<textarea name="about" id="about"></textarea>
									  </div>
									</div>
									<div class="col s12 m12 l12 Space10"></div>
									<div class="col s12 m3 l3">
										<label class="NameLable">Objective</label>
									</div>
									
									<div class="col s12 m8 l8">
									  <div class="input-field col s12" style="margin-left: -22px;">
										<textarea name="objective" id="objective"></textarea>
									  </div>
									</div>
             
									<div class="col s12 m12 l12 Space10"></div>
									<div class="col s12 m3 l3">
										<label class="NameLable">Program Duration</label>
									</div>
									<div class="col s12 m4 l4 no-padding-origin">
										<input type="text" name="duration">
									</div>
									<div class="col s12 m4 l4 no-padding-origin">
										<select id="dur-unit" name="dur-unit" class="browser-default Ustory select-dropdown" >
											<option value="" disabled selected>Choose Year/Month</option>
											<option value="Months">Months</option>
											<option value="Years">Years</option>
										</select>
									</div>
									<div class="col s12 m12 l12 Space10"></div>
									<div class="col s12 m3 l3">
										<label class="NameLable">Fees Plan</label>
									</div>
									<div class="col s12 m4 l4 no-padding-origin">
										<input type="text" name="fees">
									</div>
									<div class="col s12 m4 l4 no-padding-origin">
										<select id="dur-unit" name="dur-unit-1" class="browser-default Ustory select-dropdown" >
											<option value="" disabled selected>Choose Plan</option>
											<option value="Installment">Installment</option>
											<option value="Lumpsum">Lumpsum</option>
										
										</select>
									</div>
									<div class="col s12 m12 l12 Space10"></div>
									<div class="col s12 m3 l3">
												<label class="NameLable" ><b style="color:white">gfd</b></label>
											</div>
											<div class="col s12 m8 l8 no-padding-origin" style="margin-bottom: 15px">
												<a href="#" id="addinfo">Assingne Manage Program</a>
											</div>
									<div id="admin">
											<div class="col s12 m3 l3">
												<label class="NameLable">Administrator</label>
											</div>
											<div class="col s12 m8 l8 no-padding-origin">
												<input type="text" name="admin_name" id="admin_name_1"  value="<?php echo $userdata['name'];?>" maxlength="30">
											</div>
											<div class="col s12 m3 l3">
												<label class="NameLable">Email Id</label>
											</div>
											<div class="col s12 m8 l8 no-padding-origin">
												<input type="text" name="admin_email" id="admin_email_1" value="<?php echo $userdata['email'];?>"  maxlength="50">
											</div>
									
									<div class="col s12 m3 l3">
										<label class="NameLable">Mobile</label>
									</div>
									
									<div class="col s12 m8 l8 no-padding-origin">
										<input type="text" name="admin_mobile" id="admin_mobile_1" maxlength="12">
									</div>
									</div>
									<div class="col s12 m8 l8 offset-l3 offset-m3 no-padding-origin">
										<input type="button" value="Save" class="waves-effect waves-light btn btn-form-submit Searchbtn center-align up-save-btn" id="save_program"/>
										<input type="button" value="Cancel" class="waves-effect waves-light btn btn-form-submit Searchbtn center-align up-save-btn" id="add_cal"/>
									</div>
									<div class="col s12 m12 l12 Space10"></div>
								</div>
								</form>
								<div class="col l7 m7 s12"></div>
							</div>

<!--Start edit-->
	<div class="row col l12 m12 s12" id="edit_program_form">
								<form id="editform" action="<?php //echo base_url('academic/edit_program_manager')?>#" method="post">
								<div class="card col l5 m5 s12" style="width:70%">
									<div class="col s12 m12 l12 Space10"></div>
									<div class="col s12 m3 l3">
										<label class="NameLable">Type</label>
									</div>
									<div class="col s12 m8 l8 no-padding-origin">
										<select id="program_type" name="program_type" class="browser-default Ustory select-dropdown">
										<option value="" >Choose Stream</option>
											
										<?php foreach($type as $stream1){ ?>
											<option value="<?php echo $stream1->id; ?>"><?php echo $stream1->name; ?></option>
											<?php } ?>
										</select>
									</div>
									<div class="col s12 m12 l12 Space10"></div>

									<div class="col s12 m3 l3">
												<label class="NameLable">Eligibility</label>
											</div>
											<div class="col s12 m8 l8">
									  <div class="input-field col s12"  style="margin-left: -21px;width: 362px;">
										<select name="eligibility[]" multiple  id="test" class="edu">
										 <option value="" disabled selected>Select Education</option>
											<?php foreach($eligiblity as $eli){ ?>
											<option value="<?php echo $eli->id; ?>"><?php echo $eli->name; ?> </option>
											<?php } ?>
										</select>
									  </div>
									</div>
									<div class="col s12 m12 l12 Space10"></div>
									<div class="col s12 m3 l3">
										<label class="NameLable">Stream</label>
									</div>
									<div class="col s12 m8 l8 no-padding-origin">
										<select id="stream_edit" name="stream" class="browser-default Ustory select-dropdown">
											<option value="" >Choose Stream</option>
											
											<?php foreach($streams as $stream){ ?>
											<option value="<?php echo $stream->id; ?>"><?php echo $stream->name; ?></option>
											<?php } ?>
										</select>
									</div>
									<div class="col s12 m12 l12 Space10"></div>
									<div class="col s12 m3 l3">
										<label class="NameLable">Program</label>
									</div>
									<div class="col s12 m8 l8 no-padding-origin">
										<select id="program_edit" name="program" class="browser-default Ustory select-dropdown">
											<option value="" disabled selected>Choose Program</option>
											<?php foreach($spe as $program){ ?>
												<option value="<?php echo $program->id; ?>"><?php echo $program->name; ?></option>
											<?php } ?>
										</select>
									</div>
									<div class="col s12 m12 l12 Space10"></div>
									<div class="col s12 m3 l3">
										<label class="NameLable">Specilization</label>
									</div>
									<div class="col s12 m8 l8 no-padding-origin">
										<select id="course_edit" name="course_spe" class="browser-default Ustory select-dropdown">
											<option value="" disabled selected>Choose Specilization</option>
											<?php foreach($course_spe as $program){ ?>
												<option value="<?php echo $program->id; ?>"><?php echo $program->name; ?></option>
											<?php } ?>
										</select>
									</div>
								<!-- 	<div class="col s12 m12 l12 Space10"></div>
									<div class="col s12 m3 l3">
										<label class="NameLable">Fees Plan</label>
									</div>
									
									<div class="col s12 m8 l8">
									  <div class="input-field col s12" style="margin-left: -22px;">
										<select name="fees[]" multiple class="fees">
										 <option value=""  selected>Choose Fees Plan</option>
											<?php foreach($fees as $fee){ ?>
											<option  value="<?php echo $fee->ufeem_id; ?>"><?php echo $fee->ufeem_plan_name; ?> </option>
											<?php } ?>
										</select>
									  </div>
									</div> -->
             							<div class="col s12 m12 l12 Space10"></div>
									<div class="col s12 m3 l3">
										<label class="NameLable">About</label>
									</div>
									
									<div class="col s12 m8 l8">
									  <div class="input-field col s12" style="margin-left: -22px;">
										<textarea name="about_edit" id="about_edit"></textarea>
									  </div>
									</div>
									<div class="col s12 m12 l12 Space10"></div>
									<div class="col s12 m3 l3">
										<label class="NameLable">Objective</label>
									</div>
									
									<div class="col s12 m8 l8">
									  <div class="input-field col s12" style="margin-left: -22px;">
										<textarea name="objective_edit" id="objective_edit"></textarea>
									  </div>
									</div>
									<div class="col s12 m12 l12 Space10"></div>
									<div class="col s12 m3 l3">
										<label class="NameLable">Program Duration</label>
									</div>
									<?php //$dur = explode(' ',$programManager->uc_course_length);?>
									<div class="col s12 m4 l4 no-padding-origin">
										<input type="text" name="duration" id="duration_edit" value="<?php //echo trim($dur[0]); ?>">
									</div>
									<div class="col s12 m4 l4 no-padding-origin">
										<select id="dur-unit" name="dur-unit" class="browser-default Ustory select-dropdown" >
											<option value="" disabled selected>Choose Year/Month</option>
											<option <?php //echo ($dur[1] == 'Months')? 'selected' : ''; ?> value="Months">Months</option>
											<option <?php //echo ($dur[1] =='Years')? 'selected' : ''; ?> value="Years">Years</option>
										</select>
									</div>
									<div class="col s12 m12 l12 Space10"></div>

									<div class="col s12 m3 l3">
										<label class="NameLable">Fees Plan</label>
									</div>
									<div class="col s12 m4 l4 no-padding-origin">
										<input type="text" name="fees" id="fees">
									</div>
									<div class="col s12 m4 l4 no-padding-origin">
										<select name="dur-unit-1" id="fees_edit" class="browser-default Ustory select-dropdown" >
											<option value="" disabled selected>Choose Plan</option>
											<option value="Installment">Installment</option>
											<option value="Lumpsum">Lumpsum</option>
										
										</select>
									</div>
									<div class="col s12 m12 l12 Space10"></div>
									<div class="col s12 m3 l3">
										<label class="NameLable">Administrator</label>
									</div>
									<div class="col s12 m8 l8 no-padding-origin">
										<input type="text" name="admin_name"  id="admin_name" value="">
									</div>
									<div class="col s12 m3 l3">
										<label class="NameLable">Email Id</label>
									</div>
									<div class="col s12 m8 l8 no-padding-origin">
										<input type="text" name="admin_email" id="admin_email" value="">
									</div>
									<div class="col s12 m3 l3">
										<label class="NameLable">Mobile</label>
									</div>
									<div class="col s12 m8 l8 no-padding-origin">
										<input type="text" name="admin_mobile" id="admin_mobile" value="">
									</div>
									<input type="hidden" name="upmid"  id="upmid" value="">
									<input type="hidden" name="upmadminid" id="upmadminid"  value="">
									<input type="hidden" name="uc_id" id="uc_id" value="">
									<input type="hidden" name="userId" id="userId" value="<?php //echo $programManager->upa_user_id; ?>">
									<div class="col s12 m8 l8 offset-l3 offset-m3 no-padding-origin">
										<input type="button" value="Save" class="waves-effect waves-light btn btn-form-submit Searchbtn center-align up-save-btn" id="update_program"/>
										<input type="button" value="Cancel" class="waves-effect waves-light btn btn-form-submit Searchbtn center-align up-save-btn" id="edi_cal"/>
									</div>
									<div class="col s12 m12 l12 Space10"></div>
								</div>
								</form>
								<div class="col l7 m7 s12"></div>
							</div>
<!--End edit-->							
			<div class="col s12 m12 l12">
					<div id="responsive-table">
					  <div class="row">
						<div class="col s12 l12 m12">
						  <table class="responsive-table">
							  <thead>
									<tr>
										<th class="upm-table-head-bg center-align">Sr No</th>
										<th class="upm-table-head-bg center-align">Administrator</th>
										<th class="upm-table-head-bg center-align">Email</th>
										<!-- <th class="upm-table-head-bg center-align">Mobile</th> -->
										<th class="upm-table-head-bg center-align">Stream</th>
										<th class="upm-table-head-bg center-align">Program</th>
										<th class="upm-table-head-bg center-align">Spec</th>
										<th class="upm-table-head-bg center-align">Eligibility</th>
										<th class="upm-table-head-bg center-align">Fees</th>
										<th class="upm-table-head-bg center-align">Duration</th>
										<th class="upm-table-head-bg center-align">Type</th>
										<th class="upm-table-head-bg center-align">Status</th>
										<th class="upm-table-head-bg center-align">Action</th>
									</tr>
									<tr class="Space5"></tr>
							  </thead>
							<tbody id="program_data">
								
						  </tbody>
						</table>
					</div>
					</div>
				</div>
			</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>	
</div>

</section>
<script type="text/javascript" src="<?php echo base_url('assets/js/plugins/jquery-1.11.2.min.js');?>"></script>
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/1.3.1/js/toastr.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>tinymce/js/tinymce/jquery.tinymce.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>tinymce/js/tinymce/tinymce.js"></script>
<script type="text/javascript">
tinymce.init({
  selector: "textarea",
  height: 200,
  width: 600,
  plugins: [
    "advlist autolink autosave link image lists charmap print preview hr anchor pagebreak",
    "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
    "table contextmenu directionality emoticons template textcolor paste fullpage textcolor colorpicker textpattern"
  ],

  toolbar1: "newdocument fullpage | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
  toolbar2: "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image media code | insertdatetime preview | forecolor backcolor",
  toolbar3: "table | hr removeformat | subscript superscript | charmap emoticons | print fullscreen | ltr rtl | visualchars visualblocks nonbreaking template pagebreak restoredraft",
  content_css: [
    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tinymce.com/css/codepen.min.css'],

  menubar: false
  
});

</script>
<script type="text/javascript">
$(document).ready(function(){
	// $('.mce-notification-inner').css('display','none');
	// $('.mce-widget').CSS('display','none');
	
});
</script>
<script>

get_program()
function get_program()
{
$.ajax({
		type: "POST",
		url: "<?php echo base_url('academic/get_ajax'); ?>",
		//data:data,
		//dataType: "json",
		success: function(response){
			
			var obj = JSON.parse(response);
			console.log(obj);
			 var html='';
			var coutn=1;
				$.each(obj, function(ind,val){
					var spe='';
					if(val.course_spe==null){
						spe='-';
					}else if(val.course_spe!=null){
						spe=val.course_spe;
					}
						var msg='';
						if(val.uc_is_approved==1)
						{
							msg+='<span class="publish-approve switch-tab3 cust-cursor">Approved</span>';
						}
						else if(val.uc_is_approved==0){
							msg+='<span class="publish-pending switch-tab3 cust-cursor">Pending</span>';
						}
						else
						{
							msg+='<span class="publish-reject switch-tab3 cust-cursor">Rejected</span>';
						}
						var msg1='';
						
						if(val.uc_is_approved!=2)
						{
							msg1+='<a href="#" onclick="del_pro('+val.uc_id+')"><img class="sideIcon cust-cursor" src="<?php echo base_url('assets/img/delete-icon-02.png'); ?>"/></a>';
						}else
						{
							msg1+='<a href="#" onclick="active_pro('+val.uc_id+')"><img class="sideIcon cust-cursor" src="<?php echo base_url('assets/images/icon-03.png'); ?>"/></a>';
						}
						var eli='';
						var eli1='';
						var eli2='';
						var eli3='';
						var str = val.uc_eligibility;
							var temp = new Array();
							// this will return an array with strings "1", "2", etc.
							temp = str.split(",");
							for(i=0;i<temp.length;i++){
								if(temp[i]==1)
								{
									eli='ssc ,';
								}else if(temp[i]==2){
									eli1='Hsc ,';
								}else if(temp[i]==3){
									eli2='Diploma ,';
								}
								else if(temp[i]==4){
									eli3='Graduation,';
								}
								//console.log(temp[i]);
							}
													
						html+='<tr>'+
						'<td class="upm-table-content-bg center-align">'+coutn+++'</td>'+
						'<td class="upm-table-content-bg center-align">'+val.upm_admin_name+'</td>'+
						'<td class="upm-table-content-bg center-align">'+val.upm_admin_email+'</td>'+
						// '<td class="upm-table-content-bg center-align">'+val.upm_admin_mobile+'</td>'+
						'<td class="upm-table-content-bg center-align">'+val.name+'</td>'+
						'<td class="upm-table-content-bg center-align">'+val.spe+'</td>'+
						'<td class="upm-table-content-bg center-align">'+spe+'</td>'+
						
						'<td class="upm-table-content-bg center-align">'+eli+''+eli1+''+eli2+''+eli3+'</td>'+
						'<td class="upm-table-content-bg center-align">'+val.uc_fees+'</td>'+
						'<td class="upm-table-content-bg center-align">'+val.uc_course_length+'</td>'+
						'<td class="upm-table-content-bg center-align">'+val.typename+'</td>'+
						'<td class="upm-table-content-bg center-align">'+msg+'</td>'+
						'<td class="upm-table-content-bg center-align"> '+
						'<a href="#" onclick="edit('+val.uc_id+')"><img class="sideIcon cust-cursor" src="<?php echo base_url('assets/img/edit-icon-01.png'); ?>"/></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+
					    msg1+
						'</td></tr>';
						
						
				})
				$('#program_data').html(html);
			
			
		}
	});
	
}
$('#stream').change(function(){
	var streamid = $(this).val();
	//alert(streamid);
	$('#program').empty();
	$.ajax({
		type: "POST",
		url: "<?php echo base_url('academic/stream_program'); ?>",
		data:{streamid:streamid},
		success: function(response){
			var res = JSON.parse(response);
			$('#program').append('<option value="" disabled selected>Choose Program</option>');
			$.each(res, function (i, itm) {
				$('#program').append($('<option>', { 
					value: itm.id,
					text : itm.name 
				}));
			});
		}
	});
});

$('#program_edit').change(function(){
	var streamid = $(this).val();
	//alert(streamid);
	$('#course_edit').empty();
	$.ajax({
		type: "POST",
		url: "<?php echo base_url('academic/stream_program_spe'); ?>",
		data:{streamid:streamid},
		success: function(response){
			var res = JSON.parse(response);
			console.log(res);
			$('#course_edit').append('<option value="" disabled selected>Choose course spe</option>');
			$.each(res, function (i, itm) {
				$('#course_edit').append($('<option>', { 
					value: itm.id,
					text : itm.name 
				}));
			});
		}
	});
});

//specialization

$('#program').change(function(){
	var streamid = $(this).val();
	//alert(streamid);
	$('#course_spe').empty();
	$.ajax({
		type: "POST",
		url: "<?php echo base_url('academic/stream_program_spe'); ?>",
		data:{streamid:streamid},
		success: function(response){
			var res = JSON.parse(response);
			console.log(res);
			$('#course_spe').append('<option value="" disabled selected>Choose course spe</option>');
			$.each(res, function (i, itm) {
				$('#course_spe').append($('<option>', { 
					value: itm.id,
					text : itm.name 
				}));
			});
		}
	});
});



$('#admin').hide();
$('#addinfo').text("Assingne Manage Program");
$('#addinfo').click(function(){
	 var text = $(this).text();
	 console.log(text);
        if (text === "Assingne Manage Program") {
            $(this).text("Self Manage Program");
            	$('#admin').toggle();
        } else {
            $(this).text("Assingne Manage Program");
            	$('#admin').hide();
        }
	//$('#addinfo').text("self assign");
	//$('#admin').toggle();
});


$('#stream_edit').change(function(){
	var streamid = $(this).val();
	//alert(streamid);
	$('#program_edit').empty();
	$.ajax({
		type: "POST",
		url: "<?php echo base_url('academic/stream_program'); ?>",
		data:{streamid:streamid},
		success: function(response){
			var res = JSON.parse(response);
			$('#program_edit').append('<option value="" disabled selected>Choose Program</option>');
			$.each(res, function (i, itm) {
				$('#program_edit').append($('<option>', { 
					value: itm.id,
					text : itm.name 
				}));
			});
		}
	});
});

function edit(id)
{
	 $('select').material_select();
	$.ajax({
		type: "POST",
		url: "<?php echo base_url('academic/edit_pro'); ?>",
		data:{'id':id},
		success: function(response){
			var res = JSON.parse(response);
			console.log(res);
			$('#edit_program_form').show( "slow" );
			$('#add_program_form').hide( "slow" );
			
			$.each(res, function (i, itm) {
				$('#admin_name').val(itm.upm_admin_name);
				$('#admin_email').val(itm.upm_admin_email);
				$('#admin_mobile').val(itm.upm_admin_mobile);
				$('#upmid').val(itm.upa_upm_id);
				$('#upmadminid').val(itm.upa_id);
				$('#uc_id').val(itm.uc_id);
				$('#about').val(itm.uc_info);
				$('#objective').val(itm.uc_objective);
				$('#userId').val(itm.upa_user_id);
				var arr = itm.uc_course_length.split(' ');
				var arr_fees = itm.uc_fees.split(' ');
				$('#duration_edit').val(arr[0]);
				$('#fees').val(arr_fees[0]);
				tinymce.get('about_edit').setContent(itm.uc_info);

				tinymce.get('objective_edit').setContent(itm.uc_objective);
				//$('#dur-unit').text(arr[1]);
			//	console.log(itm.uc_course_type);
				$("#course_edit option[value='" + itm.uc_spe_id + "']").attr("selected","selected");
				$("#stream_edit option[value='" + itm.id + "']").attr("selected","selected");
				$("#program_edit option[value='" + itm.uc_com_id + "']").attr("selected","selected");
				$("#dur-unit option[value='" + arr[1] + "']").attr("selected","selected");
				$("#fees_edit option[value='" + arr_fees[1] + "']").attr("selected","selected");
				$("#program_type option[value='" + itm.uc_course_type + "']").attr("selected","selected");
				$(".fees option[value='" + itm.uc_fees + "']").attr("selected","selected");
					//$(".edu  select option[value='" + val + "']").attr("selected",true);
					var temp=itm.uc_eligibility.split(',');
					
				var index = 1;
				    $.each(temp,function(i,val) {
				    		// $('select').material_select();
					$(".edu  select option[value='" + val + "']").attr("selected",true);
						//$(".edu  select option[value='" + val + "']").eq(val).removeAttr('selected');
					$('.edu li span input[type="checkbox"]').eq(val).attr("checked",true);
					//$('.edu').find('select option[value="'+val+'"]').prop('selected', true);
//$('').material_select();
					// $('.edu li').eq(val).addClass('active');
					// $('.edu li').eq(val).addClass('selected');
				       index++;
				    });
			}); 
		}
	}); 
}

function del_pro(id)
{
	//alert(id);
	 $.ajax({
		type: "POST",
		url: "<?php echo base_url('academic/stream_program_delete'); ?>",
		data:{'id':id},
		success: function(response){
			var res = JSON.parse(response);
			console.log(res);
			get_program();
			toastr.success('Deleted Successfully');
		}
	});
}
function active_pro(id)
{
	//alert(id);
	 $.ajax({
		type: "POST",
		url: "<?php echo base_url('academic/stream_program_active'); ?>",
		data:{'id':id},
		success: function(response){
			var res = JSON.parse(response);
			console.log(res);
				get_program();
			toastr.success('Active Successfully');
		}
	});
}
$('#admin_name_1').change(function(){
			var regex = /^[a-zA-Z ]*$/;
        if (regex.test($('#admin_name_1').val())) {
        } else {
            toastr.error('Enter Only Alphates');
			$('#admin_name_1').focus();
			return false;
        }
})

$('#admin_name').change(function(){
			var regex = /^[a-zA-Z ]*$/;
        if (regex.test($('#admin_name').val())) {
        } else {
            toastr.error('Enter Only Alphates');
			$('#admin_name').focus();
			return false;
        }
})




$('#save_program').click(function(){
	// var data = $('form#addform').serialize();
	var temp =tinymce.get('about').getContent();
	var objective =tinymce.get('objective').getContent();
	
	var data = $('form#addform').serialize()+'&about='+temp+'&objective='+objective;
	//console.log(data);
	$.ajax({
		type: "POST",
		url: "<?php echo base_url('academic/add_pro')?>",
		data:data,
		success: function(response){
			console.log(res);
			var res = JSON.parse(response);
			//console.log(res);
			if(res)
			{
				toastr.success("Program  Added Successfully");
				
					setTimeout(function(){
		 get_program();	
		 $('#add_program_form').hide( "slow" );
		  $('#addform')[0].reset();
		  location.reload();
		}, 3000);
			}
			
		}
	});
})
$('#test').change(function () {
      console.log($("#toto option:selected"));
    })

$('#update_program').click(function(){
		var temp =tinymce.get('about_edit').getContent();
	var objective =tinymce.get('objective_edit').getContent();
	var data = $('form#editform').serialize()+'&about='+temp+'&objective='+objective;
	// console.log(data);
	// return false;
	$.ajax({
		type: "POST",
		url: "<?php echo base_url('academic/edit_program_manager'); ?>",
		data:data,
		success: function(response){
			var res = JSON.parse(response);
			//console.log(res);
			if(res)
			{
				toastr.success("Program  Updated Successfully");
				
					setTimeout(function(){
		 get_program();	
		 $('#edit_program_form').hide( "slow" );
		  $('#editform')[0].reset();
		}, 3000);
			}
			
		}
	});
})
$('#edit_program_form').hide( "slow" );
$('#add_program').click(function(){
	$('#add_program_form').slideToggle( "slow" );
	$('#edit_program_form').hide( "slow" );
	
});
$('#cal_master_spe').click(function(){
	$('#add_program_form').hide( "slow" );
	$('#edit_program_form').hide( "slow" );
	
});
$('#edi_cal').click(function(){
	$('#add_program_form').hide( "slow" );
	$('#edit_program_form').hide( "slow" );
	
});
$('#add_cal').click(function(){
	$('#add_program_form').hide( "slow" );
	$('#edit_program_form').hide( "slow" );
	
});

</script>