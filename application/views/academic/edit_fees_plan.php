<style>
<?php if($feesdata->ufeem_plan_type != 'Installment'){ ?>
.instam{
	display:none;
}
<?php }else{ ?>
.lampm{
	display:none;
}
<?php } ?>
</style>
<section id="content">
	<div class="row" id="side-menu-four-div" style="margin-top: -6.8px;">
		<div class="Space30 col l12 m12 s12"></div>
		<h4 style="text-align:center;">Edit Fees Plan</h4>	
		<div class="col s12 m12 l12">	
			<div class="card">
				<div class="academy-card-one-div" >
					<form action="<?php echo base_url('academic/edit_fees_plan'); ?>" method="post">
						<div class="row">
						  <div class="col s12 m12 l6">
							<div class="col s12 m12 l12 Space30"></div>	
								  <div class="row">
									<div class="col s12 m3 l3">
										<label class="NameLable">Plan Name</label>
									</div>
									<div class="col s12 m8 l8 no-padding-origin">
										<input type="text" placeholder=" Eg. One Year Plan" name="plan_name" value="<?php echo isset($feesdata->ufeem_plan_name) ? $feesdata->ufeem_plan_name : '';?>" >
									</div>
								  </div>
								  <div class="row">
									<div class="col s12 m3 l3">
										<label class="NameLable">Plan Type</label>
									</div>
									<div class="col s12 m8 l8 no-padding-origin">
									  <?php if(isset($feesdata->ufeem_plan_type)){ ?>
									  <input name="plan_type" class="plan_type" type="radio" id="test1" <?php echo ($feesdata->ufeem_plan_type == 'Lumpsum') ? 'checked="checked"' : ''; ?> value="Lumpsum" />
									  <label for="test1">Lumpsum</label>
									  <input name="plan_type" class="plan_type" type="radio" id="test2" <?php echo ($feesdata->ufeem_plan_type == 'Installment') ? 'checked="checked"' : ''; ?> value="Installment"/>
									  <label for="test2">Installment</label>
									  <?php }else{ ?>
										<input name="plan_type" class="plan_type" type="radio" id="test1"  value="Lumpsum" checked="checked"/>
										<label for="test1">Lumpsum</label>
										<input name="plan_type" class="plan_type" type="radio" id="test2"  value="Installment"/>
										<label for="test2">Installment</label>
									  <?php } ?>
									</div>
								  </div>
								  <div class="row instam">
									<div class="col s12 m3 l3">
										<label class="NameLable">No.of Installment</label>
									</div>
									<div class="col s12 m8 l8 no-padding-origin">
										<select class="browser-default" name="num_install" id="num_install">
										<option value="" disabled selected>Select No.of Installment</option>
										<option <?php echo ($feesdata->ufeem_num_install == '2') ? 'selected' : ''; ?> value="2">2</option>
										<option <?php echo ($feesdata->ufeem_num_install == '3') ? 'selected' : ''; ?> value="3">3</option>
										<option <?php echo ($feesdata->ufeem_num_install == '4') ? 'selected' : ''; ?> value="4">4</option>
										<option <?php echo ($feesdata->ufeem_num_install == '5') ? 'selected' : ''; ?> value="5">5</option>
										<option <?php echo ($feesdata->ufeem_num_install == '6') ? 'selected' : ''; ?> value="6">6</option>
										<option <?php echo ($feesdata->ufeem_num_install == '7') ? 'selected' : ''; ?> value="7">7</option>
										<option <?php echo ($feesdata->ufeem_num_install == '8') ? 'selected' : ''; ?> value="8">8</option>
										</select>
									</div>
								 </div>
								 <div class="row instam" id="installDiv">
								 <?php 
								 if($feesdata->ufeem_num_install > 1){
								 $ufeem_amount = explode(',',$feesdata->ufeem_amount);
								 for($i=0;$i<($feesdata->ufeem_num_install);$i++){ ?>
									<div class="col s12 m3 l3">
										<label class="NameLable">Installment <?php echo $i+1; ?></label>
									</div>
									<div class="col s12 m8 l8 no-padding-origin">
										<input type="text" name="install_amount[]" value="<?php echo $ufeem_amount[$i]; ?>">
									</div>
								 <?php } 
								 } ?>
								 </div>
								 <div class="row lampm" id="lumpsumDiv">
									<div class="col s12 m3 l3">
										<label class="NameLable">Lumpsum Amount</label>
									</div>
									<div class="col s12 m8 l8 no-padding-origin">
										<input type="text" name="lumpsum_amount" value="<?php echo isset($feesdata->ufeem_amount) ? $feesdata->ufeem_amount : '';?>">
									</div>
								 </div>
								 <div class="row">
									<input type="hidden" name="ufee_id" value="<?php echo isset($feesdata->ufeem_id) ? $feesdata->ufeem_id : '';?>">
									<button type="submit" class="waves-effect waves-light btn btn-form-submit Searchbtn center-align">Update</button>
								 </div>
						  </div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
<script type="text/javascript" src="<?php echo base_url('assets/js/plugins/jquery-1.11.2.min.js');?>"></script>
<script>

$("#num_install").change(function(){
	var ninsta = $(this).val();
	var instaHtml = '';
	$('#installDiv').empty();
	 for(var i=0;i<(ninsta);i++){
		instaHtml += '<div class="col s12 m3 l3"><label class="NameLable">Installment '+(i+1)+'</label></div><div class="col s12 m8 l8 no-padding-origin"><input type="text" name="install_amount[]" ></div>';
	 }
	 $('#installDiv').append(instaHtml);
});

$(".plan_type").change(function(){
	
	var isPlanType = $(this).val();
	
	if(isPlanType == 'Installment'){
		$('.instam').show();
		$('.lampm').hide();
	}else{
		$('.instam').hide();
		$('.lampm').show();
		$('#installDiv').empty();
		$('#num_install').prop('selectedIndex',0);
	}
});
</script>