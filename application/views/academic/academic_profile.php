<style>
.media_remove {
    color: red;
}
.fture-div{
	width: 70%;
    text-align: center;
    padding: 5px;
}
.fture{
	cursor:pointer;
	text-align: center;	
}
.video_div{
	width: 37.5% !important;
	margin-bottom: 40px !important;
}
.vheading{
	color: #000;
    margin-top: 3px;
}
div#card-alert {
    position: absolute;
    z-index: 9999;
    right: 11px;
	width: 25%;
}
</style>
<section id="content">
	<div class="row" id="side-menu-four-div" style="margin-top: -6.8px;">
		<div class="col s12 m12 l12">	
			<div class="card">					
				<!-- card Section Start-->
				<div id="card-stats" class="seaction">
					<div class="row">
						<div class="col s12 m8 l8 offset-l2 offset-m2">
								<div class="row row-margin-bottom">
									<div class="Space30 col l12 m12 s12"></div>
									<div class="col s12 m12 l5">
									<div class="row">
										<div class="col s12 m6 l2">
										<img src="<?php echo base_url('assets/img/academy-profile-01.png'); ?>"/>
										</div>
										<div class="col s12 m6 l10">
											<h4>Academy Profile</h4>
										</div>
									</div>
									<!--<img src="img/academy-profile-01.png'); ?>"/>
									<h4 class="academy-profile-margin">Academy Profile</h4>-->
									</div>
									<?php 
								// 	$userdata = $this->session->userdata('logged_in');
								// 	//print_r($universityData);
								// 	$percentage =null;
								// 		$maximumPoints  = 100;
        // if ($userdata) {                 
        //      if($universityData->um_name!=""){
          
        //         	$uname= 3;
               
        //      }
        //      if($universityData->um_link!=""){
        //        $um_link = 3;
                 

        //      }
        //      if($universityData->um_mobile!=""){
        //         $um_mobile = 3;
              

        //      }
        //      if($universityData->ui_aboutus!=""){
        //        $ui_aboutus = 3;
            

        //      }
        //       if($universityData->ui_coverimage!=""){
        //         $ui_coverimage = 3;
              

        //      }
        //       if($universityData->ui_logoimage!=""){
        //         $ui_logoimage = 3;

        //      }
        //        if($universityData->ui_address!=""){
        //         $ui_address = 3;
               

        //      }
        //        if($universityData->ui_contact!=""){
        //         $ui_contact = 3;
              
        //      }
        //        if($universityData->email!=""){
        //         $email = 3;
              
        //      }
        //       if($universityData->ui_recognition!=""){
            
        //         	$ui_recognition= 3;
         
        //      }
             
        //      //}
             

        //      $percentage= ($uname+$um_link+$um_mobile+$ui_aboutus+$ui_coverimage+$ui_logoimage+$ui_address+$ui_contact+$email+!empty($ui_recognition))*$maximumPoints/100;
        //      // echo "Your percentage of profile completenes is".$percentage."%";
        //      // echo "<div style='width:100px; background-color:white; height:30px; border:1px solid #000;'>
        //      // <div style='width:".$percentage."px; background-color:red; height:30px;'></div></div>";
        // } 
									?>
									<div class="col s12 m12 l5 progressbar-bg progress-bar Space10">
										
										<div class="row">
											<div class="col s12 m8 l8 inner-progress-bar">
												<div class="progress">
													<div class="determinate" style="width: <?php //echo $percentage;?>%"></div>
												</div>
											</div>
											<div class="col s12 m4 l4 profile-complete no-padding-origin">
												
											</div>
										</div>
									</div>
									<div class="col s12 m2 l2"></div>	
								</div>	
							</div>
							<div class="col s12 mobile-magin-progress-bar hide-on-large-only visible-on-med-and-down"></div>	
					<div class="col s12 m6 l2 offset-l1 tab-padding cust-cursor">
						<div class="card">
							<div class="card-content  university-profile-card white-text"  id="click-academy-card-1">
								<p class="card-stats-title"><i class="icon icon-academy-profile-icon-01"></i>&nbsp;&nbsp; Introduction 0/13</p>
							</div>
						</div>
					</div>
					<div class="col s12 m6 l2 tab-padding cust-cursor">
						<div class="card">
							<div class="card-content university-profile-card white-text"  id="click-academy-card-2">
								<p class="card-stats-title">
								<i class="icon icon-academy-profile-icon-02"></i>&nbsp;&nbsp;
								Infrastructure</p>
								
							</div>
							
						</div>
					</div>                            
					<div class="col s12 m6 l2 tab-padding cust-cursor">
						<div class="card">
							<div class="card-content university-profile-card white-text"  id="click-academy-card-3">
								<p class="card-stats-title">
								<i class="icon icon-academy-profile-icon-03"></i> &nbsp;&nbsp;Photo</p>										
							</div>									
						</div>
					</div>
					<div class="col s12 m6 l2 tab-padding cust-cursor">
						<div class="card">
							<div class="card-content university-profile-card white-text" id="click-academy-card-4">
								<p class="card-stats-title">
								<i class="icon icon-academy-profile-icon-04"></i>&nbsp;&nbsp; Video</p>
							</div>
						</div>
					</div>
					<div class="col s12 m6 l2 tab-padding cust-cursor">
						<div class="card">
							<div class="card-content university-profile-card white-text" id="click-academy-card-5">
								<p class="card-stats-title"><i class="small icon icon-academy-profile-icon-05"></i> &nbsp;&nbsp;News & Updates</p>	
							</div>
						</div>
					</div>
			</div>
			
			<!--Desktop Academy Profile Form Start -->		
			<div class="card tab-card academy-card-one-div" >
				<div class="">
				<div id="card-alert" class="card-alert-personalInfo green" style="display:none;"><div class="card-content white-text"> <p><i class="mdi-navigation-check"></i><?php if(empty($universityData)){ ?>Personal Info added successfully <?php }else{ ?>Personal Info updated successfully <?php } ?></p></div><button type="button" class="close white-text" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button></div>
				<?php //print_r($universityData); ?>
					<form id="perInfo">
						<div class="row">
						  <div class="col s12 m12 l6">
							<div class="col s12 m12 l12 Space30"></div>	
								  <div class="row">
									<div class="col s12 m3 l3">
										<label class="NameLable">Name</label>
									</div>
									<div class="col s12 m8 l8 no-padding-origin">
										<input type="text" name="fullname" id="fname" value="<?php echo isset($universityData->um_name) ? $universityData->um_name : '';?>" >
									</div>
								  </div>
								  <div class="row">
									<div class="col s12 m3 l3">
										<label class="NameLable">Affiliation</label>
									</div>
									<div class="col s12 m8 l8 no-padding-origin">
										<input type="text" name="affiliation" id="affiliation" value="<?php echo isset($universityData->ui_recognition) ? trim($universityData->ui_recognition) : '';?>" >
									</div>
								  </div>
								  <div class="row">
									<div class="col s12 m3 l3">
										<label class="NameLable">Email ID</label>
									</div>
									<div class="col s12 m7 l7 no-padding-origin">
										<input type="text" name="email"  id="email" value="<?php echo isset($universityData->email) ? $universityData->email : '';?>" >
									</div>
								  </div>
								  <div class="row">
									<div class="col s12 m3 l3">
										<label class="NameLable">Address</label>
									</div>
									<div class="col s12 m7 l7 no-padding-origin">
										<textarea type="text" id="address" name="address"><?php echo isset($universityData->ui_address) ? $universityData->ui_address : '';?></textarea>
									</div>
								  </div>
								  <div class="row">
									<div class="col s12 m3 l3">
										<label class="NameLable">Contact Number</label>
									</div>
									<div class="col s12 m7 l7 no-padding-origin">
										<input type="text" name="contact_number" id="contact_number" value="<?php echo isset($universityData->ui_contact) ? $universityData->ui_contact: '';?>">
									</div>
								  </div>

								    <div class="row">
									<div class="col s12 m3 l3">
										<label class="NameLable">Country</label>
									</div>
									<div class="col s12 m7 l7 no-padding-origin">
										<select class="browser-default"  name="country" id="country">
										<option value="">Select country</option>
										
										<option <?php echo empty($universityData)? '' : ($universityData->country_id == '100') ? 'selected="selected"' : '';?> value="100">India</option>
									</select>
									</div>
								  </div>

								    <div class="row">
									<div class="col s12 m3 l3">
										<label class="NameLable">State</label>

									</div>
									<div class="col s12 m7 l7 no-padding-origin">
										<select class="browser-default" id="state" name="state">
											<option value="">Select State</option>
											<?php print_r($statedata)?>
												<?php foreach($statedata as $sdata){ ?>
											<option <?php echo ($universityData->state_id == $sdata->sm_id) ? 'selected="selected"' : ''; ?> value="<?php echo $sdata->sm_id; ?>"><?php echo $sdata->sm_name; ?></option>
										<?php } ?>
										</select>
									</div>
								  </div>

								    <div class="row">
									<div class="col s12 m3 l3">
										<label class="NameLable">City</label>
									</div>
									<div class="col s12 m7 l7 no-padding-origin">
										<select class="browser-default" id="city" name="city">
											<option value="">Select City</option>
												<?php foreach($citydata as $cdata){ ?>
											<option <?php echo ($universityData->city_id == $cdata->cim_id) ? 'selected="selected"' : ''; ?> value="<?php echo $cdata->cim_id; ?>"><?php echo $cdata->cim_name; ?></option>
										<?php } ?>
										</select>
									</div>
								  </div>
						  </div>
						  <!--city-->

						  <!-- Form with validation -->
						  <div class="col s12 m12 l6">
							  <div class="col s12 m12 l12 Space30"></div>
								<div class="row">
									<div class="col s12 m3 l3">
										<label class="NameLable">Microsite</label>
									</div>
									<div class="col s12 m5 l3 no-padding-origin">
										
									</div>
									<div class="col s12 m4 l4 no-padding-origin">
										<input type="text" name="micro_url" id="link" value="<?php echo isset($universityData->um_link) ? $universityData->um_link : '';?>" >
										<span><label class="NameLable uni-name-link">www.helloadmission.in/</label></span>
									</div>
									<div class="col s12 m3 l3 cust-cursor">
										 <div class="file-field input-field">
										  <div >
											
											<input class="btn upload-btn pre" type="button" name="logo" value="Preview" onclick="pre()">
										  </div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col s12 m3 l3">
										<label class="NameLable">Logo (60 x 60)</label>
									</div>
									<div class="col s12 m5 l5 no-padding-origin">
										<input type="text" name="unilink"  id="logoimage" value="<?php echo isset($universityData->ui_logoimage) ? $universityData->ui_logoimage : '';?>" >
									</div>
									<div class="col s12 m3 l3 cust-cursor">
										 <div class="file-field input-field">
										  <div class="upload-btn">
											<span style="color:#fff;">Choose Photo</span>
											<input type="file" name="logo" id="logo" accept="image/*">
										  </div>
										</div>
									</div>
									<?php if(isset($universityData->ui_logoimage)  && !empty($universityData->ui_logoimage) ){ ?>
									<div class="col s12 m5 l5 no-padding-origin">
										<img id="blah" src="<?php echo base_url('uploads/logos/'.$universityData->ui_logoimage); ?>" style="width: 168px;height: 80px;margin-left: 170px;"/>
									</div>
									<?php } else{
										?>
										<img id="blah2" src="" style="width: 168px;height: 80px;margin-left: 170px;"/>
										<?php
									} ?>
								</div>
								<div class="row">
									<div class="col s12 m3 l3">
										<label class="NameLable">Cover Images(1600 X 300)</label>
									</div>
									<div class="col s12 m5 l5 no-padding-origin">
										<input type="text" name="unilink" id="cimage" value="<?php echo isset($universityData->ui_coverimage) ? $universityData->ui_coverimage : '';?>" >
									</div>
									<div class="col s12 m3 l3 cust-cursor">
										 <div class="file-field input-field">
										  <div class="upload-btn">
											<span style="color:#fff;">Choose Photo</span>
											<input type="file" name="cover_image" id="cover_image" accept="image/*">
										  </div>
										</div>
									</div>
									<?php if(isset($universityData->ui_coverimage) && !empty($universityData->ui_coverimage) ){ ?>
									<div class="col s12 m5 l5 no-padding-origin">
										<img id="back1" style="width: 100%;margin-left: 170px;height: 100px;" src="<?php echo base_url('uploads/coverimages/'.$universityData->ui_coverimage); ?>"/>
									</div>
									<?php } else{
										?>
										<img id="back2" src="" style="width: 168px;height: 80px;margin-left: 170px;"/>
										<?php
									} ?>
								</div>
						  </div>
						  <div class="col s12 m12 l12">
							<div class="row">
								<div class="col s12 m1 l1">
									<label class="NameLable">About University</label>
								</div>
								<div class="col s12 m10 l10 about-university-margin">
									<div class="col s12 m12 l12">
										<textarea id="icon_prefix2" rolw="5" cols="15" class="materialize-textarea" name="about_university"><?php echo isset($universityData->ui_aboutus) ? $universityData->ui_aboutus : '';?></textarea>
										<?php if(empty($universityData)){ ?>
										<input class="btn submit-button waves-effect waves-light " type="submit" value="Save">
										<input type="hidden" name="subtype" value="add">
										<?php }else{ ?>
										<input class="btn submit-button waves-effect waves-light " type="submit" value="Update">
										<input type="hidden" name="subtype" value="update">
										<input type="hidden" name="um_id" value="<?php echo $universityData->um_id; ?>">
										<input type="hidden" name="ui_id" value="<?php echo $universityData->ui_id; ?>">
										<?php } ?>
									</div>
								</div>
							</div>
						  </div>
						</div>
					</form>
				</div>
			</div>
		<!--Desktop Academy Profile Form End -->
		
				<div class="card tab-card academy-card-two-div">
					<div class="section">
						<div id="card-alert" class="card-alert-facility green" style="display:none;"><div class="card-content white-text"> <p><i class="mdi-navigation-check"></i>Facilities added successfully</p></div><button type="button" class="close white-text" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button></div>
						<div class="col s12 m12 l12 Space20"></div>
						<form id="faciForm">
						<div class="row">
							<div class="col s12 m12 l12">
								<div class="col s12 m12 l12">
									<p class="uap-infra-text" style="text-align: center;">SELECT INFRASTRUCTURE</p>
									<?php //print_r($acfacility)?> 
									<div class="col s12 m12 l12">
										<div class="row">
											 <div class="col s6 m2 l2">
												<div class="fture-div" id="facility_computer_lab" <?php if(in_array('facility_computer_lab',$acfacility)){ ?> style="border:1px solid green;" <?php } ?> >
													<img class="fture" src="<?php echo base_url('assets/img/facility-icon-01.png'); ?>"/>
													<p class="fture" >Comp Labs</p>
													
													<?php if(in_array('facility_computer_lab',$acfacility)){ ?>
													<input type="hidden" id="facility_computer_lab1" name="facility_computer_lab" value='3'>
													<span class="remB" id="rem-facility_computer_lab" style="font-size: 24px;color: red;cursor:pointer" ><i class="mdi-content-clear" style="cursor:pointer"></i></span>
													<?php } ?>
												</div>
											 </div>
											 <div class="col s6 m2 l2">
												<div class="fture-div" id="facility_library" <?php if(in_array('facility_library',$acfacility)){ ?> style="border:1px solid green;" <?php } ?> >
													<img class="fture" src="<?php echo base_url('assets/img/facility-icon-02.png'); ?>"/>
													<p class="fture">Library</p>
													<?php if(in_array('facility_library',$acfacility)){  ?>
													<input type="hidden" id="facility_library1" value='3'>
													<span class="remB" id="rem-facility_library" style="font-size: 24px;color: red;"><i class="mdi-content-clear" style="cursor:pointer"></i></span>
													<?php } ?>
												</div>
											 </div>
											 <div class="col s6 m2 l2">
												<div class="fture-div" id="facility_laboratory" <?php if(in_array('facility_laboratory',$acfacility)){ ?> style="border:1px solid green;" <?php } ?> >
													<img class="fture" src="<?php echo base_url('assets/img/facility-icon-03.png'); ?>"/>
													<p class="fture">Laboratory</p>
													<?php if(in_array('facility_laboratory',$acfacility)){ ?>
													<input type="hidden" id="facility_laboratory1" value='3'>
													<span class="remB" id="rem-facility_laboratory" style="font-size: 24px;color: red;"><i class="mdi-content-clear" style="cursor:pointer"></i></span>
													<?php } ?>
												</div>
											 </div>
											 <div class="col s6 m2 l2">
												<div class="fture-div" id="facility_studyroom" <?php if(in_array('facility_studyroom',$acfacility)){ ?> style="border:1px solid green;" <?php } ?> >
													<img class="fture" src="<?php echo base_url('assets/img/facility-icon-04.png'); ?>"/>
													<p class="fture">Auditorium</p>
													<?php if(in_array('facility_studyroom',$acfacility)){ ?>
													<input type="hidden" id="facility_studyroom1" value='3'>
													<span class="remB" id="rem-facility_studyroom" style="font-size: 24px;color: red;"><i class="mdi-content-clear" style="cursor:pointer"></i></span>
													<?php } ?>
												</div>
											 </div>
											 <div class="col s6 m2 l2">
												<div class="fture-div" id="facility_sport" <?php if(in_array('facility_sport',$acfacility)){ ?> style="border:1px solid green;" <?php } ?> >
													<img class="fture" src="<?php echo base_url('assets/img/facility-icon-05.png'); ?>"/>
													<p class="fture">Sports</p>
													<?php if(in_array('facility_sport',$acfacility)){ ?>
													<input type="hidden"  id="facility_sport1" value='3'>
													<span class="remB" id="rem-facility_sport" style="font-size: 24px;color: red;"><i class="mdi-content-clear" style="cursor:pointer"></i></span>
													<?php } ?>
												</div>
											 </div>
											 <div class="col s6 m2 l2">
												<div class="fture-div" id="facility_gym" <?php if(in_array('facility_gym',$acfacility)){ ?> style="border:1px solid green;" <?php } ?> >
													<img class="fture" src="<?php echo base_url('assets/img/facility-icon-06.png'); ?>"/>
													<p class="fture">Gym</p>
													<?php if(in_array('facility_gym',$acfacility)){ ?>
													<input type="hidden" id="facility_gym1" value='3'>
													<span class="remB" id="rem-facility_gym" style="font-size: 24px;color: red;"><i class="mdi-content-clear" style="cursor:pointer"></i></span>
													<?php } ?>
												</div>
											 </div>
										</div>
										<div class="row">
											 <div class="col s6 m2 l2">
												<div class="fture-div" id="facility_medical" <?php if(in_array('facility_medical',$acfacility)){ ?> style="border:1px solid green;" <?php } ?> >
													<img class="fture" src="<?php echo base_url('assets/img/facility-icon-07.png'); ?>"/>
													<p class="fture">Medical</p>
													<?php if(in_array('facility_medical',$acfacility)){ ?>
													<input type="hidden" id="facility_medical1" value='3'>
													<span class="remB" id="rem-facility_medical" style="font-size: 24px;color: red;"><i class="mdi-content-clear" style="cursor:pointer"></i></span>
													<?php } ?>
												</div>
											 </div>
											 <div class="col s6 m2 l2">
												<div class="fture-div" id="facility_canteen" <?php if(in_array('facility_canteen',$acfacility)){ ?> style="border:1px solid green;" <?php } ?> >
													<img class="fture" src="<?php echo base_url('assets/img/facility-icon-08.png'); ?>"/>
													<p class="fture">Cafeteria</p>
													<?php if(in_array('facility_canteen',$acfacility)){ ?>
													<input type="hidden" id="facility_canteen1" value='3'>
													<span class="remB" id="rem-facility_canteen" style="font-size: 24px;color: red;"><i class="mdi-content-clear" style="cursor:pointer"></i></span>
													<?php } ?>
												</div>
											 </div>
											 <div class="col s6 m2 l2">
												<div class="fture-div" id="facility_transport" <?php if(in_array('facility_transport',$acfacility)){ ?> style="border:1px solid green;" <?php } ?> >
													<img class="fture" src="<?php echo base_url('assets/img/facility-icon-10.png'); ?>"/>
													<p class="fture">Transport</p>
													<?php if(in_array('facility_transport',$acfacility)){ ?>
													<input type="hidden" id="facility_transport1" value='3'>
													<span class="remB" id="rem-facility_transport" style="font-size: 24px;color: red;"><i class="mdi-content-clear" style="cursor:pointer"></i></span>
													<?php } ?>
												</div>
											 </div>
											 <div class="col s6 m2 l2">
												<div class="fture-div" id="facility_hostel" <?php if(in_array('facility_hostel',$acfacility)){ ?> style="border:1px solid green;cursor:pointer" <?php } ?> >
													<img class="fture" src="<?php echo base_url('assets/img/facility-icon-11.png'); ?>"/>
													<p class="fture">Hostel</p>
													<?php if(in_array('facility_hostel',$acfacility)){ ?>
													<input type="hidden" id="facility_hostel1" value='3'>
													<span class="remB" id="rem-facility_hostel" style="font-size: 24px;color: red;"><i class="mdi-content-clear" style="cursor:pointer"></i></span>
													<?php } ?>
												</div>
											 </div>
										</div>
									</div>
								</div>
							</div>
							<div class="col s12 m6 l6" style="text-align: center;">
								<input type="hidden" name="facility" id="facility" value="<?php echo implode(',',$acfacility); ?>">
								<button class="btn submit-button waves-effect waves-light " type="submit" name="intro-submit">Save
								<i class="mdi-content-send right"></i>
								</button>
							</div>
						</div>
						</form>
					</div>
					
				</div>
				<div class="card tab-card academy-card-three-div">
					
					<div class="col s12 m12 l12 Space20"></div>
					<div class="row">
						<div class="col s12 m12 l12" >
							<div class="col s12 m6 l6">
								<div class="col s12 m12 l12 apphoto-border-bg">
									<p class="ap-title">Infrastructure Photos</p>
									<div class="row row-margin-bottom5" id="infra-div">
										<form id="infrForm">
											<div class="col s12 m6 l3 center-align">
												<div class="col s12 m12 l12 no-padding-origin">
													<input type="file" name="Infra-file" class="dropify" data-default-file="" />
													<input type="text" name="Infra-caption">
													<input type="submit" class="btn btn-primary" id="Infra" value="Upload"/>
												</div>
											</div>
										</form>
										<?php if($academicPhoto){
												foreach($academicPhoto as $iphoto){ 
												   if($iphoto->ugm_category == 'INFRASTRUCTURE'){
											?>
											<div class="col s12 m6 l3 center-align image_div">
												<a class="ap-image-remove media_remove" id="photo_<?php echo $iphoto->ugm_id; ?>"><img style="margin-left: 132px;cursor:pointer" src="<?php echo base_url('assets/images/cross-icon-2.png') ?>"></a>
												<img src="<?php echo base_url('uploads/'.$iphoto->ugm_link); ?>"  class="responsive-img media_media">
												<input type="hidden" id="img1" value='4'/>
												<p class="ap-image-name"><?php echo $iphoto->ugm_info; ?></p>
											</div>
											<?php  }
												}  
											} ?>										
									</div>
								</div>
							</div>
							<div class="col s12 m6 l6">
								<div class="col s12 m12 l12 apphoto-border-bg">
									<p class="ap-title">Student Play Photos</p>
									<div class="row row-margin-bottom5" id="sp-div">
										<form id="spForm">
											<div class="col s12 m6 l3 center-align">
												<div class="col s12 m12 l12 no-padding-origin">
													<input type="file" name="sp-file" class="dropify" data-default-file="" />
													<input type="text" name="sp-caption">
													<input type="submit" class="btn btn-primary" id="sp" value="Upload"/>
												</div>
											</div>
										</form>
										<?php if($academicPhoto){
												foreach($academicPhoto as $iphoto){ 
												   if($iphoto->ugm_category == 'STUDENT PLAY'){
											?>
											<div class="col s12 m6 l3 center-align image_div">
												<a class="ap-image-remove media_remove" id="photo_<?php echo $iphoto->ugm_id; ?>"><img style="margin-left: 132px;cursor:pointer" src="<?php echo base_url('assets/images/cross-icon-2.png') ?>"></a>
												<img src="<?php echo base_url('uploads/'.$iphoto->ugm_link); ?>"  class="responsive-img media_media">
												<input type="hidden" id="img2" value='4'/>
												<p class="ap-image-name"><?php echo $iphoto->ugm_info; ?></p>
											</div>
											<?php  }
												}  
											} ?>
									</div>		
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col s12 m12 l12" >
							<div class="col s12 m6 l6">
								<div class="col s12 m12 l12 apphoto-border-bg">
									<p class="ap-title">Student In Classroom and Lab</p>
									<div class="row row-margin-bottom5" id="scl-div">
										<form id="sclForm">
											<div class="col s12 m6 l3 center-align">
												<div class="col s12 m12 l12 no-padding-origin">
													<input type="file" name="scl-file" class="dropify" data-default-file="" />
													<input type="hidden" name="scl-caption">
													<input type="submit" class="btn btn-primary" id="scl" value="Upload"/>
												</div>
											</div>
										</form>
										<?php if($academicPhoto){
												foreach($academicPhoto as $iphoto){ 
												   if($iphoto->ugm_category == 'STUDENT IN CLASSROOM AND LAB'){
											?>
											<div class="col s12 m6 l3 center-align image_div">
												<a class="ap-image-remove media_remove" id="photo_<?php echo $iphoto->ugm_id; ?>"><img style="margin-left: 132px;cursor:pointer" src="<?php echo base_url('assets/images/cross-icon-2.png') ?>"></a>
												<img src="<?php echo base_url('uploads/'.$iphoto->ugm_link); ?>"  class="responsive-img media_media">
												<input type="hidden" id="img3" value='4'/>
												<p class="ap-image-name"><?php echo $iphoto->ugm_info; ?></p>
											</div>
											<?php  }
												}  
											} ?>
									</div>		
								</div>
							</div>
							<div class="col s12 m6 l6">
								<div class="col s12 m12 l12 apphoto-border-bg">
									<p class="ap-title">Student At Action</p>
									<div class="row row-margin-bottom5" id="saa-div">
										<form id="saaForm">
											<div class="col s12 m6 l3 center-align">
												<div class="col s12 m12 l12 no-padding-origin">
													<input type="file" name="saa-file" class="dropify" data-default-file="" />
													<input type="hidden" name="saa-caption">
													<input type="submit" class="btn btn-primary" id="saa" value="Upload"/>
												</div>
											</div>
										</form>
										<?php if($academicPhoto){
												foreach($academicPhoto as $iphoto){ 
												   if($iphoto->ugm_category == 'STUDENT AT ACTION'){
											?>
											<div class="col s12 m6 l3 center-align image_div">
												<a class="ap-image-remove media_remove" id="photo_<?php echo $iphoto->ugm_id; ?>"><img style="margin-left: 132px;cursor:pointer" src="<?php echo base_url('assets/images/cross-icon-2.png') ?>"></a>
												<img src="<?php echo base_url('uploads/'.$iphoto->ugm_link); ?>"  class="responsive-img media_media">
												<input type="hidden" id="img4" value='4'/>
												<p class="ap-image-name"><?php echo $iphoto->ugm_info; ?></p>
											</div>
											<?php  }
												}  
											} ?>
									</div>		
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col s12 m12 l12" >
							<div class="col s12 m6 l6">
								<div class="col s12 m12 l12 apphoto-border-bg">
									<p class="ap-title">Student News</p>
									<div class="row row-margin-bottom5" id="sn-div">
										<form id="snForm">
											<div class="col s12 m6 l3 center-align">
												<div class="col s12 m12 l12 no-padding-origin">
													<input type="file" name="sn-file" class="dropify" data-default-file="" />
													<input type="hidden" name="sn-caption">
													<input type="submit" class="btn btn-primary" id="sn" value="Upload"/>
												</div>
											</div>
										</form>
										<?php if($academicPhoto){
												foreach($academicPhoto as $iphoto){ 
												   if($iphoto->ugm_category == 'STUDENT NEWS'){
											?>
											<div class="col s12 m6 l3 center-align image_div">
												<a class="ap-image-remove media_remove" id="photo_<?php echo $iphoto->ugm_id; ?>"><img style="margin-left: 132px;cursor:pointer" src="<?php echo base_url('assets/images/cross-icon-2.png') ?>"></a>
												<img src="<?php echo base_url('uploads/'.$iphoto->ugm_link); ?>"  class="responsive-img media_media">
												<input type="hidden" id="img5" value='4'/>
												<p class="ap-image-name"><?php echo $iphoto->ugm_info; ?></p>
											</div>
											<?php  }
												}  
											} ?>
									</div>		
								</div>
							</div>
							<div class="col s12 m6 l6">
								<div class="col s12 m12 l12 apphoto-border-bg">
									<p class="ap-title">Student Project</p>
									<div class="row row-margin-bottom5" id="spro-div">
										<form id="sproForm">
											<div class="col s12 m6 l3 center-align">
												<div class="col s12 m12 l12 no-padding-origin">
													<input type="file" name="spro-file" class="dropify" data-default-file="" />
													<input type="hidden" name="spro-caption">
													<input type="submit" class="btn btn-primary" id="spro" value="Upload"/>
												</div>
											</div>
										</form>
										<?php if($academicPhoto){
												foreach($academicPhoto as $iphoto){ 
												   if($iphoto->ugm_category == 'STUDENT PROJECT'){
											?>
											<div class="col s12 m6 l3 center-align image_div">
												<a class="ap-image-remove media_remove" id="photo_<?php echo $iphoto->ugm_id; ?>"><img style="margin-left: 132px;cursor:pointer" src="<?php echo base_url('assets/images/cross-icon-2.png') ?>"></a>
												<img src="<?php echo base_url('uploads/'.$iphoto->ugm_link); ?>"  class="responsive-img media_media">
												<input type="hidden" id="img6" value='4'/>
												<p class="ap-image-name"><?php echo $iphoto->ugm_info; ?></p>
											</div>
											<?php  }
												}  
											} ?>
									</div>		
								</div>
							</div>
						</div>
					</div>
						
				</div>
				<div class="card tab-card academy-card-four-div">
					<div class="col s12 m12 l12 Space20"></div>
					<div class="row">
						<div class="col s12 m12 l12" >
							<div class="col s12 m6 l6">
								<div class="col s12 m12 l12 apphoto-border-bg">
									<p class="ap-title">Infrastructure Videos</p>
									<div class="row row-margin-bottom5" id="infra-vid-div">
									<form id="infrVidForm">
										<div class="col s12 m6 l3 center-align">
											<div class="col s12 m12 l12 no-padding-origin">
												<input type="file" name="Infra-vid-file" class="dropify" data-default-file="" />
												<input type="text" name="Infra-vid-caption">
												<input type="submit" class="btn btn-primary" id="Infra-vid" value="Upload"/>
											</div>
										</div>
									</form>
									<?php  if($academicVideo){
												foreach($academicVideo as $ivideo){ 
												   if($ivideo->ugm_category == 'INFRASTRUCTURE VIDEO'){ ?>
									<div class="col s12 m5 l5 center-align video_div">
										<a class="ap-image-remove media_remove" id="video_<?php echo $ivideo->ugm_id; ?>" style="float: right;"><img style="margin-left: 132px;cursor:pointer" src="<?php echo base_url('assets/images/cross-icon-2.png') ?>"></a>
											<div class="video-container">
												<video width="200" height="120" controls="">
													<source src="<?php echo base_url('uploads/'.$ivideo->ugm_link); ?>" type="video/ogg">Your browser does not support the video element.
												</video>
											</div>
											<input type="hidden" id="v1" value='4'/>
									<p class="ap-image-name vheading"><?php echo $ivideo->ugm_info; ?></p>	
									</div>
									<?php  			}
												}  
											} ?>
									</div>
								</div>
							</div>
							<div class="col s12 m6 l6">
								<div class="col s12 m12 l12 apphoto-border-bg">
									<p class="ap-title">Student Play Videos</p>
									<div class="row row-margin-bottom5" id="sp-vid-div">
										<form id="spVidForm">
											<div class="col s12 m6 l3 center-align">
												<div class="col s12 m12 l12 no-padding-origin">
													<input type="file" name="sp-vid-file" class="dropify" data-default-file="" />
													<input type="text" name="sp-vid-caption">
													<input type="submit" class="btn btn-primary" id="sp-vid" value="Upload"/>
												</div>
											</div>
										</form>
										<?php   if($academicVideo){
												foreach($academicVideo as $ivideo){ 
												   if($ivideo->ugm_category == 'STUDENT PLAY VIDEO'){ ?>
									<div class="col s12 m5 l5 center-align video_div">
										<a class="ap-image-remove media_remove" id="video_<?php echo $ivideo->ugm_id; ?>" style="float: right;"><img style="margin-left: 132px;cursor:pointer" src="<?php echo base_url('assets/images/cross-icon-2.png') ?>"></a>
											<div class="video-container">
												<video width="200" height="120" controls="">
													<source src="<?php echo base_url('uploads/'.$ivideo->ugm_link); ?>" type="video/ogg">Your browser does not support the video element.
												</video>
											</div>
											<input type="hidden" id="v2" value='4'/>
									<p class="ap-image-name vheading"><?php echo $ivideo->ugm_info; ?></p>	
									</div>
									<?php  			}
												}  
											} ?>
									</div>		
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col s12 m12 l12" >
							<div class="col s12 m6 l6">
								<div class="col s12 m12 l12 apphoto-border-bg">
									<p class="ap-title">Student In Classroom and Lab</p>
									<div class="row row-margin-bottom5" id="scl-vid-div">
										<form id="sclVidForm">
											<div class="col s12 m6 l3 center-align">
												<div class="col s12 m12 l12 no-padding-origin">
													<input type="file" name="scl-vid-file" class="dropify" data-default-file="" />
													<input type="text" name="scl-vid-caption">
													<input type="submit" class="btn btn-primary" id="scl-vid" value="Upload"/>
												</div>
											</div>
										</form>
										<?php   if($academicVideo){
												foreach($academicVideo as $ivideo){ 
												   if($ivideo->ugm_category == 'STUDENT IN CLASSROOM AND LAB VIDEO'){ ?>
									<div class="col s12 m5 l5 center-align video_div">
										<a class="ap-image-remove media_remove" id="video_<?php echo $ivideo->ugm_id; ?>" style="float: right;"><img style="margin-left: 132px;cursor:pointer" src="<?php echo base_url('assets/images/cross-icon-2.png') ?>"></a>
											<div class="video-container">
												<video width="200" height="120" controls="">
													<source src="<?php echo base_url('uploads/'.$ivideo->ugm_link); ?>" type="video/ogg">Your browser does not support the video element.
												</video>
											</div><input type="hidden" id="v3" value='4'/>
									<p class="ap-image-name vheading"><?php echo $ivideo->ugm_info; ?></p>	
									</div>
									<?php  			}
												}  
											} ?>
									</div>		
								</div>
							</div>
							<div class="col s12 m6 l6">
								<div class="col s12 m12 l12 apphoto-border-bg">
									<p class="ap-title">Student At Action</p>
									<div class="row row-margin-bottom5" id="saa-vid-div">
										<form id="saaVidForm">
											<div class="col s12 m6 l3 center-align">
												<div class="col s12 m12 l12 no-padding-origin">
													<input type="file" name="saa-vid-file" class="dropify" data-default-file="" />
													<input type="text" name="saa-vid-caption">
													<input type="submit" class="btn btn-primary" id="saa-vid" value="Upload"/>
												</div>
											</div>
										</form>
										<?php   if($academicVideo){
												foreach($academicVideo as $ivideo){ 
												   if($ivideo->ugm_category == 'STUDENT AT ACTION VIDEO'){ ?>
									<div class="col s12 m5 l5 center-align video_div">
										<a class="ap-image-remove media_remove" id="video_<?php echo $ivideo->ugm_id; ?>" style="float: right;"><img style="margin-left: 132px;cursor:pointer" src="<?php echo base_url('assets/images/cross-icon-2.png') ?>"></a>
											<div class="video-container">
												<video width="200" height="120" controls="">
													<source src="<?php echo base_url('uploads/'.$ivideo->ugm_link); ?>" type="video/ogg">Your browser does not support the video element.
												</video>
											</div><input type="hidden" id="v4" value='4'/>
									<p class="ap-image-name vheading"><?php echo $ivideo->ugm_info; ?></p>	
									</div>
									<?php  			}
												}  
											} ?>
									</div>		
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col s12 m12 l12" >
							<div class="col s12 m6 l6">
								<div class="col s12 m12 l12 apphoto-border-bg">
									<p class="ap-title">Student News</p>
									<div class="row row-margin-bottom5" id="sn-vid-div">
										<form id="snVidForm">
											<div class="col s12 m6 l3 center-align">
												<div class="col s12 m12 l12 no-padding-origin">
													<input type="file" name="sn-vid-file" class="dropify" data-default-file="" />
													<input type="text" name="sn-vid-caption">
													<input type="submit" class="btn btn-primary" id="sn-vid" value="Upload"/>
												</div>
											</div>
										</form>
										<?php   if($academicVideo){
												foreach($academicVideo as $ivideo){ 
												   if($ivideo->ugm_category == 'STUDENT NEWS VIDEO'){ ?>
									<div class="col s12 m5 l5 center-align video_div">
										<a class="ap-image-remove media_remove" id="video_<?php echo $ivideo->ugm_id; ?>" style="float: right;"><img style="margin-left: 132px;cursor:pointer" src="<?php echo base_url('assets/images/cross-icon-2.png') ?>"></a>
											<div class="video-container">
												<video width="200" height="120" controls="">
													<source src="<?php echo base_url('uploads/'.$ivideo->ugm_link); ?>" type="video/ogg">Your browser does not support the video element.
												</video>
											</div><input type="hidden" id="v5" value='4'/>
									<p class="ap-image-name vheading"><?php echo $ivideo->ugm_info; ?></p>	
									</div>
									<?php  			}
												}  
											} ?>
									</div>		
								</div>
							</div>
							<div class="col s12 m6 l6">
								<div class="col s12 m12 l12 apphoto-border-bg">
									<p class="ap-title">Student Project</p>
									<div class="row row-margin-bottom5" id="spro-vid-div">
										<form id="sproVidForm">
											<div class="col s12 m6 l3 center-align">
												<div class="col s12 m12 l12 no-padding-origin">
													<input type="file" name="spro-vid-file" class="dropify" data-default-file="" />
													<input type="text" name="spro-vid-caption">
													<input type="submit" class="btn btn-primary" id="spro-vid" value="Upload"/>
												</div>
											</div>
										</form>
										<?php   if($academicVideo){
												foreach($academicVideo as $ivideo){ 
												   if($ivideo->ugm_category == 'STUDENT PROJECT VIDEO'){ ?>
									<div class="col s12 m5 l5 center-align video_div">
										<a class="ap-image-remove media_remove" id="video_<?php echo $ivideo->ugm_id; ?>" style="float: right;"><img style="margin-left: 132px;cursor:pointer" src="<?php echo base_url('assets/images/cross-icon-2.png') ?>"></a>
											<div class="video-container">
												<video width="200" height="120" controls="">
													<source src="<?php echo base_url('uploads/'.$ivideo->ugm_link); ?>" type="video/ogg">Your browser does not support the video element.
												</video>
											</div><input type="hidden" id="v6" value='4'/>
									<p class="ap-image-name vheading"><?php echo $ivideo->ugm_info; ?></p>	
									</div>
									<?php  			}
												}  
											} ?>
									</div>		
								</div>
							</div>
						</div>
					</div>	
				</div>
				<div class="card tab-card academy-card-five-div">
					<div class="">
						<div  class="section">
							<div class="col s12 m12 l12 Space20"></div>
							<h2 class="center-align">Coming Soon...</h2>
							
						</div>
						
					</div>
				</div>
				</div>
							 <!-- card Section End-->
			</div>
		</div>
	</div>
</section>
<script type="text/javascript" src="<?php echo base_url('assets/js/plugins/jquery-1.11.2.min.js');?>"></script>
 <!-- dropify -->
 <script type="text/javascript" src="<?php echo base_url('assets_new/js/academic_progress.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/plugins/dropify/js/dropify.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/academic_profile.js');?>"></script>
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/1.3.1/js/toastr.js"></script>

<script>
facilities = ['<?php echo implode("','",$acfacility); ?>'];
function ajaxPhotoUpload(filedata,typephoto){
	$.ajax({
		url: "<?php echo base_url('academic/photoUpload'); ?>",
		type: "POST",
		data: filedata,
		contentType: false,
		cache: false,
		processData:false,
		success: function(data){
			/*********************** Start Photos **********************************/
			
			if(typephoto == 'INFRASTRUCTURE'){
				$("#infra-div").append(data);
			}else if(typephoto == 'STUDENT PLAY'){
				$("#sp-div").append(data);
			}else if(typephoto == 'STUDENT IN CLASSROOM AND LAB'){
				$("#scl-div").append(data);
			}else if(typephoto == 'STUDENT AT ACTION'){
				$("#saa-div").append(data);
			}else if(typephoto == 'STUDENT NEWS'){
				$("#sn-div").append(data);
			}else if(typephoto == 'STUDENT PROJECT'){
				$("#spro-div").append(data);
			}
			
			/*********************** End Photos **********************************/
			
			/*********************** Start Videos **********************************/
			
			if(typephoto == 'INFRASTRUCTURE VIDEO'){
				$("#infra-vid-div").append(data);
			}else if(typephoto == 'STUDENT PLAY VIDEO'){
				$("#sp-vid-div").append(data);
			}else if(typephoto == 'STUDENT IN CLASSROOM AND LAB VIDEO'){
				$("#scl-vid-div").append(data);
			}else if(typephoto == 'STUDENT AT ACTION VIDEO'){
				$("#saa-vid-div").append(data);
			}else if(typephoto == 'STUDENT NEWS VIDEO'){
				$("#sn-vid-div").append(data);
			}else if(typephoto == 'STUDENT PROJECT VIDEO'){
				$("#spro-vid-div").append(data);
			}
			
			/*********************** End Videos **********************************/
		},
		error: function(){} 	        
	});
}

function pre()
{ 
var i=$('#fname').val();
var url='<?php echo base_url()?>'+i;
  window.open(url,'_blank');
}

function removePhotoUpload(rmpId){
	$.ajax({
			url: "<?php echo base_url('academic/removePhotoUploaded'); ?>",
			type: "POST",
			data: {rmphotoId:rmpId},
			success: function(data){
			},
			error: function(){} 	        
	});
}

var i=$('#fname').val();
var i=$('#link').val(i);


$("#perInfo").on('submit',(function(e) {
	e.preventDefault();
	
	var formData = new FormData($(this)[0]);
 
	  $.ajax({
		url: '<?php echo base_url('academic/addPersonalInfo'); ?>',
		type: 'POST',
		data: formData,
		async: false,
		cache: false,
		contentType: false,
		processData: false,
		success: function (returndata) {
			console.log(returndata);
		 // $('.card-alert-personalInfo').show();
$('body').load(returndata);
         // $('#card-alert').delay(5000).fadeOut(400);
		  //location.reload();
		  		toastr.success("Profile Updated Successfully");
				
					setTimeout(function(){
		$('body').load(returndata);
		
		  location.reload();
   // $('#perInfo').html(returndata);

		
		}, 3000);
		}
	  });
	
}));

$("#faciForm").on('submit',(function(e) {
	e.preventDefault();
	
	  var facilities = $('#facility').val();
 
	  $.ajax({
		url: '<?php echo base_url('academic/addFacilityInfo'); ?>',
		type: 'POST',
		data: {facilities:facilities},
		success: function (returndata) {
		  //$('.card-alert-facility').show();
		toastr.success("Facilities Updated Successfully");
				
					setTimeout(function(){
		$('body').load(returndata);
		location.reload();
		}, 3000);
          //$('#card-alert').delay(5000).fadeOut(400);
		}
	  });
}));

$('#country').change(function(){
	var country = $(this).val();
	$('#state').empty();
	$.ajax({
		type: "POST",
		url: "<?php echo base_url('student/getAjaxState'); ?>",
		data:{country:country},
		success: function(response){
			var res = JSON.parse(response);
			$('#state').append('<option value="" disabled selected>Select State</option>');
			$.each(res, function(key, value) {   
				 $('#state')
					 .append($("<option></option>")
								.attr("value",value.sm_id)
								.text(value.sm_name)); 
			});
		}
	});
});

$('#state').change(function(){
	var state = $(this).val();
	$('#city').empty();
	$.ajax({
		type: "POST",
		url: "<?php echo base_url('student/getAjaxCity'); ?>",
		data:{state:state},
		success: function(response){
			var res = JSON.parse(response);
			$('#city').append('<option value="" disabled selected>Select City</option>');
			$.each(res, function(key, value) {   
				 $('#city')
					 .append($("<option></option>")
								.attr("value",value.cim_id)
								.text(value.cim_name)); 
			});
		}
	});
});
$('#blah1').hide();

 function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            // $('#blah1').hide();
            reader.onload = function (e) {
            	
            	$('#blah').show();
                $('#blah').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    $("#logo").change(function(){

        readURL(this);
    });


     function readURL1(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
             //$('#blah').hide();
            reader.onload = function (e) {
            	
            	$('#blah1').show();
                $('#blah1').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    $("#logo").change(function(){

        readURL1(this);
    });

    $('#back2').hide();

 function readURL_1(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            // $('#blah1').hide();
            reader.onload = function (e) {
            	
            	$('#back1').show();
                $('#back1').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    $("#cover_image").change(function(){

        readURL_1(this);
    });


     function readURL_2(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
             //$('#blah').hide();
            reader.onload = function (e) {
            	
            	$('#back2').show();
                $('#back2').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    $("#cover_image").change(function(){

        readURL_2(this);
    });

    
</script>
</script>