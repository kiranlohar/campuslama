<style>
.select-dropdown {
     height: auto !important; 
}
.select-wrapper input.select-dropdown{
	line-height: 2rem;
}
</style>
<section id="content">
<div class="row"  id="side-menu-five-div">
	<div class="col s12 m12 l12" style="margin-top: -6.8px;">
		<div class="card">		
			<div class="">		
				<?php 
				 //print_r($programdata); ?>
							  <!-- card Section Start-->
				<div id="card-stats" class="seaction">
					<div class="row">
						<div class="col s12 m8 l8 offset-l3 offset-m4">
							<div class="row row-margin-bottom">
								<div class="Space30 col l12 m12 s12"></div>
								<div class="col s12 m12 l12">
								<h4 class="admission-form-heading-margin">Program Manager</h4>
								</div>
								<div class="col s12 m3 l3"></div>	
							</div>	
						</div>
						<div class="Space30 col l12 m12 s12"></div>
						<div class="col l12 m12 s12">
							<div class="col l12 m12 s12">
							</div>
							<div class="col s12 m12 l12 Space10"></div>
							<div class="row col l12 m12 s12" id="add_program_form">
								<form action="<?php echo base_url('academic/edit_program_manager')?>" method="post">
								<div class="card col l5 m5 s12">
									<div class="col s12 m12 l12 Space10"></div>
									<div class="col s12 m3 l3">
										<label class="NameLable">Type</label>
									</div>
									<div class="col s12 m8 l8 no-padding-origin">
										<select id="program_type" name="program_type" class="browser-default Ustory select-dropdown">
											<option value="" disabled selected>Choose Program Type</option>
											<option <?php echo ($programManager->uc_course_type == 'FULL TIME')? 'selected' : ''; ?> value="FULL">Full Time</option>
											<option <?php echo ($programManager->uc_course_type == 'PART TIME')? 'selected' : ''; ?> value="PART">Part Time</option>
											<option <?php echo ($programManager->uc_course_type == 'DISTANCE TIME')? 'selected' : ''; ?> value="DISTANCE">Distance Learning 3</option>
										</select>
									</div>
									<div class="col s12 m12 l12 Space10"></div>
									<div class="col s12 m3 l3">
										<label class="NameLable">Stream</label>
									</div>
									<div class="col s12 m8 l8 no-padding-origin">
										<select id="stream" name="stream" class="browser-default Ustory select-dropdown">
											<option value="" disabled selected>Choose Stream</option>
											<?php foreach($streams as $stream){ ?>
											<option <?php echo ($programManager->uc_parent == $stream->id)? 'selected' : ''; ?> value="<?php echo $stream->id; ?>"><?php echo $stream->name; ?></option>
											<?php } ?>
										</select>
									</div>
									<div class="col s12 m12 l12 Space10"></div>
									<div class="col s12 m3 l3">
										<label class="NameLable">Program</label>
									</div>
									<div class="col s12 m8 l8 no-padding-origin">
										<select id="program" name="program" class="browser-default Ustory select-dropdown">
											<option value="" disabled selected>Choose Program</option>
											<?php foreach($programs as $program){ ?>
											<option <?php echo ($programManager->id == $program->id)? 'selected' : ''; ?> value="<?php echo $program->com_id; ?>"><?php echo $program->name; ?></option>
											<?php } ?>
										</select>
									</div>
									<div class="col s12 m12 l12 Space10"></div>
									<div class="col s12 m3 l3">
										<label class="NameLable">Fees Plan</label>
									</div>
									<?php $feeIds = explode(',',$programManager->uc_fees); ?>
									<div class="col s12 m8 l8">
									  <div class="input-field col s12" style="margin-left: -22px;">
										<select name="fees[]" multiple>
										 <option value="" disabled selected>Choose Fees Plan</option>
											<?php foreach($fees as $fee){ ?>
											<option <?php if(in_array($fee->ufeem_id,$feeIds)){ echo'selected'; } ?> value="<?php echo $fee->ufeem_id; ?>"><?php echo $fee->ufeem_plan_name; ?> (<?php echo $fee->ufeem_plan_type; ?> - <?php echo $fee->ufeem_amount; ?> )</option>
											<?php } ?>
										</select>
									  </div>
									</div>
             
									<div class="col s12 m12 l12 Space10"></div>
									<div class="col s12 m3 l3">
										<label class="NameLable">Program Duration</label>
									</div>
									<?php $dur = explode(' ',$programManager->uc_course_length);?>
									<div class="col s12 m4 l4 no-padding-origin">
										<input type="text" name="duration" value="<?php echo trim($dur[0]); ?>">
									</div>
									<div class="col s12 m4 l4 no-padding-origin">
										<select id="dur-unit" name="dur-unit" class="browser-default Ustory select-dropdown" >
											<option value="" disabled selected>Choose Year/Month</option>
											<option <?php echo ($dur[1] == 'Months')? 'selected' : ''; ?> value="Months">Months</option>
											<option <?php echo ($dur[1] =='Years')? 'selected' : ''; ?> value="Years">Years</option>
										</select>
									</div>
									<div class="col s12 m12 l12 Space10"></div>
									<div class="col s12 m3 l3">
										<label class="NameLable">Administrator</label>
									</div>
									<div class="col s12 m8 l8 no-padding-origin">
										<input type="text" name="admin_name" value="<?php echo $programManager->upm_admin_name; ?>">
									</div>
									<div class="col s12 m3 l3">
										<label class="NameLable">Email Id</label>
									</div>
									<div class="col s12 m8 l8 no-padding-origin">
										<input type="text" name="admin_email" value="<?php echo $programManager->upm_admin_email; ?>">
									</div>
									<div class="col s12 m3 l3">
										<label class="NameLable">Mobile</label>
									</div>
									<div class="col s12 m8 l8 no-padding-origin">
										<input type="text" name="admin_mobile" value="<?php echo $programManager->upm_admin_mobile; ?>">
									</div>
									<input type="hidden" name="upmid" value="<?php echo $programManager->upa_upm_id; ?>">
									<input type="hidden" name="upmadminid" value="<?php echo $programManager->upa_id; ?>">
									<input type="hidden" name="uc_id" value="<?php echo $programManager->uc_id; ?>">
									<input type="hidden" name="userId" value="<?php echo $programManager->upa_user_id; ?>">
									<div class="col s12 m8 l8 offset-l3 offset-m3 no-padding-origin">
										<input type="submit" value="Save" class="waves-effect waves-light btn btn-form-submit Searchbtn center-align up-save-btn"/>
									</div>
									<div class="col s12 m12 l12 Space10"></div>
								</div>
								</form>
								<div class="col l7 m7 s12"></div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>	
</div>

</section>
<script type="text/javascript" src="<?php echo base_url('assets/js/plugins/jquery-1.11.2.min.js');?>"></script>
<script>
$('#stream').change(function(){
	var streamid = $(this).val();
	$('#program').empty();
	$.ajax({
		type: "POST",
		url: "<?php echo base_url('academic/stream_program'); ?>",
		data:{streamid:streamid},
		success: function(response){
			var res = JSON.parse(response);
			$('#program').append('<option value="" disabled selected>Choose Program</option>');
			$.each(res, function (i, itm) {
				$('#program').append($('<option>', { 
					value: itm.com_id,
					text : itm.com_name 
				}));
			});
		}
	});
});


</script>