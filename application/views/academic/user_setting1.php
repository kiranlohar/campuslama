 <style type="text/css">
  .input-field div.error{
    position: relative;
    top: -1rem;
    left: 0rem;
    font-size: 0.8rem;
    color:#FF4081;
    -webkit-transform: translateY(0%);
    -ms-transform: translateY(0%);
    -o-transform: translateY(0%);
    transform: translateY(0%);
  }
  .input-field label.active{
      width:100%;
  }
  .left-alert input[type=text] + label:after, 
  .left-alert input[type=password] + label:after, 
  .left-alert input[type=email] + label:after, 
  .left-alert input[type=url] + label:after, 
  .left-alert input[type=time] + label:after,
  .left-alert input[type=date] + label:after, 
  .left-alert input[type=datetime-local] + label:after, 
  .left-alert input[type=tel] + label:after, 
  .left-alert input[type=number] + label:after, 
  .left-alert input[type=search] + label:after, 
  .left-alert textarea.materialize-textarea + label:after{
      left:0px;
  }
  .right-alert input[type=text] + label:after, 
  .right-alert input[type=password] + label:after, 
  .right-alert input[type=email] + label:after, 
  .right-alert input[type=url] + label:after, 
  .right-alert input[type=time] + label:after,
  .right-alert input[type=date] + label:after, 
  .right-alert input[type=datetime-local] + label:after, 
  .right-alert input[type=tel] + label:after, 
  .right-alert input[type=number] + label:after, 
  .right-alert input[type=search] + label:after, 
  .right-alert textarea.materialize-textarea + label:after{
      right:70px;
  }
  </style>
  <?php $userdata = $this->session->userdata('logged_in');// print_r($userdata);?>
<section id="content">
<div class="row"  id="side-menu-six-div">
	<div class="col s12 m12 l12" style="margin-top: -6.8px;">
		<div class="card">		
			<div class="">			
							  <!-- card Section Start-->
				<div id="card-stats" class="seaction" style="height:700px !important;">
					<div class="row">
						<div class="col s12 m8 l8 offset-l3 offset-m4">
							<div class="row row-margin-bottom">
								<div class="Space30 col l12 m12 s12"></div>
								<div class="col s12 m12 l12">
								<h4 class="admission-form-heading-margin">User Settings</h4>
								</div>
									
							</div>	
						</div>
						<div class="col s11 m6 l6 offset-l3 offset-m4" id="profile">
							<div class="row row-margin-bottom">
								<div class="card-panel">
                            <h4 class="header2">Profile</h4>
                            <div class="row">
                              <form id="addform" action="#" method="post">
						
									<div class="col s12 m12 l12 Space10"></div>
									<div class="col s12 m3 l3">
										<label class="NameLable">Name</label>
									</div>
									<input type="hidden" name="userid" id="userid" value="">
									<div class="col s12 m8 l8 no-padding-origin">
											<input type="text" name="username" id="username" value="" placeholder="Enter Name">
									</div>
									<div class="col s12 m12 l12 Space10"></div>
									<div class="col s12 m3 l3">
										<label class="NameLable">Email</label>
									</div>
									<div class="col s12 m8 l8 no-padding-origin">
										<input type="text" id="useremail" name="useremail" value="" placeholder="Enter Email" >
									</div>
									<div class="col s12 m12 l12 Space10"></div>
								
									<div class="col s12 m8 l8 offset-l3 offset-m3 no-padding-origin">
										<i class="waves-effect waves-light btn btn-form-submit Searchbtn center-align up-save-btn waves-input-wrapper" style="">
										<input type="button" value="Save" class="waves-button-input" id="save_profile" style="margin-top:8px"></i>
										<i class="waves-effect waves-light btn btn-form-submit Searchbtn center-align up-save-btn waves-input-wrapper" style="">
										<input type="button" value="Cancel" class="waves-button-input" id="save_cancel" style="margin-top:8px"></i>
										<i class="waves-effect waves-light btn btn-form-submit Searchbtn center-align up-save-btn waves-input-wrapper" style="margin-left:9px;background-color:#1A74B2">
										<input type="button" value="Change pass" class="waves-button-input" id="change_pass" style="margin-top:8px;"></i>
									</div>
									<div class="col s12 m12 l12 Space10"></div>
								
								</form>
                            </div>
                        </div>
							</div>	
						</div>
						
						
						<div class="col s11 m6 l6 offset-l3 offset-m4" id="changepass">
							<div class="row row-margin-bottom">
								<div class="card-panel">
                            <h4 class="header2">Change Password</h4>
                            <div class="row">
                              <form id="addform" action="#" method="post">
						
									<div class="col s12 m12 l12 Space10"></div>
									<div class="col s12 m3 l3">
										<label class="NameLable">Old Password</label>
									</div>
									<input type="hidden" name="userid" id="userid" value="">
									<div class="col s12 m8 l8 no-padding-origin">
											<input type="password" name="oldpass" id="oldpass" value="" placeholder="Enter Old Password">
									</div>
									<div class="col s12 m12 l12 Space10"></div>
									<div class="col s12 m3 l3">
										<label class="NameLable">New Password</label>
									</div>
									<div class="col s12 m8 l8 no-padding-origin">
										<input type="password" id="password" name="password" value="" placeholder="Enter New Password" >
									</div>
									
									<div class="col s12 m12 l12 Space10"></div>
									<div class="col s12 m3 l3">
										<label class="NameLable">Confirm  Password</label>
									</div>
									<div class="col s12 m8 l8 no-padding-origin">
										<input type="password" id="cpassword" name="cpassword" value="" placeholder="Enter Confirm  Password" >
									</div>
									
									<div class="col s12 m12 l12 Space10"></div>
								
									<div class="col s12 m8 l8 offset-l3 offset-m3 no-padding-origin">
										<i class="waves-effect waves-light btn btn-form-submit Searchbtn center-align up-save-btn waves-input-wrapper" style="">
										<input type="button" value="Save" class="waves-button-input" id="save_changepass" style="margin-top:8px"></i>
										<i class="waves-effect waves-light btn btn-form-submit Searchbtn center-align up-save-btn waves-input-wrapper" style="">
										<input type="button" value="Cancel" class="waves-button-input" id="save_cancel" style="margin-top:8px"></i>
										
										<i class="waves-effect waves-light btn btn-form-submit Searchbtn center-align up-save-btn waves-input-wrapper" style="margin-left:9px;background-color:#1A74B2">
										<input type="button" value="profile" class="waves-button-input" id="change_pro" style="margin-top:8px;"></i>
									</div>
									<div class="col s12 m12 l12 Space10"></div>
								
								</form>
                            </div>
                        </div>
							</div>	
						</div>
					</div>
				</div>
			</div>
		</div>	
		
		
	</div>
</div>
</section>
<script src="<?php echo base_url()?>assets/js/plugins/jquery-validation/jquery.validate.min.js"></script>
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/1.3.1/js/toastr.js"></script>
<script>
 
     $("#formValidate").validate({
        rules: {
            uname: {
                required: true,
                minlength: 5
            },
            cemail: {
                required: true,
                email:true
            },
            password: {
				required: true,
				minlength: 5
			},
			cpassword: {
				required: true,
				minlength: 5,
				equalTo: "#password"
			},
			curl: {
                required: true,
                url:true
            },
            crole:"required",
            ccomment: {
				required: true,
				minlength: 15
            },
            cgender:"required",
			cagree:"required",
        },
        //For custom messages
        messages: {
            uname:{
                required: "Enter a username",
                minlength: "Enter at least 5 characters"
            },
            curl: "Enter your website",
        },
        errorElement : 'div',
        errorPlacement: function(error, element) {
          var placement = $(element).data('error');
          if (placement) {
            $(placement).append(error)
          } else {
            error.insertAfter(element);
          }
        }
     });
</script>
<script>
get_data();
$('#changepass').hide();
function get_data()
{
	 var id='<?php echo $userdata['id'];?>'
	 var rid='<?php echo $userdata['role'];?>'
	// console.log(rid);
	 	 $.ajax({
				type: "POST",
				url: "<?php echo base_url('academic/get_user_details'); ?>",
				data:{userid:id,roleid:rid},
				success: function(response){
					var res = JSON.parse(response);
					//console.log(res[0].name);
					$.each(res,function(i,v){
						//console.log(v.name);
						$('#username').val(v.name);
						$('#useremail').val(v.email);
						$('#userid').val(v.id);
					})
				}
	});
}
$('#change_pass').click(function(){
	$('#profile').hide('slow');
	$('#changepass').show('slow');
});
$('#change_pro').click(function(){
	$('#profile').show('slow');
	$('#changepass').hide('slow');
});

$('#save_profile').click(function(){
	var user=$('#username').val();
	var email=$('#useremail').val();
	var userid=$('#userid').val();
	var dataS={
		username:user,
		useremail:email,
		userid:userid
	};
	

	if(user=='')
	{
		$('#username').focus();
		return false;
	}else if(email=='')
	{
		$('#useremail').focus();
		return false;
	}
	else
	{
			
			 $.ajax({
				type: "POST",
				url: "<?php echo base_url('academic/update_user_setting'); ?>",
				data:dataS,
				success: function(response){
					var res = JSON.parse(response);
						toastr.success("Updated Successfully");
			setTimeout(function(){
		 get_data();	
	
		},100);
				}
	});
	}
});


$('#save_changepass').click(function(){
	var oldpass=$('#oldpass').val();
	var newpass=$('#password').val();
	var cpass=$('#cpassword').val();
	var userid=$('#userid').val();
	var roleid=<?php echo $userdata['role'];?>;
	var dataS={
		oldpass:oldpass,
		newpass:newpass,
		userid:userid,
		roleid:roleid
	};
	

	if(oldpass=='')
	{
		$('#oldpass').focus();
		toastr.success("Enter old password");
		return false;
	}else if(newpass=='')
	{
		$('#password').focus();
		toastr.success("Enter new nassword");
		return false;
	}
	else if(cpass!=newpass)
	{
		$('#cpassword').focus();
		toastr.success("Password does not match");
		return false;
	}
	else
	{
			
			 $.ajax({
				type: "POST",
				url: "<?php echo base_url('academic/change_pass'); ?>",
				data:dataS,
				success: function(response){
					var res = JSON.parse(response);
					console.log(res);
					if(res.msg=='Invalid old password')
					{
						toastr.error(res.msg);
						return false;
					}
					
						toastr.success('Password changed successfully');
							setTimeout(function(){
								$('#changepass').hide('slow');
								$('#profile').show('slow');
								$('#oldpass').val('');
								$('#password').val('');
								$('#cpassword').val('');
						 get_data();	
					
						},100);
					
					
						// toastr.success("Updated Successfully");
			// setTimeout(function(){
		 // get_data();	
	
		// },100);
				}
	});
	}
});
</script>