<style>

.text-white {
    color: #fff;
}
.text-center {
    text-align:center;
}
.Space10 {
    height: 10px !important;
}
.offset-l1 {
    margin-left: 1.3% !important;
}
.show-more {
    color: #F26223;
}
.card-custom {
    margin: 0.4rem 0 0.4rem 0 !important;
}
.container .row {
    margin-bottom: 12px;
}
.apply-now {
    padding: 3px !important;
    color: #ffffff;
    font-size: 14px;
    font-weight: 500;
    background-color: #F26223 !important;
}
.course-icons {
    padding: 0 0rem !important;
    color: #5D6F71;
    font-size: 14px;
    font-weight: 500;
}
.inner-container {
    margin-left: 30px !important;
    margin-right: 30px !important;
}
.container .row {
    margin-left: -0.75rem;
    margin-right: -0.75rem;
}
.card {
    position: relative;
    margin: 0.5rem 0 1rem 0;
    background-color: #fff;
    transition: box-shadow .25s;
    border-radius: 2px;
}
.ucourse-details {
    border-right: 1px solid #ACBDBC;
    border-bottom: 1px solid #ACBDBC;
    border-top: 1px solid #ACBDBC;
    border-left: 1px solid #ACBDBC;
    background-color: #F6F7F9;
    color: #4580B8;
}

 .input-field div.error{
    position: relative;
    top: -1rem;
    left: 3rem;
    font-size: 0.8rem;
    color:#FF4081;
    -webkit-transform: translateY(0%);
    -ms-transform: translateY(0%);
    -o-transform: translateY(0%);
    transform: translateY(0%);
  }
.div-registertab{
	display:none;
}
.banner-title-name {
    color: #ffffff;
}
.banner-colg-name {
    z-index: 0;
    margin: -8% auto auto auto;
}
.colg-name-div {
    background-color: #4D7AA6;
    opacity: 0.9;
}
#chart-dashboard {
    padding-top: 25px;
}
.utitle-img {
    padding: 9px !important;
}
#content .container .row {
    margin-bottom: 10px;
}
.userlink {
    cursor: pointer;
    margin-left: 45%;
}
</style>
<?php //print_r($uniFacility);?>
<div class="university-bg"> 		
			<div class="col s12 l12 m12">
					<img src="<?php echo base_url('uploads/coverimages/'.$universData->ui_coverimage); ?>" class="responsive-img banner banner-width" style="width:100%"/>
				</div>
				<div class="col s12 l12 m12 banner-colg-name">
					<div class="row">
					<div class="col s12 l6 m6 colg-name-div">
						<div class="col s12 l8 m8 offset-l4">
							<h2 class="banner-title-name" style="margin-left: 25px;margin-top: 13px;"><?php echo $universData->um_name; ?></h2>	
							<div class="col s4 m1 l1 banner-title-name">
								<i class="mdi-action-room"></i>
							</div>
							<div class="col s6 m11 l11 banner-title-name banner-addres" style="margin-left:-14px">
								<?php echo substr($universData->ui_address,0,58).'...'; ?>
							</div>
						</div>
					</div>
					<div class="col s12 l6 m6"></div>
					</div>
				</div>
			<!-- START CONTENT -->
            <section id="content" style=" background-color: #e0e0e0;">
<div class="row">
	<div class="col s12 l12 m12">
                <!--start container-->
                <div id="container">

                    <!--chart dashboard start-->
                    <div id="chart-dashboard">
						
                            <div class="col s12 m12 l12 university-card" style="margin-top:16px;">
                                <div class="card card-custom">
									<div class="row inner-container university-card">
										<div class="col s4 m1 l1 center-align utitle-img">
											<img src="<?php echo base_url('assets/images/course-icon.jpg');?>" class="responsive-img" style="width: 25px;margin-top: 7px;margin-left: -80px;"/>
										</div>
										<div class="col s6 m11 l11 utitle-name" style="margin-top: 20px;font-weight: bold;">
											<h3><b style="margin-left: -81px;">Course</b></h3>	
										</div>
									</div>
	<?php //echo '<pre>', print_r($uniCourse);?>
	<?php $i=0;foreach($uniCourse as $uniCrs){
		
				if($uniCrs->uc_is_approved ==1 ){
					
				$crsStream = $this->academic_model->getCourseById($uniCrs->uc_parent);
				 //echo '<pre>', print_r($crsStream);
				$crsProgram	= $this->academic_model->getSpe_uni($uniCrs->uc_com_id);
				$type	= $this->academic_model->getTypeProgram($uniCrs->uc_course_type);
				// echo '<pre>', print_r($crsProgram);
				$uc_fees = explode(',',$uniCrs->uc_fees);
				// echo '<pre>', print_r($uc_fees);
				$umId = $uniCrs->uc_um_id;
				
				for($i=0;$i<count($uc_fees);$i++){	
					
					$LumpsumFeesData = $this->academic_model->getLumsumpFees($uc_fees[$i],$umId,'Lumpsum');
					
					$InstallmentFeesData = $this->academic_model->getInstallmentFees($uc_fees[$i],$umId,'Installment');
						
				}
				?>
				<div class="row inner-container ucourse-details" style="margin-bottom: -26px;margin-top: -17px;">
					<div class="col s12 m12 l12 Space10"></div>
					<div class="row">
						<div class="col s12 m12 l12" >
							<div class="col s12 m13 l3 offset-l1 inner-title">
								<?php echo $crsStream->name; ?> - <?php echo $crsProgram[0]->name; ?>
							</div>
							<!--div class="col s12 m12 l2 ustar">
								<img src="<?php echo base_url('assets/images/stars-01.png'); ?>" class="responsive-img" />
								<img src="<?php echo base_url('assets/images/stars-01.png'); ?>" class="responsive-img" />
								<img src="<?php echo base_url('assets/images/stars-01.png'); ?>" class="responsive-img" />
								<img src="<?php echo base_url('assets/images/stars-01.png'); ?>" class="responsive-img" />
								<img src="<?php echo base_url('assets/images/stars-02.png'); ?>" class="responsive-img" />
							</div-->
							<!--div class="col s12 m1 l1 ureview">26 Reviews</div-->
							
						</div>
					</div>
					<hr>
					<div class="row">
						<div class="col s12 m12 l12 Space10"></div>
						<div class="col s12 m1 l1 offset-l1">
							<div class="row">
								<div class="col s5 m5 l5 center-align">
								<img src="<?php echo base_url('assets/images/icon-01.png'); ?>" class="responsive-img" />
								</div>
								<div class="col s7 m7 l7 no-padding-origin course-icons" style="margin-top: -4px;">
								<?php echo $uniCrs->uc_course_length; ?>
								</div>
							</div>
						</div>
						<div class="col s12 m1 l1">
							<div class="row">
								<div class="col s5 m5 l5 center-align">
								<img src="<?php echo base_url('assets/images/icon-02.png'); ?>" class="responsive-img" />
								</div>
								<div class="col s7 m7 l7 no-padding-origin course-icons" style="font-size: 12px;margin-top: -4px;">
								<?php echo $type[0]->name; ?>
								</div>
							</div>
						</div>
						<div class="col s12 m1 l1">
							<div class="row">
								<div class="col s5 m5 l5 center-align">
								<img src="<?php echo base_url('assets/images/icon-03.png'); ?>" class="responsive-img" />
								</div>
								<div class="col s7 m7 l7 no-padding-origin course-icons" style="font-size: 12px;margin-top: -4px;">
								HSC
								</div>
							</div>
						</div>
						<div class="col s12 m1 l1">
							<div class="row">
								<div class="col s5 m5 l5 center-align">
								<img src="<?php echo base_url('assets/images/icon-04.png'); ?>" class="responsive-img" />
								</div>
								<div class="col s7 m7 l7 no-padding-origin course-icons" style="font-size: 12px;margin-top: -4px;">
								CET
								</div>
							</div>
						</div>
						<div class="col s12 m2 l1">
							<div class="row">
								<div class="col s5 m5 l5 center-align">
								<img src="<?php echo base_url('assets/images/icon-05.png'); ?>" class="responsive-img" />
								</div>
								<div class="col s7 m7 l7 no-padding-origin course-icons" style="font-size: 14px;margin-top: -4px;">
								<?php /* if(isset($LumpsumFeesData)){ 
										echo $LumpsumFeesData->ufeem_amount;
								}else{
									$InstallmentFeesData->ufeem_amount;
								} */ ?><!--span>  &nbsp;Per Year</span-->
								</div>
							</div>
						</div>
						<div class="col s12 m1 l1">
							<p class="show-more">Show More</p>
						</div>
						<div class="col s12 m2 l2 offset-l1">
							<div class="row">
								<div class="col s5 m3 l3 center-align">
								<img src="<?php echo base_url('assets/images/icon-06.png'); ?>" class="responsive-img" />
								</div>
								<div class="col s7 m7 l7 no-padding-origin course-icons">
								Download Brochure
								</div>
							</div>
						</div>
						<?php $userdata = $this->session->userdata('logged_in'); ?>
						<div class="col s12 m2 l2">
							<div class="row">
								<div class="col s5 m3 l3 center-align">
								<img src="<?php echo base_url('assets/images/icon-07.png'); ?>" class="responsive-img" />
								</div>
								<?php if(!$userdata){ ?>
								<div class="col s7 m7 l7 no-padding-origin course-icons">
									<a class="modal-trigger applynow" href="<?php echo base_url('login/index')?>"" id="shortlist-<?php echo $uniCrs->uc_id; ?>">Shortlist </a>
								</div>
								<?php }else{ ?>
								 <!-- Modal Trigger -->
								<?php
								$user_id=$userdata['id'];
								$course_id=$uniCrs->uc_id;
								$query = $this->db->get_where('student_mstr',array('stm_user_id'=>$user_id));
								$smdata=$query->row();
		
							$userid=$smdata->stm_id;
							
							$query = $this->db->get_where('shortlisted_courses',array('sc_stm_id'=>$userid,'sc_uc_id'=>$course_id));
								if($query->num_rows() > 0)
								{ 
							?>
							<div class="col s7 m7 l7 no-padding-origin course-icons">
														<a class="modal-trigger" >Shortlisted </a>
							</div>
							<?php
								
								}
								else{
									?>
									<div class="col s7 m7 l7 no-padding-origin course-icons">
														<a class="modal-trigger" onclick="shor(<?php echo $uniCrs->uc_id; ?>,<?php echo $userdata['id']; ?>)" >Shortlist </a>
													</div>
									<?php
								  
								}
													?>
			
								<?php } ?>
								
							</div>
						</div>
						<div class="col s8 m1 l1">
							<div class="row">
								<div class="col s12 m10 l10 center-align offset-s2 apply-now">
								<?php if(!$userdata){ ?>
								<a class="modal-trigger text-white " href="<?php echo base_url('login/index')?>" id="apply-<?php echo $uniCrs->uc_id; ?>">
								<b style="color:white">Apply Now</b>  </a>
								<?php }else{ ?>
								
									<?php
								$user_id=$userdata['id'];
								$course_id=$uniCrs->uc_id;
								$query = $this->db->get_where('student_mstr',array('stm_user_id'=>$user_id));
								$smdata=$query->row();
		
							$userid=$smdata->stm_id;
							
							$query = $this->db->get_where('student_applications',array('sa_stm_id'=>$userid,'sa_uc_id'=>$course_id));
								if($query->num_rows() > 0)
								{ 
							?>
							<a class="modal-trigger text-white "  id="<?php echo $uniCrs->uc_id; ?>" >
								<b style="color:white" >Applied</b> </a>
							<?php
								
								}
								else{
									?>
									<a class="modal-trigger text-white "  id="<?php echo $uniCrs->uc_id; ?>" >
								<b style="color:white" onclick="apply(<?php echo $uniCrs->uc_id; ?>,<?php echo $userdata['id']; ?>)">Apply Now</b> </a>
									<?php
								  
								}
													?>
			
								<?php } ?>
								
								
								
								
								
								<div id="modal-course-<?php echo $uniCrs->uc_id; ?>" class="modal" style="    height: 50%;">
								  <div class="modal-header"><a href="#" class="waves-effect waves-red btn-flat modal-action modal-close" style="    margin-left: 659px;"><i class="material-icons dp48"><b style="color:white">close</b></i></a></div>
								  <div >
									<h5 style="color: #000;margin-top: 47px;font-size: 20px;    margin-bottom: 50px;" class="text-center">Are you sure to apply ? </h5>
									<form action="<?php echo base_url('student/course_apply'); ?>" method="post">
										<div class="row center">
											<a class="btn waves-effect waves-light blue div12-btn modal-close">Cancel</a>
											<input type="hidden" name="course_id" value="<?php echo $uniCrs->uc_id; ?>">
											<input type="hidden" name="user_id" value="<?php echo $userdata['id']; ?>">
											<input type="submit" class="btn waves-effect waves-light green div12-btn" value="Ok" style="margin-top: -8px;font-weight: bold;"/>
										</div>
									</form>
								  </div>
								</div>
								<?php } ?>
								</div>
								<div class="col s12 m2 l2"></div>
							</div>
						</div>
						
						<div class="col s12 m12 l12 Space10"> </div>
					</div>
				</div>
				<div class="col s12 m12 l12 Space10 white-bg"></div>
				<?php } 
			 ?>
                                </div>
                            </div>
							
				<div id="modal1" class="modal" style="    height: 50%;">
				  <div class="modal-header"><a href="#" class="waves-effect waves-red btn-flat modal-action modal-close" style="    margin-left: 659px;">X</a></div>
				  <div class="">
					<h4 class="text-center" style="margin-top: -37px;"><b>ENTER YOUR DETAILS</b></h4>
					<div id="div12-content-action" class="div-registertab">
						<form id="registerForm" action="<?php echo base_url('login/after_apply'); ?>" method="post">
							<div class="row center">
								<div class="row center black-text">
									<div class="col l6">
										<div class="row margin">
										  <div class="input-field col s12">
											<i class="mdi-social-person-outline prefix"></i>
											<input id="uname" type="text" name="uname" placeholder="Full Name">
										
										  </div>
										</div>
										<div class="row margin">
										  <div class="input-field col s12">
											<i class="mdi-communication-email prefix"></i>
											<input id="cemail" type="email" name="cemail" placeholder="Email">
											
										  </div>
										</div>
									</div>
									<div class="col l6">
										<div class="row margin">
										  <div class="input-field col s12">
											<i class="mdi-action-lock-outline prefix"></i>
											<input id="password" type="password" name="password" placeholder="Password">
											
										  </div>
										</div>
										<div class="row margin">
										  <div class="input-field col s12">
											<i class="mdi-action-lock-outline prefix"></i>
											<input id="password-again" type="password" name="confirm_password" placeholder="Confirm Password">
											<label for="password-again"></label>
										  </div>
										</div>
									</div>									
								</div>
							</div>
							<div class="row center">
								<input type="submit" class="btn waves-effect waves-light green div12-btn" value="Submit"/>
							</div>
						</form>
						<a class="userlink" id="logintab">LOGIN</a>
					</div>
					<div id="div12-content-action" class="div-logintab">
						<form id="loginForm" action="<?php echo base_url('login/after_apply_login'); ?>" method="post">
							<div class="row center">
								<div class="row center black-text">
									<div class="col l6">
										<div class="row margin">
										  <div class="input-field col s12">
											<i class="mdi-communication-email prefix"></i>
											<input id="email" type="email" name="email" placeholder="Emails">
											
										  </div>
										</div>
									</div>
									<div class="col l6">
										<div class="row margin">
										  <div class="input-field col s12">
											<i class="mdi-action-lock-outline prefix"></i>
											<input id="password" type="password" name="password" placeholder="Password">
											
										  </div>
										</div>
									</div>									
								</div>
							</div>
							<div class="row center">
								<input type="submit" class="btn waves-effect waves-light green div12-btn" value="Submit"/>
							</div>
						</form>
						<a class="userlink" id="registertab">SIGN UP</a>
					</div>
				  </div>
				</div>
				
				
				
							<div class="col s12 m12 l12">
                                <div class="card card-custom">
									<div class="row inner-container">
										<div class="col s4 m1 l1 center-align utitle-img">
											<img src="<?php echo base_url('assets/images/facilities-icon.jpg');?>" class="responsive-img" style="margin-left: -84PX;width: 25px;margin-top: 4px;"/>
										</div>
										<div class="col s6 m11 l11 utitle-name" style="margin-top: 20px;font-weight: bold;">
											<h3><b style="margin-left:-81px">Facilities</b></h3>	
										</div>
										<div class="row">
											<div class="col s12 m12 l12" style="margin-top: 14px;margin-left: 84px;">
												<?php if(in_array('facility_computer_lab',$uniFacility)){ ?>
												<div class="col s12 m1 l1 offset-l1">
													<div class="row">
														<div class="col s12 m12 l12 center-align">
														<img src="<?php echo base_url('assets/images/facility-icon-01.png'); ?>" class="responsive-img" />
														</div>
														<div class="col s12 m12 l12 utext-color center-align">
														Camp Labs
														</div>
													</div>
												</div>
												<?php } 
												if(in_array('facility_library',$uniFacility)){
												?>
												<div class="col s12 m1 l1">
													<div class="row">
														<div class="col s12 m12 l12 center-align">
														<img src="<?php echo base_url('assets/images/facility-icon-02.png'); ?>" class="responsive-img" />
														</div>
														<div class="col s12 m12 l12 utext-color center-align">
														Library
														</div>
													</div>
												</div>
												<?php } 
												if(in_array('facility_laboratory',$uniFacility)){
												?>
												<div class="col s12 m1 l1">
													<div class="row">
														<div class="col s12 m12 l12 center-align">
														<img src="<?php echo base_url('assets/images/facility-icon-03.png'); ?>" class="responsive-img" />
														</div>
														<div class="col s12 m12 l12 utext-color center-align">
														Laboratory
														</div>
													</div>
												</div>
												<?php } 
												if(in_array('facility_studyroom',$uniFacility)){
												?>
												<div class="col s12 m1 l1">
													<div class="row">
														<div class="col s12 m12 l12 center-align">
														<img src="<?php echo base_url('assets/images/facility-icon-04.png'); ?>" class="responsive-img" />
														</div>
														<div class="col s12 m12 l12 utext-color center-align">
														Auditorium
														</div>
													</div>
												</div>
												<?php } 
												if(in_array('facility_sport',$uniFacility)){
												?>
												<div class="col s12 m1 l1 offset-l1">
													<div class="row">
														<div class="col s12 m12 l12 center-align">
														<img src="<?php echo base_url('assets/images/facility-icon-05.png'); ?>" class="responsive-img" />
														</div>
														<div class="col s12 m12 l12 utext-color center-align">
														Sport
														</div>
													</div>
												</div>
												<?php } 
												if(in_array('facility_gym',$uniFacility)){
												?>
												<div class="col s12 m1 l1">
													<div class="row">
														<div class="col s12 m12 l12 center-align">
														<img src="<?php echo base_url('assets/images/facility-icon-06.png'); ?>" class="responsive-img" />
														</div>
														<div class="col s12 m12 l12 utext-color center-align">
														Gym
														</div>
													</div>
												</div>
												<?php } 
												if(in_array('facility_medical',$uniFacility)){
												?>
												<div class="col s12 m1 l1">
													<div class="row">
														<div class="col s12 m12 l12 center-align">
														<img src="<?php echo base_url('assets/images/facility-icon-07.png'); ?>" class="responsive-img" />
														</div>
														<div class="col s12 m12 l12 utext-color center-align">
														Medical
														</div>
													</div>
												</div>
												<?php } 
												if(in_array('facility_canteen',$uniFacility)){
												?>
												<div class="col s12 m1 l1">
													<div class="row">
														<div class="col s12 m12 l12 center-align">
														<img src="<?php echo base_url('assets/images/facility-icon-08.png'); ?>" class="responsive-img" />
														</div>
														<div class="col s12 m12 l12 utext-color center-align">
														Cafeteria
														</div>
													</div>
												</div>
												<?php } 
												if(in_array('facility_transport',$uniFacility)){
												?>
												<div class="col s12 m1 l1">
													<div class="row">
														<div class="col s12 m12 l12 center-align">
														<img src="<?php echo base_url('assets/images/facility-icon-10.png'); ?>" class="responsive-img" />
														</div>
														<div class="col s12 m12 l12 utext-color center-align">
														Transport
														</div>
													</div>
												</div>
												<?php } 
												if(in_array('facility_hostel',$uniFacility)){
												?>
												<div class="col s12 m1 l1">
													<div class="row">
														<div class="col s12 m12 l12 center-align">
														<img src="<?php echo base_url('assets/images/facility-icon-11.png'); ?>" class="responsive-img" />
														</div>
														<div class="col s12 m12 l12 utext-color center-align">
														Hostel
														</div>
													</div>
												</div>
												<?php } 
												?>
											</div>
											<div class="col s12 m12 l12 Space10"></div>
										</div>	
									</div>
								</div>
							</div>
					
							
							<div class="col s12 m12 l12">
                                <div class="card card-custom">
									<div class="row inner-container">
										<div class="col s4 m1 l1 center-align utitle-img">
											<img src="<?php echo base_url('assets/images/about-collegeicon.jpg');?>" class="responsive-img" style="width: 28px; margin-left: -80px"/>
										</div>
										<div class="col s6 m11 l11 utitle-name" style="margin-top: 20px;font-weight: bold;">
											<h3><b style="margin-left:-78px">About College</b></h3>	
										</div>
										<div class="col s12 m11 l11 offset-l1">
											<p style="    margin-left: 13px;"><?php echo $universData->ui_aboutus; ?></p>
										</div>
											<div class="col s12 m12 l12 center-align">
												<p class="show-more">Show More</p>
											</div>
									</div>
								</div>
							</div>
							<div class="col s12 m12 l12">
                                <div class="card card-custom">
									<div class="row inner-container">
										<div class="col s4 m1 l1 center-align utitle-img">
											<img src="<?php echo base_url('assets/images/Gallery-icon.jpg');?>" class="responsive-img" style="width: 28px; margin-left: -80px;margin-top: 6px;"/>
										</div>
										<div class="col s6 m11 l11 utitle-name" style="margin-top: 20px;font-weight: bold;">
										<h3><b style="margin-left:-80px">Gallery</b></h3>	
										</div>
										<div style="margin-left:80px">
										<div class="row">
										<div class="col-md-6"><b>Photo</b></div>
										<div class="col-md-6"><b>Video</b></div>
											
											<?php foreach($uniMedia as $uMed){
												if($uMed->ugm_media_type == 'PHOTO'){ ?>
											<div class="col-md-3">
												<img  src="<?php echo base_url('uploads/'.$uMed->ugm_link); ?>" class="responsive-img" style="width: 100%;    height: 20%;" />
											</div>
										<?php } } ?>
										
											
											<?php foreach($uniMedia as $uMed){
												if($uMed->ugm_media_type == 'VIDEO'){ ?>
											<div class="col-md-3">
												<video width="200" height="120" controls="">
													<source src="<?php echo base_url('uploads/'.$uMed->ugm_link); ?>" type="video/ogg" >
												</video>
											</div>
											<?php } } ?>	
										</div>
										</div>
										<div class="col s12 m12 l12 Space20"></div>
									</div>
								</div>
							</div>
							<div class="col s12 m12 l12">
                                <div class="card card-custom">
									<div class="row inner-container">
										<div class="col s12 m8 l8">
										<div class="col s4 m1 l1 center-align utitle-img">
											<img src="<?php echo base_url('assets/images/Location-icon.jpg');?>" class="responsive-img" style="width: 28px; margin-left: -80px;margin-top: 6px;"/>
										</div>
										<div class="col s6 m11 l11 utitle-name" style="margin-top: 20px;font-weight: bold;">
											<h3><b style="margin-left:-57px">Location</b></h3>
											<p class="location-p"><?php echo $universData->ui_address; ?></p><hr>
										</div>
										<hr>
										<div class="col s12 m12 l12 Space30"></div>
										<div class="col s4 m1 l1 center-align utitle-img">
											<img src="<?php echo base_url('assets/images/nearby-colleges-icon.jpg');?>" class="responsive-img" style="width: 28px; margin-left: -80px;margin-top: 6px;"/>
										</div>
										<div class="col s6 m11 l11 utitle-name" style="margin-top: 20px;font-weight: bold;">
											<h3><b style="margin-left:-57px">Nearby College</b></h3>	
										</div>
										<div class="col s12 m12 l12 Space30"></div>
										</div>
										<div class="col s12 m4 l4">
										<div class="col s12 m12 l12 Space10"></div>
										<img src="<?php echo base_url('assets/images/similar-college-01.png'); ?>" class="responsive-img" />
										<div class="col s12 m12 l12 Space10"></div>
										</div>
									</div>
								</div>
							</div>
							<div class="col s12 m12 l12">
                                <div class="card card-custom">
									<div class="row inner-container">
										<div class="col s4 m1 l1 center-align utitle-img">
											<img src="<?php echo base_url('assets/images/Similar-Colleges-icon.jpg');?>" class="responsive-img" style="width: 28px; margin-left: -95px;margin-top: 6px;"/>
										</div>
										<div class="col s6 m11 l11 utitle-name" style="margin-top: 20px;font-weight: bold;">
											<h3><b style="margin-left:-79px">Similer College</b></h3>	
										</div>
										<div class="row">
										
											
											<div class="col s12 m12 l12 Space20">
											</div>
											<div class="col s12 m4 l4 offset-l2">
											<img src="<?php echo base_url('assets/images/similar-college-01.png'); ?>" class="responsive-img" />
											</div>
											<div class="col s12 m6 l6">
											<img src="<?php echo base_url('assets/images/similar-college-01.png'); ?>" class="responsive-img" />
											</div>
											</div>
											<div class="row">
											<div class="col s12 m4 l4 offset-l2">
											<img src="<?php echo base_url('assets/images/similar-college-01.png'); ?>" class="responsive-img" />
											</div>
											<div class="col s12 m6 l6">
											<img src="<?php echo base_url('assets/images/similar-college-01.png'); ?>" class="responsive-img" />
											</div>
											<div class="col s12 m12 l12 center-align">
												<p class="show-more">Show More</p>
											</div>
										</div>										
									</div>
								</div>
							</div>
						
				</div>
			</div>	
			</div>
	</div>
			</section>


<?php 

	/* Course Master Search Data START */
	
	$query = $this->db->query("SELECT * FROM course_mstr");
	$course_mstr = $query->result();
	
	$new_crs_mstr = array();  
	foreach($course_mstr as $crs_mstr){
		$crsSearch = trim($crs_mstr->com_name);
		array_push($new_crs_mstr,$crsSearch);
	}
	$new_crs_mstr = array_unique($new_crs_mstr);
	$newCMstr = implode('", "',$new_crs_mstr);
	
	/* Course Master Search Data END */
	
	/* University Master Search Data START */
	
	$query = $this->db->query("SELECT * FROM uni_mstr");
	$uni_mstr = $query->result();
			
  
	$new_uni_mstr = array();  
	foreach($uni_mstr as $u_mstr){
		$uniSearch = trim($u_mstr->um_name);
		array_push($new_uni_mstr,$uniSearch);
	}
	  
	$new_uni_mstr = array_unique($new_uni_mstr);
	  
	$newUMstr = implode('", "',$new_uni_mstr);
	  
	/* University Master Search Data START */
  ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> 
<script type="text/javascript">
	
    var availableTags = ["<?php echo $newCMstr; ?>","<?php echo $newUMstr; ?>"];
	
    $( "#search_text" ).autocomplete({
      source: availableTags,
	  select: function(event, ui) {
		  //alert(ui.item.value);
			window.location.href = "<?php echo base_url(); ?>listing/index/"+ui.item.value;
	  }
    });
	$('.userlink').click(function(){
		 var ltval = $(this).attr('id');
		 if(ltval == 'registertab'){
			 $('.div-registertab').show();
			 $('.div-logintab').hide();
		 }else{
			 $('.div-registertab').hide();
			 $('.div-logintab').show();
		 }
	 });
	 
	 function shor(id,user_id){
		//alert(user_id);
		  // var txt;
			if (confirm("Are you sure ?") == true) {
				//txt = "You pressed OK!";
				 var dataString={
					 'course_id':id,
					 'user_id':user_id
				 };
				console.log(dataString);
				 $.ajax({
					 type:'POST',
					 url:'<?php echo base_url(); ?>student/course_shortlist',
					 data:dataString,
					 success:function(res)
					 {
						 alert(res);
					 }
				 });
				 
			} else {
				//txt = "You pressed Cancel!";
			}
		 
	 }
	 
	  function apply(id,user_id){
		//alert(user_id);
		  // var txt;
			if (confirm("Are you sure ?") == true) {
				//txt = "You pressed OK!";
				 var dataString={
					 'course_id':id,
					 'user_id':user_id
				 };
				console.log(dataString);
				 $.ajax({
					 type:'POST',
					 url:'<?php echo base_url(); ?>student/course_apply',
					 data:dataString,
					 success:function(res)
					 {
						 alert(res);
						  location.reload();
					 }
				 });
				 
			} else {
				//txt = "You pressed Cancel!";
			}
		 
	 }
</script>