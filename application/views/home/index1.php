<section class="page-title ptb-50" style="padding-top:120px">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2>Cources</h2>
                        
                    </div>
                </div>
            </div>
        </section>
		<section class="section-padding">
            <div class="container">

            
              <div class="row">
			    <div class="col-md-2">
                 
              </div>
                <div class="col-md-8">
						<div class="widget widget_search">
                            <form role="search" method="get" class="search-form">
                              <input type="text" class="form-control" value="" name="s" id="search_text" placeholder="Enter Cources Name">
                              <button type="submit"><i class="fa fa-search"></i></button>
                            </form>
							<div class="widget widget_search" >
							<ul class="dropdown" style="right: auto; " id="search_result">
                                    <li>
                                      
                                    </li>
                                   
                                </ul>
							</div>
							
                          </div>
                </div><!-- /.col-md-7 -->

                <div class="col-md-2">
                 
              </div><!-- /.row -->
            </div><!-- /.container -->
        </section>


<?php 

	/* Course Master Search Data START */
	
	$query = $this->db->query("SELECT * FROM course_mstr");
	$course_mstr = $query->result();
	
	$new_crs_mstr = array();  
	foreach($course_mstr as $crs_mstr){
		$crsSearch = trim($crs_mstr->com_name);
		array_push($new_crs_mstr,$crsSearch);
	}
	$new_crs_mstr = array_unique($new_crs_mstr);
	$newCMstr = implode('", "',$new_crs_mstr);
	
	/* Course Master Search Data END */
	
	/* University Master Search Data START */
	
	$query = $this->db->query("SELECT * FROM uni_mstr");
	$uni_mstr = $query->result();
			
  
	$new_uni_mstr = array();  
	foreach($uni_mstr as $u_mstr){
		$uniSearch = trim($u_mstr->um_name);
		array_push($new_uni_mstr,$uniSearch);
	}
	  
	$new_uni_mstr = array_unique($new_uni_mstr);
	  
	$newUMstr = implode('", "',$new_uni_mstr);
	  
	/* University Master Search Data START */
  ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> 
<script type="text/javascript">
	
    var availableTags = ["<?php echo $newCMstr; ?>","<?php echo $newUMstr; ?>"];
	
    $( "#search_text" ).autocomplete({
      source: availableTags,
	  select: function(event, ui) {
		  //alert(ui.item.value);
			window.location.href = "<?php echo base_url(); ?>listing/index/"+ui.item.value;
	  }
    });
</script>