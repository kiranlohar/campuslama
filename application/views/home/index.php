<style>
.ui-helper-hidden-accessible { display:none; }
</style>
<section class="page-title ptb-50" style="padding-top:120px">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2>Search for Courses</h2>
                        
                    </div>
                </div>
            </div>
        </section>
		<section class="section-padding" style="background-color:white">
            <div class="container">

            
              <div class="row">
			    <div class="col-md-2">
                 
              </div>
                <div class="col-md-8">
						<div class="widget widget_search">
                            <form role="search" method="get" class="search-form">
                              <input type="text" class="form-control" value="" name="s" id="search_text" placeholder="Search for Courses">
                              <button type="submit"><i class="fa fa-search"></i></button>
                            </form>
							<div class="widget widget_search" >
							<ul class="dropdown" style="right: auto; " id="search_result">
                                    <li>
                                      
                                    </li>
                                   
                                </ul>
							</div>
							
                          </div>
                </div><!-- /.col-md-7 -->

                <div class="col-md-2">
                 
              </div><!-- /.row -->
            </div><!-- /.container -->
        </section>


<?php 

	/* Course Master Search Data START */
	
	$query = $this->db->query("SELECT * FROM master_program");
	$course_mstr = $query->result();
	
	$new_crs_mstr = array();  
	foreach($course_mstr as $crs_mstr){
		$crsSearch = trim($crs_mstr->name);
		array_push($new_crs_mstr,$crsSearch);
	}
	$new_crs_mstr = array_unique($new_crs_mstr);
	$newCMstr = implode('", "',$new_crs_mstr);
	
	/* Course Master Search Data END */
	
	/* University Master Search Data START */
	
	$query = $this->db->query("SELECT * FROM uni_mstr");
	$uni_mstr = $query->result();
			
  
	$new_uni_mstr = array();  
	foreach($uni_mstr as $u_mstr){
		$uniSearch = trim($u_mstr->um_name);
		array_push($new_uni_mstr,$uniSearch);
	}
	  
	$new_uni_mstr = array_unique($new_uni_mstr);
	  
	$newUMstr = implode('", "',$new_uni_mstr);
	  
	/* University Master Search Data START */

	//spelizatiopn
	  $query1 = $this->db->query("SELECT * FROM master_specialization");
	$uni_mstr1 = $query1->result();
			
  
	$new_uni_mstr1 = array();  
	foreach($uni_mstr1 as $u_mstr1){
		$uniSearch1 = trim($u_mstr1->name);
		array_push($new_uni_mstr1,$uniSearch1);
	}
	  
	$new_uni_mstr1 = array_unique($new_uni_mstr1);
	  
	$newUMstr1 = implode('", "',$new_uni_mstr1);
  ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> 
<script type="text/javascript">
	
    var availableTags = ["<?php echo $newCMstr; ?>","<?php echo $newUMstr; ?>","<?php echo $newUMstr1; ?>"];
//var availableTags = ["<?php echo $newCMstr; ?>"];

	
    $( "#search_text" ).autocomplete({
      source: availableTags,
	  select: function(event, ui) {
		//  alert(ui.item.value);
			window.location.href = "<?php echo base_url(); ?>listing/index/"+ui.item.value;
	  }
    });
</script>