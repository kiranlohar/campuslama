<style type="text/css">
  .modal {
    display: none;
    position: fixed;
    left: 0;
    right: 0;
    background-color: #a7a0a000;
    padding: 24px;
    max-height: 100%;
    width: 100%;
    margin: auto;
    overflow-y: auto;
    z-index: 1000;
    -webkit-border-radius: 2px;
    -moz-border-radius: 2px;
    border-radius: 2px;
    background-clip: padding-box;
    -webkit-transform: translate(0);
    -moz-transform: translate(0);
    -ms-transform: translate(0);
    -o-transform: translate(0);
    transform: translate(1);
    will-change: top;
    
opacity: 1.5;
}
.modal-overlay {
    position: fixed;
    z-index: 999;
    top: -25%;
    left: 0;
    bottom: 0;
    right: 0;
    height: 125%;
    width: 100%;
    background: #000;
    display: none;
    will-change: opacity;
    }
   label {
    font-size: 12px;
    color: black;
}
.carousel-inner .active.left { left: -25%; }
.carousel-inner .next        { left:  25%; }
.carousel-inner .prev    { left: -25%; }
.carousel-control        { width:  4%; }
.carousel-control.left,.carousel-control.right {margin-left:15px;background-image:none;}
#sticky {
   
background-color: #f5f5f5;
    color: black;
     width: 100%;
}

#sticky.stick {
    margin-top: 0 !important;
    position: fixed;
    top: 0px;
  width: 102%;
  z-index: 10000;
  box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
   
}

</style>
<section class="page-title ">
            <div id="container" >
              <div class="" id="sticky-anchor"></div>
                <div class="row" >
                    <div class="col-md-12">
                        
                      <!--   <ol class="breadcrumb"> -->
                            <a href="#" ><img src="<?php echo base_url()?>assets_new/img/courses-img8.jpg" style="width:100%;margin-top:100px" ></a>
                           <!--  <li class="active">FAQ Page 2</li> -->
                        <!-- </ol> -->
                    </div>
                    
                </div>
                 <div class="row " id="sticky">
                   <div class="col-md-3 col-md-offset-1">
                         <?php //echo '<pre>', print_r($details[0]);?>
                    <span style="font-size:20px;color:black;margin-top: 25px;" style="margin-top:20px"> <?php echo $details[0]->spe; ?>  - <?php echo $details[0]->course_spe;?></span>
                   <p><i class="fa fa-clock-o" aria-hidden="true"></i>&nbsp;&nbsp;Duration : <?php echo $details[0]->uc_course_length; ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  <i class="fa fa-desktop" aria-hidden="true"></i>&nbsp;&nbsp; Mode :<?php echo $details[0]->type; ?></p> <p></p>
                    </div>
                      <div class="col-md-3 " >
                        
                  
                      <span style="font-size:20px;color:#f5f5f5" style="margin-top:20px"> MANAGEMENT  - MBA</span>
                   <p><i class="fa fa-building-o" aria-hidden="true"></i>&nbsp;&nbsp;<a style="color:black" href="<?php echo base_url()?>listing/index/<?php echo $details[0]->um_name; ?>"><?php echo $details[0]->um_name; ?> </p> <p></p>
                    </div>
                    <div class="col-md-3 " >
                        <?php  $userdata = $this->session->userdata('logged_in'); ?>
                    <?php if(!$userdata){ ?>
                <!-- <a class="modal-trigger text-white " href="<?php echo base_url('login/index')?>" id="apply-<?php echo $uniCrs->uc_id; ?>"> -->
                <!-- <b style="color:white">Apply Now</b>  </a> -->
                   <p>&nbsp;&nbsp;<a data-toggle="modal" data-target="#exampleModal" data-id="<?php echo $details[0]->uc_id; ?>" data-name="apply"  data-whatever="@mdo" href="#" id="apply-<?php echo $details[0]->uc_id; ?>"  style="margin-top: 21px;background:orange" class="btn btn-lg text-capitalize waves-effect waves-light apply_modal">Apply</a></p> <p></p>
                <?php }else{ ?>
                
                  <?php
               $user_id=$userdata['id'];
                if($userdata['role']==2){
                $course_id=$details[0]->uc_id;
                $query = $this->db->get_where('student_mstr',array('stm_user_id'=>$user_id));
                $smdata=$query->row();
    
               $userid=$smdata->stm_id;
              
              // $verify = $this->db->get_where('users',array('id'=>$user_id));
              // $emailverify=$verify->result();
              // if($verify->num_rows() > 0)
                // { 
              $query = $this->db->get_where('student_applications',array('sa_stm_id'=>$userid,'sa_uc_id'=>$course_id));
                if($query->num_rows() > 0)
                { 
            
              ?>
              <a class="modal-trigger text-white "  id="<?php echo $details[0]->uc_id; ?>" >
                <!-- <b style="color:white" >Applied</b> </a> -->
                  <p>&nbsp;&nbsp;<a href="#" style="margin-top: 21px;color:white" class="btn btn-lg text-capitalize waves-effect waves-light green disabled" >Applied</a></p> <p></p>
              <?php
                
                }
                else{
                  
                  ?>
                  <a class="modal-trigger text-white "  id="<?php echo $details[0]->uc_id; ?>" >
                  <!-- <b style="color:white" onclick="apply(<?php echo $details[0]->uc_id; ?>,<?php echo $userdata['id']; ?>,<?php echo $uniCrs->uc_eligibility ?>)">Apply Now</b> </a> -->
                    <p>&nbsp;&nbsp;<a href="#" onclick="apply(<?php echo $details[0]->uc_id; ?>,<?php echo $userdata['id']; ?>,<?php echo $details[0]->uc_eligibility ?>)" style="margin-top: 21px;" class="btn btn-lg text-capitalize waves-effect waves-light">Apply</a></p> <p></p>
                  <?php
                  
                }
                ?>
      
                <?php
                } }
                ?>
                      <!-- <span style="font-size:20px;color:#f5f5f5" style="margin-top:20px"> MANAGEMENT  - MBA</span> -->
                 
                    </div>
                </div>
                
            </div>
        </section>
<section class="section-padding" style="background:#fff">
            <div class="container">
 <h2 class="mb-30" style="font-weight:bold">Introduction</h2>
              <div class=" mb-80">
                  <!-- <h2 class="section-title text-capitalize">Frequently asked questions</h2> -->
                  <p> <?php echo strip_tags($details[0]->uc_info);?></p>
              </div>

              

              <!-- /.row -->

              <div class="row">
                <div class="col-md-12">
       
                  <div class="panel-group feature-accordion brand-accordion icon angle-icon" id="support">
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h3 class="panel-title">
                          <a data-toggle="collapse" data-parent="#support" href="#support-one">
                            Objective
                          </a>
                        </h3>
                      </div>
                      <div id="support-one" class="panel-collapse collapse in">
                        <div class="panel-body">
                           <?php echo $details[0]->uc_objective?>
                        </div>
                      </div>
                    </div>
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h3 class="panel-title">
                          <a class="collapsed" data-toggle="collapse" data-parent="#support" href="#support-two">
                         Curriculum
                          </a>
                        </h3>
                      </div>
                      <div id="support-two" class="panel-collapse collapse">
                        <div class="panel-body">
                          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Incidunt maiores placeat obcaecati, beatae. Facilis dolore ipsam facere perferendis deserunt commodi blanditiis nisi accusamus omnis, animi vel tenetur cumque, sed veritatis?
                        </div>
                      </div>
                    </div>
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h3 class="panel-title">
                          <a class="collapsed" data-toggle="collapse" data-parent="#support" href="#support-three">
                            Elibibility
                          </a>
                        </h3>
                      </div>
                      <div id="support-three" class="panel-collapse collapse">
                        <div class="panel-body">
                             <?php echo $details[0]->eli?>
                        </div>
                      </div>
                    </div>
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h3 class="panel-title">
                          <a class="collapsed" data-toggle="collapse" data-parent="#support" href="#support-four">
                          Fees
                          </a>
                        </h3>
                      </div>
                      <div id="support-four" class="panel-collapse collapse">
                        <div class="panel-body">
                         <?php echo $details[0]->uc_fees?>
                        </div>
                      </div>
                    </div>
                  </div>                  
                </div>

              </div>
              <hr>
              <?php  //echo count($similar_course);?>
              <div class="row">
                <h2 class="mb-30" style="margin-left: 28px;font-weight:bold;">Related Course</h2>
             
                  <div class="col-md-12 col-md-offset-0" >

                        <div class="carousel carousel-inner flex-active-slide" id="myCarousel" data-interval="3000">
  <div class="carousel-inner">
    <?php
    $active='';
   if(!empty($similar_course)){
    $count=1;
     foreach ($similar_course as $semi) {
   
    ?>
    <div class="item ">
      <div class="col-md-3">
        <figure class="portfolio style-11 relative transition ov-hidden" style="box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);">
          <img src="http://11.helloadmission.in/assets_new/img/01.jpg" class="img-responsive" alt="MBA Branding and Advertising" style="height:170px">
          <div class="portfolio-center" style="display: block;">
            <div class="portfolio-center-inner" style="color:black;font-weight:bold;margin-top:134px;font-size:12px"><?php echo $semi->spe .'-'.$semi->course_spe;?></div>
          </div>
          <a href="http://lp.campuslama.com/programme-detail/mba/branding-and-advertising" class="title-link" style="color: #fff;"></a>
          <figcaption class="transition" style="color: #fff; padding:66px 126px; */">
          <a href="#" class="title-link" style="color: #fff;">
            <span class="mb-" style="color:black;font-weight:bold"></span></a>
            <p><a href="#" class="title-link" style="color: #fff;"></a>
              <a style="background-color: #0e2a4b;color:white;margin-left:-59px" class="btn" href="http://11.helloadmission.in/listing/view_cource/36">View More</a></p>
            </figcaption>
        </figure>
      </div>
    </div> 
   <?php $count++; }
   }?>
  <!--   <div class="item">
      <div class="col-md-3"><a href="#"><img src="http://placehold.it/500/e477e4/fff&amp;text=2" class="img-responsive"></a></div>
    </div>
    <div class="item">
      <div class="col-md-3"><a href="#"><img src="http://placehold.it/500/eeeeee&amp;text=3" class="img-responsive"></a></div>
    </div>
    <div class="item">
      <div class="col-md-3"><a href="#"><img src="http://placehold.it/500/f4f4f4&amp;text=4" class="img-responsive"></a></div>
    </div>
    <div class="item">
      <div class="col-md-3"><a href="#"><img src="http://placehold.it/500/f566f5/333&amp;text=5" class="img-responsive"></a></div>
    </div> -->
   
  </div>
  <a class="left carousel-control" href="#myCarousel" style="margin-top: -34px;margin-left: 3px;" data-slide="prev"><i class="glyphicon glyphicon-chevron-left"></i></a>
  <a class="right carousel-control" href="#myCarousel" style="margin-top: -34px;" data-slide="next"><i class="glyphicon glyphicon-chevron-right"></i></a>
</div>
                  </div>

                </div>
            </div><!-- /.container -->
        </section>
        <div class="bd-example">
  <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Open modal for @mdo</button> -->
 
  <div class="modal fade in" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h2 class="modal-title" id="exampleModalLabel">Student Login</h2>
        </div>
  
       
        <div class="modal-body">
          
                <div role="tabpanel">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#uploadTab" aria-controls="uploadTab" role="tab" data-toggle="tab" id="log">Login</a>

                        </li>
                        <li role="presentation"><a href="#browseTab" aria-controls="browseTab" role="tab" data-toggle="tab" id="reg">Register</a>

                        </li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="uploadTab">
                          <br>
                           <div class="form-group error">
            
                     </div>
                  <form id="frmlogin" method="post" autocomplete="on">
                    <div class="form-group">
                      <label for="recipient-name" class="form-control-label">Email</label>
                      <input type="text" id="email" class="form-control" placeholder="Email">
                    </div>
                     <div class="form-group">
                      <label for="recipient-name" class="form-control-label">Password</label>
                      <input type="password" id="password" class="form-control" placeholder="Password">
                    </div>
                    <div class="form-group">
                    
                      <input type="hidden" id="applytype" name="applytype" class="form-control" placeholder="Password">
                       <input type="hidden" id="course_id" name="course_id" class="form-control" placeholder="Password">
                    </div>
                  </form>
  
                <div class="modal-footer" style="text-align: center;border-top:0px solid #e5e5e5;">
                  <button type="button" class="btn btn-primary green login">Login</button> 
                  <button type="button" class="btn btn-secondary red" data-dismiss="modal">Close</button>
                  
                </div>
                </div>
                     <div role="tabpanel" class="tab-pane" id="browseTab">
                      <br>
                           <div class="form-group error">
            
                     </div>

                  <form id="frmlogin" method="post" autocomplete="on">
                    <div class="form-group">
                      <label for="recipient-name" class="form-control-label">Name</label>
                      <input type="text" id="name" class="form-control" placeholder="Name">
                    </div>

                    <div class="form-group">
                      <label for="recipient-name" class="form-control-label">Email</label>
                      <input type="text" id="email_reg" class="form-control" placeholder="Email">
                    </div>
                     <div class="form-group">
                      <label for="recipient-name" class="form-control-label">Password</label>
                      <input type="password" id="password_reg" class="form-control" placeholder="Password">
                    </div>
                     <div class="form-group">
                      <label for="recipient-name" class="form-control-label">Confirm Password</label>
                      <input type="password" id="cpassword" class="form-control" placeholder="Confirm Password">
                    </div>
                    <div class="form-group">
                    
                      <input type="hidden" id="applytype" name="applytype" class="form-control" placeholder="Password">
                       <input type="hidden" id="course_id" name="course_id" class="form-control" placeholder="Password">
                    </div>
                  </form>
  
                <div class="modal-footer" style="text-align: center;border-top: 0px solid #e5e5e5;">
                  <button type="button" class="btn btn-primary green register">Register</button> 
                  <button type="button" class="btn btn-secondary red" data-dismiss="modal">Close</button>
                  
                </div>
                     </div>
                    </div>
                </div>
          
   </div>
      </div>
    </div>
  </div>
</div>
          <script type="text/javascript" src="<?php echo base_url('assets/js/plugins/jquery-1.11.2.min.js');?>"></script>
        <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/1.3.1/js/toastr.js"></script>
        <script type="text/javascript">
function ajaxindicatorstart(text)
{
  if(jQuery('body').find('#resultLoading').attr('id') != 'resultLoading'){
  jQuery('body').append('<div id="resultLoading" style="display:none"><div><img src="<?php echo base_url()?>uploads/ajax-loader.gif"><div>'+text+'</div></div><div class="bg"></div></div>');
  }

  jQuery('#resultLoading').css({
    'width':'100%',
    'height':'100%',
    'position':'fixed',
    'z-index':'10000000',
    'top':'0',
    'left':'0',
    'right':'0',
    'bottom':'0',
    'margin':'auto'
  });

  jQuery('#resultLoading .bg').css({
    'background':'#000000',
    'opacity':'0.7',
    'width':'100%',
    'height':'100%',
    'position':'absolute',
    'top':'0'
  });

  jQuery('#resultLoading>div:first').css({
    'width': '250px',
    'height':'75px',
    'text-align': 'center',
    'position': 'fixed',
    'top':'0',
    'left':'0',
    'right':'0',
    'bottom':'0',
    'margin':'auto',
    'font-size':'16px',
    'z-index':'10',
    'color':'#ffffff'

  });

    jQuery('#resultLoading .bg').height('100%');
       jQuery('#resultLoading').fadeIn(300);
    jQuery('body').css('cursor', 'wait');
}

function ajaxindicatorstop()
{
    jQuery('#resultLoading .bg').height('100%');
       jQuery('#resultLoading').fadeOut(300);
    jQuery('body').css('cursor', 'default');
}

</script>
<script type="text/javascript">
function sticky_relocate() {
    var window_top = $(window).scrollTop();
    var div_top = $('#sticky-anchor').offset().top;
    if (window_top > div_top) {
        $('#sticky').addClass('stick');
        $('#sticky-anchor').height($('#sticky').outerHeight());
    } else {
        $('#sticky').removeClass('stick');
        $('#sticky-anchor').height(0);
    }
}

$(function() {
    $(window).scroll(sticky_relocate);
    sticky_relocate();
});
  $(document).ready(function(){




    $('#myCarousel').carousel({

  interval: 4000
})
$(".item").slice(0,1).addClass("active");
$('.carousel .item').each(function(){
  
  var next = $(this).next();
  if (!next.length) {
    next = $(this).siblings(':first');
  }
  next.children(':first-child').clone().appendTo($(this));
  
  for (var i=0;i<2;i++) {
    next=next.next();
    if (!next.length) {
      next = $(this).siblings(':first');
    }
    
    next.children(':first-child').clone().appendTo($(this));
  }
});


    $('.userlink').click(function(){
     var ltval = $(this).attr('id');
     if(ltval == 'registertab'){
       $('.div-registertab').show();
       $('.div-logintab').hide();
     }else{
       $('.div-registertab').hide();
       $('.div-logintab').show();
     }
   });
   $('.apply_modal').click(function(){
     var name = $(this).data('name');
     var id = $(this).data('id');
     console.log(id);
     console.log(name);
     $('#applytype').val(name);
      $('#course_id').val(id);
  });

  $('#reg, #log').click(function(){
  $('.error').hide();
  });


   $('.login').click(function(){
    var email=$('#email').val();
    var password=$('#password').val();
    var applytype=$('#applytype').val();
    var course_id=$('#course_id').val();
    if(email==''){
      
      $('.error').html('<div class="alert danger-dark modal-title" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><i class="fa fa-times-circle"></i>  Enter Email</div>');
      $('#email').focus();
      return false;
    }else if(password==''){
        $('.error').html('<div class="alert danger-dark modal-title" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><i class="fa fa-times-circle"></i>  Enter Password</div>');
      $('#password').focus();
      return false;
    }else{
      //$('.error').html('<div class="alert success-dark modal-title" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><i class="fa fa-times-circle"></i>  Succesfully logged in</div>');
      
         var dataString={
          'email':email,
          'password':password,
           'course_id':course_id,
           'applytype':applytype
         };
        //console.log(dataString);
         $.ajax({
           type:'POST',
           url:'<?php echo base_url(); ?>login/after_apply_login',
           data:dataString,
            beforeSend: function () {
                    ajaxindicatorstart('Please wait..');
                 },
           success:function(res)
           {
              var obj = JSON.parse(res);
            console.log(obj);
             if(obj.msg=='Please verify your email address'){
                //$('.error').show();
             //  ajaxindicatorstop();
               //$('.error').html('<div class="alert danger-dark modal-title" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><i class="fa fa-times-circle"></i> Please verify your email address</div>');
               ajaxindicatorstop();
                setTimeout(function(){
                   
                    
                location.reload();
              }, 1000);
                     
            
             }else if(obj.msg=='Successfully')
              { 
                  
                setTimeout(function(){
                     ajaxindicatorstop();
                     toastr["success"]('Applied Succesfully');
                location.reload();
              }, 4000);

             }else if(obj.msg=='Already Shortlisted'){
             //  ajaxindicatorstop();
                toastr["error"]('Course Already Shortlisted');
                setTimeout(function(){
                     ajaxindicatorstop();
                    
                location.reload();
              }, 4000);
                 //$('.error').html('<div class="alert danger-dark modal-title" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><i class="fa fa-times-circle"></i> Course Already Applied</div>');
             
             }else if(obj.msg=='Already Applied'){
             //  ajaxindicatorstop();
                toastr["error"]('Course Already Applied');
                setTimeout(function(){
                     ajaxindicatorstop();
                    
                location.reload();
              }, 4000);
                 //$('.error').html('<div class="alert danger-dark modal-title" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><i class="fa fa-times-circle"></i> Course Already Applied</div>');
             
             }else{
              ajaxindicatorstop();
               //toastr["error"]('Invalid username and password');
              $('.error').html('<div class="alert danger-dark modal-title" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><i class="fa fa-times-circle"></i> Invalid Username and Password</div>');
             }
           }
         });
    }
   });

$('#email_reg').on('focusout', function() {
  $('.error').show();
  var email=$('#email_reg').val();
  $.ajax({
      type:'POST',
      url:'<?php echo base_url()?>admin/check_email',
      data:{'email':email},
      success:function(res){
        //alert(res);
        var obj=JSON.parse(res);
        console.log(obj);
        if(obj.msg=='Exitst'){
          
          $('.error').html('<div class="alert danger-dark modal-title" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><i class="fa fa-times-circle"></i>Email already exist</div>');
          $('#email_reg').focus();
          $('register').prop('disabled',true);
        }
  
    
      }
  })
   
});
   $('.register').click(function(){
    $('.error').show();
    var name=$('#name').val();
    var email=$('#email_reg').val();
    var password=$('#password_reg').val();
    var cpassword=$('#cpassword').val();
    var applytype=$('#applytype').val();
    var course_id=$('#course_id').val();
     var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(name==''){
      $('.error').html('<div class="alert danger-dark modal-title" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><i class="fa fa-times-circle"></i>  Enter Name</div>');
      $('#name').focus();
      return false;
    }else if(email==''){
      
      $('.error').html('<div class="alert danger-dark modal-title" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><i class="fa fa-times-circle"></i>  Enter Email</div>');
      $('#email_reg').focus();
      return false;
    }else if(!re.test(email))
  {
      $('#email_reg').focus();
    $('.error').html('<div class="alert danger-dark modal-title" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><i class="fa fa-times-circle"></i>  Enter Valid email address</div>');
      return false;
  }else if(password==''){
        $('.error').html('<div class="alert danger-dark modal-title" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><i class="fa fa-times-circle"></i>  Enter Password</div>');
      $('#password_reg').focus();
      return false;
    }else if(cpassword==''){
        $('.error').html('<div class="alert danger-dark modal-title" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><i class="fa fa-times-circle"></i>  Enter Confirm Password</div>');
      $('#cpassword').focus();
      return false;
    }else if(password!=cpassword){
        $('.error').html('<div class="alert danger-dark modal-title" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><i class="fa fa-times-circle"></i>  Enter Password does not match</div>');
      $('#cpassword').focus();
      return false;
    }else{
        $('.error').hide();
      var dataString={
          'email':email,
          'password':password,
           'uname':name,
          
         };
        //console.log(dataString);
         $.ajax({
           type:'POST',
           url:'<?php echo base_url(); ?>login/register',
           data:dataString,
            beforeSend: function () {
                    ajaxindicatorstart('Please wait..');
                 },
           success:function(res)
           {
              var obj = JSON.parse(res);
            console.log(obj);
              if(obj.msg=='You are successfully registered..!  Please check your email to verify !')
              { 
                  setTimeout(function(){
                     ajaxindicatorstop();
                     toastr["success"]('Registered Succesfully');
                location.reload();
              }, 4000);
            

             }else if(obj.msg=='User already present'){
              setTimeout(function(){
                     ajaxindicatorstop();
                     toastr["success"]('Email Exist');
                location.reload();
              }, 4000);
             }
           }

        });
    }
   });

  });
</script>
        <script>
        function apply(id,user_id,eligi_id){
 
         var email="<?php echo trim($userdata['email']);?>";
    
         var dataString={
           'course_id':id,
           'user_id':user_id,
           'eligi_id':eligi_id
         };
        console.log(dataString);
         $.ajax({
           type:'POST',
           url:'<?php echo base_url(); ?>student/course_apply',
           data:dataString,
            // beforeSend: function () {
            //         ajaxindicatorstart('Please wait..');
            //      },
           success:function(res)
           {
            var obj = JSON.parse(res);
            console.log(obj);
              if(obj.msg=='Please verify your email address')
              { 
                  $('html,body').animate({
                      scrollTop: $(".second").offset().top},
                      'slow');
              setTimeout(function(){
    // location.reload();
    }, 4000);

             }else if(obj.msg=='Please Update your profile'){
              toastr["info"](obj.msg).css('width','400px');
               //return false;
             }
             else{
                       toastr["success"]('Applied Succesfully');
                  setTimeout(function(){
      location.reload();
    }, 4000);
             }
             //alert(res);
    
              
           }
         });
         
      // } else {
      //  //txt = "You pressed Cancel!";
      // }
     
   }
    function verify(id)
   {
    //alert(id);
     var email="<?php echo trim($userdata['email']);?>";
     var name="<?php echo trim($userdata['name']);?>";
     var dataString={id:id,email:email,name:name};
    $.ajax({
           type:'POST',
           url:'<?php echo base_url(); ?>student/email_verify',
           data:dataString,
           success:function(res)
           {
            var obj = JSON.parse(res);
            console.log(obj);
             if(obj.msg=='Mail send on your register email Please check your email')
              { 
               //toastr["error"]('Please verify email &nbsp;&nbsp;'+'Click here to <a href="<?php echo base_url() ?>login/verify/<?php echo md5($userdata['name']); ?>/<?php echo base64_encode($userdata['id']); ?>" target="_blank">Verify</a>');
                toastr["info"](obj.msg).css("width","500px");
             }
                        
           }
         });
         
   }
        </script>
        <script type="text/javascript">
    
        </script>