<style>
body
{
-webkit-appearance: none;
}
img {
  height: auto;
  max-width: 100%;
  vertical-align: middle;
}

  
.cards {
  display: flex;
  flex-wrap: wrap;
  list-style: none;
  margin: 0;
  padding: 0;
}

.cards__item {
  display: flex;
  padding: 1rem;
  @media(min-width: 40rem) {
    width: 50%;
  }
  @media(min-width: 56rem) {
    width: 33.3333%;
  }
}

.card {
  background-color: white;
  border-radius: 0.25rem;
  box-shadow: 0 20px 40px -14px rgba(0,0,0,0.25);
  display: flex;
  flex-direction: column;
  overflow: hidden;
  &:hover {
    .card__image {
      filter: contrast(100%);
    }
  }
}

.card__content {
  display: flex;
  flex: 1 1 auto;
  flex-direction: column;
  padding: 1rem;
}

.card__image {
  background-position: center center;
  background-repeat: no-repeat;
  background-size: cover;
  border-top-left-radius: 0.25rem;
  border-top-right-radius: 0.25rem;
  filter: contrast(70%);
  //filter: saturate(180%);
  overflow: hidden;
  position: relative;
  transition: filter 0.5s cubic-bezier(.43,.41,.22,.91);;
  &::before {
    content: "";
    display: block;
    padding-top: 56.25%; // 16:9 aspect ratio
  }
  @media(min-width: 40rem) {
    &::before {
      padding-top: 66.6%; // 3:2 aspect ratio
    }
  }
}

.card__image--flowers {
  background-image: url('<?php echo base_url()?>assets/img/idea.jpg');
}

.card__image--river {
  background-image: url('<?php echo base_url()?>assets/img/card.png');
}

.card__image--record {
  background-image: url('<?php echo base_url()?>assets/img/card.png');
}

.card__image--fence {
  background-image: url('<?php echo base_url()?>assets/img/card.png');
}

.card__title {
  color: @gray-dark;
  font-size: 11px;
  font-weight: 300;
  letter-spacing: 2px;
  text-transform: uppercase;
}

.card__text {
  flex: 1 1 auto;
  font-size: 12px;
  line-height: 1.5;
  margin-bottom: 1.25rem;
}
.stickyCompare, .stickyComButton {
    position: fixed;
    z-index: 102;
    bottom: 0;
    width: 100%;
    background: #f7f7f7;
    padding-top: 25px;
    border-top: 0px solid #b7b1b1;
    border-right: 0px solid #b7b1b1;
    border-left: 0px solid #b7b1b1;
    display: flex;
    box-shadow: 0 7px 60px 8px rgba(0,0,0,0.25);
}
</style>
<?php //echo phpinfo()?>
<form action="<?php echo base_url()?>listing/setCompareSession" method="post">
    <div class="stickyCompare" style="display:flex">
            <span style="display:">
                      <button type="submit" class="btn btn--block card__btn" style="olor:white;font-weight:;margin-right: 0px;width: 70%;background-color: #777;margin-left: 16px;margin-bottom: 13px;">COMPARE </button>
                      <a href="<?php echo base_url()?>listing/resetCompare/<?php echo $this->uri->segment(3)?>" class="btn btn--block card__btn" style="color:white;font-weight:;margin-left: 15px;margin-right: 0px;width: 70%;background-color: #f6514c;">CLEAR </a>
                    </span>
    </div>

</form>
<section class="page-title ptb-50 ">
            <div class="container" style="margin-top:100px">
                <div class="row">
                    <div class="col-md-12">
                        <h2>Search result for : <?php  echo $this->uri->segment(3);?></h2>
                        <!--ol class="breadcrumb">
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Shortcodes</a></li>
                            <li class="active">Shortcode Blog</li>
                        </ol--> 
                    </div>
                </div>
            </div>
        </section>
    <div id="container">
    <div class="row" style="background: white;">
    <div class="col-md-1" style="
     min-height: 0px; 
    padding-right: 0px; 
    padding-left: 31px;"></div>
    <div class="col-md-3" style="min-height: 0px;
   padding-right: 0px;    margin-left: 21px;">
   <!--  <div class="input-field col s12">
      Search result for : <?php //  echo $this->uri->segment(3);?>
  </div> -->
                      <div class="tt-sidebar-wrapper" role="complementary">
                          <div class="widget widget_search">
                     
                          </div><!-- /.widget_search -->


                          <div class="widget widget_categories" style="margin-top: 107px;">
                            
                            <ul class="program_new">
              <li><h4><b>Stream</b></h4></li>
              <?php foreach($streams as $stream){ ?>
                      
                       <li><input type="checkbox" class="program" id="program-<?php echo $stream->name; ?>"  name="program[]" value="<?php echo $stream->id; ?>">     <?php echo $stream->name; ?></li>
                      <?php } ?>
       
                            </ul>
              
                          </div><!-- /.widget_categories -->


                          <div class="widget widget_categories ">
             
                            <ul class="stream_1" >
                            <ul class="spdata" >
              <li><h4><b>Program</b></h4></li>
              <?php foreach($spe as $program){ ?>
                <li><input type="checkbox" class="stream1" id="stream1-<?php echo $program->name; ?>" name="stream1[]" value="<?php echo $program->id; ?>">    <?php echo $program->name; ?></li>
                      
                      <?php } ?>
                      </ul>
              </ul>
                          </div><!-- /.widget_tt_twitter -->
        
                      </div><!-- /.tt-sidebar-wrapper -->
                </div>


      <div class="col-md-8" id="cource" style="margin-top:107px" style="min-height: 0px; ">
              <div class="row list-news1" >
        <?php //echo'<pre>', print_r($uniCourses);
        if(!empty($uniCourses)){
         foreach($uniCourses as $kl=>$uniCrs){
        if($uniCrs->uc_is_approved ==1){
        $crsStream = $this->academic_model->getCourseById($uniCrs->id);
        //$uniCrs->uc_um_id;
        //$crsStream = $this->academic_model->getCityById($uniCrs->id);
        $uc_fees = explode(',',$uniCrs->uc_fees);
        $imp=explode(',',$uniCrs->uc_eligibility);
        $umId = $uniCrs->uc_um_id;
        
        for($i=0;$i<count($uc_fees);$i++)
        { 
          $LumpsumFeesData = $this->academic_model->getLumsumpFees($uc_fees[$i],$umId,'Lumpsum');
          $InstallmentFeesData = $this->academic_model->getInstallmentFees($uc_fees[$i],$umId,'Installment'); 
        }
        ?>
                <div class="col-md-4" class="cdata">
                    <article class="">
                       <div class="card cards__item">
                  <div class="card__image card__image--flowers" style="height:140px"> </div>
                  <div class="card__content">
                    <div class="" style="font-size:15px"> <a style="color:#0484bc" href="<?php echo base_url(); ?>listing/index/<?php echo $uniCrs->um_name; ?>" id="cname-<?php echo $uniCrs->uc_id;  ?>"><?php echo '<b>'.$uniCrs->spe.'</b>'; ?>  <?php echo '-'.$uniCrs->course_spe; ?></a><hr></div>
                    <p class="">
                      <i class="fa fa-clock-o"></i>&nbsp;&nbsp;<?php echo $uniCrs->type; ?>
                        &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-user"></i>
                      <?php echo $uniCrs->uc_course_length; ?><br>

                      <i class="fa fa-inr"></i>&nbsp;&nbsp;
                      <?php echo $uniCrs->uc_fees; ?>
                    &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-graduation-cap"></i>
                      <?php foreach ($imp as $k=>$value) {
                $crsEli= $this->academic_model->getEligi($value);
                echo ($crsEli[0]->name).',';
                }?>
                      <br><i class="fa fa-building-o"></i>
                      <span class="uid-<?php echo $uniCrs->uc_um_id; ?>" id="uname-<?php echo $uniCrs->uc_id; ?>"><a style="font-size:10px" href="<?php echo base_url(); ?>listing/index/<?php echo $uniCrs->um_name; ?>"><?php echo $uniCrs->um_name; ?></a>
                    </p>
                    
                    <span style="display:flex">
                      <a href="#" class="btn btn--block card__btn" style="color:white;font-weight:;margin-right:0px;width:50% ;background-color: #777;" id="com-<?php echo $uniCrs->uc_id;  ?>"  onclick="compare_course(this.id)">COMPARE </a>
                      <a href="<?php echo base_url()?>listing/viewcource/<?php echo $uniCrs->uc_id;  ?>/<?php echo $uniCrs->spe;  ?>" class="btn btn--block card__btn" style="color:white;font-weight:;margin-left: 15px;margin-right: 0px;width: 50%;">VIEW </a>
                    </span>
                  </div>
                </div>
                    

                    </article><!-- /.post-wrapper -->
                    
                </div><!-- /.col-md-6 -->
        

<?php  if(($kl+1)%3 ===0){
      echo '</div><div class="row list-news1">';
    }
 }
  }
}else{
?>
<p>No Result found</p>
<?php
}
 ?>  
             
              </div><!-- /.row -->
</div>
    </div>
    <!--<div class='col-md-8' id="cource" style="margin-top:-20px; min-height: 0px;" ></div>  
    </div>-->
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script>
    $(document).ready(function(){
  var filter="<?php echo $this->uri->segment(3);?>"
  
   if(filter){
  $.each($("input[name='program[]']"), function() {
      var id=$(this).attr('id');
       var test=id.split('-');
         console.log(test[1]);
          if(filter==test[1]){
          $('#program-'+test[1]).prop("checked", true);
    }
    });
   }

   if(filter){
  $.each($("input[name='stream1[]']"), function() {
      var id=$(this).attr('id');
       var test=id.split('-');
         console.log(test[1]);
          if(filter==test[1]){
          $('#stream1-'+test[1]).prop("checked", true);
    }
    });
   }
    $('.program_new').click(function() { 
    
        var streamid = $(this).val();

      var program = new Array();
    $.each($("input[name='program[]']:checked"), function() {
      program.push($(this).val());
 
    });
    //console.log(program);
    
    
  $.ajax({
    type: "POST",
    url: "<?php echo base_url('listing/stream_program_new'); ?>",
    data:{streamid:program},
    success: function(response){
        $('.spdata').empty();
      var res = JSON.parse(response);
   //  console.log(res);
      var html='<li><h4><b>Specialization</b></h4></li>';
  
      $.each(res.spelist, function (i, itm) {
        
      html+='<li ><input type="checkbox" class="stream1" name="stream1[]" value="'+itm.id+'">'  +itm.name+'</li>';
        
      });
      
      $('.spdata').append(html);
    }
  })
});
  
  
  
      $('.stream_1').first().find('input[type="checkbox"]').prop("disabled", true);
      $('.program , .stream_1').click('click', function() { 
    
  var streamid = $(this).val();

      var program = new Array();
    $.each($("input[name='program[]']:checked"), function() {
      program.push($(this).val());
 
    });
    //console.log(program);
    
    var program1 = new Array();   
    $.each($("input[name='stream1[]']:checked"), function() {
      program1.push($(this).val());
 
    });
    //console.log(program1);
    
  $.ajax({
    type: "POST",
    url: "<?php echo base_url('listing/stream_program'); ?>",
    data:{streamid:program,spid:program1},
    success: function(response){
        
      var res = JSON.parse(response);
    //console.log(res);
      var html='<li><h4><b>Specialization</b></h4></li>';
  
      $.each(res.spelist, function (i, itm) {
        
      html+='<li ><input type="checkbox" class="stream1" name="stream1[]" value="'+itm.id+'">'  +itm.name+'</li>';
        
      });
      
      //$('.spdata').replaceWith(html);
       
      
      
      var html2='';
      $.each(res.cource, function (j, itm1) {
          $('.list-news1').empty();
          $('#cource').html('');
          //  console.log(itm1);
          if (itm1.length==0) {
            html2+= '<p>No Record Found</p>';
          }
          $('.stream_1').first().find('input[type="checkbox"]').prop("disabled", false);
          html2 += '<div class="row list-news1" >';
        for (i = 0; i < itm1.length; i++) { 
          console.log(itm1[i]);
          var spe='';
          if(itm1[i].course_spe==null){
            spe='';
          }else if(itm1[i].course_spe!=null){
            spe='-'+itm1[i].course_spe;
          }
      html2+=' <div class="col-md-4">'+
                    '<article class="">'+
                     '<div class="card cards__item">'+
                  '<div class="card__image card__image--flowers" style="height:140px"> </div>'+
                  '<div class="card__content">'+
                   ' <div class="" style="font-size:15px"> <a style="color:#0484bc" href="<?php echo base_url()?>listing/index/'+itm1[i].um_name+'" id="cname-'+itm1[i].uc_id+'">'+itm1[i].spe +' '+ spe+'</a><hr></div>'+
                    // '<p class="">'+
                    //    '<i class="fa fa-user"></i> <a href="#">'+itm1[i].type+'</a>&nbsp;'+
                    // '</p>'+
                    '<p class="">'+
                    '<i class="fa fa-clock-o"></i>&nbsp;&nbsp;'+itm1[i].type+ '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-user"></i>&nbsp;&nbsp;&nbsp;'+itm1[i].uc_course_length+'<br>'+
                    '<i class="fa fa-inr"></i>&nbsp;&nbsp;'+itm1[i].uc_fees+'&nbsp;&nbsp;&nbsp;&nbsp;'+
                    '<i class="fa fa-graduation-cap"></i>Graduate,<br><br><i class="fa fa-building-o"></i>&nbsp;&nbsp;<span class="uid-'+itm1[i].uc_um_id+'" id="uname-'+itm1[i].uc_id+'"><a href="<?php echo base_url(); ?>listing/index/'+itm1[i].um_name+'" style="font-size:10px">'+itm1[i].um_name+'</a></p>'+
                    '  <span style="display:flex">'+
                  '<a href="#" class="btn btn--block card__btn" style="color:white;font-weight:;margin-right:0px;width:50% ;background-color: #777;" id="com-'+itm1[i].uc_id+'" onclick="compare_course(this.id)">COMPARE </a>'+
                    '<a href="<?php echo base_url()?>listing/viewcource/'+itm1[i].uc_id+'/'+itm1[i].spe+'" class="btn btn--block card__btn" style="color:white;font-weight:;margin-left: 15px;margin-right: 0px;width: 50%;">VIEW </a>'+
                    '</span>'+
                  '</div>'+
               ' </div>'+
   //                   ' <div class="hover-overlay purple accent-2"></div>'+
   //                    ' <div class="blog-content" style="padding: 0px 0px;">'+
   //                     ' <header class="entry-header-wrapper sticky">'+
   //                      '  <div class="entry-header">'+
      // ' <a href="<?php echo base_url()?>listing/index/'+itm1[i].um_name+'">'+itm1[i].spe +' '+ spe+'</a>'+
   //                        '  <div class="entry-meta">'+
   //                         '     <ul class="list-inline">'+
   //                          '        <li>'+
   //                           '           <i class="fa fa-user"></i> <a href="#">'+itm1[i].type+'</a>'+
   //                            '      </li>'+
   //                             '     <li>'+
   //                              '        <i class="fa fa-clock-o"></i> <a href="#">'+itm1[i].uc_course_length+'</a>'+
   //                               '   </li>'+
      //            '<li>'+
   //                                   '   <i class="fa fa-clock-o"></i> <a href="#">'+itm1[i].updated_at+'</a>'+
   //                                  '</li>'+
   //                              '</ul>'+
   //                          '</div>'+
   //                        '</div>'+
   //                      '</header>'+

   //                      '<div class="entry-content">'+
   //                        '<p>'+itm1[i].um_name+'</p>'+
   //                        ' <a href="<?php echo base_url()?>listing/view_cource/'+itm1[i].uc_id+'" class="btn btn-lg text-capitalize waves-effect waves-light">View Details</a>'+
   //                      '</div>'+
   //                    '</div>'+

                    '</article>'+
                '</div>';

                if((i+1)%3 ===0){
                  html2+= '</div><div class="row list-news1" >';
                }
      }
      html2 += '</div>';
        //});
      });

        $('#cource').html(html2);
      
    }
  });
});





});

$('.stickyCompare').hide();
  $('.stickyComButton').hide();
  var compcount=0;

function compare_course(compId){
  $('.stickyCompare').show();
  $('.stickyComButton').show();
  
compsplitId = compId.split('-');
//var compId = $(this).attr('id');
console.log(compsplitId);

if(!(compcount >2)){
var comcname = $('#cname-'+compsplitId[1]).text();
var comuId = $('#uname-'+compsplitId[1]).attr('class').split('-');
console.log(comcname);
var compareHtml =''+
                    '<article class="" style="width:28%">'+
                     '<div class="card cards__item" style="width: 410px;">'+
                  '<div class="card__image " style=""> <span class="remove" style="float: right;cursor: pointer;"><img src="<?php echo base_url()?>assets/img/cross-icon-2.png" style="margin-right: 7px;margin-top: 8px;width: 26px;z-index: 1022;"></span></div>'+
                  '<div class="card__content">'+
                   ' <div class="" style="font-size:15px"><a style="color: #0484bc;" href="<?php echo base_url("course-details"); ?>/'+compsplitId[1]+'">'+comcname+'</a></h4></div></div></div></div></div>'+
                    // '<p class="">'+
                    //    '<i class="fa fa-user"></i> <a href="#">'+itm1[i].type+'</a>&nbsp;'+
                    // '</p>'+
                 
                    '  <span style="display:flex">'+
                 ''+
                '<input type="hidden" name="courseIds[]" value="'+compsplitId[1]+'" />'+
                '<input type="hidden" name="uId[]" value="'+comuId[1]+'" />'+
                    '</span>'+
                  '</div>';
              
      compcount++;
   }

   $('.stickyCompare').append(compareHtml);
}
    </script>
    <script type="text/javascript">
    $(document).on('click', '.remove', function() {
      console.log($(this));
    $(this).parent().parent().parent().remove();
  compcount =compcount-1;
  //alert(compcount);
  if(compcount ==0){
    $('.stickyCompare').hide();
    $('.stickyComButton').hide();
  }
});
    </script>