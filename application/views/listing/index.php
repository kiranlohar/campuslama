 <head>
		<!--00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000 -->
		
		<meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="materialize is a material design based mutipurpose responsive template">
        <meta name="keywords" content="material design, card style, material template, portfolio, corporate, business, creative, agency">
        <meta name="author" content="trendytheme.net">
     
     
           <!--  favicon -->
        <link rel="shortcut icon" href="<?php echo base_url('assets_new/img/ico/favicon.png' ); ?>">
        <!--  apple-touch-icon -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url('assets/img/ico/apple-touch-icon-144-precomposed.png'); ?>">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/img/ico/apple-touch-icon-114-precomposed.png'); ?>">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/img/ico/apple-touch-icon-72-precomposed.png'); ?>">
        <link rel="apple-touch-icon-precomposed" href="assets/img/ico/apple-touch-icon-57-precomposed.png'); ?>">
        
        <link href='https://fonts.googleapis.com/css?family=Raleway:400,300,500,700,900' rel='stylesheet' type='text/css'>
        <!-- FontAwesome CSS -->
        <link href="<?php echo base_url('assets_new/fonts/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet">
        <!-- Material Icons CSS -->
        <link href="<?php echo base_url('assets_new/fonts/iconfont/material-icons.css'); ?>" rel="stylesheet">
        <!-- magnific-popup -->
        <link href="<?php echo base_url('assets_new/magnific-popup/magnific-popup.css'); ?>" rel="stylesheet">
        <!-- owl.carousel -->
        <link href="<?php echo base_url('assets_new/owl.carousel/assets/owl.carousel.css'); ?>" rel="stylesheet">
        
        <link href="<?php echo base_url('assets_new/owl.carousel/assets/owl.theme.default.min.css'); ?>" rel="stylesheet">
        <!-- flexslider -->
        <link href="<?php echo base_url('assets_new/flexSlider/flexslider.css'); ?>" rel="stylesheet">
        <!-- materialize -->
        <link href="<?php echo base_url('assets_new/materialize/css/materialize.min.css'); ?>" rel="stylesheet">
        <!-- Bootstrap -->
        <link href="<?php echo base_url('assets_new/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
        <!-- shortcodes -->
        <link href="<?php echo base_url('assets_new/css/shortcodes/shortcodes.css'); ?>" rel="stylesheet">
        <!-- Style CSS -->
        <link href="<?php echo base_url('assets_new/style.css') ?>" rel="stylesheet">

        <!-- RS5.0 Main Stylesheet -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets_new/revolution/css/settings.css'); ?>">
        <!-- RS5.0 Layers and Navigation Styles -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets_new/revolution/css/layers.css'); ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets_new/revolution/css/navigation.css'); ?>">
     
        
        <!--00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000 -->
        
		<title>Course List</title>		
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<link href="<?php echo base_url('assets/explore/css/animate.css');?>" type="text/css" rel="stylesheet">
		<link href="<?php echo base_url('assets/explore/css/materialize.min.css');?>" type="text/css" rel="stylesheet">
		<link href="<?php echo base_url('assets/explore/css/ion.rangeSlider.css');?>" type="text/css" rel="stylesheet"/>
		<link href="<?php echo base_url('assets/explore/css/ion.rangeSlider.skinHTML5.css');?>" type="text/css" rel="stylesheet"/>
		<link href="<?php echo base_url('assets/explore/css/frontend/quiz_mycustom.css');?>" type="text/css" rel="stylesheet">
		<link href="<?php echo base_url('assets/explore/css/frontend/quiz_mycustom_media.css');?>" type="text/css" rel="stylesheet">
		<script type="text/javascript" src="<?php echo base_url('assets/explore/js/jquery-2.1.4.min.js');?>"></script>
		<script type="text/javascript" src="<?php echo base_url('assets/explore/js/materialize.min.js');?>"></script>
		<script type="text/javascript" src="<?php echo base_url('assets/explore/js/ion.rangeSlider.js');?>"></script>
		<script type="text/javascript" src="<?php echo base_url('assets/explore/js/frontend/mobile_detect.js');?>"></script>
    </head>

<style>
.text-white {
    color: #fff;
}
.text-center {
    text-align:center;
}
.Space10 {
    height: 10px !important;
}
.offset-l1 {
    margin-left: 1.3% !important;
}
.show-more {
    color: #F26223;
}
.card-custom {
    margin: 0.4rem 0 0.4rem 0 !important;
}
.container .row {
    margin-bottom: 12px;
}
.apply-now {
    padding: 3px !important;
    color: #ffffff;
    font-size: 14px;
    font-weight: 500;
    background-color: #F26223 !important;
}
.course-icons {
    padding: 0 0rem !important;
    color: #5D6F71;
    font-size: 14px;
    font-weight: 500;
}
.inner-container {
    margin-left: 30px !important;
    margin-right: 30px !important;
}
.container .row {
    margin-left: -0.75rem;
    margin-right: -0.75rem;
}
.card {
	width:450px;
    position: relative;
    margin: 0.5rem 0 1rem 0;
    background-color: #fff;
    transition: box-shadow .25s;
    border-radius: 2px;
}
.ucourse-details {
    border-right: 1px solid #ACBDBC;
    border-bottom: 1px solid #ACBDBC;
    border-top: 1px solid #ACBDBC;
    border-left: 1px solid #ACBDBC;
    background-color: #F6F7F9;
    color: #4580B8;
}

 .input-field div.error{
    position: relative;
    top: -1rem;
    left: 3rem;
    font-size: 0.8rem;
    color:#FF4081;
    -webkit-transform: translateY(0%);
    -ms-transform: translateY(0%);
    -o-transform: translateY(0%);
    transform: translateY(0%);
  }
.div-registertab{
	display:none;
}
.userlink {
    cursor: pointer;
    margin-left: 45%;
}
</style>


  <!--header id="header" class="page-topbar">
        <div class="navbar-fixed">
            <nav class="navbar-color">
                <div class="nav-wrapper">
					
					<div class="container">

                    <div class="search-wrapper">
                        
                        <i class="search-close material-icons">&#xE5CD;</i>
                        <div class="search-form-wrapper">
                            <form action="#" class="white-form">
                                <div class="input-field">
                                    <input type="text" name="search" id="search">
                                    <label for="search" class="">Search Here...</label>
                                </div>
                                <button class="btn pink search-button waves-effect waves-light" type="submit"><i class="material-icons">&#xE8B6;</i></button>
                                
                            </form>
                        </div>
                    </div>
                
                    <div id="materialize-menu" class="menuzord">

                      

                        <a href="index.html" class="">
									<a href="<?php echo base_url('home/index'); ?>" class="LogoMargin">
										<span class="red-text">Hello</span>
										<span class="black-text">Admission</span>
									</a>
                        </a>
                      

                        <ul class="menuzord-menu pull-right">

                            <li>
								<a onclick="GLogin();" class="black-text">Google</a>&nbsp;&nbsp;
                            </li>
                            
                            <li>
								<a onclick="FBLogin();" class="black-text">Facebook</a>&nbsp;&nbsp;		
                            </li>
                            
                            <li >
									<a href="<?php echo base_url('explore/index'); ?>" class="black-text">Explore</a>&nbsp;&nbsp;
                            </li>
                            
                            
                            <li class="">
									<a href="<?php echo base_url('login/index'); ?>" class="black-text">Login</a>&nbsp;&nbsp;
                            </li>
                            
                            <li class="">
									<a href="<?php echo base_url('login/student_register'); ?>" class="black-text">SignUp</a>&nbsp;&nbsp;
                            </li>

                        </ul>
                    
                    </div>
    
    </div>
            </nav>
        </div>
    </header-->
    
    <body>
<div class="container">
<!--chart dashboard start-->
<div id="chart-dashboard">
<div class="row university-card">
<!--Start-->
<section class="page-title ptb-50">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h3>cource</h3>
                      
                    </div>
                </div>
            </div>
        </section>
		
		<section class="section-padding">
            <div class="container">


                <div class="portfolio-container">
                    <!--ul class="portfolio-filter brand-filter text-center">
                        <li class="waves-effect waves-light" data-group="all">All</li>
                        <li class="waves-effect waves-light" data-group="websites">Websites</li>
                        <li class="waves-effect waves-light active" data-group="branding">Branding</li>
                        <li class="waves-effect waves-light" data-group="marketing">Marketing</li>
                        <li class="waves-effect waves-light" data-group="photography">Photography</li>
                    </ul-->
					<div class="row">
<?php foreach($uniCourses as $uniCrs){
				if($uniCrs->uc_is_approved ==1 ){
				$crsStream = $this->academic_model->getCourseById($uniCrs->com_parent);
				$uc_fees = explode(',',$uniCrs->uc_fees);
				
				$umId = $uniCrs->uc_um_id;
				
				for($i=0;$i<count($uc_fees);$i++)
				{	
					$LumpsumFeesData = $this->academic_model->getLumsumpFees($uc_fees[$i],$umId,'Lumpsum');
					$InstallmentFeesData = $this->academic_model->getInstallmentFees($uc_fees[$i],$umId,'Installment');	
				}
				?>

<div class="col-md-4">  <div class="portfolio portfolio-with-title col-3 gutter mtb-50 shuffle" style="width: 483px;position: relative; height: 338px; transition: height 250ms ease-out;">

                        <div class="portfolio-item shuffle-item filtered" data-groups="[&quot;all&quot;, &quot;websites&quot;, &quot;branding&quot;]" style="position: absolute; top: 0px; left: 0px; transform: translate3d(0px, 0px, 0px) scale3d(1, 1, 1); opacity: 1; transition: transform 250ms ease-out, opacity 250ms ease-out;">

                          <div class="card">
                            <div class="card-image waves-effect waves-block waves-light">
                              <img class="activator" src="http://localhost/HA/assets/images/BBA.jpg" alt="image">
                            </div>
                            <div class="card-content">
                              <span class="card-title activator"><?php echo $crsStream->com_name; ?> - <?php echo $uniCrs->com_name; ?> <i class="fa fa-ellipsis-v right"></i> 
							 
							  <?php $userdata = $this->session->userdata('logged_in'); ?>

							
								<?php if(!$userdata){ ?>
								
									<!--a class="modal-trigger applynow fa fa-heart-o right" href="#modal1" id="shortlist-<?php echo $uniCrs->uc_id; ?>"> <i class="fa fa-heart-o right"></i></span> </a-->
									<i class="fa fa-heart-o right"></i>
						
								<?php }else{ ?>
								
									<a class="modal-trigger" href="#modal-short-<?php echo $uniCrs->uc_id; ?>"> <i class="fa fa-heart-o right"></i></span>  </a>
						
							
								<?php } ?>
								
							
							  
                              <p><a href="#"><?php echo $uniCrs->um_name; ?></a></p>
                            </div>
                            <div class="card-reveal" style="transform: translateY(0px);">
                              <span class="card-title"><?php echo $crsStream->com_name; ?> <i class="material-icons right"></i></span>
                              <p><a href="#"><?php echo $uniCrs->com_name; ?></a></p>
                              <p> <ul >
						  <li><i class="fa fa-university" aria-hidden="true"></i> <?php echo $uniCrs->um_name; ?></li>
						  <li><i class="fa fa-inr" aria-hidden="true"></i> <?php if($LumpsumFeesData)
								{ 
										echo $LumpsumFeesData->ufeem_amount;
								}
								else
								{
									$InstallmentFeesData->ufeem_amount;
								} ?><span>  &nbsp;per year</span></li>
						  <li><i class="fa fa-graduation-cap" aria-hidden="true"></i> <?php echo $uniCrs->uc_course_type; ?></li>
  
							</ul>
							<a href="#" class="readmore waves-effect waves-light btn btn-large pink">View </a> <a href="#" style="margin-left:-12px" class="readmore waves-effect waves-light btn btn-large green"><i class="material-icons "></i> Apply</a>
							 
							</p>
							 
                              
							 
                            </div>
                          </div><!-- /.card -->

                        </div><!-- /.portfolio-item -->

                    </div><!-- /.portfolio --></div>
	<div id="modal-short-<?php echo $uniCrs->uc_id; ?>" class="modal" >
								  <div class="modal-header"><a href="#" class="waves-effect waves-red btn-flat modal-action modal-close" style="float: right;">X</a></div>
								  <div class="modal-content">
									<h5 style="color: #000;" class="text-center">Are you sure to shortlist ? </h5>
									<form action="<?php echo base_url('student/course_shortlist'); ?>" method="post">
										<div class="row center">
											<a class="btn waves-effect waves-light blue div12-btn modal-close">Cancel</a>
											<input type="hidden" name="course_id" value="<?php echo $uniCrs->uc_id; ?>">
											<input type="hidden" name="user_id" value="<?php echo $userdata['id']; ?>">
											<input type="submit" class="btn waves-effect waves-light green div12-btn" value="Ok"/>
										</div>
									</form>
								  </div>
								</div>
<?php } } ?>    
</div>              

					

					
                    <!--div class="load-more-button text-center">
                      <a class="waves-effect waves-light btn btn-large pink"> <i class="fa fa-spinner left"></i> Load More</a>
                    </div-->

                </div><!-- portfolio-container -->

            </div><!-- /.container -->
        </section>
<!--End-->
		<!--div class="col s12 m12 l12 university-card">
			<div class="card card-custom">
				<div class="row inner-container university-card">
					<div class="col s4 m1 l1 center-align utitle-img">
						<img src="<?php echo base_url('assets/images/course-icon.jpg'); ?>" class="responsive-img" />
					</div>
					<div class="col s6 m11 l11 utitle-name">
						<h4>Course</h4>	
					</div>
				</div>
				<?php //echo "<pre>",print_r($uniCourses); ?>
				<?php foreach($uniCourses as $uniCrs){
				if($uniCrs->uc_is_approved ==1 ){
				$crsStream = $this->academic_model->getCourseById($uniCrs->com_parent);
				$uc_fees = explode(',',$uniCrs->uc_fees);
				
				$umId = $uniCrs->uc_um_id;
				
				for($i=0;$i<count($uc_fees);$i++)
				{	
					$LumpsumFeesData = $this->academic_model->getLumsumpFees($uc_fees[$i],$umId,'Lumpsum');
					$InstallmentFeesData = $this->academic_model->getInstallmentFees($uc_fees[$i],$umId,'Installment');	
				}
				?>
				<div class="row inner-container ucourse-details">
					<div class="col s12 m12 l12 Space10"></div>
					<div class="row">
						<div class="col s12 m12 l12" >
							<div class="col s12 m13 l3 offset-l1 inner-title">
								<?php echo $crsStream->com_name; ?> - <?php echo $uniCrs->com_name; ?>
							</div>
							<div class="col s12 m12 l2 ustar">
								<img src="<?php echo base_url('assets/images/stars-01.png'); ?>" class="responsive-img" />
								<img src="<?php echo base_url('assets/images/stars-01.png'); ?>" class="responsive-img" />
								<img src="<?php echo base_url('assets/images/stars-01.png'); ?>" class="responsive-img" />
								<img src="<?php echo base_url('assets/images/stars-01.png'); ?>" class="responsive-img" />
								<img src="<?php echo base_url('assets/images/stars-02.png'); ?>" class="responsive-img" />
							</div>
							<div class="col s12 m1 l1 ureview">26 Reviews</div>
							<div class="col s12 m1 l4 ureview" style="float:right;"><img src="<?php echo base_url('assets/images/university-login-icon-hover-4.png'); ?>" class="responsive-img" />&nbsp;&nbsp;<span style="vertical-align: super;"><?php echo $uniCrs->um_name; ?></span></div>
						</div>
					</div>
					<div class="row">
						<div class="col s12 m12 l12 Space10"></div>
						<div class="col s12 m1 l1 offset-l1">
							<div class="row">
								<div class="col s5 m5 l5 center-align">
								<img src="<?php echo base_url('assets/images/icon-01.png'); ?>" class="responsive-img" />
								</div>
								<div class="col s7 m7 l7 no-padding-origin course-icons">
								<?php echo $uniCrs->uc_course_length; ?>
								</div>
							</div>
						</div>
						<div class="col s12 m1 l1">
							<div class="row">
								<div class="col s5 m5 l5 center-align">
								<img src="<?php echo base_url('assets/images/icon-02.png'); ?>" class="responsive-img" />
								</div>
								<div class="col s7 m7 l7 no-padding-origin course-icons">
								<?php echo $uniCrs->uc_course_type; ?>
								</div>
							</div>
						</div>
						<div class="col s12 m1 l1">
							<div class="row">
								<div class="col s5 m5 l5 center-align">
								<img src="<?php echo base_url('assets/images/icon-03.png'); ?>" class="responsive-img" />
								</div>
								<div class="col s7 m7 l7 no-padding-origin course-icons">
								HSC
								</div>
							</div>
						</div>
						<div class="col s12 m1 l1">
							<div class="row">
								<div class="col s5 m5 l5 center-align">
								<img src="<?php echo base_url('assets/images/icon-04.png'); ?>" class="responsive-img" />
								</div>
								<div class="col s7 m7 l7 no-padding-origin course-icons">
								CET
								</div>
							</div>
						</div>
						<div class="col s12 m2 l1">
							<div class="row">
								<div class="col s5 m5 l5 center-align">
								<img src="<?php echo base_url('assets/images/icon-05.png'); ?>" class="responsive-img" />
								</div>
								<div class="col s7 m7 l7 no-padding-origin course-icons">
								<?php if($LumpsumFeesData)
								{ 
										echo $LumpsumFeesData->ufeem_amount;
								}
								else
								{
									$InstallmentFeesData->ufeem_amount;
								} ?><span>  &nbsp;per year</span>
								</div>
							</div>
						</div>
						<div class="col s12 m1 l1">
							<p class="show-more">Show More</p>
						</div>
						<div class="col s12 m2 l2 offset-l1">
							<div class="row">
								<div class="col s5 m3 l3 center-align">
								<img src="<?php echo base_url('assets/images/icon-06.png'); ?>" class="responsive-img" />
								</div>
								<div class="col s7 m7 l7 no-padding-origin course-icons">
								Download Brochure
								</div>
							</div>
						</div>
						<?php $userdata = $this->session->userdata('logged_in'); ?>
						<div class="col s12 m2 l2">
							<div class="row">
								<div class="col s5 m3 l3 center-align">
								<img src="<?php echo base_url('assets/images/icon-07.png'); ?>" class="responsive-img" />
								</div>
								<?php if(!$userdata){ ?>
								<div class="col s7 m7 l7 no-padding-origin course-icons">
									<a class="modal-trigger applynow" href="#modal1" id="shortlist-<?php echo $uniCrs->uc_id; ?>">Shortlist </a>
								</div>
								<?php }else{ ?>
								<div class="col s7 m7 l7 no-padding-origin course-icons">
									<a class="modal-trigger" href="#modal-short-<?php echo $uniCrs->uc_id; ?>">Shortlist </a>
								</div>
								<div id="modal-short-<?php echo $uniCrs->uc_id; ?>" class="modal">
								  <div class="modal-header"><a href="#" class="waves-effect waves-red btn-flat modal-action modal-close" style="float: right;">X</a></div>
								  <div class="modal-content">
									<h5 style="color: #000;" class="text-center">Are you sure to shortlist ? </h5>
									<form action="<?php echo base_url('student/course_shortlist'); ?>" method="post">
										<div class="row center">
											<a class="btn waves-effect waves-light blue div12-btn modal-close">Cancel</a>
											<input type="hidden" name="course_id" value="<?php echo $uniCrs->uc_id; ?>">
											<input type="hidden" name="user_id" value="<?php echo $userdata['id']; ?>">
											<input type="submit" class="btn waves-effect waves-light green div12-btn" value="Ok"/>
										</div>
									</form>
								  </div>
								</div>
								<?php } ?>
								
							</div>
						</div>
						<div class="col s8 m1 l1">
							<div class="row">
								<div class="col s12 m10 l10 center-align offset-s2 apply-now">
								<?php if(!$userdata){ ?>
								<a class="modal-trigger text-white applynow" href="#modal1" id="apply-<?php echo $uniCrs->uc_id; ?>">
								Apply Now </a>
								<?php }else{ ?>
								<a class="modal-trigger text-white" href="#modal-course-<?php echo $uniCrs->uc_id; ?>" id="<?php echo $uniCrs->uc_id; ?>">
								Apply Now </a>
								<div id="modal-course-<?php echo $uniCrs->uc_id; ?>" class="modal">
								  <div class="modal-header"><a href="#" class="waves-effect waves-red btn-flat modal-action modal-close" style="float: right;">X</a></div>
								  <div class="modal-content">
									<h5 style="color: #000;" class="text-center">Are you sure to apply ? </h5>
									<form action="<?php echo base_url('student/course_apply'); ?>" method="post">
										<div class="row center">
											<a class="btn waves-effect waves-light blue div12-btn modal-close">Cancel</a>
											<input type="hidden" name="course_id" value="<?php echo $uniCrs->uc_id; ?>">
											<input type="hidden" name="user_id" value="<?php echo $userdata['id']; ?>">
											<input type="submit" class="btn waves-effect waves-light green div12-btn" value="Ok"/>
										</div>
									</form>
								  </div>
								</div>
								<?php } ?>
								</div>
								<div class="col s12 m2 l2"></div>
							</div>
						</div>
						
						<div class="col s12 m12 l12 Space10"> </div>
					</div>
				</div>
				<div class="col s12 m12 l12 Space10 white-bg"></div>
				<?php } } ?>
				<div id="modal1" class="modal">
				  <div class="modal-header"><a href="#" class="waves-effect waves-red btn-flat modal-action modal-close" style="float: right;">X</a></div>
				  <div class="modal-content">
					<h4 class="text-center">ENTER YOUR DETAILS</h4>
					<div id="div12-content-action" class="div-registertab">
						<form id="registerForm" action="<?php echo base_url('login/after_apply'); ?>" method="post">
							<div class="row center">
								<div class="row center black-text">
									<div class="col l6">
										<div class="row margin">
										  <div class="input-field col s12">
											<i class="mdi-social-person-outline prefix"></i>
											<input id="uname" type="text" name="uname">
											<label for="uname">Full Name</label>
										  </div>
										</div>
										<div class="row margin">
										  <div class="input-field col s12">
											<i class="mdi-communication-email prefix"></i>
											<input id="cemail" type="email" name="cemail">
											<label for="cemail">Email</label>
										  </div>
										</div>
									</div>
									<div class="col l6">
										<div class="row margin">
										  <div class="input-field col s12">
											<i class="mdi-action-lock-outline prefix"></i>
											<input id="password" type="password" name="password">
											<label for="password">Password</label>
										  </div>
										</div>
										<div class="row margin">
										  <div class="input-field col s12">
											<i class="mdi-action-lock-outline prefix"></i>
											<input id="password-again" type="password" name="confirm_password">
											<label for="password-again">Confirm Password</label>
										  </div>
										</div>
									</div>									
								</div>
							</div>
							<input type="hidden" name="course_id" class="course_id">
							<input type="hidden" name="applytype" class="applytype">
							<div class="row center">
								<input type="submit" class="btn waves-effect waves-light green div12-btn" value="Submit"/>
							</div>
						</form>
						<a class="userlink" id="logintab">LOGIN</a>
					</div>
					<div id="div12-content-action" class="div-logintab">
						<form id="loginForm" action="<?php echo base_url('login/after_apply_login'); ?>" method="post">
							<div class="row center">
								<div class="row center black-text">
									<div class="col l6">
										<div class="row margin">
										  <div class="input-field col s12">
											<i class="mdi-communication-email prefix"></i>
											<input id="email" type="email" name="email">
											<label for="email">Email</label>
										  </div>
										</div>
									</div>
									<div class="col l6">
										<div class="row margin">
										  <div class="input-field col s12">
											<i class="mdi-action-lock-outline prefix"></i>
											<input id="password" type="password" name="password">
											<label for="password">Password</label>
										  </div>
										</div>
									</div>									
								</div>
							</div>
							<input type="hidden" name="course_id" class="course_id">
							<input type="hidden" name="applytype" class="applytype">
							<div class="row center">
								<input type="submit" class="btn waves-effect waves-light green div12-btn" value="Submit"/>
							</div>
						</form>
						<a class="userlink" id="registertab">SIGN UP</a>
					</div>
				  </div>
				</div>
			</div>
		</div-->
		
</div>
</div>
</div>



<!--####################################################--!>

<!DOCTYPE html>
<html lang="en">
    <head>
        <!--  favicon -->
        <link rel="shortcut icon" href="assets/img/ico/favicon.png">
        <!--  apple-touch-icon -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/img/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/img/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/img/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="assets/img/ico/apple-touch-icon-57-precomposed.png">

        <link href='https://fonts.googleapis.com/css?family=Raleway:400,300,500,700,900' rel='stylesheet' type='text/css'>
        <!-- Material Icons CSS -->
        <link href="assets/fonts/iconfont/material-icons.css" rel="stylesheet">
        <!-- FontAwesome CSS -->
        <link href="assets/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- owl.carousel -->
        <link href="assets/owl.carousel/assets/owl.carousel.css" rel="stylesheet">
        <link href="assets/owl.carousel/assets/owl.theme.default.min.css" rel="stylesheet">
        <!-- materialize -->
        <link href="assets/materialize/css/materialize.min.css" rel="stylesheet">
        <!-- Bootstrap -->
        <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- shortcodes -->
        <link href="assets/css/shortcodes/shortcodes.css" rel="stylesheet">
        <!-- Style CSS -->
        <link href="style.css" rel="stylesheet">
        
    </head>



        <!-- Top bar start-->
  		 
        <!-- Grid News -->
        <!--section class="section-padding default-blog grid-blog">
            <div class="container">
              <div class="row">
              
              </div>
              <ul class="pagination post-pagination text-center mt-50">
                <li><a href="#." class="waves-effect waves-light"><i class="fa fa-angle-left"></i></a></li>
                <li><span class="current waves-effect waves-light">1</span></li>
                <li><a href="#." class="waves-effect waves-light">2</a></li>
                <li><a href="#." class="waves-effect waves-light"><i class="fa fa-angle-right"></i></a></li>
              </ul>
              
            </div>
        </section>
        <!-- Grid News End -->



        <footer class="footer footer-four"  style="
    /* float: left; */
    margin-left: -240px;
    /* width: 100%; */
    width: 1660px;
">
            <div class="primary-footer brand-bg text-center">
                <div class="container">

                  <a href="#top" class="page-scroll btn-floating btn-large pink back-top waves-effect waves-light" data-section="#top">
                    <i class="material-icons"></i>
                  </a>

                  <ul class="social-link tt-animate ltr mt-20">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-tumblr"></i></a></li>
                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                    <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                    <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                    <li><a href="#"><i class="fa fa-rss"></i></a></li>
                  </ul>

                  <hr class="mt-15">

                  <div class="row">
                    <div class="col-md-12">
                          <div class="footer-logo">
                            <img src="http://localhost/HA/assets_new/img/logo-white.png" alt="">
                          </div>

                          <span class="copy-text">Copyright © 2016 <a href="#">Materialize</a> &nbsp; | &nbsp;  All Rights Reserved &nbsp; | &nbsp;  Designed By <a href="#">TrendyTheme</a></span>
                          <div class="footer-intro">
                            <p>Penatibus tristique velit vestibulum adipiscing habitant aenean feugiat at condimentum aptent odio orci vulputate hac mollis a.Vestibulum adipiscing gravida justo a ac euismod vitae.</p>
                          </div>
                    </div><!-- /.col-md-12 -->
                  </div><!-- /.row -->
                </div><!-- /.container -->
            </div><!-- /.primary-footer -->

            <div class="secondary-footer brand-bg darken-2 text-center">
                <div class="container">
                    <ul>
                      <li><a href="#">Home</a></li>
                      <li><a href="#">About us</a></li>
                      <li><a href="#">Services</a></li>
                      <li><a href="#">Portfolio</a></li>
                      <li><a href="#">Contact us</a></li>
                    </ul>
                </div><!-- /.container -->
            </div><!-- /.secondary-footer -->
        </footer>
  

        <!-- Preloader -->
        <div id="preloader">
          <div class="preloader-position"> 
            <img src="assets/img/logo-colored.png" alt="logo" >
            <div class="progress">
              <div class="indeterminate">gfdgd</div>
            </div>
          </div>
        </div>
        <!-- End Preloader -->
        

        <!-- jQuery -->
        <script src="assets/js/jquery-2.1.3.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/materialize/js/materialize.min.js"></script>
        <script src="assets/js/menuzord.js"></script>
        <script src="assets/js/bootstrap-tabcollapse.min.js"></script>
        <script src="assets/js/jquery.easing.min.js"></script>
        <script src="assets/js/imagesloaded.js"></script>
        <script src="assets/js/smoothscroll.min.js"></script>
        <script src="assets/owl.carousel/owl.carousel.min.js"></script>
        <script src="assets/js/scripts.js"></script>

    </body>
  
</html>








		<script type="text/javascript" src="<?php echo base_url('assets/explore/js/frontend/frontend_quiz.js');?>"></script>
	    <script src="<?php echo base_url('assets_new/js/jquery-2.1.3.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets_new/bootstrap/js/bootstrap.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets_new/materialize/js/materialize.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets_new/js/jquery.easing.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets_new/js/jquery.sticky.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets_new/js/smoothscroll.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets_new/js/imagesloaded.js'); ?>"></script>
        <script src="<?php echo base_url('assets_new/js/jquery.stellar.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets_new/js/jquery.inview.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets_new/js/jquery.shuffle.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets_new/js/menuzord.js'); ?>"></script>
        <script src="<?php echo base_url('assets_new/js/bootstrap-tabcollapse.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets_new/owl.carousel/owl.carousel.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets_new/flexSlider/jquery.flexslider-min.js'); ?>"></script>
        <script src="<?php echo base_url('assets_new/magnific-popup/jquery.magnific-popup.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets_new/js/scripts.js'); ?>"></script>

<script type="text/javascript" src="<?php echo base_url('assets/js/plugins/jquery-validation/jquery.validate.min.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/plugins/jquery-validation/additional-methods.min.js');?>"></script>
<script>
$("#registerForm").validate({
        rules: {
            uname: {
                required: true
            },
            cemail: {
                required: true,
                email:true
            },
            password: {
				required: true,
				minlength: 5
			},
			confirm_password: {
				required: true,
				minlength: 5,
				equalTo: "#password"
			},
			role: { 
				required: true
			}
        },
        //For custom messages
        messages: {
            uname:{
                required: "Enter a full name"
            },
			cemail: {
                required: "Enter a email address",
                email:"Enter valid email address"
            },
			password: {
				required: "Enter a password",
				minlength: "Enter at least 5 characters"
			},
			confirm_password: {
				required: "Enter a confirm password",
				minlength: "Enter at least 5 characters",
				equalTo: "Please enter the same value again"
			}
        },
        errorElement : 'div',
        errorPlacement: function(error, element) {
          var placement = $(element).data('error');
          if (placement) {
            $(placement).append(error)
          } else {
            error.insertAfter(element);
          }
        }
     });
	 
	 $("#loginForm").validate({
        rules: {
            email: {
                required: true,
                email:true
            },
            password: {
				required: true,
				minlength: 5
			}
        },
        //For custom messages
        messages: {
            email: {
                required: "Enter a email address",
                email:"Enter valid email address"
            },
			password: {
				required: "Enter a password",
				minlength: "Enter at least 5 characters"
			}
        },
        errorElement : 'div',
        errorPlacement: function(error, element) {
          var placement = $(element).data('error');
          if (placement) {
            $(placement).append(error)
          } else {
            error.insertAfter(element);
          }
        }
     });
	 
	 $('.userlink').click(function(){
		 var ltval = $(this).attr('id');
		 if(ltval == 'registertab'){
			 $('.div-registertab').show();
			 $('.div-logintab').hide();
		 }else{
			 $('.div-registertab').hide();
			 $('.div-logintab').show();
		 }
	 });
	 
	  $('.applynow').click(function(){
		 var appId = $(this).attr('id');
		 var appsval = appId.split('-');
		 $('.course_id').val(appsval[1]);
		 $('.applytype').val(appsval[0]);
	  });
</script>
	
</body>

