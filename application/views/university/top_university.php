<style>
.ui-helper-hidden-accessible { display:none; }
</style>
<section class="page-title ptb-50" style="padding-top:120px">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2>Top University</h2>
                        
                    </div>
                </div>
            </div>
        </section>
		<section class="section-padding list-news" style="background:white">
            <div class="container">


<div class="row">
	 <div class="col-md-3 vertical-tab">
	 	
                        <!-- Nav tabs -->
                      <!--   <ul class="nav nav-tabs nav-stacked hidden-xs" role="tablist">
                          <li role="presentation" class=""><a href="#tab-5" class="waves-effect waves-light" role="tab" data-toggle="tab" aria-expanded="false">About us</a></li>
                          <li role="presentation" class=""><a href="#tab-6" class="waves-effect waves-light" role="tab" data-toggle="tab" aria-expanded="false">What We Do</a></li>
                          <li role="presentation" class=""><a href="#tab-7" class="waves-effect waves-light" role="tab" data-toggle="tab" aria-expanded="false">Our Mission</a></li>
                          <li role="presentation" class=""><a href="#tab-8" class="waves-effect waves-light" role="tab" data-toggle="tab" aria-expanded="false">Our Team</a></li>
                          <li role="presentation" class="active"><a href="#tab-9" class="waves-effect waves-light" role="tab" data-toggle="tab" aria-expanded="true">Our Skills</a></li>
                        </ul><div class="panel-group visible-xs" id="undefined-accordion"></div>                       -->
                           <ul class="nav nav-tabs nav-stacked hidden-xs" role="tablist">
                        	<?php //echo'<pre>',print_r($top_cities);?>
                        	<?php $count=5;
                        		foreach ($top_cities as $city) {
                        			$active='';
                        			//echo  $city->city;
                        			if($city->city==='Pune'){
                        				$active.="active";
                        			}
                        		?>
                        		<li role="presentation" class="<?php echo $active;?>"><a href="#tab-<?php echo $count++?>" class="waves-effect waves-light js-tabcollapse-panel-heading" role="tab" data-toggle="tab" data-parent="" onclick='get_top_city(<?php echo $city->city_id?>)'><?php echo $city->city?></a></li>
                        		<?php
                        		}
                        	?>
                          
                          <!-- <li role="presentation"><a href="#tab-6" class="waves-effect waves-light js-tabcollapse-panel-heading" role="tab" data-toggle="tab" data-parent="" onclick='get_top_city()'>Mumbai</a></li>
                          <li role="presentation"><a href="#tab-7" class="waves-effect waves-light js-tabcollapse-panel-heading" role="tab" data-toggle="tab" data-parent="" onclick='get_top_city()'>Delhi</a></li>
                          <li role="presentation"><a href="#tab-8" class="waves-effect waves-light js-tabcollapse-panel-heading" role="tab" data-toggle="tab" data-parent="" onclick='get_top_city()'>Banglore</a></li>
                          <li role="presentation"><a href="#tab-9" class="waves-effect waves-light js-tabcollapse-panel-heading" role="tab" data-toggle="tab" data-parent="" onclick='get_top_city()'>Hyderabad</a></li> -->
                          
                        </ul>
                        <div class="panel-group visible-md" id="undefined-accordion"></div>    
                 
	 </div>
	 <div class="col-md-9">
	 	<div class="row" id="university">
                
        </div>
	 </div>
</div>
        
            </div><!-- /.container -->
        </section>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> 
<script type="text/javascript">
function ajaxindicatorstart(text)
{
    if(jQuery('body').find('#resultLoading').attr('id') != 'resultLoading'){
    jQuery('body').append('<div id="resultLoading" style="display:none"><div><img src="<?php echo base_url()?>uploads/ajax-loader.gif"><div>'+text+'</div></div><div class="bg"></div></div>');
    }

    jQuery('#resultLoading').css({
        'width':'100%',
        'height':'100%',
        'position':'fixed',
        'z-index':'10000000',
        'top':'0',
        'left':'0',
        'right':'0',
        'bottom':'0',
        'margin':'auto'
    });

    jQuery('#resultLoading .bg').css({
        'background':'#000000',
        'opacity':'0.7',
        'width':'100%',
        'height':'100%',
        'position':'absolute',
        'top':'0'
    });

    jQuery('#resultLoading>div:first').css({
        'width': '250px',
        'height':'75px',
        'text-align': 'center',
        'position': 'fixed',
        'top':'0',
        'left':'0',
        'right':'0',
        'bottom':'0',
        'margin':'auto',
        'font-size':'16px',
        'z-index':'10',
        'color':'#ffffff'

    });

    jQuery('#resultLoading .bg').height('100%');
       jQuery('#resultLoading').fadeIn(300);
    jQuery('body').css('cursor', 'wait');
}

function ajaxindicatorstop()
{
    jQuery('#resultLoading .bg').height('100%');
       jQuery('#resultLoading').fadeOut(300);
    jQuery('body').css('cursor', 'default');
}

</script>
 <script type="text/javascript">

   
    var city_id='<?php echo $this->uri->segment(3); ?>  ';
    //console.log(city_id);
    get_top_city(city_id);
     function get_top_city(id){
$('#university').empty();
//$('#icontab-2').empty();
    	$.ajax({
    		type:'POST',
    		url:'<?php echo base_url()?>explore/get_top_city_view',
    		data:{'id':id},
            beforeSend: function () {
                          ajaxindicatorstart('Loading.....');
                     },
    		success:function(res){
    			var obj=JSON.parse(res);
                setTimeout(function(){
                                     ajaxindicatorstop();
                                    
                                
                            }, 1000);
    			console.log(obj);
    			var html='';
    			if (obj.length==0) {
					  html+= '<p>No University Found</p>';
					  console.log(html);
					}
    			$.each(obj,function(i,item){
    				console.log(item);

    			var cource='';
					if(item.course_spe==null){
						cource='-';
					}else if(item.course_spe!=null){
						cource='-'+item.course_spe;
					}
    			
						html+='<div class="col-md-6">'+
		                    '<article class="post-wrapper list-article">'+
		                      '<div class="hover-overlay brand-bg"></div>'+
		                      '<div class="thumb-wrapper waves-effect waves-block waves-light">'+
		                       ' <a href="#"><img src="<?php echo base_url();?>uploads/coverimages/'+item.ui_coverimage+'" class="img-responsive" alt="" style="width:178px;height:228px"></a>'+
		                     ' </div>'+
		                      '<div class="blog-content">'+
		                        '<header class="entry-header-wrapper sticky">'+
		                          '<div class="entry-header">'+
		                            '<h3 class="entry-title" style="font-size: 13px;font-weight:  bold;"><a href="<?php echo base_url()?>listing/index/'+item.um_name+'" class="white-link link-underline">'+item.um_name+'</a></h3>'+
		                            '<div class="entry-meta">'+
		                                '<ul class="list-inline">'+
		                                    '<li>'+
		                                       ' <i class="fa fa-map-marker"></i> <a href="#">'+item.cim_name+'</a>'+
		                                    '</li>'+
		                                    '<li>'+
		                                     
		                                    '</li></ul></div></div></header>'+
		                              '<div class="entry-content"><p>'+item.ui_address+'</p>'+
		                         '</div></div></article></div>';
    			});
    			//html+=' </div></div></div></div></div>';
    			$('#university').html(html);

    		
    		}
    	});
    }
// </script>