d<style>

.text-white {
    color: #fff;
}
.text-center {
    text-align:center;
}
.Space10 {
    height: 10px !important;
}
.offset-l1 {
    margin-left: 1.3% !important;
}
.show-more {
    color: #F26223;
}
.card-custom {
    margin: 0.4rem 0 0.4rem 0 !important;
}
.container .row {
    margin-bottom: 12px;
}
.apply-now {
    padding: 3px !important;
    color: #ffffff;
    font-size: 14px;
    font-weight: 500;
    background-color: #F26223 !important;
}
.course-icons {
    padding: 0 0rem !important;
    color: #5D6F71;
    font-size: 14px;
    font-weight: 500;
}
.inner-container {
    margin-left: 30px !important;
    margin-right: 30px !important;
}
.container .row {
    margin-left: -0.75rem;
    margin-right: -0.75rem;
}
.card {
    position: relative;
    margin: 0.5rem 0 1rem 0;
    background-color: #fff;
    transition: box-shadow .25s;
    border-radius: 2px;
}
.ucourse-details {
    border-right: 1px solid #ACBDBC;
    border-bottom: 1px solid #ACBDBC;
    border-top: 1px solid #ACBDBC;
    border-left: 1px solid #ACBDBC;
    background-color: #F6F7F9;
    color: #4580B8;
}

 .input-field div.error{
    position: relative;
    top: -1rem;
    left: 3rem;
    font-size: 0.8rem;
    color:#FF4081;
    -webkit-transform: translateY(0%);
    -ms-transform: translateY(0%);
    -o-transform: translateY(0%);
    transform: translateY(0%);
  }
.div-registertab{
	display:none;
}
.banner-title-name {
    color: #ffffff;
}
.banner-colg-name {
    z-index: 0;
    margin: -8% auto auto auto;
}
.colg-name-div {
    background-color: #4D7AA6;
    opacity: 0.9;
}
#chart-dashboard {
    padding-top: 25px;
}
.utitle-img {
    padding: 9px !important;
}
#content .container .row {
    margin-bottom: 10px;
}
.userlink {
    cursor: pointer;
    margin-left: 45%;
}

.modal {
    display: none;
    position: fixed;
    left: 0;
    right: 0;
    background-color: #a7a0a000;
    padding: 24px;
    max-height: 100%;
    width: 100%;
    margin: auto;
    overflow-y: auto;
    z-index: 1000;
    -webkit-border-radius: 2px;
    -moz-border-radius: 2px;
    border-radius: 2px;
    background-clip: padding-box;
    -webkit-transform: translate(0);
    -moz-transform: translate(0);
    -ms-transform: translate(0);
    -o-transform: translate(0);
    transform: translate(1);
    will-change: top;
    
opacity: 1.5;
}
.modal-overlay {
    position: fixed;
    z-index: 999;
    top: -25%;
    left: 0;
    bottom: 0;
    right: 0;
    height: 125%;
    width: 100%;
    background: #000;
    display: none;
    will-change: opacity;
    }
   label {
    font-size: 12px;
    color: black;
}
</style>

<div class="university-bg"> 		
			<div class="col s12 l12 m12">
					<img src="<?php echo base_url('uploads/coverimages/'.$universData->ui_coverimage); ?>" class="responsive-img banner banner-width" style="width:100%"/>
				</div>
				<div class="col s12 l12 m12 banner-colg-name">
					<div class="row">
					<div class="col s12 l6 m6 colg-name-div">
						<div class="col s12 l8 m8 offset-l4">
							<h2 class="banner-title-name" style="margin-left: 25px;margin-top: 13px;"><?php echo $universData->um_name; ?></h2>	
							<div class="col s4 m1 l1 banner-title-name">
								<i class="mdi-action-room"></i>
							</div>
							<div class="col s6 m11 l11 banner-title-name banner-addres" style="margin-left:-14px">
								<?php echo substr($universData->ui_address,0,58).'...'; ?>
							</div>
						</div>
					</div>
					<div class="col s12 l6 m6"></div>
					</div>
				</div>
			<!-- START CONTENT -->
            <section id="content" style=" background-color: #e0e0e0;">
<div class="row">
	<div class="col s12 l12 m12">
                <!--start container-->
                <div id="container">

                    <!--chart dashboard start-->
                    <div id="chart-dashboard">
						
                            <div class="col s12 m12 l12 university-card" style="margin-top:16px;">
                                <div class="card card-custom">
									<div class="row inner-container university-card">
										<div class="col s4 m1 l1 center-align utitle-img">
											<img src="<?php echo base_url('assets/images/course-icon.jpg');?>" class="responsive-img" style="width: 25px;margin-top: 7px;margin-left: -80px;"/>
										</div>
										<div class="col s6 m11 l11 utitle-name" style="margin-top: 20px;font-weight: bold;">
											<h3><b style="margin-left: -81px;">Course</b></h3>	
										</div>
									</div>
									
	<?php  //echo '<pre>', print_r($uniCourse); 
	if(isset($uniCourse))

$userdata=$this->session->userdata('logged_in');
if(!empty($userdata)){
$user_id=$userdata['id'];
//echo '<pre>', print_r($this->session->userdata('logged_in'));
$query = $this->db->get_where('student_mstr',array('stm_user_id'=>$user_id));
$smdata=$query->row();

if(!empty($userid)){
	$userid=$smdata->stm_id;
$query = $this->db->get_where('student_personal_details',array('spd_stm_id'=>$userid));
$smdata_edu=$query->row();
}
}
$el=null;
	?>
	<?php $i=0;foreach($uniCourse as $uniCrs){
		if(!empty($smdata_edu->eligi_cate_id))
		{
				$el=$uniCrs->uc_is_approved ==1 && $uniCrs->uc_eligibility ==$smdata_edu->eligi_cate_id;
		}else
		{
			$el=$uniCrs->uc_is_approved ==1;
		}
				if($el){
				// if($uniCrs->uc_is_approved ==1 ){
					$imp=explode(',',$uniCrs->uc_eligibility);
				$crsStream = $this->academic_model->getCourseById($uniCrs->uc_parent);
				 //echo '<pre>', print_r($crsStream);
				$query=$this->db->where('id',$uniCrs->uc_spe_id)->get('master_course_specialization');
				 $spe=$query->result();
				$crsProgram	= $this->academic_model->getSpe_uni($uniCrs->uc_com_id);
				// $crsProgram= $this->academic_model->getSpe_uni($uniCrs->uc_com_id);
				$type	= $this->academic_model->getTypeProgram($uniCrs->uc_course_type);
				 // echo '<pre>', print_r($crsProgram);
				$uc_fees = explode(',',$uniCrs->uc_fees);
				// echo '<pre>', print_r($uc_fees);
				$umId = $uniCrs->uc_um_id;
				
				for($i=0;$i<count($uc_fees);$i++){	
					
					$LumpsumFeesData = $this->academic_model->getLumsumpFees($uc_fees[$i],$umId,'Lumpsum');
					
					$InstallmentFeesData = $this->academic_model->getInstallmentFees($uc_fees[$i],$umId,'Installment');
						
				}
				?>
				<div class="row inner-container ucourse-details" style="margin-bottom: -26px;margin-top: -17px;">
					<div class="col s12 m12 l12 Space10"></div>
					<div class="row">
						<div class="col s12 m12 l12" >
							<div class="col s12 m13 l3 offset-l1 inner-title">
								 <?php $spe1=''; if(!empty($spe[0]->name)) {
									$spe1= ' - '.$spe[0]->name;

								}else{
									$spe1= '';
								}
								?>
								<a style="color:blue" href="<?php echo base_url()?>listing/viewcource/<?php echo $uniCrs->uc_id  ?>/<?php echo$crsProgram[0]->name;  ?>"><?php echo $crsProgram[0]->name; ?>  <?php echo  $spe1 ?></a>
								

							</div>
							<!--div class="col s12 m12 l2 ustar">
								<img src="<?php echo base_url('assets/images/stars-01.png'); ?>" class="responsive-img" />
								<img src="<?php echo base_url('assets/images/stars-01.png'); ?>" class="responsive-img" />
								<img src="<?php echo base_url('assets/images/stars-01.png'); ?>" class="responsive-img" />
								<img src="<?php echo base_url('assets/images/stars-01.png'); ?>" class="responsive-img" />
								<img src="<?php echo base_url('assets/images/stars-02.png'); ?>" class="responsive-img" />
							</div-->
							<!--div class="col s12 m1 l1 ureview">26 Reviews</div-->
							
						</div>
					</div>
					<hr>
					<div class="row">
						<div class="col s12 m12 l12 Space10"></div>
						<div class="col s12 m1 l1 offset-l1">
							<div class="row">
								<div class="col s5 m5 l5 center-align">
								<img src="<?php echo base_url('assets/images/icon-01.png'); ?>" class="responsive-img" />
								</div>
								<div class="col s7 m7 l7 no-padding-origin course-icons" style="margin-top: -4px;">
								<?php echo $uniCrs->uc_course_length; ?>
								</div>
							</div>
						</div>
						<div class="col s12 m1 l1">
							<div class="row">
								<div class="col s5 m5 l5 center-align">
								<img src="<?php echo base_url('assets/images/icon-02.png'); ?>" class="responsive-img" />
								</div>
								<div class="col s7 m7 l7 no-padding-origin course-icons" style="font-size: 12px;margin-top: -4px;">
								<?php echo $type[0]->name; ?>
								</div>
							</div>
						</div>
						<div class="col s12 m1 l1">
							<div class="row">
								<div class="col s5 m5 l5 center-align">
								<img src="<?php echo base_url('assets/images/icon-03.png'); ?>" class="responsive-img" />
								</div>
								<div class="col s7 m7 l7 no-padding-origin course-icons" style="font-size: 12px;margin-top: -4px;">
									<?php 
								foreach ($imp as $k=>$value) {
								$crsEli= $this->academic_model->getEligi($value);
								echo ($crsEli[0]->name).',';
								}
								?>
								</div>
							</div>
						</div>
						<div class="col s12 m1 l1">
							<div class="row">
								<div class="col s5 m5 l5 center-align">
								<img src="<?php echo base_url('assets/images/icon-04.png'); ?>" class="responsive-img" />
								</div>
								<div class="col s7 m7 l7 no-padding-origin course-icons" style="font-size: 12px;margin-top: -4px;">
								CET
								</div>
							</div>
						</div>
						<div class="col s12 m2 l1">
							<div class="row">
								<div class="col s5 m5 l5 center-align">
								<img src="<?php echo base_url('assets/images/icon-05.png'); ?>" class="responsive-img" />
								</div>
								<div class="col s7 m7 l7 no-padding-origin course-icons" style="font-size: 14px;margin-top: -4px;">
								<?php 

									echo $uniCrs->uc_fees; 
								/* if(isset($LumpsumFeesData)){ 
										echo $LumpsumFeesData->ufeem_amount;
								}else{
									$InstallmentFeesData->ufeem_amount;
								} */ ?><!--span>  &nbsp;Per Year</span-->
								</div>
							</div>
						</div>
						<div class="col s12 m1 l1">
							<p class="show-more">Show More</p>
						</div>
						<div class="col s12 m2 l2 offset-l1">
							<div class="row">
								<div class="col s5 m3 l3 center-align">
								<img src="<?php echo base_url('assets/images/icon-06.png'); ?>" class="responsive-img" />
								</div>
								<div class="col s7 m7 l7 no-padding-origin course-icons">
								Download Brochure
								</div>
							</div>
						</div>
						<?php $userdata = $this->session->userdata('logged_in'); //echo '<pre>',print_r($userdata); ?>
						<div class="col s12 m2 l2">
							<div class="row">
								<div class="col s5 m3 l3 center-align">
								<!--img src="<?php echo base_url('assets/images/icon-07.png'); ?>" class="responsive-img" /-->
								</div>
								<?php if(!$userdata){ ?>
								<div class="col s7 m7 l7 no-padding-origin course-icons">
									<a data-toggle="modal" data-target="#exampleModal" data-id="<?php echo $uniCrs->uc_id; ?>" data-name="shortlist" data-whatever="@mdo" class="short_modal" id="shortlist-<?php echo $uniCrs->uc_id; ?>"><img src="<?php echo base_url()?>assets/img/heart.png" style="width:30px;height: 30px;" title="Shortlist">  </a>
								</div>
								<?php }else{ ?>
								 <!-- Modal Trigger -->
								<?php
								
								if($userdata['role']==2){
								$user_id=$userdata['id'];
								
								$course_id=$uniCrs->uc_id;
								$query = $this->db->get_where('student_mstr',array('stm_user_id'=>$user_id));
								$smdata=$query->row();
						
							$userid=$smdata->stm_id;
							$query_1 = $this->db->get_where('student_applications',array('sa_stm_id'=>$userid,'sa_uc_id'=>$course_id));
							$query = $this->db->get_where('shortlisted_courses',array('sc_stm_id'=>$userid,'sc_uc_id'=>$course_id));
							
								if(!$query_1->num_rows() > 0 )
								{ 
							if($query->num_rows() > 0 )
								{ 
							?>
							<div class="col s7 m7 l7 no-padding-origin course-icons">
							<a class="modal-trigger" ><img src="<?php echo base_url()?>assets/img/heart_1.png" style="width:30px;height: 30px;" title="shortlisted"> </a>
							</div>
							<?php
								
								}
								else{
									?>
									<div class="col s7 m7 l7 no-padding-origin course-icons">
														<a class="modal-trigger" onclick="shor(<?php echo $uniCrs->uc_id; ?>,<?php echo $userdata['id']; ?>)" ><img src="<?php echo base_url()?>assets/img/heart.png" style="width:30px;height: 30px;"> </a>
													</div>
									<?php
								  
								}
								}				?>
								
								<?php } } ?>
								
							</div>
						</div>
						<div class="col s8 m1 l1">
							<div class="row">
								<div class="col s12 m10 l10 center-align offset-s2 apply-now">
								<?php if(!$userdata){ ?>
								<a  data-toggle="modal" data-target="#exampleModal" data-id="<?php echo $uniCrs->uc_id; ?>" data-name="apply" class="apply_modal" data-whatever="@mdo" href="#" id="apply-<?php echo $uniCrs->uc_id; ?>" style="margin-right: 0px;">
								<b style="color:white">Apply</b>  </a>
								<?php }else{ ?>
								
									<?php
							 $user_id=$userdata['id'];
								if($userdata['role']==2){
								$course_id=$uniCrs->uc_id;
								$query = $this->db->get_where('student_mstr',array('stm_user_id'=>$user_id));
								$smdata=$query->row();
		
							 $userid=$smdata->stm_id;
							
							// $verify = $this->db->get_where('users',array('id'=>$user_id));
							// $emailverify=$verify->result();
							// if($verify->num_rows() > 0)
								// { 
							$query = $this->db->get_where('student_applications',array('sa_stm_id'=>$userid,'sa_uc_id'=>$course_id));
								if($query->num_rows() > 0)
								{ 
						
							?>
							<a class="modal-trigger text-white "  id="<?php echo $uniCrs->uc_id; ?>" >
								<b style="color:white" >Applied</b> </a>
							<?php
								
								}
								else{
									
									?>
									<a class="modal-trigger text-white "  id="<?php echo $uniCrs->uc_id; ?>" >
									<b style="color:white" onclick="apply(<?php echo $uniCrs->uc_id; ?>,<?php echo $userdata['id']; ?>)">Apply Now</b> </a>
									<?php
								  
								}
								?>
			
								<?php
								} }
								?>
								
								
								
								
								
								<!-- <div id="modal-course-<?php echo $uniCrs->uc_id; ?>" class="modal" style="    height: 50%;">
								  <div class="modal-header"><a href="#" class="waves-effect waves-red btn-flat modal-action modal-close" style="    margin-left: 659px;"><i class="material-icons dp48"><b style="color:white">close</b></i></a></div>
								  <div >
									<h5 style="color: #000;margin-top: 47px;font-size: 20px;    margin-bottom: 50px;" class="text-center">Are you sure to apply ? </h5>
									<form action="<?php echo base_url('student/course_apply'); ?>" method="post">
										<div class="row center">
											<a class="btn waves-effect waves-light blue div12-btn modal-close">Cancel</a>
											<input type="hidden" name="course_id" value="<?php echo $uniCrs->uc_id; ?>">
											<input type="hidden" name="user_id" value="<?php echo $userdata['id']; ?>">
											<input type="submit" class="btn waves-effect waves-light green div12-btn" value="Ok" style="margin-top: -8px;font-weight: bold;"/>
										</div>
									</form>
								  </div>
								</div> -->
								<?php } ?>
								</div>
								<div class="col s12 m2 l2"></div>
							</div>
						</div>
						
						<div class="col s12 m12 l12 Space10"> </div>
					</div>
				</div>
				<div class="col s12 m12 l12 Space10 white-bg"></div>
				<?php } 
			 ?>
                                </div>
                            </div>
							
		
				
				
				
							<div class="col s12 m12 l12">
                                <div class="card card-custom">
									<div class="row inner-container">
										<div class="col s4 m1 l1 center-align utitle-img">
											<img src="<?php echo base_url('assets/images/facilities-icon.jpg');?>" class="responsive-img" style="margin-left: -84PX;width: 25px;margin-top: 4px;"/>
										</div>
										<div class="col s6 m11 l11 utitle-name" style="margin-top: 20px;font-weight: bold;">
											<h3><b style="margin-left:-81px">Facilities</b></h3>	
										</div>
										<?php
				if(!empty($uniFacility)){
				 ?>
										<div class="row">
											<div class="col s12 m12 l12" style="margin-top: 14px;margin-left: 84px;">
												<?php if(in_array('facility_computer_lab',$uniFacility)){ ?>
												<div class="col s12 m1 l1 offset-l1">
													<div class="row">
														<div class="col s12 m12 l12 center-align">
														<img src="<?php echo base_url('assets/images/facility-icon-01.png'); ?>" class="responsive-img" />
														</div>
														<div class="col s12 m12 l12 utext-color center-align">
														Camp Labs
														</div>
													</div>
												</div>
												<?php } 
												if(in_array('facility_library',$uniFacility)){
												?>
												<div class="col s12 m1 l1">
													<div class="row">
														<div class="col s12 m12 l12 center-align">
														<img src="<?php echo base_url('assets/images/facility-icon-02.png'); ?>" class="responsive-img" />
														</div>
														<div class="col s12 m12 l12 utext-color center-align">
														Library
														</div>
													</div>
												</div>
												<?php } 
												if(in_array('facility_laboratory',$uniFacility)){
												?>
												<div class="col s12 m1 l1">
													<div class="row">
														<div class="col s12 m12 l12 center-align">
														<img src="<?php echo base_url('assets/images/facility-icon-03.png'); ?>" class="responsive-img" />
														</div>
														<div class="col s12 m12 l12 utext-color center-align">
														Laboratory
														</div>
													</div>
												</div>
												<?php } 
												if(in_array('facility_studyroom',$uniFacility)){
												?>
												<div class="col s12 m1 l1">
													<div class="row">
														<div class="col s12 m12 l12 center-align">
														<img src="<?php echo base_url('assets/images/facility-icon-04.png'); ?>" class="responsive-img" />
														</div>
														<div class="col s12 m12 l12 utext-color center-align">
														Auditorium
														</div>
													</div>
												</div>
												<?php } 
												if(in_array('facility_sport',$uniFacility)){
												?>
												<div class="col s12 m1 l1 offset-l1">
													<div class="row">
														<div class="col s12 m12 l12 center-align">
														<img src="<?php echo base_url('assets/images/facility-icon-05.png'); ?>" class="responsive-img" />
														</div>
														<div class="col s12 m12 l12 utext-color center-align">
														Sport
														</div>
													</div>
												</div>
												<?php } 
												if(in_array('facility_gym',$uniFacility)){
												?>
												<div class="col s12 m1 l1">
													<div class="row">
														<div class="col s12 m12 l12 center-align">
														<img src="<?php echo base_url('assets/images/facility-icon-06.png'); ?>" class="responsive-img" />
														</div>
														<div class="col s12 m12 l12 utext-color center-align">
														Gym
														</div>
													</div>
												</div>
												<?php } 
												if(in_array('facility_medical',$uniFacility)){
												?>
												<div class="col s12 m1 l1">
													<div class="row">
														<div class="col s12 m12 l12 center-align">
														<img src="<?php echo base_url('assets/images/facility-icon-07.png'); ?>" class="responsive-img" />
														</div>
														<div class="col s12 m12 l12 utext-color center-align">
														Medical
														</div>
													</div>
												</div>
												<?php } 
												if(in_array('facility_canteen',$uniFacility)){
												?>
												<div class="col s12 m1 l1">
													<div class="row">
														<div class="col s12 m12 l12 center-align">
														<img src="<?php echo base_url('assets/images/facility-icon-08.png'); ?>" class="responsive-img" />
														</div>
														<div class="col s12 m12 l12 utext-color center-align">
														Cafeteria
														</div>
													</div>
												</div>
												<?php } 
												if(in_array('facility_transport',$uniFacility)){
												?>
												<div class="col s12 m1 l1">
													<div class="row">
														<div class="col s12 m12 l12 center-align">
														<img src="<?php echo base_url('assets/images/facility-icon-10.png'); ?>" class="responsive-img" />
														</div>
														<div class="col s12 m12 l12 utext-color center-align">
														Transport
														</div>
													</div>
												</div>
												<?php } 
												if(in_array('facility_hostel',$uniFacility)){
												?>
												<div class="col s12 m1 l1">
													<div class="row">
														<div class="col s12 m12 l12 center-align">
														<img src="<?php echo base_url('assets/images/facility-icon-11.png'); ?>" class="responsive-img" />
														</div>
														<div class="col s12 m12 l12 utext-color center-align">
														Hostel
														</div>
													</div>
												</div>
												<?php } 
												?>
											</div>
											<div class="col s12 m12 l12 Space10"></div>
										</div>	
										<?php } ?>
									</div>
								</div>
							</div>
					
							
							<div class="col s12 m12 l12">
                                <div class="card card-custom">
									<div class="row inner-container">
										<div class="col s4 m1 l1 center-align utitle-img">
											<img src="<?php echo base_url('assets/images/about-collegeicon.jpg');?>" class="responsive-img" style="width: 28px; margin-left: -80px"/>
										</div>
										<div class="col s6 m11 l11 utitle-name" style="margin-top: 20px;font-weight: bold;">
											<h3><b style="margin-left:-78px">About College</b></h3>	
										</div>
										<div class="col s12 m11 l11 offset-l1">
											<p style="    margin-left: 13px;"><?php echo $universData->ui_aboutus; ?></p>
										</div>
											<div class="col s12 m12 l12 center-align">
												<p class="show-more">Show More</p>
											</div>
									</div>
								</div>
							</div>
							<div class="col s12 m12 l12">
                                <div class="card card-custom">
									<div class="row inner-container">
										<div class="col s4 m1 l1 center-align utitle-img">
											<img src="<?php echo base_url('assets/images/Gallery-icon.jpg');?>" class="responsive-img" style="width: 28px; margin-left: -80px;margin-top: 6px;"/>
										</div>
										<div class="col s6 m11 l11 utitle-name" style="margin-top: 20px;font-weight: bold;">
										<h3><b style="margin-left:-80px">Gallery</b></h3>	
										</div>
										<div style="margin-left:80px">
										<div class="row">
										<div class="col-md-6"><b>Photo</b></div>
										<div class="col-md-6"><b>Video</b></div>
											
											<?php foreach($uniMedia as $uMed){
												if($uMed->ugm_media_type == 'PHOTO'){ ?>
											<div class="col-md-3">
												<img  src="<?php echo base_url('uploads/'.$uMed->ugm_link); ?>" class="responsive-img" style="width: 100%;    height: 20%;" />
											</div>
										<?php } } ?>
										
											
											<?php foreach($uniMedia as $uMed){
												if($uMed->ugm_media_type == 'VIDEO'){ ?>
											<div class="col-md-3">
												<video width="200" height="120" controls="">
													<source src="<?php echo base_url('uploads/'.$uMed->ugm_link); ?>" type="video/ogg" >
												</video>
											</div>
											<?php } } ?>	
										</div>
										</div>
										<div class="col s12 m12 l12 Space20"></div>
									</div>
								</div>
							</div>
							<div class="col s12 m12 l12">
                                <div class="card card-custom">
									<div class="row inner-container">
										<div class="col s12 m8 l8">
										<div class="col s4 m1 l1 center-align utitle-img">
											<img src="<?php echo base_url('assets/images/Location-icon.jpg');?>" class="responsive-img" style="width: 28px; margin-left: -80px;margin-top: 6px;"/>
										</div>
										<div class="col s6 m11 l11 utitle-name" style="margin-top: 20px;font-weight: bold;">
											<h3><b style="margin-left:-57px">Location</b></h3>
											<p class="location-p"><?php echo $universData->ui_address; ?></p><hr>
										</div>
										<hr>
										<div class="col s12 m12 l12 Space30"></div>
										<div class="col s4 m1 l1 center-align utitle-img">
											<img src="<?php echo base_url('assets/images/nearby-colleges-icon.jpg');?>" class="responsive-img" style="width: 28px; margin-left: -80px;margin-top: 6px;"/>
										</div>
										<div class="col s6 m11 l11 utitle-name" style="margin-top: 20px;font-weight: bold;">
											<h3><b style="margin-left:-57px">Nearby College</b></h3>	
										</div>
										<div class="col s12 m12 l12 Space30"></div>
										</div>
										<div class="col s12 m4 l4">
										<div class="col s12 m12 l12 Space10"></div>
										<img src="<?php echo base_url('assets/images/similar-college-01.png'); ?>" class="responsive-img" />
										<div class="col s12 m12 l12 Space10"></div>
										</div>
									</div>
								</div>
							</div>
							<div class="col s12 m12 l12">
                                <div class="card card-custom">
									<div class="row inner-container">
										<div class="col s4 m1 l1 center-align utitle-img">
											<img src="<?php echo base_url('assets/images/Similar-Colleges-icon.jpg');?>" class="responsive-img" style="width: 28px; margin-left: -95px;margin-top: 6px;"/>
										</div>
										<div class="col s6 m11 l11 utitle-name" style="margin-top: 20px;font-weight: bold;">
											<h3><b style="margin-left:-79px">Similer College</b></h3>	
										</div>
										<div class="row">
									
											<div class="col s12 m12 l12 Space20">
											</div>
											<div class="col s12 m4 l4 offset-l2">
											<img src="<?php echo base_url('assets/images/similar-college-01.png'); ?>" class="responsive-img" />
											</div>
											<div class="col s12 m6 l6">
											<img src="<?php echo base_url('assets/images/similar-college-01.png'); ?>" class="responsive-img" />
											</div>
											</div>
											<div class="row">
											<div class="col s12 m4 l4 offset-l2">
											<img src="<?php echo base_url('assets/images/similar-college-01.png'); ?>" class="responsive-img" />
											</div>
											<div class="col s12 m6 l6">
											<img src="<?php echo base_url('assets/images/similar-college-01.png'); ?>" class="responsive-img" />
											</div>
											<div class="col s12 m12 l12 center-align">
												<p class="show-more">Show More</p>
											</div>
										</div>										
									</div>
								</div>
							</div>
						
				</div>
			</div>	
			</div>
	</div>
			</section>

<!-- <div class="bd-example">
  <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Open modal for @mdo</button> -->
 
 <!--  <div class="modal fade in" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title" id="exampleModalLabel">Login</h4>
        </div>

        <div class="modal-body">
        	 <div class="form-group error">
         		
              </div>
          <form id="frmlogin" method="post" autocomplete="on">
            <div class="form-group">
              <label for="recipient-name" class="form-control-label">Email</label>
              <input type="text" id="email" class="form-control" placeholder="Email">
            </div>
             <div class="form-group">
              <label for="recipient-name" class="form-control-label">Password</label>
              <input type="password" id="password" class="form-control" placeholder="Password">
            </div>
            <div class="form-group">
            
              <input type="hidden" id="applytype" name="applytype" class="form-control" placeholder="Password">
               <input type="hidden" id="course_id" name="course_id" class="form-control" placeholder="Password">
            </div>
          </form>
        </div>
        <div class="modal-footer" style="text-align: center;">
        	<button type="button" class="btn btn-primary green login">Login</button> 
          <button type="button" class="btn btn-secondary red" data-dismiss="modal">Close</button>
          
        </div>
      </div>
    </div>
  </div>
</div> --> 

<div class="bd-example">
  <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Open modal for @mdo</button> -->
 
  <div class="modal fade in" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h2 class="modal-title" id="exampleModalLabel">Student Login</h2>
        </div>
  
       
        <div class="modal-body">
        	
                <div role="tabpanel">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#uploadTab" aria-controls="uploadTab" role="tab" data-toggle="tab" id="log">Login</a>

                        </li>
                        <li role="presentation"><a href="#browseTab" aria-controls="browseTab" role="tab" data-toggle="tab" id="reg">Register</a>

                        </li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="uploadTab">
                        	<br>
                        	 <div class="form-group error">
         		
             				 </div>
				          <form id="frmlogin" method="post" autocomplete="on">
				            <div class="form-group">
				              <label for="recipient-name" class="form-control-label">Email</label>
				              <input type="text" id="email" class="form-control" placeholder="Email">
				            </div>
				             <div class="form-group">
				              <label for="recipient-name" class="form-control-label">Password</label>
				              <input type="password" id="password" class="form-control" placeholder="Password">
				            </div>
				            <div class="form-group">
				            
				              <input type="hidden" id="applytype" name="applytype" class="form-control" placeholder="Password">
				               <input type="hidden" id="course_id" name="course_id" class="form-control" placeholder="Password">
				            </div>
				          </form>
  
				        <div class="modal-footer" style="text-align: center;border-top:0px solid #e5e5e5;">
				        	<button type="button" class="btn btn-primary green login">Login</button> 
				          <button type="button" class="btn btn-secondary red" data-dismiss="modal">Close</button>
				          
				        </div>
            		</div>
                     <div role="tabpanel" class="tab-pane" id="browseTab">
                     	<br>
                     			 <div class="form-group error">
         		
             				 </div>

				          <form id="frmlogin" method="post" autocomplete="on">
				          	<div class="form-group">
				              <label for="recipient-name" class="form-control-label">Name</label>
				              <input type="text" id="name" class="form-control" placeholder="Name">
				            </div>

				            <div class="form-group">
				              <label for="recipient-name" class="form-control-label">Email</label>
				              <input type="text" id="email_reg" class="form-control" placeholder="Email">
				            </div>
				             <div class="form-group">
				              <label for="recipient-name" class="form-control-label">Password</label>
				              <input type="password" id="password_reg" class="form-control" placeholder="Password">
				            </div>
				             <div class="form-group">
				              <label for="recipient-name" class="form-control-label">Confirm Password</label>
				              <input type="password" id="cpassword" class="form-control" placeholder="Confirm Password">
				            </div>
				            <div class="form-group">
				            
				              <input type="hidden" id="applytype" name="applytype" class="form-control" placeholder="Password">
				               <input type="hidden" id="course_id" name="course_id" class="form-control" placeholder="Password">
				            </div>
				          </form>
  
				        <div class="modal-footer" style="text-align: center;border-top: 0px solid #e5e5e5;">
				        	<button type="button" class="btn btn-primary green register">Register</button> 
				          <button type="button" class="btn btn-secondary red" data-dismiss="modal">Close</button>
				          
				        </div>
                     </div>
                    </div>
                </div>
        	
   </div>
      </div>
    </div>
  </div>
</div>
<?php 

	/* Course Master Search Data START */
	
	$query = $this->db->query("SELECT * FROM course_mstr");
	$course_mstr = $query->result();
	
	$new_crs_mstr = array();  
	foreach($course_mstr as $crs_mstr){
		$crsSearch = trim($crs_mstr->com_name);
		array_push($new_crs_mstr,$crsSearch);
	}
	$new_crs_mstr = array_unique($new_crs_mstr);
	$newCMstr = implode('", "',$new_crs_mstr);
	
	/* Course Master Search Data END */
	
	/* University Master Search Data START */
	
	$query = $this->db->query("SELECT * FROM uni_mstr");
	$uni_mstr = $query->result();
			
  
	$new_uni_mstr = array();  
	foreach($uni_mstr as $u_mstr){
		$uniSearch = trim($u_mstr->um_name);
		array_push($new_uni_mstr,$uniSearch);
	}
	  
	$new_uni_mstr = array_unique($new_uni_mstr);
	  
	$newUMstr = implode('", "',$new_uni_mstr);
	  
	/* University Master Search Data START */
  ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> 
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/1.3.1/js/toastr.js"></script>
<script type="text/javascript">
function ajaxindicatorstart(text)
{
	if(jQuery('body').find('#resultLoading').attr('id') != 'resultLoading'){
	jQuery('body').append('<div id="resultLoading" style="display:none"><div><img src="<?php echo base_url()?>uploads/ajax-loader.gif"><div>'+text+'</div></div><div class="bg"></div></div>');
	}

	jQuery('#resultLoading').css({
		'width':'100%',
		'height':'100%',
		'position':'fixed',
		'z-index':'10000000',
		'top':'0',
		'left':'0',
		'right':'0',
		'bottom':'0',
		'margin':'auto'
	});

	jQuery('#resultLoading .bg').css({
		'background':'#000000',
		'opacity':'0.7',
		'width':'100%',
		'height':'100%',
		'position':'absolute',
		'top':'0'
	});

	jQuery('#resultLoading>div:first').css({
		'width': '250px',
		'height':'75px',
		'text-align': 'center',
		'position': 'fixed',
		'top':'0',
		'left':'0',
		'right':'0',
		'bottom':'0',
		'margin':'auto',
		'font-size':'16px',
		'z-index':'10',
		'color':'#ffffff'

	});

    jQuery('#resultLoading .bg').height('100%');
       jQuery('#resultLoading').fadeIn(300);
    jQuery('body').css('cursor', 'wait');
}

function ajaxindicatorstop()
{
    jQuery('#resultLoading .bg').height('100%');
       jQuery('#resultLoading').fadeOut(300);
    jQuery('body').css('cursor', 'default');
}

</script>
<script type="text/javascript">
	
    var availableTags = ["<?php echo $newCMstr; ?>","<?php echo $newUMstr; ?>"];
	
    $( "#search_text" ).autocomplete({
      source: availableTags,
	  select: function(event, ui) {
		  //alert(ui.item.value);
			window.location.href = "<?php echo base_url(); ?>listing/index/"+ui.item.value;
	  }
    });
	$('.userlink').click(function(){
		 var ltval = $(this).attr('id');
		 if(ltval == 'registertab'){
			 $('.div-registertab').show();
			 $('.div-logintab').hide();
		 }else{
			 $('.div-registertab').hide();
			 $('.div-logintab').show();
		 }
	 });
	 $('.short_modal, .apply_modal').click(function(){
		 var name = $(this).data('name');
		 var id = $(this).data('id');
		 console.log(id);
		 console.log(name);
		 $('#applytype').val(name);
		  $('#course_id').val(id);
	});

	$('#reg, #log').click(function(){
	$('.error').hide();
	});


	 $('.login').click(function(){
	 	var email=$('#email').val();
	 	var password=$('#password').val();
	 	var applytype=$('#applytype').val();
	 	var course_id=$('#course_id').val();
	 	if(email==''){
	 		
	 		$('.error').html('<div class="alert danger-dark modal-title" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><i class="fa fa-times-circle"></i>  Enter Email</div>');
	 		$('#email').focus();
	 		return false;
	 	}else if(password==''){
	 			$('.error').html('<div class="alert danger-dark modal-title" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><i class="fa fa-times-circle"></i>  Enter Password</div>');
	 		$('#password').focus();
	 		return false;
	 	}else{
	 		//$('.error').html('<div class="alert success-dark modal-title" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><i class="fa fa-times-circle"></i>  Succesfully logged in</div>');
	 		
	 			 var dataString={
	 			 	'email':email,
	 			 	'password':password,
					 'course_id':course_id,
					 'applytype':applytype
				 };
				//console.log(dataString);
				 $.ajax({
					 type:'POST',
					 url:'<?php echo base_url(); ?>login/after_apply_login',
					 data:dataString,
					  beforeSend: function () {
           				  ajaxindicatorstart('Please wait..');
           			 },
					 success:function(res)
					 {
					 		var obj = JSON.parse(res);
					 	console.log(obj);
						 if(obj.msg=='Please verify your email address'){
						 	ajaxindicatorstop();
						   setTimeout(function(){
			                location.reload();
			              }, 1000);
						  			 
						
						 }else if(obj.msg=='Successfully')
						  { 
						  		
								setTimeout(function(){
						  			 ajaxindicatorstop();
						  			 toastr["success"]('Applied Succesfully');
								location.reload();
							}, 4000);

						 }else if(obj.msg=='Already Shortlisted'){
						 //	 ajaxindicatorstop();
						 	  toastr["error"]('Course Already Shortlisted');
						 	  setTimeout(function(){
						  			 ajaxindicatorstop();
						  			
								location.reload();
							}, 4000);
						 		 //$('.error').html('<div class="alert danger-dark modal-title" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><i class="fa fa-times-circle"></i> Course Already Applied</div>');
						 
						 }else if(obj.msg=='Already Applied'){
						 //	 ajaxindicatorstop();
						 	  toastr["error"]('Course Already Applied');
						 	  setTimeout(function(){
						  			 ajaxindicatorstop();
						  			
								location.reload();
							}, 4000);
						 		 //$('.error').html('<div class="alert danger-dark modal-title" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><i class="fa fa-times-circle"></i> Course Already Applied</div>');
						 
						 }else{
						  ajaxindicatorstop();
						 	 //toastr["error"]('Invalid username and password');
						  $('.error').html('<div class="alert danger-dark modal-title" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><i class="fa fa-times-circle"></i> Invalid Username and Password</div>');
						 }
					 }
				 });
	 	}
	 });

$('#email_reg').on('focusout', function() {
	$('.error').show();
	var email=$('#email_reg').val();
	$.ajax({
			type:'POST',
			url:'<?php echo base_url()?>admin/check_email',
			data:{'email':email},
			success:function(res){
				//alert(res);
				var obj=JSON.parse(res);
				console.log(obj);
				if(obj.msg=='Exitst'){
					
					$('.error').html('<div class="alert danger-dark modal-title" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><i class="fa fa-times-circle"></i>Email already exist</div>');
					$('#email_reg').focus();
					$('register').prop('disabled',false);
				}
	
		
			}
	})
   
});
	 $('.register').click(function(){
	 	$('.error').show();
	 	var name=$('#name').val();
	 	var email=$('#email_reg').val();
	 	var password=$('#password_reg').val();
	 	var cpassword=$('#cpassword').val();
	 	var applytype=$('#applytype').val();
	 	var course_id=$('#course_id').val();
	 	 var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	 	if(name==''){
	 		$('.error').html('<div class="alert danger-dark modal-title" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><i class="fa fa-times-circle"></i>  Enter Name</div>');
	 		$('#name').focus();
	 		return false;
	 	}else if(email==''){
	 		
	 		$('.error').html('<div class="alert danger-dark modal-title" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><i class="fa fa-times-circle"></i>  Enter Email</div>');
	 		$('#email_reg').focus();
	 		return false;
	 	}else if(!re.test(email))
	{
			$('#email_reg').focus();
		$('.error').html('<div class="alert danger-dark modal-title" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><i class="fa fa-times-circle"></i>  Enter Valid email address</div>');
			return false;
	}else if(password==''){
	 			$('.error').html('<div class="alert danger-dark modal-title" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><i class="fa fa-times-circle"></i>  Enter Password</div>');
	 		$('#password_reg').focus();
	 		return false;
	 	}else if(cpassword==''){
	 			$('.error').html('<div class="alert danger-dark modal-title" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><i class="fa fa-times-circle"></i>  Enter Confirm Password</div>');
	 		$('#cpassword').focus();
	 		return false;
	 	}else if(password!=cpassword){
	 			$('.error').html('<div class="alert danger-dark modal-title" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><i class="fa fa-times-circle"></i>  Enter Password does not match</div>');
	 		$('#cpassword').focus();
	 		return false;
	 	}else{
	 			$('.error').hide();
	 		var dataString={
	 			 	'email':email,
	 			 	'password':password,
					 'uname':name,
					
				 };
				//console.log(dataString);
				 $.ajax({
					 type:'POST',
					 url:'<?php echo base_url(); ?>login/register',
					 data:dataString,
					  beforeSend: function () {
           				  ajaxindicatorstart('Please wait..');
           			 },
					 success:function(res)
					 {
					 		var obj = JSON.parse(res);
					 	console.log(obj);
					 	  if(obj.msg=='You are successfully registered..!  Please check your email to verify !')
						  { 
						  		setTimeout(function(){
						  			 ajaxindicatorstop();
						  			 toastr["success"]('Registered Succesfully');
								location.reload();
							}, 4000);
						

						 }else if(obj.msg=='User already present'){
						 	setTimeout(function(){
						  			 ajaxindicatorstop();
						  			 toastr["success"]('Email Exist');
								location.reload();
							}, 4000);
						 }
					 }

				});
	 	}
	 });

	 function shor(id,user_id){
		//alert(user_id);
		  // var txt;
			//if (confirm("Are you sure ?") == true) {
				//txt = "You pressed OK!";
				 var dataString={
					 'course_id':id,
					 'user_id':user_id
				 };
				//console.log(dataString);
				 $.ajax({
					 type:'POST',
					 url:'<?php echo base_url(); ?>student/course_shortlist',
					 data:dataString,
					 beforeSend: function () {
           				  ajaxindicatorstart('Please wait..');
           			 },
					 success:function(res)
					 {
					 		var obj = JSON.parse(res);
					 	console.log(obj);
						  if(obj.msg=='Please verify your email address')
						  { 
					  $('html,body').animate({
							        scrollTop: $(".second").offset().top},
							        'slow');
						  		  
						  	ajaxindicatorstop();
						 }else if(obj.msg=='Already Shortlisted'){
						 	 ajaxindicatorstop();
						 	toastr["error"]('Already Shortlisted');
						 }else{
						 	
						 	setTimeout(function(){
						  			 ajaxindicatorstop();
						  			toastr["success"]('Shortlisted successfull');
								location.reload();
							}, 4000);
						 }
					 }
				 });
				 
			// } else {
				//txt = "You pressed Cancel!";
			// }
		// verify(user_id);
	 }
	 
	  function apply(id,user_id){
		//alert(user_id);
		  // var txt;
		  //   ajaxindicatorstart('Please wait..');
		  // return false;
		  var email="<?php echo trim($userdata['email']);?>";
			//if (confirm("Are you sure ?") == true) {
				//txt = "You pressed OK!";
				 var dataString={
					 'course_id':id,
					 'user_id':user_id
				 };
				//console.log(dataString);
				 $.ajax({
					 type:'POST',
					 url:'<?php echo base_url(); ?>student/course_apply',
					 data:dataString,
					 beforeSend: function () {
           				  ajaxindicatorstart('Please wait..');
           			 },
					 success:function(res)
					 {
					 	var obj = JSON.parse(res);
					 	console.log(obj);
						  if(obj.msg=='Please verify your email address')
						  { 
						  		    $('html,body').animate({
							        scrollTop: $(".second").offset().top},
							        'slow');
						  		   // $('.second').css('background-color','blue');
						  	ajaxindicatorstop();

						 }else if(obj.msg=='Please Update your profile and education details'){
						 	toastr["info"](obj.msg).css('width','400px');
						 	ajaxindicatorstop();
						 }else if(obj.msg=='Already Applied'){
						 	toastr["error"](obj.msg).css('width','400px');
						 }else{
						 	setTimeout(function(){
						  			 ajaxindicatorstop();
						 
						 	toastr["info"](obj.msg).css('width','400px');
						 		 toastr["success"]('Applied Succesfully');
								location.reload();
							}, 4000);
						 }
					
		
						  
					 }
				 });
				 
			// } else {
			// 	//txt = "You pressed Cancel!";
			// }
		 
	 }

	 function verify(id)
	 {
	 	//alert(id);
	 	 var email="<?php echo trim($userdata['email']);?>";
	 	 var name="<?php echo trim($userdata['name']);?>";
	 	 var dataString={id:id,email:email,name:name};
	 	$.ajax({
					 type:'POST',
					 url:'<?php echo base_url(); ?>student/email_verify',
					 data:dataString,
					 success:function(res)
					 {
					 	var obj = JSON.parse(res);
					 	console.log(obj);
					 	 if(obj.msg=='Mail send on your register email Please check your email')
						  { 
							 //toastr["error"]('Please verify email &nbsp;&nbsp;'+'Click here to <a href="<?php echo base_url() ?>login/verify/<?php echo md5($userdata['name']); ?>/<?php echo base64_encode($userdata['id']); ?>" target="_blank">Verify</a>');
							  toastr["info"](obj.msg).css("width","500px");
						 }
						
		// 				  		setTimeout(function(){
		// 	location.reload();
		// }, 4000);
						  
					 }
				 });
				 
	 }
</script>