
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
    <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
    <title>Hello Admission</title>

    <!-- Favicons-->
    <link rel="icon" href="<?php echo base_url('assets/images/favicon/favicon-32x32.png');?>" sizes="32x32">
    <!-- Favicons-->
    <link rel="apple-touch-icon-precomposed" href="images/favicon/apple-touch-icon-152x152.png">
    <!-- For iPhone -->
    <meta name="msapplication-TileColor" content="#00bcd4">
    <meta name="msapplication-TileImage" content="<?php echo base_url('assets/images/favicon/mstile-144x144.png');?>">
    <!-- For Windows Phone -->
	
	<link href="<?php echo base_url('assets/css/materialize.min.css');?>" type="text/css" rel="stylesheet" media="screen,projection">
	<link href="<?php echo base_url('assets/css/style.min.css');?>" type="text/css" rel="stylesheet" media="screen,projection">
    <!-- Custome CSS-->    
    <link href="<?php echo base_url('assets/css/custom/custom.min.css');?>" type="text/css" rel="stylesheet" media="screen,projection">
	
    <link href="<?php echo base_url('assets/css/custom.css');?>" type="text/css" rel="stylesheet" media="screen,projection">

    <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
    <link href="<?php echo base_url('assets/js/plugins/perfect-scrollbar/perfect-scrollbar.css');?>" type="text/css" rel="stylesheet');?>" media="screen,projection">
    <link href="<?php echo base_url('assets/js/plugins/jvectormap/jquery-jvectormap.css');?>" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="<?php echo base_url('assets/js/plugins/chartist-js/chartist.min.css');?>" type="text/css" rel="stylesheet" media="screen,projection">
	
	
	   <!--  favicon -->
        <link rel="shortcut icon" href="<?php echo base_url('assets_new/img/ico/favicon.png' ); ?>">
        <!--  apple-touch-icon -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url('assets/img/ico/apple-touch-icon-144-precomposed.png'); ?>">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/img/ico/apple-touch-icon-114-precomposed.png'); ?>">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/img/ico/apple-touch-icon-72-precomposed.png'); ?>">
        <link rel="apple-touch-icon-precomposed" href="assets/img/ico/apple-touch-icon-57-precomposed.png'); ?>">

        <link href='https://fonts.googleapis.com/css?family=Raleway:400,300,500,700,900' rel='stylesheet' type='text/css'>
        <!-- FontAwesome CSS -->
        <link href="<?php echo base_url('assets_new/fonts/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet">
        <!-- Material Icons CSS -->
        <link href="<?php echo base_url('assets_new/fonts/iconfont/material-icons.css'); ?>" rel="stylesheet">
        <!-- magnific-popup -->
        <link href="<?php echo base_url('assets_new/magnific-popup/magnific-popup.css'); ?>" rel="stylesheet">
        <!-- owl.carousel -->
        <link href="<?php echo base_url('assets_new/owl.carousel/assets/owl.carousel.css'); ?>" rel="stylesheet">
        
        <link href="<?php echo base_url('assets_new/owl.carousel/assets/owl.theme.default.min.css'); ?>" rel="stylesheet">
        <!-- flexslider -->
        <link href="<?php echo base_url('assets_new/flexSlider/flexslider.css'); ?>" rel="stylesheet">
        <!-- materialize -->
        <link href="<?php echo base_url('assets_new/materialize/css/materialize.min.css'); ?>" rel="stylesheet">
        <!-- Bootstrap -->
        <link href="<?php echo base_url('assets_new/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
        <!-- shortcodes -->
        <link href="<?php echo base_url('assets_new/css/shortcodes/shortcodes.css'); ?>" rel="stylesheet">
        <!-- Style CSS -->
        <link href="<?php echo base_url('assets_new/style.css') ?>" rel="stylesheet">

        <!-- RS5.0 Main Stylesheet -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets_new/revolution/css/settings.css'); ?>">
        <!-- RS5.0 Layers and Navigation Styles -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets_new/revolution/css/layers.css'); ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets_new/revolution/css/navigation.css'); ?>">
</head>
<style>
input[type="submit"] {
    padding-top: 8px !important;
}
</style>
<body>
    <!-- Start Page Loading -->
    <div id="loader-wrapper">
        <div id="loader"></div>        
        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
    </div>
    <!-- End Page Loading -->
    <!-- START HEADER -->
    <header id="header" class="page-topbar1">
        <div class="navbar-fixed">
            <nav class="navbar-color" >
                <div class="nav-wrapper">
                    <ul class="left">                      
                      <li><h1 class="logo-wrapper"><a href="<?php echo base_url(); ?>" class="LogoMargin"><span class="red-text">Hello</span><span class="black-text">Admission</span></a></h1></li>
					  
                    </ul>                   
                    <ul class="right right-nav"> 
						<li>	
							<a onclick="FBLogin();" class="btn btn-large waves-effect waves-light blue">Facebook</a>&nbsp;&nbsp;					
						</li>
						<li>
							<a onclick="GLogin();" class="btn btn-large waves-effect waves-light red">Google</a>&nbsp;&nbsp;
						</li>
						<li>	
							<a href="<?php echo base_url('explore/index'); ?>" class="black-text">Explore</a>&nbsp;&nbsp;
						</li>
						<?php if(!$this->session->userdata('logged_in')){ ?>
						<li>	
						
							<a href="<?php echo base_url('login/index'); ?>" class="black-text">Login</a>&nbsp;&nbsp;
						</li>
						<li>	
							<a href="<?php echo base_url('login/student_register'); ?>" class="black-text">SignUp</a>&nbsp;&nbsp;
						</li>
						<?php }else{ 
						 $userdata = $this->session->userdata('logged_in');
						
						?>
						<li>
							<?php if($userdata['role'] == 2){ ?>
							<a href="<?php echo base_url('dashboard/student'); ?>" class="black-text"><?php echo $userdata['name']; ?></a>&nbsp;&nbsp;
							<?php }else if($userdata['role'] == 1){ ?>
							<a href="<?php echo base_url('dashboard/au'); ?>" class="black-text"><?php echo $userdata['name']; ?></a>&nbsp;&nbsp;
							<?php } ?>
						</li>
						<li>	
							<a href="<?php echo base_url('login/logout'); ?>" class="black-text">Logout</a>&nbsp;&nbsp;
						</li>
						<?php } ?>
						<li>	
							<a href="<?php echo base_url('login/academic_register'); ?>" class="black-text">College Signup</a>&nbsp;&nbsp;
						</li>
						
					</ul>                    
                </div>
            </nav>
        </div>
    </header>
    <!-- END HEADER -->
