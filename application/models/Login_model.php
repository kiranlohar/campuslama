<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model {

	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->tableName = 'users';
		$this->primaryKey = 'id';
	}

	public function checkUser($data = array()){
		$this->db->select($this->primaryKey);
		$this->db->from($this->tableName);
		$this->db->where(array('oauth_provider'=>$data['oauth_provider'],'oauth_uid'=>$data['oauth_uid']));
		$prevQuery = $this->db->get();
		$prevCheck = $prevQuery->num_rows();
		
		if($prevCheck > 0){
			$prevResult = $prevQuery->row_array();
			$data['updated_at'] = date("Y-m-d H:i:s");
			$update = $this->db->update($this->tableName,$data,array('id'=>$prevResult['id']));
			$userID = $prevResult['id'];
		}else{
			$data['created_at'] = date("Y-m-d H:i:s");
			$data['updated_at'] = date("Y-m-d H:i:s");
			$insert = $this->db->insert($this->tableName,$data);
			$userID = $this->db->insert_id();
		}

		return $userID?$userID:FALSE;
    }
    public function get_fb_user($userID){
	$query=$this->db->where('id',$userID)->get('users');
	return $query->result();
}

	public function insertUser($data){
		$this->db->insert('users',$data);
		return $this->db->insert_id();
	}
	
	public function insertStudentMstr($smdata){
		$this->db->insert('student_mstr',$smdata);
		return $this->db->insert_id();
	}
	
	
	public function updateUser($data,$userId){
		
		$this->db->where('id',$userId);
		$this->db->update('users',$data);
		return true;
	}
	
	public function userPresent($email){
		$query = $this->db->get_where('users',array('email'=>$email));
		return $query->row();
	}
	
	public function LoginPresent_admin($email,$password){
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where('email', $email);
		//$this->db->where('name', $email);
		$this->db->where('password', $password);
		//$this->db->where('IsActive', 1);
		$query = $this->db->get();
		return $query->row();
	}

	public function LoginPresent($email,$password){
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where('email', $email);
		//$this->db->where('name', $email);
		//$this->db->where('IsActive', 1);
		$this->db->where('password', $password);
		$query = $this->db->get();
		return $query->row();
	}

	public function Login_fb($email,$oauth_uid){
		//print_r(expression)
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where('email', $email);
		//$this->db->where('name', $email);
		//$this->db->where('IsActive', 1);
		$this->db->where('oauth_uid', $oauth_uid);
		$query = $this->db->get();
		return $query->row();
	}
	
	public function LoginPresent_student($email,$password){
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where('email', $email);
		$this->db->where('role', 2);
		//$this->db->where('IsActive', 1);
		$this->db->where('password', $password);
		$query = $this->db->get();
		return $query->row();
	}
	function verifyEmailID($key)
    {
        $data = array('status' => 1);
        $this->db->where('md5(email)', $key);
        return $this->db->update('user', $data);
    }
	
	function check_email($email)
    {
        //$data = array('email' => $email);
        //$this->db->where('md5(email)', $key);
        $check=$this->db->where('email', $email)->get('users');
		if($check->num_rows() > 0)
		{
			return $check->result();
			return true;
		}else
		{
			return false;
		}
    }
}
