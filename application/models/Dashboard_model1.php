<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	public function getUserApplications($stmId){
		
		$query = $this->db->query('SELECT * FROM student_applications as sp JOIN uni_courses AS uc ON uc.uc_id = sp.sa_uc_id JOIN uni_mstr AS um ON uc.uc_um_id = um.um_id JOIN uni_info AS ui ON uc.uc_um_id = ui.ui_um_id JOIN course_mstr AS cm ON uc.uc_com_id = cm.com_id WHERE sp.sa_stm_id ='.$stmId);
		return $query->result();
	}
	
	public function getUserShortlist($stmId){
		
		$query = $this->db->query('SELECT * FROM shortlisted_courses as sc JOIN uni_courses AS uc ON uc.uc_id = sc.sc_uc_id JOIN uni_mstr AS um ON uc.uc_um_id = um.um_id JOIN uni_info AS ui ON uc.uc_um_id = ui.ui_um_id JOIN course_mstr AS cm ON uc.uc_com_id = cm.com_id WHERE sc.sc_stm_id ='.$stmId);
		return $query->result();
	}
}
