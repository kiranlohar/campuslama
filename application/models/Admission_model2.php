<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admission_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	public function getCategories(){
		$query = $this->db->get('field_categories');
		return $query->result();
	}
	
	public function getCatFeilds($fm_id=''){
		
		if($fm_id != ''){
			$query = $this->db->get_where('field_mstr',array('fm_id'=>$fm_id));
			return $query->row();
		}
		$query = $this->db->get('field_mstr');
		return $query->result();
	}
	
	public function getFormRadioOption($fr_fm_id){
		
		$query = $this->db->get_where('field_radios',array('fr_fm_id'=>$fr_fm_id));
			
		return $query->result();
	}
	
	public function getCourseByName($crsName){
		$query = $this->db->get_where('course_mstr',array('com_name'=>$crsName));
		return $query->row();
	}
	
	public function getProgramMasterData($crsId){
		$query = $this->db->get_where('uni_program_mstr',array('upm_com_id_program'=>$crsId));
		return $query->row();
	}
	
	public function getAppUpdatedStatus($as_id){
		//$query = $this->db->query('SELECT uc.*,cm.*,sa.*,sm.*,aps.*,usr.* FROM application_status as aps JOIN student_mstr as sm ON sm.stm_id = aps.as_stm_id JOIN student_applications as sa ON sa.sa_id = aps.as_sa_id JOIN uni_courses as uc ON uc.uc_id = sa.sa_uc_id JOIN master_specialization as cm ON cm.id = uc.uc_com_id JOIN users as usr ON usr.id = sm.stm_user_id WHERE aps.as_id ='.$as_id.' AND aps.as_app_type = "Apply"');
		$query = $this->db->query('SELECT uc.*, cm.*, sa.*, sm.*, aps.*, usr.*, ms.* FROM application_status AS aps JOIN student_mstr AS sm ON sm.stm_id = aps.as_stm_id JOIN student_applications AS sa ON sa.sa_id = aps.as_sa_id JOIN uni_courses AS uc ON uc.uc_id = sa.sa_uc_id JOIN master_program AS cm ON cm.id= uc.uc_parent JOIN master_specialization AS ms ON ms.id = uc.uc_com_id JOIN users AS usr ON usr.id = sm.stm_user_id WHERE aps.as_id = "'.$as_id.'" AND aps.as_app_type = "Apply"');
		//echo $this->db->last_query();
		return $query->row();
	}
	
	public function getUComByParent($com_id,$id){
		// $query = $this->db->query('SELECT uc.*,cm.name as spe,sa.*,sm.*,aps.*,usr.* FROM uni_courses as uc JOIN student_applications as sa ON sa.sa_uc_id = uc.uc_id JOIN application_status as aps ON aps.as_sa_id = sa.sa_id JOIN master_program as cm ON cm.id = uc.uc_com_id JOIN student_mstr as sm ON sm.stm_id = sa.sa_stm_id JOIN users as usr ON usr.id = sm.stm_user_id WHERE uc.uc_parent ='.$com_id.' AND aps.as_app_type = "Apply"');
		//echo $this->db->last_query();
		// return $query->result();
		//$query = $this->db->query('SELECT uc.*,cm.*,sa.*,sm.*,aps.*,usr.* FROM uni_courses as uc JOIN student_applications as sa ON sa.sa_uc_id = uc.uc_id JOIN application_status as aps ON aps.as_sa_id = sa.sa_id JOIN course_mstr as cm ON cm.com_id = uc.uc_com_id JOIN student_mstr as sm ON sm.stm_id = sa.sa_stm_id JOIN users as usr ON usr.id = sm.stm_user_id WHERE uc.uc_parent ='.$com_id.' AND aps.as_app_type = "Apply"');
		$query = $this->db->query("SELECT
			  uc.*,
			  cm.*,
			  sa.*,
			  sm.*,
			  aps.*,
			  usr.*,
			  ms.name as spe,
			  um.*
			FROM
			  uni_courses AS uc
			JOIN
			  student_applications AS sa ON sa.sa_uc_id = uc.uc_id
			JOIN
			  application_status AS aps ON aps.as_sa_id = sa.sa_id
			JOIN
			  master_program AS cm ON cm.id = uc.uc_parent
			JOIN
			  master_specialization AS ms ON ms.id = uc.uc_com_id
			JOIN
			  student_mstr AS sm ON sm.stm_id = sa.sa_stm_id
			JOIN
			  users AS usr ON usr.id = sm.stm_user_id
			JOIN
			  uni_mstr AS um ON um.um_id = uc.uc_um_id
			WHERE
			  uc.uc_parent = '".$com_id."' AND aps.as_app_type = 'Apply' AND um.um_user_id='".$id."'");
		//echo $this->db->last_query();
		return $query->result();

	}
	
	public function insertFormMaster($ufmdata){
		$this->db->insert('uni_form_mstr',$ufmdata);
		return $this->db->insert_id();
	}
	
	public function insertFormFields($uffdata){
		$this->db->insert('uni_form_fields',$uffdata);
		return $this->db->insert_id();
	}
	
	public function updateApplicationStatus($as_id,$sudata){
		
		$this->db->where('as_id',$as_id);
		$this->db->update('application_status',$sudata);
		return true;
	}
	
}
