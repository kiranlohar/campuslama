<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Listing_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	
	public function getMasterCourse($coursname){
		
		$query = $this->db->get_where('master_program',array('name'=>$coursname));
		
		return $query->row();
	}
	public function getMasterCourse_spe($coursname){
		
		$query = $this->db->get_where('master_specialization',array('name'=>$coursname));
		
		return $query->row();
	}
	/* public function getUniversityCourses($crsId){
		$this->db->select('*');
		$this->db->from('uni_courses');
		$this->db->join('course_mstr', 'course_mstr.com_id = uni_courses.uc_com_id', 'LEFT');
		$this->db->join('uni_mstr', 'uni_mstr.um_id = uni_courses.uc_um_id', 'LEFT');
		$this->db->where('uni_courses.uc_is_approved',1);
		$this->db->where('uni_courses.uc_com_id',$crsId);
		$this->db->or_where('uni_courses.uc_parent',$crsId);
		$query = $this->db->get();
		return $query->result();
	} */
	public function getUniversityCourses($crsId){
		$this->db->select('*,master_program.*,master_specialization.name as spe,master_type.name as type,master_course_specialization.name as course_spe');
		$this->db->from('uni_courses');
		$this->db->join('master_program', 'master_program.id = uni_courses.uc_parent');
		$this->db->join('master_specialization', 'master_specialization.id = uni_courses.uc_com_id');
		$this->db->join('master_course_specialization ', 'master_course_specialization.id = uni_courses.uc_spe_id','left');
		$this->db->join('uni_mstr', 'uni_mstr.um_id = uni_courses.uc_um_id');
		$this->db->join('master_type', 'master_type.id = uni_courses.uc_course_type');
		$this->db->where('uni_courses.uc_is_approved',1);
		$this->db->where('uni_courses.uc_com_id',$crsId);
		$this->db->or_where('uni_courses.uc_parent',$crsId);
		
		$query = $this->db->get();
		//echo $this->db->last_query();
		return $query->result();

		
	}
	public function getUniversityCourses_visitory($crsId,$visitor)
	{
		$this->db->select('*,master_program.*,master_specialization.name as spe,master_type.name as type,master_course_specialization.name as course_spe');
		$this->db->from('uni_courses');
		$this->db->join('master_program', 'master_program.id = uni_courses.uc_parent');
		$this->db->join('master_specialization', 'master_specialization.id = uni_courses.uc_com_id');
		$this->db->join('master_course_specialization ', 'master_course_specialization.id = uni_courses.uc_spe_id','left');
		$this->db->join('uni_mstr', 'uni_mstr.um_id = uni_courses.uc_um_id');
		$this->db->join('master_type', 'master_type.id = uni_courses.uc_course_type');
		if($visitor['cate_id']!=0){
		$this->db->where_in('uni_courses.uc_course_type',$visitor['type']);
		$this->db->where_in('uni_courses.uc_eligibility',$visitor['cate_id']);
	}
		$this->db->where('uni_courses.uc_parent',$crsId);
		$this->db->where('uni_courses.uc_is_approved',1);
		$query = $this->db->get();
	//echo $this->db->last_query();
		return $query->result();
	}
	
	public function getUniversityCourses_filter($crsId,$sid){
		$this->db->select('*,master_program.*,master_specialization.name as spe,master_type.name as type,master_course_specialization.name as course_spe');
		$this->db->from('uni_courses');
		$this->db->join('master_program', 'master_program.id = uni_courses.uc_parent');
		$this->db->join('master_specialization', 'master_specialization.id = uni_courses.uc_com_id');
		$this->db->join('master_course_specialization ', 'master_course_specialization.id = uni_courses.uc_spe_id','left');
		$this->db->join('uni_mstr', 'uni_mstr.um_id = uni_courses.uc_um_id');
		$this->db->join('master_type', 'master_type.id = uni_courses.uc_course_type');
		$this->db->where('uni_courses.uc_is_approved',1);
		
		$this->db->where_in('uni_courses.uc_com_id',$sid);
		//$this->db->where_in('master_specialization.id',$sid);
	
		//$this->db->where('uni_courses.uc_com_id',$crsId);
		/* if($sid!=""){
		$this->db->where_in('uni_courses.uc_com_id',$sid);
		} */
		$this->db->where_in('uni_courses.uc_parent',$crsId);
		
		$query = $this->db->get();
		//echo $this->db->last_query();
		return $query->result();
	}
	public function get_cource_details($sid){
		//echo $sid;
		$this->db->select('*,master_program.*,master_specialization.name as spe,master_type.name as type,master_Eligibility.name as eli,master_course_specialization.name as course_spe');
		$this->db->from('uni_courses');
		$this->db->join('master_program', 'master_program.id = uni_courses.uc_parent');
		$this->db->join('master_specialization', 'master_specialization.id = uni_courses.uc_com_id');
		$this->db->join('master_course_specialization ', 'master_course_specialization.id = uni_courses.uc_spe_id','left');
		$this->db->join('uni_mstr', 'uni_mstr.um_id = uni_courses.uc_um_id');
		$this->db->join('master_type', 'master_type.id = uni_courses.uc_course_type');
		$this->db->join('master_Eligibility', 'master_Eligibility.id = uni_courses.uc_eligibility');
		$this->db->where('uni_courses.uc_is_approved',1);
		$this->db->where_in('uni_courses.uc_id',$sid);
		// $query=$this->db->where('uc_id',$sid)->get('uni_courses');
		$query = $this->db->get();
		 return $query->result();
	}
	public function getCourseMainMaster($compparent=''){
	
		if($compparent != ''){
			$query = $this->db->where_in('program_id',$compparent)->get('master_specialization');
	
		}else{
			
			$query = $this->db->get_where('master_specialization',array('status'=>1));
		}
		
		return $query->result();
	}

	public function get_similar_coruse($uc_um_id){
		//echo $sid;
		$this->db->select('*,master_program.*,master_specialization.name as spe,master_type.name as type,master_Eligibility.name as eli,master_course_specialization.name as course_spe');
		$this->db->from('uni_courses');
		$this->db->join('master_program', 'master_program.id = uni_courses.uc_parent');
		$this->db->join('master_specialization', 'master_specialization.id = uni_courses.uc_com_id');
		$this->db->join('master_course_specialization ', 'master_course_specialization.id = uni_courses.uc_spe_id','left');
		$this->db->join('uni_mstr', 'uni_mstr.um_id = uni_courses.uc_um_id');
		$this->db->join('master_type', 'master_type.id = uni_courses.uc_course_type');
		$this->db->join('master_Eligibility', 'master_Eligibility.id = uni_courses.uc_eligibility');
		$this->db->where('uni_courses.uc_is_approved',1);
		//$this->db->where_in('uni_courses.uc_id',$sid);
		$this->db->where_in('uni_courses.uc_um_id',$uc_um_id);
		$this->db->order_by('uni_courses.uc_um_id','desc');
		$this->db->limit(5);
		$query = $this->db->get();
		 return $query->result();
	}
}
