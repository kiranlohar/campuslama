<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Student_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	public function getUniversityByUser($userId){
		
		$this->db->select('uni_mstr.*,uni_info.*,users.*');
		$this->db->from('uni_mstr');
		$this->db->join('uni_info', 'uni_mstr.um_id = uni_info.ui_um_id', 'INNER');
		$this->db->join('users', 'uni_mstr.um_user_id = users.id', 'INNER');
		$this->db->where('uni_mstr.um_user_id',$userId);
		$query = $this->db->get();
		return $query->row();
	}
	
	public function getStudentMasterById($userId){
		//print_r($userId);die;
		$query = $this->db->get_where('student_mstr',array('stm_user_id'=>$userId));

		return $query->row();
	}
	
	public function getCourseByComId($comid){
		$query = $this->db->get_where('master_program',array('id'=>$comid));
		return $query->row();
	}
	
	public function getAppStatus($said){
		
		$query = $this->db->get_where('application_status',array('as_sa_id'=>$said,'as_app_type'=>'Apply'));
		return $query->row();
	}
	
	public function getShortStatus($said){
		$query = $this->db->get_where('application_status',array('as_sa_id'=>$said,'as_app_type'=>'Shortlist'));
		return $query->row();
	}
	
	public function getPersonalDetails($userId){
		$query = $this->db->get_where('student_personal_details',array('spd_stm_id'=>$userId));
		return $query->row();
	}
	
	public function getEducationalDetails($userId){
		$query = $this->db->get_where('student_education_details',array('sed_stm_id'=>$userId));
		return $query->row();
	}
	
	public function getWorkDetails($userId){
		$query = $this->db->get_where('student_work_details',array('swd_stm_id'=>$userId));
		return $query->row();
	}
	
	public function getStateByCountry($countryId){
		$query = $this->db->get_where('states_mstr',array('sm_cm_id'=>$countryId));
		return $query->result();
	}
	
	public function getCityByState($stateId){
		$query = $this->db->get_where('cities_mstr',array('cim_sm_id'=>$stateId));
		return $query->result();
	}
	
	public function getCourseName(){
		$query = $this->db->get('course_mstr');
		return $query->result();
	}
	
	public function getPackages(){
		$this->db->select('*');
		$this->db->from('package_mstr');
		$query = $this->db->get();
		return $query->result();
	}
	
	public function insertPersonalData($sperdata){
		$this->db->insert('student_personal_details',$sperdata);
		return $this->db->insert_id();
	}
	
	public function insertAppliedCourse($appdata){
		$this->db->insert('student_applications',$appdata);
		return $this->db->insert_id();
	}
	
	public function insertShortlistCourse($sldata){
		$this->db->insert('shortlisted_courses',$sldata);
		return $this->db->insert_id();
	}
	
	public function insertAppliedStatus($asdata){
		$this->db->insert('application_status',$asdata);
		return $this->db->insert_id();
	}
	
	public function insertShortlistStatus($ssdata){
		$this->db->insert('application_status',$ssdata);
		return $this->db->insert_id();
	}
	public function check_shorlist($userid,$cid)
	{
		$query = $this->db->get_where('shortlisted_courses',array('sc_stm_id'=>$userid,'sc_uc_id'=>$cid));
			if($query->num_rows() > 0)
			{ 
			  return true;
			}
			else{
			  return false;
			}

	}
	public function check_applied($userid,$cid)
	{
		$query = $this->db->get_where('student_applications',array('sa_stm_id'=>$userid,'sa_uc_id'=>$cid));
			if($query->num_rows() > 0)
			{ 
			  return true;
			}
			else{
			  return false;
			}

	}
	public function updatePersonalData($sperdata,$spd_id){
		$this->db->where('spd_id',$spd_id);
		$this->db->update('student_personal_details',$sperdata);
		return true;
	}
	
	public function updatePersonalData_exp($sperdata,$spd_id){
		$this->db->where('spd_stm_id',$spd_id);
		$this->db->update('student_personal_details',$sperdata);
		return true;
	}
	
	public function insertEducationalData($sedudata){
		$this->db->insert('student_education_details',$sedudata);
		return $this->db->insert_id();
	}
	
	public function updateEducationalData($sedudata,$sed_id){
		$this->db->where('sed_id',$sed_id);
		$this->db->update('student_education_details',$sedudata);
		return true;
	}
	
	public function insertWorkData($sworkdata){
		$this->db->insert('student_work_details',$sworkdata);
		return $this->db->insert_id();
	}
	
	public function updateWorkData($sworkdata,$swd_id){
		$this->db->where('swd_id',$swd_id);
		$this->db->update('student_work_details',$sworkdata);
		return true;
	}
	
	public function updateUPM($upmdata,$upmid){
		$this->db->where('upm_id',$upmid);
		$this->db->update('uni_program_mstr',$upmdata);
		return true;
	}

	public function insert_testimonial($data){

	return $this->db->insert('student_testimonial',$data);

	}

	public function update_testimonials($id,$testi){

	return $this->db->where('id',$id)->update('student_testimonial',$testi);

	}

	public function get_testimonial_list($id){
		$condition=array('student_id'=>$id,'status'=>1);
	$query= $this->db->where($condition)->get('student_testimonial');
	return $query->result();
	}
	public function get_t_data($id){

	$query= $this->db->where('id',$id)->get('student_testimonial');
	return $query->result();
	}

	public function active_testimonials($id,$state){

	return $this->db->where('id',$id)->update('student_testimonial',$state);

	}
	
}
