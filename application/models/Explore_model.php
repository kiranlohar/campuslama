<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Explore_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	public function insertStudentField($fielddata){
		$this->db->insert('user_field_datas',$fielddata);
		return $this->db->insert_id();
	}
	
	public function insertStudentField_update($id,$k,$stformdata){
		//print_r($stformdata);
		$arr=array('ufd_stm_id'=>$id,'ufd_fm_id'=>$k);
	$query=	$this->db->where($arr)->update('user_field_datas',$stformdata);
	return $query;
		//echo $this->db->last_query();
	}

	public function check_id($id){
		$query=$this->db->where('ufd_stm_id',$id)->get('user_field_datas');
		if($query->num_rows() > 0){
			return true;
		}else
		{
			return false;
		}
	}
	public function getCourseMaster(){
		
		$query = $this->db->get('master_program');
		
		return $query->result();
	}
	public function get_top_university(){
		
		$this->db->select('st.sa_uc_id,uc.uc_um_id,uf.ui_coverimage,um.um_name');
	
		$this->db->from('student_applications as st');
		$this->db->join('uni_courses as uc', 'uc.uc_id = st.sa_uc_id','');
		$this->db->join('uni_mstr as um', 'um.um_id = uc.uc_um_id','');
		$this->db->join('uni_info as uf', 'uf.ui_um_id = um.um_id','');
		$this->db->distinct('um.um_name');
		$this->db->group_by('st.sa_uc_id');
		$this->db->limit(8);
		
		//$this->db->join('uni_program_admins ', 'uni_program_admins.upa_id = uni_courses.uc_upa_id');
		$query = $this->db->get();
		return $query->result_array();
	}

	public function getUniversityCourses_top($id)
	{
		$this->db->select('*,master_program.*,master_specialization.name as spe,master_type.name as type,master_course_specialization.name as course_spe');
		$this->db->from('uni_courses');
		$this->db->join('master_program', 'master_program.id = uni_courses.uc_parent');
		$this->db->join('master_specialization', 'master_specialization.id = uni_courses.uc_com_id');
		$this->db->join('master_course_specialization ', 'master_course_specialization.id = uni_courses.uc_spe_id','left');
		$this->db->join('uni_mstr', 'uni_mstr.um_id = uni_courses.uc_um_id');
		$this->db->join('master_type', 'master_type.id = uni_courses.uc_course_type');
		if($id!=0){
	//	$this->db->where_in('uni_courses.uc_course_type',$visitor['type']);
		$this->db->where_in('uni_courses.uc_eligibility',$id);
		}
		$this->db->group_by('uni_courses.uc_id');
		$this->db->order_by('uni_courses.uc_id','desc');
		$this->db->limit(3);
		$this->db->where('uni_courses.uc_is_approved',1);
		$query = $this->db->get();
	//echo $this->db->last_query();
		return $query->result();
	}

	public function get_testimonial(){
		
		$this->db->select('st.testimonial,us.name');
	
		$this->db->from('student_testimonial as st');
		$this->db->join('users as us', 'us.id = st.student_id','');
		$this->db->group_by('st.student_id');
		$this->db->order_by('st.id','DESC');
		$this->db->limit(3);
		
		//$this->db->join('uni_program_admins ', 'uni_program_admins.upa_id = uni_courses.uc_upa_id');
		$query = $this->db->get();
		return $query->result();
	}

	public function getUniversity_city_top($id)
	{
		$this->db->select('*,master_program.*,master_specialization.name as spe,master_type.name as type,master_course_specialization.name as course_spe,uni_info.*,cities_mstr.cim_name');
		$this->db->from('uni_courses');
		$this->db->join('master_program', 'master_program.id = uni_courses.uc_parent');
		$this->db->join('master_specialization', 'master_specialization.id = uni_courses.uc_com_id');
		$this->db->join('master_course_specialization ', 'master_course_specialization.id = uni_courses.uc_spe_id','left');
		$this->db->join('uni_mstr', 'uni_mstr.um_id = uni_courses.uc_um_id','left');
		$this->db->join('uni_info', 'uni_info.ui_id = uni_courses.uc_um_id','left');
		$this->db->join('cities_mstr', 'cities_mstr.cim_id = uni_info.city_id ','left');
		$this->db->join('master_type', 'master_type.id = uni_courses.uc_course_type');
		$this->db->join('student_applications', 'student_applications.sa_uc_id = uni_courses.uc_id');
		if($id!=0){
	//	$this->db->where_in('uni_courses.uc_course_type',$visitor['type']);
		$this->db->where_in('uni_info.city_id',$id);
		}
		$this->db->group_by('uni_courses.uc_um_id');
		$this->db->order_by('uni_courses.uc_id','DESC');
		$this->db->limit(3);
		$this->db->where('uni_courses.uc_is_approved',1);
		$query = $this->db->get();
	//echo $this->db->last_query();
		return $query->result();
	}
	public function getUniversity_city_top_view($id)
	{
		$this->db->select('uni_info.*,uni_mstr.*,cities_mstr.cim_name');
		$this->db->from('uni_mstr');
		$this->db->join('uni_info', 'uni_info.ui_um_id = uni_mstr.um_id','left');
		$this->db->join('cities_mstr', 'cities_mstr.cim_id = uni_info.city_id ','left');
	
		if($id!=0){
	//	$this->db->where_in('uni_courses.uc_course_type',$visitor['type']);
		$this->db->where_in('uni_info.city_id',$id);
		}

		$query = $this->db->get();
	//echo $this->db->last_query();
		return $query->result();
	}
	public function get_top_cities(){
			$query=$this->db->order_by('id','DESC')->get('top_cities');
			return $query->result();
		}

		public function getUniversity_city_top_5()
	{
		$this->db->select('*,master_program.*,master_specialization.name as spe,master_type.name as type,master_course_specialization.name as course_spe,uni_info.*,cities_mstr.cim_name');
		$this->db->from('uni_courses');
		$this->db->join('master_program', 'master_program.id = uni_courses.uc_parent');
		$this->db->join('master_specialization', 'master_specialization.id = uni_courses.uc_com_id');
		$this->db->join('master_course_specialization ', 'master_course_specialization.id = uni_courses.uc_spe_id','left');
		$this->db->join('uni_mstr', 'uni_mstr.um_id = uni_courses.uc_um_id','left');
		$this->db->join('uni_info', 'uni_info.ui_id = uni_courses.uc_um_id','left');
		$this->db->join('cities_mstr', 'cities_mstr.cim_id = uni_info.city_id ','left');
		$this->db->join('master_type', 'master_type.id = uni_courses.uc_course_type');
		$this->db->join('student_applications', 'student_applications.sa_uc_id = uni_courses.uc_id');
		$this->db->group_by('uni_courses.uc_um_id');
		$this->db->order_by('uni_courses.uc_id','ASC');
		//$this->db->limit(3);
		$this->db->where('uni_courses.uc_is_approved',1);
		$query = $this->db->get();
	//echo $this->db->last_query();
		return $query->result();
	}
}
