<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class University_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	public function getUniversityByName($uname){
		$this->db->select('*');
		$this->db->from('uni_mstr');
		$this->db->join('uni_info', 'uni_info.ui_um_id = uni_mstr.um_id', 'LEFT');
		$this->db->where('uni_mstr.um_name',$uname);
		$query = $this->db->get();
		return $query->row();
	}
	
	public function getUniversityMedia($um_id){
		$this->db->select('*');
		$this->db->from('uni_gallery_medias');
		$this->db->where('ugm_um_id',$um_id);
		$query = $this->db->get();
		return $query->result();
	}
	
	public function getUniversityFacility($um_id){
		$this->db->select('*');
		$this->db->from('uni_facilities');
		$this->db->where('ufc_um_id',$um_id);
		$query = $this->db->get();
		return $query->result();
	}
	
	public function getUniversityCourse($um_id){
		$this->db->select('*');
		$this->db->from('uni_courses');
		$this->db->where('uc_um_id',$um_id);
		$query = $this->db->get();
		return $query->result();
	}

	public function getsemilar_college(){
		$this->db->select('*');
		$this->db->from(' uni_info');
		$this->db->join('uni_mstr', 'uni_mstr.um_id = uni_info.ui_um_id','left');
		$this->db->order_by('uni_info.ui_um_id','DESC');
		$this->db->limit(0,3);
		$query = $this->db->get();
		//echo $this->db->last_query();
		return $query->result();
	}
}
