<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	public function insertUser($data){
		$this->db->insert('users',$data);
		return $this->db->insert_id();
	}
	
	public function insertStudentMstr($smdata){
		$this->db->insert('student_mstr',$smdata);
		return $this->db->insert_id();
	}
	
	
	public function updateUser($data,$userId){
		
		$this->db->where('id',$userId);
		$this->db->update('users',$data);
		return true;
	}
	
	public function userPresent($email){
		$query = $this->db->get_where('users',array('email'=>$email));
		return $query->row();
	}
	
	public function LoginPresent($email,$password){
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where('email', $email);
		//$this->db->where('name', $email);
		$this->db->where('password', $password);
		$query = $this->db->get();
		return $query->row();
	}
	
	function verifyEmailID($key)
    {
        $data = array('status' => 1);
        $this->db->where('md5(email)', $key);
        return $this->db->update('user', $data);
    }
}
