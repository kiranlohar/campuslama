<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Explore_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	public function insertStudentField($fielddata){
		$this->db->insert('user_field_datas',$fielddata);
		return $this->db->insert_id();
	}
	
	public function getCourseMaster(){
		
		$query = $this->db->get('course_mstr');
		
		return $query->result();
	}
}
