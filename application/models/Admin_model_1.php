<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	public function getProgramsList(){
		
		$this->db->select('uc.uc_id, uc.uc_com_id, uc.uc_parent, uc.uc_course_length, uc.uc_course_type, uc.uc_is_approved, cmi.com_id, cmii.com_id, cmi.com_name as program, cmii.com_name as stream');
		$this->db->from('uni_courses as uc');
		$this->db->join('course_mstr as cmi', 'cmi.com_id = uc.uc_com_id','left');
		$this->db->join('course_mstr as cmii', 'cmii.com_id = uc.uc_parent','left');
		$query = $this->db->get();
		return $query->result();
	}
	
	public function getPackageList(){
		
		$this->db->select('pm.*, um.*');
		$this->db->from('package_mstr as pm');
		$this->db->join('uni_mstr as um', 'um.um_id = pm.pack_um_id','left');
		$query = $this->db->get();
		return $query->result();
	}
	
	public function getUniversities(){
		
		$this->db->select('*');
		$this->db->from('uni_mstr');
		$query = $this->db->get();
		return $query->result();
	}
	
	public function insertWorkData($sworkdata){
		$this->db->insert('student_work_details',$sworkdata);
		return $this->db->insert_id();
	}
	
	public function insertPackage($packdata){
		$this->db->insert('package_mstr',$packdata);
		return $this->db->insert_id();
	}
	/* Menu */
	public function insertMenu($mdata){
		$this->db->insert('menu_mstr',$mdata);
		return $this->db->insert_id();
	}
	public function updateMenu($id,$mdata){
		$this->db->where('id',$id)->update('menu_mstr',$mdata);
		return $this->db->insert_id();
	}
	
	public function editMenu($id){
		
		$this->db->select('*');
		$this->db->from('menu_mstr');
		$this->db->where('id',$id);
		$this->db->where('status',1);
		$query = $this->db->get();
		return $query->result();
	}
	public function getMasterMenu(){
		
		$this->db->select('*');
		$this->db->from('menu_mstr');
		$this->db->where('status',1);
		$query = $this->db->get();
		return $query->result();
	}
	
	public function getMasterMenu_drop($id){
		
		$this->db->select('*');
		$this->db->from('menu_mstr');
		$this->db->where('id',$id);
		//$this->db->where('status',1);
		
		$query = $this->db->get();
		return $query->result();
	}
	
	## end menu
	
	## Master page
	public function edit_master($idm,$table){
		
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where('id',$idm);
		//$this->db->where('status',1);
		$query = $this->db->get();
		return $query->result();
	}
	
	public function update_master_p($id,$mdata,$table){
		$this->db->where('id',$id)->update($table,$mdata);
		return $this->db->insert_id();
	}
	
	public function update_master_d($id,$mdata,$table){
		$this->db->where('id',$id)->update($table,$mdata);
		return $this->db->insert_id();
	}
	
	## sub menu
	    public function insertMenu_sub($mdata){
		$this->db->insert('sub_menu_mstr',$mdata);
		return true;
	}
	
	public function user_exists($mdata)
	{	
	
	$this->db->where('name', $mdata);
	$query = $this->db->get('sub_menu_mstr');
	 if ($query->num_rows() == 0) {
         return true;
     } else {
         return false;
     }
	}
	
	public function user_exists_master($mdata,$table)
	{	
	//print_r($table);
	$this->db->where('name', $mdata);
	$query = $this->db->get($table);
	 if ($query->num_rows() == 0) {
         return true;
     } else {
         return false;
     }
	}
	
	
	public function getMasterSub_Menu($id){
		
		$this->db->select('*');
		$this->db->from('sub_menu_mstr');
		//$this->db->where('status',1);
		$this->db->where('menu_id',$id);
		$query = $this->db->get();
		return $query->result();
	}
	
	public function update_sub_Menu($id,$mdata){
		$this->db->where('id',$id)->update('sub_menu_mstr',$mdata);
		return true;
	}
	
	public function edit_Sub_Menu($id){
		
		$this->db->select('*');
		$this->db->from('sub_menu_mstr');
		$this->db->where('id',$id);
		//$this->db->where('status',1);
		$query = $this->db->get();
		return $query->result();
	}
	
	public function front_menu()
	{
		$this->db->select('*');
		$this->db->from('menu_mstr');
		$this->db->where('status',1);
		$query = $this->db->get();
		return $query->result_array();
	}
	
	## end submenu
	
	## child sub menu
	   public function insertMenu_child_sub($mdata){
		$this->db->insert('child_sub_menu_mstr',$mdata);
		return $this->db->insert_id();
	}
	
	public function user_exists_ch($mdata)
	{	
	
	$this->db->where('child_sub_menu_name', $mdata);
	$query = $this->db->get('child_sub_menu_mstr');
	 if ($query->num_rows() == 0) {
         return true;
     } else {
         return false;
     }
	}
	
	public function getMaster_child_Sub_Menu($id){
		
		$this->db->select('*');
		$this->db->from('child_sub_menu_mstr');
	//	$this->db->where('status',1);
		$this->db->where('sub_menu_id',$id);
		$query = $this->db->get();
		return $query->result();
	}
	public function get_child_menu_detail($id){
		
		$this->db->select('*');
		$this->db->from('child_sub_menu_mstr');
		$this->db->where('status',1);
		$this->db->where('id',$id);
		$query = $this->db->get();
		return $query->result();
	}
	
	public function update_child_sub_Menu($id,$mdata){
		$arr=array('id'=>$id);
		$this->db->where($arr)->update('child_sub_menu_mstr',$mdata);
		return true;
	}
	public function update_child_sub_Menu_del($id,$mdata){
		$arr=array('id'=>$id);
		$this->db->where($arr)->update('child_sub_menu_mstr',$mdata);
		return true;
	}
	
	public function update_child_master($id,$mdata){
		$this->db->where('child_id',$id)->update('master_child_table',$mdata);
		return $this->db->insert_id();
	}
	
	public function updateCourseList($cldata,$uc_id){
		$this->db->where('uc_id',$uc_id);
		$this->db->update('uni_courses',$cldata);
		return true;
	}
	
	public function get_table_child($childid){
		$query=$this->db->where('child_id',$childid)->get('master_child_table');
		return $query->result();
	}
	public function get_page_detail($table){
		$query=$this->db->get($table);
		return $query->result();
	}
	
	public function edit_child_Sub_Menu($id){
		
		$this->db->select('*');
		$this->db->from('child_sub_menu_mstr');
		$this->db->where('id',$id);
		//$this->db->where('status',1);
		$query = $this->db->get();
		return $query->result();
	}
	
	
	/* public function update_child_sub_Menu($id,$mdata){
		$this->db->where('id',$id)->update('child_sub_menu_mstr',$mdata);
		return $this->db->insert_id();
	} */
	
	
	public function getMaster_sub_Menu_drop($id){
		
		$this->db->select('*');
		$this->db->from('sub_menu_mstr');
		$this->db->where('id',$id);
		//$this->db->where('status',1);
		
		$query = $this->db->get();
		return $query->result();
	}
	
}
