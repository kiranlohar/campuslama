<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Academic_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	public function getCourseMainMaster($compparent=''){
		
		if($compparent != ''){
			
			$query = $this->db->get_where('master_specialization',array('program_id'=>$compparent));
			
		}else{
			
			$query = $this->db->get_where('master_program',array('status'=>1));
		}
		
		return $query->result();
	}
	
	
	
	public function getProgManager($uc_id){
		/* $query = $this->db->query('SELECT uc.*, cm.*, upa.* FROM uni_courses as uc JOIN master_program as cm ON cm.id = uc.uc_com_id JOIN uni_program_admins as upa ON upa.upa_id = uc.uc_upa_id WHERE uc.uc_id ='.$uc_id);
		//echo $this->db->last_query(); */
		
	$this->db->select('uni_courses.*,uni_program_admins.*, master_program.name,master_program.id, master_specialization.name as spe,master_type.name as typename')
         ->from('uni_courses')
         ->join('master_program', 'uni_courses.uc_parent = master_program.id')
         //	->join('uni_program_mstr', 'uni_program_mstr.upm_com_id_stream = master_program.id')
         ->join('master_specialization', 'master_specialization.id = uni_courses.uc_com_id')
		 ->join('master_type ', 'master_type.id = uni_courses.uc_course_type')
		 ->join('uni_program_admins ', 'uni_program_admins.upa_id = uni_courses.uc_upa_id')
		  ->where('uni_courses.uc_id',$uc_id);
		  
		  
		$result = $this->db->get();
		return $result->result();
		//echo $this->db->last_query();
		//return $query->result();
		
	}
	
	public function getProjectManagerByUmId($umId){
		//echo $umId;
	//	$query = $this->db->query('SELECT uc.*, cm.* FROM uni_courses as uc JOIN master_program as cm ON cm.id = uc.uc_com_id WHERE uc.uc_um_id ='.$umId);
		


	$this->db->select('uni_courses.*, master_program.name, master_specialization.name as spe,master_type.name as typename,uni_program_admins.*')
         ->from('uni_courses')
         ->join('master_program', 'uni_courses.uc_parent = master_program.id')
         ->join('master_specialization', 'master_specialization.id = uni_courses.uc_com_id')
		 ->join('master_type ', 'master_type.id = uni_courses.uc_course_type')
		 ->join('uni_program_admins ', 'uni_program_admins.upa_id = uni_courses.uc_upa_id')
		  ->where('uni_courses.uc_um_id',$umId);
		$result = $this->db->get();
		return $result->result();
	}
	
	public function getCourseStatus($crsId){
		
		$query = $this->db->get_where('uni_courses', array('uc_com_id'=>$crsId));
		return $query->row();
	}
	
	public function getSpe(){
		
		$query = $this->db->get_where('master_specialization', array('status'=>1));
		return $query->result();
	}
	
	public function getTypeProgram($compparent=null){
		
			if($compparent != ''){
			
			$query = $this->db->get_where('master_type',array('id'=>$compparent));
			
		}else{
			
			$query = $this->db->get_where('master_type', array('status'=>1));
		}
		
		//return $query->result();
		
		return $query->result();
	}
	
	// public function getUnvCrsByComUmid($comId,$umId){
		//$query = $this->db->query('SELECT uc.*, cm.*,sa.*,sm.*,usr.*,aps.* FROM uni_courses as uc JOIN course_mstr as cm ON cm.com_id = uc.uc_com_id JOIN student_applications as sa ON sa.sa_uc_id = uc.uc_id JOIN student_mstr as sm ON sm.stm_id = sa.sa_stm_id JOIN users as usr ON usr.id = sm.stm_user_id JOIN application_status as aps ON aps.as_sa_id = sa.sa_id WHERE uc.uc_com_id ='.$comId.' AND uc.uc_um_id ='.$umId.' AND aps.as_app_type = "Apply"');
		// $query = $this->db->query("SELECT
  // uc.*,
  // cm.*,
  // sa.*,
  // sm.*,
  // usr.name as stuname,
  // aps.*,
  // ms.name as spe,
  // cm.*
// FROM
  // uni_courses AS uc
// JOIN
  // master_program AS cm ON cm.id = uc.uc_parent
// JOIN
  // master_specialization AS ms ON ms.id = uc.uc_com_id
// JOIN
  // student_applications AS sa ON sa.sa_uc_id = uc.uc_id
// JOIN
  // student_mstr AS sm ON sm.stm_id = sa.sa_stm_id
// JOIN
  // users AS usr ON usr.id = sm.stm_user_id
// JOIN
  // application_status AS aps ON aps.as_sa_id = sa.sa_id
// WHERE
  // uc.uc_com_id = '".$comId."' AND uc.uc_um_id = '".$umId."' AND aps.as_app_type ='Apply'");
		//echo $this->db->last_query();
		// return $query->result();
	// }
	
	public function getUnvCrsByComUmid($comId,$umId){
		//$query = $this->db->query('SELECT uc.*, cm.*,sa.*,sm.*,usr.*,aps.* FROM uni_courses as uc JOIN course_mstr as cm ON cm.com_id = uc.uc_com_id JOIN student_applications as sa ON sa.sa_uc_id = uc.uc_id JOIN student_mstr as sm ON sm.stm_id = sa.sa_stm_id JOIN users as usr ON usr.id = sm.stm_user_id JOIN application_status as aps ON aps.as_sa_id = sa.sa_id WHERE uc.uc_com_id ='.$comId.' AND uc.uc_um_id ='.$umId.' AND aps.as_app_type = "Apply"');
		// $query = $this->db->query("SELECT
			  // uc.*,
			  // cm.*,
			  // sa.*,
			  // sm.*,
			  // usr.*,
			  // aps.*,
			  // ms.name as spe,
			  // cm.*
			// FROM
			  // uni_courses AS uc
			// JOIN
			  // master_program AS cm ON cm.id = uc.uc_parent
			// JOIN
			  // master_specialization AS ms ON ms.id = uc.uc_com_id
			// JOIN
			  // student_applications AS sa ON sa.sa_uc_id = uc.uc_id
			// JOIN
			  // student_mstr AS sm ON sm.stm_id = sa.sa_stm_id
			// JOIN
			  // users AS usr ON usr.id = sm.stm_user_id
			// JOIN
			  // application_status AS aps ON aps.as_sa_id = sa.sa_id
			// WHERE
			  // uc.uc_com_id = '".$comId."' AND uc.uc_um_id = '".$umId."' AND aps.as_app_type ='Apply'");
		//echo $this->db->last_query();
		
		$this->db->select('uc.*,cm.*,sa.*,sm.*,usr.name stuname,aps.*,ms.name as spe,cm.*');
         $this->db->from('uni_courses AS uc');
         $this->db->join('master_program AS cm', 'cm.id = uc.uc_parent');
         $this->db->join('master_specialization AS ms', 'ms.id = uc.uc_com_id');
		 $this->db->join('student_applications AS sa', 'sa.sa_uc_id = uc.uc_id');
		 $this->db->join('student_mstr AS sm', 'sm.stm_id = sa.sa_stm_id');
		 $this->db->join('users AS usr', 'usr.id = sm.stm_user_id');
		 $this->db->join('application_status AS aps', 'aps.as_sa_id = sa.sa_id');
		 
		 $this->db->where_in('uc.uc_com_id',$comId);
		 $this->db->where('uc.uc_um_id',$umId);
		 /* if(!$status==0){
		 $this->db->where('aps.as_status',$status);
		 } */
		 $this->db->where('aps.as_app_type','Apply');
		 $query = $this->db->get();
		return $query->result();
	}
	public function getUnvCrsByComUmid_status($comId,$umId,$status){
	
		//$this->db->select('uc.*,cm.*,sa.*,sm.*,usr.*,aps.*,ms.name as spe,cm.*');
		 $this->db->select('uc.*,cm.*,sa.*,sm.*,usr.name as stuname,aps.*,ms.name as spe,cm.*');
         $this->db->from('uni_courses AS uc');
         $this->db->join('master_program AS cm', 'cm.id = uc.uc_parent');
         $this->db->join('master_specialization AS ms', 'ms.id = uc.uc_com_id');
		 $this->db->join('student_applications AS sa', 'sa.sa_uc_id = uc.uc_id');
		 $this->db->join('student_mstr AS sm', 'sm.stm_id = sa.sa_stm_id');
		 $this->db->join('users AS usr', 'usr.id = sm.stm_user_id');
		 $this->db->join('application_status AS aps', 'aps.as_sa_id = sa.sa_id');
		 $this->db->where('uc.uc_parent',$comId);
		 $this->db->where('uc.uc_um_id',$umId);
		 if(!$status==0){
		 $this->db->where('aps.as_status',$status);
		 }
		 $this->db->where('aps.as_app_type','Apply');
		 
		$result = $this->db->get();
		return $result->result();
	}
	
	public function getUniversityByUser($userId){
		$this->db->select('uni_mstr.*,uni_info.*,users.*');
		$this->db->from('uni_mstr');
		$this->db->join('uni_info', 'uni_mstr.um_id = uni_info.ui_um_id', 'INNER');
		$this->db->join('users', 'uni_mstr.um_user_id = users.id', 'INNER');
		$this->db->where('uni_mstr.um_user_id',$userId);
		$query = $this->db->get();
		return $query->row();
	}
	
	public function getCourseById($crsId){
		
		$query = $this->db->get_where('master_program',array('id'=>$crsId));
		
		return $query->row();
	}
	
	public function getSpe_uni($crsId){
		
		$query = $this->db->get_where('master_specialization', array('status'=>1,'id'=>$crsId));
		return $query->result();
	}
	
	public function getCourseByComName($com_name){
		
		$query = $this->db->get_where('master_program',array('name'=>$com_name));
		
		return $query->row();
	}
	
	public function getCourseByComParent($com_id){
		
		$query = $this->db->get_where('master_specialization',array('program_id'=>$com_id));
		
		return $query->result();
	}
	
	public function getAcademicPhoto($uId){
		
		$query = $this->db->get_where('uni_gallery_medias',array('ugm_um_id'=>$uId,'ugm_media_type'=>'PHOTO'));
		
		return $query->result();
	}
	
	public function getAcademicVideo($uId){
		
		$query = $this->db->get_where('uni_gallery_medias',array('ugm_um_id'=>$uId,'ugm_media_type'=>'VIDEO'));
		
		return $query->result();
	}
	
	public function getAcademicFacility($uId){
		
		$query = $this->db->get_where('uni_facilities',array('ufc_um_id'=>$uId));
		
		return $query->result();
	}
	
	public function getFeesMaster($feesId =''){
		
		if($feesId !=''){
			
			$query = $this->db->get_where('uni_fees_mstr',array('ufeem_id'=>$feesId));
			
			return $query->row();
		}
		
		$query = $this->db->get('uni_fees_mstr');
			
		return $query->result();
	}
	
	public function getInstallmentFees($feesId,$umId,$inst){
		
		$query = $this->db->get_where('uni_fees_mstr',array('ufeem_id'=>$feesId,'ufeem_um_id'=>$umId,'ufeem_plan_type'=>$inst));
		
		return $query->row();
	}
	
	public function getLumsumpFees($feesId,$umId,$lum){
		
		$query = $this->db->get_where('uni_fees_mstr',array('ufeem_id'=>$feesId,'ufeem_um_id'=>$umId,'ufeem_plan_type'=>$lum));
		
		return $query->row();
	}
	
	public function addUniversity($udata){
		$this->db->insert('uni_mstr',$udata);
		return $this->db->insert_id();
	}
	
	public function addUniversityInfo($uinfodata){
		$this->db->insert('uni_info',$uinfodata);
		return $this->db->insert_id();
	}
	
	public function updateUniversity($umdata,$um_id){
		$this->db->where('um_id',$um_id);
		$this->db->update('uni_mstr',$umdata);
		return true;
	}
	
	public function updateUniversityInfo($uidata,$ui_id){
		$this->db->where('ui_id',$ui_id);
		$this->db->update('uni_info',$uidata);
		return true;
	}
	
	public function addFacility($facdata){
		$this->db->insert('uni_facilities',$facdata);
		return $this->db->insert_id();
	}
	
	
	public function insertUPM($upmdata){
		$this->db->insert('uni_program_mstr',$upmdata);
		return $this->db->insert_id();
	}
	
	public function insertUPMAdmin($upmadmindata){
		$this->db->insert('uni_program_admins',$upmadmindata);
		return $this->db->insert_id();
	}
	
	public function insertUCourse($updata){
		$this->db->insert('uni_courses',$updata);
		return $this->db->insert_id();
	}
	
	public function updateUCourse($ucdata,$uc_id){
		$this->db->where('uc_id',$uc_id);
		$this->db->update('uni_courses',$ucdata);
		return true;
	}
	
	public function updateUPM($upmdata,$upmid){
		$this->db->where('upm_id',$upmid);
		$this->db->update('uni_program_mstr',$upmdata);
		return true;
	}
	
	public function updateUPMAdmin($upmadmindata,$upmadminid){
		$this->db->where('upa_id',$upmadminid);
		$this->db->update('uni_program_admins',$upmadmindata);
		return true;
	}
	
	public function insertUniversityPhoto($fudat){
		$this->db->insert('uni_gallery_medias',$fudat);
		return $this->db->insert_id();
	}
	
	public function insertFeesPlan($fpdata){
		$this->db->insert('uni_fees_mstr',$fpdata);
		return $this->db->insert_id();
	}
	
	public function updateFeesPlan($fpdata,$ufee_id){
		$this->db->where('ufeem_id',$ufee_id);
		$this->db->update('uni_fees_mstr',$fpdata);
		return true;
	}
	
	public function deleteUniversityPhoto($rmphid){
		$this->db->where('ugm_id', $rmphid);
		$this->db->delete('uni_gallery_medias');
	}
	
}
