<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	public function getUserApplications($stmId){
		
		//$query = $this->db->query('SELECT * FROM student_applications as sp JOIN uni_courses AS uc ON uc.uc_id = sp.sa_uc_id JOIN uni_mstr AS um ON uc.uc_um_id = um.um_id JOIN uni_info AS ui ON uc.uc_um_id = ui.ui_um_id JOIN course_mstr AS cm ON uc.uc_com_id = cm.com_id WHERE sp.sa_stm_id ='.$stmId);
		// $query = $this->db->query("SELECT
		// 	  *,ms.name as spe,cm.*
		// 	FROM
		// 	  student_applications AS sp
		// 	JOIN
		// 	  uni_courses AS uc ON uc.uc_id = sp.sa_uc_id
		// 	JOIN
		// 	  uni_mstr AS um ON uc.uc_um_id = um.um_id
		// 	JOIN
		// 	  uni_info AS ui ON uc.uc_um_id = ui.ui_um_id
		// 	JOIN
		// 	  master_program AS cm ON uc.uc_parent = cm.id
		// 	JOIN
		// 	  master_specialization AS ms ON uc.uc_com_id = ms.id
		// 	WHERE
		// 	  sp.sa_stm_id = '".$stmId."'");
		//echo $this->db->last_query();
			$query = $this->db->query("SELECT
			  *,ms.name as spe,cm.*,mcs.name as course_spe
			FROM
			  student_applications AS sp
			JOIN
			  uni_courses AS uc ON uc.uc_id = sp.sa_uc_id
			JOIN
			  uni_mstr AS um ON uc.uc_um_id = um.um_id
			JOIN
			  uni_info AS ui ON uc.uc_um_id = ui.ui_um_id
			JOIN
			  master_program AS cm ON uc.uc_parent = cm.id
			JOIN
			  master_specialization AS ms ON uc.uc_com_id = ms.id
			LEFT JOIN
			  master_course_specialization AS mcs ON uc.uc_spe_id = mcs.id
			WHERE
			  sp.sa_stm_id = '".$stmId."'");
		return $query->result();
	}
	
	public function getUserShortlist($stmId){
		
		//$query = $this->db->query('SELECT *,ms.name as spe,cm.* FROM shortlisted_courses AS sc JOIN uni_courses AS uc ON uc.uc_id = sc.sc_uc_id JOIN uni_mstr AS um ON uc.uc_um_id = um.um_id JOIN uni_info AS ui ON uc.uc_um_id = ui.ui_um_id JOIN master_program AS cm ON uc.uc_parent = cm.id JOIN master_specialization AS ms ON uc.uc_com_id = ms.id WHERE sc.sc_stm_id ='.$stmId);
		$query = $this->db->query('SELECT *,ms.name as spe,cm.*,mcs.name as course_spe FROM shortlisted_courses AS sc JOIN uni_courses AS uc ON uc.uc_id = sc.sc_uc_id JOIN uni_mstr AS um ON uc.uc_um_id = um.um_id JOIN uni_info AS ui ON uc.uc_um_id = ui.ui_um_id JOIN master_program AS cm ON uc.uc_parent = cm.id JOIN master_specialization AS ms ON uc.uc_com_id = ms.id LEFT JOIN master_course_specialization AS mcs ON uc.uc_spe_id = mcs.id WHERE sc.sc_stm_id ='.$stmId);
		//echo $this->db->last_query();
		return $query->result();
	}
}
