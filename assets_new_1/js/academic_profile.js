$(document).ready(function(){
	// Basic
	$('.dropify').dropify();

	// Translated
	$('.dropify-fr').dropify({
		messages: {
			default: 'Glissez-déposez un fichier ici ou cliquez',
			replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
			remove:  'Supprimer',
			error:   'Désolé, le fichier trop volumineux'
		}
	});

	// Used events
	var drEvent = $('.dropify-event').dropify();

	drEvent.on('dropify.beforeClear', function(event, element){
		return confirm("Do you really want to delete \"" + element.filename + "\" ?");
	});

	drEvent.on('dropify.afterClear', function(event, element){
		alert('File deleted');
	});
});

var facilities = [];
$(".fture").on('click',(function() {
	var featId = $(this).parent().attr('id');
	$(this).parent().css('border','1px solid green');
	$(this).parent().append('<span class="remB" id="rem-'+featId+'" style="font-size: 24px;color: red;"><i class="mdi-content-clear" style="cursor:pointer"></i></span>');
	
	var found = jQuery.inArray(featId, facilities);
	
	if (found >= 0) {
	} else {
		facilities.push(featId);
	}
	$('#facility').val(facilities);
}));

$(document).on('click', '.remB', function () {
	var rembf = $(this).attr('id');
	rembf = rembf.split('-');
	
	var rmfound = jQuery.inArray(rembf[1], facilities);
	
	if (rmfound >= 0) {
		facilities.splice(rmfound, 1);
	} 
	$('#facility').val(facilities);
	$('#'+rembf[1]).css('border','none');
	$(this).remove();
});

$('.tab-card').hide();
$('.academy-card-one-div').show();
$('#click-academy-card-1').css("cssText", "background-color: #00bcd4 !important;");
$('.university-profile-card').click(function(){
	
	var activeid = $(this).attr('id');
	
	$('.tab-card').hide();
	$('.university-profile-card').css("cssText", "background-color: #EBECEE !important;");
	$('#'+activeid).css("cssText", "background-color: #00bcd4 !important;");
	
	if(activeid == 'click-academy-card-1'){
		$('.academy-card-one-div').show();
	}else if(activeid == 'click-academy-card-2'){
		$('.academy-card-two-div').show();
	}else if(activeid == 'click-academy-card-3'){
		$('.academy-card-three-div').show();
	}else if(activeid == 'click-academy-card-4'){
		$('.academy-card-four-div').show();
	}else if(activeid == 'click-academy-card-5'){
		$('.academy-card-five-div').show();
	}
	
});



/****************************** START INFRASTRUCTURE PHOTO **********************/

$("#infrForm").on('submit',(function(e) {
	e.preventDefault();
	var filedata = new FormData(this);
	var typephoto = 'INFRASTRUCTURE';
	ajaxPhotoUpload(filedata,typephoto);	
}));

$("#infra-div").on('click','.media_remove',function(){
		var rmbttnId = $(this).attr('id');
		rmbttnId = rmbttnId.split('_');
		$(this).parent().remove();
		removePhotoUpload(rmbttnId[1]);
});

/****************************** END INFRASTRUCTURE PHOTO **********************/

/****************************** START STUDENT PLAY PHOTO **********************/

$("#spForm").on('submit',(function(e) {
	e.preventDefault();
	var filedata = new FormData(this);
	var typephoto = 'STUDENT PLAY';
	ajaxPhotoUpload(filedata,typephoto);	
}));

$("#sp-div").on('click','.media_remove',function(){
		var rmbttnId = $(this).attr('id');
		rmbttnId = rmbttnId.split('_');
		$(this).parent().remove();
		removePhotoUpload(rmbttnId[1]);
});

/****************************** END STUDENT PLAY PHOTO **********************/

/********************* START STUDENT IN CLASSROOM AND LAB PHOTO ****************/

$("#sclForm").on('submit',(function(e) {
	e.preventDefault();
	var filedata = new FormData(this);
	var typephoto = 'STUDENT IN CLASSROOM AND LAB';
	ajaxPhotoUpload(filedata,typephoto);	
}));

$("#scl-div").on('click','.media_remove',function(){
		var rmbttnId = $(this).attr('id');
		rmbttnId = rmbttnId.split('_');
		$(this).parent().remove();
		removePhotoUpload(rmbttnId[1]);
});

/******************* END STUDENT IN CLASSROOM AND LAB PHOTO **********/

/********************* START STUDENT AT ACTION PHOTO ****************/

$("#saaForm").on('submit',(function(e) {
	e.preventDefault();
	var filedata = new FormData(this);
	var typephoto = 'STUDENT AT ACTION';
	ajaxPhotoUpload(filedata,typephoto);	
}));

$("#saa-div").on('click','.media_remove',function(){
		var rmbttnId = $(this).attr('id');
		rmbttnId = rmbttnId.split('_');
		$(this).parent().remove();
		removePhotoUpload(rmbttnId[1]);
});

/******************* END STUDENT AT ACTION PHOTO **********/

/********************* START STUDENT NEWS PHOTO ****************/

$("#snForm").on('submit',(function(e) {
	e.preventDefault();
	var filedata = new FormData(this);
	var typephoto = 'STUDENT NEWS';
	ajaxPhotoUpload(filedata,typephoto);	
}));

$("#sn-div").on('click','.media_remove',function(){
		var rmbttnId = $(this).attr('id');
		rmbttnId = rmbttnId.split('_');
		$(this).parent().remove();
		removePhotoUpload(rmbttnId[1]);
});

/******************* END STUDENT NEWS PHOTO **********/


/********************* START STUDENT NEWS PHOTO ****************/

$("#sproForm").on('submit',(function(e) {
	e.preventDefault();
	var filedata = new FormData(this);
	var typephoto = 'STUDENT PROJECT';
	ajaxPhotoUpload(filedata,typephoto);	
}));

$("#spro-div").on('click','.media_remove',function(){
		var rmbttnId = $(this).attr('id');
		rmbttnId = rmbttnId.split('_');
		$(this).parent().remove();
		removePhotoUpload(rmbttnId[1]);
});

/******************* END STUDENT NEWS PHOTO **********/



/*-------------------------------------------Videos ---------------------------------*/


/****************************** START INFRASTRUCTURE PHOTO **********************/

$("#infrVidForm").on('submit',(function(e) {
	e.preventDefault();
	var filedata = new FormData(this);
	var typephoto = 'INFRASTRUCTURE VIDEO';
	ajaxPhotoUpload(filedata,typephoto);	
}));

$("#infra-vid-div").on('click','.media_remove',function(){
		var rmbttnId = $(this).attr('id');
		rmbttnId = rmbttnId.split('_');
		$(this).parent().remove();
		removePhotoUpload(rmbttnId[1]);
});

/****************************** END INFRASTRUCTURE PHOTO **********************/

/****************************** START STUDENT PLAY PHOTO **********************/

$("#spVidForm").on('submit',(function(e) {
	e.preventDefault();
	var filedata = new FormData(this);
	var typephoto = 'STUDENT PLAY VIDEO';
	ajaxPhotoUpload(filedata,typephoto);	
}));

$("#sp-vid-div").on('click','.media_remove',function(){
		var rmbttnId = $(this).attr('id');
		rmbttnId = rmbttnId.split('_');
		$(this).parent().remove();
		removePhotoUpload(rmbttnId[1]);
});

/****************************** END STUDENT PLAY PHOTO **********************/

/********************* START STUDENT IN CLASSROOM AND LAB PHOTO ****************/

$("#sclVidForm").on('submit',(function(e) {
	e.preventDefault();
	var filedata = new FormData(this);
	var typephoto = 'STUDENT IN CLASSROOM AND LAB VIDEO';
	ajaxPhotoUpload(filedata,typephoto);	
}));

$("#scl-vid-div").on('click','.media_remove',function(){
		var rmbttnId = $(this).attr('id');
		rmbttnId = rmbttnId.split('_');
		$(this).parent().remove();
		removePhotoUpload(rmbttnId[1]);
});

/******************* END STUDENT IN CLASSROOM AND LAB PHOTO **********/

/********************* START STUDENT AT ACTION PHOTO ****************/

$("#saaVidForm").on('submit',(function(e) {
	e.preventDefault();
	var filedata = new FormData(this);
	var typephoto = 'STUDENT AT ACTION VIDEO';
	ajaxPhotoUpload(filedata,typephoto);	
}));

$("#saa-vid-div").on('click','.media_remove',function(){
		var rmbttnId = $(this).attr('id');
		rmbttnId = rmbttnId.split('_');
		$(this).parent().remove();
		removePhotoUpload(rmbttnId[1]);
});

/******************* END STUDENT AT ACTION PHOTO **********/

/********************* START STUDENT NEWS PHOTO ****************/

$("#snVidForm").on('submit',(function(e) {
	e.preventDefault();
	var filedata = new FormData(this);
	var typephoto = 'STUDENT NEWS VIDEO';
	ajaxPhotoUpload(filedata,typephoto);	
}));

$("#sn-vid-div").on('click','.media_remove',function(){
		var rmbttnId = $(this).attr('id');
		rmbttnId = rmbttnId.split('_');
		$(this).parent().remove();
		removePhotoUpload(rmbttnId[1]);
});

/******************* END STUDENT NEWS PHOTO **********/


/********************* START STUDENT NEWS PHOTO ****************/

$("#sproVidForm").on('submit',(function(e) {
	e.preventDefault();
	var filedata = new FormData(this);
	var typephoto = 'STUDENT PROJECT VIDEO';
	ajaxPhotoUpload(filedata,typephoto);	
}));

$("#spro-vid-div").on('click','.media_remove',function(){
		var rmbttnId = $(this).attr('id');
		rmbttnId = rmbttnId.split('_');
		$(this).parent().remove();
		removePhotoUpload(rmbttnId[1]);
});

/******************* END STUDENT NEWS PHOTO **********/